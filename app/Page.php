<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    // get the site this page belongs to
    public function site()
    {
        return $this->belongsTo('App\Site');
    }

    // get the parent of this page
    public function parent()
    {
        return $this->belongsTo('App\Page', 'parent_id');
    }

    // get the children of this page
    public function children()
    {
        return $this->hasMany('App\Page', 'parent_id');
    }

    // get breadcrumbs
    public function getBreadcrumbsAttribute()
    {
      $breadcrumbs = array();

      // make sure this is not the homepage for a site
      if (!$this->default)
      {
        // add this page to the beginning
        $breadcrumbs[] = array($this->url, $this->title);
        $parent = $this->parent;

        // add all the parents of this page
        while (isset($parent))
        {
          $breadcrumbs[] = array($parent->url, $parent->title);
          $parent = $parent->$parent;
        }
      }

      // finally add the site and reverse sort so it's at the beginning
      $breadcrumbs[] = array('/' . $this->site->slug, $this->site->name);
      krsort($breadcrumbs);

      return $breadcrumbs;
    }

    // get url
    public function getURLAttribute()
    {
      $slugs = array();

      // make sure this is not the homepage for a site
      if (!$this->default)
      {
        // add this page's slug to the beginning
        $slugs[] = $this->slug;
        $parent = $this->parent;

        // add all the parents' slugs
        while (isset($parent))
        {
          $slugs[] = $parent->slug;
          $parent = $parent->$parent;
        }
      }

      // finally add the site's slug and reverse sort so it's at the beginning
      $slugs[] = $this->site->slug;
      krsort($slugs);

      return '/' . implode('/', $slugs);
    }

    // get the url by id
    public function getURLIDAttribute()
    {
      return route('pages.show', ['site' => site()->slug, 'id' => $this->id]);
    }
}
