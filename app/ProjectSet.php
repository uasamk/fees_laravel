<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectSet extends Model
{
    // get the site this project set belongs to
    public function site()
    {
        return $this->belongsTo('App\Site');
    }

    // get the projects for this set
    public function projects()
    {
        return $this->hasMany('App\Project');
    }
    
    // get the files for this project set
    public function files()
    {
        return $this->morphMany('App\File', 'fileable');
    }

    // url to projects index
    public function getProjectsURLAttribute()
    {
        return route('projects.index', ['site' => $this->site->slug, 'project_set' => $this->slug]);
    }

    public function getAppDeadlineTimeAttribute()
    {
        return date('g:i a', strtotime($this->application_deadline));
    }

    public function getAppDeadlineDateAttribute()
    {
        return formatDeadline($this->application_deadline);
    }

    public function getProgressDeadlineDateAttribute()
    {
        return formatDeadline($this->progress_deadline);
    }
    
    // full url for budget template
    public function getBudgetTemplateUrlAttribute()
    {
      return getFileUrl($this->files, 1);
    }

    public function getBudgetTemplateTitleAttribute()
    {
      return getFileTitle($this->files, 1);
    }
}
