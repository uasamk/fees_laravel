<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Par extends Model
{
    // get the project this PAR belongs to
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    // get the files for this PAR
    public function files()
    {
        return $this->morphMany('App\File', 'fileable');
    }

    // get the questions for this PAR
    public function questions()
    {
        return $this->morphMany('App\Question', 'questionable');
    }

    // full url for supporting file
    public function getFileSupportURLAttribute()
    {
        return getFileUrl($this->files, 2);
    }

    // full url for advice file
    public function getFileAdviceURLAttribute()
    {
        return getFileUrl($this->files, 6);
    }

    // if there is no board id in the db, auto generate one. use 'T' to indicate temporary
    public function getBoardIdAttribute($value)
    {
        return isset($value) ? $value : site()->abbreviation . '-T' . $this->id;
    }

    // clean up relationships on delete
    public function delete()
    {
        deleteItems($this->files);
        deleteItems($this->questions);
        Parent::delete();
    }
}
