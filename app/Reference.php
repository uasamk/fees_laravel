<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    protected $fillable = 
    [
        'first_name',
        'last_name',
        'email',
        'address',
        'city',
        'state',
        'zip',
        'phone',
        'relationship',
    ];

    // get the application for this reference
    public function application()
    {
        return $this->belongsTo('App\FundingStatus', 'user_application_id');
    }
}
