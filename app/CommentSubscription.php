<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentSubscription extends Model
{
    // get the project this subscription belongs to
    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
