<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundingStatus extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    
    // get the projects with this funding status
    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    public function identifiableName()
    {
        return $this->name;
    }
}
