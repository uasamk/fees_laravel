<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use App\File;

class Project extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    
    protected $fillable =
    [
      'board_id',
      'netid',
      'admin',
      'first_name',
      'last_name',
      'phone',
      'email',
      'department_id',
      'department_other',
      'division',
      'director_first_name',
      'director_last_name',
      'director_email',
      'fiscal_first_name',
      'fiscal_last_name',
      'fiscal_email',
      'project_name',
      'project_type',
      'amount',
      'permanent_funding',
      'new_project',
      'past_funding',
      'duration',
      'yearly_variation',
      'justification',
      'sole_funding',
      'agree_terms',
      'board_comments',
      'archived',
      'public',
      'locked',
      'start_year',
    ];

    protected $revisionFormattedFields = array(
        'sole_funding' => 'boolean:No|Yes',
        'agree_terms' => 'boolean:No|Yes',
        'archived' => 'boolean:No|Yes',
        'locked' => 'boolean:No|Yes',
        'public' => 'boolean:No|Yes',
        'pre_deadline' => 'boolean:No|Yes',
    );

    protected $revisionFormattedFieldNames = array(
        'board_id' => 'board id'
    );

    // default duration to 1 year if 0
    public function getDurationAttribute($value)
    {
        return $value == 0 ? 1 : $value;
    }

    // get director netid from email
    public function getDirectorNetidAttribute()
    {
        return strstr($this->director_email, '@', true);
    }

    // get the funding status of this project
    public function fundingStatus()
    {
        return $this->belongsTo('App\FundingStatus');
    }

    // get the department of this project
    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    // get the project set this project set belongs to
    public function projectSet()
    {
        return $this->belongsTo('App\ProjectSet');
    }

    // get the initiatives associated with this project
    public function initiatives()
    {
        return $this->belongsToMany('App\Initiative')->withPivot('is_grad');
    }

    // get the undergrad initiatives associated with this project
    public function undergraduateInitiatives()
    {
        return $this->belongsToMany('App\Initiative')->wherePivot('is_grad', 0);
    }

    // get the graduate initiatives associated with this project
    public function graduateInitiatives()
    {
        return $this->belongsToMany('App\Initiative')->wherePivot('is_grad', 1);
    }

    // get the files for this project
    public function files()
    {
        return $this->morphMany('App\File', 'fileable');
    }

    // get the progress reports for this project
    public function progressReports()
    {
        return $this->hasMany('App\ProgressReport');
    }

    // get the PARs for this project
    public function pars()
    {
        return $this->hasMany('App\Par');
    }

    // get the questions for this project
    public function questions()
    {
        return $this->morphMany('App\Question', 'questionable');
    }

    // get the comments for this project
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    // get the comment subscriptions for this project
    public function commentSubscriptions()
    {
        return $this->hasMany('App\CommentSubscription');
    }

    // get original application files
    public function getApplicationFilesAttribute()
    {
        return $this->files->where('application', 1);
    }

    public function getFileBudgetAttribute()
    {
        return $this->applicationFiles->where('file_type_id', 1)->first();
    }

    // full url for budget file
    public function getFileBudgetURLAttribute()
    {
        return getFileUrl($this->applicationFiles, 1);
    }

    public function getFileDescriptionAttribute()
    {
        return $this->applicationFiles->where('file_type_id', 2)->first();
    }

    // full url for description file
    public function getFileDescriptionURLAttribute()
    {
        return getFileUrl($this->applicationFiles, 2);
    }

    public function getFileExtraAttribute()
    {
        return $this->applicationFiles->where('file_type_id', 3)->first();
    }

    // full url for extra file
    public function getFileExtraURLAttribute()
    {
        return getFileUrl($this->applicationFiles, 3);
    }

    // get the current year budget url...if no current year just get latest
    public function getCurrentYearBudgetURLAttribute()
    {
        // only return a url for funded projects
        if (in_array($this->funding_status_id, [2, 3, 4])) {
            $currentYearBudget = $this->files->filter(function ($file, $key) {
                return $file->file_type_id == 1 && getFiscalYear(strtotime($file->created_at)) == currentFiscalYear();
            });
            if ($currentYearBudget->count() == 0) {
                $currentYearBudget = $this->files->filter(function ($file) {
                    return $file->file_type_id == 1;
                })->sortBy('created_at');
            }

            return $currentYearBudget->count() > 0 ? Storage::url($currentYearBudget->last()->name) : NULL;
        }
        return null;
    }

    // full url to project
    public function getUrlAttribute()
    {
        return route('projects.show', ['site' => $this->projectSet->site->slug, 'project_set' => $this->projectSet->slug, 'id' => $this->id]);
    }

    // get the funding items for this project
    public function fundings()
    {
        return $this->hasMany('App\Funding');
    }

    // get the fundings for this project - fall back on legacy information if nothing is in the fundings table
    public function getAllFundingsAttribute()
    {
        $fundings = $this->fundings;

        // if no funding data exists, calculate amounts from project data
        // this is only necessary for old projects that were imported
        if ($fundings->count() == 0) {
            $fundings = collect();

            $legacy_funding = array();
            for ($year = 1; $year <= $this->duration_received; $year++) {
                $legacy_funding[$year] = $this->amount_received;
            }

            for ($year = 1; $year <= $this->duration; $year++) {
                $funding = new Funding();
                if (isset($legacy_funding[$year])) {
                    $funding->board_amount = $legacy_funding[$year];
                }
                else {
                    $funding->board_amount = 0;
                }
                $funding->year = $year;
                $funding->requested_amount = $this->amount;
                $funding->final_amount = 0;
                $fundings->push($funding);
            }

            // if NO legacy funding data was found, just insert an empty funding item
            if ($fundings->count() == 0) {
                $fundings->push($this->_newFunding(1));
            }
        }
        // make sure there's funding data for each year in case a year was added after creation
        else if ($fundings->count() < $this->duration) {
            $last_year = $fundings->last()->year;
            
            for ($year = $last_year + 1; $year <= $this->duration; $year++) {
                $fundings->push($this->_newFunding($year));
            }
        }
        // also make sure the duration wasn't reduced
        else if ($fundings->count() > $this->duration) {
            foreach ($fundings as $key => $funding) {
                if ($funding->year > $this->duration) {
                    $fundings->forget($key);
                }
            }
        }
        
        return $fundings;
    }

    // helper function for all funding
    private function _newFunding($year) {
        $funding = new Funding();
        $funding->year = $year;
        $funding->requested_amount = 0;
        $funding->board_amount = 0;
        $funding->final_amount = 0;

        return $funding;
    }

    // get the total funding requested
    public function getTotalFundingAttribute()
    {
        $total_funding = new Funding();
        foreach ($this->allFundings as $funding) {
            $total_funding->requested_amount += $funding->requested_amount;
            $total_funding->board_amount += $funding->board_amount;
            $total_funding->final_amount += $funding->final_amount;
        }

        return $total_funding;
    }

    // get the funded duration of the project
    public function getDurationFundedAttribute()
    {
        return $this->allFundings->filter(function ($funding, $key) {
            return $funding->board_amount > 0;
        })->count();
    }

    // get the range of funding years
    public function getFundingYearRangeAttribute()
    {
        // always add the start year
        $funding_years = [$this->start_year];

        // add each additional year based on the funded duration
        $a = 1;
        while ($a < $this->durationFunded) {
            array_push($funding_years, $this->start_year + $a);
            $a++;
        }

        return $funding_years;
    }

    // get the funding for a specificed year
    public function selectedYearFunding($year)
    {
        $selectedYearFunding = $this->allFundings->filter(function ($funding, $key) use ($year) {
            return $this->start_year + ($funding->year - 1) == $year;
        });

        // if there is no funding item for the specified year (presumably, the project is in the past), just get the latest *funded* item
        if ($selectedYearFunding->count() == 0) {
            $selectedYearFunding = $this->allFundings->filter(function ($funding) {
                return $funding->board_amount > 0;
            });
        }

        // if there's still no funding items, just return all items
        if ($selectedYearFunding->count() == 0) {
            $selectedYearFunding = $this->allFundings;
        }

        return $selectedYearFunding->sortBy('year')->last();
    }

    // update the project funding
    public function updateFunding($amounts, $amount_key)
    {
      if (!empty($amounts))
      {
          
        $total = 0;
        $all_null = 0;

        foreach ($amounts as $key => $amount)
        {
            if(!is_null($amount)){
            $all_null += 1;
          // check if funding data already exists for the specified year
          $funding = Funding::where(['project_id' => $this->id, 'year' => $key]);
          $total += $amount;

          // if not, create a new funding item and associate it with the project
          if ($funding->count() == 0)
          {
            $funding = new Funding();
            $funding->year = $key;
            $funding->requested_amount = 0;
            $funding->board_amount = 0;
            $funding->final_amount = 0;
            $funding->project()->associate($this);
          }
          else 
          {
            $funding = $funding->first();
          }

          // add the amount to the funding item based on type
          switch ($amount_key) {
            case 0:
              $funding->requested_amount = $amount;
              break;
            case 1:
                $temp = $amount;
                if($amount == -3){
                    $temp = 0;
                }
                $funding->board_amount = $temp;
              break;
            case 2:
              $funding->final_amount = $amount;
              break;
          }

          $funding->save();
        }
    }

    
    if($all_null > 0){

        // if board amount, update funding status
        if ($amount_key == 1) {
            // reject if board amount is 0
              if ($total == 0) {
                $this->funding_status_id = 5;
              } else if ($total == -1) {
                $this->funding_status_id = 6; // request withdrawn
              // only proposals with full or greater than requested amounts considered fully funded
              } else if ($total >= $this->totalFunding->requested_amount) {
                $this->funding_status_id = 3;
              } else if ($total < $this->totalFunding->requested_amount) {
                $this->funding_status_id = 4; // partial funding
              }
              else if($total == -3){
                $this->funding_status_id = 3;
              }
    
              $this->save();
            }
    }
      }
    }

    // get recent progress reports
    public function recentProgressReports($days) {
        return $this->progressReports->filter( function ($progressReport) use($days) {
            $progress_date = new \DateTime($progressReport->created_at);
            $today = new \DateTime('now');
            $interval = $progress_date->diff($today);

            return $interval->days <= $days;
        })->sortByDesc('created_at');
    }

    // attach file to project
    public function attachFile($file_input, $type, $apply, $comments = NULL, $netid = NULL)
    {
      if (isset($file_input)) {
        $file = new File();
        $new = true;

        // if creating/editing application, make sure the file isn't the original. if so, replace it
        if ($apply) {
          $application_files = $this->files->filter( function($file) use ($type) {
              return $file->file_type_id == $type && $file->application;
          });
          if ($application_files->count() > 0) {
            $file = $application_files->first();
            $new = false;
          }
        }

        $file->name = $file_input->store($this->projectSet->site->slug);
        $file->file_type_id = $type;
        $file->comments = $comments;
        $file->netid = $netid;
        $file->application = $apply;
        
        if ($apply && !$new) {
            $file->save();
        }
        else {
            $this->files()->save($file);
        }
      }
    }

    // detach initiatives from project
    public function detachInitiatives($initiatives)
       {
           $this->initiatives()->detach();
       }

    // attach initiatives to project
    public function attachInitiatives($initiatives, $is_grad)
    {
      if (isset($initiatives))
      {
        // $this->initiatives()->detach();
        foreach ($initiatives as $initiative_id) 
        {
          $this->initiatives()->attach($initiative_id, ['is_grad' => $is_grad]);
        }
      }
    }

    // set the start year
    public function setStartYear()
    {
        $this->pre_deadline = strtotime('now') < strtotime(projectSet()->application_deadline);

        /**
         * determine start year based on fiscal cutoff month and whether or not a year gets added
        * SSF: Since the funding application typically goes live in November for the NEXT funding year/cycle, advance to next Year
        * GFMG: For Mini Grants the monies must be used within the CURRENT fiscal year, so step back to previous year if before cutoff
        */
        $month = date('n');
        $year = date('Y');
        if (projectSet()->fiscal_cutoff_add) {
            $this->start_year = $month >= projectSet()->fiscal_cutoff_month ? $year + 1 : $year;
        }
        else {
            $this->start_year = $month < projectSet()->fiscal_cutoff_month ? $year - 1 : $year;
        }
    }

    // clean up relations before deleting
    public function delete()
    {
        deleteItems($this->files);
        deleteItems($this->progressReports);
        deleteItems($this->pars);
        deleteItems($this->questions);
        $this->comments()->delete();
        $this->fundings()->delete();
        Parent::delete();
    }

    public function getFiscalYearAttribute() {
        return $this->start_year + 1;
    }
}   
