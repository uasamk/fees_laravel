<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStanding extends Model
{
    // get the users with this standing
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
