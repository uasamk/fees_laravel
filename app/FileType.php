<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileType extends Model
{
    // get the files of this type
    public function files()
    {
        return $this->hasMany('App\File');
    }
}
