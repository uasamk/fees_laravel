<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funding extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    
    // get the project for this funding
    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
