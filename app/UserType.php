<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    // get the users with this type
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
