<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ProgressReport extends Model
{
    // get the project this progress report belongs to
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    // get the files for this progress report
    public function files()
    {
        return $this->morphMany('App\File', 'fileable');
    }

    // full url for outcomes file
    public function getFileOutcomesURLAttribute()
    {
        return getFileUrl($this->files, 5);
    }

    // full url for budget file
    public function getFileBudgetURLAttribute()
    {
        return getFileUrl($this->files, 1);
    }
 
    // clean up relationships on delete
    public function delete()
    {
        $this->files()->delete();
        Parent::delete();
    } 
}
