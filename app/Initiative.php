<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Initiative extends Model
{
    // get the site this initiative belongs to
    public function site()
    {
        return $this->belongsTo('App\Site');
    }
    
    // get the projects that use this initiative
    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }
}
