<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    // get the users with this status
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
