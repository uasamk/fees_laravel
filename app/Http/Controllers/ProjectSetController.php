<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ProjectSet;

class ProjectSetController extends Controller
{
    public function __construct()
    {
        $this->middleware('webauth', ['except' => ['setSession']]);
        $this->middleware('auth.site', ['except' => ['setSession']]);
        $this->middleware('power', ['except' => ['setSession']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($site_slug, $id)
    {
        $project_set = ProjectSet::find($id);

        return view('admin.project-set', ['project_set' => $project_set]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $site_slug, $id)
    {
        $this->validate($request, [
            'application_deadline' => 'after:application_open',
            'board_application_deadline' => 'after:board_application_open',
            'question_deadline' => 'required|integer|min:1|max:30',
        ]);

        $project_set = ProjectSet::find($id);
        $fiscal_cutoff_month = date_parse($request->input('fiscal_cutoff_month'));

        $project_set->application_open = formatDateDb($request->input('application_open'));
        $project_set->application_deadline = formatDateDb($request->input('application_deadline'));
        $project_set->progress_deadline = formatDateDb($request->input('progress_deadline'));
        $project_set->question_deadline = $request->input('question_deadline');
        $project_set->fiscal_cutoff_month = $fiscal_cutoff_month['month'];
        $project_set->fiscal_cutoff_add = $request->has('fiscal_cutoff_add');
        
        if ($request->hasFile('budget_template')) {
            attachFile($site_slug, $project_set, $request->file('budget_template'), 1, true);
        }

        $project_set->save();

        $project_set->site->board_application_open = formatDateDb($request->input('board_application_open'));
        $project_set->site->board_application_deadline = formatDateDb($request->input('board_application_deadline'));
        $project_set->site->save();

        return redirect()->route('project-sets.edit', ['site' => $site_slug, 'id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // set the project set in session/url
    public function setSession(Request $request, $site_slug, $id)
    {
        $project_set = ProjectSet::find($id);
        $current_slug = $request->query('current');
        session([$site_slug . '.project_set_slug' => $project_set->slug]);
        if (!empty($current_slug)) {
            // if the project set slug is already in the url, swap with new project set
            return redirect(str_replace($current_slug, $project_set->slug, url()->previous()));
        }
        else {
            return back();
        }
    }
}
