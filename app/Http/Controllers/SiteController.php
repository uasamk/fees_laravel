<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Site;
use App\User;
use App\File;
use App\Department;
use App\Initiative;
use App\Mail\Contact;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('webauth', ['only' => ['admin']]);
        $this->middleware('auth.site', ['only' => ['admin']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $site = Site::where('slug', $slug)->first();
        $homepage = $site->pages->where('default', 1)->first();

        return view('page.view', ['page' => $homepage]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $site = Site::find($id);
        $users = $site->users->count() > 0 ? $site->users : User::all();

        return view('admin.site', ['users' => $users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|min:1|max:100',
        ]);

        $site = Site::find($id);

        $site->name = $request->input('name');
        //$site->slug = str_slug($site->name); //don't allow changing the slug due to file storage
        $site->abbreviation = $request->input('abbreviation');
        $site->contact_id = $request->input('contact_id');
        $site->funding_app_instructions = clean($request->input('funding_app_instructions'));
        $site->board_app_instructions = clean($request->input('board_app_instructions'));

        if ($request->hasFile('governing_doc')) {
            attachFile($site->slug, $site, $request->file('governing_doc'), 7, true);
        }

        $site->save();

        return redirect()->route('sites.edit', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function admin($site_slug, Request $request)
    {
        $applicants = $request->query('apps');

        return view('admin.index', ['applicants' => $applicants]);
    }

    // uploads
    public function saveFiles(Request $request, $id)
    {
        $this->validate($request, [
            'file.*' => 'max:9000',
        ]);

        $site = Site::find($id);

        if ($request->hasFile('uploads')) {
            foreach ($request->file('uploads') as $file) {
                attachFile($site->slug, $site, $file, 8, true, true);
            }
        }

        return $this->redirectIndex($site);
    }

    public function deleteFile($id, $file_id)
    {
        $site = Site::find($id);
        $file = File::find($file_id);
        $file->delete();

        return $this->redirectIndex($site);
    }

    // departments
    public function saveDepartment(Request $request, $id) 
    {
        $site = Site::find($id);

        if ($request->has('department')) {
            $department = new Department();
            $department->name = $request->input('department');
            $site->departments()->save($department);
        }

        return $this->redirectIndex($site);
    }

    public function deleteDepartment($id, $dept_id)
    {
        $site = Site::find($id);
        $department = Department::find($dept_id);
        $department->delete();

        return $this->redirectIndex($site);
    }

    // initiatives
    public function saveInitiative(Request $request, $id)
    {
        $site = Site::find($id);

        if ($request->has('initiative')) {
            $initiative = new Initiative();
            $initiative->name = $request->input('initiative');
            $initiative->undergraduate_order = $request->input('undergraduate_order');
            $initiative->graduate_order = $request->input('graduate_order');
            $site->initiatives()->save($initiative);
        }

        return $this->redirectIndex($site);
    }

    public function updateInitiative(Request $request, $id, $init_id)
    {
        $site = Site::find($id);

        if ($request->has('initiative')) {
            $initiative = Initiative::find($init_id);
            $initiative->name = $request->input('initiative');
            $initiative->undergraduate_order = $request->input('undergraduate_order');
            $initiative->graduate_order = $request->input('graduate_order');
            $initiative->save();
        }

        return $this->redirectIndex($site);
    }

    public function deleteInitiative(Request $request, $id, $init_id)
    {
        $site = Site::find($id);

        $initiative = Initiative::find($init_id);
        $initiative->delete();

        return $this->redirectIndex($site);
    }

    public function contact($site_slug)
    {
        return view('contact');
    }

    // send questions/comments
    public function contactSend(Request $request, $site_slug)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'g-recaptcha-response' => 'required',
        ]);

        // send to site contact
        $this->_contactSend($request, site()->contact->netid);
        // send to anyone else subscribed
        foreach (site()->users->where('question_contact', 1) as $question_contact) {
            $this->_contactSend($request, $question_contact->netid);
        }

        return redirect()->route('contact', ['site' => $site_slug])->with('sentmessage', 'Your message has been sent.');
    }

    // helper function to send emails
    public function _contactSend($request, $netid) {
        Mail::to(getNetidEmail($netid))->send(
            new Contact($request->input('name'), $request->input('email'), $request->input('message'))); 
    }

    // common redirect to content management index
    private function redirectIndex($site)
    {
        return redirect()->route('pages.index', ['site' => $site->slug]);
    }
}
