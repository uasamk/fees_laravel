<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

use App\Project;
use App\ProjectSet;
use App\ProgressReport;
use App\Mail\ProgressSubmitted;

class ProgressReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('webauth')->except('index');
        $this->middleware('auth.site')->only('request');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $project = Project::find($request->query('project'));
        return view('progress.index', ['project' => $project]);
    }

    /**
     * Display a listing of projects eligible for progress reports.
     *
     * @return \Illuminate\Http\Response
     */
    public function createStart(Request $request, $site_slug, $project_set_slug)
    {
        $netid = $request->session()->get('webauth.net_id');
        
        return view('progress.start', ['netid' => $netid, 'projects' => getActiveProjects()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $netid = $request->session()->get('webauth.net_id');
        $project = Project::find($request->query('project'));
        //check if there are any progress reports for this project from the past 30 days
        $recent = $project->recentProgressReports(30)->first();

        return view('progress.create', [
            'netid' => $netid, 
            'project' => $project, 
            'recent' => $recent,
            'admin_edit' => false
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $site_slug, $project_set_slug)
    {
        $this->validate($request, $this->rules());

        $project = Project::find($request->input('project_id'));
        $report = new ProgressReport();

        $report->netid = $project->netid;
        $report->scope = $request->input('scope');
        $report->outcomes = $request->input('outcomes');
        $report->response = $request->input('response');
        $project->progressReports()->save($report);

        attachFile($site_slug, $report, $request->file('file_outcomes'), 5);
        attachFile($site_slug, $report, $request->file('budget'), 1);

        return redirect()->route('progress.review', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $report->id])->with('sentmessage', 'Your Progress Report has been sent.')->with('delay', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($site_slug, $project_set_slug, $id, $review = false)
    {
        $report = ProgressReport::find($id);

        session()->keep('sentmessage');

        return view('progress.view', ['report' => $report, 'review' => $review]);
    }

    // alias function for reviewing reports
    public function review($site_slug, $project_set_slug, $id)
    {
        return $this->show($site_slug, $project_set_slug, $id, true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($site_slug, $project_set_slug, $id, $admin_edit = false)
    {
        $report = ProgressReport::find($id);

        return view('progress.create', [
            'project' => $report->project, 
            'report' => $report,
            'admin_edit' => $admin_edit
        ]);
    }

    public function adminEdit($site_slug, $project_set_slug, $id)
    {
        return $this->edit($site_slug, $project_set_slug, $id, true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $site_slug, $project_set_slug, $id)
    {
        $this->validate($request, $this->rules(false));

        $admin_edit = $request->input('admin_edit');

        $report = ProgressReport::find($id);

        $report->scope = $request->input('scope');
        $report->outcomes = $request->input('outcomes');
        $report->response = $request->input('response');
        $report->save();

        attachFile($site_slug, $report, $request->file('file_outcomes'), 5);
        attachFile($site_slug, $report, $request->file('budget'), 1);

        if ($admin_edit) {
            return redirect()->route('progress.index', ['site' => $site_slug, 'project_set' => $project_set_slug, 'project' => $report->project->id]);
        }
        else {
            return redirect()->route('progress.review', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $report->id])->with('sentmessage', 'Your Progress Report has been sent.');
        }
    }

    // move to another project
    public function move(Request $request, $site_slug, $project_set_slug, $id)
    {
        $report = ProgressReport::find($id);
        $project_id = $request->input('project_id');
        moveAttachment($report, 'project_id', $project_id);
        return redirect()->route('progress.index', ['site' => $site_slug, 'project_set' => $project_set_slug, 'project' => $project_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($site_slug, $project_set_slug, $id)
    {
        $report = ProgressReport::find($id);
        $project = $report->project;
        $report->delete();

        return redirect()->route('progress.index', ['site' => $site_slug, 'project_set' => $project_set_slug, 'project' => $project->id])->with('sentmessage', 'The progress report has been deleted.');
    }

    /**
     * Send the progress report request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request, $site_slug, $project_set_slug) {
        $project = Project::find($request->input('project_id'));
        $comments = $request->input('comments');

        // check if project is set...if not, send request for all locked, active projects older than 10 months
        if ($project != null) {
            $this->_send($project, $comments);

            return redirect()->route('projects.index', ['site' => $site_slug, 'project_set' => $project->projectSet->slug])->with('sentmessage', 'Your progress report request has been sent.');
        }
        else {
            $today = new \DateTime('now');
            $limit = $today->sub(new \DateInterval('P10M'));
            $project_set = site()->projectSets()->where('slug', $project_set_slug)->first();

            $projects = Project::where(['project_set_id' => $project_set->id, 'archived' => 0, 'locked' => 1, 'permanent_funding' => 0, 'admin' => 0])
                ->whereDate('final_submit_time', '<', $limit->format('Y-m-d'))
                ->whereIn('funding_status_id', [2, 3, 4])
                ->orderBy('start_year', 'project_name')
                ->get();

            foreach ($projects as $project) {
                $this->_send($project, $comments, true);
            }
        }
        
        return redirect()->route('admin', ['site' => $site_slug])->with('applicants', $projects->count());
    }

    // helper function to send the email(s)
    private function _send($project, $comments, $queue = false) {
        if (!empty($project->email)) {
            $this->_sendOrQueue($queue, $project->email, $project, $comments, 1);
        }
        if (!empty($project->director_email) && $project->director_email != $project->email) {
            $this->_sendOrQueue($queue, $project->director_email, $project, $comments, 2);
        }
        if (!empty($project->fiscal_email) && $project->fiscal_email != $project->email) {
            $this->_sendOrQueue($queue, $project->fiscal_email, $project, $comments, 3);
        }
    }

    // helper function to determine if mail should be sent or queued
    private function _sendOrQueue($queue, $email, $project, $comments, $message_type) {
        if ($queue) {
            Mail::to($email)->queue(new ProgressSubmitted($project, $message_type, $comments));
        }
        else {
            Mail::to($email)->send(new ProgressSubmitted($project, $message_type, $comments)); 
        }
    }

    // validation rules
    private function rules($new = true) {
        $required_new = $new ? 'required|' : '';

        return [
            'scope' => 'required|min:25', //|maxwordcount:250',
            'outcomes' => 'required|min:25', //|maxwordcount:800',
            'response' => 'required|min:25', //|maxwordcount:300',
            'file_outcomes' => $required_new . 'file|mimes:pdf|max:1500',
            'budget' => $required_new . 'file|mimes:xlsx,pdf|max:1500',
        ];
    }
}
