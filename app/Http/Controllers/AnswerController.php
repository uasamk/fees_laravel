<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Question;
use App\Answer;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($site_slug, Request $request)
    {
        $question = Question::find($request->query('question'));
        $title = $question->questionable_type == 'App\Par' ? 'PAR' : 'Project';

        if (strtotime('now') > strtotime($question->deadline)) {
            return redirect()->route('projects.show', ['site' => $site_slug, 'project_set' => $question->project->projectSet->slug, 'id' => $question->project->id])->with('error', 'The deadline to submit answers to your question has passed.');
        }
        else {
            return view('answer', ['question' => $question, 'title' => $title]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($site_slug, Request $request)
    {
        $this->validate($request, [
            'answer_text' => 'required|min:25|maxwordcount:1500',
        ]);

        $question = Question::find($request->input('question_id'));
        $answer = new Answer();

        $answer->answer_text = $request->input('answer_text');
        $question->answers()->save($answer);

        return redirect()->route('projects.show', ['site' => $site_slug, 'project_set' => $question->project->projectSet->slug, 'id' => $question->project->id])->with('sentmessage', 'Your answer has been submitted.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
