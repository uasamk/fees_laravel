<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\UserTitle;
use App\UserStanding;
use App\UserStatus;
use App\UserType;
use App\Role;
use App\UserApplication;
use App\Reference;

use App\Mail\UserAppNotification;

use CountryState;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('webauth', ['only' => ['login', 'index', 'create', 'edit', 'delete', 'apply', 'confirm', 'applyEdit']]);
        $this->middleware('auth.site', ['only' => ['applyIndex', 'applyView']]);
        $this->middleware('power', ['only' => ['index', 'create', 'edit', 'delete', 'applyDestroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $site_slug, $directory = false)
    {
        $users = site()->users->sortByDesc('start_year');
        $admin_users = $users->whereNotIn('user_type_id', [3, 4])->where('user_status_id', 1);
        $users = $directory ? 
            $users->where('directory', 1)
            ->where('user_type_id', 3)
            ->where('user_status_id', 1) : $users;
        $user_statuses = UserStatus::all();
        $outside_users = User::all()->filter( function($user) {
            return $user->sites->count() == 0 || $user->sites->where('id', site()->id)->count() == 0;
        });

        $years = collect();
        foreach ($users as $user) {
            if (!empty($user->fiscal_year)) {
                $years->push($user->fiscal_year);
            }
        }
        $years = $years->unique()->sort()->reverse();

        // check if filters were submitted or exist in the session
        if (!empty($request->all()) || $request->session()->has('user-filters')) {
            $user_status = filterParam('user-filters.', 'status', $request);
            $year = filterParam('user-filters.', 'year', $request);

            $users = $users->filter(function($user) use($user_status) {
                return $user_status == -1 ? true : $user->user_status_id == $user_status;
            })->filter(function($user) use($year) {
                return $year == -1 ? true : $user->fiscal_year == $year;
            });

            $request->session()->put('user-filters.status', $user_status);
            $request->session()->put('user-filters.year', $year);
            $request->session()->save();
        }

        return view('user.index', [
            'users' => $users, 
            'admin_users' => $admin_users,
            'user_statuses' => $user_statuses,
            'years' => $years,
            'outside_users' => $outside_users,
            'directory' => $directory,
        ]);
    }

    public function directory(Request $request, $site_slug) 
    {
        return $this->index($request, $site_slug, true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create', $this->_createParams(['user_is_contact' => false]));
    }

    // paramaters for create form
    private function _createParams($extras = null)
    {
        $params = [
            'states' => CountryState::getStates('US'),
            'user_titles' => UserTitle::all(),
            'user_standings' => UserStanding::all(),
            'user_types' => UserType::all(),
            'user_statuses' => UserStatus::all(),
            'roles' => Role::all(),
        ];

        if (!empty($extras)) {
            return array_merge($params, $extras);
        }

        return $params;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $site_slug)
    {
        $this->validate($request, $this->rules(), $this->messages());

        $user = new User($request->all());
        
        // handle checkboxes
        $user->directory = $request->has('directory');
        $user->par_contact = $request->has('par_contact');
        $user->question_contact = $request->has('question_contact');

        $user->save();

        $user->sites()->attach(site()->id);

        return redirect()->route('admin', ['site' => $site_slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($site_slug, $id)
    {
        $user = User::find($id);

        return view('user.create', $this->_createParams(['object' => $user, 'user_is_contact' => (isset(site()->contact) && $user->id === site()->contact->id)]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $site_slug, $id)
    {
        $user = User::find($id);
        $this->validate($request, $this->rules($user), $this->messages());

        $user->update($request->all());

        // handle checkboxes
        $user->directory = $request->has('directory');
        $user->par_contact = $request->has('par_contact');
        $user->question_contact = $request->has('question_contact');
        $user->save();

        return redirect()->route('users.index', ['site' => $site_slug]);
    }

    public function updateSite(Request $request, $site_slug)
    {
        $user = User::find($request->input('user_id'));
        $user->sites()->attach(site()->id);

        return redirect()->route('users.index', ['site' => $site_slug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($site_slug, $id)
    {
        $user = User::find($id);

        // if the user has access to other sites, just remove them from the current site
        if ($user->sites->count() > 1) {
            $user->sites()->detach(site()->id);
        }
        // otherwise, delete as per usual
        else {
            $user->delete();
        }

        return redirect()->route('users.index', ['site' => $site_slug]);
    }

    // apply - create or update a user through application
    public function apply(Request $request, $site_slug)
    {
        if (boardAppClosed(true)) {
            return deadlineRedirect($site_slug, site()->board_application_open);
        }

        $netid = $request->session()->get('webauth.net_id');

        // get user based on netid
        $user = User::where('netid', $netid)->first();

        return view('user.apply.create', $this->_createParams(['user' => $user, 'user_is_contact' => false]));
    }

    public function applyStore(Request $request, $site_slug)
    {
        $this->validate($request, $this->rulesApply(false), $this->messages(), $this->attributes());

        $netid = $request->session()->get('webauth.net_id');
        $user = User::where('netid', $netid)->first();

        if ($user !== null) {
            $user->update($request->all());
        }
        else {
            $user = new User($request->all());
            $user->user_status_id = 4;
            $user->role_id = 1;
            $user->netid = $netid;
            $user->save();
            $user->sites()->attach(site()->id);
        }

        $application = new UserApplication($request->all());
        $application->graduation_date = formatDateDb($request->input('graduation_date'));
        $application->membership_type = $request->input('membership_type');
        $application->locked = false;
        $application->user()->associate($user);
        $application->save();

        attachFile($site_slug, $application, $request->file('resume'), 9);

        $references_input = $request->input('reference');
        foreach ($references_input as $reference_input) {
            $reference = new Reference($reference_input);
            $reference->application()->associate($application);
            $reference->save();
        }

        return redirect()->route('users.apply.confirm', ['site' => $site_slug, 'id' => $application->id]);
    }

    public function applyView($site_slug, $id, $confirm = false)
    {
        $application = UserApplication::find($id);
        $application->status = isset($application->accepted) ? 3 - $application->accepted : 1;

        if ($confirm && !$this->_creatorAccess($application)) {
            return redirectBackError('This application has already been submitted.');
        }

        return view('user.apply.view', ['application' => $application, 'confirm' => $confirm, 'applicant_statuses' => $this->_applicantStatuses()]);
    }

    public function confirm($site_slug, $id)
    {
        return $this->applyView($site_slug, $id, true);
    }

    public function applyEdit($site_slug, $id)
    {
        $application = UserApplication::find($id);
        $user = $application->user;

        if (!userIsPower() && !$this->_creatorAccess($application)) {
            return redirectBackError('You are not authorized to edit that application.');
        }

        return view('user.apply.create', $this->_createParams(['object' => $application, 'user' => $user, 'user_is_contact' => false]));
    }

    private function _creatorAccess($application) {
        return $application->user->netid == session()->get('webauth.net_id') && !$application->locked;
    }

    public function applyUpdate(Request $request, $site_slug, $id)
    {
        $application = UserApplication::find($id);

        $this->validate($request, $this->rulesApply(isset($application->fileResumeURL)), $this->messages(), $this->attributes());

        $application->user->update($request->all());
        $application->update($request->all());
        $application->graduation_date = formatDateDb($request->input('graduation_date'));
        $application->save();

        attachFile($site_slug, $application, $request->file('resume'), 9);

        $a = 1;
        $references_input = $request->input('reference');
        foreach ($application->references as $reference) {
            $reference->update($references_input[$a]);
            $a++;
        }

        return redirect()->route('users.apply.confirm', ['site' => $site_slug, 'id' => $application->id]);
    }

    public function applySave(Request $request, $site_slug, $id)
    {
        $application = UserApplication::find($id);
        $application->locked = true;
        $application->save();

        Mail::to($application->user->email)->send(new UserAppNotification($application, 1));
        
        return redirect('/' . site()->slug)->with('sentmessage', 'Your application has been submitted!');
    }

    public function applyAccept(Request $request, $site_slug, $id)
    {
        $application = UserApplication::find($id);

        $application->review_comments = $request->input('review_comments');
        $application->accepted = $request->input('status') == 1 ? null : 3 - $request->input('status');
        $application->reject_email = $request->has('reject_email');
        $application->save();

        // only change the user status if their status was not set to 'N/A' (Power User)
        if ($application->user->user_status_id != 3) {
            if ($request->has('accepted')) {
                if ($application->accepted) {
                    $application->user->user_status_id = 1;
                }
                else {
                    $application->user->user_status_id = 2;
                }
            }
            else {
                $application->user->user_status_id = 4;
            }
            $application->user->save();
        }

        if ($application->reject_email) {
            Mail::to($application->user->email)->send(new UserAppNotification($application, 2));
        }

        return redirect()->route('users.apply.index', ['site' => $site_slug])->with('sentmessage', 'Your comments and status updates have been noted in the database' . ($application->reject_email ? ' and a rejection email has been sent to the candidate.' : '.') . ' Applicants accepted to the board will appear in the "Edit a Board Member" section available from the main Board Room page.');
    }

    public function applyIndex(Request $request)
    {
        $applications = UserApplication::all();
        $applications->filter(function ($application) {
            return $application->user->sites->where('id', site()->id)->count() > 0;
        });

        $years = collect();
        foreach ($applications as $application) {
            $years->push(date('Y', strtotime($application->created_at)));
        }
        $years = $years->unique()->sort()->reverse();

        // check if filters were submitted or exist in the session
        if (!empty($request->all()) || $request->session()->has('board-app-filters')) {
            $applicant_status = filterParam('board-app-filters.', 'status', $request);
            $year = filterParam('board-app-filters.', 'year', $request);

            $applications = $applications->filter(function ($application) use($applicant_status) {
                if ($applicant_status == -1) return true;
                if ($applicant_status == 1) return $application->accepted === null;
                $applicant_status = 3 - $applicant_status; // invert the options to 'convert' to boolean
                return $applicant_status === $application->accepted;
            })->filter(function($application) use($year) {
                return $year == -1 ? true : date('Y', strtotime($application->created_at)) == $year;
            });

            $request->session()->put('board-app-filters.status', $applicant_status);
            $request->session()->put('board-app-filters.year', $year);
            $request->session()->save();
        }

        return view('user.apply.index', ['applications' => $applications, 'applicant_statuses' => $this->_applicantStatuses([null => 'Pending Review']), 'years' => $years]);
    }

    public function applyDestroy($site_slug, $id)
    {
        $application = UserApplication::find($id);
        $application->delete();

        return redirect()->route('users.apply.index', ['site' => $site_slug]);
    }

    private function _applicantStatuses() {
        return [1 => 'Pending Review', 2 => 'Accepted', 3 => 'Rejected'];
    }

    // custom login based on netid
    public function login(Request $request)
    {
      $netid = $request->session()->get('webauth.net_id');

      // get user based on netid
      $user = User::where('netid', $netid)->first();

      // authenticate user
      if (isset($user))
      {
        Auth::login($user);
      }

      // if user was successfully authenticated and they have access to the site, send them on their way
      // otherwise go back and display access denied message
      if (null !== Auth::user())
      {
        if (session()->has('url.previous')) {
            return redirect(session('url.previous'));
        }
        return redirect()->intended();
      }
      else
      {
        // check if we just came from webauth to redirect back to the correct url...otherwise just go back
        if ($request->session()->get('url.intended') == url()->previous())
        {
          $request->session()->forget('url.intended');
          return redirect($request->session()->get('url.previous'))->with('error', accessError());
        }

        return redirect()->back()->with('error', accessError());
      }
    }

    private function rules($user = null) {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'start_year' => 'required',
            'user_title_id' => 'required',
            'title_other' => 'required_if:user_title_id,8',
            'user_standing_id' => 'required',
            'user_type_id' => 'required',
            'user_status_id' => 'required',
            'role_id' => 'required',
            'netid' => [
                'required',
                isset($user) ? ('unique:users,netid,' . $user->id) : 'unique:users',
            ],
        ];
    }

    private function rulesApply($resumeAttached) {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'student_id' => 'required',
            'phone' => 'required|regex:/[0-9]{3}-[0-9]{3}-[0-9]{4}/|size:12',
            'email' => 'required|email',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'club_org_1' => 'required',
            'club_org_2' => 'required',
            'club_org_3' => 'required',
            'club_org_4' => 'required',
            'user_standing_id' => 'required',

            'major' => 'required',
            'minor' => 'required',
            'gpa' => 'required|numeric',
            'gpa_cumulative' => 'required|numeric',
            'graduation_date' => 'required',
            'credits_fall' => 'required|numeric',
            'credits_spring' => 'required|numeric',
            'current_jobs' => 'required|numeric',
            'job_hours' => 'required_if:current_jobs,1|numeric',
            'job_description' => 'required_if:current_jobs,1|min:25',
            'board_interest' => 'required|min:25',
            'board_activities' => 'required|min:25',
            'resume' => !$resumeAttached ? 'required' : '',
            //'board_other' => 'required',
            'agree_terms' => 'required',
            //'accepted' => 'required',
            //'reject_email' => 'required',
            //'review_comments' => 'required',

            'reference.*.first_name' => 'required',
            'reference.*.last_name' => 'required',
            'reference.*.email' => 'required',
            'reference.2.email' => 'different:reference.1.email',
            'reference.*.address' => 'required',
            'reference.*.city' => 'required',
            'reference.*.state' => 'required',
            'reference.*.zip' => 'required',
            'reference.*.phone' => 'required|regex:/[0-9]{3}-[0-9]{3}-[0-9]{4}/|size:12',
            'reference.*.relationship' => 'required',
        ];
    }

    private function messages() {
        $phone_message = 'Please enter a valid phone number. E.g. 520-123-4567';

        return [
            'user_title_id.required' => 'Please select a title.',
            'title_other.required_if' => 'Please select a title.',
            'user_standing_id.required' => 'Please select a class standing.',
            'user_type_id.required' => 'Please select a type.',
            'user_status_id.required' => 'Please select a status.',
            'role_id.required' => 'Please select an access level.',
            '*phone.regex' => $phone_message,
            '*phone.size' => $phone_message,
            'reference.2.email.different' => 'The contact informaton for your second reference cannot be same as the first',
        ];
    }

    private function attributes() {
        return [
            'reference.*.first_name' => 'first name',
            'reference.*.last_name' => 'last name',
            'reference.*.email' => 'email',
            'reference.*.address' => 'address',
            'reference.*.city' => 'city',
            'reference.*.state' => 'state',
            'reference.*.zip' => 'zip',
            'reference.*.phone' => 'phone',
            'reference.*.relationship' => 'relationship',
            'gpa_cumulative' => 'cumulative gpa'
        ];
    }
}
