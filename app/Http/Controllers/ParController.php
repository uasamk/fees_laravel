<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Project;
use App\Par;
use App\Mail\ParNotification;

class ParController extends Controller
{
    private $text_rules;
    private $file_rules;

    public function __construct()
    {
        $this->middleware('webauth')->except('index');
        $this->middleware('auth.site', ['only' => ['admin']]);
        $this->middleware('chair', ['only' => ['vp', 'advice']]);
        $this->middleware('power', ['only' => ['adminEdit', 'destroy']]);

        $this->text_rules = 'min:25|maxwordcount:1500';
        $this->file_rules = 'file|mimes:pdf,doc,docx,xls,xlsx|max:1500';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $project = Project::find($request->query('project'));
        return view('par.index-project', ['project' => $project]);
    }

    // admin index
    public function admin(Request $request, $site_slug, $project_set_slug)
    {
        $pars = Par::all();
        $pars = $pars->filter( function($par) use($project_set_slug) {
            return $par->project->projectSet->slug == $project_set_slug;
        });

        $par_statuses = [1 => 'Pending', 2 => 'Approved', 3 => 'Rejected'];

        // check if filters were submitted or exist in the session
        if (!empty($request->all()) || $request->session()->has('par-status')) {
            $par_status = filterParam('', 'par-status', $request);

            $pars = $pars->filter(function($par) use($par_status) {
                if ($par_status == 2) return ($par->approved && $par->vp_approved);
                else if ($par_status == 3) return ($par->approved === 0 || $par->vp_approved === 0);
                else if ($par_status == -1) return true;

                return $par->approved === null || $par->vp_approved === null;
            });

            $request->session()->put('par-status', $par_status);
            $request->session()->save();
        }

        return view('par.index', ['pars' => $pars, 'par_statuses' => $par_statuses]);
    }

    /**
     * Display a listing of projects eligible for PARs.
     *
     * @return \Illuminate\Http\Response
     */
    public function createStart(Request $request, $site_slug, $project_set_slug)
    {
        $netid = $request->session()->get('webauth.net_id');

        return view('par.start', ['netid' => $netid, 'projects' => getActiveProjects()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $site_slug)
    {
        $netid = $request->session()->get('webauth.net_id');
        $project = Project::find($request->query('project'));

        return view('par.create', ['netid' => $netid, 'project' => $project, 'mode' => 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $site_slug, $project_set_slug)
    {
        $this->validate($request, $this->rules());

        $project = Project::find($request->input('project_id'));
        $par = new Par();

        $par->netid = $project->netid;
        $par->changes = $request->input('changes');
        $project->pars()->save($par);

        attachFile($site_slug, $par, $request->file('file_support'), 2);
        $this->_prepareNotifications($par, 1);
        $this->_prepareNotifications($par, 2);

        return redirect()->route('pars.review', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $par->id])->with('sentmessage', 'Your PAR has been sent.')->with('delay', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($site_slug, $project_set_slug, $id, $review = false, $mode = 4)
    {
        $par = Par::find($id);

        // keep the sent message for another round since final submit is plain link
        session()->keep('sentmessage');

        $title = 'PAR';
        $edit_route = 'edit';
        $submit_route = route('pars.start', ['site' => $site_slug, 'project_set' => $project_set_slug]);

        if ($mode == 2) {
            $title = 'Advice';
            $edit_route = 'advice';
            $submit_route = route('pars.index', ['site' => $site_slug, 'project_set' => $project_set_slug]);
        }
        else if ($mode == 3) {
            $title = 'VP Review';
            $edit_route = 'vp';
            $submit_route = route('pars.approve', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $par->id]);
        }

        return view('par.view', [
            'par' => $par, 
            'review' => $review, 
            'mode' => $mode,
            'title' => $title,
            'edit_route' => $edit_route,
            'submit_route' => $submit_route,
        ]);
    }

    // alias function for reviewing pars
    public function review($site_slug, $project_set_slug, $id)
    {
        return $this->show($site_slug, $project_set_slug, $id, true, 1);
    }

    // review advice portion of par
    public function reviewAdvice($site_slug, $project_set_slug, $id)
    {
        return $this->show($site_slug, $project_set_slug, $id, true, 2);
    }

    // review vp portion of par
    public function reviewVp($site_slug, $project_set_slug, $id)
    {
        return $this->show($site_slug, $project_set_slug, $id, true, 3);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($site_slug, $project_set_slug, $id, $mode = 1)
    {
        $par = Par::find($id);
        $admin_edit = false;

        if ($mode == 2) {
            return view('par.advice', ['par' => $par, 'mode' => $mode]);    
        }
        else if ($mode == 3) {
            return view('par.vp', ['par' => $par, 'mode' => $mode]); 
        }

        return view('par.create', ['project' => $par->project, 'par' => $par, 'mode' => $mode]);
    }

    // alias function for adding advice
    public function advice($site_slug, $project_set_slug, $id)
    {
        return $this->edit($site_slug, $project_set_slug, $id, 2);
    }

    // alias function for adding vp approval
    public function vp($site_slug, $project_set_slug, $id)
    {
        return $this->edit($site_slug, $project_set_slug, $id, 3);
    }

    // alias function for admin editing
    public function adminEdit($site_slug, $project_set_slug, $id)
    {
        return $this->edit($site_slug, $project_set_slug, $id, 4);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $site_slug, $project_set_slug, $id)
    {
        $mode = $request->input('mode');
        $par = Par::find($id);

        $this->validate($request, $this->rules(false));

        // if admin edit, validate/save advice/vp review
        if ($mode == 4) {
            $this->_validateAdvice($request);
            $this->_validateVp($request);

            $this->_updateAdvice($request, $par, $site_slug);
            $this->_updateVp($request, $par);

            $par->board_id = $request->input('board_id');
        }

        $par->changes = $request->input('changes');
        $par->save();

        attachFile($site_slug, $par, $request->file('file_support'), 2);

        if ($mode == 4) {
            return redirect()->route('pars.admin.index', ['site' => $site_slug, 'project_set' => $project_set_slug]);
        }
        return redirect()->route('pars.review', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $par->id])->with('sentmessage', 'Your PAR has been sent.');
    }

    public function updateAdvice(Request $request, $site_slug, $project_set_slug, $id)
    {
        $this->_validateAdvice($request);

        $par = Par::find($id);
        $this->_updateAdvice($request, $par, $site_slug);
        $this->_prepareNotifications($par, 3);
        $par->save();

        return redirect()->route('pars.advice.review', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $par->id])->with('sentmessage', 'Your Advice has been saved.');
    }

    // advice helper functions
    private function _validateAdvice($request)
    {
        $this->validate($request, [
            'advice' => 'required|' . $this->text_rules,
            'approved' => 'required',
            'file_support_advice' => $this->file_rules,
        ]);
    }
    private function _updateAdvice($request, &$par, $site_slug)
    {
        $par->advice = $request->input('advice');
        $par->approved = $request->input('approved');
        attachFile($site_slug, $par, $request->file('file_support_advice'), 6);
    }

    public function updateVp(Request $request, $site_slug, $project_set_slug, $id)
    {
        $this->_validateVp($request);

        $par = Par::find($id);
        $this->_updateVp($request, $par);
        $par->save();

        return redirect()->route('pars.vp.review', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $par->id])->with('sentmessage', 'Your VP Review has been sent.');
    }

    // vp helper functions
    private function _validateVp($request)
    {
        $this->validate($request, [
            'vp_comments' => 'nullable|' . $this->text_rules,
            'vp_approved' => 'required',
        ]);
    }
    private function _updateVp($request, &$par)
    {
        $par->vp_comments = $request->input('vp_comments');
        $par->vp_approved = $request->input('vp_approved');
    }

    public function approve($site_slug, $project_set_slug, $id) {
        $par = Par::find($id);
        
        $this->_prepareNotifications($par, 4);

        return redirect()->route('pars.index', ['site' => $site_slug, 'project_set' => $project_set_slug])->with('sentmessage', 'Your VP Review has been sent.');
    }

    // route par notifications to the appropriate contacts
    private function _prepareNotifications($par, $message_type)
    {
        $project = $par->project;
        
        // board notifications
        if ($message_type == 1 || $message_type == 3) {
            // always send board notifications to site contact
            if (isset(site()->contact)) {
                $this->_sendNotification(getNetidEmail(site()->contact->netid), $par, $message_type);
            }
            // also send to any subscribed board member
            foreach (site()->users->where('par_contact', 1) as $par_contact) {
                $this->_sendNotification(getNetidEmail($par_contact->netid), $par, $message_type);
            }
        }
        // user notifications
        else {
            if (!empty($project->email)) {
                $this->_sendNotification($project->email, $par, $message_type);
            }
            if (!empty($project->director_email) && $project->director_email != $project->email) {
                $this->_sendNotification($project->director_email, $par, $message_type);
            }
            if (!empty($project->fiscal_email) && $project->fiscal_email != $project->email) {
                $this->_sendNotification($project->fiscal_email, $par, $message_type);
            }
        }
    }

    // send the notification
    private function _sendNotification($email, $par, $message_type) {
        Mail::to($email)->send(new ParNotification($par, $message_type));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($site_slug, $project_set_slug, $id)
    {
        $par = Par::find($id);
        $board_id = 'PAR ' . $par->board_id;
        $par->delete();

        return redirect()->route('pars.admin.index', ['site' => $site_slug, 'project_set' => $project_set_slug])->with('sentmessage', $board_id . ' has been deleted.');
    }

    // validation rules
    private function rules($new = true) {
        $required_new = $new ? 'required|' : '';

        return [
            'changes' => 'required|' . $this->text_rules,
            'file_support' => $this->file_rules,
        ];
    }
}
