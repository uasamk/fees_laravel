<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Site;
use App\ProjectSet;
use App\Project;
use App\FundingStatus;
use App\Department;
use App\Initiative;
use App\Funding;
use App\File;
use App\Comment;
use App\CommentSubscription;

use App\Mail\ProjectSubmitted;
use App\Mail\FileUploaded;
use App\Mail\CommentNotification;


class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('webauth', ['except' => ['index', 'filter', 'show', 'full', 'approve', 'revisions', 'budgets', 'notifications']]);
        $this->middleware('auth.site', ['only' => ['comments', 'review']]);
        $this->middleware('chair', ['only' => ['admin']]);
        $this->middleware('admin', ['only' => ['saveFile']]);
        $this->middleware('power', ['only' => ['deleteFile', 'moveFile']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $site_slug, $project_set_slug, $level = 0)
    {
      $params = $this->_index();
      $projects = projectSet()->projects->sortByDesc(function($project) {
        return date('Y', strtotime($project->created_at)) . $project->board_id;
      });

      // show public projects only for public view
      if ($level === 0)
      {
        $projects = $projects->where('public', 1);

        if (empty($request->all()) && !$request->session()->has('project-filters')) {
          $request->session()->put('project-filters.funding-status', 2);
          $request->session()->put('project-filters.year', currentFiscalYear());
          $request->session()->put('project-filters.project-status', '0');
        }
      }

      // check if filters were submitted or exist in the session
      if (!empty($request->all()) || $request->session()->has('project-filters'))
      {
        $funding_status_id = filterParam('project-filters.', 'funding-status', $request);
        $year = filterParam('project-filters.', 'year', $request);
        $archived = filterParam('project-filters.', 'project-status', $request);
        $progress_reports = filterParam('project-filters.', 'progress-reports', $request);
        $admin_projects = filterParam('project-filters.', 'admin-projects', $request);

        $projects = $projects->filter(function($project) use($funding_status_id) {
            return $funding_status_id == -1 ? true : ($project->funding_status_id == $funding_status_id || $project->fundingStatus->alias_id == $funding_status_id);
        })->filter(function($project) use($year) {
            return $year == -1 ? true : in_array($year, $project->fundingYearRange);
        })->filter(function($project) use($archived) {
            return $archived == -1 ? true : $project->archived == $archived;
        })->filter(function($project) use($progress_reports) {
            return (empty($progress_reports) || $progress_reports == -1) ? true : $project->recentProgressReports($progress_reports)->count() > 0;
        })->filter(function($project) use($admin_projects) {
            return $admin_projects ? $project->admin == true : true;
        });

        $request->session()->keep(['requestsent', 'sentmessage']);
        $request->flash();
        $request->session()->put('project-filters.funding-status', $funding_status_id);
        $request->session()->put('project-filters.year', $year);
        $request->session()->put('project-filters.project-status', $archived);
        $request->session()->put('project-filters.progress-reports', $progress_reports);
        $request->session()->put('project-filters.admin-projects', $admin_projects);
      }

      $return_route = $level > 1 ? 'projects.admin.index' : ($level > 0 ? 'projects.review' : 'projects.index');
      $request->session()->put('project-index', $return_route);
      $request->session()->save();

      return view('project.index',
      [
        'funding_statuses' => $params['funding_statuses'],
        'years' => $params['years'],
        'apply' => false,
        'projects' => $projects,
        'authorized' => Auth::check() && $level > 0,
        'admin' => userIsChair() && $level > 1,
        'filter_route' => $return_route,
      ]);
    }

    // helper function for index and filter
    private function _index()
    {
      $params = array();
      $params['funding_statuses'] = FundingStatus::where('public', 1)->get();
      // use the funding year ranges from all projects to get a complete list of possible years
      $params['years'] = collect();
      foreach (projectSet()->projects as $project) {
        $params['years'] = $params['years']->merge($project->fundingYearRange);
      }
      $params['years'] = $params['years']->unique()->sort()->reverse();

      return $params;
    }

    // Alias methods for index to hide sensitive content
    public function review(Request $request, $site_slug, $project_set_slug) {
      return $this->index($request, $site_slug, $project_set_slug, 1);
    }
    public function admin(Request $request, $site_slug, $project_set_slug) {
      return $this->index($request, $site_slug, $project_set_slug, 2);
    }

    /**
     * Show the list of project revisions
     *
     * @return \Illuminate\Http\Response
     */
    public function files($site_slug, $project_set_slug, $id, $type)
    {
      $project = Project::find($id);
      // get all files for this project except the first since that is part of the original application
      $files = $project->files->filter( function($file) use ($type) {
        return $file->file_type_id == $type && !$file->application;
      })->sortByDesc('created_at');
      
      $title1 = $title2 = '';
      if ($type == 1) {
        $title1 = $title2 = "budget";
      }
      else if ($type == 2) {
        $title1 = "project revision";
        $title2 = "revision";
      }
      else {
        $title1 = "funding notification";
        $title2 = "notification";
      }

      return view('project.files', [
        'project' => $project, 
        'files' => $files, 
        'type' => $type,
        'title1' => $title1,
        'title2' => $title2,
      ]);
    }

    // Alias methods for files
    public function revisions($site_slug, $project_set_slug, $id) {
      return $this->files($site_slug, $project_set_slug, $id, 2);
    }
    public function budgets($site_slug, $project_set_slug, $id) {
      return $this->files($site_slug, $project_set_slug, $id, 1);
    }
    public function notifications($site_slug, $project_set_slug, $id) {
      return $this->files($site_slug, $project_set_slug, $id, 4);
    }

    /**
     * Save project files
     *
     * @return \Illuminate\Http\Response
     */
    public function saveFile(Request $request, $site_slug, $project_set_slug, $id)
    {
      $type = $request->input('type');
      $instructions = $request->input('instructions', false);

      $validation_extra = $type == 1 ? ',xls,xlsx' : ''; // allow excel for budgets only

      $this->validate($request, [
        'file' => 'required|mimes:pdf,doc,docx' . $validation_extra,
      ]);

      $project = Project::find($id);
      $project->attachFile(
        $request->file('file'), 
        $type,
        false, 
        $request->input('comments'), 
        $request->session()->get('webauth.net_id')
      );

      if (!empty($project->email)) {
        Mail::to($project->email)->send(new FileUploaded($project, $type, $instructions));
      }
      if (!empty($project->director_email) && $project->director_email != $project->email) {
        Mail::to($project->director_email)->send(new FileUploaded($project, $type, $instructions));
      }
      if (!empty($project->fiscal_email) && $project->fiscal_email != $project->email) {
        Mail::to($project->fiscal_email)->send(new FileUploaded($project, $type, $instructions));
      }

      return redirect()->route('projects.' . $this->_getFileRedirect($type), ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $id]);
    }

    /**
     * Delete project files
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteFile($site_slug, $project_set_slug, $id)
    {
      $file = File::find($id);      

      $project_id = $file->fileable->id;
      $file_type = $file->file_type_id;
      $redirect_part = $file->application ? 'admin.edit' : $this->_getFileRedirect($file_type);
      $file->delete();

      return redirect()->route('projects.' . $redirect_part, ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $project_id]);
    }

    // move file to another project
    public function moveFile(Request $request, $site_slug, $project_set_slug, $id)
    {
        $file = File::find($id);
        $project_id = $request->input('project_id');
        $file_type = $file->file_type_id;
        moveAttachment($file, 'fileable_id', $project_id);
        return redirect()->route('projects.' . $this->_getFileRedirect($file_type), ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $project_id]);
    }

    // helper function to get file operation redirects
    private function _getFileRedirect($type)
    {
      $redirect = '';

      if ($type == 1) {
        $redirect = 'budgets';
      }
      else if ($type == 2) {
        $redirect = 'revisions';
      }
      else {
        $redirect = 'notifications';
      }

      return $redirect;
    }
    
     /**
     * Show the list of project questions & answers
     *
     * @return \Illuminate\Http\Response
     */
    public function questions($site_slug, $project_set_slug, $id)
    {
      $project = Project::find($id);

      return view('project.questions', ['project' => $project]);
    }

    /**
     * Show the list of project comments
     *
     * @return \Illuminate\Http\Response
     */
    public function comments($site_slug, $project_set_slug, $id)
    {
      $project = Project::find($id);
      $netid = session('webauth.net_id');
      $comments = $project->comments->where('parent_id', 0);
      $subscribed = $project->commentSubscriptions->where('netid', $netid)->count() > 0;

      return view('project.comments', ['project' => $project, 'comments' => $comments, 'subscribed' => $subscribed]);
    }

    /**
     * Save project comments
     *
     * @return \Illuminate\Http\Response
     */
    public function saveComment(Request $request, $site_slug, $project_set_slug, $id)
    {
      $this->validate($request, 
        ['comment_text.*' => 'required|maxwordcount:1500'],
        [],
        ['comment_text.*' => 'Comment']
      );

      $project = Project::find($id);
      $comment = new Comment();

      $comment->netid = $request->session()->get('webauth.net_id');
      $comment->parent_id = $request->input('parent_id');
      $comment->comment_text = $request->input('comment_text.' . $comment->parent_id);
      $project->comments()->save($comment);

      $user_subscriptions = $project->commentSubscriptions->where('netid', $comment->netid);

      // save comment subscription
      if ($request->has('subscribe.' . $comment->parent_id)) {
        if ($user_subscriptions->count() == 0) {
          $subscription = new CommentSubscription();
          $subscription->netid = $comment->netid;
          $project->commentSubscriptions()->save($subscription);
        }
      }
      // delete the subscription
      else {
        if ($user_subscriptions->count() > 0) {
          $user_subscriptions->first()->delete();
        }
      }

      // send notifications if not PM
      if(explode('@',$project->email)[0] != $project->netid){
        Mail::to($project->email)->send(new CommentNotification($project));
      }
    
      // refresh subscriptions before sending
      $project->load('commentSubscriptions');  
      foreach ($project->commentSubscriptions as $subscription) {
        // only send notification to subscribed users who are not the project manager
        if($subscription->netid != $project->netid){
          Mail::to(getNetidEmail($subscription->netid))->queue(new CommentNotification($project, $subscription->netid));
        }
      }

      return redirect()->route('projects.comments', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $id]);
    }

    /**
     * Show the list of unlocked applications
     *
     * @return \Illuminate\Http\Response
     */
    public function createStart(Request $request, $site_slug, $project_set_slug)
    {
      $netid = $request->session()->get('webauth.net_id');
      $user_projects = projectSet()->projects->where('netid', $netid)->where('locked', 0);

      if (fundingAppClosed(true)) {
        return deadlineRedirect($site_slug, projectSet()->application_open);
      }

      if ($user_projects->count() > 0)
      {
        $params = $this->_index();

        return view('project.index',
        [
          'funding_statuses' => $params['funding_statuses'],
          'years' => $params['years'],
          'apply' => true,
          'netid' => $netid,
          'projects' => $user_projects,
          'authorized' => false,
          'sent' => false,
        ]);
      }
      else
      {
        return redirect()->route('projects.create', ['site' => $site_slug, 'project_set' => $project_set_slug]);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($site_slug, $project_set_slug)
    {
      $params = $this->_create();

      if (fundingAppClosed(true)) {
        return deadlineRedirect($site_slug, projectSet()->application_open);
      }

      return view('project.create',
      [
        'departments' => $params['departments'],
        'undergraduate_initiatives' => $params['undergraduate_initiatives'],
        'graduate_initiatives' => $params['graduate_initiatives'],
        'template_url' => $params['template_url'],
        'admin_edit' => false,
        'creator_access' => true,
        'authorized' => false,
      ]);
    }

    // initial parameters for create/edit form
    private function _create()
    {
      $params = array();
      $params['departments'] = Department::all();
      $params['template_url'] = projectSet()->budget_template_url;

      $initiatives = site()->initiatives;
      $params['undergraduate_initiatives'] = $initiatives->filter(function($initiative) {
        return $initiative->undergraduate_order > 0;
      })->sortBy('undergraduate_order');
      $params['graduate_initiatives'] = $initiatives->filter(function($initiative) {
        return $initiative->graduate_order > 0;
      })->sortBy('graduate_order');

      return $params;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $site_slug, $project_set_slug)
    {
      $site = Site::where('slug', $site_slug)->first();
      $save_draft = $request->input('save_draft');

      $this->validate($request, $this->rules($site, $save_draft), $this->messages());

      $input = $request->all();
      $project = new Project($input);

      $project->projectSet()->associate(projectSet());
      $project->netid = $request->session()->get('webauth.net_id');
      $project->funding_status_id = 1; // pending review
      $project->setStartYear();
      
      $this->saveProject($project, $site_slug, $save_draft, $request);

      if ($save_draft)
      {
        return redirect()->route('projects.start', ['site' => $site_slug, 'project_set' => $project_set_slug]);
      }

      return redirect()->route('projects.apply.confirm', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $project->id]);
    }

    /**
     * Save and lock the project
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request, $site_slug, $project_set_slug)
    {
      $project = Project::find($request->input('id'));

      $project->setStartYear();
      $project->locked = true; // lock the application
      $project->final_submit_time = date('Y-m-d H:i:s');
      $project->save();

      Mail::to($project->email)->send(new ProjectSubmitted($project, 5));

      return redirect()->route('projects.show', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $project->id])->with('success', true);
    }

    /**
     * Approve the project
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $site_slug, $project_set_slug)
    {
      $project = Project::find($request->input('id'));

      $project->signoff = true;
      $project->save();

      Mail::to($project->email, $project->director_email)->send(new ProjectSubmitted($project, 4));

      return redirect()->route('projects.show', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $project->id]);
    }

    // send a reminder to the project director to approve project
    public function remind($site_slug, $project_set_slug, $id)
    {
      $project = Project::find($id);

      $project->fiscal_email_sent = true;
      $project->save();

      Mail::to($project->fiscal_email)->send(new ProjectSubmitted($project, 3));

      return redirect()->route('projects.show', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $project->id])->with('sentmessage', 'Your reminder message has been sent!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $site_slug, $project_set_slug, $id, $mode = 0)
    {
      $netid = $request->session()->get('webauth.net_id');
      $project = Project::find($id);
      $day_format = 'Y-m-d';

      if ($mode > 0) {
        return view('project.view',
        [
          'project' => $project,
          'confirm' => $mode == 2 && !$project->locked,
          'approve' => $mode == 3 && !$project->signoff && $netid == $project->directorNetid,
        ]);
      }
      else {
        return view('project.summary', ['project' => $project]);
      }
    }

    // alias methods for show to allow for different view modes
    // mode 0: show summary
    // mode 1: show full applications
    public function full(Request $request, $site_slug, $project_set_slug, $id) {
      return $this->show($request, $site_slug, $project_set_slug, $id, 1);
    }
    // mode 2: show confirmation/review screen for applicants
    public function confirm(Request $request, $site_slug, $project_set_slug, $id) {
      return $this->show($request, $site_slug, $project_set_slug, $id, 2);
    }
    // mode 3: show approval/review screen for directors
    public function showApprove(Request $request, $site_slug, $project_set_slug, $id) {
      return $this->show($request, $site_slug, $project_set_slug, $id, 3);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $site_slug, $project_set_slug, $id, $admin_edit = false)
    {
      $params = $this->_create($site_slug, $project_set_slug);
      $project = Project::find($id);
      
      $creator_access = $project->netid == $request->session()->get('webauth.net_id') && !$project->locked;

      // make sure user is either >= chair or creator
      if (!userIsChair() && !$creator_access) {
        return redirectBackError('You are not authorized to edit that project.');
      }

      // get history for project and funding
      $project_history = $project->revisionHistory;
      foreach ($project->allFundings as $funding) {
        if (isset($funding->revisionHistory)) {
          $project_history = $project_history->merge($funding->revisionHistory);
        }
      }
      
      // group history by date and then move comments and author netid out of change list
      $history_grouped = $project_history->sortBy('created_at')->groupBy(function ($item, $key) {
          return date(baseFormat(), strtotime($item['created_at']));
      });

      $history_final = array();

      foreach ($history_grouped as $key => $history_group)
      {
        foreach ($history_group as $history)
        {
          $history_final[$key]['netid'] = isset($history->userResponsible()->netid) ? $history->userResponsible()->netid : '';

          if ($history->fieldName() == 'board_comments')
          {
            $history_final[$key]['comment'] = $history->newValue();
          }
          else
          {
            $revision_year = '';
            if ($history->revisionable_type == 'App\Funding') {
              $funding = Funding::find($history->revisionable_id);
              $revision_year = ' for ' . ($funding->year + $project->start_year);
            }
            $history_final[$key]['changes'][] = 
              ucwords(str_replace("_", " ", $history->fieldName())) .
              ' was changed from ' .
              ($history->oldValue() !== null ? $history->oldValue() : 'nothing') .
              ' to ' .
              ($history->newValue() !== null ? $history->newValue() : 'nothing') .
              $revision_year;
          }
        }
      }

      return view('project.create',
      [
        'departments' => $params['departments'],
        'undergraduate_initiatives' => $params['undergraduate_initiatives'],
        'graduate_initiatives' => $params['graduate_initiatives'],
        'template_url' => $params['template_url'],
        'project' => $project,
        'object' => $project,
        'admin_edit' => $admin_edit,
        'creator_access' => $creator_access,
        'history' => $history_final,
      ]);
    }

    // alias method for admin editing
    public function adminEdit(Request $request, $site_slug, $project_set_slug, $id) {
      return $this->edit($request, $site_slug, $project_set_slug, $id, true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $site_slug, $project_set_slug, $id)
    {
      $site = Site::where('slug', $site_slug)->first();
      $project = Project::find($id);
      $save_draft = $request->input('save_draft');
      $admin_edit = $request->input('admin_edit');
      $fiscal_override = $request->input('fiscal_override');

      $requested_amount = $request->input('requested_amount');
      $board_funded = $request->input('board_amount');
      $final_budget = $request->input('final_amount');
      
      $this->validate($request, $this->rules($site, $save_draft, $admin_edit, $project, $fiscal_override), $this->messages());
      $project->update($request->all());

      $project->updateFunding($requested_amount, 0);
      $project->updateFunding($board_funded, 1);
      $project->updateFunding($final_budget, 2);

      $this->saveProject($project, $site_slug, $save_draft, $request);

      if ($admin_edit)
      {
        return redirect()->route('projects.admin.index', ['site' => $site_slug, 'project_set' => $project_set_slug]);
      }
      else if ($save_draft)
      {
        return redirect()->route('projects.start', ['site' => $site_slug, 'project_set' => $project_set_slug]);
      }

      return redirect()->route('projects.apply.confirm', ['site' => $site_slug, 'project_set' => $project_set_slug, 'id' => $project->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($site_slug, $project_set_slug, $id)
    {
      $project = Project::find($id);
      $project_name = $project->project_name;
      $project->delete();

      return redirect()->route('projects.admin.index', ['site' => $site_slug, 'project_set' => $project_set_slug])->with('sentmessage', $project_name . ' has been deleted.');
    }

    /**
    * validation rules for store and update
    *
    * @param bool $save_draft - if saving for later, fields are not required
    * @return array $rules
    */
    private function rules($site, $save_draft, $admin_edit = false, $project = NULL, $fiscal_override = false)
    {
      $save_draft = $admin_edit ? true : $save_draft;
      $required = $save_draft ? '' : 'required';
      $required_and = $save_draft ? 'nullable|' : 'required|';

      // for updates, make sure the project doesn't already have a budget & description attached
      $required_budget = $required_desc = $required_and;
      if (isset($project))
      {
        if ($project->files->where('file_type_id', 1)->count() > 0) $required_budget = 'nullable|';
        if ($project->files->where('file_type_id', 2)->count() > 0) $required_desc = 'nullable|';
      }

      $required_fiscal = $fiscal_override ? '' : $required;
      $required_and_fiscal = $fiscal_override ? 'nullable|' : $required_and;

      $rules = [
        'first_name' => $required,
        'last_name' => $required,
        'email' => $required_and . 'email',
        'director_first_name' => $required,
        'director_last_name' => $required,
        'director_email' => $required_and . 'email|different:email',
        'fiscal_first_name' => $required_fiscal,
        'fiscal_last_name' => $required_fiscal,
        'fiscal_email' => $required_and_fiscal . 'email|different:email',
        'project_name' => $required,
        'amounts.1' => $required_and . 'numeric',
        'amounts.2' => $save_draft ? '' : 'required_if:duration,2|required_if:duration,3',
        'amounts.3' => $save_draft ? '' : 'required_if:duration,3',
        'new_project' => $required,
        'past_funding' => $save_draft ? '' : 'required_if:new_project,0',
        'duration' => $required,
        'justification' => $save_draft ? '' : 'required_if:duration,2|required_if:duration,3|min:25',
        'sole_funding' => $required,
        'budget' => $required_budget . 'file|mimes:xlsx|max:1500',
        'description' => $required_desc . 'file|mimes:pdf|max:1500',
        'additional_information' => 'file|mimes:pdf|max:1500',
        'agree_terms' => $required,
        'board_comments' => $admin_edit ? 'required' : '',
      ];

      // require department only if the site has departments
      if ($site->departments->count() > 0)
      {
        $rules['department_id'] = $required;
      }
      else 
      {
        $rules['department_other'] = $required;
      }

      return $rules;
    }

    // validation messages
    private function messages()
    {
      $binary_message = 'Please select Yes or No.';

      $messages =
      [
        'required' => 'Please enter the :attribute.',
        'required_if' => 'Please enter a :attribute.',
        'email' => 'Please enter a valid email address.',
        'different' => 'The Project Director and Fiscal Officer must be different from the Project Manager.',
        'first_name.required' => 'Please enter your first name.',
        'last_name.required' => 'Please enter your last name.',
        'email.required' => 'Please enter your email address.',
        'department_id.required' => 'Please select a department.',
        'department_other.required' => 'Please enter a department.',
        'amounts.1.required' => 'Please enter an amount.',
        'amounts.2.required_if' => 'Please enter an amount for Year 2.',
        'amounts.3.required_if' => 'Please enter an amount for Year 3.',
        'new_project.required' => $binary_message,
        'past_funding.required_if' => $binary_message,
        'sole_funding.required' => $binary_message,
        'duration.required' => 'Please select a duration.',
        'budget.required' => 'Please upload a budget.',
        'description.required' => 'Please upload a project description.',
        'agree_terms.required' => 'You must agree to the terms to submit an application.',
        'board_comments.required' => 'Please enter a justification for your changes.',
      ];

      return $messages;
    }

    /**
    * shared function to save the project
    *
    * @param App\Project $project - the new or updated project to be saved
    * @param string $site_slug
    * @param bool $save_draft
    * @param \Illuminate\Http\Request  $request
    */
    private function saveProject($project, $site_slug, $save_draft, $request)
    {
      $file_budget = $request->file('budget');
      $file_description = $request->file('description');
      $file_extra = $request->file('additional_information');
      $undergraduate_initiatives = $request->input('undergraduate_initiatives');
      $graduate_initiatives = $request->input('graduate_initiatives');
      $amounts = $request->input('amounts');

      $project->past_funding = $project->new_project ? 0 : $project->past_funding;
      $project->yearly_variation = $project->duration == 1 ? 0 : $project->yearly_variation;

      $project->save();

      // send email notifications if they haven't been sent already
      if (isset($project->first_name, $project->last_name, $project->email)) {
        if (!$project->email_sent && isset($project->email)) {
          Mail::to($project->email)->send(new ProjectSubmitted($project, 1));
          $project->email_sent = true;
        }
        if (!$project->director_email_sent && isset($project->director_email)) {
          Mail::to($project->director_email)->send(new ProjectSubmitted($project, 2));
          $project->director_email_sent = true;
        }
        if (!$project->fiscal_email_sent && isset($project->fiscal_email)) {
          Mail::to($project->fiscal_email)->send(new ProjectSubmitted($project, 3));
          $project->fiscal_email_sent = true;
        }
      }

      // save project again in case emails were sent
      $project->save();

      $project->updateFunding($amounts, 0);
      $project->attachFile($file_budget, 1, true);
      $project->attachFile($file_description, 2, true);
      $project->attachFile($file_extra, 3, true);
      $project->detachInitiatives($undergraduate_initiatives);
      $project->detachInitiatives($graduate_initiatives);
      $project->attachInitiatives($undergraduate_initiatives, false);
      $project->attachInitiatives($graduate_initiatives, true);
    }
}
