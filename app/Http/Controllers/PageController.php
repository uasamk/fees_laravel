<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Site;
use App\Page;
use App\File;

class PageController extends Controller
{
    public $pages_app;

    public function __construct()
    {
        $this->middleware('webauth', ['except' => ['show', 'showBySlug']]);
        $this->middleware('auth.site', ['except' => ['show', 'showBySlug']]);
        $this->middleware('power', ['except' => ['show', 'showBySlug']]);

        if (null !== site()) {
            // get urls for common built in pages
            $this->pages_app = [
                [route('projects.start', baseRouteParams()), 'Apply for Funding'],
                [route('projects.index', baseRouteParams()), 'Proposals and Projects'],
                [route('progress.start', baseRouteParams()), 'Submit Progress Reports'],
                [route('pars.start', baseRouteParams()), 'Submit PARs'],
                [route('admin', ['site' => site()->slug]), 'Board Room'],
            ];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($site_slug)
    {
        $site = Site::where('slug', $site_slug)->first();
        $files = site()->files()->where('file_type_id', 8)->orderBy('created_at', 'desc')->paginate(10);

        return view('page.index', ['files' => $files]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.create', ['pages_app' => $this->pages_app]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $site_slug)
    {
        $this->validate($request, $this->rules($request), $this->messages());

        $page = new Page();
        $site = Site::where('slug', $site_slug)->first();

        $this->_clearBoardHome($request);

        $page->site_id = $site->id;
        $page->default = false; // TODO: allow user to change default (home) page?
        $page->list_order = 0;
        $page->title = $request->input('title');
        $page->slug = str_slug($page->title);
        $page->published = $request->has('published');
        $page->parent_id = $request->has('parent_id') ? $request->input('parent_id') : 0;
        $page->body = clean($request->input('body'));
        $page->info = $request->input('info');
        $page->board_home = $request->has('board_home');

        $page->save();

        return redirect()->route('pages.index', ['site' => $site_slug]);
    }

    // clear old board home pages
    private function _clearBoardHome($request) {
        // only clear if attempting to set this page to board home
        if ($request->has('board_home')) {
            $board_home_pages = site()->pages->where('board_home', 1);

            foreach($board_home_pages as $old_page) {
                $old_page->board_home = false;
                $old_page->save();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $site_slug
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($site_slug, $id)
    {
        $page = Page::find($id);

        return redirect($page->url);
    }

    // get a page by slug
    public function showBySlug($site_slug, $page_slugs)
    {
        //$site = Site::where('slug', $site_slug)->first();
        $slugs = explode('/', $page_slugs);
        $page = site()->pages()->where('slug', $slugs[count($slugs)-1])->first();

        if (isset($page))
        {
          return view('page.view', ['page' => $page]);
        }
        else
        {
          abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($site_slug, $id)
    {
        $page = Page::find($id);

        return view('page.create', ['page' => $page, 'pages_app' => $this->pages_app]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $site_slug, $id)
    {
        $this->validate($request, $this->rules($request), $this->messages());

        $page = Page::find($id);

        $this->_clearBoardHome($request);

        $page->title = $request->input('title');
        $page->slug = str_slug($page->title);
        $page->published = $request->has('published');
        $page->parent_id = $request->has('parent_id') ? $request->input('parent_id') : 0;
        $page->body = clean($request->input('body'));
        $page->info = $request->input('info');
        $page->board_home = $request->has('board_home');

        $page->save();

        return redirect()->route('pages.index', ['site' => $site_slug]);
    }

    // update order of pages
    public function updateOrder(Request $request, $site_slug)
    {
        $list_orders = $request->input('list_order');
        foreach ($list_orders as $key => $list_order) {
            if (ctype_digit($list_order) && $list_order >= 0) {
                $page = Page::find($key);
                // default (home) stays first
                if (!$page->default) {
                    $page->list_order = $list_order;
                    $page->save();
                }
            }
        }

        return redirect()->route('pages.index', ['site' => $site_slug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($site_slug, $id)
    {
        $page = Page::find($id);
        $page->delete();

        return redirect()->route('pages.index', ['site' => $site_slug]);
    }

    // validation rules
    private function rules($request, $new = true) {
        $parent_id = '';
        if ($request->has('board_home')) {
            $parent_id = [
                Rule::in([0]),
            ];
        } 
        return [
            'title' => 'required',
            'body' => 'required',
            'parent_id' => $parent_id,
        ];
    }

    private function messages() {
        return [
            'parent_id.in' => 'You can not set a parent when \'Board Home Page\' is checked.',
        ];
    }
}
