<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Question;
use App\Project;
use App\Par;
use App\Mail\QuestionNotification;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($site_slug, Request $request)
    {
        $this->validate($request, [
            'question' => 'required|min:25|max:1500',
        ]);

        $type = $request->input('item_type');
        $id = $request->input('item_id');
        $question_text = $request->input('question');

        $question = new Question();
        $question->netid = $request->session()->get('webauth.net_id');
        $question->question_text = $question_text;

        // par
        if ($type == 2) {
            $par = Par::find($id);         
            $this->_storeFinal($question, $par->questions(), $type, $par->project);

            return redirect()->route('pars.admin.index', ['site' => $site_slug, 'project_set' => $par->project->projectSet->slug])->with('sentmessage', 'Your question has been sent.');
        }
        // default to project
        else {
            $project = Project::find($id);         
            $this->_storeFinal($question, $project->questions(), $type, $project);

            return redirect()->route('projects.admin.index', ['site' => $site_slug, 'project_set' => $project->projectSet->slug])->with('sentmessage', 'Your question has been sent.');
        }
    }

    private function _storeFinal($question, $question_collection, $type, $project) {
        $today = new \DateTime('now');
        $deadline = $today->add(new \DateInterval('P' . $project->projectSet->question_deadline . 'D'));
        $question->deadline = $deadline->format(dbFormat());
        $question_collection->save($question);

        Mail::to($project->email)->send(new QuestionNotification($type, $project, $question));
    }

    public function questionExtension(Request $request, $site_slug){
        $this->validate($request, [
            'days' => 'required|min:1|max:1500',
        ]);
        
        // Get question info
        $q_id = $request->input('q_id');
        $days = $request->input('days');
        $id = $request->input('item_id');

        // Find question
        $question = Question::find($q_id); 
        
        // Get deadline
        $deadline = new \DateTime($question->deadline);

        // Get new deadline
        $today = new \DateTime('now');
        
        // Set new deadline
        //  if deadline expired it set extension from today
        //  if deadline not expired it adds days onto its deadline
        if($deadline < $today){
            $extended_deadline = $today->add(new \DateInterval('P' . $days . 'D'));
        }else{
            $extended_deadline = $deadline->add(new \DateInterval('P'.$days.'D'));
        }
        
        // update Question deadline
        $question->deadline = $extended_deadline->format(dbFormat());

        // save
        $project = Project::find($id); 
        $project->questions()->save($question);
        
        return redirect()->route('projects.questions', routeParams(['id' => $project->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
