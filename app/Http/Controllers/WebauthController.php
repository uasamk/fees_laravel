<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

use App\User;

class WebAuthController extends Controller
{
  // handle redirects back from webauth
  public function redirect($site_slug, Request $request)
  {
      //setup curl to webauth
      $curlHandle = curl_init();
      $options = [
          CURLOPT_HEADER => false,
          CURLOPT_URL => 'https://webauth.arizona.edu/webauth/serviceValidate?ticket=' . $request->input('ticket') . '&service=' . route('guest.login.redirect', ['site' => $site_slug]),
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_FORBID_REUSE => true,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_TIMEOUT => 10,
      ];
      curl_setopt_array($curlHandle, $options);
      $temp = curl_exec($curlHandle);
      //create xml parser
      $xml_parser = xml_parser_create();
      //parse xml into an array of values and an array of indexes
      xml_parse_into_struct($xml_parser, $temp, $struct, $index);
      //check if ticket was authenticated successfully
      if (isset($index['CAS:AUTHENTICATIONSUCCESS'])) {
          $netid = $struct[$index['CAS:USER'][0]]['value'];
          $request->session()->put('webauth', [
            'net_id' => $netid,
            'emplid' => $struct[$index['CAS:EMPLID'][0]]['value']
          ]);

          // attempt to authenticate user
          $user = User::where('netid', $netid)->first();
          if (isset($user)) {
            Auth::login($user);
          }

          // check if session has intended url, otherwise redirect home
          if ($request->session()->has('url.intended')) {
            // flash webauth flag to session in case the next stop is auth
            return redirect($request->session()->get('url.intended'))->with('webauth_handoff', 1);
          }
      }

      return redirect(route('index', ['site' => $site_slug]));
  }
}
