<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Project;
use App\ProjectSet;
use App\File;
use App\Site;

// get the current fiscal year
function currentFiscalYear() {
    return getFiscalYear(strtotime('now'));
}

// get fiscal year from timestamp
function getFiscalYear($timestamp) {
    $year = date('Y', $timestamp);
    $month = date('n', $timestamp);

    // if it's after july, the fiscal year is 1 "ahead" of current year
    return $month > 7 ? $year + 1 : $year;
}

// get active user projects
function getActiveProjects() {
    $netid = session('webauth.net_id');
    $user_projects = Project::where(['project_set_id' => projectSet()->id, 'archived' => 0, 'locked' => 1])
        ->whereIn('funding_status_id', [2, 3, 4])
        ->orderBy('start_year', 'project_name')
        ->get();

    // filter to netid of manager or director
    return $user_projects->filter( function($project) use($netid) {
        return $project->netid == $netid || $project->director_netid == $netid;
    });
}

// attach file to progress report/par
function attachFile($site_slug, $obj, $file_input, $type, $keep_file_name = false, $new_override = false) {
    if (isset($file_input)) {
        $file = new File();
        $new = true;

        $obj_files = $obj->files->filter( function($file) use ($type) {
            return $file->file_type_id == $type;
        });
        if ($obj_files->count() > 0 && !$new_override) {
          $file = $obj_files->first();
          $new = false;
          // delete old file
          Storage::delete($file->name);
        }

        $file->title = $keep_file_name ? $file_input->getClientOriginalName() : '';
        $file->name = $file_input->store($site_slug);

        if (!$new) {
            $file->save();
        }
        else {
            $file->file_type_id = $type;
            $file->application = false;
            
            $obj->files()->save($file);
        }
    }
}

function dbFormat() {
    return 'Y-m-d H:i:s';
}

function baseFormat() {
    return 'F j, Y g:i a';
}

// standard db date format
function formatDateDb($timestamp) {
    return date(dbFormat(), strtotime($timestamp));   
}

// standard date format for site
function formatDate($timestamp) {
    return date('D, ' . baseFormat(), strtotime($timestamp));   
}

// standard date format for deadlines
function formatDeadline($timestamp) {
    return date('l, F j, Y', strtotime($timestamp));
}

function formatBoolean($bool) {
    return isset($bool) ? ($bool ? 'Yes' : 'No') : 'Pending';
}

// helper function for filter paramaters
function filterParam($param_group, $param, $request) {
    return !empty($request->all()) ? $request->query($param) : $request->session()->get($param_group . $param);
}

// get file url from a collection of files filtered by type
function getFileUrl($files, $type) {
    return $files->where('file_type_id', $type)->count() > 0 ? Storage::url($files->where('file_type_id', $type)->first()->name) : NULL;
}

// get file title from a collection of files filtered by type
function getFileTitle($files, $type) {
    return $files->where('file_type_id', $type)->count() > 0 ? $files->where('file_type_id', $type)->first()->title : NULL;
}

function getNetidEmail($netid) {
    /* TODO: make this database driven */
    return $netid . '@email.arizona.edu';
}

// user authentication and role checks
function userAccessSite() {
    // make sure the user has access to the current site and is not retired or pending
    return Auth::check() && Auth::user()->sites->where('id', site()->id)->count() > 0 && in_array(Auth::user()->user_status_id, [1, 3]);
}
function userIsChair() {
    return userAccessSite() && Auth::user()->role->id > 1;
}

function userIsAdmin() {
    return userAccessSite() && Auth::user()->role->id > 2;
}

function userIsPower() {
    return userAccessSite() && Auth::user()->role->id > 3;
}

// move an attachment to a new project
function moveAttachment($attachment, $property_name, $project_id) {
    $attachment->$property_name = $project_id;
    $attachment->save();
}

// delete items in a collection - call delete on each item individually to cascade into relationships
function deleteItems($collection) {
    $collection->each(function($collection_item) {
        $collection_item->delete();
    });
}

// get current site from url
function site() {
    $request = app(\Illuminate\Http\Request::class);
    $site_slug = $request->route('site');
    return is_numeric($site_slug) ? Site::find($site_slug) : Site::where('slug', $site_slug)->first();
}

// get current project set from url or session
function projectSet() {
    if (site()->projectSets->count() > 0) {
        $request = app(\Illuminate\Http\Request::class);
        $project_set_slug = $request->route('project_set');
        $session_key = site()->slug . '.project_set_slug';
            
        // check if the project set isn't already in the url
        if (!isset($project_set_slug) || is_numeric($project_set_slug)) {
            // if project is in session, make sure it matches db
            if (session()->has($session_key)) {
                if (site()->projectSets->where('slug', session($session_key))->count() == 0) {
                    // if it doesn't match, delete
                    session()->forget($session_key);
                }
            }

            // default to first project set if not already in session
            if (!session()->has($session_key) && null !== site())
            {
                session([$session_key => site()->projectSets->first()->slug]);
            }
            $project_set_slug = session($session_key);
        }

        $project_sets = site()->projectSets->where('slug', $project_set_slug);

        // ensure the project set actually exists in the current site
        if ($project_sets->count() > 0) {
            return $project_sets->first();
        }
    }
        
    return new ProjectSet();
}

// base paramaters for most routes
function baseRouteParams() {
    return ['site' => site()->slug, 'project_set' => projectSet()->slug];
}

// get all route params from an array
function routeParams($params) {
    return array_merge(baseRouteParams(), $params);
}

// check if an application is open
function applicationClosed($open_date, $close_date, $power) {
    return (nowTimestamp() < strtotime($open_date) || nowTimestamp() > strtotime($close_date)) && ($power ? !userIsPower() : true);
}

// check for funding application
function fundingAppClosed($power = false) {
    return applicationClosed(projectSet()->application_open, projectSet()->application_deadline, $power);
}

// check for board application
function boardAppClosed($power = false) {
    return applicationClosed(site()->board_application_open, site()->board_application_deadline, $power);
}

// check back date message partial
function appReturnDate($open_date) {
    return (strtotime($open_date) > nowTimestamp()) ?
        'in ' . Date('F \of Y', strtotime($open_date)) : 'later';
}

// get the current timestamp for the correct time zone
function nowTimestamp() {
    $now = new \DateTime('now', new \DateTimeZone('America/Phoenix'));
    return strtotime($now->format(dbFormat()));
}

// redirect with error for accessing apps with passed deadlines
function deadlineRedirect($site_slug, $open_date) {
    return redirect('/' . $site_slug)->with('error', 'The application is currently closed. Please check back ' . appReturnDate($open_date) . '.');
}

// verbiage for unauthorized access error
function accessError() {
    return 'You are not authorized to access that area. If you believe this is in error, please Contact Us';
}

// redirect to home with error
function redirectHome() {
    return redirect('/' . site()->slug)->with('error', accessError());
}

// attempt to redirect back with error
function redirectBackError($error) {
    if (null !== url()->previous() && url()->previous() != url()->current()) {
        return back()->with('error', $error);
    }
    else if (null !== site()) {
        return redirect()->route('index', ['site' => site()->slug])->with('error', $error);
    }

    return redirect('/');
}