<?php

namespace App\Http\Middleware;

use Closure;

class SiteAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (userAccessSite()) {
            return $next($request);
        }

        return redirectHome();
    }
}
