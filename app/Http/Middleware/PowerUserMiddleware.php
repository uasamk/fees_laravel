<?php

namespace App\Http\Middleware;

use Closure;

class PowerUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (userIsPower()) {
            return $next($request);
        }

        return redirectHome();
    }
}
