<?php

namespace App\Http\Middleware;

use Closure;

class WebauthMiddleware
{
    /**
     * Middleware for webauth - redirect users to webauth if route is within webauth group
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (!$request->session()->has('webauth')) {
          $site_slug = $request->route('site');
          // preserve intended url for redirect after webauth
          // preserve previous url in case going directly to auth after - see UserController@login
          $request->session()->put('url.intended', url()->full());
          $request->session()->put('url.previous', url()->previous());
          $request->session()->save();
          return redirect()->to('https://webauth.arizona.edu/webauth/login?service=' . route('guest.login.redirect', ['site' => $site_slug]));
      }
      return $next($request);
    }
}
