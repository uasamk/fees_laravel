<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'student_id',
        'phone',
        'email',
        'address',
        'city',
        'state',
        'zip',
        'club_org_1',
        'club_org_2',
        'club_org_3',
        'club_org_4',
        'user_title_id',
        'text',
        'user_standing_id',
        'user_type_id',
        'user_status_id',
        'responsibilities',
        'netid',
        'role_id',
        'start_year',
        'fiscal_year',
        'directory',
        'par_contact',
        'question_contact',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // get the role this user has
    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    // get the status of this user
    public function status()
    {
        return $this->belongsTo('App\UserStatus', 'user_status_id');
    }

    // get the standing for this user
    public function standing()
    {
        return $this->belongsTo('App\UserStanding', 'user_standing_id');
    }

    // get the title of this user
    public function title()
    {
        return $this->belongsTo('App\UserTitle', 'user_title_id');
    }

    // get the type of this user
    public function type()
    {
        return $this->belongsTo('App\UserType', 'user_type_id');
    }

    // get the applications belonging to this user
    public function applications()
    {
        return $this->hasMany('App\UserApplication');
    }

    // get the sites associated with this user
    public function sites()
    {
        return $this->belongsToMany('App\Site');
    }

    // check if the user is an admin
    public function isAdmin()
    {
        return $this->role->id == 4;
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    // clean up relations before deleting
    public function delete()
    {
        $this->applications()->delete();
        Parent::delete();
    }
}
