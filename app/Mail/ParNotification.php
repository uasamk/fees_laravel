<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Par;

class ParNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $message_type;
    public $par;
    public $site;
    public $admin_url;
    public $par_url;
    public $projects_url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Par $par, int $message_type)
    {
        $project = $par->project;

        $this->message_type = $message_type;
        $this->par = $par;
        $this->site = $par->project->projectSet->site;
        $this->admin_url = route('admin', ['site' => $this->site->slug]);
        $this->par_url = route('pars.index', ['site' => $this->site->slug, 'project_set' => $project->projectSet->slug, 'project' => $project->id]);
        $this->projects_url = route('projects.index', ['site' => $this->site->slug, 'project_set' => $project->projectSet->slug]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->message_type < 3 ? 'Submitted' : $this->par->board_id . ' Update';
        return $this->subject($this->site->name . ': PAR ' . $subject)
            ->view('email.par');
    }
}
