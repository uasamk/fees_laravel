<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Project;

class ProgressSubmitted extends Mailable
{
    use Queueable, SerializesModels;

    public $project;
    public $message_type;
    public $comments;
    public $site;
    public $tries = 1; // # of attempts to process job in queue

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Project $project, int $message_type, $comments)
    {
        $this->project = $project;
        $this->message_type = $message_type;
        $this->comments = $comments;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->project->projectSet->site->name . ': Progress Report Request')
            ->view('email.progress');
    }
}
