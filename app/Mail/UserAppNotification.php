<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\UserApplication;

class UserAppNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $application;
    public $message_type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserApplication $application, $message_type)
    {
        $this->application = $application;
        $this->message_type = $message_type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(site()->name . ': Board Application')
            ->view('email.user-app');
    }
}
