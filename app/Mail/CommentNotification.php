<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $project;
    public $current_site;
    public $netid;
    public $subscribed;
    public $tries = 1; // # of attempts to process job in queue

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($project, $netid = null)
    {
        $this->project = $project;
        $this->current_site = $project->projectSet->site;
        $this->netid = $netid;
        $this->subscribed = isset($netid);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->current_site->name . ': New Comment On ' . ($this->subscribed ? 'A Project You Subscribed To' : 'Your Project'))
            ->view('email.comment');
    }
}
