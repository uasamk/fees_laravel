<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuestionNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $title;
    public $project;
    public $site;
    public $question;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($type, $project, $question)
    {
        $this->title = $type == 2 ? 'PAR' : 'Project';
        $this->project = $project;
        $this->site = $project->projectSet->site;
        $this->question = $question;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->site->name . ': Question About Your ' . $this->title)
            ->view('email.question');
    }
}
