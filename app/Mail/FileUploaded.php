<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Project;

class FileUploaded extends Mailable
{
    use Queueable, SerializesModels;

    public $project;
    public $type;
    public $instructions;
    public $contacts;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Project $project, int $message_type, bool $instructions)
    {
        $this->project = $project;
        $this->instructions = $instructions;
        $this->contacts = array($project->first_name);
        
        if (!empty($project->director_first_name) && $project->director_first_name != $project->first_name) {
            $this->contacts[] = $project->director_first_name;
        }
        if (!empty($project->fiscal_first_name) && $project->firscal_first_name != $project->first_name) {
            $this->contacts[] = $project->fiscal_first_name;
        }

        if ($message_type == 1) {
            $this->type = 'budget';
        }
        else if ($message_type == 2) {
            $this->type = 'revised application';
        }
        else {
            $this->type = 'funding notification';
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->project->projectSet->site->name . ': File Upload Notification')
            ->view('email.file');
    }
}
