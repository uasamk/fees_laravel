<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Project;

class ProjectSubmitted extends Mailable
{
    use Queueable, SerializesModels;

    public $project;
    public $message_type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Project $project, int $message_type)
    {
        $this->project = $project;
        $this->message_type = $message_type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->project->projectSet->site->name . ': Funding Application')
            ->view('email.project');
    }
}
