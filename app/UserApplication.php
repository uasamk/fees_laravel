<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserApplication extends Model
{
    protected $fillable = 
    [
        'major',
        'minor',
        'gpa',
        'gpa_cumulative',
        'credits_fall',
        'credits_spring',
        'current_jobs',
        'job_hours',
        'job_description',
        'board_interest',
        'board_activities',
        'board_other',
        'agree_terms',
        'accepted',
        'reject_email',
        'review_comments',
    ];

    // get the user this application belongs to
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // get the references for this application
    public function references()
    {
        return $this->hasMany('App\Reference');
    }

    // get the files for this site
    public function files()
    {
        return $this->morphMany('App\File', 'fileable');
    }

    // full url for resume
    public function getFileResumeUrlAttribute()
    {
      return getFileUrl($this->files, 9);
    }
}
