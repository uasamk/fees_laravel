<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    // get the project this comment belongs to
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    // get the parent of this comment
    public function parent()
    {
        return $this->belongsTo('App\Comment', 'parent_id');
    }

    // get the children of this comment
    public function children()
    {
        return $this->hasMany('App\Comment', 'parent_id');
    }
}
