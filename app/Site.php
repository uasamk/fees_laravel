<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Site extends Model
{
    // get the pages that belong to this site
    public function pages()
    {
        return $this->hasMany('App\Page');
    }

    // get the Project Sets that belong to this site
    public function projectSets()
    {
      return $this->hasMany('App\ProjectSet');
    }

    // get the departments associated with this site
    public function departments()
    {
      return $this->hasMany('App\Department')->orderBy('name');
    }

    // get the initiatives associated with this site
    public function initiatives()
    {
      return $this->hasMany('App\Initiative');
    }

    // get the contact for this site
    public function contact()
    {
      return $this->belongsTo('App\User', 'contact_id');
    }

    // get the users that are associated with this site
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    // get the files for this site
    public function files()
    {
        return $this->morphMany('App\File', 'fileable');
    }

    // get uploads for the site
    public function getUploadsAttribute()
    {
        return $this->files->where('file_type_id', 8);
    }

    // get the url for the site
    public function getUrlAttribute()
    {
      return route('index', ['site' => $this->slug]);
    }

    // get the governing doc url
    public function getGoverningDocUrlAttribute()
    {
      return getFileUrl($this->files, 7);
    }

    public function getGoverningDocTitleAttribute()
    {
      return getFileTitle($this->files, 7);
    }

    // get the governing doc url
    public function getBoardRequirementsUrlAttribute()
    {
      return getFileUrl($this->files, 3);
    }

    // get the homepage
    public function getHomeAttribute()
    {
      return $this->pages()->where('default', 1)->first();
    }

    public function getBoardAppDeadlineTimeAttribute()
    {
        return date('g:i a', strtotime($this->board_application_deadline));
    }

    public function getBoardAppDeadlineDateAttribute()
    {
        return formatDeadline($this->board_application_deadline);
    }
}
