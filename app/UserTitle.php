<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTitle extends Model
{
    // get the users with this title
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
