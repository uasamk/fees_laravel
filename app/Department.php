<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
  // get the projects with this department
  public function projects()
  {
      return $this->hasMany('App\Project');
  }

  // get the site this department belongs to
  public function site()
  {
      return $this->belongsTo('App\Site');
  }
}
