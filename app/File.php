<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    // get the model this file belongs to
    public function fileable()
    {
        return $this->morphTo();
    }

    // get the type of file
    public function file_type()
    {
        return $this->belongsTo('App\FileType');
    }

    public function delete()
    {
        // delete the actual file
        Storage::delete($this->name);
        Parent::delete();
    }
}
