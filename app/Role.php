<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // get the users for this role
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
