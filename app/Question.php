<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    // get the model this question belongs to
    public function questionable()
    {
        return $this->morphTo();
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
    
    public function getProjectAttribute()
    {
        return $this->questionable_type == 'App\Par' ? 
            $this->questionable->project : 
            $this->questionable;
    }

    // clean up relationships on delete
    public function delete()
    {
        $this->answers()->delete();
        Parent::delete();
    }
}
