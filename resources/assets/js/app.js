
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

Vue.directive('init', {
  bind: function(el, binding, vnode) {
    vnode.context[binding.arg] = binding.value;
  }
});

const app = new Vue({
    el: '#app',
    data: {
      department: '',
      project_type: '',
      new_project: null,
      duration: -1,
      undergraduate_initiatives: [],
      graduate_initiatives: [],
      user_title_id: '',
      current_jobs: null,
    },
});

// request modal dialog
$('#modalRequestReport').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var project = button.data('project');

  if (project !== undefined) {
    $(this).find('.modal-body .project').text(project);
    $(this).find('.modal-body .name').text(button.data('name'));
    $(this).find('.modal-body .director').text(button.data('director'));
    $(this).find('.modal-body .fiscal').text(button.data('fiscal'));
    $(this).find('.modal-footer .project-id').val(button.data('id'));
    $(this).find('.multiple').hide();
    $(this).find('.single').show();
  }
  else {
    $(this).find('.single').hide();
    $(this).find('.multiple').show();
  }
});

// question dialog
$('#modalQuestion').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var title = button.data('title');

  if (title !== undefined) {
    $(this).find('.modal-body .title').text(title);
    $(this).find('.modal-body .project').text(button.data('project'));
    $(this).find('.modal-footer .item-type').val(button.data('item-type'));
    $(this).find('.modal-footer .item-id').val(button.data('item-id'));
  }
});

// delete/move modal
$('#modalDelete, #modalMove').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var title = button.data('title');
  var action = button.data('action');

  if (title !== undefined) {
    $(this).find('.modal-body .title').text(title);
    $(this).find('form').attr('action', action);
  }
});

// initiative modal
$('#modalInitiative').on('show.bs.modal', function(event) {
  var button = $(event.relatedTarget);
  var action = button.data('action');
  var initiative = button.data('initiative');

  if (initiative !== undefined) {
    $(this).find('form').attr('action', action);
    $(this).find('#initName').val(initiative.name);
    $(this).find('#undergradOrder').val(initiative.undergraduate_order);
    $(this).find('#gradOrder').val(initiative.graduate_order);
  }
});

// fix lame accordion since putting anything out of the ordinary between panel group and collapse breaks it
$('.panel-group .collapse').on('show.bs.collapse', function() {
  $('.panel-group .in').collapse('toggle');
});

// datetimepicker stuff
$('.datetimepicker').datetimepicker({
  showClear: true,
  showClose: true,
  allowInputToggle: true,
  focusOnShow: false,
});

window.setDatePickerDate = function(date, datepicker_id) {
  if (date !== undefined) {
    date_obj = new Date(date);
    $(datepicker_id).data('DateTimePicker').date(date_obj);
  }
}

// summernote
var paraButton = function (context) {
  var ui = $.summernote.ui;

  // create button
  var button = ui.button({
    contents: '<i class="glyphicon glyphicon-font"/>',
    tooltip: 'Paragraph',
    click: function () {
      context.invoke('editor.formatPara');
    }
  });

  return button.render();   // return button as jquery object
}

var h3Button = function (context) {
  var ui = $.summernote.ui;

  var button = ui.button({
    contents: '<i class="glyphicon glyphicon-header"/>',
    tooltip: 'Header',
    click: function () {
      context.invoke('editor.formatH3');
    }
  });

  return button.render();
}

var fileLinkButton = function (context) {
  var ui = $.summernote.ui;

  var button = ui.button({
    contents: '<i class="glyphicon glyphicon-file"/>',
    tooltip: 'Insert Link To Page or File',
    click: function () {
      $('#modalFile').modal('toggle');
    }
  });

  return button.render();
}

$('.summernote').summernote({
  toolbar: [
    // [groupName, [list of button]]
    ['paragraph', ['para', 'h3']],
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['insert', ['link', 'hr', 'file']],
  ],

  buttons: {
    para: paraButton,
    h3: h3Button,
    file: fileLinkButton,
  },

  onCreateLink : function(originalLink) {
      return originalLink; // return original link 
  }
});

$('#modalFile').on('show.bs.modal', function() {
  var class_part = $(this).find('.radio-insert:checked').val();
  insertSelectToggle(class_part);
});

$('.insert-item').click(function() {
  var parent = $(this).parents('.modal');
  var class_part = parent.find('.radio-insert:checked').val();
  var selected = parent.find('.' + class_part + '-select option:selected');
  
  if (selected.val() != '0') {
    $('.summernote').summernote('createLink', {
      text: selected.text(),
      url: selected.val(),
      isNewWindow: false
    });

    $(this).parents('.modal').modal('toggle');
  }
});

$('.radio-insert').click(function() {
  var class_part = $(this).val();
  insertSelectToggle(class_part);
});

function insertSelectToggle(class_part) {
  $('.insert-select').hide();
  $('.insert-' + class_part).show();
}
