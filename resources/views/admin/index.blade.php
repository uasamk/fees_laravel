@extends('layouts.app')

@section('breadcrumbs')
  @component('partials.breadcrumbs.base')
    <li class="active">Board Room</li>
  @endcomponent
@endsection

@section('title')
  Board Room
@endsection

@section('content')

  @if (null !== session('applicants'))
    @component('partials.alert.base', ['type' => session('applicants') > 0 ? 'success' : 'warning'])
      @if (session('applicants') > 0)
        <h4>Success</h4>
        <p>Your request has been sent to <strong>{{ session('applicants') }}</strong> applicants.</p>
      @else
        <h4>Warning</h4>
        <p>Your progress report request could not be sent because no applications meet the necessary criteria.</p>
      @endif
    @endcomponent
  @endif

  <div class="row">
    @if (site()->projectSets->count() > 0)
      <div class="col-md-6">
        <h2>Project Tools</h2>
        <h4>General Review &amp; Discussion</h4>
        <ul>
          <li><a href="{{ route('projects.review', baseRouteParams()) }}">Review Applications</a></li>
          <li><a href="{{ route('pars.admin.index', baseRouteParams()) }}">Review PARs</a></li>
        </ul>
        @if(userIsChair())
          <h4>Administrative Tasks</h4>
          <ul>
            <li><a href="{{ route('projects.admin.index', baseRouteParams()) }}">Edit Projects</a></li>
            @if(userIsAdmin())
              <li>
                <a href="#" title="Request Progress Report"
                    data-toggle="modal"
                    data-target="#modalRequestReport">
                    Semi-Annual Progress Report Request
                </a>
              </li>
              @if(userIsPower())
                <li><a href="{{ route('projects.start', baseRouteParams()) }}">View Application</a></li>
              @endif
            @endif
          </ul>
        @endif
      </div>
    @endif
    <div class="col-md-6">
      <h2>Board Membership</h2>
      <ul>
        <li><a href="{{ route('users.apply.index', ['site' => site()->slug]) }}">Review Applications</a></li>
        @if (userIsPower())
          <li><a href="{{ route('users.create', ['site' => site()->slug]) }}">Add a Board Member</a></li>
          <li><a href="{{ route('users.index', ['site' => site()->slug]) }}">Edit Board Members</a></li>
          <li><a href="{{ route('users.apply', ['site' => site()->slug]) }}">View Application</a></li>
        @endif
      </ul>
      @if (userIsPower())
        <h2>Site Maintenance</h2>
        <ul>  
          @if (site()->projectSets->count() > 0)
            <li><a href="{{ route('project-sets.edit', ['slug' => site()->slug, 'project_set' => projectSet()->id]) }}">Update Deadlines &amp; Templates</a></li>
          @endif
          <li><a href="{{ route('sites.edit', ['id' => site()->id])}}">Update Site Settings</a></li>
          <li><a href="{{ route('pages.index', ['site' => site()->slug])}}">Manage Content</a></li>
        </ul>
      @endif
    </div>
  </div>

  @if (site()->projectSets->count() > 0)
    @include('partials.progress.request')
  @endif

@endsection
