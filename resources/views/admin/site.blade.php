@extends('layouts.app')

@section('breadcrumbs')
  @component('partials.breadcrumbs.default')
    <li class="active">Site Settings</li>
  @endcomponent
@endsection

@section('title')
  Site Settings
@endsection

@section('content')
    <p>The form below allows you to edit various settings of the {{ site()->name }} website, such as name, contact information and budget template.</p>
    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('sites.update', ['id' => site()->id]) }}">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Site Settings</h3>
            </div>
            <div class="panel-body">
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'name', 'label' => 'Site Name', 'object' => site()])
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'abbreviation', 'label' => 'Abbreviation', 'object' => site()])
                @component('partials.form.field', ['inline' => true, 'name' => 'contact_id', 'label' => 'Site Contact'])
                    <select name="contact_id" class="form-control">
                        <option value="">Select...</option>
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}"{{ null !== old('contact_id') ? (old('contact_id') == $user->id ? ' selected' : '') : ((null !== site()->contact && site()->contact->id == $user->id) ? ' selected' : '') }}>{{ $user->full_name }} ({{ $user->netid }})</option>
                        @endforeach
                    </select>
                @endcomponent
                @component('partials.form.field', ['inline' => true, 'name' => 'governing_doc', 'label' => 'Governing Document'])
                    <p><a href="{{ site()->governing_doc_url }}">{{ site()->governing_doc_title }}</a></p>
                    <input type="file" name="governing_doc">
                @endcomponent
                @component('partials.form.field', ['inline' => true, 'name' => 'info', 'label' => 'Funding Application Instructions'])
                    <textarea name="funding_app_instructions" class="summernote">
                        {!! null !== old('funding_app_instructions') ? old('funding_app_instructions') : (null !== site()->funding_app_instructions ? site()->funding_app_instructions : '') !!}
                    </textarea>
                @endcomponent
                @component('partials.form.field', ['inline' => true, 'name' => 'info', 'label' => 'Board Application Instructions'])
                    <textarea name="board_app_instructions" class="summernote">
                        {!! null !== old('board_app_instructions') ? old('board_app_instructions') : (null !== site()->board_app_instructions ? site()->board_app_instructions : '') !!}
                    </textarea>
                @endcomponent
            </div>
            <div class="panel-footer text-right">
                <a href="{{ route('admin', ['site' => site()->slug]) }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection