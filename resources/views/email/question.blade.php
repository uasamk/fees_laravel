<p>Dear {{ $project->first_name}},</p>
<p>You have received a question about your {{ $site->name }} {{ $title }} for the '{{ $project->project_name }}' initiative.</p>
<p>******************************** IMPORTANT ********************************</p>
<p>This question WILL EXPIRE {{ formatDate($question->deadline) }}.</p>
<p>***************************************************************************</p>
<p>Please follow the below link and reply to this question BEFORE the expiration date. Failure to do so may result in your application being disqualified.</p>
<p><a href="{{ route('answers.create', ['site' => $site->slug, 'question' => $question->id]) }}">{{ route('answers.create', ['site' => $site->slug, 'question' => $question->id]) }}</a></p>
<p>Thank you!</p>