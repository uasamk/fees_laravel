<p>
    Dear,
    @foreach ($contacts as $contact){{ ($loop->last && !$loop->first ? ' and ' : (!$loop->first ? ', ' : '')) . $contact }}@endforeach     
</p>
<p>A new {{ $type }} has been attached to your {{ site()->name }} "{{ $project->project_name }}" (FY{{ $project->fiscal_year }}) project. To review these files, as well as your project's complete history, please visit <a href="{{ $project->url }}">{{ $project->url }}</a>. To access information about all {{ site()->name }} projects, visit <a href="{{ $project->projectSet->projectsURL }}">{{ $project->projectSet->projectsURL }}</a>.</p>
@if ($instructions)
    <p>{{ $project->first_name }}, as the primary contact for this project, you are responsible for filing progress reports at specific times during the year, as well as maintaining accurate and up-to-date contact information throughout your project\'s funding cycle.</p>
    <p>You will receive email notifications when reports and updates are required. You may also refer to the {{ site()->name }} website (<a href="{{ $project->projectSet->site->url }}">{{ $project->projectSet->site->url }}</a>) for additional information.</p>
@endif
<p>Thank you for your interest in the {{ site()->name }}.</p>