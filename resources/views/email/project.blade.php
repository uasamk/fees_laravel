@if ($message_type === 1)
    <p>Dear {{ $project->first_name}},</p>
    <p>Thank you for beginning your {{ $project->projectSet->site->name }} funding application. You may return to <a href="{{ route('projects.edit', ['site' => $project->projectSet->site->slug, 'project_set' => $project->projectSet->slug, 'id' => $project->id]) }}">{{ route('projects.edit', ['site' => $project->projectSet->site->slug, 'project_set' => $project->projectSet->slug, 'id' => $project->id]) }}</a> to login and make additional edits at any time, up until {{ $project->projectSet->appDeadlineTime }} on {{ $project->projectSet->appDeadlineDate }}. Be sure you have submitted your final application by this time or it will not be considered for funding.</p>
    <p>Thank you for your interest in the {{ $project->projectSet->site->name }}.</p>
@elseif ($message_type === 2)
    <p>Dear {{ $project->director_first_name }},</p>
    <p>You have been listed as the director on {{ $project->first_name }} {{ $project->last_name }}'s {{ $project->projectSet->site->name }} funding proposal for the {{ $project->project_name }} initiative. You must approve this proposal by {{ $project->projectSet->appDeadlineTime }} on {{ $project->projectSet->appDeadlineDate }}, if you want it to be considered for funding.
    <p>You may review and approve the proposal here:<br><a href="{{ route('projects.apply.approve', ['site' => $project->projectSet->site->slug, 'project_set' => $project->projectSet->slug, 'id' => $project->id]) }}">{{ route('projects.apply.approve', ['site' => $project->projectSet->site->slug, 'project_set' => $project->projectSet->slug, 'id' => $project->id]) }}</a></p>
    <p>If you need further information or clarification about this proposal, please contact {{ $project->first_name }} {{ $project->last_name }} (<a href="mailto:{{ $project->email }}">{{ $project->email }}</a>) and review the proposal with him/her BEFORE approving.</p>
    <p>Once approved, you and the applicant will receive an email confirming your approval.</p>
    <p>Thank you for your interest in the {{ $project->projectSet->site->name }}.</p>
@elseif ($message_type === 3)
    <p>Dear {{ $project->fiscal_first_name }},</p>
    <p>You have been listed as the fiscal officer on {{ $project->first_name }} {{ $project->last_name }}'s {{ $project->projectSet->site->name }} funding proposal for the {{ $project->project_name }} initiative. This email is simply to alert you of your status. No further action is required on your part at this time.</p>
    <p>If you have questions about this proposal, please contact {{ $project->first_name }} {{ $project->last_name }} (<a href="mailto:{{ $project->email }}">{{ $project->email }}</a>).</p>
    <p>Thank you for your interest in the {{ $project->projectSet->site->name }}.</p>
@elseif ($message_type === 4)
    <p>Dear  {{ $project->first_name }} and {{ $project->director_first_name }},</p>
    <p>This email confirms {{ $project->director_first_name }}'s approval of the funding application for the {{ $project->project_name }} initiative. If you have not already done so, you (the applicant) must still submit your application in its final form and receive an email confirming your application's final, 'locked' submission status before it will be considered for funding. If you have not already completed this step, please do so as soon as possible.</p>
    <p>Thank you for your interest in the {{ $project->projectSet->site->name }}.</p>
@elseif ($message_type === 5)
    <p>Dear {{ $project->first_name}},</p>
    <p>Thank you for submitting your {{ $project->projectSet->site->name }} funding application. Your application is now closed and locked. No further edits are possible at this time. You will be contacted if additional information and/or clarification is required.</p>
    <p>Thank you for your interest in the {{ $project->projectSet->site->name }}.</p>
@endif
