<p>Dear {{ $subscribed ? $netid : $project->first_name}},</p>
<p>A new comment has been posted about the {{ $current_site->name }} Project for the '{{ $project->project_name }}' initiative.</p>
<p>Please follow the below link to view the comment.</p>
<p><a href="{{ route('projects.comments', ['site' => $current_site->slug, 'project_set' => $project->projectSet->slug, 'id' => $project->id]) }}">{{ route('projects.comments', ['site' => $current_site->slug, 'project_set' => $project->projectSet->slug, 'id' => $project->id]) }}</a></p>
<p>Thank you!</p>