@if ($message_type === 1)
    <p>Visit <a href="{{ $admin_url }}">{{ $admin_url }}</a>, click on the "Review PARs" link and choose "Pending Review" from the search pulldown. From here you will be able to view the PAR, ask questions, take part in discussions and/or submit an Advice for this PAR.</p>
@elseif ($message_type === 2)
    <p>You are listed on this project as a Director or Fiscal Officer. Visit <a href="{{ $par_url }}">{{ $par_url }}</a> to view this PAR as it progresses through the review process. You will be able to see related questions from the Board, the official Board Advice and the VP's final decision.</p>
@elseif ($message_type === 3)
    <p>Visit <a href="{{ $admin_url }}">{{ $admin_url }}</a>, click on the "Review PARs" link and choose "Pending Review" from the search pulldown to view the original PAR and all associated information leading up to this Advice. You will be able to approve or reject the PAR at this location as well.</p>
@else
    <p>The VP for Student Affairs and Enrollment Management has {{ $par->vp_approved ? 'approved' : 'rejected' }} PAR {{ $par->board_id }} for the "{{ $par->project->project_name }}" project.</p>
    <p>You are receiving this notice because you are listed as this project's Primary Contact, Director or Fiscal Officer. To view details about this PAR and its review by the {{ $site->name }} Advisory Board and the VP for Student Affairs and Enrollment Management, visit <a href="{{ $par_url }}">{{ $par_url }}</a>. To access information about other {{ $site->name }} projects, visit <a href="{{ $projects_url }}">{{ $projects_url }}</a>.</p>
@endif