@if ($message_type == 1)
	<p>Dear {{ $project->first_name }},</p>
    <p>You have received a progress report request from the {{ $project->projectSet->site->name }} Advisory Board for the '{{ $project->project_name }}' [FY {{ $project->fiscal_year }}] initiative. Please follow the link below and complete the report at your earliest convenience.</p>
    <p>If you are no longer the Project Manager for this project, please contact <a href="mailto:{{ getNetidEmail(site()->contact->netid) }}">{{ site()->contact->full_name }}</a> and provide new contact information as soon as possible.</p>
    <p>Progress Report Link:<br><a href="{{ route('progress.create', ['site' => $project->projectSet->site->slug, 'project_set' => $project->projectSet->slug, 'project' => $project->id]) }}">{{ route('progress.create', ['site' => $project->projectSet->site->slug, 'project_set' => $project->projectSet->slug, 'project' => $project->id]) }}</a></p>
@elseif ($message_type == 2 || $message_type == 3)
    <p>Dear {{ $message_type == 2 ? $project->director_first_name : $project->fiscal_first_name }},</p>
    <p>You are receiving a copy of this message because you are listed as the {{ $message_type == 2 ? 'Director' : 'Fiscal Officer' }} on the {{ $project->projectSet->site->name }} '{{ $project->project_name }}' [FY {{ $project->fiscal_year }}] initiative. A Progress Report for this initiative has been requested by the {{ $project->projectSet->site->name }} Advisory Board and should be filed as soon as possible. Filing the report is the responsibility of {{ $project->first_name }} {{ $project->last_name }}, the Project Manager on the original application. {{ $project->first_name }} has also received an email with instructions and a direct link to the online Progress Report submission form.</p>
    <p>You do not need to take any direct action on this email unless {{ $project->first_name }} {{ $project->last_name }} is no longer the Project Manager. In that case, please contact <a href="mailto:{{ getNetidEmail(site()->contact->netid) }}">{{ site()->contact->full_name }}</a> and provide new Project Manager information as soon as possible.</p>
@endif

@if (isset($comments))
    <p>ADDITIONAL COMMENTS FROM THE BOARD:<br>{{ $comments }}</p>
@endif

<p>Thank you!</p>