@if ($message_type === 1)
    <p>Dear {{ $application->user->full_name }},</p>
    <p>Thank you for submitting your application to serve on the {{ site()->name }} Advisory Board. You will be contacted if additional information and/or clarification is required.</p>
    <p>Thank you for your interest in the {{ site()->name }}.</p>
@else
    <p>Dear {{ $application->user->full_name }},</p>
    <p>Thank you for your interest in applying to the {{ site()->name }} Advisory Board. The Seating Committee has reviewed your application. With the many exceptional applications we received we regret to inform you that we cannot offer you a seat on the board for one or more of the following reasons:</p>
    <ul>
        <li>The minimum requirements of a 2.0 cumulative GPA were not met;</li>
        <li>The minimum skills and experiences were not as accomplished as other applicants;</li>
        <li>The number of applicants exceeds the number of seats available;</li>
        <li>The application deadline has passed.</li>
    </ul>
    <p>We thank you for your time and enthusiasm and we hope that you will reapply next spring.</p>
    <p>
        Sincerely,<br>
        The {{ site()->name }} Advisory Board
    </p>
@endif
