<div class="modal fade" id="modalMove">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="ua-brand-x"></i></button>
                <h4 class="modal-title">Move to Project</h4>
            </div>
            <form id="moveItem" style="display:inline" role="form" method="POST">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="modal-body">
                    <p>Which project would you like to move <span class="title"></span> to?</p>
                    <select name="project_id" class="form-control">
                        <option value="">Select...</option>
                        @foreach ($project->projectSet->projects->whereNotIn('project_name', [''])->whereNotIn('id', [$project->id])->sortByDesc('id') as $project)
                            <option value="{{ $project->id }}">
                                {{ $project->project_name }}
                                @if (!empty($project->board_id))
                                    ({{ $project->board_id }})
                                @endif
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" value="0">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>