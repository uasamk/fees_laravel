@component('partials.breadcrumbs.default')
    <li><a href="{{ route(!empty(session('project-index')) ? session('project-index') : 'projects.index', baseRouteParams()) }}">Projects</a></li>
    {{ $slot }}
@endcomponent