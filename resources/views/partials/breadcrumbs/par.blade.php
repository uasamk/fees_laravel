@component('partials.breadcrumbs.default')
    <li><a href="{{ route('pars.admin.index', baseRouteParams()) }}">PARs</a></li>
    {{ $slot }}
@endcomponent