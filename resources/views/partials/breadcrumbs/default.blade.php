@component('partials.breadcrumbs.base')
    @if (Auth::check())
        <li><a href="{{ route('admin', ['site' => site()->slug]) }}">Board Room</a></li>
    @endif
    {{ $slot }}
@endcomponent
