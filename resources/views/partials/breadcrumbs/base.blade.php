<li><a href="{{ site()->url }}">{{ site()->name }}</a></li>
@if (site()->projectSets->count() > 1)
    <li><a href="{{ route('projects.index', baseRouteParams()) }}">{{ projectSet()->name }}</a></li>
@endif
{{ $slot }}