@component('partials.breadcrumbs.project_base', ['project' => $project])
    <li><a href="{{ $project->url }}">{{ $project->board_id or $project->project_name }}</a></li>
    {{ $slot }}
@endcomponent