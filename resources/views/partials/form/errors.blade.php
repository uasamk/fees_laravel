@if ($errors->has($name_error))
    <span class="help-block">
        <strong>{{ $errors->first($name_error) }}</strong>
    </span>
@endif