@php
    $name_error = isset($name_error) ? $name_error : $name;
@endphp
<div class="form-group{{ $errors->has($name_error) ? ' has-error' : '' }}"{!! isset($v_if) ? (' v-if="' . $v_if . '"') : '' !!}>
    @if (isset($label))
        <label for="{{ $name }}" class="control-label{{ $inline ? ' col-md-4' : '' }}">{{ $label }}</label>
    @endif
    
    <div{{ $inline ? ' class=col-md-6' : '' }}>
      {{ $slot }}
      @include('partials.form.errors', ['name_error' => $name_error])
    </div>
</div>
