@component('partials.form.field', ['inline' => $inline, 'name' => $name, 'label' => isset($label) ? $label : null, 'v_if' => (isset($v_if) ? $v_if : NULL)])
  @include('partials.form.radio', ['name' => $name, 'key' => 1, 'value' => '1', 'label' => 'Yes', 'model' => (isset($model) ? $model : NULL)])
  @include('partials.form.radio', ['name' => $name, 'key' => 2, 'value' => '0', 'label' => 'No', 'model' => (isset($model) ? $model : NULL)])
@endcomponent
