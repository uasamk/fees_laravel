@php
    $name_error = $name;
    $original = isset($object->$name) ? $object->$name : '';
    if (isset($id)) {
        $name_error = $name . '.' . $id . (isset($id2) ? ('.' . $id2) : '');
        $name = $name . '[' . $id . ']' . (isset($id2) ? ('[' . $id2 . ']') : '');
    }

    if (isset($id2) && isset($object->$id2)) $original = $object->$id2;
    if (isset($name_other) && isset($object->$name_other)) $original = $object->$name_other;
@endphp
@component('partials.form.field', ['name' => $name, 'name_error' => $name_error, 'label' => isset($label) ? $label : null, 'inline' => $inline])
  <input id="{{ $name }}" type="{{ $type }}" class="form-control" name="{{ $name }}" value="{{ null !== old($name_error) ? old($name_error) : $original }}">
@endcomponent
