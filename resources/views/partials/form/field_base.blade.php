@php
    $name_error = isset($name_error) ? $name_error : $name;
@endphp
<div class="form-group{{ $errors->has($name_error) ? ' has-error' : '' }}"{!! isset($v_if) ? (' v-if="' . $v_if . '"') : '' !!}>
    @if (!empty($label))
        <label for="{{ $name }}" class="control-label">{{ $label }}</label>
    @endif

    {{ $slot }}
    @include('partials.form.errors', ['name_error' => $name_error])
</div>