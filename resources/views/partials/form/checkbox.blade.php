@php
    $name_error = $name;
    if (isset($id)) {
        $name_error = $name . '.' . $id;
        $name = $name . '[' . $id . ']';
    }
@endphp
<div class="checkbox{{ $errors->has($name_error) ? ' has-error' : '' }}">
  <label>
    <input
      type="checkbox"
      name="{{ $name }}"
      value="{{ $value }}"
      @if (isset($model))
        v-model="{{ $model }}"
      @else
        {{ (isset($object->$name) && $object->$name == $value) || old($name) == $value ? ' checked' : ''}}
      @endif
      {{ isset($disabled) && $disabled ? ' disabled' : '' }}
    >
    {{ $label }}
    @if ($errors->has($name_error))
        <span class="help-block">
            <strong>{{ $errors->first($name_error) }}</strong>
        </span>
    @endif
  </label>
</div>
