@php
    $name_error = $name;
    if (isset($id)) {
        $name_error = $name . '.' . $id . (isset($id2) ? ('.' . $id2) : '');
        $name = $name . '[' . $id . ']' . (isset($id2) ? ('[' . $id2 . ']') : '');
    }

    $original = '';
    // if there's old input, select this option in the select
    if (null !== old($name_error)) {
        $original = old($name_error);
    }
    // otherwise, if the provided opject has a set property matching the field name, set the selected option to this
    else if (isset($object->$name)) {
        $original = $object->$name;
    }

    // since id2 will usually represent the actual 'name', use this for the object property if set
    if (isset($id2) && isset($object->$id2)) $original = $object->$id2;
@endphp
@component('partials.form.field', ['inline' => $inline, 'name' => $name, 'name_error' => $name_error, 'label' => $label])
    <select name="{{ $name }}" class="form-control" 
        @if (isset($model))
            v-model="{{ $name }}"
            v-init:{{ $name }}="{{ $original }}"
        @endif
    >
        @if(!isset($nodefault))
            <option value="">Select...</option>
        @endif
        {{-- 
            If $value and $text are provided, access them as dynamic properties in each item:
                For example, if $items is a collection of $users and $value = 'id' then $item->$value is $user->id
            Otherwise, treat $items as a standard array
        --}}
        @if(isset($value) && isset($text))
            @foreach ($items as $item)
                <option value="{{ $item->$value }}"{{ $item->$value == $original ? ' selected' : ''}}>{{ $item->$text }}</option>
            @endforeach
        @elseif(isset($value_as_key))
            @foreach ($items as $item)
                <option {{ $item == $original ? ' selected' : ''}}>{{ $item }}</option>
            @endforeach
        @else
            @foreach ($items as $key => $item)
                <option value="{{ $key }}"{{ $key == $original ? ' selected' : ''}}>{{ $item }}</option>
            @endforeach
        @endif
    </select>
@endcomponent