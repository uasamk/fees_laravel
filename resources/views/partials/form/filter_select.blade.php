@php
    $session_name = isset($session_group) ? $session_group . '.' . $name : $name;
@endphp
<div class="form-group">
    <select name="{{ $name }}" class="form-control" onchange="this.form.submit()">
        <option value="-1">{{ $label }}...</option>
        @if(isset($value) && isset($text))
            @foreach ($items as $item)
                <option value="{{ $item->$value }}"{{ $item->$value == Session::get($session_name) ? ' selected' : ''}}>{{ $item->$text }}</option>
            @endforeach
        @elseif(isset($value_as_key))
            @foreach ($items as $item)
                <option {{ $item == Session::get($session_name) ? ' selected' : ''}}>{{ $item }}</option>
            @endforeach
        @else
            @foreach ($items as $key => $item)
                <option value="{{ $key }}"{{ $key == Session::get($session_name) ? ' selected' : ''}}>{{ $item }}</option>
            @endforeach
        @endif
    </select>
</div>