@php
    $name_error = $name;
    if (isset($id)) {
        $name_error = $name . '.' . $id;
        $name = $name . '[' . $id . ']';
    }
@endphp
@component('partials.form.field_base', ['name' => $name, 'name_error' => $name_error, 'label' => isset($label) ? $label : null])
  <textarea name="{{ $name }}" class="form-control" rows="5">{{ null !== old($name_error) ? old($name_error) : (isset($object->$name) ? $object->$name : '') }}</textarea>
@endcomponent