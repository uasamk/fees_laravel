<label class="radio-inline">
  <input
    type="radio"
    name="{{ $name }}"
    id="{{ $name }}{{ $key }}"
    value="{{ $value }}"
    @if (isset($model))
      v-model="{{ $name }}"
      @if (isset($object->$name) || null !== old($name))
        v-init:{{ $name }}="'{{ null !== old($name) ? old($name) : $object->$name }}'"
      @endif
    @else
      @if (null !== old($name))
        @if (old($name) == $value)
          checked
        @endif
      @elseif (isset($object->$name) && $object->$name == $value)
        checked
      @endif
    @endif
  >
  {{ $label }}
</label>
