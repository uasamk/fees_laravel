<div class="alert alert-{{ $type }}">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i aria-hidden="true" class="ua-brand-x"></i>
    </button>
    {{ $slot }}
</div>