@if (session()->has('sentmessage') && !session()->has('delay'))
    @component('partials.alert.base', ['type' => 'success'])
        <h4>Success</h4>
        <p>{{ session('sentmessage') }}</p>
    @endcomponent
@endif