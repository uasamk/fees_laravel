@component('partials.alert.base', ['type' => 'success'])
    <p>You're almost finished, <strong>BUT YOU MUST STILL SUBMIT THE FINAL VERSION</strong>. To submit your {{ $title }}, click the "Submit Final Version" button at the bottom of the page.</p>
@endcomponent