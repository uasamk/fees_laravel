@component('partials.alert.base', ['type' => 'danger'])
    <h4>Primary Contact Mismatch</h4>
    <p>Our records indicate you are not listed as the manager or director for this project. Only the manager/director can submit a {{ $title }}. To find the manager or director for a project, go to the <a href="{{ $project->projectSet->projectsURL }}">projects page</a> and search for the project. Please contact us if you have questions.</p>
@endcomponent