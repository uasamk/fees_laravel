@include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'reference', 'id2' => 'first_name', 'label' => 'First Name'])
@include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'reference', 'id2' => 'last_name', 'label' => 'Last Name'])
@include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'reference', 'id2' => 'email', 'label' => 'Email'])
@include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'reference', 'id2' => 'address', 'label' => 'Address'])
@include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'reference', 'id2' => 'city', 'label' => 'City'])
@include('partials.form.select', ['inline' => true, 'name' => 'reference', 'id2' => 'state', 'label' => 'State', 'items' => $states, 'value_as_key' => true])
@include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'reference', 'id2' => 'zip', 'label' => 'Zip Code'])
@include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'reference', 'id2' => 'phone', 'label' => 'Phone'])
@include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'reference', 'id2' => 'relationship', 'label' => 'Relationship'])