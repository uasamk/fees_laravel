<dl class="dl-horizontal">
    <dt>First Name</dt>
    <dd>{{ $reference->first_name }}</dd>
    <dt>Last Name</dt>
    <dd>{{ $reference->last_name }}</dd>
    <dt>Email</dt>
    <dd>{{ $reference->email }}</dd>
    <dt>Address</dt>
    <dd>{{ $reference->address }}</dd>
    <dt>City</dt>
    <dd>{{ $reference->city }}</dd>
    <dt>State</dt>
    <dd>{{ $reference->state }}</dd>
    <dt>Zip Code</dt>
    <dd>{{ $reference->zip }}</dd>
    <dt>Phone</dt>
    <dd>{{ $reference->phone }}</dd>
    <dt>Relationship</dt>
    <dd>{{ $reference->relationship }}</dd>
</dl>