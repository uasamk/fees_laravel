<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Academic Information</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                {{ $academic_information_left }}
            </div>
            <div class="col-md-6">
                {{ $academic_information_right }}
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Time Commitments</h3>
    </div>
    <div class="panel-body horizontal-reset">
        {{ $time_commitments }}
    </div>
</div>
{{ $clubs_orgs }}
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Qualifications</h3>
    </div>
    <div class="panel-body">
        <div class="horizontal-reset">
            {{ $qualifications }}
            <br>
            <p><strong>Please attach a resume to this application</strong></p>
            {{ $resume }}
        </div>
        <br>
        <p><strong>Please list two references we may contact</strong></p>
        <div class="row">
            <div class="col-md-6">
                {{ $references_left }}
            </div>
            <div class="col-md-6">
                {{ $references_right }}
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
    <h3 class="panel-title">Terms and Conditions</h3>
</div>
<div class="panel-body">
    <p>By checking the box below and submitting this application, I understand that my name, class and major can be used in publications; that I will abide by any adopted governing documents, and that all information provided is accurate.</p>
    {{ $terms }}
</div>
</div>