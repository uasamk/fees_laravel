@component('partials.user.clubs')
    @slot('clubs_orgs_intro')
        @if (isset($clubs_orgs_intro))
            {{ $clubs_orgs_intro }}
        @endif
    @endslot
    @slot('clubs_orgs_left')
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'club_org_1', 'label' => 'Club/Org #1'])
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'club_org_2', 'label' => 'Club/Org #2'])
    @endslot
    @slot('clubs_orgs_right')
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'club_org_3', 'label' => 'Club/Org #3'])
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'club_org_4', 'label' => 'Club/Org #4'])
    @endslot
@endcomponent