<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Contact Information</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                {{ $contact_information_left }}
            </div>
            <div class="col-md-6">
                {{ $contact_information_right }}
            </div>
        </div>
    </div>
</div>