<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Clubs &amp; Organizations</h3>
    </div>
    <div class="panel-body">
        {{ $clubs_orgs_intro }}
        <div class="row">
            <div class="col-md-6">
                {{ $clubs_orgs_left }}
            </div>
            <div class="col-md-6">
                {{ $clubs_orgs_right }}
            </div>
        </div>
    </div>
</div>