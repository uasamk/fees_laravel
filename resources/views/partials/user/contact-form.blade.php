@component('partials.user.contact')
    @slot('contact_information_left')
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'first_name', 'label' => 'First Name'])
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'last_name', 'label' => 'Last Name'])
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'student_id', 'label' => 'Student ID'])
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'phone', 'label' => 'Phone'])
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'email', 'label' => 'Email'])
    @endslot
    @slot('contact_information_right')
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'address', 'label' => 'Address'])
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'city', 'label' => 'City'])
        @include('partials.form.select', ['inline' => true, 'name' => 'state', 'label' => 'State', 'items' => $states, 'value_as_key' => true])
        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'zip', 'label' => 'Zip Code'])
    @endslot
@endcomponent