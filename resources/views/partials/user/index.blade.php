<div class="table-responsive">
    <table class="table-dark table-hover table-striped table-condensed">
        <thead>
            <tr>
                @if(!$directory)
                    <th>#</th>
                @endif
                <th>Name</th>
                @if(!$directory)
                    <th>Start Year</th>
                    <th>Fiscal Year</th>
                    <th>Status</th>
                @endif
                <th>Title</th>
                @if(!$directory)
                    <th>Type</th>
                    <th>Access Level</th>
                    <th>Directory</th>
                    <th></th>
                    <th></th>
                @endif
                @if($directory)
                    @if($admin)
                        <th>Membership Type</th>
                    @else
                        <th>Class</th>
                    @endif
                    <th>Responsibilities</th>
                @endif
            </tr>
        </thead>
        <tbody>
        @foreach ($users as $user)
            <tr>
                @if(!$directory)
                    <td>{{ $loop->iteration }}</td>
                @endif
                <td>{{ $user->full_name }}</td>
                @if(!$directory)
                    <td>{{ $user->start_year }}</td>
                    <td>{{ $user->fiscal_year > 0 ? $user->fiscal_year : '' }}</td>
                    <td>{{ $user->status['attributes']['name'] }}</td>
                @endif
                <td>{{ (empty($user->user_title_id) || $user->user_title_id == 8) ? $user->title_other : $user->title->name }}</td>
                @if(!$directory)
                    <td>{{ $user->type->name or '' }}</td>
                    <td>{{ $user->role->name }}</td>
                    <td>{{ formatBoolean($user->directory) }}</td>
                    <td><a class="text-blue30w" href="{{ route('users.edit', ['site' => site()->slug, 'id' => $user->id]) }}" title="Edit Board Member"><span class="glyphicon glyphicon-pencil"></a></td>
                    <td>
                        <a href="#" class="text-blue30w" title="Delete Board Member"
                        data-toggle="modal"
                        data-target="#modalDelete"
                        data-title="{{ $user->full_name }}"
                        data-action="{{ route('users.destroy', ['site' => site()->slug, 'id' => $user->id]) }}"
                        ><span class="glyphicon glyphicon-trash"></a>
                    </td>
                @endif
                @if($directory)
                    @if($admin)
                        <td>{{ $user->type->name }}</td>
                    @else
                        <td>{{ $user->standing->name or ''}}</td>
                    @endif
                    <td>{{ $user->responsibilities }}</td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>