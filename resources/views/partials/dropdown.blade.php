<li class="dropdown">
    <a @if(!empty($url)) href="{{ $url }}" @endif role="button" aria-haspopup="true" aria-expanded="false">{{ $title }} <span class="caret hidden-xs"></span></a>
    <a class="dropdown-toggle visible-xs" data-toggle="dropdown"><span class="caret"></span></a>
    <ul class="dropdown-menu">
        {{ $slot }}
    </ul>
</li>