@slot('vp_comments')
    @include('partials.form.textarea', ['name' => 'vp_comments', 'object' => isset($par) ? $par : null])
@endslot
@slot('vp_approval')
    @include('partials.form.radio_binary', ['inline' => false, 'name' => 'vp_approved', 'object' => isset($par) ? $par : null])
@endslot