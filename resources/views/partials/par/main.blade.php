<div class="panel-body">
    @if ($mode == 1 || $mode == 4)
        {{ $introduction }}

        <h4>Requested Changes</h4>
        <p>Please describe the changes you wish to make to your project in the space below. Limited to 1500 words.</p>
        {{ $changes }}

        <p><strong>As needed, attach a document with supporting data:</strong></p>
        {{ $document }}
    @endif
    @if ($mode == 4)
        <hr>
        <h4>Questions &amp; Answers</h4>
        {{ $questions }}
        <hr>
    @endif
    @if ($mode == 2 || $mode == 4)
        <h4>Board Advice</h4>
        <p><strong>Advice</strong> (Please detail the Board's recommendations in the field below. Limited to 1500 words.):</p>
        {{ $advice }}

        <p><strong>Does the board approve this PAR?</strong></p>
        {{ $approval }}

        <p><strong>As needed, attach a document with supporting data:</strong></p>
        {{ $advice_document }}
    @endif
    @if ($mode == 4)
        <hr>
    @endif
    @if ($mode == 3 || $mode == 4)
        <h4>VP Approval</h4>
        <p><strong>Comments:</strong> (Optional / Limited to 1500 words)</p>
        {{ $vp_comments }}

        <p><strong>VP Approval</strong> (Does the Office of the Provost this PAR?):</p>
        {{ $vp_approval }}
    @endif
</div>