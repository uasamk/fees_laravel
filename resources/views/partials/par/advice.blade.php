@slot('advice')
    @include('partials.form.textarea', ['name' => 'advice', 'object' => isset($par) ? $par : null])
@endslot
@slot('approval')
    @include('partials.form.radio_binary', ['inline' => false, 'name' => 'approved', 'object' => isset($par) ? $par : null])
@endslot
@slot('advice_document')
    @if (isset($par->fileAdviceURL))
        <p><a href="{{ $par->fileAdviceURL }}">Supporting Document</a></p>
    @endif
    @component('partials.form.field_base', ['name' => 'file_support_advice', 'label' => 'Excel, PDF or Word format (1.75MB or less)'])
        <input type="file" name="file_support_advice">
    @endcomponent
@endslot