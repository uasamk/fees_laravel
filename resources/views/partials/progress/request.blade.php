<div class="modal fade" id="modalRequestReport">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="ua-brand-x"></i></button>
            <h4 class="modal-title">Request Progress Report</h4>
        </div>
        <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('progress.send', baseRouteParams()) }}">
            {{ csrf_field() }}
            <div class="modal-body">
            <p class="single">
                This form will email a progress report request to <strong><span class="name"></span></strong> for the <strong>"<span class="project"></span>"</strong> initiative. 
                <strong><span class="director"></span></strong>, the initiative director, will also receive notification of the request.
                Fiscal Officer <strong><span class="fiscal"></span></strong> will receive an alert as well. Comments may be included below, if desired:
            </p>
            <p class="multiple">
                This form emails progress report requests to all contacts (applicants, directors &amp; fiscal officers) of <strong>ALL</strong> (active, funded, locked, 10 months or older) applications. Comments may be included below, if desired:
            </p>
            @include('partials.form.textarea', ['name' => 'comments', 'label' => '', 'object' => null])
            <p class="multiple">
                Current due date:
                @include('partials.progress.deadlines')
                (included w/email)
            </p>
            </div>
            <div class="modal-footer">
                <input class="project-id" type="hidden" name="project_id" value="">
                <button type="submit" class="btn btn-primary" name="save_draft" value="0">Submit</button>
            </div>
        </form>
        </div>
    </div>
</div>