<div class="panel-body">
    {{ $introduction }}
    <h4>Project Scope</h4>
    <p>Please provide a <strong>brief</strong> summary of the project being supported by the {{ site()->name }}. Limited to 250 words.</p>
    {{ $scope }}<br>
    <h4>Outcomes</h4>
    <p>What were the goals of your proposal and were the goals met? For example, the project was able to provide 20 additional students a month with counseling; 50 additional grad students were able to attend conferences; Safe Ride expanded its services by 20 hours a week. Please include data i.e. graphs, budgets, numbers.</p>
    {{ $outcomes }}
    <p><strong>Attach a document with supporting data:</strong></p>
    {{ $file_outcomes }}<br>
    <h4>Student Response</h4>
    <p>Please provide a brief summary of the impact this project has had on students including the number of students served and/or the type of service provided. Limited to 300 words.</p>
    {{ $response }}<br>
    <h4>Finances</h4>
    <p>Please upload the completed excel Budget Template that was e-mailed to you. Be sure to complete the section on the spreadsheet providing detail of other funding sources secured to maintain this project, if any beyond the funding of the {{ site()->name }}.</p>
    <p><strong>Attach a document with supporting data:</strong></p>
    {{ $budget }}
</div>