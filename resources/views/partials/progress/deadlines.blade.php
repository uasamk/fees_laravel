@if (site()->projectSets->count() > 1)
    @foreach (site()->projectSets as $project_set)
        @if ($loop->last)
            and 
        @elseif (!$loop->first && !$loop->last)
            , 
        @endif
        <strong>{{ $project_set->progressDeadlineDate }}</strong> for {{ $project_set->name }}{{ $loop->last ? '.' : '' }}
    @endforeach
@else
    <strong>{{ site()->projectSets->first()->progressDeadlineDate }}</strong>.
@endif 