<table class="table table-hover table-striped table-condensed">
    <thead>
        <tr>
        <th>Year</th>
        <th class="text-right">Requested</th>
        <th class="text-right">Board Funded</th>
        <th class="text-right">Final Budget*</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($project->allFundings as $funding)
            <tr>
                <td class="text-left">{{ $project->start_year + $funding->year /* remember we add one year to get the fiscal year */ }}</td>
                @if ($edit)
                    <td><input id="requested_amount{{ $funding->year }}" type="text" class="form-control input-sm text-right" name="requested_amount[{{ $funding->year }}]" placeholder="{{ old('requested_amount.' . $funding->year) !== null ? old('requested_amount.' . $funding->year) : $funding->requested_amount }}"></td>
                    <td><input id="board_amount{{ $funding->year }}" type="text" class="form-control input-sm text-right" name="board_amount[{{ $funding->year }}]" placeholder="{{ $funding->board_amount }}"></td>
                    <td><input id="final_amount{{ $funding->year }}" type="text" class="form-control input-sm text-right" name="final_amount[{{ $funding->year }}]" placeholder="{{ $funding->final_amount }}"></td>
                @else
                    <td class="text-right">${{ number_format($funding->requested_amount) }}</td>
                    <td class="text-right">${{ number_format($funding->board_amount) }}</td>
                    <td class="text-right">${{ number_format($funding->final_amount) }}</td>
                @endif
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
        <td>Total</td>
        <td class="text-right">${{ number_format($project->totalFunding->requested_amount) }}</td>
        <td class="text-right">${{ number_format($project->totalFunding->board_amount) }}</td>
        <td class="text-right">${{ number_format($project->totalFunding->final_amount) }}</td>
        </tr>
    </tfoot>
</table>
<p class="text-left"><small>* - Difference between Board Funded and Final Budget are due to changes in ERE rates or other University mandated or SVP approved changes to budgets</small></p>