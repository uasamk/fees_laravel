  @if (!$summary)
    @include ('partials.project.contact_main')
  @endif
  @php
    $admin_edit = isset($admin_edit) ? $admin_edit : false;
  @endphp
  @if (!$admin_edit || ($admin_edit && userIsPower()))
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Project Information</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            {{ $project_information_left }}
          </div>
          <div class="col-md-6">
            {{ $project_information_right }}
          </div>
        </div>
        {{ $project_information_bottom }}
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Supporting Documents</h3>
      </div>
      <div class="panel-body">
        {{ $supporting_documents_top }}
        @if (!$summary)
          <ol>
            <li>
              <strong>Please attach an itemized budget using the spreadsheet provided below</strong>
              {{ $supporting_documents_1 }}
            </li>
            <li>
              <strong>Please attach NO MORE than a 2-page project description, addressing but not limited to the following points</strong>
              {{ $supporting_documents_2 }}
            </li>
            <li>
              <strong>As needed, you may attach up to 2 pages of additional information</strong>
              {{ $supporting_documents_3 }}
            </li>
          </ol>
        @endif
      </div>
    </div>
    @if (!$summary)
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Terms and Conditions</h3>
        </div>
        <div class="panel-body">
          <p>
            By checking the box below and submitting this application, I understand that all of this information may be used in publications; that I will abide by any adopted 
            @if (!empty(site()->governing_doc_url))
              <a href="{{ site()->governing_doc_url }}">governing documents</a>,
            @else
              governing documents,
            @endif
            and that all information provided is accurate.
          </p>
          {{ $terms }}
        </div>
      </div>
    @else
      @include ('partials.project.contact_main')
    @endif
  @endif
