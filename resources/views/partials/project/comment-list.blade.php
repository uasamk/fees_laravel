@foreach ($comments as $comment)
    <p>
        <strong>Submitted: {{ formatDate($comment->created_at) }}</strong><br>
        by NetID: {{ $comment->netid }}
    </p>
    <p>{{ $comment->comment_text }}</p>
    <p><a data-toggle="collapse" href="#collapseComment{{ $comment->id }}" aria-expanded="false" aria-controls="collapseComment{{ $comment->id }}">Reply</a></p>
    <div class="collapse{{ $errors->has('comment_text.' . $comment->id) ? ' in' : '' }}" id="collapseComment{{ $comment->id }}">
        @include('partials.project.comment', ['parent' => $comment->id])
    </div>
    @if ($comment->children->count() > 0)
        <hr>
        <div class="left-indent">
            @include('partials.project.comment-list', ['comments' => $comment->children])
        </div>
    @endif
    @if(!$loop->last)
        <hr>
    @endif
@endforeach