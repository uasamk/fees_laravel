<form role="form" method="POST" enctype="multipart/form-data" action="{{ route('projects.comments.save', routeParams(['id' => $project->id])) }}">
    {{ csrf_field() }}
    @include('partials.form.textarea', ['name' => 'comment_text', 'label' => 'Comment', 'id' => $parent])
    <input type="checkbox" name="subscribe[{{ $parent }}]" value="1"{{ $subscribed ? ' checked' : ''}}> Subscribe to Project Comments
    <input type="hidden" name="parent_id" value="{{ $parent }}">
    <p class="text-right">
        <button type="submit" class="btn btn-primary">Submit</button>
    </p>
</form>