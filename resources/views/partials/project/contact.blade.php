@slot('contact_information_left')
    <h4>Project Manager</h4>
    <dl class="dl-horizontal">
        <dt>First Name</dt>
        <dd>{{ $project->first_name }}</dd>
        <dt>Last Name</dt>
        <dd>{{ $project->last_name }}</dd>
        <dt>Email</dt>
        <dd>{{ $project->email }}</dd>
        <dt>Department</dt>
        <dd>{{ $project->department->name or $project->department_other }}</dd>
        @if (site()->departments->count() > 0)
            <dt>Area/Unit</dt>
            <dd>{{ $project->division }}</dd>
        @endif
    </dl>
@endslot
@slot('contact_information_right')
    <h4>Project Director</h4>
    <dl class="dl-horizontal">
        <dt>First Name</dt>
        <dd>{{ $project->director_first_name }}</dd>
        <dt>Last Name</dt>
        <dd>{{ $project->director_last_name }}</dd>
        <dt>Email</dt>
        <dd>{{ $project->director_email }}</dd>
    </dl>

    <h4>Fiscal Officer</h4>
    <dl class="dl-horizontal">
        <dt>First Name</dt>
        <dd>{{ $project->fiscal_first_name }}</dd>
        <dt>Last Name</dt>
        <dd>{{ $project->fiscal_last_name }}</dd>
        <dt>Email</dt>
        <dd>{{ $project->fiscal_email }}</dd>
        <dt>Approval Status</dt>
        <dd>
        @if ($project->signoff)
            Approved
        @elseif (!empty($project->fiscal_email))
            No business manager approval
            {{-- check if not confirming application and user is admin or above to show option to send reminder --}}
            @if (!$confirm && userIsAdmin())
                <form method="POST" enctype="multipart/form-data" action="{{ route('projects.remind', routeParams(['id' => $project->id])) }}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn-link">Send reminder</button>
                </form>
            @endif
        @else
            Business Manager's email not yet entered
        @endif
        </dd>
    </dl>
@endslot