@foreach ($pages->sortBy('id')->sortBy('list_order') as $child_page)
    @if($child_page->published)
        <li><a href="{{ $child_page->url }}">{{ $child_page->title }}</a></li>
        @if ($child_page->children->count() > 0)
            <ul>
                @include('partials.nested_menus', ['pages' => $child_page->children])
            </ul>
        @endif
    @endif
@endforeach