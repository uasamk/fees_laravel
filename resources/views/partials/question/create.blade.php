@php
    $old_title = '';
    $old_project = '';
    
    if (null !== old('item_type')) {
        $type = old('item_type');
        $item_id = old('item_id');

        if ($type == 2) {
            $par = App\Par::find($item_id);
            $old_title = $par->board_id;
            $old_project = $par->project->project_name;
        }
    }
@endphp

<div class="modal fade" id="modalQuestion">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="ua-brand-x"></i></button>
                <h4 class="modal-title">Submit Question</h4>
            </div>
            <form id="submitQuestion" role="form" method="POST" enctype="multipart/form-data" action="{{ route('questions.store', ['site' => site()->slug]) }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <p class="single">
                        You are submitting a question regarding <strong><span class="title">{{ $old_title }}</span></strong> for the <strong>"<span class="project">{{ $old_project }}</span>"</strong> initiative.
                        Enter your question in the form below and click "Submit" to send your question to the applicant. Please submit only ONE question at a time.
                        If you have multiple questions, please return to this form to submit them.
                    </p>
                    @include('partials.form.textarea', ['name' => 'question', 'label' => '', 'object' => null])
                </div>
                <div class="modal-footer">
                    <input class="item-type" type="hidden" name="item_type" value="{{ old('item_type') }}">
                    <input class="item-id" type="hidden" name="item_id" value="{{ old('item_id') }}">
                    <button type="submit" class="btn btn-primary" name="save" value="0">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@section('scripts')
    @if ($errors->has('question'))
        <script>
            // automatically toggle modal on reload (if errors)
            $('#modalQuestion').modal('toggle');
        </script>
    @endif
@endsection