@if ($questions->count() > 0)
    @foreach ($questions as $question)
        <p>
            <strong>Submitted: {{ formatDate($question->created_at) }}</strong><br>
            <span>Deadline: {{ formatDate($question->deadline) }}</span><br>
            by NetID: {{ $question->netid }}
        </p>
        <p><strong>Q:</strong> {!! nl2br($question->question_text) !!}</p>

        @if ($question->answers->count() > 0)
            <div class="left-indent">
                @foreach ($question->answers as $answer)
                    <p><strong>A:</strong> {!! nl2br($answer->answer_text) !!}</p>
                @endforeach
            </div>
        @else
            @if (userIsChair() && session('webauth.net_id') != $project->netid)
                <div class="deadline_extension_container">
                    <span class="question_extension_text">How many days would you like to extend the question deadline?</span>
                    <form role="form" enctype="multipart/form-data" method="post" action="{{ route('questions.questionExtension', routeParams(['site' => site()->slug])) }}" class="question_input_wrap">
                        <?php 
                            $today = new \DateTime('now');
                            $deadline = new \DateTime($question->deadline);

                            $expired = ($today > $deadline) ? true : false;
                        ?>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="q_id" value="<?= $question->id ?>">
                        <input type="hidden" name="item_id" value="<?= $project->id ?>">
                        <input type="number" min="1" max="365" value="" name="days" class="question_extension_input <?= ($expired) ? 'expired' :'' ?>" type="number" placeholder="<?= ($expired) ? '(Expired) ' :'' ?>Days">
                        <button type="submit">Extend</button>
                    </form>
                </div>
            @endif
        @endif
        
        @if(!$loop->last)
            <hr>
        @endif
    @endforeach
@else
    <p>No Questions or Answers</p>
@endif