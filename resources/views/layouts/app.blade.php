<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="robots" content="noindex">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ null !== site() ? site()->name : config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
      <header class="l-arizona-header bg-red" id="region_header_ua">
        <section class="container l-container">
          <div class="row"><a class="arizona-logo" href="http://www.arizona.edu" title="The University of Arizona homepage"><img class="arizona-line-logo" alt="The University of Arizona Wordmark Line Logo White" src="https://brand.arizona.edu/sites/default/files/digital-resources/ua_wordmark_line_logo_white_rgb.svg"/></a></div>
        </section>
      </header>
      <div id="header_site">
        <div class="container">
          <div class="row">
            <div id="site_identifier">
              <a href="http://saem-aiss.arizona.edu/"><img src="{{ asset('img/logo.png') }}"></a>
            </div>
            <div id="utility_links">
              <ul>
                @if (null !== site())
                  @if(!empty(site()->contact_id))
                    <li><a href="{{ route('contact', ['site' => site()->slug]) }}">Comments &amp; Questions</a></li>
                  @endif
                  <li><a href="{{ route('admin', ['site' => site()->slug]) }}">Board Room</a></li>
                  @if (Auth::guest())
                    <li><a href="{{ route('login', ['site' => site()->slug]) }}">Login</a></li>
                  @else
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                  @endif
                @endif
             </ul>
           </div>
          </div>

          @if (null !== site())
            @php
              $pages = site()->pages->where('parent_id', 0);
            @endphp
            <div class="row">
              <div class="navbar navbar-default navbar-static-top" role="navigation">
                <div class="container">
                  <div class="row">
                    <div class="navbar-header">
                      <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false" aria-controls="navbar"><span class="sr-only">Toggle navigation</span>
                        <!-- span.icon-bar-->
                        <!-- span.icon-bar-->
                        <!-- span.icon-bar--><span class="text">MAIN MENU</span>
                      </button>
                    </div>
                    <div class="navbar-collapse collapse" id="main-navbar">
                      <ul class="nav navbar-nav">
                        <li><a href="{{ site()->home->url }}">{{ site()->home->title }}</a></li>
                        @if (site()->projectSets->count() > 0)
                          @component('partials.dropdown', ['url' => route('projects.index', baseRouteParams()), 'title' => 'Projects'])
                            <li><a href="{{ route('progress.start', baseRouteParams()) }}">Submit Progress Reports</a></li>
                            <li><a href="{{ route('pars.start', baseRouteParams()) }}">Submit PARs</a></li>
                          @endcomponent
                        @endif
                        @foreach ($pages->where('default', 0)->where('board_home', 1)->sortBy('id')->sortBy('list_order') as $page)
                          @if($page->published)
                            @component('partials.dropdown', ['url' => $page->url, 'title' => $page->title])
                              <li><a href="{{ route('users.directory', ['site' => site()->slug]) }}">Board Member Directory</a></li>
                              @if ($page->children->where('published', 1)->count() > 0)
                                @include('partials.nested_menus', ['pages' => $page->children])  
                              @endif
                            @endcomponent
                          @endif
                        @endforeach
                        @foreach ($pages->where('default', 0)->where('board_home', 0)->sortBy('id')->sortBy('list_order') as $page)
                          @if($page->published)
                            @if ($page->children->where('published', 1)->count() > 0)
                              @component('partials.dropdown', ['url' => $page->url, 'title' => $page->title])
                                @include('partials.nested_menus', ['pages' => $page->children])
                              @endcomponent
                            @else
                              <li><a href="{{ $page->url }}">{{ $page->title }}</a></li>
                            @endif
                          @endif
                        @endforeach
                      </ul>
                      <ul class="nav navbar-nav navbar-right">
                        @if (site()->projectSets->count() > 1)
                          @component('partials.dropdown', ['url' => '', 'title' => 'Selected: ' . projectSet()->name])
                            @if (site()->projectSets->count() > 1)
                              @foreach (site()->projectSets as $project_set)
                                <li><a href="{{ route('project-sets.set', [site()->slug, 'id' => $project_set->id, 'current' => request()->route('project_set')]) }}">{{ $project_set->name }}</a></li>
                              @endforeach
                            @endif
                          @endcomponent
                        @endif
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endif
        </div>
      </div>
      <div class="container">      
        <ol class="breadcrumb">
            @yield('breadcrumbs')
        </ol>
        @if (session('error'))
            @component('partials.alert.base', ['type' => 'danger'])
            {{ session('error') }}
            @endcomponent
        @endif
        <h1>@yield('title')</h1>
        @include('partials.alert.sent-message')
        @yield('content')
      </div>
    </div>

    <footer id="footer_site">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="text-center-sm text-center-xs text-left" id="footer_logo">
              <a href="http://saem-aiss.arizona.edu/"><img src="{{ asset('img/logo.png') }}"></a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-center-sm text-center-xs text-right" id="footer_menu">
              Administration Building, Room 401<br>
              The University of Arizona<br>
              Tucson, AZ USA 85721-0066 <br>
              Phone: 520-621-3772 | Fax: 520-621-3776
            </div>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-12">
            <div class="text-center" id="footer_copyright">
              Copyright {{ date('Y') }} | Arizona Board of Regents<br>
              <a href="https://privacy.arizona.edu/privacy-statement" target="_blank" rel="noopener">University Privacy Statement</a>
            </div>
          </div>
        </div>
      </div>
      <!-- /.container-->
    </footer>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>
