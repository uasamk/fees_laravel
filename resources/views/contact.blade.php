@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.base')
        <li class="active">Comments &amp; Questions</li>
    @endcomponent
@endsection

@section('title')
    Comments &amp; Questions
@endsection

@section('content')
    @if(empty(site()->contact_id))
        @component('partials.alert.base', ['type' => 'warning'])
            <h4>Form Unavailable</h4>
            <p>The Comments &amp; Questions form is currently unavailable. Please check back later.</p>
        @endcomponent
    @endif
    <p>Use the form below to submit comments or questions to the {{ site()->name }} Advisory Board.</p>
    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('contact.send', ['site' => site()->slug]) }}">
        {{ csrf_field() }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Contact Us</h3>
            </div>
            <div class="panel-body">
                <fieldset{{ empty(site()->contact_id) ? ' disabled' : '' }}>
                    @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'name', 'label' => 'Name'])
                    @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'email', 'label' => 'Email'])
                    @component('partials.form.field', ['inline' => true, 'name' => 'message', 'label' => 'Questions/Comments'])
                        <textarea name="message" class="form-control" rows="5">{{ old('message') }}</textarea>
                    @endcomponent
                </fieldset>
            </div>
            <div class="g-recaptcha" style="margin-left: auto; max-width: 304px;" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY')  }}"></div>
            <div class="panel-footer text-right">
                <button type="submit" class="btn btn-primary"{{ empty(site()->contact_id) ? ' disabled' : '' }}>Submit</button>
            </div>
        </div>
    </form>
@endsection