@extends('layouts.app')

@section('breadcrumbs')
  @if ($apply)
    @include('partials.breadcrumbs.create')
  @elseif ($authorized)
    @component('partials.breadcrumbs.default')
      <li class="active">Projects</li>
    @endcomponent
  @else
    @component('partials.breadcrumbs.base')
      <li class="active">Projects</li>
    @endcomponent
  @endif
@endsection

@section('title')
  @if ($apply)
    Apply for Funding
  @else
    {{ $admin ? 'Edit' : 'View' }} Projects
  @endif
@endsection

@section('content')

  @if ($apply)
    <p>Welcome, {{ $netid }}!</p>
    <p>You have the following applications open and available for editing. You may also <a href="{{ route('projects.create', baseRouteParams()) }}">create a new application</a> if you wish.</p>
  @else
    <h4>Which group of funding applications would you like to see?</h4>
    <form role="form" method="GET" action="{{ route($filter_route, baseRouteParams()) }}">
      <div class="row">
        <div class="col-md-2">
          @include('partials.form.filter_select', ['session_group' => 'project-filters', 'name' => 'funding-status', 'label' => 'Funding Status', 'items' => $funding_statuses, 'value' => 'id', 'text' => 'name'])
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <select name="year" class="form-control" onchange="this.form.submit()">
              <option value="-1">Fiscal Year...</option>
              @foreach ($years as $year)
                @if ($year > 0)
                  <option value="{{ $year }}"{{ $year == Session::get('project-filters.year')? ' selected' : ''}}>FY {{ $year+1 }}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <select name="project-status" class="form-control" onchange="this.form.submit()">
              <option value="-1">Project Status...</option>
              <option value="0"{{ Session::get('project-filters.project-status') === '0' ? ' selected' : ''}}>Active</option>
              <option value="1"{{ Session::get('project-filters.project-status') === '1' ? ' selected' : ''}}>Archived</option>
            </select>
          </div>
        </div>
        @if ($authorized)
          <div class="col-md-2">
            <div class="form-group">
              <select name="progress-reports" class="form-control" onchange="this.form.submit()">
                <option value="-1">Progress Reports...</option>
                <option value="30"{{ Session::get('project-filters.progress-reports') == 30 ? ' selected' : ''}}>Last 30 Days</option>
                <option value="60"{{ Session::get('project-filters.progress-reports') == 60 ? ' selected' : ''}}>Last 60 Days</option>
                <option value="90"{{ Session::get('project-filters.progress-reports') == 90 ? ' selected' : ''}}>Last 90 Days</option>
                <option value="180"{{ Session::get('project-filters.progress-reports') == 180 ? ' selected' : ''}}>Last 180 Days</option>
                <option value="365"{{ Session::get('project-filters.progress-reports') == 365 ? ' selected' : ''}}>Last 365 Days</option>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <input type="checkbox" name="admin-projects" value="1"{{ Session::get('project-filters.admin-projects') == 1 ? ' checked' : ''}} onclick="this.form.submit()">
            Administrative Projects
          </div>
        @endif
      </div>
    </form>
  @endif
  <div class="table-responsive">
    <table class="table-dark table-hover table-striped table-condensed{{ $authorized ? ' table-funding' : '' }}">
      <thead>
        <tr>
          @if ($authorized || $apply)
            <th>#</th>
          @endif
          @if (!$apply)
            <th>ID</th>
          @endif
          <th>Project Name</th>
          <th>Department</th>
          <th>Manager</th>
          <th>Initial Year</th>
          <th class="text-right">
            @if ($authorized)
              Request/Years
            @elseif (!$apply)
              Selected Year Funding
            @endif
          </th>
          @if (!$apply)
            <th>Status</th>
            @if ($authorized)
              <th>Public</th>
              <th>Locked</th>
              <th>Withdrawn</th>
              @if ($admin)
                <th></th>
                @if (userIsAdmin())
                  <th></th>
                @endif
                <th></th>
                @if (userIsPower())
                  <th></th>
                @endif
              @endif
            @endif
          @endif
        </tr>
      </thead>
      <tbody>
      @foreach ($projects as $key => $project)
        <tr>
          @if ($authorized || $apply)
            <td>{{ $apply ? $loop->iteration : $project->id }}</td>
          @endif
          @if (!$apply)
            <td>{{ $project->board_id }}</td>
          @endif
          <td><a href="{{ route($apply ? 'projects.edit' : 'projects.show', routeParams(['id' => $project->id])) }}">{{ !empty($project->project_name) ? $project->project_name : 'Untitled' }}</a></td>
          <td>{{ $project->department->name or '' }}</td>
          <td>{{ $project->first_name }} {{ $project->last_name }}</td>
          <td>{{ !empty($project->start_year) ? ('FY' . substr($project->fiscal_year, 2, 2)) : '' }}</td>
          <td class="text-right" style="position: relative;">
            @if ($authorized)
              <a href="#fundingCollapse{{ $project->id }}" role="button" data-toggle="collapse">${{ number_format($project->totalFunding->requested_amount/$project->duration) }}/{{ $project->duration }}</a>
            @elseif (!$apply)
              ${{ number_format($project->selectedYearFunding(Session::get('project-filters.year'))->board_amount) }}
            @endif
          </td>
          @if (!$apply)
            <td>{{ $project->archived ? 'Archived' : 'Active' }}</td>
            @if ($authorized)
              <td>{{ formatBoolean($project->public) }}</td>
              <td>{{ formatBoolean($project->locked) }}</td>
              <td>{{ formatBoolean($project->funding_status_id == 6) }}</td>
              @if ($admin)
                <td>
                  <a class="text-blue30w" href="#" title="Submit Question"
                      data-toggle="modal"
                      data-target="#modalQuestion"
                      data-title="{{ $project->board_id }}"
                      data-project="{{ $project->project_name }}"
                      data-item-type="1"
                      data-item-id="{{ $project->id }}">
                      <span class="glyphicon glyphicon-question-sign">
                  </a>
                </td>
                @if (userIsAdmin())
                  <td>
                    <a class="text-blue30w" href="#" title="Request Progress Report"
                      data-toggle="modal"
                      data-target="#modalRequestReport"
                      data-project="{{ $project->project_name }}"
                      data-name="{{ $project->first_name }} {{ $project->last_name }}"
                      data-director="{{ $project->director_first_name }} {{ $project->director_last_name }}"
                      data-fiscal="{{ $project->fiscal_first_name }} {{ $project->fiscal_last_name }}"
                      data-id="{{ $project->id }}">
                      <span class="glyphicon glyphicon-list-alt">
                    </a>
                  </td>
                @endif
                <td><a class="text-blue30w" href="{{ route('projects.admin.edit', routeParams(['id' => $project->id])) }}" title="Edit Project"><span class="glyphicon glyphicon-pencil"></a></td>
                @if (userIsPower())
                  <td>
                    <a href="#" class="text-blue30w" title="Delete Project"
                      data-toggle="modal"
                      data-target="#modalDelete"
                      data-title="{{ $project->project_name }}"
                      data-action="{{ route('projects.destroy', routeParams(['id' => $project->id])) }}"
                      ><span class="glyphicon glyphicon-trash"></a>
                  </td>
                @endif
              @endif
            @endif
          @endif
        </tr>
        @if ($authorized)
          <tr>
            <td colspan="15" style="padding:0"> 
              <div id="fundingCollapse{{ $project->id }}" class="collapse" style="padding:.2em;" id="fundingCollapse{{ $project->id }}">
                @include('partials.project.funding', ['edit' => false])
              </div>
            </td>
          </tr>
        @endif
      @endforeach
      </tbody>
    </table>
    @if ($projects->count() == 0)
      <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i aria-hidden="true" class="ua-brand-x"></i>
        </button>
        There were no results. Please try again.
      </div>
    @endif
  </div>

  @include('partials.progress.request')
  @include('partials.question.create')
  @include('partials.confirm_delete')

@endsection
