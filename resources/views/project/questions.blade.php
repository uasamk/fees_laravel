@extends('layouts.app')

@section('breadcrumbs')
  @component('partials.breadcrumbs.project', ['project' => $project])
        <li class="active">Questions &amp; Answers</li>
  @endcomponent
@endsection

@section('title')
    Board Questions and Applicant Answers
@endsection

@section('content')
    <h3>{{ $project->project_name }}</h3>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Questions &amp; Answers</h3>
        </div>
        <div class="panel-body">
            @include('partials.question.index', ['questions' => $project->questions])
        </div>
    </div>
@endsection