@extends('layouts.app')

@section('breadcrumbs')
  @component('partials.breadcrumbs.project', ['project' => $project])
        <li class="active">Comments</li>
  @endcomponent
@endsection

@section('title')
    Comments
@endsection

@section('content')
    <h3>{{ $project->project_name }}</h3>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Comment Details</h3>
        </div>
        <div class="panel-body">
            @if ($comments->count() == 0)
                <p>No comments on file for this project.</p>
            @else    
                @include('partials.project.comment-list', ['comments' => $comments])
            @endif
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Add New Comment</h3>
        </div>
        <div class="panel-body">
            @include('partials.project.comment', ['parent' => 0])
        </div>
    </div>
@endsection