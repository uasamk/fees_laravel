@extends('layouts.app')

@section('breadcrumbs')
  @component('partials.breadcrumbs.project', ['project' => $project])
        <li class="active">{{ ucwords($title1) }}s</li>
  @endcomponent
@endsection

@section('title')
    {{ ucwords($title1) }}s
@endsection

@section('content')
    <h3>{{ $project->project_name }}</h3>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{ ucwords($title2) }}s</h3>
        </div>
        <div class="panel-body">
            @if ($files->count() == 0)
                <p>No {{ $title1 }}s on file for this project.</p>
            @else    
                @foreach ($files as $file)
                    @if (userIsPower())
                        <p>
                            <a href="#"
                                data-toggle="modal"
                                data-target="#modalDelete"
                                data-title="this {{ $title1 }}"
                                data-action="{{ route('projects.files.destroy', routeParams(['id' => $file->id])) }}"
                                >Delete File</a>
                            | <a href="#"
                                data-toggle="modal"
                                data-target="#modalMove"
                                data-title="this {{ $title1 }}"
                                data-action="{{ route('projects.files.move', routeParams(['id' => $file->id])) }}"
                                >Move File</a>
                        </p>
                    @endif
                    <p>
                        <strong>Submitted: {{ formatDate($file->created_at) }}</strong><br>
                        by NetID: {{ $file->netid }}
                    </p>
                    <dl class="dl-horizontal">
                        <dt><a href="{{ Storage::url($file->name) }}">View File</a></dt>
                        <dd><strong>Comments:</strong><br> {!! nl2br($file->comments) !!}</dd>
                    </dl>
                    @if(!$loop->last)
                        <hr>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
    @if (userIsAdmin())
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Attach {{ ucwords($title1) }}</h3>
            </div>
            <div class="panel-body">
                <p>Use the form below to upload {{ $title1 }}s for the "<strong>{{ $project->project_name }}</strong>" project.</p>
                <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('projects.files.save', routeParams(['id' => $project->id])) }}">
                    {{ csrf_field() }}
                    @component('partials.form.field', ['inline' => false, 'name' => 'file', 'label' => ($type == 1 ? 'Excel, ': '') . 'PDF or Word (1.5MB or less)'])
                        <input type="file" name="file">
                    @endcomponent
                    @if ($type == 4)
                        @include('partials.form.checkbox', ['name' => 'instructions', 'value' => 1, 'label' => 'Send "Instructions" Email'])
                        <p class="help-block">Check this box if this is the initial funding notification letter and you wish to automatically send an email summarizing important dates and responsibilities to the primary contact for this project.</p>
                    @endif
                    @include('partials.form.textarea', ['name' => 'comments', 'label' => 'Comments'])
                    <input type="hidden" name="type" value="{{ $type }}">
                    <p class="text-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </p>
                </form>
            </div>
        </div>
    @endif
@endsection

@include('partials.confirm_delete')
@include('partials.move_attachment', ['projects' => $project])