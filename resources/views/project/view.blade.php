@extends('layouts.app')

@section('breadcrumbs')
  @component('partials.breadcrumbs.project_base', ['project' => $project])
      <li class="active">{{ $project->board_id or 'New Project' }}</li>
  @endcomponent
@endsection

@section('title')
  {{ $confirm ? 'Apply for Funding' : ($project->funding_status_id == 1 ? 'Application' : 'Project') }}
@endsection

@section('content')
  @if ($confirm)
    @component('partials.alert.base', ['type' => 'warning'])
      <p>You're almost finished! Your application has been saved and contains all the required information for final submission, <strong>but you must still submit the final version</strong>. To submit and lock your application, click the "Submit" button at the bottom of the page.</p>
    @endcomponent
  @elseif ($approve)
    @component('partials.alert.base', ['type' => 'warning'])
      @if ($project->signoff)
        <p>The project has already been approved.</p>
      @else
        <p>Clicking the "Approve" button will result in immediate and final approval. Please be sure you have fully reviewed the proposal below first. After approval, you and the applicant will receive a confirmation email.</p>
        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('projects.approve', routeParams(['id' => $project->id])) }}">
          {{ method_field('PUT') }}
          {{ csrf_field() }}
          <p class="text-right">
            <button type="submit" class="btn btn-primary" value="0">Approve</button>
          </p>
        </form>
      @endif
    @endcomponent
  @else
    <h3>{{ $project->project_name }}</h3>
    <h4>{{ $project->first_name }} {{ $project->last_name }}</h4>
    <p>Board Assigned ID: {{ $project->board_id }}</p>
    <p><a href="{{ $project->url }}">Back to Summary</a></p>
  @endif

  @component('partials.project.main', ['summary' => false])
    @include('partials.project.contact')
    @slot('project_information_left')
      <dl class="dl-horizontal">
        <dt>Project Name</dt>
        <dd>{{ $project->project_name }}</dd>
        <dt>Fiscal Year</dt>
        <dd>{{ $project->fiscal_year }}</dd>
        <dt>Is this a new project?</dt>
        <dd>{{ formatBoolean($project->new_project) }}</dd>
        <dt>If no, has the {{ site()->name }} funded this request in the past?</dt>
        <dd>{{ formatBoolean($project->past_funding) }}</dd>
        <dt>Would a {{ site()->name }} allocation be the sole funding for this initiative?</dt>
        <dd>{{ formatBoolean($project->sole_funding) }}</dd>
      </dl>
    @endslot
    @slot('project_information_right')
      <dl class="dl-horizontal">
        <dt>Duration</dt>
        <dd>{{ $project->duration }} year(s)</dd>
        <dt>Multi-year justification</dt>
        <dd>{{ $project->justification }}</dd>
        <dt>Total Project Funding Requested</dt>
        <dd>${{ number_format($project->totalFunding->requested_amount) }}</dd>
        @foreach ($project->allFundings as $funding)
          <dt>Requested Funding - Year {{ $funding->year }}</dt>
          <dd>${{ number_format($funding->requested_amount) }}</dd>
        @endforeach
      </dl>
    @endslot
    @slot('project_information_bottom')
      @if ($project->undergraduateInitiatives->count() > 0)
        <p>Undergraduate initiatives identified in the annual Survey Results this project would support:</p>
        <ol>
          @foreach ($project->undergraduateInitiatives->unique() as $initiative)
            <li>{{ $initiative->name }}</li>
          @endforeach
        </ol>
      @endif

      @if ($project->graduateInitiatives->count() > 0)
        <p>Graduate initiatives identified in the annual Survey Results this project would support:</p>
        <ol>
          @foreach ($project->graduateInitiatives->unique() as $initiative)
            <li>{{ $initiative->name }}</li>
          @endforeach
        </ol>
      @endif
    @endslot
    @slot('supporting_documents_top')
    @endslot
    @slot('supporting_documents_1')
      @if (isset($project->fileBudgetURL))
        <p><a href="{{ $project->fileBudgetURL }}">Budget</a></p>
      @else
        <p>No budget attached</p>
      @endif
    @endslot
    @slot('supporting_documents_2')
      @if (isset($project->fileDescriptionURL))
        <p><a href="{{ $project->fileDescriptionURL }}">Description</a></p>
      @else
        <p>No description attached</p>
      @endif
    @endslot
    @slot('supporting_documents_3')
      @if (isset($project->fileExtraURL))
        <p><a href="{{ $project->fileExtraURL }}">Additional Information</a></p>
      @else
        <p>No additional information addatched</p>
      @endif
    @endslot
    @slot('terms')
      <p><strong>I [{{ $project->agree_terms ? 'agree' : 'do not agree'}}] to the terms and conditions and wish to have my application considered for funding.</strong></p>
    @endslot
  @endcomponent
  @if ($confirm)
    <p class="text-right">
      <a href="{{ route('projects.edit', routeParams(['id' => $project->id])) }}" class="btn btn-default" name="save_draft" value="1">Edit</a>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalConfirmSave">Submit Final Version</button>
    </p>

    <div class="modal fade" id="modalConfirmSave">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="ua-brand-x"></i></button>
            <h4 class="modal-title">Submit Application?</h4>
          </div>
          <div class="modal-body">
            <p>You are about to <strong>SUBMIT AND LOCK</strong> your final application. If you continue with this process, you will not be able to return to make future edits. Are you sure you want to continue?</p>
          </div>
          <div class="modal-footer">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('projects.save', routeParams(['id' => $project->id])) }}">
              {{ method_field('PUT') }}
              {{ csrf_field() }}
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary" value="0">Yes</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  @endif
@endsection
