@extends('layouts.app')
@section('breadcrumbs')
  @if ($admin_edit)
    @component('partials.breadcrumbs.project_base', ['project' => $project])
        <li class="active">{{ $project->board_id or 'New Project' }}</li>
    @endcomponent
  @else
    @include('partials.breadcrumbs.create', ['title' => 'Apply for Funding'])
  @endif
@endsection

@section('title')
  @if ($admin_edit)
    Edit Project
  @else
    Apply for Funding
  @endif
@endsection

@section('content')
  @if (!$admin_edit)
    <div class="row">
        <div class="col-md-9">
            {!! site()->funding_app_instructions !!}
            @if (isset(site()->contact))
                <p>For questions concerning the application process, please contact <a href="mailto:{{ getNetidEmail(site()->contact->netid) }}">{{ site()->contact->full_name }}</a>.</p>
            @endif
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Application Deadline</h3>
                </div>
                <div class="panel-body">
                    <p>Funding applications are currently being accepted. Applications are due by <strong>{{ projectSet()->app_deadline_time }} on {{ projectSet()->app_deadline_date }}.</strong></p>
                </div>
            </div>
        </div>
    </div>
  @endif
  {{-- if $project is set, show form for editing, otherwise, show application --}}
  @if (isset($project))
    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('projects.update', routeParams(['id' => $project->id])) }}">
    {{ method_field('PUT') }}
  @else
    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('projects.store', baseRouteParams()) }}">
  @endif
    {{ csrf_field() }}
    {{-- show fields for power users to edit --}}
    @if ($admin_edit && userIsChair())
      <h3>{{ $project->project_name }}</h3>
      @if (userIsPower())
        <p><a data-toggle="collapse" href="#projectHistory" aria-expanded="false" aria-controls="projectHistory">View the project history</a></p>
        <div class="collapse" id="projectHistory">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Project History</h3>
            </div>
            <div class="panel-body">
              <div class="list-group">
                <div class="list-group-item">
                  <h4 class="list-group-item-heading">{{ date('F j, Y g:i a', strtotime($project->created_at)) }}</h4>
                    <p>
                      Project Created
                    </p>
                </div>
                @if ($project->revisionHistory->count() > 0)
                  @foreach ($history as $key => $history_date)
                    <div class="list-group-item">
                      <h4 class="list-group-item-heading">{{ $key }}</h4>
                      <p>
                        NetID: {{ $history_date['netid'] }} | 
                        Comments: {{ $history_date['comment'] or 'No comments.' }}
                      </p>
                      @if (!empty($history_date['changes']))
                        <ul>
                          @foreach ($history_date['changes'] as $change)
                            <li>{{ $change }}</li>
                          @endforeach
                        </ul>
                      @endif
                    </div>
                  @endforeach
                @endif
              </div>
            </div>
          </div>
        </div>
      @endif
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Board Information</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              @if (userIsAdmin())
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'board_id', 'label' => 'Board Assigned ID'])
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'netid', 'label' => 'NetID'])
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'start_year', 'label' => 'Start Year'])
                @include('partials.form.radio_binary', ['inline' => true, 'name' => 'admin', 'label' => 'Administrative Project'])
              @endif
              @include('partials.form.radio_binary', ['inline' => true, 'name' => 'archived', 'label' => 'Archived'])
              @include('partials.form.radio_binary', ['inline' => true, 'name' => 'public', 'label' => 'Public'])
              @include('partials.form.radio_binary', ['inline' => true, 'name' => 'locked', 'label' => 'Locked'])
            </div>
            <div class="col-md-6">
              @component('partials.form.field', ['inline' => true, 'name' => 'board_comments', 'label' => 'Comments'])
                <textarea name="board_comments" class="form-control" rows="8">{{ old('board_comments') }}</textarea>
              @endcomponent
              @if (userIsPower())
                @component('partials.form.field', ['inline' => true, 'name' => 'fiscal_override', 'label' => ''])
                  @include('partials.form.checkbox', ['name' => 'fiscal_override', 'value' => 1, 'label' => 'Override Fiscal Officer Requirement'])
                @endcomponent
              @endif
            </div>
          </div>
        </div>
      </div>
      @if (userIsAdmin())
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Funding Information</h3>
          </div>
          <div class="panel-body">
            @include('partials.project.funding', ['edit' => true])
            @include('partials.form.checkbox', ['name' => 'permanent_funding', 'value' => 1, 'label' => 'Permanently Funded'])
          </div>
        </div>
      @endif
    @endif
    @if($creator_access || userIsAdmin())
      @component('partials.project.main', ['summary' => false, 'admin_edit' => $admin_edit])
        @slot('contact_information_left')
          <h4>Project Manager</h4>
          @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'first_name', 'label' => 'First Name'])
          @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'last_name', 'label' => 'Last Name'])
          @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'email', 'label' => 'Email'])
          @if (site()->departments->count() > 0)
            @component('partials.form.field', ['inline' => true, 'name' => 'department_id', 'label' => 'Department'])
              <select name="department_id" class="form-control" v-model="department" v-init:department="{{ null !== old('department_id') ? old('department_id') : (isset($project->department->id) ? $project->department->id : '')}}">
                <option value="">Select...</option>
                @foreach (site()->departments as $key => $department)
                  <option value="{{ $department->id }}">{{ $department->name }}</option>
                @endforeach
              </select>
            @endcomponent
            @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'division', 'label' => 'Area/Unit'])
          @else
            @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'department_other', 'label' => 'Department'])
          @endif
        @endslot
        @slot('contact_information_right')
          <h4>Project Director</h4>
          @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'director_first_name', 'label' => 'First Name'])
          @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'director_last_name', 'label' => 'Last Name'])
          @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'director_email', 'label' => 'Email'])
          <h4>Fiscal Officer</h4>
          @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'fiscal_first_name', 'label' => 'First Name'])
          @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'fiscal_last_name', 'label' => 'Last Name'])
          @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'fiscal_email', 'label' => 'Email'])
        @endslot
        @slot('project_information_left')
          @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'project_name', 'label' => 'Project Name'])
          @component('partials.form.field', ['inline' => true, 'name' => 'duration', 'label' => 'Project Duration'])
            <select name="duration" class="form-control" v-model="duration" v-init:duration="{{ null !== old('duration') ? old('duration') : (isset($project) ? $project->duration : '') }}">
              <option value="">Select...</option>
              <option value="1">1 Year</option>
              @if(site()->name != 'Freshman Fee')
              <option value="2">2 Years</option>
              <option value="3">3 Years</option>
              @endif
            </select>
          @endcomponent
          @include('partials.form.radio_binary', ['model' => true, 'inline' => true, 'name' => 'new_project', 'label' => 'Is this a new project?'])
          <transition name="fade">
            @include('partials.form.radio_binary', ['v_if' => 'new_project == 0', 'inline' => true, 'name' => 'past_funding', 'label' => 'Has the ' . site()->name . ' funded this request in the past?'])
          </transition>
          @include('partials.form.radio_binary', ['inline' => true, 'name' => 'sole_funding', 'label' => 'Would a ' . site()->name . ' allocation be the sole funding for this initiative?'])
          <p class="help-block">If no, please include ALL (potential and secured) funding sources in your description and itemized budget.</p>
        @endslot
        @slot('project_information_right')
          @if (!$admin_edit)
            <div v-if="duration <= 1 || duration == null">
              @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'amounts', 'name_other' => 'requested_amount', 'id' => '1', 'label' => 'Amount Requested', 'object' => isset($object) ? $object->allFundings->first() : null])
            </div>
          @endif
          <transition name="fade">
            <div v-if="duration > 1">
              @if (!$admin_edit)
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'amounts', 'name_other' => 'requested_amount', 'id' => '1', 'label' => 'Amount - Year 1', 'object' => isset($object) ? $object->allFundings->first() : null])
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'amounts', 'name_other' => 'requested_amount', 'id' => '2', 'label' => 'Amount - Year 2', 'object' => isset($object) ? $object->allFundings->get(1) : null])
                <div v-if="duration > 2">
                  @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'amounts', 'name_other' => 'requested_amount', 'id' => '3', 'label' => 'Amount - Year 3', 'object' => isset($object) ? $object->allFundings->get(2) : null])
                </div>
              @endif
              @component('partials.form.field', ['inline' => true, 'name' => 'justification', 'label' => 'Please provide justification (min. 25 char)'])
                <textarea name="justification" class="form-control" rows="5">{{ null !== old('justification') ? old('justification') : (isset($project) ? $project->justification : '') }}</textarea>
              @endcomponent
            </div>
          </transition>
        @endslot
        @slot('project_information_bottom')
          @if($undergraduate_initiatives->count() > 0 && $graduate_initiatives->count() > 0)
            <br>
            <h4>Initiatives Identified in the Student Services Fee Survey</h4>
            <p><a href="#">View all initiatives and responses</a> (pdf)</p>
            <br>
            <h4>Top 10 Undergraduate Initiatives</h4>
            <p>If applicable, check below which of the undergraduate initiatives identified in the annual Survey Results this project would support?</p>
            @php
              $undergraduate_old = '';
              $graduate_old = '';
              if (isset($project)) {
                $undergraduate_old = implode(',', $project->undergraduateInitiatives->pluck('id')->toArray());
                $graduate_old = implode(',', $project->graduateInitiatives->pluck('id')->toArray());
              }
              else {
                $undergraduate_old = is_array(old('undergraduate_initiatives')) ? implode(',', old('undergraduate_initiatives')) : '';
                $graduate_old = is_array(old('graduate_initiatives')) ? implode(',', old('graduate_initiatives')) : '';
              }
            @endphp
            <div v-init:undergraduate_initiatives="[{{ $undergraduate_old }}]">
              @foreach ($undergraduate_initiatives as $initiative)
                @include('partials.form.checkbox', ['model' => 'undergraduate_initiatives', 'name' => 'undergraduate_initiatives[]', 'value' => $initiative->id, 'label' => $initiative->name])
              @endforeach
            </div>
            <br>
            <h4>Top 10 Graduate Initiatives</h4>
            <p>If applicable, check below which of the graduate initiatives identified in the annual Survey Results this project would support?</p>
            <div v-init:graduate_initiatives="[{{ $graduate_old }}]">
              @foreach ($graduate_initiatives as $initiative)
                @include('partials.form.checkbox', ['model' => 'graduate_initiatives', 'name' => 'graduate_initiatives[]', 'value' => $initiative->id, 'label' => $initiative->name])
              @endforeach
            </div>
          @endif
        @endslot
        @slot('supporting_documents_top')
          <p>Please keep these documents concise so that we may consider them thoroughly and efficiently. Thank you!</p>
        @endslot
        @slot('supporting_documents_1')
          <p>
            The spreadsheet contains 2 tabs/worksheets. One worksheet contains instructions and the other is the budget that needs to be completed for the proposal. Once you have filled in the spreadsheet with your information, save it to your computer (you may rename the file) and then attach it to the proposal. 
            @if (!empty(projectSet()->budget_template_url))
              Download the spreadsheet <a href="{{ projectSet()->budget_template_url }}">here</a>. 
            @endif
            @if (isset(site()->contact))
              For questions about this spreadsheet, contact <a href="mailto:{{ getNetidEmail(site()->contact->netid) }}">{{ site()->contact->full_name }}</a>.
            @endif
          </p>
          @if (isset($project->fileBudgetURL))
            <p>
              <a href="{{ $project->fileBudgetURL }}">Your budget is attached</a>
              @if ($admin_edit && userIsPower())
                | <a href="#"
                      data-toggle="modal"
                      data-target="#modalDelete"
                      data-title="this project's budget"
                      data-action="{{ route('projects.files.destroy', routeParams(['id' => $project->fileBudget->id])) }}"
                      >Delete</a>
              @endif
            </p>
          @endif
          @component('partials.form.field', ['inline' => false, 'name' => 'budget', 'label' => 'Excel only (1.5MB or less)'])
            <input type="file" name="budget">
          @endcomponent
        @endslot
        @slot('supporting_documents_2')
          <ul>
            <li>How does this project support the Office of the Provost mission?</li>
            <li>What is the reason for creating/maintaining this program?</li>
            <li>How many students will be directly impacted?</li>
            <li>How the success of this initiative will be assessed?</li>
            <li>How are you ensuring the longevity of this program?</li>
          </ul>
          @if (isset($project->fileDescriptionURL))
            <p>
              <a href="{{ $project->fileDescriptionURL }}">Your description is attached</a>
              @if ($admin_edit && userIsPower())
                | <a href="#"
                      data-toggle="modal"
                      data-target="#modalDelete"
                      data-title="this project's description"
                      data-action="{{ route('projects.files.destroy', routeParams(['id' => $project->fileDescription->id])) }}"
                      >Delete</a>
              @endif
            </p>
          @endif
          @component('partials.form.field', ['inline' => false, 'name' => 'description', 'label' => 'PDF only (1.5MB or less)'])
            <input type="file" name="description">
          @endcomponent
        @endslot
        @slot('supporting_documents_3')
          @if (isset($project->fileExtraURL))
            <p>
              <a href="{{ $project->fileExtraURL }}">Your additional information is attached</a>
              @if ($admin_edit && userIsPower())
                | <a href="#"
                      data-toggle="modal"
                      data-target="#modalDelete"
                      data-title="this project's additional information"
                      data-action="{{ route('projects.files.destroy', routeParams(['id' => $project->fileExtra->id])) }}"
                      >Delete</a>
              @endif
            </p>
          @endif
          @component('partials.form.field', ['inline' => false, 'name' => 'additional_information', 'label' => 'PDF only (1.5MB or less)'])
            <input type="file" name="additional_information">
          @endcomponent
        @endslot
        @slot('terms')
          @include('partials.form.checkbox', ['name' => 'agree_terms', 'value' => 1, 'label' => 'I agree to the terms and conditions and wish to have my application considered for funding.'])
        @endslot
      @endcomponent
    @endif
    <p class="text-right">
      @if ($admin_edit)
        <a href="{{ route('projects.admin.index', baseRouteParams()) }}" class="btn btn-default">Cancel</a>
        <button type="submit" class="btn btn-primary" name="admin_edit" value="1">Save</button>
      @else
        <button type="submit" class="btn btn-default" name="save_draft" value="1">Save for Later</button>
        <button type="submit" class="btn btn-primary" name="save_draft" value="0">Submit</button>
      @endif
    </p>
  </form>
@endsection

@include('partials.confirm_delete')
