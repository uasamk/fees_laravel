@extends('layouts.app')

@section('breadcrumbs')
  @component('partials.breadcrumbs.project_base', ['project' => $project])
        <li class="active">{{ $project->board_id or 'New Project' }}</li>
  @endcomponent
@endsection

@section('title')
    Project Summary
@endsection

@section('content')
    @if (session('success'))
        @component('partials.alert.base', ['type' => 'success'])
            <p>Your application is now closed and locked. You will receive an email confirmation shortly.</p>
            <p>Thank you for your application, and your interest in the {{ site()->name }}.</p>
        @endcomponent
    @endif
    <h3>{{ $project->project_name }}</h3>
    <h4>{{ $project->first_name }} {{ $project->last_name }}</h4>
    <p>
        Board Assigned ID: {{ $project->board_id }}
        <br>
        Project Created: {{ date('F j, Y g:i a', strtotime($project->created_at)) }}
    </p>
    @if(getActiveProjects()->where('id', $project->id)->count() > 0)
        <p><a href="{{ route('progress.create', routeParams(['project' => $project->id])) }}">Submit a Progress Report</a> | <a href="{{ route('pars.create', routeParams(['project' => $project->id])) }}">Submit a PAR</a></p>
    @endif

    @component('partials.project.main', ['summary' => true])
        @slot('project_information_left')
            <dl class="dl-horizontal">
                <dt>Project Name</dt>
                <dd>{{ $project->project_name }}</dd>
                <dt>Fiscal Year</dt>
                <dd>{{ $project->fiscal_year }}</dd>
            </dl>
        @endslot
        @slot('project_information_right')
        @endslot
        @slot('project_information_bottom')
            <h4>Funding</h4>
            @include('partials.project.funding', ['edit' => false])
        @endslot
        @slot('supporting_documents_top')
            <div class="row">
                <div class="col-md-6">
                    <h4>Project Plan</h4>
                    <ol>
                        <li>
                            @if (isset($project->fileDescriptionURL))
                                <a href="{{ $project->fileDescriptionURL }}">Original Project Description</a>
                            @else
                                No description attached
                            @endif
                        </li>
                        <li>
                            @if (isset($project->fileExtraURL))
                                <a href="{{ $project->fileExtraURL }}">Additional Information submitted with Project Application</a>
                            @else
                                No additional information attached
                            @endif
                        </li>
                        {{-- @if ($project->funding_status_id != 3) --}}
                            <li><a href="{{ route('projects.revisions', routeParams(['id' => $project->id])) }}">Revisions to Original Project Plan</a></li>
                        {{-- @endif --}}
                    </ol>
                    <h4>Project Budget</h4>
                    <ol>
                        <li>
                            @if (isset($project->fileBudgetURL))
                                <a href="{{ $project->fileBudgetURL }}">Original Project Budget Requested</a>
                            @else
                                No budget attached
                            @endif
                        </li>
                        <li>
                            @if (isset($project->currentYearBudgetURL))
                                <a href="{{ $project->currentYearBudgetURL }}">Current Year Approved Budget</a>
                            @else
                                No budget for current year
                            @endif
                        </li>
                        <li><a href="{{ route('projects.budgets', routeParams(['id' => $project->id])) }}">Budget History</a></li>
                    </ol>
                </div>
                <div class="col-md-6">
                    <h4>Other Documents</h4>
                    <ol>
                        <li><a href="{{ route('projects.notifications', routeParams(['id' => $project->id])) }}">Funding Notifications</a></li>
                        <li><a href="{{ route('progress.index', routeParams(['project' => $project->id])) }}">Progress Reports</a></li>
                        <li><a href="{{ route('pars.index', routeParams(['project' => $project->id])) }}">Program Alteration Requests (PARs)</a></li>
                        <li><a href="{{ route('projects.questions', routeParams(['id' => $project->id])) }}">Board Questions and Applicant Answers</a></li>
                        @if (Auth::check())
                            <li><a href="{{ route('projects.comments', routeParams(['id' => $project->id])) }}">Comments</a></li>
                        @endif
                    </ol>
                </div>
            </div>
            <p><a href="{{ route('projects.full', routeParams(['id' => $project->id])) }}">View Original Project Application</a></p>
        @endslot
        @include('partials.project.contact', ['confirm' => false])
    @endcomponent
@endsection