@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.base')
        <li class="active">Answer Funding Questions</li>
    @endcomponent
@endsection

@section('title')
  Answer Funding Questions
@endsection

@section('content')
    <p>Welcome <strong>{{ $question->project->first_name }}</strong>! You are responding to the following question about the <strong>{{ $title }}</strong> you submitted for the <strong>{{ $question->project->project_name }}</strong> initiative.</p>

    <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('answers.store', ['site' => site()->slug]) }}">
        {{ csrf_field() }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Question &amp; Answer</h3>
            </div>
            <div class="panel-body">
                <h4>Question</h4>
                <div class="well">{!! nl2br($question->question_text) !!}</div>

                <h4>Answer</h4>
                <p><strong>Enter your answer in the following field (limited to 1500 words):</strong> NOTE: For best results, please enter plain text. Pasting preformatted Word content may yield unpredictable results.</p>
                @include('partials.form.textarea', ['name' => 'answer_text', 'label' => ''])
            </div>
            <div class="panel-footer text-right">
                <input type="hidden" name="question_id" value="{{ $question->id }}">
                <button type="submit" class="btn btn-primary">Submit Answer</button>
            </div>
        </div>
    </form>
@endsection