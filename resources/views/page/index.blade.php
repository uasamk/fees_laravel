@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.default')
        <li class="active">Manage Content</li>
    @endcomponent
@endsection

@section('title')
    Manage Content
@endsection

@section('content')
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('pages.order', ['site' => site()->slug]) }}">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingPages">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            Pages
                        </a>
                    </h4>
                </div>
                <div id="collapsePages" class="panel-collapse collapse{{ null === app('request')->input('page') ? ' in' : '' }}" role="tabpanel" aria-labelledby="headingPages">
                    <table class="table-dark table-hover table-striped table-condensed">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Order</th>
                                <th>Title</th>
                                <th>Parent</th>
                                <th>Published</th>
                                <th>Default</th>
                                <th style="width: 20px"></th>
                                <th style="width: 20px"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach (site()->pages->sortBy('id')->sortBy('list_order') as $page)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><input type="number" step="1" min="0" required name="list_order[{{ $page->id }}]" value="{{ $page->list_order }}"{{ $page->default ? ' disabled' : '' }}></td>
                                <td><a href="{{ $page->url }}">{{ $page->title }}</a></td>
                                <td>{{ $page->parent->title or 'N/A' }}</td>
                                <td>{{ formatBoolean($page->published) }}</td>
                                <td>{{ formatBoolean($page->default) }}</td>
                                <td><a class="text-blue30w" href="{{ route('pages.edit', [site()->slug, 'id' => $page->id]) }}" title="Edit Page"><span class="glyphicon glyphicon-pencil"></a></td>
                                <td>
                                    @if (!$page->default)
                                        <a href="#" class="text-blue30w" title="Delete Page"
                                            data-toggle="modal"
                                            data-target="#modalDelete"
                                            data-title="{{ $page->title }}"
                                            data-action="{{ route('pages.destroy', [site()->slug, 'id' => $page->id]) }}"
                                            ><span class="glyphicon glyphicon-trash"></a>
                                    @else
                                        <span class="glyphicon glyphicon-trash">
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="panel-footer text-right">
                        <button type="submit" class="btn btn-default">Update Order</button>
                        <a href="{{ route('pages.create', ['site' => site()->slug]) }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Create Page</a>
                    </div>
                </div>
            </div>   
        </form>

        <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('sites.files.save', ['id' => site()->id]) }}">
            {{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingUploads">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseUploads" aria-expanded="true" aria-controls="collapseUploads">
                            Uploads
                        </a>
                    </h4>
                </div>
                <div id="collapseUploads" class="panel-collapse collapse{{ null !== app('request')->input('page') ? ' in' : '' }}" role="tabpanel" aria-labelledby="headingUploads">
                    <div class="panel-body">
                        Upload files to be inserted into pages.
                    </div>
                    <table class="table-dark table-hover table-striped table-condensed">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>File Name</th>
                                <th style="width: 20px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($files as $file)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><a href="{{ Storage::url($file->name) }}">{{ $file->title }}</a>
                                    <td>
                                        <a href="#" class="text-blue30w" title="Delete File"
                                            data-toggle="modal"
                                            data-target="#modalDelete"
                                            data-title="{{ $file->title }}"
                                            data-action="{{ route('sites.files.destroy', ['id' => site()->id, 'file_id' => $file->id]) }}"
                                            ><span class="glyphicon glyphicon-trash"></a>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td><input type="file" name="uploads[]" multiple=""></td>
                                <td><button type="submit" class="btn btn-xs btn-link text-blue30w"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></button></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="panel-footer">
                        {{ $files->links() }}
                    </div>
                </div>
            </div>
        </form>

        <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('sites.departments.save', ['id' => site()->id]) }}">
            {{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingDepartments">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDepartments" aria-expanded="true" aria-controls="collapseDepartments">
                            Departments
                        </a>
                    </h4>
                </div>
                <div id="collapseDepartments" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDepartments">
                    <div class="panel-body">
                        Add/Remove departments available on the application page.
                    </div>
                    <table class="table-dark table-hover table-striped table-condensed">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Department</th>
                                <th style="width: 20px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (site()->departments as $department)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $department->name }}</a>
                                    <td>
                                        <a href="#" class="text-blue30w" title="Delete Department"
                                            data-toggle="modal"
                                            data-target="#modalDelete"
                                            data-title="{{ $department->name }}"
                                            data-action="{{ route('sites.departments.destroy', ['id' => site()->id, 'dept_id' => $department->id]) }}"
                                            ><span class="glyphicon glyphicon-trash"></a>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td><input type="text" name="department" placeholder="Department"></td>
                                <td><button type="submit" class="btn btn-xs btn-link text-blue30w"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>

        <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('sites.initiatives.save', ['id' => site()->id]) }}">
            {{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingInitiatives">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseInitiatives" aria-expanded="true" aria-controls="collapseInitiatives">
                            Initiatives
                        </a>
                    </h4>
                </div>
                <div id="collapseInitiatives" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingInitiatives">
                    <div class="panel-body">
                        Add/Remove initiatives available on the application page. Set the order to 0 to hide the initiative from that category.
                    </div>
                    <table class="table-dark table-hover table-striped table-condensed">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Initiative</th>
                                <th>Undergraduate Order</th>
                                <th>Graduate Order</th>
                                <th style="width: 20px"></th>
                                <th style="width: 20px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (site()->initiatives as $initiative)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $initiative->name }}</a>
                                    <td>{{ $initiative->undergraduate_order ? $initiative->undergraduate_order : 'Hidden'}}</td>
                                    <td>{{ $initiative->graduate_order ? $initiative->graduate_order : 'Hidden' }}</td>
                                    <td>
                                        <a href="#" class="text-blue30w" title="Edit Initiative"
                                            data-toggle="modal"
                                            data-target="#modalInitiative"
                                            data-initiative="{{ $initiative }}"
                                            data-action="{{ route('sites.initiatives.update', ['id' => site()->id, 'dept_id' => $initiative->id]) }}"
                                            ><span class="glyphicon glyphicon-pencil"></a>
                                    </td>
                                    <td>
                                        <a href="#" class="text-blue30w" title="Delete Initiative"
                                            data-toggle="modal"
                                            data-target="#modalDelete"
                                            data-title="{{ $initiative->name }}"
                                            data-action="{{ route('sites.initiatives.destroy', ['id' => site()->id, 'dept_id' => $initiative->id]) }}"
                                            ><span class="glyphicon glyphicon-trash"></a>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td><input type="text" name="initiative" placeholder="Initiative" style="width:100%"></td>
                                <td><input type="number" step="1" min="0" required name="undergraduate_order"></td>
                                <td><input type="number" step="1" min="0" required name="graduate_order"></td>
                                <td></td>
                                <td><button type="submit" class="btn btn-xs btn-link text-blue30w"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
@endsection

<div class="modal fade" id="modalInitiative">
    <div class="modal-dialog">
        <form id="editInitiative" style="display:inline" role="form" method="POST">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="ua-brand-x"></i></button>
                    <h4 class="modal-title">Edit Initiative</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="initiative" id="initName">
                    </div>
                    <div class="form-group">
                        <label for="name">Undergraduate Order</label>
                        <input type="number" step="1" min="0" class="form-control" name="undergraduate_order" id="undergradOrder">
                    </div>
                    <div class="form-group">
                        <label for="name">Graduate Order</label>
                        <input type="number" step="1" min="0" class="form-control" name="graduate_order" id="gradOrder">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" value="0">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

@include('partials.confirm_delete')