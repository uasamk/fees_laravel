@extends('layouts.app')

@section('breadcrumbs')
  @foreach ($page->breadcrumbs as $key => $breadcrumb)
    @if ($loop->last)
      <li class="active">{{ $breadcrumb[1] }}</li>
    @else
      <li><a href="{{ $breadcrumb[0] }}">{{ $breadcrumb[1] }}</a></li>
    @endif
  @endforeach
@endsection

@section('title')
  {{ $page->default ? site()->name : $page->title }}
@endsection

@section('content')
  @if ($page->default || !empty($page->info))
    <div class="row">
      <div class="col-md-9">
        {!! $page->body !!}
      </div>
      <div class="col-md-3">
        @if ($page->default)
          @if (site()->projectSets->count() > 0)
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Apply For Funding</h3>
              </div>
              <div class="panel-body">
                @if (fundingAppClosed())
                  @if (projectSet()->slug === 'annual-grants')
                    <p>Annual Grant Project applications are not being accepted at this time. Please check back {{ appReturnDate(projectSet()->application_open) }}.</p>
                  @elseif (projectSet()->slug === 'mini-grants')
                    <p>Mini Grant Project applications are not being accepted at this time. Please check back {{ appReturnDate(projectSet()->application_open) }}.</p>
                  @else             
                    Projects applications are not being accepted at this time. Please check back {{ appReturnDate(projectSet()->application_open) }}.
                  @endif
                @else
                  @if (site()->slug === 'ua-green-fund')
                    @if (projectSet()->slug === 'annual-grants')
                      <p>Funding applications are currently being accepted for Annual Grants.</p>
                      <a class="btn btn-default" href="{{ route('projects.start', baseRouteParams()) }}">Apply Here</a>
                    @endif
                    @if (projectSet()->slug === 'mini-grants')
                      <p>Funding applications are currently being accepted for Mini Grants.</p>
                      <a class="btn btn-default" href="{{ route('projects.start', routeParams(['site' => site()->slug, 'project_set' => 'mini-grants'])) }}">Apply Here</a>
                    @endif                  
                  @else
                    <p>Funding applications are currently being accepted.</p>
                    <a class="btn btn-default" href="{{ route('projects.start', baseRouteParams()) }}">Apply Now</a>
                  @endif
                @endif
              </div>
            </div>
          @endif
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Apply To Be A Board Member</h3>
            </div>
            <div class="panel-body">
              @if (boardAppClosed())
                Board Membership applications are not being accepted at this time. Please check back {{ appReturnDate(site()->board_application_open) }}.
              @else
                <p>Board applications are currently being accepted.</p>
                <a class="btn btn-default" href="{{ route('users.apply', ['site' => site()->slug]) }}">Apply Now</a>
              @endif
            </div>
          </div>
        @endif
        @if (!empty($page->info))
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Important Information</h3>
            </div>
            <div class="panel-body">
              {!! $page->info !!}
            </div>
          </div>
        @endif
      </div>
    </div>
  @else
    {!! $page->body !!}
  @endif
@endsection
