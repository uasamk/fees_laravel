@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.default')
        <li><a href="{{ route('pages.index', ['site' => site()->slug ])}}">Manage Content</a></li>
        <li class="active">{{ isset($page) ? 'Edit' : 'Create' }} Page</li>
    @endcomponent
@endsection

@section('title')
    @if (isset($page))
        Editing {{ $page->title }}
    @else  
        Create Page
    @endif
@endsection

@section('content')
    @if (isset($page))
        <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('pages.update', [site()->slug, 'id' => $page->id]) }}">
        {{ method_field('PUT') }}
    @else
        <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('pages.store', ['site' => site()->slug]) }}">
    @endif
        {{ csrf_field() }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Page Details</h3>
            </div>
            <div class="panel-body">
                @include('partials.form.text', ['inline' => false, 'type' => 'text', 'name' => 'title', 'label' => 'Title', 'object' => isset($page) ? $page : null])
                @component('partials.form.field', ['inline' => false, 'name' => 'parent_id', 'label' => 'Parent'])
                    <select name="parent_id" class="form-control"{{ (isset($page) && $page->default) ? ' disabled' : ''}}>
                        <option value="0">Select...</option>
                        @foreach (site()->pages as $parent_page)
                            <option value="{{ $parent_page->id }}"{{ null !== old('parent_id') ? (old('parent_id') == $parent_page->id ? ' selected' : '') : (isset($page) && $page->parent_id == $parent_page->id ? ' selected' : '') }}>{{ $parent_page->title }}</option>
                        @endforeach
                    </select>
                @endcomponent
                @include('partials.form.checkbox', ['name' => 'published', 'value' => 1, 'label' => 'Published', 'object' => (isset($page) ? $page : null), 'disabled' => isset($page) && $page->default])
                @include('partials.form.checkbox', ['name' => 'board_home', 'value' => 1, 'label' => 'Board Home Page', 'object' => (isset($page) ? $page : null), 'disabled' => isset($page) && $page->default])
                <p class="help-block">If 'Board Home Page' is checked, the Board Directory page will show as a child of this page. The Board Home can not be a child of another page. Also, the main home page can not be set to the Board Home page. Only one page can be set as Board Home.</p>
                @component('partials.form.field', ['inline' => false, 'name' => 'body', 'label' => 'Body'])
                    <textarea name="body" class="summernote">
                        {!! null !== old('body') ? old('body') : (isset($page) ? $page->body : '') !!}
                    </textarea>
                @endcomponent
                @component('partials.form.field', ['inline' => false, 'name' => 'info', 'label' => 'Extra Information'])
                    <p>This will be displayed in the 'Important Information' section in the side bar.</p>
                    <textarea name="info" class="summernote">
                        {!! null !== old('info') ? old('info') : (isset($page) ? $page->info : '') !!}
                    </textarea>
                @endcomponent
            </div>
            <div class="panel-footer text-right">
                <a href="{{ route('pages.index', ['site' => site()->slug]) }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection

<div class="modal fade" id="modalFile">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="ua-brand-x"></i></button>
                <h4 class="modal-title">Insert Page/File Link</h4>
            </div>
            <div class="modal-body">
                @component('partials.form.field', ['inline' => false, 'name' => 'insert', 'label' => 'Which would you like to insert?'])
                    <label class="radio-inline">
                        <input name="radio-insert" class="radio-insert" type="radio" value="page" checked /> Page
                    </label>
                    <label class="radio-inline">
                        <input name="radio-insert" class="radio-insert" type="radio" value="file" /> File
                    </label>
                @endcomponent
                <div class="insert-select insert-page">
                    @component('partials.form.field', ['inline' => false, 'name' => 'page', 'label' => 'Page To Insert'])
                        <select name="page" class="form-control page-select">
                            <option value="0">Select...</option>
                            @foreach ($pages_app as $page_app)
                                <option value="{{ parse_url($page_app[0])['path'] }}">{{ $page_app[1] }}</option>
                            @endforeach
                            @foreach (site()->pages as $page)
                                <option value="{{ parse_url($page->urlid)['path'] }}">{{ $page->title }}</option>
                            @endforeach
                        </select>
                    @endcomponent
                </div>
                <div class="insert-select insert-file" style="display:none">
                    @component('partials.form.field', ['inline' => false, 'name' => 'file', 'label' => 'File To Insert'])
                        <select name="file" class="form-control file-select">
                            <option value="0">Select...</option>
                            @foreach (site()->uploads->sortByDesc('created_at') as $upload)
                                <option value="{{ Storage::url($upload->name) }}">{{ $upload->title }}</option>
                            @endforeach
                        </select>
                    @endcomponent
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary insert-item" name="save" value="0">Submit</button>
            </div>
        </div>
    </div>
</div>