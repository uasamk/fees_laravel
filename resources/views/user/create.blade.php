@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.default')
        <li class="active">{{ $object->netid or 'New User' }}</li>
    @endcomponent
@endsection

@section('title')
    @if (isset($object))
        Edit 
    @else
        Create 
    @endif
    Board Member
@endsection

@section('content')
    @if (isset($object))
        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('users.update', ['site' => site()->slug, 'id' => $object->id]) }}">
        {{ method_field('PUT') }}
    @else
        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('users.store', ['site' => site()->slug]) }}">
    @endif
        {{ csrf_field() }}
        @include('partials.user.contact-form')
        @include('partials.user.clubs-form')
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Board Information</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        @include('partials.form.select', ['model' => true, 'inline' => true, 'name' => 'user_title_id', 'label' => 'Title', 'items' => $user_titles, 'value' => 'id', 'text' => 'name'])
                        <div v-if="user_title_id == 8">
                            @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'title_other', 'label' => 'Title (Other)'])
                        </div>
                        @include('partials.form.select', ['inline' => true, 'name' => 'user_standing_id', 'label' => 'Class Standing', 'items' => $user_standings, 'value' => 'id', 'text' => 'name'])
                        @include('partials.form.select', ['inline' => true, 'name' => 'user_type_id', 'label' => 'Member Type', 'items' => $user_types, 'value' => 'id', 'text' => 'name'])
                        @include('partials.form.select', ['inline' => true, 'name' => 'user_status_id', 'label' => 'Status', 'items' => $user_statuses, 'value' => 'id', 'text' => 'name'])
                        @component('partials.form.field', ['inline' => true, 'name' => 'responsibilities', 'label' => 'Responsibilities'])
                            <textarea name="responsibilities" class="form-control" rows="3">{{ null !== old('responsibilities') ? old('responsibilities') : (isset($object) ? $object->responsibilities : '') }}</textarea>
                        @endcomponent
                    </div>
                    <div class="col-md-6">
                        @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'netid', 'label' => 'NetID'])
                        @include('partials.form.select', ['inline' => true, 'name' => 'role_id', 'label' => 'Access Level', 'items' => $roles, 'value' => 'id', 'text' => 'name'])
                        @component('partials.form.field', ['inline' => true, 'name' => 'start_year', 'label' => 'Start Year'])
                            <div class="input-group date datetimepicker" id="startYear">
                                <input name="start_year" type="text" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        @endcomponent
                        @component('partials.form.field', ['inline' => true, 'name' => 'fiscal_year', 'label' => 'Fiscal Year'])
                            <div class="input-group date datetimepicker" id="fiscalYear">
                                <input name="fiscal_year" type="text" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        @endcomponent
                        @component('partials.form.field', ['inline' => true, 'name' => '', 'label' => ''])
                            @include('partials.form.checkbox', ['name' => 'directory', 'value' => 1, 'label' => 'Display in Directory'])
                            @include('partials.form.checkbox', ['name' => 'par_contact', 'value' => 1, 'label' => 'Receive PAR Notifications', 'disabled' => $user_is_contact])
                            @include('partials.form.checkbox', ['name' => 'question_contact', 'value' => 1, 'label' => 'Receive Comments & Questions', 'disabled' => $user_is_contact])
                            @if ($user_is_contact)
                                <p class="help-block">The site contact will automatically receive PAR Board notifications and comments/questions</p>
                            @endif
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>  
        <p class="text-right">
            <a href="{{ route((isset($object) ? 'users.index' : 'admin'), ['site' => site()->slug]) }}" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-primary" name="admin_edit" value="1">Save</button>
        </p>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        var options = {
            format: 'YYYY',
            viewMode: 'years',
        };

        $('#startYear').data('DateTimePicker').options(options);
        $('#fiscalYear').data('DateTimePicker').options(options);

        window.setDatePickerDate('{{ (null !== old('start_year') ? old('start_year') : (isset($object) ? $object->start_year : '')) . '-01-01 00:00:00' }}', '#startYear');
        window.setDatePickerDate('{{ (null !== old('fiscal_year') ? old('fiscal_year') : (isset($object) ? $object->fiscal_year : '')) . '-01-01 00:00:00' }}', '#fiscalYear');
    </script>
@endsection