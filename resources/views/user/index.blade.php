@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.default')
        <li class="active">
            @if ($directory)
                Board Member Directory
            @else
                Edit Board Members
            @endif
        </li>
    @endcomponent
@endsection

@section('title')
    @if ($directory)
        Board Member Directory
    @else
        Edit Board Members
    @endif
@endsection

@section('content')
    @if($directory)
        <h3>Voting Members</h3>
    @else
        <h4>Which group of board members would you like to see?</h4>
        <form role="form" method="GET" action="{{ route('users.index', ['site' => site()->slug]) }}">
            <div class="row">
                <div class="col-md-3">
                    @include('partials.form.filter_select', ['session_group' => 'user-filters', 'name' => 'status', 'label' => 'User Status', 'items' => $user_statuses, 'value' => 'id', 'text' => 'name'])
                </div>
                <div class="col-md-3">
                    @include('partials.form.filter_select', ['session_group' => 'user-filters', 'name' => 'year', 'label' => 'Fiscal Year', 'items' => $years, 'value_as_key' => true])
                </div>
            </div>
        </form>
    @endif
    @include('partials.user.index', ['users' => $users, 'admin' => false])
    @if($directory)
        <h3>Administrative Members</h3>
        @include('partials.user.index', ['users' => $admin_users, 'admin' => true])
    @else
        <div class="text-right">
            <a href="#" data-toggle="modal" data-target="#modalUser" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Add Existing User</a>
        </div>
    @endif
@endsection

@include('partials.confirm_delete')

<div class="modal fade" id="modalUser">
    <div class="modal-dialog">
        <form id="editInitiative" style="display:inline" role="form" method="POST" action="{{ route('users.site.update', ['site' => site()->slug])}}">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="ua-brand-x"></i></button>
                    <h4 class="modal-title">Add Existing User</h4>
                </div>
                <div class="modal-body">
                    <p>Use the dropdown below to select an existing user from another site to be given access to the {{ site()->name }} site.</p>
                    <div class="form-group">
                        <select name="user_id" class="form-control">
                            <option value="">Select...</option>
                            @foreach ($outside_users as $user)
                                <option value="{{ $user->id }}">{{ $user->full_name }} ({{ $user->netid }}) -
                                    @if ($user->sites->count() > 0)
                                        @foreach ($user->sites as $site)
                                            {{ ($loop->index > 0 ? ', ' : '') . $site->name }}
                                        @endforeach
                                    @endif
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" value="0">Add</button>
                </div>
            </div>
        </form>
    </div>
</div>
