@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.base')
        <li class="active">Apply for Board Membership</li>
    @endcomponent
@endsection

@section('title')
    Apply for Board Membership
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            {!! site()->board_app_instructions !!}
            @if (isset(site()->contact))
                <p>For questions concerning the application process, please contact <a href="mailto:{{ getNetidEmail(site()->contact->netid) }}">{{ site()->contact->full_name }}</a>.</p>
            @endif
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Application Deadline</h3>
                </div>
                <div class="panel-body">
                    <p>Board applications are currently being accepted. Applications are due by <strong>{{ site()->board_app_deadline_time }} on {{ site()->board_app_deadline_date }}.</strong></p>
                </div>
            </div>
        </div>
    </div>

    @if (isset($object))
        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('users.apply.update', ['site' => site()->slug, 'id' => $object->id]) }}">
        {{ method_field('PUT') }}
    @else
        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('users.apply.store', ['site' => site()->slug]) }}">
    @endif
        {{ csrf_field() }}
        <input type="hidden" name="membership_type" value="{{site()->slug}}">
        @include('partials.user.contact-form', ['object' => isset($user) ? $user : null])
        @component('partials.user.apply')
            @slot('academic_information_left')
                @include('partials.form.select', ['inline' => true, 'name' => 'user_standing_id', 'label' => 'Class Standing', 'items' => $user_standings, 'value' => 'id', 'text' => 'name', 'object' => isset($user) ? $user : null])
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'major', 'label' => 'Declared Major'])
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'minor', 'label' => 'Declared Minor'])
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'gpa', 'label' => 'Current GPA'])
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'gpa_cumulative', 'label' => 'Cumulative GPA'])
            @endslot
            @slot('academic_information_right')
                @component('partials.form.field', ['inline' => true, 'name' => 'graduation_date', 'label' => 'Expected Graduation Date'])
                    <div class="input-group date datetimepicker" id="gradDate">
                        <input name="graduation_date" type="text" class="form-control" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                @endcomponent
                <p><strong>Number of credits you expect to take each upcoming semester:</strong></p>
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'credits_fall', 'label' => 'Fall'])
                @include('partials.form.text', ['inline' => true, 'type' => 'text', 'name' => 'credits_spring', 'label' => 'Spring'])
            @endslot
            @slot('time_commitments')
                @include('partials.form.radio_binary', ['model' => true, 'inline' => false, 'name' => 'current_jobs', 'label' => 'Do you currently hold any positions or jobs (paid or volunteer)?'])
                <transition name="fade">
                    <div v-if="current_jobs == 1">
                        @include('partials.form.text', ['inline' => false, 'type' => 'text', 'name' => 'job_hours', 'label' => 'How many hours per week?'])
                        @include('partials.form.textarea', ['name' => 'job_description', 'label' => 'Please describe the position(s)'])
                    </div>
                </transition>
            @endslot
            @slot('clubs_orgs')
                @include('partials.user.clubs-form', ['object' => isset($user) ? $user : null])
            @endslot
            @slot('qualifications')
                @include('partials.form.textarea', ['name' => 'board_interest', 'label' => '1. Please briefly explain why you are interested in serving on the Advisory Board.'])
                @include('partials.form.textarea', ['name' => 'board_activities', 'label' => '2. Please explain how any activities that you are involved in may be pertinent to serving as a member of the Advisory Board (ex. student organizations, community service, volunteer activities, job experience, etc.).'])
                @include('partials.form.textarea', ['name' => 'board_other', 'label' => '3. Please list any other information that might have a bearing on your qualification for participation in the Advisory Board (ex. honors, fellowships and scholarship, leadership experience, etc.)'])
            @endslot
            @slot('resume')
                @if (isset($object->fileResumeURL))
                    <p>
                        <a href="{{ $object->fileResumeURL }}">Your resume is attached</a>
                    </p>
                @endif
                @component('partials.form.field', ['inline' => false, 'name' => 'resume', 'label' => 'Word or PDF format (1MB or less)'])
                    <input type="file" name="resume">
                @endcomponent
            @endslot
            @slot('references_left')
                @include('partials.user.reference.create', ['id' => 1, 'object' => isset($object) ? $object->references->first() : null])
            @endslot
            @slot('references_right')
                @include('partials.user.reference.create', ['id' => 2, 'object' => isset($object) ? $object->references->last() : null])
            @endslot
            @slot('terms')
                @include('partials.form.checkbox', ['name' => 'agree_terms', 'value' => 1, 'label' => 'I agree to the terms and conditions and wish to be considered for board membership.'])
            @endslot
        @endcomponent
        <p class="text-right">
            <button type="submit" class="btn btn-primary">Submit</button>
        </p>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        var options = {
            format: 'MMMM, YYYY',
        };

        $('#gradDate').data('DateTimePicker').options(options);

        window.setDatePickerDate('{{ null !== old('graduation_date') ? formatDateDb(old('graduation_date')) : (isset($object) ? $object->graduation_date : '') }}', '#gradDate');
    </script>
@endsection
