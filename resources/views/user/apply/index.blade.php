@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.default')
        <li class="active">
            Review Board Applications
        </li>
    @endcomponent
@endsection

@section('title')
    Review Board Applications
@endsection

@section('content')
    <h4>Which group of applications would you like to see?</h4>
    <form role="form" method="GET" action="{{ route('users.apply.index', ['site' => site()->slug]) }}">
        <div class="row">
            <div class="col-md-3">
                @include('partials.form.filter_select', ['session_group' => 'board-app-filters', 'name' => 'status', 'label' => 'Application Status', 'items' => $applicant_statuses])
            </div>
            <div class="col-md-3">
                @include('partials.form.filter_select', ['session_group' => 'board-app-filters', 'name' => 'year', 'label' => 'Term', 'items' => $years, 'value_as_key' => true])
            </div>
        </div>
    </form>
    <div class="table-responsive">
        <table class="table-dark table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Date/Time Submitted</th>
                    <th>Status</th>
                    <th>Rejection Email</th>
                    @if(userIsPower())
                        <th></th>
                    @endif
                </tr>
            </thead>
            <tbody>
            @foreach ($applications as $application)
                @if($application->membership_type == site()->slug)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td><a href="{{ route('users.apply.view', ['site' => site()->slug, 'id' => $application->id]) }}">{{ $application->user->full_name }}</a></td>
                    <td>{{ formatDate($application->created_at) }}</td>
                    <td>{{ isset($application->accepted) ? ($application->accepted ? 'Accepted' : 'Rejected') : 'Pending Review' }}</td>
                    <td>{{ formatBoolean($application->reject_email) }}</td>
                    @if(userIsPower())
                        <td>
                            <a href="#" class="text-blue30w" title="Delete Application"
                            data-toggle="modal"
                            data-target="#modalDelete"
                            data-title="{{ $application->user->full_name }}'s Application"
                            data-action="{{ route('users.apply.destroy', ['site' => site()->slug, 'id' => $application->id]) }}"
                            ><span class="glyphicon glyphicon-trash"></a>
                        </td>
                    @endif
                </tr>
                @endif
            @endforeach
            </tbody>
        </table>
        @if ($applications->count() == 0)
            <div class="alert alert-info" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i aria-hidden="true" class="ua-brand-x"></i>
                </button>
                There were no results. Please try again.
            </div>
        @endif
    </div>
@endsection

@include('partials.confirm_delete')