@extends('layouts.app')

@section('breadcrumbs')
    @if ($confirm)
        @component('partials.breadcrumbs.base')
            <li class="active">
                Apply for Board Membership
            </li>
        @endcomponent
    @else
        @component('partials.breadcrumbs.default')
            <li class="active">
                Review Board Application
            </li>
        @endcomponent
    @endif
@endsection

@section('title')
    @if ($confirm)
        Apply for Board Membership
    @else
        Review Board Application
    @endif
@endsection

@section('content')
    @if ($confirm)
        <p><strong>You are almost finished!</strong></p>
        <p>Please review your information below for accuracy. If everything is correct, click the "Submit Final Version" button at the bottom of the page and <strong>carefully read the instructions</strong> that follow. You may also continue editing this form or print a hard copy for your records.</p>
        <p>Just be sure to click the <strong>"Submit Final Version"</strong> button at the bottom of this page before leaving this site. If you do not click this button, <strong>your application will not be submitted</strong>.</p>
    @else
        <h3>{{ $application->user->full_name }}</h3>
        <h4>Submitted: {{ formatDate($application->created_at) }}</h4>
    @endif

    @component('partials.user.contact')
        @slot('contact_information_left')
            <dl class="dl-horizontal">
                <dt>First Name</dt>
                <dd>{{ $application->user->first_name }}</dd>
                <dt>Last Name</dt>
                <dd>{{ $application->user->last_name }}</dd>
                <dt>Student ID</dt>
                <dd>{{ $application->user->student_id }}</dd>
                <dt>Phone</dt>
                <dd>{{ $application->user->phone }}</dd>
                <dt>Email</dt>
                <dd>{{ $application->user->email }}</dd>
            </dl>
        @endslot
        @slot('contact_information_right')
            <dl class="dl-horizontal">
                <dt>Address</dt>
                <dd>{{ $application->user->address }}</dd>
                <dt>City</dt>
                <dd>{{ $application->user->city }}</dd>
                <dt>State</dt>
                <dd>{{ $application->user->state }}</dd>
                <dt>Zip</dt>
                <dd>{{ $application->user->zip }}</dd>
            </dl>
        @endslot
    @endcomponent
    @component('partials.user.apply')
        @slot('academic_information_left')
            <dl class="dl-horizontal">
                <dt>Class Standing</dt>
                <dd>{{ $application->user->standing->name }}</dd>
                <dt>Declared Major</dt>
                <dd>{{ $application->major }}</dd>
                <dt>Declared Minor</dt>
                <dd>{{ $application->minor }}</dd>
                <dt>Current GPA</dt>
                <dd>{{ $application->gpa }}</dd>
                <dt>Cumulative GPA</dt>
                <dd>{{ $application->gpa_cumulative }}</dd>
            </dl>
        @endslot
        @slot('academic_information_right')
            <dl class="dl-horizontal">
                <dt>Expected Graduation Date</dt>
                <dd>{{ $application->graduation_date }}</dd>
                <dt>Fall Credits</dt>
                <dd>{{ $application->credits_fall }}</dd>
                <dt>Spring Credits</dt>
                <dd>{{ $application->credits_spring }}</dd>
            </dl>
        @endslot
        @slot('time_commitments')
            <dl>
                <dt>Do you currently hold any positions or jobs (paid or volunteer)?</dt>
                <dd>{{ formatBoolean($application->current_jobs) }}</dd>
                @if ($application->current_jobs)
                    <dt>How many hours per week?</dt>
                    <dd>{{ $application->job_hours }}</dd>
                    <dt>Please describe the position(s)</dt>
                    <dd>{{ $application->job_description }}</dd>
                @endif
            </dl>
        @endslot
        @slot('clubs_orgs')
            @component('partials.user.clubs')
                @slot('clubs_orgs_intro')
                @endslot
                @slot('clubs_orgs_left')
                    <dl class="dl-horizontal">
                        <dt>Club/Org #1</dt>
                        <dd>{{ $application->user->club_org_1 }}</dd>
                        <dt>Club/Org #2</dt>
                        <dd>{{ $application->user->club_org_2 }}</dd>
                    </dl>
                @endslot
                @slot('clubs_orgs_right')
                    <dl class="dl-horizontal">
                        <dt>Club/Org #3</dt>
                        <dd>{{ $application->user->club_org_3 }}</dd>
                        <dt>Club/Org #4</dt>
                        <dd>{{ $application->user->club_org_4 }}</dd>
                    </dl>
                @endslot
            @endcomponent
        @endslot
        @slot('qualifications')
            <dl>
                <dt>1. Please briefly explain why you are interested in serving on the Advisory Board.</dt>
                <dd>{{ $application->board_interest }}</dd>
                <dt>2. Please explain how any activities that you are involved in may be pertinent to serving as a member of the Advisory Board (ex. student organizations, community service, volunteer activities, job experience, etc.).</dt>
                <dd>{{ $application->board_activities }}</dd>
                <dt>3. Please list any other information that might have a bearing on your qualification for participation in the Advisory Board (ex. honors, fellowships and scholarship, leadership experience, etc.)</dt>
                <dd>{{ $application->board_other }}</dd>
            </dl>
        @endslot
        @slot('resume')
            @if (isset($application->fileResumeURL))
                <p><a href="{{ $application->fileResumeURL }}">Resume</a></p>
            @else
                <p>No resume attached</p>
            @endif
        @endslot
        @slot('references_left')
            @include('partials.user.reference.view', ['reference' => $application->references->count() > 0 ? $application->references->first() : new App\UserApplication() ])
        @endslot
        @slot('references_right')
            @include('partials.user.reference.view', ['reference' => $application->references->count() > 1 ? $application->references->last() : new App\UserApplication() ])
        @endslot
        @slot('terms')
            <p><strong>I [{{ $application->agree_terms ? 'agree' : 'do not agree'}}] to the terms and conditions and wish to be considered for board membership.</strong></p>
        @endslot
    @endcomponent
    @if ($confirm)
        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('users.apply.save', ['site' => site()->slug, 'id' => $application->id]) }}">
            <p class="text-right">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <a href="{{ route('users.apply.edit', ['site' => site()->slug, 'id' => $application->id]) }}" class="btn btn-default" name="save_draft" value="1">Edit</a>
                <button type="submit" class="btn btn-primary">Submit Final Version</button>
            </p>
        </form>
    @elseif (userIsChair())
        <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('users.apply.accept', ['site' => site()->slug, 'id' => $application->id]) }}">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Application Review</h3>
                </div>
                <div class="panel-body">
                    @include('partials.form.textarea', ['name' => 'review_comments', 'label' => 'Please enter your comments about this application/applicant', 'object' => $application])
                    @include('partials.form.select', ['inline' => false, 'name' => 'status', 'label' => 'Applicant Status', 'items' => $applicant_statuses, 'object' => $application, 'nodefault' => true])
                    @if ($application->accepted)
                        <p class="help-block">This applicant has already been accepted and moved into the "Active" Board Member database. If you are rescinding their "Accepted" status, their member status will be set to 'Retired'.</p>
                    @endif
                    @include('partials.form.checkbox', ['name' => 'reject_email', 'value' => 1, 'label' => 'Send rejection notice via email'])
                </div>
                <div class="panel-footer text-right">
                    <a href="{{ route('users.apply.index', ['site' => site()->slug]) }}" class="btn btn-default">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    @endif
@endsection