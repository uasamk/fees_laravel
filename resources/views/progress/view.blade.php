@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.base')
        <li><a href="{{ route('progress.start', baseRouteParams()) }}">Progress Reports</a></li>
        <li class="active">{{ $report->project->project_name }}</li>
    @endcomponent
@endsection

@section('title')
  Progress Reports
@endsection

@section('content')
    <h3>{{ $report->project->project_name }}</h3>

    @if ($review)
        @include('partials.alert.review', ['title' => 'progress report'])
    @endif

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Progress Report Information</h3>
        </div>
        @component('partials.progress.main')
            @slot('introduction')
            @endslot
            @slot('scope')
                <div class="well">{!! nl2br($report->scope) !!}</div>
            @endslot
            @slot('outcomes')
                <div class="well">{!! nl2br($report->outcomes) !!}</div>
            @endslot
            @slot('file_outcomes')
                <p><a href="{{ $report->fileOutcomesURL }}">Supporting Document</a></p>
            @endslot
            @slot('response')
                <div class="well">{!! nl2br($report->response) !!}</div>
            @endslot
            @slot('budget')
                <p><a href="{{ $report->fileBudgetURL }}">Budget</a></p>
            @endslot
        @endcomponent
        @if ($review)
            <div class="panel-footer text-right">
                <a href="{{ route('progress.edit', routeParams(['id' => $report->id])) }}" class="btn btn-default">Edit</a>
                <a href="{{ route('progress.start', baseRouteParams()) }}" class="btn btn-primary submit-review">Submit Final Version</a>
            </div>
        @endif
    </div>

@endsection