@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.project', ['project' => $project])
        <li class="active">Progress Reports</li>
    @endcomponent
@endsection

@section('title')
  Progress Reports
@endsection

@section('content')
    <h3>{{ $project->project_name }}</h3>
    @if ($project->progressReports->count() == 0)
        <p>There are no progress reports attached to this project.</p>
    @else
        @foreach ( $project->progressReports->sortByDesc('created_at') as $report )
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Progress Report #{{ $loop->count - $loop->index }}</h3>
                </div>
                @component('partials.progress.main')
                    @slot('introduction')
                        @if (userIsPower())
                            <p>
                                <a href="{{ route('progress.admin', routeParams(['id' => $report->id])) }}">Edit Progress Report</a>
                                | <a href="#"
                                    data-toggle="modal"
                                    data-target="#modalDelete"
                                    data-title="this progress report"
                                    data-action="{{ route('progress.destroy', routeParams(['id' => $report->id])) }}"
                                    >Delete Progress Report</a>
                                | <a href="#"
                                    data-toggle="modal"
                                    data-target="#modalMove"
                                    data-title="this progress report"
                                    data-action="{{ route('progress.move', routeParams(['id' => $report->id])) }}"
                                    >Move Progress Report</a>
                            </p>
                        @endif
                        <p>
                            Submitted: {{ formatDate($report->created_at) }}<br>
                            by NetID: {{ $report->netid }}
                        </p>
                    @endslot
                    @slot('scope')
                        <div class="well">{!! nl2br($report->scope) !!}</div>
                    @endslot
                    @slot('outcomes')
                        <div class="well">{!! nl2br($report->outcomes) !!}</div>
                    @endslot
                    @slot('file_outcomes')
                        <p>
                            @if (isset($report->fileOutcomesURL))
                                <a href="{{ $report->fileOutcomesURL }}">Supporting Document</a>
                            @else
                                No supporting document attached
                            @endif
                        </p>
                    @endslot
                    @slot('response')
                        <div class="well">{!! nl2br($report->response) !!}</div>
                    @endslot
                    @slot('budget')
                        <p>
                            @if (isset($report->fileBudgetURL))
                                <a href="{{ $report->fileBudgetURL }}">Budget</a>
                            @else
                                No budget attached
                            @endif
                        </p>
                    @endslot
                @endcomponent
            </div>
        @endforeach
    @endif
@endsection

@include('partials.confirm_delete')
@include('partials.move_attachment', ['projects' => $project])