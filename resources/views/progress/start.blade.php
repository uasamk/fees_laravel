@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.base')
        <li class="active">Progress Reports</li>
    @endcomponent
@endsection

@section('title')
  Submit Progress Reports
@endsection

@section('content')
    
    <p><strong>Welcome, {{ $netid }}.</strong></p>
    @if ($projects->count() > 0)
        <p>
            Below is a list of your active projects and their fiscal years. Please select the appropriate project to begin your progress report.<br>
            <strong>NOTE: If you have multiple projects with the same or similar names, be sure you choose the project associated with the CORRECT FISCAL YEAR for the report you are filing. Please refer to the email you received requesting this report if you are unsure of your project's fiscal year.</strong><br>
        </p>

        <ol>
            @foreach ($projects as $project)
                <li><a href="{{ route('progress.create', routeParams(['project' => $project->id])) }}">{{ $project->project_name }}</a> / FY{{ $project->fiscal_year }}</li>
            @endforeach
        </ol>
    @else
        <p>Our records indicate that you do not have any funded initiatives available for Progress Report submission.</p>
    @endif
    
    <p>
        Semi-annual progress reports are due no later than 
        @include('partials.progress.deadlines')
        If this is not a semi-annual report, it should be completed as soon as possible after receiving the email request to file the report.
    </p>
    <p><strong>NOTE: Only the manager's and director's projects are displayed on this page. If you are expecting to see a project that is not listed above, you may not be listed as the manager or director. To find the contact information for a project, go to the <a href="{{ site()->projectSets->first()->projectsURL }}">projects page</a> and search for the project. Please contact us if you have questions.</strong></p>
@endsection