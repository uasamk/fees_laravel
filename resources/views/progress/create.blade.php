@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.base')
        <li><a href="{{ route('progress.start', baseRouteParams()) }}">Progress Reports</a></li>
        <li class="active">{{ $project->board_id or $project->project_name }}</li>
    @endcomponent
@endsection

@section('title')
  Submit Progress Reports
@endsection

@section('content')
    <h3>{{ $project->project_name }}</h3>

    @if (!isset($report) && $project->netid != $netid && $project->director_netid != $netid)
        @include('partials.alert.contact-mismatch', ['title' => 'Progress Report'])
    @else
        @if (isset($report))
            <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('progress.update', routeParams(['id' => $report->id])) }}">
            {{ method_field('PUT') }}
        @else
            <p>
                <strong>Welcome, {{ $project->director_netid == $netid ? $project->director_first_name : $project->first_name }}.</strong><br>
                Use the form below to complete your progress report for the {{ $project->project_name }} initiative.
            </p>

            @if (isset($recent))
                @component('partials.alert.base', ['type' => 'warning'])
                    <h4>Recent Filing Notice</h4>
                    <p>We noticed you submitted a <a href="{{ route('progress.show', routeParams(['id' => $recent->id])) }}">progress report</a> for this project within the past 30 days. Generally, progress reports are not required this frequently for the same project. If you do need to submit another report for this project, please proceed as instructed below. If you are looking for a different project, <a href="{{ route('progress.start', baseRouteParams()) }}">click here</a> for a listing of all of your current projects.</p>
                @endcomponent
            @endif

            <p><strong>READ THESE IMPORTANT NOTES</strong></p>
            <ol>
                <li>Be sure to review this form before you begin entering content. You should have all resources (text field content &amp; required file attachments) prepared, accessible and ready to submit in one sitting. UA NetID/WebAuth sessions will expire if no activity is detected on this page after a period of time and you will have to re-enter your data.</li>
                <li>For best results, please enter plain text. Pasting pre-formatted Word content into this form may yield unpredictable results. If you already have formatted content in Word, save the Word document as plain text or copy the content into a plain text editor such as Notepad or WordPad (Windows) or TextEdit (Mac). Be sure the editor is in text-only mode and then copy/paste the plain text content into this form.</li>
                <li>Progress reports are part of this project's permanent record and will be viewable by the general public.</li>
            </ol>

            <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('progress.store', baseRouteParams()) }}">
        @endif
            {{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Progress Report Information</h3>
                </div>
                @component('partials.progress.main')
                    @slot('introduction') @endslot
                    @slot('scope')
                        @include('partials.form.textarea', ['name' => 'scope', 'label' => '', 'object' => isset($report) ? $report : null])
                    @endslot
                    @slot('outcomes')
                        @include('partials.form.textarea', ['name' => 'outcomes', 'label' => '', 'object' => isset($report) ? $report : null])
                    @endslot
                    @slot('file_outcomes')
                        @if (isset($report))
                            <p><a href="{{ $report->fileOutcomesURL }}">Supporting Document</a></p>
                        @endif
                        @component('partials.form.field_base', ['name' => 'file_outcomes', 'label' => 'PDF only (1MB or less)'])
                            <input type="file" name="file_outcomes">
                        @endcomponent
                    @endslot
                    @slot('response')
                        @include('partials.form.textarea', ['name' => 'response', 'label' => '', 'object' => isset($report) ? $report : null])
                    @endslot
                    @slot('budget')
                        @if (isset($report))
                            <p><a href="{{ $report->fileBudgetURL }}">Budget</a></p>
                        @endif
                        @component('partials.form.field_base', ['name' => 'budget', 'label' => 'Excel or PDF only (1MB or less)'])
                            <input type="file" name="budget">
                        @endcomponent
                    @endslot
                @endcomponent
                <div class="panel-footer text-right">
                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                    @if ($admin_edit)
                        <a href="{{ route('progress.index', routeParams(['project' => $project->id])) }}" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-primary" name="admin_edit" value="1">Save</button>
                    @else 
                        <button type="submit" class="btn btn-primary">Submit</button>
                    @endif
                </div>
            </div>
        </form>
    @endif
@endsection