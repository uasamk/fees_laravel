@php
$pars_array = [];
$years = [];

foreach ($pars as $key => $par){
    array_push($pars_array, $par);
    if(!in_array($par->project->start_year+1, $years)){
        array_push($years, $par->project->start_year+1);
    }
}


function cmp($a, $b)
{
    if($a->project->start_year == $b->project->start_year){ return 0 ; }
	return ($a->project->start_year > $b->project->start_year) ? -1 : 1;
}

usort($pars_array, "cmp");
arsort($years);
@endphp

@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.default')
        <li class="active">PARs</li>
    @endcomponent
@endsection

@section('title')
  Program Alteration Requests (PARs)
@endsection

@section('content')
    <h4>Which group of PARs would you like to see?</h4>
    <div id="filter_wrap">
        <form id="status_filter" role="form" method="GET" action="{{ route('pars.admin.index', baseRouteParams()) }}">
            <div>
                <div>
                    @include('partials.form.filter_select', ['name' => 'par-status', 'label' => 'PAR Status', 'items' => $par_statuses])
                </div>
            </div>
        </form>
        <select name="fiscal_year" id="fiscal_year" class="form-control">
            <option disabled selected hidden value="Fiscal Year">Fiscal Year</option>
            <?php foreach($years as $year){ ?>
                <option value="<?=$year?>"><?= $year ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="table-responsive">
        <table class="table-dark table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Project ID</th>
                    <th>PAR ID</th>
                    <th>Applicant</th>
                    <th>Project Name</th>
                    <th>Fiscal Year</th>
                    <th>Board Approval</th>
                    <th>VP Approval</th>
                    <th>Date Submitted</th>
                    @if (userIsAdmin())
                        <th></th>
                        <th></th>
                        <th></th>
                    @endif
                    @if (userIsPower())
                        <th></th>
                        <th></th>
                    @endif
                    @if (userIsChair())
                        <th></th>
                    @endif
                </tr>
            </thead>
            <tbody id="pars_table_container">
            @foreach ($pars_array as $key => $par)
                <tr data-year="<?= $par->project->start_year + 1 ?>" class="show_row par_row">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $par->project->board_id }}</td>
                    <td><a href="{{ route('pars.show', routeParams(['id' => $par->id])) }}">{{ $par->board_id }}</a></td>
                    <td>{{ $par->project->first_name }} {{ $par->project->last_name }}</td>
                    <td>{{ $par->project->project_name }}</td>
                    <td>{{ $par->project->start_year + 1 }}</td>
                    <td>{{ formatBoolean($par->approved) }}</td>
                    <td>{{ formatBoolean($par->vp_approved) }}</td>
                    <td>{{ formatDate($par->created_at) }}</td>
                    @if (userIsAdmin())
                        <td>
                            @if (isset($par->approved))
                                <span class="text-silver glyphicon glyphicon-question-sign">
                            @else
                                <a class="text-blue30w" href="#" title="Submit Question"
                                    data-toggle="modal"
                                    data-target="#modalQuestion"
                                    data-title="{{ $par->board_id }}"
                                    data-project="{{ $par->project->project_name }}"
                                    data-item-type="2"
                                    data-item-id="{{ $par->id }}">
                                    <span class="glyphicon glyphicon-question-sign">
                                </a>
                            @endif
                        </td>
                        <td>
                            @if (isset($par->approved))
                                <span class="text-silver glyphicon glyphicon-list-alt">
                            @else
                                <a class="text-blue30w" href="{{ route('pars.advice', routeParams(['id' => $par->id])) }}" title="Add Advice"><span class="glyphicon glyphicon-list-alt"></a>
                            @endif
                        </td>
                        <td>
                            @if (isset($par->vp_approved))
                                <span class="text-silver glyphicon glyphicon-ok-sign">
                            @else
                                <a class="text-blue30w" href="{{ route('pars.vp', routeParams(['id' => $par->id])) }}" title="VP Review"><span class="glyphicon glyphicon-ok-sign"></a>
                            @endif
                        </td>
                    @endif
                    @if (userIsPower())
                        <td><a class="text-blue30w" href="{{ route('pars.admin.edit', routeParams(['id' => $par->id])) }}" title="Edit Par"><span class="glyphicon glyphicon-pencil"></a></td>
                        <td>
                            <a href="#" class="text-blue30w" title="Delete Par"
                            data-toggle="modal"
                            data-target="#modalDelete"
                            data-title="PAR {{ $par->board_id }}"
                            data-action="{{ route('pars.destroy', routeParams(['id' => $par->id])) }}"
                            ><span class="glyphicon glyphicon-trash"></a>
                        </td>
                    @endif
                    <!-- Adding function for chairs to give advice -->
                    @if (userIsChair())
                        <td>
                        @if (isset($par->approved))
                                <span class="text-silver glyphicon glyphicon-list-alt">
                            @else
                                <a class="text-blue30w" href="{{ route('pars.advice', routeParams(['id' => $par->id])) }}" title="Add Advice"><span class="glyphicon glyphicon-list-alt"></a>
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
        @if ($pars->count() == 0)
            <div class="alert alert-info" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i aria-hidden="true" class="ua-brand-x"></i>
                </button>
                There were no results. Please try again.
            </div>
        @endif
    </div>
@endsection

@include('partials.question.create')
@include('partials.confirm_delete')

<script>
    NodeList.prototype.forEach = Array.prototype.forEach;

    document.addEventListener("DOMContentLoaded", function() {
        var el = document.getElementById('fiscal_year');
        el.addEventListener('change', function(e){
            document.querySelectorAll('.par_row').forEach(function(row){
                if(row.getAttribute('data-year') == el.options[el.selectedIndex].value){
                    if(!row.classList.contains('show_row')){
                        row.classList.add('show_row');
                    }
                }else{
                    if(row.classList.contains('show_row')){
                        row.classList.remove('show_row');
                    }
                }
            });
        });
    });
</script>