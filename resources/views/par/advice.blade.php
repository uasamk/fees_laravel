@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.par')
        <li class="active">Advice</li>
    @endcomponent
@endsection

@section('title')
  Submit Advice
@endsection

@section('content')
    <h3>{{ $par->project->project_name }}</h3>
    <p>Use the form below to submit an Advice for PAR {{ $par->board_id }}.</p>
    <p><strong>NOTE:</strong> For best results, please <strong>enter plain text</strong>. Pasting preformatted Word content may yield unpredictable results. If you already have formatted content in Word, try saving the Word document as plain text or copying the content into a plain text editor such as Notepad or TextEdit. Then copy/paste the plain text content into this form.</p>
    <p><strong>ALSO NOTE:</strong> this Advice will be part of the original proposal's permanent record and <strong>will be viewable by the general public.</strong></p>

    <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('pars.advice.update', routeParams(['id' => $par->id])) }}">
    {{ method_field('PUT') }}
    {{ csrf_field() }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Advice Information</h3>
            </div>
            @component('partials.par.main', ['mode' => $mode])
                @include('partials.par.advice')
            @endcomponent
            <div class="panel-footer text-right">
                <input type="hidden" name="par_id" value="{{ $par->id }}">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>

@endsection