@extends('layouts.app')

@section('breadcrumbs')
    @if ($mode == 4)
        @component('partials.breadcrumbs.par')
            <li class="active">{{ $par->board_id }}</li>
        @endcomponent
    @else
        @component('partials.breadcrumbs.base')
            <li><a href="{{ route('pars.start', baseRouteParams()) }}">PARs</a></li>
            <li class="active">{{ $project->board_id or $project->project_name }}</li>
        @endcomponent
    @endif
@endsection

@section('title')
    @if ($mode == 4)
        Edit PAR
    @else
        Submit PARs
    @endif
@endsection

@section('content')
    <h3>{{ $project->project_name }}</h3>
    @if (!isset($par) && $project->netid != $netid && $project->director_netid != $netid)    
        @include('partials.alert.contact-mismatch', ['title' => 'PAR'])
    @else
        @if (isset($par))
            <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('pars.update', routeParams(['id' => $par->id])) }}">
            {{ method_field('PUT') }}
        @else
            <p>
                <strong>Welcome, {{ $project->director_netid == $netid ? $project->director_first_name : $project->first_name }}.</strong><br>
                Use the form below to complete your PAR for the {{ $project->project_name }} initiative.
            </p>
            <p>
                <strong>NOTE:</strong> For best results, please <strong>enter plain text</strong>. Pasting preformatted Word content may yield unpredictable results.
                If you already have formatted content in Word, try saving the Word document as plain text or copying the content into a plain text editor such as Notepad or TextEdit. Then copy/paste the plain text content into this form.
                ALSO NOTE, PARs are part of this application's permanent record and <strong>will be viewable by the general public</strong>. 
            </p>

            <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('pars.store', baseRouteParams()) }}">
        @endif
            {{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">PAR Information</h3>
                </div>
                @component('partials.par.main', ['mode' => $mode])
                    @slot('introduction')
                        @if ($mode == 4)
                            @include('partials.form.text', ['inline' => false, 'type' => 'text', 'name' => 'board_id', 'label' => 'Override Board ID', 'object' => isset($par) ? $par : null])
                        @endif
                    @endslot
                    @slot('changes')
                        @include('partials.form.textarea', ['name' => 'changes', 'label' => '', 'object' => isset($par) ? $par : null])
                    @endslot
                    @slot('document')
                        @if (isset($par->fileSupportURL))
                            <p><a href="{{ $par->fileSupportURL }}">Supporting Document</a></p>
                        @endif
                        @component('partials.form.field_base', ['name' => 'file_support', 'label' => 'Excel, PDF or Word format (1.75MB or less)'])
                            <input type="file" name="file_support">
                        @endcomponent
                    @endslot
                    @if ($mode == 4)
                        @slot('questions')
                            @include('partials.question.index', ['questions' => $par->questions])
                        @endslot
                        @include('partials.par.advice')
                        @include('partials.par.vp')
                    @endif
                @endcomponent
                <div class="panel-footer text-right">
                    <input type="hidden" name="mode" value="{{ $mode }}">
                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                    <button type="submit" class="btn btn-primary" name="save_draft" value="0">Submit</button>
                </div>
            </div>
        </form>
    @endif
@endsection