@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.project', ['project' => $project])
        <li class="active">PARs</li>
    @endcomponent
@endsection

@section('title')
  Program Alteration Requests (PARs)
@endsection

@section('content')
    <h3>{{ $project->project_name }}</h3>
    @if ($project->pars->count() == 0)
        <p>There are no PARs attached to this project.</p>
    @else
        @foreach ( $project->pars->sortByDesc('created_at') as $par )
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">PAR {{ $par->board_id }}</h3>
                </div>
                @component('partials.par.main', ['mode' => 4])
                    @slot('introduction')
                        <p>
                            Submitted: {{ formatDate($par->created_at) }}<br>
                            by NetID: {{ $par->netid }}
                        </p>
                    @endslot
                    @slot('changes')
                        <div class="well">{!! nl2br($par->changes) !!}</div>
                    @endslot
                    @slot('document')
                        <p>
                            @if (isset($par->fileSupportURL))
                                <a href="{{ $par->fileSupportURL }}">Supporting Document</a>
                            @else
                                No Document Attached
                            @endif
                        </p>
                    @endslot
                    @slot('questions')
                        @include('partials.question.index', ['questions' => $par->questions])                        
                    @endslot
                    @slot('advice')
                        @if (isset($par->advice))
                            <div class="well">{!! nl2br($par->advice) !!}</div>
                        @else  
                            <p>No advice available</p>
                        @endif
                    @endslot
                    @slot('approval')
                        <p>{{ formatBoolean($par->approved) }}</p>
                    @endslot
                    @slot('advice_document')
                        @if (isset($par->fileAdviceURL))
                            <a href="{{ $par->fileAdviceURL }}">Supporting Document</a>
                        @else
                            No Document Attached
                        @endif
                    @endslot
                    @slot('vp_comments')
                        @if (isset($par->vp_comments))
                            <div class="well">{!! nl2br($par->vp_comments) !!}</div>
                        @else  
                            <p>No comments available</p>
                        @endif
                    @endslot
                    @slot('vp_approval')
                        <p>{{ formatBoolean($par->vp_approved) }}</p>
                    @endslot
                @endcomponent
            </div>
        @endforeach
    @endif
@endsection