@extends('layouts.app')

@section('breadcrumbs')
    @component('partials.breadcrumbs.base')
        <li class="active">PARs</li>
    @endcomponent
@endsection

@section('title')
  Submit Program Alteration Request (PAR)
@endsection

@section('content')
    <p><strong>Welcome, {{ $netid }}.</strong></p>
    @if ($projects->count() > 0)
        <p>
            Below is a list of your active projects and their fiscal years. Please select the appropriate project to begin your PAR.<br>
        </p>

        <ol>
            @foreach ($projects as $project)
                <li><a href="{{ route('pars.create', routeParams(['project' => $project->id])) }}">{{ $project->project_name }}</a> / FY{{ $project->fiscal_year }}</li>
            @endforeach
        </ol>
    @else
        <p>Our records indicate that you do not have any funded initiatives available for PAR submission.</p>
    @endif
    
    <p><strong>NOTE: Only the manager's and director's projects are displayed on this page. If you are expecting to see a project that is not listed above, you may not be listed as the manager or director. To find the contact information for a project, go to the <a href="{{ site()->projectSets->first()->projectsURL }}">projects page</a> and search for the project. Please contact us if you have questions.</strong></p>
@endsection