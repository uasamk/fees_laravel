@extends('layouts.app')

@section('breadcrumbs')
    @if ($mode > 1)
        @component('partials.breadcrumbs.par')
            <li class="active">{{ $mode == 4 ? $par->project->board_id : $title }}</li>
        @endcomponent
    @else
        @component('partials.breadcrumbs.base')
            <li><a href="{{ route('progress.start', baseRouteParams()) }}">PARs</a></li>
            <li class="active">{{ $par->project->board_id or $par->project->project_name }}</li>
        @endcomponent
    @endif
@endsection

@section('title')
    @if ($mode == 1)
        Submit PARs
    @elseif ($mode == 2)
        Submit Advice
    @else
        PAR Review
    @endif
@endsection

@section('content')
    <h3>
        @if ($mode == 2)
            PAR {{ $par->board_id }}
        @else
            {{ $par->project->project_name }}
        @endif
    </h3>

    @if ($review)
        @include('partials.alert.review', ['title' => $title])
    @endif

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">PAR Information</h3>
        </div>

        @component('partials.par.main', ['mode' => $mode])
            @slot('introduction') @endslot
            @slot('changes')
                <div class="well">{!! nl2br($par->changes) !!}</div>
            @endslot
            @slot('document')
                <p>
                    @if (isset($par->fileSupportURL))
                        <a href="{{ $par->fileSupportURL }}">Supporting Document</a>
                    @else
                        No Document Attached
                    @endif
                </p>
            @endslot
            @if ($mode == 4)
                @slot('questions')
                    @include('partials.question.index', ['questions' => $par->questions])
                @endslot
            @endif
            @if ($mode == 2 || $mode == 4)
                @slot('advice')
                    @if (isset($par->advice))
                        <div class="well">{!! nl2br($par->advice) !!}</div>
                    @else  
                        <p>No advice available</p>
                    @endif
                @endslot
                @slot('approval')
                    <p>{{ formatBoolean($par->approved) }}</p>
                @endslot
                @slot('advice_document')
                    @if (isset($par->fileAdviceURL))
                        <a href="{{ $par->fileAdviceURL }}">Supporting Document</a>
                    @else
                        No Document Attached
                    @endif
                @endslot
            @endif
            @if ($mode == 3 || $mode == 4)
                @slot('vp_comments')
                    @if (isset($par->vp_comments))
                        <div class="well">{!! nl2br($par->vp_comments) !!}</div>
                    @else  
                        <p>No comments available</p>
                    @endif
                @endslot
                @slot('vp_approval')
                    <p>{{ formatBoolean($par->vp_approved) }}</p>
                @endslot
            @endif
        @endcomponent
        @if ($review)
            <div class="panel-footer text-right">
                <a href="{{ route('pars.' . $edit_route, routeParams(['id' => $par->id])) }}" class="btn btn-default">Edit</a>
                <a href="{{ $submit_route }}" class="btn btn-primary submit-review">Submit Final Version</a>
            </div>
        @endif
    </div>
@endsection