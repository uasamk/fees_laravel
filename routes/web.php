<?php

use App\Site;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

// sites
Route::group(['prefix' => 'sites', 'as' => 'sites.'], function () {
  Route::post('{id}/files/save', 'SiteController@saveFiles')->name('files.save');
  Route::delete('{id}/files/{file_id}/delete', 'SiteController@deleteFile')->name('files.destroy');
  Route::post('{id}/departments/save', 'SiteController@saveDepartment')->name('departments.save');
  Route::delete('{id}/departments/{dept_id}/delete', 'SiteController@deleteDepartment')->name('departments.destroy');
  Route::post('{id}/initiatives/save', 'SiteController@saveInitiative')->name('initiatives.save');
  Route::put('{id}/initiatives/{init_id}/update', 'SiteController@updateInitiative')->name('initiatives.update');
  Route::delete('{id}/initiatives/{init_id}/delete', 'SiteController@deleteInitiative')->name('initiatives.destroy');
});
Route::resource('sites', 'SiteController');

// site specific routes
Route::group(['prefix' => '{site}'], function () {
  Route::get('/', 'SiteController@show')->name('index');
  Route::get('contact', 'SiteController@contact')->name('contact');
  Route::post('contact', 'SiteController@contactSend')->name('contact.send');
  Route::get('admin', 'SiteController@admin')->name('admin');

  // pages (editing)
  Route::put('pages/order', 'PageController@updateOrder')->name('pages.order');
  Route::resource('pages', 'PageController');

  // project sets
  Route::group(['prefix' => 'project-sets', 'as' => 'project-sets.'], function() {
    Route::get('{id}/set', 'ProjectSetController@setSession')->name('set');
  });
  Route::resource('project-sets', 'ProjectSetController');

  // questions and answers
  Route::post('{id}/extend', 'QuestionController@questionExtension')->name('questions.questionExtension');

  // Route::get('{id}/extend/done','QuestionController@questionExtensionDone');
  Route::resource('questions', 'QuestionController');

  Route::resource('answers', 'AnswerController');

  // users
  Route::resource('users', 'UserController');
  Route::put('users/site/update', 'UserController@updateSite')->name('users.site.update');
  Route::get('directory', 'UserController@directory')->name('users.directory');
  Route::group(['prefix' => 'board/apply'], function() {
    Route::get('/', 'UserController@apply')->name('users.apply');
    Route::post('/', 'UserController@applyStore')->name('users.apply.store');
    Route::get('index', 'UserController@applyIndex')->name('users.apply.index');
    Route::get('{id}/view', 'UserController@applyView')->name('users.apply.view');
    Route::get('{id}/confirm', 'UserController@confirm')->name('users.apply.confirm');
    Route::get('{id}/edit', 'UserController@applyEdit')->name('users.apply.edit');
    Route::put('{id}', 'UserController@applyUpdate')->name('users.apply.update');
    Route::put('{id}/save', 'UserController@applySave')->name('users.apply.save');
    Route::put('{id}/accept', 'UserController@applyAccept')->name('users.apply.accept');
    Route::delete('{id}/destroy', 'UserController@applyDestroy')->name('users.apply.destroy');
  });

  Route::group(['prefix' => '{project_set}'], function() {
    // custom project routes
    Route::group(['prefix' => 'projects', 'as' => 'projects.'], function () {
      Route::get('start', 'ProjectController@createStart')->name('start');
      Route::put('save', 'ProjectController@save')->name('save');
      Route::put('approve', 'ProjectController@approve')->name('approve');
      Route::get('review', 'ProjectController@review')->name('review');
      Route::get('admin', 'ProjectController@admin')->name('admin.index');
      Route::get('{id}/admin', 'ProjectController@adminEdit')->name('admin.edit');
      Route::get('{id}/confirm', 'ProjectController@confirm')->name('apply.confirm');
      Route::get('{id}/approve', 'ProjectController@showApprove')->name('apply.approve');
      Route::get('{id}/full', 'ProjectController@full')->name('full');
      Route::post('{id}/remind', 'ProjectController@remind')->name('remind');
      Route::get('{id}/files', 'ProjectController@files')->name('files');
      Route::post('{id}/files/save', 'ProjectController@saveFile')->name('files.save');
      Route::delete('files/{id}/delete', 'ProjectController@deleteFile')->name('files.destroy');
      Route::put('files/{id}/move', 'ProjectController@moveFile')->name('files.move');
      Route::get('{id}/revisions', 'ProjectController@revisions')->name('revisions');
      Route::get('{id}/budgets', 'ProjectController@budgets')->name('budgets');
      Route::get('{id}/notifications', 'ProjectController@notifications')->name('notifications');
      Route::get('{id}/questions', 'ProjectController@questions')->name('questions');
      Route::get('{id}/comments', 'ProjectController@comments')->name('comments');
      Route::post('{id}/comments/save', 'ProjectController@saveComment')->name('comments.save');
    });
    // project resource routes
    Route::resource('projects', 'ProjectController');

    // progress reports
    Route::group(['prefix' => 'progress', 'as' => 'progress.'], function() {
      Route::get('start', 'ProgressReportController@createStart')->name('start');
      Route::get('{id}/review', 'ProgressReportController@review')->name('review');
      Route::get('{id}/admin', 'ProgressReportController@adminEdit')->name('admin');
      Route::post('send', 'ProgressReportController@send')->name('send');
      Route::put('{id}/move', 'ProgressReportController@move')->name('move');
    });
    Route::resource('progress', 'ProgressReportController');

    // pars
    Route::group(['prefix' => 'pars', 'as' => 'pars.'], function() {
      Route::get('start', 'ParController@createStart')->name('start');
      Route::get('admin', 'ParController@admin')->name('admin.index');
      Route::get('{id}/review', 'ParController@review')->name('review');
      Route::get('{id}/advice', 'ParController@advice')->name('advice');
      Route::put('{id}/advice/update', 'ParController@updateAdvice')->name('advice.update');
      Route::get('{id}/advice/review', 'ParController@reviewAdvice')->name('advice.review');
      Route::get('{id}/vp', 'ParController@vp')->name('vp');
      Route::put('{id}/vp/update', 'ParController@updateVp')->name('vp.update');
      Route::get('{id}/vp/review', 'ParController@reviewVp')->name('vp.review');
      Route::get('{id}/approve', 'ParController@approve')->name('approve');
      Route::get('{id}/admin/edit', 'ParController@adminEdit')->name('admin.edit');
    });
    Route::resource('pars', 'ParController');
  });

  // login
  Route::get('login', 'UserController@login')->name('login');

  // handle redirects from webauth
  Route::get('webauth/redirect', ['as' => 'guest.login.redirect', 'uses' => 'WebauthController@redirect']);

  // this goes last to not catch anything important
  Route::get('/{any}', 'PageController@showBySlug')->where('any', '.*');
});
