<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id');
            $table->string('name');
            $table->string('slug');
            $table->integer('fiscal_cutoff_month');
            $table->boolean('fiscal_cutoff_add');
            $table->datetime('application_open');
            $table->datetime('application_deadline');
            $table->datetime('progress_deadline');
            $table->integer('question_deadline');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_sets');
    }
}
