<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('netid')->unique();
            $table->string('student_id')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->integer('user_standing_id')->nullable();
            $table->string('club_org_1')->nullable();
            $table->string('club_org_2')->nullable();
            $table->string('club_org_3')->nullable();
            $table->string('club_org_4')->nullable();
            $table->integer('user_title_id')->nullable();
            $table->string('title_other')->nullable();
            $table->integer('user_type_id')->nullable();
            $table->text('responsibilities')->nullable();
            $table->integer('start_year')->nullable();
            $table->integer('fiscal_year')->nullable();
            $table->integer('user_status_id')->nullable();
            $table->boolean('directory')->nullable();
            //$table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
