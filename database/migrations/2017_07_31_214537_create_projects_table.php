<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_old')->nullable(); // for import
            $table->integer('project_set_id');
            $table->integer('funding_status_id')->nullable();
            $table->string('board_id')->nullable();
            $table->string('netid');
            $table->string('netid_modified')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->integer('department_id')->nullable();
            $table->string('department_other')->nullable();
            $table->string('division')->nullable();
            $table->string('director_first_name')->nullable();
            $table->string('director_last_name')->nullable();
            $table->string('director_email')->nullable();
            $table->string('fiscal_first_name')->nullable();
            $table->string('fiscal_last_name')->nullable();
            $table->string('fiscal_email')->nullable();
            $table->string('project_name')->nullable();
            $table->integer('start_year');
            $table->integer('amount')->nullable();
            $table->integer('amount_received')->nullable();
            $table->boolean('permanent_funding')->default(0);
            $table->boolean('new_project')->nullable();
            $table->boolean('past_funding')->nullable();
            $table->integer('duration')->nullable();
            $table->integer('duration_received')->nullable();
            $table->boolean('yearly_variation')->nullable();
            $table->text('justification')->nullable();
            $table->boolean('sole_funding')->nullable();
            $table->boolean('agree_terms')->nullable();
            $table->boolean('locked')->default(0);
            $table->boolean('pre_deadline');
            $table->boolean('email_sent')->default(0);
            $table->boolean('director_email_sent')->default(0);
            $table->boolean('fiscal_email_sent')->default(0);
            $table->boolean('signoff')->default(0);
            $table->boolean('admin')->default(0);
            $table->boolean('public')->default(0);
            $table->boolean('archived')->default(0);
            $table->datetime('final_submit_time')->nullable();
            $table->text('board_comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
