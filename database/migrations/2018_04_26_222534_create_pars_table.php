<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_old')->nullable();
            $table->integer('project_id');
            $table->string('netid');
            $table->string('netid_review')->nullable();
            $table->string('netid_vp')->nullable();
            $table->text('changes');
            $table->text('advice')->nullable();
            $table->text('vp_comments')->nullable();
            $table->boolean('approved')->nullable();
            $table->boolean('vp_approved')->nullable();
            $table->datetime('timestamp_review')->nullable();
            $table->datetime('timestamp_vp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pars');
    }
}
