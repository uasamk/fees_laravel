<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('major');
            $table->string('minor');
            $table->decimal('gpa', 10, 2);
            $table->decimal('gpa_cumulative', 10, 2);
            $table->datetime('graduation_date');
            $table->decimal('credits_fall', 10, 2);
            $table->decimal('credits_spring', 10, 2);
            $table->boolean('current_jobs');
            $table->integer('job_hours')->nullable();
            $table->text('job_description')->nullable();
            $table->text('board_interest');
            $table->text('board_activities');
            $table->text('board_other')->nullable();
            $table->boolean('agree_terms');
            $table->boolean('accepted')->nullable();
            $table->boolean('reject_email')->nullable();
            $table->text('review_comments')->nullable();
            $table->boolean('locked');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_applications');
    }
}
