<?php

use Illuminate\Database\Seeder;

class GfParsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        
        \DB::table('pars')->insert(array (
            0 => 
            array (
                'id_old' => 29991,
                'netid' => 'mkacira',
                'changes' => 'Since we will be able to complete the greenhouse system installations as well as PV system installations during the original proposed project timeline, we kindly request a no-cost extension for one more year from May 10, 2012 to be able to use the funding provided for our project to complete the research and outreach components initially proposed in the project.',
                'advice' => 'This PAR asked for a no cost extension for one year so that they may finish the work they have started and conduct the research and testing with the new systems. This extension will expire at the end of FY2013.',
                'approved' => 1,
                'vp_comments' => '',
                'vp_approved' => 1,
                'created_at' => '2012-02-09 22:45:30.000000',
                'timestamp_review' => '2012-05-02 20:28:09.000000',
                'netid_review' => 'rudnick1',
                'timestamp_vp' => '2012-05-02 20:33:34.000000',
                'netid_vp' => 'rudnick1',
                'project_id' => 303,
            ),
            1 => 
            array (
                'id_old' => 29992,
                'netid' => 'mapel',
            'changes' => 'Unexpended personnel and operations funds from our 2011-2012 Green Fund award has resulted in a savings of $5,559 that we would like to apply to our 2012-2013 Green Fund request, to fund the wages for student externs in May and June of 2012, prior to our allocation of Green Fund monies, if awarded, in mid-July.  Details regarding the savings we incurred from our 2011-2012 award can be provided by our Budget Manager, Patti Baciewicz in Cooperative Extension (621-5308) or pattib@cals.arizona.edu.  In general, the savings resulted from one non-UA student not completing her externship due to difficulties in getting compensated in a timely manner and some operations money that was not spent on materials as planned.',
            'advice' => 'This PAR asked for a no-cost extension for one year so that they may utilize the money not spent in FY2012 and will spend it on student salary ($5,559). This extension will expire at the end of FY2013.',
                'approved' => 1,
                'vp_comments' => '',
                'vp_approved' => 1,
                'created_at' => '2012-02-22 18:23:47.000000',
                'timestamp_review' => '2012-05-02 20:38:06.000000',
                'netid_review' => 'rudnick1',
                'timestamp_vp' => '2012-05-02 21:19:24.000000',
                'netid_vp' => 'rudnick1',
                'project_id' => 308,
            ),
            2 => 
            array (
                'id_old' => 29993,
                'netid' => 'jillb1',
                'changes' => 'We are requesting to change the dashboard installed in Posada San Pedro.  Instead of the Lucid dashboard, we are requesting the committee approve the “Sunny Portal” site.  There are several reasons.  First, the Lucid installation would only be for one year, with no anticipated funding to pay for additional annual service fees.  In addition, due to how the data is being captured and transmitted from the solar panels, the Lucid System has proven to be unforeseeably cost-prohibitive; there is not enough money in the Green Fund grant to pay for even one year.  The Sunny Portal is a welcome alternative.  This site is provided by the company who installed the PV panels, at no additional annual cost.  Using this system, the information will be readily available for students for many years to come.  

The Sunny Portal site will monitor the PV output, using visual representations and conversions much like the Lucid system.  One difference, however, is that the site is not particularly kiosk friendly—there are too many ways to navigate away from the intended pages if the website is displayed on a touchscreen.  To ensure the security of the UA computing system, as well as to ensure that students will always be able to see the appropriate information, we are recommending that the kiosk not be touchscreen.  This will, of course, drop the price.

Another difference from Lucid is that the Sunny Portal does not display the hall’s electricity usage.  The problem here is that students and researchers would not be able to see patterns, like electricity use declining as PV output climbs.  To enable such research, we would like to buy two monitors.  One will display the Sunny Portal, while the other will display the new Facilities Management Energy Dashboard set to Posada San Pedro’s electricity usage page.  With these two monitors, students retain the ability to watch for patterns in the data, despite not having the Lucid System.  

In sum, we will use less money to purchase the equipment and service for the Sunny Portal than for the Lucid system.  We are, however, requesting that funds be shifted into different operation codes.  We need money for the following:
1)	Install internet service to PV monitors, to get the data to the Sunny Portal for display over the web (take money from “data loggers.”)
2)	Purchase supplies and labor to build a cabinet that will safely house the computer running the kiosk (take money from “installation”).
3)	Purchase 1 computer, 2 monitors, and 2 mounting brackets to display the Sunny Portal and the FM Energy Dashboard (this money is already in the correct account, labeled “computer”).

We feel this is the best course of action, as it will be available indefinitely as opposed to only for 1 year.  This also prevents any party from being forced to find additional funding to cover the cost of the Lucid system.   Thank you for your consideration.',
                'advice' => 'This PAR asked to change the equipment supplier from Lucid Systems to Sunny Portal Systems.',
                'approved' => 1,
                'vp_comments' => '',
                'vp_approved' => 1,
                'created_at' => '2012-04-10 22:34:47.000000',
                'timestamp_review' => '2012-05-02 20:39:27.000000',
                'netid_review' => 'rudnick1',
                'timestamp_vp' => '2012-05-02 21:19:39.000000',
                'netid_vp' => 'rudnick1',
                'project_id' => 314,
            ),
            3 => 
            array (
                'id_old' => 29994,
                'netid' => 'powersj',
                'changes' => 'The total funds of $38,900 requested from the Green Fund committee for Computer Power Management software will remain unchanged.  The proposed pilot will still implement approximately 2,250 computer management software licenses throughout the university and assess the results.  The accepted budget also is unchanged in year one of the project.  The loss of the TEP rebate will only effect year two of the pilot project.  The TEP rebate was to fund $1,000 or 80 hours of student labor plus $14,061 for three years of subsequent year maintenance and support.  Facilities Management will provide 80 hours of student labor in year two of the project.  The options available to provide additional maintenance are for the participating departments to accept fiscal responsibility after the initial year, for the project team to submit a new proposal which includes maintenance for the purchased software licenses to the Green Fund committee for 2013-14, or some combination of the first two options.  Under the current circumstances without any subsequent changes, participating pilot project departments will need to accept fiscal responsibility for vendor support and maintenance after the initial year.',
                'advice' => 'The initial PAR request indicated that the original proposal included partial funding from TEP in the form of a rebate for $15,061.00. This amount would essentially fund the project during years two and three. The rebate is no longer available but the project manager requested the project continue without the additional funding. The board amended the proposal and voted to suspend funding the project as specified in the original proposal until the matching funds could be replaced by the project manager. The matter will be turned over to the FY13 Green Fund Committee and its advisors for further review before funding can be released.',
                'approved' => 0,
                'vp_comments' => '',
                'vp_approved' => 0,
                'created_at' => '2012-04-17 23:44:54.000000',
                'timestamp_review' => '2012-05-04 16:04:04.000000',
                'netid_review' => 'rudnick1',
                'timestamp_vp' => '2012-05-04 16:04:49.000000',
                'netid_vp' => 'rudnick1',
                'project_id' => 350,
            ),
            4 => 
            array (
                'id_old' => 29995,
                'netid' => 'abhappu',
            'changes' => 'We would like an extension to spend our grant monies through the end of summer 2012 (8/10/12), at which time all data collection and analyses will be complete.',
                'advice' => 'This PAR asked for a no cost extension for one year so that they may finish collecting and analyze data.  This extension will expire at the end of FY2013.',
                'approved' => 1,
                'vp_comments' => '',
                'vp_approved' => 1,
                'created_at' => '2012-04-19 00:39:15.000000',
                'timestamp_review' => '2012-05-02 20:40:45.000000',
                'netid_review' => 'rudnick1',
                'timestamp_vp' => '2012-05-02 21:19:54.000000',
                'netid_vp' => 'rudnick1',
                'project_id' => 321,
            ),
            5 => 
            array (
                'id_old' => 29996,
                'netid' => 'taitjohnson',
            'changes' => 'We are requesting an extension of two months time (to August 31 )to complete the project Lights Out: Occupancy Sensors for Lighting Control at CALA.

The reason for the request is to anticipate any possible unexpected contingency in scheduling the installation of the occupancy sensors. At this time, UA Electrical Shop has told us they will install the sensors beginning May 21, 2012, and complete work within two weeks. We do not expect otherwise at this time.

However, if they were for some unforeseen reason (such as an electrical outage, or other electrical event requiring priority staffing as determined by Facilities Management) not able to complete the work on time, the project would require an extension.

Again, we do not anticipate the extra time is needed. However, we want to plan ahead for possible contingencies, given the fact that we do not directly control staffing and installation of the sensors and are reliant upon the schedule of Facilities Management.',
                'advice' => 'This PAR request a no-cost extension in order to be assured the project will finish installation.  This extension will expire August 31, 2012.',
                'approved' => 1,
                'vp_comments' => '',
                'vp_approved' => 1,
                'created_at' => '2012-04-20 03:38:04.000000',
                'timestamp_review' => '2012-05-02 20:41:51.000000',
                'netid_review' => 'rudnick1',
                'timestamp_vp' => '2012-05-02 21:20:33.000000',
                'netid_vp' => 'rudnick1',
                'project_id' => 319,
            ),
            6 => 
            array (
                'id_old' => 29997,
                'netid' => 'rudnick1',
            'changes' => 'The UA Mesquite Bean Harvest has spent approximately $1,693.00 to date. It was awarded $7,200 for FY2011/2012.  The project has spent considerably less than anticipated. This was largely due to the donation of equipment by the City of Tucson, unanticipated in-kind support from Student Affairs (marketing), and the lower-than-anticipated hours logged by the student coordinator.  
Approximately $5,507 of the original budget remains unspent, thus the Mesquite Bean Harvest project would like the Green Fund committee to extend the use of the funds until July 31, 2013.  The proposed Mesquite harvest project intent and outcomes will remain largely unchanged. The project still strives to divert mesquite bean pods from the UA waste stream, engage students in the collection of bean pods as well as in the processing, packaging, marketing, use and sale of the mesquite flour, provide a hands on educational experience for students to learn about an indigenous regional practice, and lastly to scale up the harvest collection and continue to reach a greater number of students on the UA campus and eventually the surrounding community. If additional time is granted by the Green Fund committee, the project will conduct another mesquite harvest on campus this summer, continue to educate the public about mesquite harvesting, solidify market mesquite products with Dining Services and Student Affairs marketing department and develop and evaluate a prototype passive harvesting net. The first harvest season demonstrated a small but adequate amount of Mesquite pods and was by most accounts wildly successful. We would like to increase the amount of pods collected this summer all the while decreasing collection hours and increasing pod yields. The program is currently investigating a prototype harvesting net that may help achieve these results. The net will collect pods as they fall off the tree at peak ripeness and catch them before they land on the ground. Once the pods are on the ground they cannot be used due to risk of contamination. It is for this reason that the Mesquite Bean Program would like to have one more year to spend previously allocated funds. We are also asking for approval to transfer $2000 from the category of student wages, with $500 of that going to the professional services object code and $1500 to the supplies object code.  These changes will facilitate collaboration between the mesquite project and various departments on campus; for example, in FY2012, the Student Affairs marketing team donated many of their student work hours to the project.  Moving forward, more money needs to be spent on marketing our mesquite products (professional services), as well as developing and building nets and other harvesting supplies. See attached spreadsheet for new budget.',
                'advice' => 'This PAR request a no cost extension for one year. They also request approval to transfer $2,000 from student wages and put $500 into the professional services category and $1,500 into the supplies category.   This extension will expire at the end of FY2013.',
                'approved' => 1,
                'vp_comments' => '',
                'vp_approved' => 1,
                'created_at' => '2012-04-20 17:04:28.000000',
                'timestamp_review' => '2012-05-02 21:03:18.000000',
                'netid_review' => 'rudnick1',
                'timestamp_vp' => '2012-05-02 21:20:46.000000',
                'netid_vp' => 'rudnick1',
                'project_id' => 333,
            ),
            7 => 
            array (
                'id_old' => 29998,
                'netid' => 'mrriley',
            'changes' => 'In this Green fund supported project our aim is to construct a small biodiesel production facility at the Campus Ag Center (Campbell and Roger Roads).  We have made great progress in this development with much assistance from our corporate partner (Grecycle).  There is one piece of equipment, a distillation column specifically to remove methanol from the near finished biodiesel, which has required a longer time to design than anticipated.  We had initially thought that we could purchase a commercial piece of equipment, but all available are made of copper (which would then leach into the biodiesel).  We recognize now that we need this made from steel.  We have been developing a design, which needs sign off by a professional engineer from Grecycle, and have identified a local company that can produce this to the necessary specifications.  Unfortunately it will take the fabrication shop longer to produce than anticipated and will likely be delivered in August of this year, after the close of the fiscal year.  

With this likely delivery date being shortly after the end of the fiscal year, it is necessary for us to carry over some of the budget in order to pay the fabrication shop.  There is a small possibility that they will be able to deliver the equipment before June 30, but there are no guarantees on timing.  

We estimate that the equipment will cost approximately $30,000 (it needs to be explosion proof in order to pass safety regulations) and so we request that we carry over $30,000 from our FY12 budget into FY13.  Necessary funds are available in the account.  We are happy to provide any additional information needed.',
                'advice' => 'This PAR request for a no cost extension.  This extension will expire at the end of FY2013.',
                'approved' => 1,
                'vp_comments' => '',
                'vp_approved' => 1,
                'created_at' => '2012-04-20 23:00:41.000000',
                'timestamp_review' => '2012-05-02 21:04:18.000000',
                'netid_review' => 'rudnick1',
                'timestamp_vp' => '2012-05-02 21:21:01.000000',
                'netid_vp' => 'rudnick1',
                'project_id' => 310,
            ),
            8 => 
            array (
                'id_old' => 29999,
                'netid' => 'cfp',
                'changes' => 'Compost Cats 4/20/12 PAR Request



Compost Cats requests adjustment of our 2012 budget in the following ways:

1) We project to be under our projected total budget for 2012 by $5000-6000. We would like to use this money beyond June 30th for additional projected student labor and equipment costs not anticipated in our 2013 budget. 

For labor, we are under budget for 2012, but our successful expansion has meant more student labor hours in recent months. It has become clear that this increased labor need will continue into 2013. We are adding pick up of the UA animal labs’ bedding, an estimated 52 tons of additional waste diverted from the landfill with consequent landfill cost and C02 emissions savings. In addition, the Park Student Union and University Medical Center are now interested in giving us their food waste. This success means more time on compost pick up runs and thus more student labor.

2) For equipment, our compost screener cost $27, 284.50 in the end, over the budgeted $25000. This was an essential piece of equipment, and we went through a lengthy bid acquiring and purchasing process.

3) It is easy to see that we may need some of the remaining 2012 funds in 2013 for a similar equipment cost. This summer we are acquiring a new, more fuel-efficient truck and lift gate to reduce the risk of student worker injury. It has become clear that we will need a truck with a stake bed to accommodate our growing waste quantities. This will likely cost more than the budgeted $18000.

4) We were under budget for total operations, but we did have $209.53 in office supply costs and $332.12 in sign, banner, and other printing costs that we had just lumped under supplies. In the accompanying financial report, we have included those costs with their appropriate object codes.

5) Our borrowed truck’s repeated break downs meant $1291 in unanticipated vehicle rental costs so that compost runs could continue while our truck was in the shop. In addition to making this adjustment, we request that $800 of the money remaining after June 30th be allocated for vehicle rentals in 2013 that we do not have in our 2013 budget. While we expect our new vehicle to save money and be more reliable, in general, break downs may still occur, and we expect a few days of rentals this summer as our new vehicle has a lift gate and other equipment to ensure worker safety and efficiency installed.',
'advice' => 'This PAR approved by the Green Fund Committee in open meeting on 04.26.12. jr',
'approved' => 1,
'vp_comments' => 'Melissa Vito supports the committee recommendation to approve this PAR as of 04.26.12.',
'vp_approved' => 1,
'created_at' => '2012-04-21 00:49:23.000000',
'timestamp_review' => '2012-09-04 15:31:39.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-09-04 15:36:38.000000',
'netid_vp' => 'rudnick1',
'project_id' => 328,
),
9 => 
array (
'id_old' => 299910,
'netid' => 'rudnick1',
'changes' => 'Dear Green Fund Committee,
A great deal of change has occurred in the Community Garden since last year in March when $16,300 was appropriated to the project by the Green Fund committee. The contents of this PAR will explain how the money was spent and will also indicate how GITD would like to spend the remainder of the funding.
First of all, please understand that the person who began as a project manager has graduated and is no longer on campus and the business manager has since retired and she is no longer on campus either. To some degree we have to infer actions. This project was intended to be part funded by the Green Fund in the amount of 16,300 and Student Services Fee in the amount of 10,000.00. The total amount requested and shown on the initial budget was 26,300. It is unknown if the Garden Project was actually awarded this money from Student Services Fee. The original application from ASUA to Student Services Fee asked for 103,650. There were 9 programs requesting funding in this one program proposal. It is unknown if any money was allocated for the Community Garden project as the entire project was only awarded 37,500.  Either way, the Community Garden was never given any money to spend on this project and the money that was not spent in this account has since been swept back into the Student Services Fund. Thus, a project that intended to spend 26,300 only had 16,300 to spend. The Green Fund did not ask for a new budget due to decreased funding nor is it even known if they were aware that the funding was or was not available. The total funding for the Garden Project is 16,300.
In June of 2011, a month before the Green Fund money was allocated; the previous garden project managers were beginning to discuss the implementation of irrigation and the excavation of garden beds with Grant McCormick, a staff member for Planning, Design and Construction. It became apparent at this time that students and volunteers would not be able to dig and place the beds, due to the ground being too hard or be able to put in the irrigation due to the fact that it would be a larger work load than could be taken on by students.  There was also a concern the students did not possess the expertise needed to install an irrigation system correctly. All parties wanted to avoid this. Therefore, with the recommendation of FM; PD&C, the advisers of ASUA; the Community Garden decided that the beds and irrigation should be installed by FM. The problem that was faced with this decision was that it meant allotting nearly all the money that was awarded to the garden to dig garden pits and install irrigation to the garden beds. This left little to no funding remaining for other items that needed to be completed in the garden. After much deliberation, it was decided that the money should be spent on the irrigation, shed and beds with the conclusion that it was the only way the construction of the garden was going to take place. It is now apparent that the garden project should have waited until the Green Fund committee was back in session and submitted a PAR before work was started.  The project paid FM 11,206.39 to excavate, dig and install the sprinkler systems that water each garden plot.  The remaining expenses were utilities/water ($719.47); a shed ($1,704.17), printing ($106.66), and supplies/irrigation parts such as drip tape ($1,418.49). There is currently approximately $1,144.82 remaining from the Green Fund project which will be used for supplies and pay for water during the hot summer months. Overall, there are 45 garden beds, installed irrigation, a shed, limited tools (some borrowed from the Community Gardens of Tucson program that needs to be returned), and miscellaneous items such as a hoses, a shifter, and branch cutters.
Students for Sustainability loaned the Garden Project $875.00 for a computer program that will keep track of the garden plots and the rent they generate. They also loaned the UA Community Garden project $271.00 for a new computer. These items were approved in the project proposal approved in FY2013 Green Fund project.  The computer program will provide security to the UA Community Garden and SFS as organizations since a formal way needs to exist to keep track of the money that will be collected for the renting of the plots. This financial system is a computer program that allows management to organize the information of the renters, such as when payment was made as well as which plots are being rented. Students for Sustainability accepted the financial risk associated with buying these items before they know if the UA Community Garden would get funding for FY13. These items were requested in the budget outlined in FY2013. Students for Sustainability is requested to be reimbursed these fees when the program is funded in July 2012.
The management of the garden has changed and a new governing structure has been enacted. The current project managers of the UA Community Garden are Laura Hanson and Taryn Contento. The original leader of the project, Caleb Weaver, has graduated and left the project. The initial group that was involved in the garden plans at the beginning of the summer was Planning, Design and Construction, the Office of Sustainability, and the Community Gardens of Tucson (CGT). However, part of the original group, the partnership between UA Community Garden and the CGT, was never fully realized and defined. This became a conflict when it came time to discussing who was managing the plots and their owners. Chet Phillips and the ASUA staff began a process to develop a Memorandum of Understanding (MOU) between the UA Community Garden and Community Gardens of Tucson in order to define the relationship, roles and responsibilities of each group.  Although compromises were considered at that time neither group could come to an agreement. At that point, the partnership was terminated between the groups.

In order for the Community Garden to continue to receive guidance, and to keep stakeholders apprised of current events, a Community Garden Oversight Committee was established. This committee currently meets at 9:00 a.m. on Wednesdays. Currently involved in the committee are a faculty advisor, Tom Wilson, the three Students for Sustainability Directors (Michele MacMillan, Chet Phillips and I), and the two current project managers of the garden (Taryn Contento and Laura Hanson). There is also communication between this committee and Facilities Management and Planning, Design and Construction to have one of their staff members become a member of the garden committee. Additionally, as relations are built with the community, a community member will be invited to sit on the board. GITD would also like organizations that not only have an understanding of garden basics but also a wide range of gardening expertise to sit on the committee. Once the project is functioning correctly, the committee will no longer meet weekly, but rather every other week or monthly. Currently weekly meetings are necessary in order to organize and keep everyone up to date on events such as a planting day and ribbon cutting Grand Opening day. Other critical components of the project are also discussed by the committee, such as getting a fence to protect the garden and a sign to define and draw attention to the garden. Committee meetings are open for to the general public, and the group GITD also holds weekly meetings in order to engage outside students in the project.

There are a few things currently planned for the future of the garden financially. We were not able to put in all of the plants listed in the original Green Fund proposal, and there have also been changes in how we are proceeding with the garden as you can see from this short history. We plan on putting in a fence, sign, benches, a Ramada, picnic tables, beautification plants and composting units as we move forward. Each mini project for the garden will have to be presented to the committee and then to FM/ PD&C for approval. Each of these components are necessary for the garden to come together and feel like a community where people will want to be. We will be pursuing the implementation of these components within the next year. We have future funding from the Green Fund and hope to get additional support from other units to add a Ramada and benches.
Recap of requested PAR items:
1)	The UA Community Garden project request an extension to use funds not spent in FY12. They will use these funds ($1,144.82) to pay for water and additional supplies over the next 6 months.
2)	The UA Community Garden project request – post spending- to change spending categories. It is very hard to say what spending categories consisted of before the PAR as the budget had an additional 10 thousand rolled into it. The categories we have now and moving forward are attached in the financial spreadsheet.',
'advice' => 'This PAR request a no-cost extension.  This PAR also request to spend the remaining funds ($1,144.82) to pay for water and additional supplies over the next 6 months. The extension will expire on 10.26.12.',
'approved' => 1,
'vp_comments' => '',
'vp_approved' => 1,
'created_at' => '2012-04-25 16:35:15.000000',
'timestamp_review' => '2012-05-02 21:08:29.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-05-02 21:21:16.000000',
'netid_vp' => 'rudnick1',
'project_id' => 325,
),
10 => 
array (
'id_old' => 299911,
'netid' => 'mkacira',
'changes' => 'Dear GreenFunds Committee,

We have submitted a PAR form on Feb 2012 based on the request from the committee along with a detailed progress report in our project. We have requested a one year no cost extension extension for our project to mainly continue working on the research component in the project. 

With this PAR document, we would like to also request transfer of some of the funds allocated originally under "equipments\' section to the "operations" section. The total amounts we would like transfer from equipments to operations are $24,000. The reason for this is that we have some electronics and system components which are equipments but not exceeding $5,000 limit, but they were charged and shown under operations section on our budget document.

I thank you very much for your kind consideration for this request.

With best regards,
Murat Kacira
Project PI',
'advice' => 'This PAR request, post spending, permission to transfer $24,000 from the equipment budget to the operations budget. Equipment will still be purchased, however, each piece will cost less than $5,000 and thus it will be placed under the operations category.',
'approved' => 1,
'vp_comments' => '',
'vp_approved' => 1,
'created_at' => '2012-04-26 21:35:03.000000',
'timestamp_review' => '2012-05-02 21:14:13.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-05-02 21:21:27.000000',
'netid_vp' => 'rudnick1',
'project_id' => 303,
),
11 => 
array (
'id_old' => 299912,
'netid' => 'mgoode',
'changes' => 'Based on my discussions with Julia Rudnick, I am requesting an extension for this project to allow for more time to expend remaining funds.  The reason I am making the request is due to the fact that I missed the deadline to extend the project due to problems associated with the online reporting system.  Originally, I tried to submit an update back in December of 2011, but after writing a detailed progress report, the text was lost when I tried to submit it electronically.  Then, when asked to submit a PAR to extend the project deadline, again, I apparently had problems with the system, although this time, I did not realize that until some time elapsed and I did not receive an approval of the extension.  I found out later that the PAR had not been received.  To bring the project back into compliance, I have submitted the first progress report that was lost, and the second progress report that is now due.  I have already ordered radio transmitters from the company that supplies them and my intention was to pay for them with remaining funds.  This extension will allow me to do so.',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Matt Goode, GF12.06 Tumamoc Hill: Sustaining Ecosystem Health within the Urban Landscape on 07.19.12. This program requested additional time to finish the project by 12.31.12. Mr. Goode also requested additional time to spend remaining allocated Green Fund Project money. The committee expressed concern about the late submission of the first progress report. The committee also expressed concern that Mr. Goode spent money out of category. He spent an additional $5,500 on student employee wages and underspent by this much in equipment. The committee will grant the PAR due to the fact that the program is in its final completion stage and almost all project money is spent at this time, but will be cautious in regards to future funding.  
The Committee voted 10/0/0 in favor of this PAR during the 10.02.12 open committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on October 8, 2012.',
'vp_approved' => 1,
'created_at' => '2012-07-19 14:17:23.000000',
'timestamp_review' => '2012-10-08 21:53:11.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-10-31 16:27:16.000000',
'netid_vp' => 'rudnick1',
'project_id' => 313,
),
12 => 
array (
'id_old' => 299913,
'netid' => 'nallen',
'changes' => 'I request that our budget be extended to Oct. 1 to provide support for a final volunteer weekend to complete the installation of the project. 

visit: http://vimeo.com/38885602 and http://www.youtube.com/watch?v=zgw3Sk_j5R4

The remaining work includes connecting the solar panels to the inverts and electrical grid, the mounting and wiring of the power inverters, and the powering of the system. This should only require one additional work weekend as previously planned. Additional tasks to be completed on the project includes the installation of monitoring equipment that wil measure the performance of the system and the development and installation of signage to interpret the benefits and accomplishments of the project to B2 visitors. 

The majority of the Biosphere 2 solar install is completed. The original proposed timeline had Earth Day 2011 as the date for completion. Work on the project began in September with site preparation that included grading and the construction of safety railing. Additional work was completed on the electrical panel and trenching in October. Biosphere 2 partnered with the Solar Guild, a local NGO that provides hands-on training in solar installation to those who want to begin a career in solar technology integration. B2 and the Solar Guild held work weekends on Nov. 5&6, Jan. 14&15, and Mar. 3&4. During these three weekends almost 100 volunteers (including high school students, PCC students, UA students, and volunteers front the general public) completed the racking of all 40kW of solar panels, wiring of the panels into strings, and running building the rack for inverters. The final installation weekend was to be the weekend of Earth Day, Apr. 21&22. However, the guild had scheduling conflicts and the work weekend was canceled. B2 was not able to get a final date schedule to complete the system before it became to hot over the summer for the volunteers.',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Nate Allen, GF12.23 Biosphere 2 Solar PV Test Bed on 07.31.12. This program requested additional time to finish the project by 10.01.12. Mr. Allen also requested additional time to spend remaining allocated Green Fund Project money. The committee expressed concern about the late submission of the first progress report. The committee will grant the PAR due to the fact that the program is in its final completion stage and almost all project money is spent at this time. The committee also recommends a project end date of 12.31.12 to allow time for the formal report. 
The Committee voted 10/0/0 in favor of this PAR during the 10.02.12 open committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on October 8, 2012.',
'vp_approved' => 1,
'created_at' => '2012-07-31 20:10:53.000000',
'timestamp_review' => '2012-11-01 18:36:58.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-11-01 18:38:17.000000',
'netid_vp' => 'rudnick1',
'project_id' => 300,
),
13 => 
array (
'id_old' => 299914,
'netid' => 'rbarnes',
'changes' => 'The project is nearly complete, however, installations, conditions and actions such as fixture types, fixture settings, fixture locations, fixture connections, fixture controls, etc., must be reviewed and changed, calibrated, or adjusted, etc., based on student occupation of the currently empty classroom spaces.  Such evaluation, to take place in late August and September, after the students return for the fall semester, requires student feedback and discussion - and more importantly the sensors have to be adjusted for the light-level and reflectivity impact that students have on the space with their desk locations and their bodies.  (Right now and until school starts the sensors are reading light levels from a generally open gray-colored floor.  All that will change when the space is full of students and their rearranged studio desk cubicles.)

Also, over the next few weeks, we will be changing out the sensors in the toilet rooms to be more energy effective with the lighting control system, taking into account the natural lighting available from the frosted windows.

Thanks.',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Wil Peterson, GF12.25, Lights Out: Occupancy Sensors for Lighting Control at CALA on 08.01.12. This program requested additional time to finish the project. Mr. Peterson also requested additional time to spend remaining allocated Green Fund Project money. The committee will grant the PAR due to the fact that the program is in its final completion testing stage and almost all project money is spent at this time. The committee also recommends a project end date of 12.31.12 to allow time for the formal report. 
The Committee voted 10/0/0 in favor of this PAR during the 10.02.12 open committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on October 8, 2012.',
'vp_approved' => 1,
'created_at' => '2012-08-01 23:05:53.000000',
'timestamp_review' => '2012-11-01 18:42:35.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-11-01 18:43:11.000000',
'netid_vp' => 'rudnick1',
'project_id' => 319,
),
14 => 
array (
'id_old' => 299918,
'netid' => 'mrriley',
'changes' => 'We request a change to the budget categories for our ongoing project.  Much of the small equipment purchased so far has fallen below the $10k threshold set by the UA for categorizing as equipment.  To date we have overspent on the operations part of the budget by $15,383 but underspent on the equipment to $47,269.  We request a recategorization to reduce the equipment portion by $30k and increase the operations by the same $30k.  This will in part address the issue with last year\'s budget and will provide flexibility for purchases for the upcoming year. 

We anticipate needing to purchase a number of small pieces of equipment which will fall below the $10k limit.  These include three pumps, filters and filter housings, liquid flow meter, storage drums, piping, and a spill containment system.  The large equipment purchase remaining is approximately $17k needed for the distillation.  We initially had planned on purchasing a commercial distillation equipment but now will use a combination of outside contractor and our own machine shop in order to stay within budget. 

Please contact me if you require any additional information.  
Thank you.  Mark Riley',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Mark Riley, GF12.09, On campus conversion of waste cooking oil to biodiesel to power on campus vehicles on 09.25.12. This was a two year program that is set to conclude on 06.30.13. This program request permission to move $30,000 from the equipment category and place the money into the operations category. The committee will grant the PAR due to the fact that the program has adapted to unforeseen equipment problems as well as project goals remain the same. The Committee voted 10/0/0 in favor of this PAR during the 10.02.12 open committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on October 8, 2012.',
'vp_approved' => 1,
'created_at' => '2012-09-25 21:29:46.000000',
'timestamp_review' => '2012-11-01 19:33:06.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-11-01 19:33:30.000000',
'netid_vp' => 'rudnick1',
'project_id' => 310,
),
15 => 
array (
'id_old' => 299916,
'netid' => 'gwoodard',
'changes' => 'I am requesting the following alterations:
1. No-cost extension of the project through February 2013.  Delays were experienced, as detailed in the progress report.  Installation of cisterns and associated plumbing and pumps will occur this Fall, but must be scheduled around tail gate activities.  Also, this project is linked to the newly funded project to harvest air conditioning condensate at Flandrau and the extension will facilitate linking the two projects.

2. Move $3,000 of the current balance of $3,507.75 in Personnel and $550 of the current balance of $644.40 in ERE to Operations.  This will facilitate continued involvement of one key graduate student who graduated in May 2012 and one key undergraduate student who graduated in December 2011.

Thank you for your consideration.  I would be happy to attend your meeting to answer any questions, and to give an update on both water reuse projects (rainfall harvesting and AC condensate harvesting) at Flandrau Science Center.

Sincerely,

Gary Woodard
850-4249 gwoodard@email.arizona.edu',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Gary Woodard, GF12.28, Reducing the Potable Water Footprint at Flandrau Science Center on 09.25.12. This program requested additional time to finish the project with an end date of February 28, 2013. Mr. Woodard also requested additional time to spend remaining allocated Green Fund Project money. Lastly, Mr. Woodard requested permission to move $3,000 from personnel and $550.00 (corresponding ERE) to operations. The committee will grant the PAR due to the fact that the program is in its final completion testing stage, almost all project money is spent at this time, and the project experienced unforeseen circumstances that produced a project time delay. 
The Committee voted 10/0/0 in favor of this PAR during the 10.02.12 open committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on October 8, 2012.',
'vp_approved' => 1,
'created_at' => '2012-09-17 17:58:49.000000',
'timestamp_review' => '2012-11-01 18:55:58.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-11-01 18:56:24.000000',
'netid_vp' => 'rudnick1',
'project_id' => 335,
),
16 => 
array (
'id_old' => 299917,
'netid' => 'rudnick1',
'changes' => 'Request:
The UA Mesquite Bean project requested and was granted a PAR in April 2012. The project asked to extend out the project completion date to June 2013 as well as move $2000 from student employee salary to operations. At that time, the program wanted to build a passive system that would collect mesquite pods as they fell from the tree while stopping them from dropping to the ground. Once mesquite pods land on the ground they can become moldy and inedible. They are also more prome to pest infestation while on the ground. It was thought that the passive harvest system was going to be expensive to manufacture. Since last April, two mesquite harvest prototypes have been made and materials were not as expensive as originally planned. In fact, the designer actively sought materials that were inexpensive and readily available at the local hardware store in the hopes others could utilize the design easily and increase mesquite bean harvesting. 
To date, the UA Mesquite Bean Harvest has spent approximately $3,924.00. It was awarded $7,200 for FY2011/2012.  The project has spent considerably less than anticipated in the operations category and more than anticipated in the salary category. This PAR request to move $2,000 from operations back to salary/ERE. This will allow the project room to pay student workers as they refine the net prototypes as well as provide outreach to the UA community and surrounding areas.  In fact, this request puts the project money back in the categories in which it was originally funded. We have attached a spreadsheet of our current spending.
Recap:
The proposed Mesquite harvest project intent and outcomes have remained largely unchanged. The project still strives to divert mesquite bean pods from the UA waste stream, engage students in the collection of bean pods as well as in the processing, packaging, marketing, use and sale of the mesquite flour, provide a hands on educational experience for students to learn about an indigenous regional practice, and lastly to scale up the harvest collection and continue to reach a greater number of students on the UA campus and eventually the surrounding community. The project completed another mesquite harvest on campus this summer, continues to educate the public about mesquite harvesting, continues to market mesquite products with Dining Services and Student Affairs marketing department and is refining a net prototype for passive harvesting. The first harvest season demonstrated a small but adequate amount of Mesquite pods and was by most accounts wildly successful. We were not able to increase the amount of pods collected this summer and believe now, more than ever, a passive net system is necessary.  The program is currently working with Levi Van Buggenuma, a student from the College of Architecture and Landscape Architecture. He has developed and built 2 prototypes for passive mesquite collection. On Saturday 10.06.12 from 9-11 am the mesquite project is hosting a Mesquite Bean Charette. The mesquite team has invited interested UA participants as well as involved regional mesquite bean activists. The intent of the Mesquite Bean Charette is to gather feedback and to share the new harvest system with people from the UA and the surrounding community whom are individually harvesting mesquite beans. We welcome participation of the Green Fund Committee and hope members will attend the Charette.',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Ome Eichenberger, GF12.22, UA Mesquite Bean Harvest on 09.19.12. This program requested additional time to finish the project with an end date of June 30, 2013. Ms. Eichenberger also requested additional time to spend remaining allocated Green Fund Project money. Lastly, Ms. Eichenberger requested permission to move $2,000 from operations to student salary and ERE. The committee will grant the PAR due to the fact that the program has adapted, proved fiscally responsible, and the project goals remain the same. The Committee voted 10/0/0 in favor of this PAR during the 10.02.12 open committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on October 8, 2012.',
'vp_approved' => 1,
'created_at' => '2012-09-19 21:26:09.000000',
'timestamp_review' => '2012-11-01 19:19:23.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-11-01 19:19:50.000000',
'netid_vp' => 'rudnick1',
'project_id' => 333,
),
17 => 
array (
'id_old' => 299919,
'netid' => 'mapicell',
'changes' => 'We would like to request an extension on our project to 11/15/13.  The summer sustainability institutes that we are going to send students to take place from mid-June through late July and the disbursement of the award from the Green Fund came too late to register students for them this past summer(examples of summer institutes: http://islandwood.org/school_programs/educators/sesi-2012, http://celfeducation.org/WhatWeDo-summer-middle-register.php, http://www.shelburnefarms.org/educationprograms/professional.shtml).  
The requested extension into the fall will allow the students the time to develop materials and work with us to organize and deliver the workshops to other UA students and to public school teachers, as laid out in the original proposal.',
'advice' => 'The Green Fund Committee recommended approving the request to extend the project deadline. The new deadline is November 15, 2013.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President for Student Affairs, approved the Green Fund Committee’s recommendations on November 30, 2012.',
'vp_approved' => 1,
'created_at' => '2012-11-14 21:46:49.000000',
'timestamp_review' => '2012-12-12 21:00:30.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-12-12 21:04:06.000000',
'netid_vp' => 'rudnick1',
'project_id' => 365,
),
18 => 
array (
'id_old' => 299920,
'netid' => 'gogden',
'changes' => 'Our original budget had all our supply funding listed under "Equipment".  Following submission, I learned that this category was actually for Capital Equipment costing in excess of $10,000 per item.

Thus, I am asking that the Equipment funding and some personnel funding be moved to Operation.

Please clear out line item 56
and adjust Line A35, Operations to $17, 079.68 as noted in the attached spreadsheet.

Also, the students working on the project were very productive finishing must faster than planned, thus we would like to re-allocate personnel funding as follows:  

A20 Appointed & Faculty Regular adjust to $7636.80
A23 Student Employees  adjust to  1520.80

under ERE
A27  Appointed & Faculty Regular adjust to $2315.58
A30 Student Employees   adjust to 47.14

The additional funding under Lines 20 adn 27 will cover troubleshooting and startup activities occuring during fall semester.

Total Green Fund Budget stays the same at $28,600

This combined with a $1500 alumni donation provides a total of $30,100 to cover the entire project cost.',
'advice' => 'The Committee has placed this account on hold due to spending out of category. The Committee has requested additional information and has also requested the project manager complete another PAR.',
'approved' => 0,
'vp_comments' => '',
'vp_approved' => 0,
'created_at' => '2012-11-15 21:46:50.000000',
'timestamp_review' => '2012-11-21 21:13:34.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-11-21 21:14:02.000000',
'netid_vp' => 'rudnick1',
'project_id' => 341,
),
19 => 
array (
'id_old' => 299921,
'netid' => 'gogden',
'changes' => 'the students working on the project were very productive finishing must faster than planned and hence using less of the personel budget than planed.  Thus we would like to re-allocate personnel funding as follows: 

A20 Appointed & Faculty Regular  add projected Expenses Dec-June to 3719.09



under ERE
A27 Appointed & Faculty Regular Projected Expenses Dec-Jun to 1160.00


The additional funding under Lines 20 adn 27 will cover troubleshooting and shakedown activities.

Current Expenditures under Operations, Line 38 for item 5290 add $66.39
line 39, Item 3510  Add $11,100.99

with these line item changes, the total budget expected to be 28,599.64 or $0.36 below the original budget.',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Greg Ogden, GF13.21, Improving Sustainability with Cooling Water Recirculation on 12.11.12. This program request permission to move funds from student salary to appointed and faculty salary along with the corresponding ERE. After several questions asked by the committee and answered by the project manager (see above) the committee voted to grant the PAR.  The Committee voted 05/00/01 in favor of this PAR during the 12.11.12 open committee meeting.
The new budget is attached to this PAR.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on December 14, 2012.',
'vp_approved' => 1,
'created_at' => '2012-11-20 21:58:16.000000',
'timestamp_review' => '2012-12-17 22:36:51.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2012-12-17 22:37:22.000000',
'netid_vp' => 'rudnick1',
'project_id' => 341,
),
20 => 
array (
'id_old' => 299922,
'netid' => 'mkacira',
'changes' => 'Our original project proposed installation of 30 kW photovoltaic system at the Controlled Environment Agriculture Research Center (CEAC). The 5 kW of this total installation was constructed and has been used to provide power needs of an off-grid sustainable greenhouse system for research purposes since April 2012. The rest of the 25 kW PV system installation was projected to be used as surplus energy generated from the PV system that was intended to be returned through the College of Agriculture and Life Sciences (CALS) Campus Agricultural Center (CAC) service meter as energy savings. The initial location of the 25 kW PV system installation was planned to be at the CEAC site. However, due to CALS’s reassessment of current and future site use and allocations, as well as the photovoltaic power system installations at the Animal Science Research Center site, CALS would like to change the location of 25 kW installations (which was put on hold due to CALS re-assessment process) and use the funds for PV system installation at the Animal Science Research Center site. Thus, we request approval of using $26,000 funds from the equipment budget section to be used by CALS for the PV installations and spending at the Animal Science Research Center site for energy savings. Additionally, we request $3,000 travel budget allocation to be moved to the personnel section to be used for student hiring for project.  

Sincerely,

Dr. Murat Kacira
Project PI
Agricultural and Biosystems Engineering',
'advice' => 'Dr. Kacira submitted another PAR on 01.22.13 and asked the Green Fund committee to disregard this PAR. Therefore, the committee does not approve this PAR.',
'approved' => 0,
'vp_comments' => 'Dr. Kacira submitted another PAR on 01.22.13 and asked the Green Fund committee to disregard this PAR. Therefore, the committee does not approve this PAR.',
'vp_approved' => 0,
'created_at' => '2012-12-09 09:32:01.000000',
'timestamp_review' => '2013-01-25 22:17:15.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-01-25 22:18:07.000000',
'netid_vp' => 'rudnick1',
'project_id' => 303,
),
21 => 
array (
'id_old' => 299923,
'netid' => 'marston',
'changes' => 'I am requesting a change to the original budget for the "Growing Sustainable Connections" grant due to the increase in graduate ERE rates that were put into effect for AY 2012/2013.  The original grant included four semesters of research assistantship with some summer money. The latter was expended in 2012 and the research assistantship will be completed this Spring 2013.  The grant will need an additional $2,217 to cover the remaining ERE for Spring 2013.',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Sallie Marston, GF 12.10, Growing Sustainable Connections: the UA, Community Food Bank, and Tucson Schools on 12.11.12. This program requests an additional $2,217 to pay ERE on Graduate Employees. ERE was raised and the program will not have enough money to finish paying the grad student if additional funds are not allocated. The committee voted to grant the PAR. The Committee voted 9/0/0 in favor of this PAR during the 01.29.13 open committee meeting.
The new budget is attached to this PAR.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on January 30, 2013.',
'vp_approved' => 1,
'created_at' => '2012-12-10 23:59:38.000000',
'timestamp_review' => '2013-02-05 20:50:36.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-02-05 21:05:36.000000',
'netid_vp' => 'rudnick1',
'project_id' => 322,
),
22 => 
array (
'id_old' => 299924,
'netid' => 'rudnick1',
'changes' => 'The UA Community Garden (UACG) submits a Program Alteration Request so that the budgeted finances for the project will more accurately reflect the reality of garden spending. Specifically, UACG rescinds its request to build an ocotillo fence surrounding the garden and will replace it with a different design. 

The UACG has exceeded expectations. Almost all plots have been continuously occupied throughout the year. Many infrastructure improvements have been made, including a pollinator-friendly hedgerow, additional beds, and more. A perimeter fence is now needed to mark official garden boundaries and keep out rabbits and javelinas. The original intents and purposes of the fence remain in effect. Because the new fence will cost less than $5,000, the UA Community Garden requests that the entirety of the allocated “Equipment” funds be reallocated to ‘Operations,’ as is reflected in the attached new budget.',
'advice' => 'Chet Phillips asked the committee on 01.13.13 to disregard this PAR.',
'approved' => 0,
'vp_comments' => '',
'vp_approved' => 0,
'created_at' => '2012-12-13 15:34:44.000000',
'timestamp_review' => '2013-01-25 22:22:08.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-01-28 22:46:01.000000',
'netid_vp' => 'rudnick1',
'project_id' => 360,
),
23 => 
array (
'id_old' => 299925,
'netid' => 'mkacira',
'changes' => 'Dear GreenFunds Committee,

Our original project budget file, using the template provided, submitted to the GreenFunds committee for the first time included a total of $69,000 for Equipment category by mistake, which should have been $45,000 allocated for the installation of the 30 kW PV system at CEAC. In the second interim reporting period, the PI recognized this and requested from the Committee to move $24,000 from Equipment category to the Operations category to correctly allocate the funds in the project. The Committee graciously approved the moving of the funds as requested. 

The high tunnel greenhouse structure installed in the project is traditionally simple and free standing hoophouse used for seasonal extension for food production. Their construction, by the farmers, is easy and simpler requiring only adequate amounts of footings and strength to secure the structure to the ground to withstand design loads. However, building such a structure on the university campus required more measures and demanded larger amounts of construction materials especially for the footings to further increase the strength of the greenhouse system and increase its reliability. Thus, this resulted in unexpected and unaccounted operations cost for constructions. In additions, we needed to go through UA Planning, Design and Construction office to obtain the required permits and pay required cost for services provided along with engineering seal and authorization on the design and structures of the greenhouse system. Thus, these services and procedures required unforeseen costs which were not allocated in the original project submission and as budget line items. We presented a very tight budget without any contingency budget allocation during the original project submission. Therefore, these are the main reasons for overspending in the Operations category. 

During the project submission, we aimed and allocated funds to support two graduate students one focusing on solar energy component of the project and the other focusing on biomass waste management component. The first graduate student was recruited to work on the solar energy component, and has been successfully continuing the research project, and is very close to graduting and receiving MS degree under the supervision of the project PI, and thus the funds have been being used for this student as planned. However, the potential graduate student who had initial interest in the biomass waste management component changed the research direction to another topic which was not part of this project. Therefore, we have not been able to recruit a student to work on biomass waste management component, and thus have not used the allocated funds till today.

Since there is overspending in the Operations category in the amount of $12,469, I kindly request permission to move $3,000 from Travel category and $15,000 from Personnel (originally allocated for the 2nd grad student, since there is no sufficient time to recruit a grad student in the originally intended topic and spend this fund until the project ending period which is by 06/13) category into the Operations category.     

Our original project proposed installation of 30 kW photovoltaic system at the Controlled Environment Agriculture Research Center (UA-CEAC). The 5 kW of this total installation was constructed and has been used to provide power needs of an off-grid sustainable greenhouse system for research purposes since April 2012. The rest of the 25 kW PV system installation was projected to be used as surplus energy generated from the PV system that was intended to be returned through the College of Agriculture and Life Sciences (CALS) Campus Agricultural Center (CAC) service meter as energy savings. The initial location of the 25 kW PV system installation was planned to be at the CEAC site. However, due to CALS’s reassessment of current and future site use and allocations, as well as larger photovoltaic power system installations for energy savings at the Animal Science Research Center (Meat Lab) site, CALS would like to use the remaining funds for the PV installations already made at the Meat Labs site. Thus, we request approval of having $26,000 funds from the Equipment category to be used by CALS for the PV installations already made at Meat Labs site. This Animal Sciences Program also supports a wide variety of student related activities, contributes significantly to energy saving in UA campus, and it fits well with the GreenFund program agenda.   

Since our project started, through the grant from GreenFunds Committee, there has been significant interest from researchers, students, and general public about our project. Our project facility has been visited by a large group of visitors including domestic and international visitors, student from elementary level to graduate level, and general public. The facilities have been one of the important and regular technical touring stations for our visitors at the UA-CEAC. Some of the efforts, interest, and project related outcomes were already provided with our previously submitted project report in more details. We are projecting near future collaborative projects from industry and academia wanting to include our alternative energy integrated greenhouse systems as an important component of the planned larger projects. There has been a significant movement and attraction towards local, safe, fresh, pesticide free food production with the technology level similar to one in our project among the consumers in US and also around the world. In this regard, our project has a huge potential to be an example and alternative sustainable crop production system offered to wide range of stakeholders. We also expect that this initial project will lead to extended extramural funding from various funding agencies as well as private sector. Our communications, preparations, and planning is already underway to secure those funds and establish funding sustainability for our project.    

I like to thank GreenFunds Committee for all the support given for the project and kindly request for positive considerations to the requests made for program alterations presented with this document.

Sincerely,

Dr. Murat Kacira, 
Project PI',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Dr. Murat Kacira, GF 12.01, Technologies for Enhancing Food Production, Resource Use Efficiencies and Environmental Friendliness on 01.22.13. This program requests three items. The first thing this program would like to do is to transfer 15,000 from the Graduate Assistant Personnel category and move it into Operations. The second thing this program would like to do is to move 3,000 from the travel budget and place it in operations. The last thing this PAR asks for is to transfer the remaining 45,000 to the Meat Lab site for the PV system already installed. The committee voted on these three items separately. 
1)	The committee voted to allow $15,000 to be transferred from personnel/ERE to Operations.
2)	The committee voted to allow $3,000 in travel funds to be transferred to Operations.
3)	The committee voted down the request to transfer $45,000 to a installed PV instillation at the Meat Lab Site.

The new budget is attached to this PAR.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on January 30, 2013.',
'vp_approved' => 1,
'created_at' => '2013-01-23 06:53:00.000000',
'timestamp_review' => '2013-02-05 22:02:40.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-02-05 22:03:09.000000',
'netid_vp' => 'rudnick1',
'project_id' => 303,
),
24 => 
array (
'id_old' => 299926,
'netid' => 'schmelz',
'changes' => 'We request to make the following changes:

1) Extension of project performance period to December 31, 2013

The current project end date is June 31, 2013. We request an extension of the project period to December 31, 2013.

The storage time points to be evaluated, as lined out in the initial project proposal are 3,6,12,and 24 months. The first 3 time points were evaluated in October 2011, April 2011, and October 2012. Thus the last experiments are to be performed in October 2013. 
The delay is because we trained students how to isolate RNA and DNA from frozen patient cells and samples followed by lessions on PCR methods including spending time for the students to understand the background information. In addition, we had to optimize primers and experimental settings, and perform some trouble shooting of technical and instrumentation problems.
The time until December 2013 is needed to perform the final data analaysis and prepare the manuscript for publication of the accrued data. 

2) Request of $437 to compensate for increase in ERE rates.


3) Transfer of $ 2,993 from category supplies to personnel.

We have currently a remainder of $ 4,293  for supplies. We calculated that we need another $1,300 in supplies and operations to finish the project. With the compensation in ERE rates and the roll over of $2,993 to salaries, we will be able to carry this project through the extended period and finish it.',
'advice' => 'Dear Dr. Vito, 
The Green Fund Committee received this PAR from Monika Schmelz, GF12.30 Reducing Cost, Energy and Space Consumption for Biological Sample Storage –A Collaborative on 02.22.13. This program requested three items. Ms. Schmelz requested additional time to finish the project as well as spend funds after the project end date of 06.30.13. She requested the new project end date be 12.31.13.  Ms. Schmelz also requested an additional $437.00. This amount will be used to pay the increased ERE Graduate Assistant rate that went into effect at the beginning of FY2013. The last thing Ms. Schmelz requested was to move $2,993.00 from operations and place it into Salary. Specifically, she would like to place $2,095 into classified personnel and corresponding ERE and the remaining $898.00 be placed into faculty salary and ERE. The Committee voted 9/0/0 in favor of this PAR during the 03.07.13 Open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on March 8, 2013.',
'vp_approved' => 1,
'created_at' => '2013-02-22 21:19:22.000000',
'timestamp_review' => '2013-03-19 22:59:48.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-03-19 23:00:55.000000',
'netid_vp' => 'rudnick1',
'project_id' => 315,
),
25 => 
array (
'id_old' => 299927,
'netid' => 'rudnick1',
'changes' => 'Compost Cats hereby submits a Program Alteration Request to reallocate the $25,000 previously allocated for a bagging machine/equiptment in order to purchase more immediately necessary large farm equipment that will enable us to continue our operation. Originally we planned to use the bagger to bag compost and then sell it to stores. We anticipate selling compost in the near future; however, we have come to realize we will not need a bagger. Currently we donate compost to local school garden projects and the UA community garden. We donate compost in large quantities. We expect to sell compost by weight and transport the compost via truck and will not need a bagging system. Over the past two years since the bagging machine was originally requested, its necessity for our purposes has decreased. The equipment we request to purchase instead though is absolutely essential. 

The Bobcat skid steer we were using belonged to the Campus Agricultural Center and is no longer available for us to use and we need to acquire a machine owned by the compost project. We have recently been renting equipment on a short-term basis to get by but must quickly find a better long-term solution. The new equipment will be used in our most basic farm work operations including loading, turning, and sifting compost at both our current and any future operating location. Please feel free to contact me, Chet Phillips, with any questions about this request.',
'advice' => 'Dear Dr. Vito, 
The Green Fund Committee received this PAR from Chet Phillips, GF 13.31P Compost Cats on 02.25.13. This program requested to purchase a tractor/Bobcat machine instead of the bagging machine originally listed in the project request. The Committee voted 9/0/0 in favor of this PAR during the 03.07.13 Open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on March 8, 2013.',
'vp_approved' => 1,
'created_at' => '2013-02-25 18:14:09.000000',
'timestamp_review' => '2013-03-19 23:03:53.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-03-19 23:04:25.000000',
'netid_vp' => 'rudnick1',
'project_id' => 362,
),
26 => 
array (
'id_old' => 299928,
'netid' => 'rudnick1',
'changes' => 'The UA Community Garden (UACG) submits a Program Alteration Request so that the budgeted finances for the project will more accurately reflect the reality of garden spending. Specifically, there are two requested alterations. The first is that the UACG rescinds its request to build an ocotillo fence surrounding the garden in order to replace it with a more practical and affordable design detailed below. Secondly, the UACG wishes to move funds from Employees to Operations in order to have secure funding for water bills and maintenance for the remainder of the fiscal year and to aid in repaying irrigation debts to Facilities Management. The Operations account, which covers both water bills and maintenance, is currently empty. We are requesting a transfer of $1,000 from the Employees account to the Operations account to alleviate this, as is detailed in the third paragraph.
In summary of current projects, the UACG has exceeded expectations. Nearly all plots have been continuously occupied throughout the year, the designs for the sign and fence are being completed and both are scheduled for installation by the end of April 2013. It was determined that the ocotillo fence would not be suitable for the purposes for the garden because of higher costs and liabilities, including initial costs, irrigation requirements, a more complex installation process, and potential unsightliness if installed incorrectly. Therefore, the UACG committee has decided that a 3-foot chain-link fence would be a more manageable and fiscally responsible option. Although a chain-link fence is not expected to cost as much relative to an ocotillo fence, the UACG would like to ensure that there is enough money in the Equipment account to have it constructed by semester’s end. Facilities Management quoted the fence at $11,000, which is far too costly given our current circumstances, so we are currently looking into other options.
The decision to allocate $1000 to Operations was made through calculating the amount of money that would be needed to sustain watering throughout the remainder of the fiscal year and to cover costs of irrigation installed by Facilities Management. It is projected that water will cost approximately $350/month (for a total of $1700 through July of this year) and $700 is owed to FM. The total of this cost is approximately $2400 and the $700 owed to FM would be paid off first. Due to renewals of plot rentals, the UACG should be able to absorb the remainder of future water bills and maintenance for this funding year. However, should that prove untrue, Students for Sustainability will absorb additional costs.',
'advice' => 'Dear Dr. Vito, 
The Green Fund Committee received this PAR from Chet Phillips, GF 13.31P UA Community Garden on 02.25.13. This program would like to change the type of fence installed from Ocotillo to chain link. The second thing the project would like to do is to transfer $1,000.00 out of Employee Salary and move it to Operations to pay for unexpected water costs.  The Committee voted 9/0/0 in favor of this PAR during the 03.07.13 Open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on March 8, 2013.',
'vp_approved' => 1,
'created_at' => '2013-02-25 18:18:52.000000',
'timestamp_review' => '2013-03-20 20:50:52.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-03-20 20:51:12.000000',
'netid_vp' => 'rudnick1',
'project_id' => 360,
),
27 => 
array (
'id_old' => 299929,
'netid' => 'gwoodard',
'changes' => 'We request a time extension for this project to allow us to excavate the Flandrau lawn and install the cisterns in a time window that does not contain any scheduled uses of the area.  We anticipated installation in Fall 2012, but a handful of activities, including 4 Dean’s football tailgate parties on the Flandrau lawn, did not provide an adequate window of opportunity.  There were no conflicting activities scheduled during the holiday break, but key people were not around during the closure.

Flandrau has continued to host a number of high-profile events in early 2013, but the last one is the Tucson Festival of Books, on March 9 & 10.  After that, we can schedule the excavation and get the cisterns installed.  This will provide enough time to plumb the cisterns and install the pump to the modified irrigation system well before the summer monsoon.

I am available and willing to attend your upcoming meeting if you have any questions or would like more information.

Sincerely,

Gary Woodard',
'advice' => 'Dear Dr. Vito, 
The Green Fund Committee received this PAR from Bill Plant, GF 12.28 Reducing the Potable Water Footprint at Flandrau Science Center on 02.25.13. This program requested additional time to finish the work of the project as well as spend remaining money on the project past the project end date of 06.30.13. The project asked for the new project end date to be 12.30.13.  The Committee voted 8/0/1 in favor of this PAR during the 03.07.13 Open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on March 8, 2013.',
'vp_approved' => 1,
'created_at' => '2013-02-26 00:29:24.000000',
'timestamp_review' => '2013-03-19 23:08:02.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-03-19 23:08:21.000000',
'netid_vp' => 'rudnick1',
'project_id' => 335,
),
28 => 
array (
'id_old' => 299930,
'netid' => 'mzucker',
'changes' => 'My primary concerns relate to student and technician support on the project.  In effort to support data collection on this project conducted by an undergraduate and technician in July and August of 2013, I\'d like to be able to spend funds on this grant beyond the June 30 target date. We will complete installation of the project in May, so the remaining funds should be these two budget items (classified regular staff, and student employees).',
'advice' => 'Dear Dr. Vito, 
The Green Fund Committee received this PAR from Mitchell Pavao-Zuckerman, GF 13.01 Using Green Infrastructure to Maximize Ecosystem Functions and Services in Cities on 04.22.13. This program requested additional time to finish the work of the project as well as spend remaining money on the project past the origional project end date of 06.30.13. The project asked for the new project end date to be 12.30.13. The Committee voted 9/0/0 in favor of this PAR during the 04.30.13 Open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on May 01, 2013.',
'vp_approved' => 1,
'created_at' => '2013-04-22 22:09:31.000000',
'timestamp_review' => '2013-05-02 18:01:01.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-05-02 18:01:41.000000',
'netid_vp' => 'rudnick1',
'project_id' => 338,
),
29 => 
array (
'id_old' => 299931,
'netid' => 'mapicell',
'changes' => 'There are two changes that we need to make to our project.  First, we have only  two interns who have the interest and ability to attend the summer sustainability institute.  We had proposed sending four.  We have determined that it would be beneficial if I were to take the place of one of the four interns at the institute, and help the two who are going to organize the fall workshop.
In addition, the price of airline tickets has gone up significantly in the year since I first submitted a Green Fund application.  They have gone from about five hundred dollars to over seven hundred.  Since there would only be three of us going, we would like to move four hundred dollars from the room and board budget(currently $2000) to the air/ground transport budget(currently $2000). This would make the air/ground budget $2400, and the room/board budget $1600.
Flight prices on kayak.com: http://www.kayak.com/#/flights/TUS-HPN/2013-07-14/2013-07-19',
'advice' => 'Dear Dr. Vito, 
The Green Fund Committee received this PAR from Morgan Apicella, GF 13.05P Promoting Student Leadership in Sustainability Education on Campus and in the Community on 04.23.13. This program would like to send one less student to the planned education seminar as well as move $400.00 from the operations category to the travel out-of-state category to account for higher than expected airfare cost. The Committee voted 9/0/0 in favor of this PAR during the 04.30.13 Open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on May 01, 2013.',
'vp_approved' => 1,
'created_at' => '2013-04-23 15:17:28.000000',
'timestamp_review' => '2013-05-02 18:44:04.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-05-02 18:44:36.000000',
'netid_vp' => 'rudnick1',
'project_id' => 365,
),
30 => 
array (
'id_old' => 299932,
'netid' => 'wplant',
'changes' => 'I am requesting a PAR due to the complexity of this project and the number of different departments and players required in executing the substantial excavation, plumbing and landscaping work that remains. Due to how busy all of these folks are it has been challenging scheduling all of the necessary meetings to move forward. While we are working aggressively to get this project finalized and much progress has been made, we acknowledge that Flandrau is in such a prominent spot on campus that all of the many details of this project must be considered and planned for carefully so Flandrau and Green Fund will have what we have envisioned all along, a showcase for sustainability that also creates a more usable and enjoyable space for the public. We anticipate completion of this project in late fall/early winter 2013. Thank you for your consideration.',
'advice' => 'Dear Dr. Vito, 
The Green Fund Committee received this PAR from Bill Plant, GF 12.28 Reducing the Potable Water Footprint at Flandrau Science Center on 04.23.13. This program requested additional time to finish the work of the project as well as spend remaining money on the project past the project end date of 06.30.13. The Project Manager asked for the new project end date to be 06.30.14. The Committee voted 9/0/0 in favor of this PAR during the 04.30.13 Open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on May 01, 2013.',
'vp_approved' => 1,
'created_at' => '2013-04-23 16:21:38.000000',
'timestamp_review' => '2013-05-02 18:56:57.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-05-02 18:57:26.000000',
'netid_vp' => 'rudnick1',
'project_id' => 335,
),
31 => 
array (
'id_old' => 299933,
'netid' => 'wplant',
'changes' => 'As the plumbing for this project ties directly into our other Green Fund project, "Reducing the Potable Water Footprint at Flandrau Science Center", we are requesting a PAR for the same reasons described there, namely, anticipated challenges in scheduling all of the complex work in time before June 30th. Thank you for your consideration.',
'advice' => 'This PAR was withdrawn on 04.25.13 via email to Julia Rudnick. A more detailed PAR was submitted for this project on 04.25.13. Please see PAR GF-36 for more information.',
'approved' => 0,
'vp_comments' => 'Dr. Melissa Vito, Vice President of Student Affairs, did not approve this PAR as it was withdrawn on 04.25.13. Please see PAR GF-36 for more information.',
'vp_approved' => 0,
'created_at' => '2013-04-23 16:27:53.000000',
'timestamp_review' => '2013-05-02 18:50:13.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-05-02 18:52:18.000000',
'netid_vp' => 'rudnick1',
'project_id' => 335,
),
32 => 
array (
'id_old' => 299934,
'netid' => 'spoe',
'changes' => 'We would like a no-cost extension to allow time for student clubs to participate in collection of waste oil and the making and distribution of the Biodiesel. The plant will be running by the end of the current contract, but won\'t have been educationally available to large numbers of students. By extending the contract we will be able to allow many student groups the opportunity to either work with or view the process.

We would like to request moving $3,000 from equipment to student employee. This will allow us to pay a student over the summer to help continue the process plant. This will cover salary and ERE.

We would like to request moving $15,000 from equipment to operations. This move is necessary to cover the cost of the drying towers, pumps, and other facility items that cost less than $5,000 each.

Thank you for your consideration and help with this important project.',
'advice' => 'Dear Dr. Vito, 
The Green Fund Committee received this PAR from Steven Poe, GF 12.09 “On campus conversion of waste cooking oil to biodiesel to power on campus vehicles” on 04.23.13. This program requested additional time to finish the work of the project as well as spend remaining money on the project past the project end date of 06.30.13. The Project Manager asked for the new project end date to be 06.30.14. The project manager also request moving $3,000 from the equipment category to the Student employee and associated ERE category. The project also request $15,000 be moved out of the equipment category and be placed into the operations category. The committee verified the same equipment would be purchased as planned for in the initial project. The Committee voted 9/0/0 in favor of this PAR during the 04.30.13 Open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on May 01, 2013.',
'vp_approved' => 1,
'created_at' => '2013-04-23 18:23:46.000000',
'timestamp_review' => '2013-05-02 19:08:48.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-05-02 19:09:11.000000',
'netid_vp' => 'rudnick1',
'project_id' => 310,
),
33 => 
array (
'id_old' => 299935,
'netid' => 'mkacira',
'changes' => 'Dear GreenFunds Committee,

We request program alterations and adjustments in budget in the following ways:

1.	We request a no cost extension till the end of 2014 Fiscal year to continue in our project and also be able to use the funds available from our project. 

We were delayed for a year to start our project with system installations (especially with the PV system installations) due to being a smaller portion of larger PV system installations conducted by CALS. Thus, we naturally started our project back in April 2012. We had great progress and success in the project implementation, research, educational and outreach components which were described in more detail in the previous project progress reports.   

2.	In previous PAR, we request $26,000 to be used by CALS for PV installations at Animal Science Center, and the request was rejected. Thus, we request transfer of this total $26,000 from Equipment category into the following categories as:

Travel	: $3,500 (for the grad student from the project and PI to attend 2013 ASABE to make   an oral presentation about project results). Grad student is expected to graduate by end of Summer 2013 and will be a good opportunity to make an oral presentation at an international conference.

Student	: $15,000 (to hire a new graduate student who will continue research on improving efficiency of DC/DC PV integrated greenhouse system, and involve in solar energy integrated greenhouse system research program.  

Operations: $7,500 (to help supporting the projected expenses related to utilities, hydroponics production system supplies, instrumentation, contingency etc.)

Since our project started, through the grant from GreenFunds Committee, there has been significant interest from researchers, students, and general public about our project. Our project facility has been visited by hundreds of visitors including domestic and international visitors, student from elementary level to graduate level, and general public. The facilities have been one of the important and regular technical touring stations for our visitors at the UA-CEAC. Some of the efforts, interest, and project related outcomes were already provided with our previously submitted project report in more details. We are projecting near future collaborative projects from industry and academia wanting to include our alternative energy integrated greenhouse systems as an important component of the planned larger projects. We also expect that this initial project will lead to extended extramural funding from various funding agencies as well as private sector for continued alternative energy integrated controlled environment agriculture food production systems. Our communications, preparations, and planning is already underway to secure those funds, by leveraging our project and research program, and establish future funding sustainability for our project.    

On behalf of our project team, I thank GreenFunds Committee for all the support given for the project and kindly request for positive considerations to the requests made for program alterations presented with this document.

Sincerely,

Murat Kacira, Project PI',
'advice' => 'Dear Dr. Vito, 
The Green Fund Committee received this PAR from Dr. Murat Kacira, GF 12.01 Technologies for Enhancing Food Production, Resource Use Efficiencies and Environmental Friendliness on 04.23.13. This program would like to move $3,500 from equipment to travel out-of-state to send the Project Manager and a graduate student to an ASABE conference. The program would like to move $15,000 from equipment to graduate student salary and ERE. The program would also like to move $7,500 from equipment to operations to support the current project and its operating costs. Lastly, the project would like an extension to spend remaining money and work on the project until 06.30.14. The Committee voted 0/8/1 to disallow this PAR during the 04.30.13 Open Green Fund Committee meeting.',
'approved' => 0,
'vp_comments' => 'Dr. Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on May 01, 2013.',
'vp_approved' => 0,
'created_at' => '2013-04-23 18:51:02.000000',
'timestamp_review' => '2013-05-02 22:31:38.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-05-02 22:32:21.000000',
'netid_vp' => 'rudnick1',
'project_id' => 303,
),
34 => 
array (
'id_old' => 299936,
'netid' => 'wplant',
'changes' => 'We are requesting a PAR for this project as it is tied directly into the work on the main Green Fund water harvesting project at Flandrau, GF12.28. The main reason for requesting a PAR is due to the complexity of this project and the number of different departments and players required in executing the substantial excavation, plumbing and landscaping work that remains. Due to how busy all of these folks are it has been challenging scheduling all of the necessary meetings to move forward. While we are working aggressively to get this project finalized and much progress has been made, we acknowledge that Flandrau is in such a prominent spot on campus that all of the many details of this project must be considered and planned for carefully so Flandrau and Green Fund will have what we have envisioned all along, a showcase for sustainability that also creates a more usable and enjoyable space for the public. We are requesting a new completion date of 6/30/2014.
Thank you for your Consideration.
Bill Plant',
'advice' => 'Dear Dr. Vito, 
The Green Fund Committee received this PAR from Bill Plant, GF 12.28 Reducing the Potable Water Footprint at Flandrau Science Center on 04.23.13. This program requested additional time to finish the work of the project as well as spend remaining money on the project past the project end date of 06.30.13. The Project Manager asked for the new project end date to be 06.30.14. The Committee voted 9/0/0 in favor of this PAR during the 04.30.13 Open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on May 01, 2013.',
'vp_approved' => 1,
'created_at' => '2013-04-25 20:40:29.000000',
'timestamp_review' => '2013-05-02 18:58:04.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-05-02 18:58:32.000000',
'netid_vp' => 'rudnick1',
'project_id' => 363,
),
35 => 
array (
'id_old' => 299937,
'netid' => 'rudnick1',
'changes' => 'I am requesting that the $1176 that remains in the the green fund account for the "service learning and student awareness of sustainability education" grant (2233010) be transferred from equipment to student salary and ERE. The monies would be used to complete analysis on two types of data collected to assess the impacts of the UA community and school garden program on service learning and sustainability education.  The first set of data was collected for two semesters each week from all the interns who participated in the program (677 surveys).  These are quantitative data and they cover variables such as hours worked, tasks undertaken and completed, groups supported, etc.  The second set of data are qualitative and focus on a much smaller group of interns who recorded the effects of the program on them through field notes and autoethnographic reflections.  Both of these data would be analyzed by Jeffrey Wilson.  Jeff is the graduate student who ran the course in which the assessment of the program as a research project was undertaken.  He trained 6 undergraduate interns to undertake ethnographic work and then supervised their data collection with respect to other interns as well as the school children and teachers we serve.  There is no one more familiar with the data than Jeff and he is both willing and able to undertake the qualitative and quantitative analysis needed to complete the assessment promised in the grant proposal.

Thanks for your consideration.
Sallie',
'advice' => 'Dear Dr. Vito,
Professor Sallie Marston contacted our office on 06.12.13 asking the Green Fund to approve a PAR for her 2013 project, Service Learning and Student Awareness of Sustainability Education. She is requesting remaining funds of  $1,178 dollars be transferred from an operations account to a student salary account. This transfer will allow the graduate student most familiar with the project additional time to compile a qualitative and quantitative assessment of the project.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 06.13.13.',
'vp_approved' => 1,
'created_at' => '2013-06-12 22:24:55.000000',
'timestamp_review' => '2013-07-02 22:49:43.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-07-02 22:50:29.000000',
'netid_vp' => 'rudnick1',
'project_id' => 355,
),
36 => 
array (
'id_old' => 299939,
'netid' => 'cmoeller',
'changes' => 'Hello,

As originally proposed the grant was going to fund Ann Chanecka to teach a planning course and incorporate a research report on the before and after affects of the green lane marking along Mountain Ave. into the curriculum of the class.  The Green Fund Committee decided to not fund Ms. Chanecka teaching the course, but she is not a member of the permanent faculty of the School and without funds was not hired to teach the course and complete the research project.

The project can still move forward, as the Green Fund is proposing to fund the green lane marking and the City of Tucson is still willing to partner to complete that portion of the project.

The research project and it\'s student involvement on the effectiveness of the lane marking will no longer be completed, but the marking can still occur as originally proposed.

If you have any questions, or need additional information, please contact me.

Thank you,

Colby',
'advice' => 'Dear Mr. Moeller,
The Green Fund Committee reviewed your program alteration (PAR) request on October 1, 2013. They reaffirmed their support of the project as originally approved by the committee. They do not support the project if the student engagement portion is withdrawn. Therefore your PAR was denied.    
Thank you,
The Green Fund Committee',
'approved' => 0,
'vp_comments' => '',
'vp_approved' => 0,
'created_at' => '2013-09-06 01:41:47.000000',
'timestamp_review' => '2013-10-10 22:00:04.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2013-12-09 22:46:59.000000',
'netid_vp' => 'rudnick1',
'project_id' => 402,
),
37 => 
array (
'id_old' => 299940,
'netid' => 'rudnick1',
'changes' => 'Project GF13.35, Sustainability Program Assessment, by Shoshanna Mayden formally concluded on 12.31.13. The project was short ERE funds in the amount of $250.52. Please let this PAR serve as a formal request for an additional $250.52 due to slightly higher than expected ERE rates. While Shoshana’s work with the project has officially concluded the work of the project continues. BARA along with the Office of Sustainability will be distributing and promoting the final project deliverables to the UA campus community as well as presenting the work in various conferences. The Office of Sustainability has contributed funds of $14,058.78 to date. Under the supervision of BARA, the Office will continue to pay 2 students to finish and promote the work. While this payment will formally conclude the project, final findings and final report will be submitted to the Green Fund.',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Julia Rudnick, GF 13.35, Sustainability Program Assessment on 01.15.14. This program requested an additional $251.00 to pay for additional ERE costs. 
The Committee voted 8/0/0 in favor of this PAR during the 01.28.14 open committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on January 30, 2014.',
'vp_approved' => 1,
'created_at' => '2014-01-15 19:10:31.000000',
'timestamp_review' => '2014-02-11 18:54:38.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-02-11 19:18:19.000000',
'netid_vp' => 'rudnick1',
'project_id' => 361,
),
38 => 
array (
'id_old' => 299941,
'netid' => 'mlenart',
'changes' => 'As the LEAF on the UA campus project begins its second semester, it has become clear that some of the funding left unused would serve well to help support the graduate students who are continuing to dedicate many hours to the project. The graduate students, Angela Knerl and Alexis Arizpe, led the LEAF effort to harvest olives in the fall. This spring, they will be devoting their efforts to marketing the products of the harvest (olive oil) as well as contributing their expertise to citrus harvest efforts. In particular, they have become savvy at dealing with the media. They have also expressed some willingness to help organize and report LEAF meetings and projects.

Specifically, we would like to allocate $1,200 for support for graduate students this spring. This is identical to what we provided in the fall. With the combined ERE of $800, this request would amount to $2,000.  

We propose to take the funding from the unused portion of fall funding for undergraduate student interns, which amounts to about $3,400.  At the graduate student hourly rate, this translates to one or both students working a combined total of about five hours a week during the 16 weeks of the semester. 

Less than half (about 40%) of the money allocated for undergraduate student interns during the summer and fall was used by the interns. Please see attached document showing that only $2,331.63 of the $10,600 budgeted for undergraduate student interns was used as of January 17, 2014. Of the $10,600, $5,800 had been allocated for use during the summer and fall of 2013. 

About $1,000 of the money budgeted was left unused because of the Office of Sustainability decided to the support the student who was designated as the “mesquite intern” during the summer of 2013. Thus the funding did not come from the LEAF budget. The intern, Maya Cross-Killingsworth, joined LEAF efforts in the fall, but she only worked a few hours before she left the project because of a heavy course schedule and another job.

We followed up by hiring Elondra Ome Eichenberger to replace her. Similarly, Elondra claimed only a few hours before deciding that working as a manager at Chipotle in addition to doing her coursework was preventing her from fully engaging in the LEAF work. Elondra has decided to continue to contribute to the project by writing up some of the details on the mesquite harvest conducted under the Office of Sustainability, but she will be doing so as a capstone thesis for her Sustainable Built Environments program. As such, she is declining to draw LEAF funding for her contributions.

We have hired a third intern, Victoria Scaven, to help out generally with the LEAF project. She started working in late December. The other two student interns, Lisa Van Wagenen and Ty Trainer, will continue to work on the project. Both of them also have other jobs, which is partly why some of the money designated to support them for 10 hours a week during the fall semester was not worked or claimed.

Just in case all three student undergraduates claim their full 10 hours a week during the spring semester, we have set aside sufficient funding for this purpose. Even so, there remains enough unused funding that had been budgeted for the fall semester and the summer mesquite project to support the two graduate students at the same level they received during the fall. 

Both students are putting into far more hours than they are being paid for, and without complaint. As the project manager, I would like to see them rewarded for at least some of their hard work. I hope the UA Green Fund committee will agree that these hard-working students deserve some financial compensation. 

We thank the members of the committee for considering our request. We appreciate the work you do in keeping the student oriented projects running smoothly.

Submitted by Melanie Lenart on January 20, 2014.',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Melanie Lenart, GF 14.15, Linking Edible Arizona Forest (LEAF) on the UA Campus on 01.20.14. This program requested moving $2,000 from undergraduate salary to graduate salary. 

The Committee voted 8/0/0 in favor of this PAR during the 01.28.14 open committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on January 30, 2014.',
'vp_approved' => 1,
'created_at' => '2014-01-20 21:10:19.000000',
'timestamp_review' => '2014-02-11 19:01:37.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-02-11 19:19:18.000000',
'netid_vp' => 'rudnick1',
'project_id' => 396,
),
39 => 
array (
'id_old' => 299942,
'netid' => 'hhopkins',
'changes' => 'The RHNA Community Garden has completed the removal of the debris but would like to request that the monies that are left would be allotted for refitting our irrigation system to something more secure and long-term for our existing garden beds. During this past Fall it became evident that the garden cannot be financially sustainable, if we are to dig 28 more garden beds which we received funding for. The water bill would go up exponentially during the summer months and would cripple the garden from May to September. Also, the garden experienced a few unfortunate water leaks, which is another reason why we are proposing this change. This is why we are asking for the following changes for the monies (approximately $2000) that we still have available to us:
Centralized time system that does not require electricity (other than batteries or small solar panel), piping for centralized system, 5 additional stand pipes, and refitting all beds in rows A-D.

Currently each row of garden beds are set on separate timers and having one centralized system would decrease the risk of additional water leaks.',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Heidi Hopkins, GF 14.16P, RHNA Community Garden on 01.21.14. This program requests to change the project by not digging additional garden beds as outlined the the original proposal and to use the cost savings to improve the current inadequate irrigation system. 

The Committee voted 9/0/1 in favor of this PAR during the 03.04.14 open committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on March 5, 2014.',
'vp_approved' => 1,
'created_at' => '2014-01-21 20:09:29.000000',
'timestamp_review' => '2014-03-28 19:20:59.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-03-28 19:41:45.000000',
'netid_vp' => 'rudnick1',
'project_id' => 401,
),
40 => 
array (
'id_old' => 299943,
'netid' => 'kimiapakfar',
'changes' => 'Dear Green fund committee,
As you may know the project “House Energy Doctor Online Distance Deliverable Curriculum and Certificate in Energy Conservation“ have been approved to be granted by Green fund in February 20, 2012 with amount of $12,200. Accordingly the two graduate students (Kimia Pakfar, Suzan Borazjani) have been working on this project for a long time, from August 2013.  Therefore they are now in a high level of understanding the project. Undeniably they are completely familiar with energy aspect and all the related details of the project more than any other students.
At the end of December 2013, we figured out that since they have become graduate assistant, the considered ERE has increased to %59.5, which results in taking more money from the budget compared to what was expected. In this situation we were offered 3 solutions:
1.	Requesting more money
2.	Replacing the graduate students
3.	Keeping just one of them
Consequently we chose the first option for several sensible reasons. First of all, at this time replacing them is not possible. As they have worked on this project for more than 6 months, currently they are even more experienced. For sure they are best match to finish this project. Moreover they are part of this project now and there is no way that the project can be done without them due to their familiarity how to continue the work. Secondly, they are both integrated in this project; the amount of work would be too much for one of them as well. This will prevent the project to be well done.
So in order to finish the project, we will need to reach the same working hours as expected in the first place. The graduate students both combined have got almost $4,200 out of $9,600 (for 350 hours work) that has been approved to be given to them. In order to finish the project with the same quality expected, the project team needs to ask for more $6,200. 
Explaining in detail, if we want to continue the project with 59.5% ERE and the remained budget, we would be left just with 98 more working hours combined for both graduate students. [The remaining budget (excluding the operations) is $2,670. Considering 59.5% ERE, we will be left with 98 working hours which makes it impossible to finish the project]. If we were paying the 4% ERE as the first place, we would have $5,000 left which would give us 417 working hours. In current situation with 59.5% ERE we will need to ask for more $6,200 to be able to reach the same working hours and desirable project quality. 
Attached is more detailed calculation to clarify our request.

Sincerely,
‘House Energy Doctor Online Distance Deliverable Curriculum and Certificate in Energy Conservation ‘Project Team',
'advice' => 'Dr. Vito, 
The Green Fund Committee received this PAR from Kimia Pakfar, GF 14.13, House Energy Doctor Online Distance Deliverable Curriculum and Certificate in Energy Conservation on 02.24.14. This program originally received funding to hire students for an hourly pay rate plus student ERE. The original proposal did not specify hiring and paying graduate students pay rate, ERE and tuition reimbursement. The project requests permission, post employment, to pay graduate students pay rate, ERE and tuition reimbursement. Consequently, the project has not concluded and the student team does not have enough funds to conclude the project. The project is also asking for an additional $6,200 to wrap up the work of the project. 

The committee approved by a vote of 0/9/1 to allow post graduate employment. The committee does not approve by a vote of 0/9/1 the additional $6,200 funding request.',
'approved' => 0,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs, approved the recommendations of the Green Fund Committee on March 5, 2014.',
'vp_approved' => 0,
'created_at' => '2014-02-24 19:24:21.000000',
'timestamp_review' => '2014-03-28 19:40:33.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-03-28 19:42:07.000000',
'netid_vp' => 'rudnick1',
'project_id' => 404,
),
41 => 
array (
'id_old' => 299944,
'netid' => 'uafrank0',
'changes' => 'AGTM LABORATORY PHOTOVOLTAIC TEACHING STATIONS PAR

We are submitting a program alteration request (PAR) for the funded project "AGTM Laboratory Photovoltaic Teaching Stations" to request a transfer funds from the "Equipment" portion of the grant budget to the "Operations" portion of the grant budget. We have been awarded $5,100.00 in the Equipment portion of the budget to acquire a storage container for the purpose of increasing our usable storage space for PV projects constructed by the AGTM 350 student groups, as well as our expanding inventory of PV-related teaching tools and materials.

We contacted three local vendors requesting updated quotes for a storage container to be placed near our teaching laboratory at the UA Campus Agricultural Center (CAC). The quotes range from approximately $4,700.00 to $5,300.00. During the process of receiving quotes, we were notified by our Farm Manager of the UA Campus Agricultural Center that we have an opportunity to obtain a storage container through federal surplus. The container is located in the Buckeye area. If the transfer from federal surplus is accepted, we will be able to have it transported to the UA Campus Agricultural Center. A request made by the UA Campus Agricultural Center is that the storage container be prepped and painted to closely match the storage containers already in place and in use at the UA Campus Agricultural Center. 

We request the funds moved to the Operations portion  be used for expenses related to moving the container from Buckeye and delivering it to the CAC ($575.00), to cover the expense of labor & materials for prepping and painting the container, and securing it with a lock (approximately $500.00-$750.00).  Additionally, remaining funds placed in the Operations portion of the budget would be used for installing a security light (motion detector) on the outside of the container (approximately $100.00), to acquire interior lighting materials (LED lamps, metal conduit, switch box, metal lamp holders, conductors, mounting materials & fasteners; approximately $300.00) to provide visibility, a solar-powered ventilation fan ($300.00) to aid in temperature control inside the container, and for a battery-based PV system to provide power for lighting inside the container and to demonstrate to AGTM students how a stand-alone battery-based PV system, similar to the type used in a mountain cabin, is installed and operates (approximately $3,100.00.

Lastly, we request to extend the deadline for completion of this project from June 30, 2014, to November 30, 2014. This action will provide an opportunity to engage our students enrolled in the AGTM 350 course during the fall semester in the installation of the battery-based PV system, as well as both interior lighting and exterior security lighting components, and the solar-powered ventilation fan.',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Ed Franklin, GF 14.29, AGTM Laboratory Photovoltaic Teaching Stations on 03.23.14. This program requests to move $5,100 from equipment to operations. The project also requests to extend the project end date to 11.30.14. 

The Committee voted 9/0/1 in favor of this PAR during the 04.15.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs and Enrollment Management, approved the recommendations of the Green Fund Committee on April 23, 2014.',
'vp_approved' => 1,
'created_at' => '2014-03-23 19:07:41.000000',
'timestamp_review' => '2014-04-21 18:18:57.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-04-23 23:19:15.000000',
'netid_vp' => 'rudnick1',
'project_id' => 370,
),
42 => 
array (
'id_old' => 299945,
'netid' => 'powersj',
'changes' => 'The PC Power Management project selected software which was less per license than budgeted.  The budgeted estimated was for 2250 licenses at $16.50 per license with support.  The purchased software from Verismic was $4.80 per license.  We purchased the 2250 licenses required for the pilot project for a total of $11404.80 with tax.   With the allotted funds of $37,300 for software, the UA Green Fund could purchase an additional 5000 licenses for 
distribution to campus.  The project would need to be extended for an additional six months minimum to complete the pilot project in the Medical College, evaluate the results, and then potentially turn distribution of the software over to UITS.  In addition, the student employee has not been able to start yet 
and we will be rushed to get his 120 scheduled hours in before June 30th.  We are asking for an additional six months and permission to spend the already allocated funds on additional licenses.',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from John Powers, GF 14.29, PC Power Management on 03.28.14. This program was able to purchase less expensive software licenses than budgeted. The project would like to use the cost savings to purchase an additional 5,000 licenses than proposed in the initial project request.  The project also requests to extend the project end date to 12.30.14. 

The Committee voted 9/0/1 in favor of this PAR during the 04.15.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs and Enrollment Management, approved the recommendations of the Green Fund Committee on April 23, 2014.',
'vp_approved' => 1,
'created_at' => '2014-03-28 22:00:44.000000',
'timestamp_review' => '2014-04-21 18:23:19.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-04-23 23:19:35.000000',
'netid_vp' => 'rudnick1',
'project_id' => 392,
),
43 => 
array (
'id_old' => 299946,
'netid' => 'mapel',
'changes' => 'We are requesting to move our remaining Operations funds ($1653) into Wages to ensure we have enough to fund our student externs from May through June 30.',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Mark Apel, GF 14.31, Externships in Community Sustainability on 04.02.14. This program would like to move $1,653 from operations to student wages.

The Committee voted 9/0/1 in favor of this PAR during the 04.15.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs and Enrollment Management, approved the recommendations of the Green Fund Committee on April 23, 2014.',
'vp_approved' => 1,
'created_at' => '2014-04-02 18:32:48.000000',
'timestamp_review' => '2014-04-21 18:31:49.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-04-23 23:19:55.000000',
'netid_vp' => 'rudnick1',
'project_id' => 384,
),
44 => 
array (
'id_old' => 299947,
'netid' => 'uafrank0',
'changes' => 'We request a no-cost extension of the first-year project beyond June 30, 2014. To date we have been successful in fabricating a mount for the three-cylinder diesel engine, support for the two fuel filter canisters, and a stand for the external fuel tanks. We acquired and attached the necessary engine components to be able to start and run the engine on diesel fuel. This includes the radiator, fuel, filters, air filter, 12-volt battery, electrical wiring harness, tachometer, and muffler. Our next step is to be able to mount the dynamometer to the flywheel-end of the engine. This requires machining a stub attachment to match the attachment of the dynamometer, and fabricating a mount for stabilizing this component to the trailer.  Additionally, we need to fabricate a control panel and stand to mount the electrical switches that control the solenoid valves for directing the flow of fuel from the fuel tanks and fuel filters, to the engine fuel pump and return to the fuel tanks. An important piece of equipment used to fabricate the materials was out of commission for a while and was recently returned to operation. This resulted in delays in our ability to continue work. The equipment has recently been repaired and modified. The laboratory supervisor has been in contact with us is nearly ready to once again return to work on the project. The extension will allow us to resume work on the project and continue to acquire the materials during the summer as needed to complete the first phase.

Thank you for your consideration and help with this important project. Please contact me if you require any additional information.
Thank you, Edward Franklin',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Ed Franklin, GF 14.24, Biofuel Demonstration Trailer on 04.07.14. This 2 year program would like additional time to spend funds earmarked for year one during year two. 

The Committee voted 10/0/0 in favor of this PAR during the 04.15.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs and Enrollment Management, approved the recommendations of the Green Fund Committee on April 23, 2014.',
'vp_approved' => 1,
'created_at' => '2014-04-07 20:14:14.000000',
'timestamp_review' => '2014-04-21 18:38:20.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-04-23 23:20:17.000000',
'netid_vp' => 'rudnick1',
'project_id' => 369,
),
45 => 
array (
'id_old' => 299948,
'netid' => 'mlenart',
'changes' => 'We are very appreciative of the support from the UA Green Fund for the LEAF on the UA Campus project in both 2013-14 and 2014-15. The Green Fund has been a wonderful resource for sustainability projects on campus, and we are so happy to be involved in that effort to make the campus more sustainable.

As you can see from our budget (attached), the student interns have not been claiming as many hours as we had projected – in part because two out of three of the undergraduates have other jobs as well. The graduate students will be receiving pay through April and some of May, thanks to a positive response to an earlier PAR request.) Although the interns have done a good job in keeping up with their required duties, we feel there are many additional publications that could be written. 

Fortunately, one of our three undergraduate interns will be continuing with us in next year\'s UA LEAF project. The intern, Victoria Scaven, is an excellent writer, among other things, and we would love to put her writing skills to work on producing material for us during the summer. We are delighted that she is planning to continue on this project next year, and we feel that keeping her employed will not only help us get more information distributed but will help ensure that she will continue to direct her useful skills toward our project. Having this continuity will also help with our social media, as she can continue to provide new material to keep our Facebook page fresh and prepare us to launch a Twitter page.

As a result, I am requesting that we extend the 2013-2014 grant coverage for the LEAF on the UA Campus project through December 31, 2014. This would provide additional time for some of the wrap-up on the project from this year. Our main goal is to use some of that money to support her through the summer and potentially beyond, but extending the project could help us in other ways as well. 

I also request that we shift the remaining $869.30 of the money budgeted for Travel into the Operations fund so that we can access it to print out additional publications as we produce them. We may have other costs as well associated with setting up the olive oil for sale; in recent conversations with members of a UA marketing team, they have indicated there may be licensing fees as well as other application fees for creating a product (olive oil) under the UA brand. In addition, we will need to print labels and possibly purchase bottles as well, not mention potential marketing costs. 

We thank you heartily for considering this request for additional time to finish the project (until December 31, 2014) and an extension to spend the remaining project money. We also would sincerely appreciate it if you allowed us to transfer the remaining travel money ($869.30), which we no longer need, to the operations budget to support our efforts to produce publications and cover other costs related to the operations of the project.

Thanks again for considering this request. We remain grateful for all the support and consideration by the members of the UA Green Fund.',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Melanie Lenart, GF 14.15, Linking Edible Arizona Forest (LEAF) on the UA Campus on 04.07.14. This program requests to move $869.00 from travel to operations. The project also requests to extend the project end date to 12.31.14. 

The Committee voted 9/0/1 in favor of this PAR during the 04.15.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs and Enrollment Management, approved the recommendations of the Green Fund Committee on April 23, 2014.',
'vp_approved' => 1,
'created_at' => '2014-04-07 21:34:42.000000',
'timestamp_review' => '2014-04-21 18:44:07.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-04-23 23:20:32.000000',
'netid_vp' => 'rudnick1',
'project_id' => 396,
),
46 => 
array (
'id_old' => 299949,
'netid' => 'wplant',
'changes' => 'After meeting with Valerie Rountree and Jesse Minor about the progress being made on our project they recommended I submit a PAR to ensure that funds will be available to complete the work if it is not complete by June 30. We have made substantial progress and are aiming to have the project complete by June 30, we would rather be safe and submit a PAR. I would like to suggest a completion Date of December 1, 2014.',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Bill Plant, GF 12.28, Reducing the Potable Water Footprint at Flandrau Science Center on 04.22.14. This program requests to extend the project end date to 12.01.14. 

The Committee voted 9/0/0 in favor of this PAR during the 05.06.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs and Enrollment Management, approved the recommendations of the Green Fund Committee on May 10, 2014.',
'vp_approved' => 1,
'created_at' => '2014-04-22 16:50:52.000000',
'timestamp_review' => '2014-05-12 16:04:28.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-05-12 16:25:16.000000',
'netid_vp' => 'rudnick1',
'project_id' => 335,
),
47 => 
array (
'id_old' => 299950,
'netid' => 'wplant',
'changes' => 'Sine this project is directly tied into the completion of the "Reducing the potable water footprint of Flandrau" project, I would also like to submit a completion date of December 1, 2014.',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Bill Plant, GF 13.09, Incorporating AC condensate into Rainwater Harvesting Systems on 04.22.14. This program requests to extend the project end date to 12.01.14. 

The Committee voted 9/0/0 in favor of this PAR during the 05.06.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs and Enrollment Management, approved the recommendations of the Green Fund Committee on May 10, 2014.',
'vp_approved' => 1,
'created_at' => '2014-04-22 16:53:11.000000',
'timestamp_review' => '2014-05-12 16:06:07.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-05-12 16:25:42.000000',
'netid_vp' => 'rudnick1',
'project_id' => 363,
),
48 => 
array (
'id_old' => 299951,
'netid' => 'timl',
'changes' => 'SolarCats would like to request a one-year extension for the Solar Golf Cart Pilot Test project. A solar panel that we ordered did not fit any of the available golf carts, and we would like more time to determine how to exchange the solar panel for one that does fit and/or more monitoring equipment to use with golf carts that already had solar panels installed on them by PTS. Although it might be possible to complete the project with only a 6-month extension our club members are mostly unavailable during the summer and the extra time should increase the quantity and quality of the data we can collect. We should not require additional funding, but assistance in getting the panel refunded could be necessary since we have very few active members over the summer.',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Tim Luensman, GF 14.01, SolarCats Pilot Test for Retrofitting PTS Golf Cats with Solar Power on 04.27.14. This program requests to extend the project end date to 06.31.15. 

The Committee voted 9/0/0 in favor of this PAR during the 05.06.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs and Enrollment Management, approved the recommendations of the Green Fund Committee on May 10, 2014.',
'vp_approved' => 1,
'created_at' => '2014-04-28 00:09:45.000000',
'timestamp_review' => '2014-05-12 16:08:50.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-05-12 16:25:59.000000',
'netid_vp' => 'rudnick1',
'project_id' => 393,
),
49 => 
array (
'id_old' => 299952,
'netid' => 'nrlucas',
'changes' => 'The Institute of the Environment requests an extension to the Green Degree Guide, project timeline only – no additional funding requested, to see the project through the 2015 fiscal year. This additional time will allow the project team to substantially increase the impact of the Green Degree Guide and its sister-products – the Green Course, Career, and upcoming Engagement Guides.

To date, the project team has made incredibly successful strides in creating the Green Degree Guide; Contacting hundreds of individuals across campus, the team determined the degrees that are “Green,” they designed and are in the process of building the website, and have fostered numerous conversations to find ways to publicize this guide to make it as useful to students as possible. However although we have made significant progress, the team has identified a potential for a significant improvement to the project, without requiring additional funding. 

The team hired the Graduate Research Assistant much later than it expected, and as a result there is approximately $10,000 that will be left over that the project will not be able to spend by the end of this fiscal year. Along with the funds that remain, the project team will not be able to effectively market the finished guide by the end of this fiscal year. The website will be built and functioning, but its reach will be minimal without a significant push in correspondence with the other Green Guides.

The Green Degree Guide project team requests that the Green Fund allow us to extend the remaining funds through the 2015 fiscal year to hire one, potentially two, undergraduate students to focus on heavily marketing the Green Guides package that the University of Arizona has been developing for the past several years, with an emphasis on the Green Degree Guide. These undergraduates will not only work to develop additional marketing materials beyond the scope of the original project, but will also allow the team to directly reach out to units and students across the university, into the UA recruitment sphere, and beyond to other potential markets to significantly raise the profile of the Green Guides.',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Natalie Lucas, GF 14.42, Green Degree Guide on 04.28.14. This program requests to extend the project end date to 06.31.15. The program also requests to move remaining unspent funds earmarked for graduate salary to undergraduate salary.

The Committee voted 9/0/0 in favor of this PAR during the 05.06.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs and Enrollment Management, approved the recommendations of the Green Fund Committee on May 10, 2014.',
'vp_approved' => 1,
'created_at' => '2014-04-28 21:48:25.000000',
'timestamp_review' => '2014-05-12 16:17:47.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-05-12 16:26:13.000000',
'netid_vp' => 'rudnick1',
'project_id' => 405,
),
50 => 
array (
'id_old' => 299953,
'netid' => 'bmpryor',
'changes' => 'We would like to move $3148 from Operations category to Personnel category.  This would support the employment of one undergraduate student from 4/17/14 (end of the semester) to 6/30/14 (end of the 2013-2014 Green Fund grant).  A continuation of employment is critical to insure our mushroom production pipeline continues through the summer, albeit at a reduce level.  After June 30, our renewed Green Fund grant should kick in and will be available to continue support for this student until the new academic years begins.  The movement of funds from Operations category to Personnel category will be less critical to Operations as we have supplies remaining from the previous year\'s operation to carry us through to the beginning of the next funding cycle.

We would like to thank the Green Fund for their support.

Barry Pryor
Project Director
Lauren Jackson
Project Manager',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Barry Pryor, GF 14.32P, Production of Common and Gourmet Mushrooms from Landscaping and Consumer Waste Products  on 04.28.14. This program requests to move remaining unspent operations funds of $3,148 to undergraduate salary.

The Committee voted 9/0/0 in favor of this PAR during the 05.06.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Melissa Vito, Vice President of Student Affairs and Enrollment Management, approved the recommendations of the Green Fund Committee on May 10, 2014.',
'vp_approved' => 1,
'created_at' => '2014-04-28 23:47:54.000000',
'timestamp_review' => '2014-05-12 16:22:39.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-05-12 16:26:29.000000',
'netid_vp' => 'rudnick1',
'project_id' => 390,
),
51 => 
array (
'id_old' => 299954,
'netid' => 'lcsamuels',
'changes' => 'We are lucky enough to have one or two students working with us on this project who will be funded from other sources. These are positions we set money aside for in the grant that we may need to reallocate. Because of this, we would like to know if it is possible to use the designated funds in the UA Green Fund to fund one larger position rather than two, or to fund one graduate student instead of two undergraduates. The students enrolled in the course will be entirely undergraduate architecture students. Thank you!',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Linda Samuels on 08.18.14, GF 15.18 Urban Activation for Sustainable Stewardship. This program requests to spend allocated salary funds on a UA graduate student instead of undergraduate students.

The Committee voted 10/0/0 in favor of this PAR during the 09.30.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 09.30.14.',
'vp_approved' => 1,
'created_at' => '2014-08-19 00:45:29.000000',
'timestamp_review' => '2014-10-07 14:47:36.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-10-07 14:58:48.000000',
'netid_vp' => 'rudnick1',
'project_id' => 445,
),
52 => 
array (
'id_old' => 299955,
'netid' => 'cfp',
'changes' => 'Compost Cats continues to go through the process of getting retail bag designs approved by UA Trademark and Licensing. This process has been very slow, but we have secured the approval we need from Risk Management and have completed a very detailed lab analysis of samples from both of the compost recipes we produce. With this lab analysis, we have secured the US Composting Council Seal of Testing Assurance for our compost, which is a major certification of our product quality.

Given that we have not been able to start bagged compost sales, and bagged sales are where the greater profit margins that will sustain the program lie, we need money for basic operations: equipment maintenance and repair, tabling and event supplies, safety equipment, and other essentials. We would like to move $10,000 from our student staff payroll into operations. 

We have recently secured an additional $10,000 in student staff funding from the Agnese Haury Undergraduate Engagement Fund, and thus this transfer will not cause us to run short on payroll, but it will allow us to acquire needed safety supplies and do needed repairs and maintenance.',
'advice' => 'Dr. Vito,
The Green Fund Committee received this PAR from Chester Phillips on 09.22.14, GF 15.29P Compost Cats: Developing Sustainable Partnerships and a Green Business Model at the UA. This program requests to move $10,000 from student salary to operations.

The Committee voted 10/0/0 in favor of this PAR during the 09.30.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the Green Fund committee recommendation on 09.30.14.',
'vp_approved' => 1,
'created_at' => '2014-09-23 03:59:45.000000',
'timestamp_review' => '2014-10-07 15:13:57.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-10-07 15:15:07.000000',
'netid_vp' => 'rudnick1',
'project_id' => 424,
),
53 => 
array (
'id_old' => 299956,
'netid' => 'wplant',
'changes' => 'We are requesting a PAR for this project as it is tied directly into the work on the main Green Fund water harvesting project at Flandrau, GF12.28. We are requesting a PAR as the collaboration with Facilities Management has taken more time than we anticipated since we applied for our last PAR, however we have recently made significant progress in engaging that group. Please see the attached survey of the site completed by FM. They have identified some challenges to the site which we feel confident can be overcome with some minor modifications to the design, none of which will impact the overall goal of the project. We are requesting an extension until 6/30/2015. Tank you for your consideration.
Bill Plant',
'advice' => 'Project Manager accidentally submitted two PAR\'s for the same project. He intended this PAR to address GF 12.28, Reducing the Potable Water Footprint at Flandrau Science Center. Mr. Plant is seeking a project extension that will end 06.30.15 as well as requesting permission to continue to spend project funds until 06.30.15. Please see PAR GF-60.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the Green Fund committee recommendations on 12.10.14.',
'vp_approved' => 1,
'created_at' => '2014-11-25 20:24:55.000000',
'timestamp_review' => '2014-12-16 18:36:38.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-12-16 18:43:11.000000',
'netid_vp' => 'rudnick1',
'project_id' => 363,
),
54 => 
array (
'id_old' => 299957,
'netid' => 'wplant',
'changes' => 'We are requesting a PAR as the collaboration with Facilities Management has taken more time than we anticipated since we applied for our last PAR, however we have recently made significant progress in engaging that group. Please see the attached survey of the site completed by FM. They have identified some challenges to the site which we feel confident can be overcome with some minor modifications to the design, none of which will impact the overall goal of the project. We are requesting an extension until 6/30/2015. Tank you for your consideration.
Bill Plant',
'advice' => 'Dear Dr. Vito,
The Green Fund Committee received this PAR from Bill Plant on 11.25.14. Mr. Plant is seeking a project extension that will end 06.30.15 as well as requesting permission to continue to spend project funds until 06.30.15. 

The Committee voted 9/0/0 in favor of this PAR during the 12.09.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the Green Fund committee recommendations on 12.10.14.',
'vp_approved' => 1,
'created_at' => '2014-11-25 20:25:36.000000',
'timestamp_review' => '2014-12-16 18:40:42.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-12-16 18:43:26.000000',
'netid_vp' => 'rudnick1',
'project_id' => 363,
),
55 => 
array (
'id_old' => 299958,
'netid' => 'andrewfalwell',
'changes' => 'PAR is attached in PDF and under 1500 words.',
'advice' => 'Dear Dr. Vito,
The Green Fund Committee received this PAR from Andrew Falwell on 11.30.14. The Green Fund Committee asked he reformat his request and provide more detailed information regarding his specific needs as well as a project. This PAR was resubmitted on 12.07.14, see GF59.',
'approved' => 0,
'vp_comments' => 'Dr. Vito approved the Green Fund committee recommendation on 12.10.14.',
'vp_approved' => 0,
'created_at' => '2014-11-30 21:22:37.000000',
'timestamp_review' => '2014-12-16 18:49:12.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-12-16 19:14:06.000000',
'netid_vp' => 'rudnick1',
'project_id' => 444,
),
56 => 
array (
'id_old' => 299959,
'netid' => 'andrewfalwell',
'changes' => 'Dear Green Fund,

Thank you for taking the time to review and vote on ASUA Students for Sustainability\'s Project Alteration Request to the Highland Garage Cistern: Saving Water, Cutting Water Bills, and Spreading the Word on Rainwater Harvesting initiative.

Please see the attached PDF.

Sincerely,
Andrew Falwell
ASUA SFS Co-Director',
'advice' => 'Dear Dr. Vito,
The Green Fund Committee received this PAR from Andrew Falwell on 12.07.14 regarding GF 15.27P, Highland Garage Cistern: Saving Water, Cutting Water Bills, and Spreading the Word on Rainwater Harvesting. This PAR replaces the request(GF-58) made on 11.30.14 and provides more detail regarding the project request. This PAR requests:
1)	Additional funds of $8,385 from the Green Fund to complete the project as originally planned, or
2)	Change the project scope and final deliverable s. The project planned to install one cistern. Due to cost overruns they will not be able to install the system. The PM asked to continue working on the project and use remaining funds to continue plumbing work and other preparation work in anticipation of future funding to install cistern(s). 

The Committee voted on each request separately. For request number 1 as listed above they did not approve the PAR with a vote of 0,9,0. For request number 2 as listed above they did not approve the PAR with a vote of 1,8,0 in the 12.09.14 open Green Fund Committee meeting.',
'approved' => 0,
'vp_comments' => 'Dr. Vito approved the Green Fund committee recommendation on 12.10.14.',
'vp_approved' => 0,
'created_at' => '2014-12-08 03:25:04.000000',
'timestamp_review' => '2014-12-16 19:11:58.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-12-16 19:14:20.000000',
'netid_vp' => 'rudnick1',
'project_id' => 444,
),
57 => 
array (
'id_old' => 299960,
'netid' => 'rudnick1',
'changes' => 'This PAR was entered into the GF site by Bill Plant under GF 13.09 by mistake. 

We are requesting a PAR as the collaboration with Facilities Management has taken more time than we anticipated since we applied for our last PAR, however we have recently made significant progress in engaging that group. Please see the attached survey of the site completed by FM. They have identified some challenges to the site which we feel confident can be overcome with some minor modifications to the design, none of which will impact the overall goal of the project. We are requesting an extension until 6/30/2015. Tank you for your consideration.
Bill Plant',
'advice' => 'Dear Dr. Vito,
The Green Fund Committee received this PAR from Bill Plant on 11.25.14. Mr. Plant is seeking a project extension that will end 06.30.15 as well as requesting permission to continue to spend project funds until 06.30.15. 

The Committee voted 9/0/0 in favor of this PAR during the 12.09.14 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the Green Fund committee recommendations on 12.10.14.',
'vp_approved' => 1,
'created_at' => '2014-12-09 19:04:00.000000',
'timestamp_review' => '2014-12-16 18:41:04.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2014-12-16 18:43:38.000000',
'netid_vp' => 'rudnick1',
'project_id' => 335,
),
58 => 
array (
'id_old' => 299961,
'netid' => 'kastroth',
'changes' => 'We are seeking approval to make a budget modification in our original proposal.  Our camp staff is limited to working 19 hours per week, and they are spending a lot of time during this time of year installing Green Fund projects.  Yet we did not originally request any funds to help pay for their installation time.  We are asking to delete the ice maker from the budget ($3500) and also move $2000 from the line item of student meals and lodging (so far we have expended $646.00) to the Classified Temporary staff line plus ERE of 11.2%.  This change will help cover the costs of some of their time spent installing GF projects.  The installation of the composting toilet will also require a significant amount of their time.  Thank you for considering our request.',
'advice' => 'Dr. Vito,

The Green Fund Committee received this PAR from Kirk Astroth on 12.16.14, GF 15.35 "UA James 4-H Camp Getting Greener Retrofit Project". This program requests to move $2000 from student meals and lodging and $3500 from the funds budgeted towards an ice maker to "classified temporary labor". 

The Committee voted 9/0/0 in favor of this PAR during the 01.27.15 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr.Vito approved this PAR on 01.28.15.',
'vp_approved' => 1,
'created_at' => '2014-12-16 16:49:12.000000',
'timestamp_review' => '2015-02-09 21:15:32.000000',
'netid_review' => 'natalieramos',
'timestamp_vp' => '2015-02-09 21:17:48.000000',
'netid_vp' => 'natalieramos',
'project_id' => 446,
),
59 => 
array (
'id_old' => 299962,
'netid' => 'kelkind',
'changes' => 'Due to the difficulties of coordinating operations at UA Baseball games, Greening the Game (GtG) will not be expanding into baseball this spring. Additionally, while expanding our operations into UA softball games, we have found that hiring a student club for a post-game cleanup is not necessary due to the small size of the stadium. GtG also has $350.00 of unused funds in our budget that was allocated for payment of student clubs during football operations. These left-over funds resulted from partial payments to student clubs due to lack of volunteer attendance.  

These three developments leave $1,150.00 left to be unused in the operations section of our budget. We propose that $500.00 of these funds be left in operations to pay for shirts for employees to wear during operations in the fall and other general expenses incurred. We propose that the remaining $650.00 of these funds be transferred to personnel and ERE categories so that GtG can hire more student employees.

In light of the approval of Greening the Game’s FY 2016 annual grant, we additionally propose that the funds remaining in the Greening the Game FY 2015 Green Fund account at the end of this fiscal year be transferred to the FY 2016 account and be left in their respective categories. In fiscal year 2016 these funds will be used hire more student workers, increase the payment to student clubs, and purchase general operating supplies and promotional materials.',
'advice' => 'Dr. Vito,

The Green Fund Committee received this PAR from Kaitlyn Elkind on 03.11.15, GF 15.23 "Greening the Game: Football and Beyond". This program requests to move $650 from  operations to student salary/ERE and request a one year project extension. The new project end date will be 06.30.16.

The Committee voted 8/0/1 in favor of this PAR during the 04.14.15 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 04.16.15.',
'vp_approved' => 1,
'created_at' => '2015-03-12 00:12:53.000000',
'timestamp_review' => '2015-04-16 22:22:06.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2015-04-16 22:23:13.000000',
'netid_vp' => 'rudnick1',
'project_id' => 441,
),
60 => 
array (
'id_old' => 299963,
'netid' => 'tquist',
'changes' => 'Greenfund PAR
The time expected to complete the campus arboretum sign project has far exceeded what we anticipated due to logistical and technical hurdles. We have made terrific progress: 445 signs and sign posts have been located and removed, posts have been refurbished and inventoried, new sign locations have been mapped, new web pages describing species have been created and content researched and added to the these webpages. Further, updated botanical terminology has been researched, and individual files for the 445 new signs have been created.  Although the important preparatory, behind-the-scenes work has been completed, wages spent have already exceeded the original budget request by $4589. Without additional funding we will unable to complete the project.  This unfortunately, will mean no tangible physical evidence of more than 7 months of labor by our 4 Campus Arboretum student workers. Further, the educational value of the landscape will be reduced in the absence of botanical signage. 
We would like to request a transfer of $4589 from operations to personnel to cover the wages deficit (including anticipated wage encumbrances through the end of the project). Unfortunately, transferring funds from operations to personnel will leave us with only $999 of the needed $5900 for materials. I have begun to explore options for additional funding to purchase the materials needed. However, if it is possible to request additional funds to cover needed wages ($4589) or needed materials ($4901), please let me know.',
'advice' => 'Dr. Vito,

The Green Fund Committee received this PAR from Tanya Quist on 03.27.15, GF 15.04 "Promoting Landscape Stewardship with Interactive interpretation". This PAR requests, post-spending, to move $4,589 from Operations to Student personnel/ERE. If this PAR is approved, the deliverable will change as well. Additional funds spent on student salary means the signs will not be purchased and installed. Lastly, this PAR request an additional $4,589 to cover the project overage. 

The Committee voted down the request with a vote of 0/9/0 for additional funds of $4,589 and hopes Tanya Quist can get funds from another source to install signs. The committee approved the request with a vote of 9/0/0 to move $4,589 from operations to student salary/ERE and accepted the terms of the project change with the understanding Green Fund funds will not be used to pay for signage.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 04.16.15.',
'vp_approved' => 1,
'created_at' => '2015-03-27 23:36:52.000000',
'timestamp_review' => '2015-04-16 22:56:42.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2015-04-16 23:01:49.000000',
'netid_vp' => 'rudnick1',
'project_id' => 429,
),
61 => 
array (
'id_old' => 299964,
'netid' => 'nrlucas',
'changes' => 'The Green Guides team has made significant progress this year on the development of the Green Course Guide, the Green Degree Guide, and the Green Engagement Guide, which will be paired up with Career Service’s Green Career Guide to complete the Green Guides collection. The website, greenguides.arizona.edu, will be up at the end of the Spring Semester 2015 with links to all four guides embedded in that website. This year the Green Guides team focused on the development and the creation of the Green Engagement Guide and the completion of the Green Degree Guide.

All of the content for the Green Degree Guide has been put in the back end of the Druple site, however, the site is not live yet because of the postponed Institute of the Environment website, which is expected to be live in April. After the site is live it will have to be reviewed and modified to ensure that all the components are working properly, which will need to completed next fiscal year due to the more pressing need of using student time to finalize the Green Engagement Guide website this semester. 

The Green Engagement Guide has several components, which includes a page similar to Wildcat Joblink that posts green opportunities (jobs, internships, externships, volunteer work, research) for students on and off campus, a “How to Engage in a Sustainable Lifestyle” that has tips for living sustainably, and guides to help students work through the development of projects on campus and off related to sustainability such as “Meeting with Decision Makers,” “Writing a Plan,” “Creating a Team,” and “Tips for Collaboration.” Developing the components to this guide has been an exciting challenge, and one that took longer than anticipated. Although the website will be up at the end of the semester and students will be able to access the Project Guides and the Sustainable Living Guide, the page that hosts Green Opportunities will most likely not be populated, which is a problem since this is the core of the Green Engagement Guide. 

Therefore, the Green Guides team would like to request from the UA Green Fund that the remaining amount of money left for the projects Green Degree Guide and Green Engagement Guide be extended another fiscal year in order for us to hire students to complete these guides, and spend down these existing funds.',
'advice' => 'Dr. Vito,

The Green Fund Committee received this PAR from Natalie Lucas on 03.30.15, GF 14.42 "Green Degree Guide". This program requests a one year extension to finish the work of the project as well as to spend the remaining funds doing such work.

The Committee voted 8/0/1 in favor of this PAR during the 04.15.15 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 04.16.15. The new project end date will be 06.30.16.',
'vp_approved' => 1,
'created_at' => '2015-03-30 19:47:48.000000',
'timestamp_review' => '2015-04-16 23:07:08.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2015-04-16 23:09:10.000000',
'netid_vp' => 'rudnick1',
'project_id' => 405,
),
62 => 
array (
'id_old' => 299965,
'netid' => 'nrlucas',
'changes' => 'The Green Guides team has made significant progress this year on the development of the Green Course Guide, the Green Degree Guide, and the Green Engagement Guide, which will be paired up with Career Service’s Green Career Guide to complete the Green Guides collection. The website, greenguides.arizona.edu, will be up at the end of the Spring Semester 2015 with links to all four guides embedded in that website. This year the Green Guides team focused on the development and the creation of the Green Engagement Guide and the completion of the Green Degree Guide.

All of the content for the Green Degree Guide has been put in the back end of the Druple site, however, the site is not live yet because of the postponed Institute of the Environment website, which is expected to be live in April. After the site is live it will have to be reviewed and modified to ensure that all the components are working properly, which will need to completed next fiscal year due to the more pressing need of using student time to finalize the Green Engagement Guide website this semester. 

The Green Engagement Guide has several components, which includes a page similar to Wildcat Joblink that posts green opportunities (jobs, internships, externships, volunteer work, research) for students on and off campus, a “How to Engage in a Sustainable Lifestyle” that has tips for living sustainably, and guides to help students work through the development of projects on campus and off related to sustainability such as “Meeting with Decision Makers,” “Writing a Plan,” “Creating a Team,” and “Tips for Collaboration.” Developing the components to this guide has been an exciting challenge, and one that took longer than anticipated. Although the website will be up at the end of the semester and students will be able to access the Project Guides and the Sustainable Living Guide, the page that hosts Green Opportunities will most likely not be populated, which is a problem since this is the core of the Green Engagement Guide. 

Therefore, the Green Guides team would like to request from the UA Green Fund that the remaining amount of money left for the projects Green Degree Guide and Green Engagement Guide be extended another fiscal year in order for us to hire students to complete these guides, and spend down these existing funds.',
'advice' => 'This PAR was submitted in error. See PAR GF-66 as its replacement.',
'approved' => 0,
'vp_comments' => 'This PAR was subbmitted in error. See GF-66.',
'vp_approved' => 0,
'created_at' => '2015-03-30 19:48:06.000000',
'timestamp_review' => '2015-04-16 23:00:41.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2015-04-16 23:02:35.000000',
'netid_vp' => 'rudnick1',
'project_id' => 405,
),
63 => 
array (
'id_old' => 299966,
'netid' => 'nrlucas',
'changes' => 'The Green Guides team has made significant progress this year on the development of the Green Course Guide, the Green Degree Guide, and the Green Engagement Guide, which will be paired up with Career Service’s Green Career Guide to complete the Green Guides collection. The website, greenguides.arizona.edu, will be up at the end of the Spring Semester 2015 with links to all four guides embedded in that website. This year the Green Guides team focused on the development and the creation of the Green Engagement Guide and the completion of the Green Degree Guide.

All of the content for the Green Degree Guide has been put in the back end of the Druple site, however, the site is not live yet because of the postponed Institute of the Environment website, which is expected to be live in April. After the site is live it will have to be reviewed and modified to ensure that all the components are working properly, which will need to completed next fiscal year due to the more pressing need of using student time to finalize the Green Engagement Guide website this semester. 

The Green Engagement Guide has several components, which includes a page similar to Wildcat Joblink that posts green opportunities (jobs, internships, externships, volunteer work, research) for students on and off campus, a “How to Engage in a Sustainable Lifestyle” that has tips for living sustainably, and guides to help students work through the development of projects on campus and off related to sustainability such as “Meeting with Decision Makers,” “Writing a Plan,” “Creating a Team,” and “Tips for Collaboration.” Developing the components to this guide has been an exciting challenge, and one that took longer than anticipated. Although the website will be up at the end of the semester and students will be able to access the Project Guides and the Sustainable Living Guide, the page that hosts Green Opportunities will most likely not be populated, which is a problem since this is the core of the Green Engagement Guide. 

Therefore, the Green Guides team would like to request from the UA Green Fund that the remaining amount of money left for the projects Green Degree Guide and Green Engagement Guide be extended another fiscal year in order for us to hire students to complete these guides, and spend down these existing funds.',
'advice' => 'Dr. Vito,

The Green Fund Committee received this PAR from Natalie Lucas on 04.07.15, GF 15.11 "Green Engagement Guide". This program requests a one year extension to finish the work of the project as well as to spend the remaining funds doing such work. The new project end date will be 06.30.16.

The Committee voted 8/0/1 in favor of this PAR during the 04.15.15 open Green Fund Committee meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 04.16.15.',
'vp_approved' => 1,
'created_at' => '2015-04-07 19:57:29.000000',
'timestamp_review' => '2015-04-16 23:08:27.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2015-04-16 23:09:24.000000',
'netid_vp' => 'rudnick1',
'project_id' => 431,
),
64 => 
array (
'id_old' => 299967,
'netid' => 'cfp',
'changes' => 'Compost Cats requests to move $7500 of our FY 2015 allocation from payroll to operations. Two factors have led us to this request. First, the City of Tucson\'s assumption of most of the organic material pick ups around town has lowered project labor costs modestly, and the $10,000 we received from the Agnese Haury Endowment for student wages has also supplemented our student payroll funding. Meanwhile, organic certification, compost bagging equipment for retail sales, and scale maintenance costs have exceeded what we initially allotted for the year. Moving this money from one category to another should leave us in great shape through the end of this fiscal year, and for next fiscal year, we are in a stronger financial position than ever before due to our growing partnerships and revenues.',
'advice' => 'Dr. Vito, 

The Green Fund Committee received this PAR from Chester Phillips on 04.15.15, GF 15.29 "Compost Cats: Developing Sustainable Partnerships and a Green Business Model at the UA". This project requests to move $7,500 of their budget from student payroll to operations. 

The committee voted 8/0/0 in favor of this PAR at the 05.05.15 Green Fund Committee open meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 05.07.15.',
'vp_approved' => 1,
'created_at' => '2015-04-15 22:52:00.000000',
'timestamp_review' => '2015-05-07 18:40:24.000000',
'netid_review' => 'natalieramos',
'timestamp_vp' => '2015-05-07 19:05:47.000000',
'netid_vp' => 'natalieramos',
'project_id' => 424,
),
65 => 
array (
'id_old' => 299968,
'netid' => 'cfp',
'changes' => 'The Highland Garage Cistern Project requests to extend our FY 2015 allocation for an additional six months, through December 2015. Unexpected budget shortfalls documented in our funded FY 2016 proposal mean that completion of the cistern as planned by the end of June 2015 will not be possible. With the additional funds granted for FY 2016, however, we should be able to use FY 2015 funds to complete a single cistern by December 2015. We hope to complete it sooner but will depend on Facilities Management coordination with us to accomplish the installation.',
'advice' => 'Dr. Vito, 

The Green Fund Committee received this PAR from Chester Phillips on 04.15.15, GF 15.27 "Highland Garage Cistern: Saving Water, Cutting Water Bills, and Spreading the Word on Rainwater Harvesting". This program requests to extend their project end date. The new project end date will be 12.31.15. 

The committee voted 8/0/0 in favor of this PAR at the 05.05.15 Green Fund Committee open meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 05.07.15.',
'vp_approved' => 1,
'created_at' => '2015-04-15 23:03:37.000000',
'timestamp_review' => '2015-05-07 18:43:52.000000',
'netid_review' => 'natalieramos',
'timestamp_vp' => '2015-05-07 19:06:29.000000',
'netid_vp' => 'natalieramos',
'project_id' => 444,
),
66 => 
array (
'id_old' => 299969,
'netid' => 'rudnick1',
'changes' => 'PAR Request
Estimated Funding Left
$1,000: We request all operation and travel funds be moved to the salary line and roll over to the 2015-2016 academic year.  Additionally, all funds in the salary line roll over to the 2015-2016 academic year.  The exact amount would be determined after the current Off-Campus Housing Sustainability Coordinator (OCHSC) completes work for the semester (last day for the 2014-2015 academic year is 20 May 2015). 

Proposal
Hire one part time student employee as an Off-Campus Housing Sustainability Intern for 2-3 hours per week at $8.50/hr.
Intern Responsibilities
•	Develop social media and marketing plans for all fall programs
•	Shadow the OCHSC in contacting Featured Lister apartment complexes
•	Shadow the OCHSC on presentations and staff trainings
•	Do various research projects to better inform the OCHSC’s sustainability programming
•	Attend monthly team meetings with the Off-Campus Housing Sustainability Team
•	Develop relationships with on-campus sustainability entities (including but not limited to: the Office of Sustainability, UA Students for Sustainability, ASUA, ACT, the Women’s Resource Center, Sustainable Built Environments, etc.)
Reasons for Additional Staffing 
•	The Off-Campus Housing Sustainability Coordinator is leaving in May, 2016.
o	In essence, this position would be a training and testing ground for the 2016-2017 academic year.  This person would get a taste of what the OCHSC position is like and what the team is like.  In addition, this position requires developed skills in communication, writing, and diplomacy.  The team would be able to see those skills in action and make a hiring decision based off of that in the spring time.
•	Having an additional student worker would give the OCHSC more time to focus on programming for the fall.
o	Throughout the past two years, the Fall semester has been dedicated in large part to recruiting apartment complexes to join the Off-Campus Housing Sustainability program.  As of now, the OCHSC has 14 apartment complexes on board with the program.  Recruitment is no longer necessary, maintenance is.
o	The only program that happens in fall, currently, is Tug-O-Water.  Having another student worker also helping to maintain relationships with the apartment complexes frees up time for the OCHSC to work on developing a better plan for fall programming.
o	This will help to create a smoother transition from Spring 2016 to Fall 2016 during the switch from current to new OCHSC.
•	This provides another student with the opportunity to develop professional skills necessary for their completion of their college career and future success.',
'advice' => 'Dr. Vito, 

The Green Fund Committee received this PAR from Madeline Bynes on 04.17.15, GF 15.02 "Turning Footprint to Pawprint". This program requests to extend their project end date to 06.30.16, as well as transfer all remaining funds from travel and operations to student salary. 

The committee voted 8/0/0 in favor of this PAR at the 05.05.15 Green Fund Committee open meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 05.07.15.',
'vp_approved' => 1,
'created_at' => '2015-04-17 21:38:15.000000',
'timestamp_review' => '2015-05-07 18:47:14.000000',
'netid_review' => 'natalieramos',
'timestamp_vp' => '2015-05-07 19:06:41.000000',
'netid_vp' => 'natalieramos',
'project_id' => 442,
),
67 => 
array (
'id_old' => 299970,
'netid' => 'mapel',
'changes' => 'We were approved to have our FY15 Green Fund Award extend until August 31, 2015 to accommodate another summer of externships in sustainability projects. I respectfully request to have this date extended until October 31 to ensure that projects can be completed by externs and funds expended accordingly. This year, three projects are in the Tucson area, thereby making it possible for students returning to classes in August to still continue working with Pima County Extension faculty on their projects, as their schedule and the externship budget allow.',
'advice' => 'Dr. Vito, 

The Green Fund Committee received this PAR from Mark Apel on 04.20.15, GF 15.01 "Externships in Community Sustainability Through Cooperative Extension". This program requests to extend their project end date to 10.31.15. 

The committee voted 8/0/0 in favor of this PAR at the 05.05.15 Green Fund Committee open meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 05.07.15.',
'vp_approved' => 1,
'created_at' => '2015-04-20 16:37:46.000000',
'timestamp_review' => '2015-05-07 18:49:44.000000',
'netid_review' => 'natalieramos',
'timestamp_vp' => '2015-05-07 19:06:55.000000',
'netid_vp' => 'natalieramos',
'project_id' => 418,
),
68 => 
array (
'id_old' => 299971,
'netid' => 'reneeguerin',
'changes' => 'The Green Purchasing Project requests that funds remaining from FY 2015 allocations be extended to FY 2016.  This includes $11,500 for student employee wages and $1,200 in travel funds. Specifically, the funds would be used to hire two student coordinators for the next academic year; the students would continue and expand the Green Purchasing Project. Both of these students will come from the 10 student coordinators hired to represent the Project this year. The heads of the Green Purchasing Project – Julia Rudnick, Jill Ramirez, and Renee Guerin – have selected one outstanding student coordinator, Samantha Orwell, who has agreed to be the Administrative Co-Leader next year, as the current Project Manager, Renee Guerin, is graduating.  In addition, Jill and Julia will continue their supervisorial roles. 

In the next year, the student coordinators will work with the Green Team run by Julia Rudnick to “green” events and further develop partnerships with departments and purchasers around campus. Job responsibilities may include contributing to a green events guide, creating a greener office checklist, collaborating with the BookStore on a “Back-to-School” event, and shadowing administrators, such as Ted Nasser of Procurement and Contracting Services. Current Administrative Co-Leader, Renee Guerin, will compile all of the information assembled in the first two years of the Project and work with Samantha to develop a plan for the upcoming year.    

It is requested that the Project be permitted to spend $2,000 for Samantha, as administrative co-leader, to travel to the AASHE conference in Minneapolis, MI.  This sum would include the $1,200 remaining in travel funds and an additional $800 from the student wage funds. The remaining money in the student employee wages section will be used to pay the administrative Co-Leader $12/hour and the student coordinator $10/hour for the duration of school year. The Administrative Co-Leader will work 10-15 hours each week and the Student Coordinator will work approximately 5-10.  ',
'advice' => 'Dr. Vito, 

The Green Fund Committee received this PAR from Renee Guerin on 04.20.15, GF 15.08 "Green Purchasing Project". This project requests to move $800 of their budget from student payroll to travel in order to attend the AASHE conference, as well as a new project end date request. The new end date will be 06.30.16.

The committee voted 8/0/0 in favor of this PAR at the 05.05.15 Green Fund Committee open meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 05.07.15.',
'vp_approved' => 1,
'created_at' => '2015-04-21 02:54:54.000000',
'timestamp_review' => '2015-05-07 18:52:39.000000',
'netid_review' => 'natalieramos',
'timestamp_vp' => '2015-05-07 19:07:05.000000',
'netid_vp' => 'natalieramos',
'project_id' => 437,
),
69 => 
array (
'id_old' => 299972,
'netid' => 'mlenart',
'changes' => 'Summary of Project Alteration Request

We are appreciative of the support from the UA Green Fund for the LEAF on the UA Campus project since 2013. The Green Fund has been a wonderful resource for sustainability projects on campus, and we are delighted to be involved in the effort to make the campus more sustainable.

However, UA LEAF cannot operate without a graduate student. There are so many tasks and moving parts that even half-time graduate students struggle with completing everything that needs to be done in a timely fashion (Table 1). With that in mind, we submit a Project Alteration Request that will bring our vision into line with the funding provided for the upcoming academic year. Specifically, we request that the money allocated for undergraduate students be decreased in order to support a half-time graduate assistantship in the fall, and a quarter-time graduate assistantship in the spring. 

In addition, to make this plan work, we will need to extend the time frame of the current project (2014-15) as well to help us cover some gaps in our funding for the priorities we describe below. We request that any remaining funds from the current project be available for our use in next year’s project, either by extending the current project through December or by transferring those funds in the budget for the 2015-16 academic year. We will use remaining funds to support undergraduate students, and we also expect to have some volunteer interns to support the project. We have an additional bookkeeping request described below regarding the movement of existing funds into a more appropriate category.

Graduate student work

We plan to hire one quarter-time graduate student to work with the Iskashitaa Refugee Network throughout the year, organizing harvests that would include campus harvests. This student would be involved in networking as a representative of UA LEAF with many groups on and off campus (see Table 1).  One undergraduate student would be designated to assist this student, with additional interns working on a volunteer basis. 

The other graduate student would work a quarter-time for the fall only, and would focus on helping us in our goal of producing some videos that can guide others in harvesting and processing fruit from trees available on campus. One undergraduate student would be designated to assist this student.

Undergraduate work

Regarding undergraduate salaries, we plan to retain undergraduate Alyssa Los, who has proven herself a valuable asset to the project with many skills in organizing and publishing. Alyssa and Alexis Haley Kubek, whom we plan to hire once she finishes the volunteer hours she has been contributing via her coursework with Jennifer Ravia, both have done a great job, including by producing “best practices” publications for cultivation, harvesting and processing the products of edible trees on the UA campus. 

Alexis has also agreed to work on a video this summer of an upcoming preserves workshop so that UA students will continue to benefit from this knowledge for years to come. We would like to put Alexis to work on producing additional videos in the 2015-16 academic year, while we will keep Alyssa for organizing, publication and other jobs that go with the project. In addition, we hope to have at least two new volunteers a semester from our collaboration with Jennifer Ravia, whose course encourages undergraduates to spend 39 hours a semester on our project (and others). 

Bookkeeping request

Finally, we have a request that will help keep our books in line. As it happens, Barbara Eiswerth’s stipend goes to the Iskashitaa Refugee Network (IRN) rather than her as a person and adjunct professor in the School of Natural Resources and the Environment. Because of this, the money comes out of Operations rather than Personnel. We request that the $1,000 for her work in the 2014-15 academic year and the $2,500 for her work in the 2015-16 academic year be transferred from Personnel to Operations in order to reflect this reality. This has the additional benefit of saving us roughly 10% in ERE, so we ask that this money ($100 and $250) be moved as well so we can provide additional payment to the IRN for the extensive expertise and guidance she provides to this project. 

Extension or transfer of funds, 2014-15 project

As part of the PAR today, we request that any funding for Personnel that is not used by the end of June be transferred to our 2015-16 budget. This will make it easier to manage than it would to extend the project through December. However, if this transfer is more challenging on the Green Fund’s end, we request that the 2014-15 project be extended through December so we can access the funding this way. The attached table highlights the changes we are requesting (Table 2). 

Differences in olive harvest from 2014 to 2015

UA LEAF will not be harvesting olives for oil production this year. Planning an olive harvest that leads to oil production requires long-range planning, because we must work with Facilities Management (FM) to set aside some trees for this purpose. Most campus olive trees are treated to prevent fruiting. Given that we did not know whether we would have a graduate student to lead this effort in March – which is when trees need to be treated to prevent fruiting – we advised FM not to set aside any trees for our use. 

Still, there may be enough olives produced anyway for us to hold a harvest encouraging individuals to cure the olives they collect for their own use. We believe that this would provide a valuable service to the campus community. After all, few people can collect the hundreds or thousands of pounds needed for olive oil production, while many people can collect a few olives from their own trees or others to do their own curing of olives. We feel this educational approach will expand interest in local olives. It will also help FM keep the campus clean of fallen olives, which can make a mess on sidewalks. 

This activity fits well into our plans to continue educating the campus and community about the qualities of edible trees and capturing a return of the water investment in the edible trees on campus. 

Thank you!

We thank you heartily for considering this request for either an extension of the 2014-15 UA LEAF project (until December 31, 2015) or a transfer of the remaining project money into the fund for 2015-16, and to shift some of the line items as described above and outlined in Table 2. 

Project manager Melanie Lenart also plans to attend the Green Fund meeting where these plans will be discussed if there will be opportunities to answer any questions committee members might have. We remain grateful for all the support and consideration by the members of the UA Green Fund.',
'advice' => 'Dr. Vito,

The Green Fund Committee received this PAR from Melanie Lenart on 04.24.15, GF 15.20 "LEAF on the UA Campus". This project requests to extend the project to 12.31.15 and move all remaining funds at fiscal year end to graduate student salary and ERE.  

The committee voted 8/0/0 in favor of this PAR at the 05.05.15 Green Fund Committee open meeting.
Dr. Vito,

The Green Fund Committee received this PAR from Melanie Lenart on 04.24.15, GF 15.20 "LEAF on the UA Campus". This project requests to extend the project to 12.31.15 and move all remaining funds at fiscal year end to graduate student salary and ERE.  

The committee voted 8/0/0 in favor of this PAR at the 05.05.15 Green Fund Committee open meeting.
',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 05.07.15.',
'vp_approved' => 1,
'created_at' => '2015-04-24 22:46:48.000000',
'timestamp_review' => '2015-05-07 18:53:42.000000',
'netid_review' => 'natalieramos',
'timestamp_vp' => '2015-05-07 19:07:19.000000',
'netid_vp' => 'natalieramos',
'project_id' => 448,
),
70 => 
array (
'id_old' => 299973,
'netid' => 'uafrank0',
'changes' => 'We are requesting a PAR for our Biofuel Demonstration Project. Our one undergraduate student who is currently paid to work on the project, will be participating in an internship for her major this summer and will be out of the state. She plans to return to the project in the fall semester. 

Thank you,

Dr. Ed Franklin
Project Manager',
'advice' => 'Dr. Vito, 

The Green Fund Committee received this PAR from Ed Franklin on 04.26.15, GF 14.24 "Biofuel Demonstration Trailer". This project requests a new project end date that will be 06.30.16.

The committee voted 8/0/0 in favor of this PAR at the 05.05.15 Green Fund Committee open meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 05.07.15.',
'vp_approved' => 1,
'created_at' => '2015-04-26 16:19:40.000000',
'timestamp_review' => '2015-05-07 18:57:54.000000',
'netid_review' => 'natalieramos',
'timestamp_vp' => '2015-05-07 19:07:29.000000',
'netid_vp' => 'natalieramos',
'project_id' => 369,
),
71 => 
array (
'id_old' => 299974,
'netid' => 'bmpryor',
'changes' => 'We would like to move $4500 of our budget from Personnel to Operations.  The reason for this need is two-fold; First, we utilized more Internships (for credit) this year and less paid Lab Assistants that we budgeted for.  As a result, we will not need all of the Personnel funds.  Second, our production has increased this year more than projected and we have increased production costs in terms of media, lab supplies, production bags, and autoclave charges.  As a result, we will need more funds in Operations to cover these expenses.

Thank you for your support!!
Barry Pryor
The MycoCats',
'advice' => 'Dr. Vito, 

The Green Fund Committee received this PAR from Barry Pryor on 04.27.15, GF 14.32 "Production of Common and Gourmet Mushrooms from Landscaping and Consumer Waste Products". This project requests to move $4,500 of their budget from personnel to operations. This program also requests a new project end date that will be 06.30.16. 

The committee voted 8/0/0 in favor of this PAR at the 05.05.15 Green Fund Committee open meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 05.07.15.',
'vp_approved' => 1,
'created_at' => '2015-04-27 23:58:11.000000',
'timestamp_review' => '2015-05-07 19:00:37.000000',
'netid_review' => 'natalieramos',
'timestamp_vp' => '2015-05-07 19:07:42.000000',
'netid_vp' => 'natalieramos',
'project_id' => 390,
),
72 => 
array (
'id_old' => 299975,
'netid' => 'rudnick1',
'changes' => '1)	The $2,500 slated to pay Barbara Eliswerth, Director for the Iskashitaa Refugee Network be moved from personnel to operations.

2)	Move $17,700 from undergraduate salary/ERE  to Graduate salary/ERE',
'advice' => 'This PAR was submitted in error. It should have been entered into account GF 16.07P. It will be entered into the site today.',
'approved' => 0,
'vp_comments' => 'This PAR was submitted in error. It should have been entered into account GF 16.07P. It will be entered into the site today',
'vp_approved' => 0,
'created_at' => '2015-04-28 18:09:57.000000',
'timestamp_review' => '2015-05-04 17:54:27.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2015-05-04 17:54:59.000000',
'netid_vp' => 'rudnick1',
'project_id' => 396,
),
73 => 
array (
'id_old' => 299976,
'netid' => 'rudnick1',
'changes' => '1) The $2,500 salary and associated ERE slated to pay Barbara Eliswerth, Director for the Iskashitaa Refugee Network be moved from personnel to operations.                                                                                                                                                    2) Move $17,700 from undergraduate salary/ERE to Graduate salary/ERE',
'advice' => 'This PAR submitted in error. Please see PAR GF-76.',
'approved' => 0,
'vp_comments' => 'This PAR submitted in error.',
'vp_approved' => 0,
'created_at' => '2015-05-04 18:05:46.000000',
'timestamp_review' => '2015-05-06 22:04:24.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2015-05-06 22:04:59.000000',
'netid_vp' => 'rudnick1',
'project_id' => 396,
),
74 => 
array (
'id_old' => 299977,
'netid' => 'rudnick1',
'changes' => '1) The $2,500 salary/ERE slated to pay Barbara Eliswerth, Director for the Iskashitaa Refugee Network be moved from personnel to operations.

2) Move $17,700 from undergraduate salary/ERE to Graduate salary/ERE',
'advice' => 'Dr. Vito, 

The Green Fund Committee received this PAR from Melanie Lenart on 05.04.15, GF 16.07 "Linking Edible Arizona Forests (LEAF) on the UA Campus". This project requests to move $17,700 of their budget from undergraduate salaray/ERE to Graduate salary/ERE. This program also requests to move the $2,500 slated to pay Barbara Eliswerth, Director for the Iskashitaa Refugee Network, from personnel to operations.  

The committee voted 8/0/0 in favor of this PAR at the 05.05.15 Green Fund Committee open meeting.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 05.07.15.',
'vp_approved' => 1,
'created_at' => '2015-05-04 18:24:14.000000',
'timestamp_review' => '2015-05-07 19:04:54.000000',
'netid_review' => 'natalieramos',
'timestamp_vp' => '2015-05-07 19:07:52.000000',
'netid_vp' => 'natalieramos',
'project_id' => 472,
),
75 => 
array (
'id_old' => 299978,
'netid' => 'aabrown5',
'changes' => 'The Green Degree Guide and Green Engagement Guide Green Fund projects would like to move the remaining Grad Assistant Funding and Grad Assistant ERE to Student Employees and Student Employee ERE. The project no longer needs a Graduate Assistant, nor does it have the funds to support one. However, in order to fill the website with opportunities and troubleshoot the website we will need to continue to pay undergraduate student employees. This will complete the project in the next year. We have hired two student employees for the fall to fill these positions.',
'advice' => 'This PAR was already voted on in GF-96. See that entry.',
'approved' => 1,
'vp_comments' => 'This PAR was already voted on in GF-96. See that entry.',
'vp_approved' => 1,
'created_at' => '2015-05-13 22:17:46.000000',
'timestamp_review' => '2016-09-27 20:01:40.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-09-27 20:02:21.000000',
'netid_vp' => 'rudnick1',
'project_id' => 405,
),
76 => 
array (
'id_old' => 299979,
'netid' => 'aabrown5',
'changes' => 'The Green Degree Guide and Green Engagement Guide Green Fund projects would like to move the remaining Grad Assistant Funding and Grad Assistant ERE to Student Employees and Student Employee ERE. The project no longer needs a Graduate Assistant, nor does it have the funds to support one. However, in order to fill the website with opportunities and troubleshoot the website we will need to continue to pay undergraduate student employees. This will complete the project in the next year. We have hired two student employees for the fall to fill these positions.',
'advice' => 'This PAR was already voted on in GF-96. See that entry.',
'approved' => 1,
'vp_comments' => 'This PAR was already voted on in GF-96. See that entry.',
'vp_approved' => 1,
'created_at' => '2015-05-13 22:18:03.000000',
'timestamp_review' => '2016-09-27 20:02:00.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-09-27 20:02:36.000000',
'netid_vp' => 'rudnick1',
'project_id' => 405,
),
77 => 
array (
'id_old' => 299980,
'netid' => 'rudnick1',
'changes' => 'Barry Pryor submitted this PAR on 04.27.15 (PAR ID# GF-74). It was put into his FY14 account - not the FY15 account. It read:
PAR ID#: GF-74
Submitted: Mon, April 27, 2015, 4:58:11 PM
by NetID: bmpryor

Requested Changes (Please describe the changes you wish to make to your project in the space below. Limited to 1500 words.):
We would like to move $4500 of our budget from Personnel to Operations. The reason for this need is two-fold; First, we utilized more Internships (for credit) this year and less paid Lab Assistants that we budgeted for. As a result, we will not need all of the Personnel funds. Second, our production has increased this year more than projected and we have increased production costs in terms of media, lab supplies, production bags, and autoclave charges. As a result, we will need more funds in Operations to cover these expenses.

Thank you for your support!!
Barry Pryor
The MycoCats',
'advice' => 'Barry Pryer put this PAR into the wrong account. It was approved correctly, just not transferred over until now.
UA Green Fund Committee Advice
Submitted: Thu, May 7, 2015, 12:00:37 PM
by NetID: natalieramos

Advice (Please detail the Committee\'s recommendations in the field below. Limited to 1500 words.):
Dr. Vito,

The Green Fund Committee received this PAR from Barry Pryor on 04.27.15, GF 14.32 "Production of Common and Gourmet Mushrooms from Landscaping and Consumer Waste Products". This project requests to move $4,500 of their budget from personnel to operations. This program also requests a new project end date that will be 06.30.16.

The committee voted 8/0/0 in favor of this PAR at the 05.05.15 Green Fund Committee open meeting.

Does the UA Green Fund Committee approve this PAR?
Yes

As needed, attach a document with supporting data:
No file attached.',
'approved' => 1,
'vp_comments' => 'VP Approval
Submitted: Thu, May 7, 2015, 12:07:42 PM
by NetID: natalieramos

Comments (Optional / Limited to 1500 words):
Dr. Vito approved this PAR on 05.07.15.

VP Approval (Does the VP for Student Affairs and Enrollment Management approve this PAR?):
Yes',
'vp_approved' => 1,
'created_at' => '2015-07-15 22:55:29.000000',
'timestamp_review' => '2015-07-15 23:00:25.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2015-07-15 23:01:09.000000',
'netid_vp' => 'rudnick1',
'project_id' => 421,
),
78 => 
array (
'id_old' => 299981,
'netid' => 'samanthaorwoll',
'changes' => 'The Green Purchasing Team has approximately $800 left in the travel budget and requests all travel funds be moved to operations. This will allow the team to purchase samples of the environmentally friendly products we endorse. Having physical examples rather than an online picture allows purchasers to truly get a sense for the product and to ensure the quality and other attributes will be appropriate. They won’t have to worry if the product they are receiving is what they want. In the spring semester, the Green Purchasing Team will focus more on offering environmentally friendly products to events around campus. Many of these products are food containers. When purchasing food containers, it is important to know if products will stand up to heat, oil, and other conditions. Having the products in hand would allow both purchasers and the Green Purchasing Team to test them. Additionally, with this increase in eco-friendly food containers, many of these events will involve handling whether the products go to landfill, recycle, and compost. The Green Purchasing Team will be a part of the process of diverting these products, so part of the operations funds will also go to purchasing Sustainable UA branded vests. With this increased visibility, at basketball games for example, a vest would be seen by thousands and will show the sustainable initiatives at work at the University of Arizona. These vests would be reused for workers and volunteers at future Zero Waste Events.',
'advice' => 'This project requests to move $800 from travel to operations. The Green Fund approved this PAR during the open meeting on 01.28.16.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 02.10.16.',
'vp_approved' => 1,
'created_at' => '2015-12-07 22:00:33.000000',
'timestamp_review' => '2016-02-26 18:29:56.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-02-26 18:32:02.000000',
'netid_vp' => 'rudnick1',
'project_id' => 437,
),
79 => 
array (
'id_old' => 299982,
'netid' => 'mapel',
'changes' => 'In order to maximize the number of student externs that we can hire for the summer of 2016 in our Cooperative Extension and Externships program, we are requesting to transfer $4,500 from Operations to Personnel in our FY16.40 budget. Thank you for your consideration.',
'advice' => 'This PAR request to move $4,500 from operations to student salary. The committee voted to approve this PAR during the Green Fund open meeting on 01.28.16.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 02.10.16.',
'vp_approved' => 1,
'created_at' => '2015-12-17 20:44:44.000000',
'timestamp_review' => '2016-02-26 19:45:04.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-02-26 19:46:10.000000',
'netid_vp' => 'rudnick1',
'project_id' => 458,
),
80 => 
array (
'id_old' => 299983,
'netid' => 'bmpryor',
'changes' => 'The MycoCats are requesting a modification of our budget within the Personnel category.  We originally requested $17,000 salary and $2400 ERE for graduate student support.  We have since been able to secure a TA position for this graduate student and would like to move these funds over to Student Employees (undergraduate) support.  The graduate student will still be working with the MycoCats in a leadership and advisory position, but much of the daily managerial duties of time scheduling and work flow organization will be conducted by undergraduate shift leaders.',
'advice' => 'This PAR approved by the Green Fund Committee during the Open meeting on 01.28.16. The committee approved the request to move remaining graduate salary funds to undergraduate salary.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito approved the recommendations of the Green Fund Committee on 02.10.16. This PAR is approved.',
'vp_approved' => 1,
'created_at' => '2015-12-21 20:36:28.000000',
'timestamp_review' => '2016-03-23 22:46:00.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-03-23 22:50:07.000000',
'netid_vp' => 'rudnick1',
'project_id' => 470,
),
81 => 
array (
'id_old' => 299984,
'netid' => 'kebonine',
'changes' => '20 January 2016
Please move remaining $2400 travel budget (travel-related request was not more than a few hundred dollars for van transport in the original ask but somehow got changed in the budget spreadsheet along the way) to the overspent operations category (which was $5000 in the original ask, not $2600). 
Please also transfer $200 from \'supplemental comp\' (which should all be \'student employee\' because we were able to hire a great undergraduate instead of the expected graduate student) to cover the overspend in \'other\' that comprised stipends for the many participating teachers.
Thank you, Kevin Bonine',
'advice' => 'The Green Fund Committee reviewed this PAR during the open meeting on 01.28.16. The committee approved this PAR to move $2,400 from travel to operations and $200 from supplemental compensation to operations.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito approved the recommendations of the Green Fund Committee on 02.10.16. This PAR is approved.',
'vp_approved' => 1,
'created_at' => '2016-01-21 05:01:52.000000',
'timestamp_review' => '2016-03-23 22:48:14.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-03-23 22:50:25.000000',
'netid_vp' => 'rudnick1',
'project_id' => 477,
),
82 => 
array (
'id_old' => 299985,
'netid' => 'cfp',
'changes' => 'The UA Community Garden is currently significantly under its expected budget for operations, with less than expected maintenance and irrigation costs so far this year. Meanwhile, UACG is over budget for the student garden manager\'s payroll. The UA Community Garden team requests to move $1000 from operations to payroll.',
'advice' => 'The Green Fund Committee approved this PAR to move $1,000 from operations to student salary during the open meeting on April 7, 2016.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations of the Green Fund Committee on 04.15.16.',
'vp_approved' => 1,
'created_at' => '2016-03-02 20:38:52.000000',
'timestamp_review' => '2016-04-15 21:26:53.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-04-15 21:36:29.000000',
'netid_vp' => 'rudnick1',
'project_id' => 461,
),
83 => 
array (
'id_old' => 299986,
'netid' => 'uafrank0',
'changes' => 'We are submitting a PAR request based on the recent email received on 3/23/2016 regarding use of funds from the Green Fund Committee to extend access to those funds after April 25, 2016. We anticipate the project will be completed by June 30, 2016. We have acquired most the materials we will need to complete the project. There will be a need to rent an attachment for our auger from a local equipment vendor (as we discovered during the installation process of the first project) and plumbing fittings for the project based on alterations made following the installation and operation of the first mock well project.',
'advice' => 'This PAR withdrawn as Ed Franklin estimates the project will conclude by June 30, 2016.',
'approved' => 1,
'vp_comments' => 'This PAR withdrawn.',
'vp_approved' => 1,
'created_at' => '2016-03-28 16:23:55.000000',
'timestamp_review' => '2016-04-07 16:49:47.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-04-07 16:50:38.000000',
'netid_vp' => 'rudnick1',
'project_id' => 462,
),
84 => 
array (
'id_old' => 299987,
'netid' => 'spater',
'changes' => 'We would like to make a program alteration request (PAR) for the “UA Sierra Vista: Sustainable Landscape Demonstration Garden & Learning Center” project – Board Assigned ID: GF 16.05. The major component of this project is the construction of the outdoor classroom structure. Despite all parties involved supporting the project, we are still awaiting formal approval of a facility use agreement between the UA and the UA South Foundation. This agreement must be approved before bids can be obtained and construction can begin. Due to the delay in getting approvals we have made no expenditures to date. We did not want to spend funds without knowing we could fully complete the project. As of March 25th, we now have an agreement that is being sent to the Foundation, with approval expected by April 15th. Once approved we will be able to move forward, but we will most likely not be able to complete the project by June 30th. This shortened time frame will also not allow us to hire the students to assist with planning and implementation and will lessen our ability to fully engage the students as desired. In order to complete the project as intended, we request to have the completion deadline extended from June 30, 2016 to December 15, 2016.

Thank you for your consideration of our request.',
'advice' => 'The Green Fund Committee approved this PAR to extend the project end date to 12.31.16 during the open meeting on April 7, 2016.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations of the Green Fund Committee on 04.15.16.',
'vp_approved' => 1,
'created_at' => '2016-03-28 22:50:11.000000',
'timestamp_review' => '2016-04-15 21:28:02.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-04-15 21:37:26.000000',
'netid_vp' => 'rudnick1',
'project_id' => 455,
),
85 => 
array (
'id_old' => 299988,
'netid' => 'samanthaorwoll',
'changes' => 'The Green Purchasing Team requests a project extension to extend the project until December 31, 2016. The team will have approximately $2,700 left for salaries when the spring semester finishes. With any remaining funds for salaries, the team would like to work on the move-out project with Residence Life and Procurement and Contracting Services. Working with a Green Team member, a Green Purchasing Project member will help sort and record the items collected during move-out that will be re-sold to incoming students in the fall. This will give the Green Purchasing Team insight into what items are most frequently purchased and discarded by students each year. Using this information, the Green Purchasing Project member will create a guide with suggestions for more sustainable items. The guide will be completed by December 31, 2016 and Residence Life will help support its dissemination.',
'advice' => 'The Green Fund Committee approved this PAR to extend the project end date to 12.31.16 during the open meeting on April 7, 2016.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations of the Green Fund Committee on 04.15.16.',
'vp_approved' => 1,
'created_at' => '2016-03-30 04:45:10.000000',
'timestamp_review' => '2016-04-15 21:29:26.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-04-15 21:36:50.000000',
'netid_vp' => 'rudnick1',
'project_id' => 437,
),
86 => 
array (
'id_old' => 299989,
'netid' => 'ebitieamughan',
'changes' => 'The UA Green Team project managers are requesting a PAR in the amount of $747.00 from travel to operations. We are also a seeking a PAR to amend the project timeline and extend the project till December 31, 2016. The transferred funds will help finance the Green Team’s summer project – sustainable move out in collaboration with UA Residence Life and UA Procurements. The transferred funds will be directed to payroll and operations in order to make sure the event is fully staffed and a success. This project is a pilot and we would sincerely appreciate the support of the Green Fund committee in approving our request to extend the original proposed timeline and amend the appropriated funds.
Thank you,
Green Team Project Managers',
'advice' => 'The Green Fund Committee approved this PAR to move remaining travel funds to operations and extend the project end date to 12.31.16 during the open meeting on April 7, 2016.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations of the Green Fund Committee on 04.15.16.',
'vp_approved' => 1,
'created_at' => '2016-03-30 06:21:25.000000',
'timestamp_review' => '2016-04-15 21:30:42.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-04-15 21:37:06.000000',
'netid_vp' => 'rudnick1',
'project_id' => 459,
),
87 => 
array (
'id_old' => 299990,
'netid' => 'acec',
'changes' => 'Original Objectives of UA Green Fund Grant: 

The UA Office of Early Academic Outreach endeavored to build four aquaponic systems in and around Tucson in an effort to increase efforts toward a stronger understanding of this alternative growing method. However, there have been certain circumstances beyond the office’s control that have led to a request for a PAR to change the way in which the funding might be spent for a portion of the grant awarded. Two of these systems have been built without incident or hindrance at Biosphere 2 and Tucson Magnet High School, but two remain unbuilt at this time. One of these is still intended to be built before the end of the fiscal year, but we do not anticipate having the capacity at this time to complete the fourth build. 

Personnel change at a partner organization has been the principal factor that has prevented budget spending in the manner originally requested in the grant application. The organization mentioned is UA Yuma campus. Due to the departure of the main contact at this site, we do not foresee an ability to secure an aquaponics build within this fiscal year, especially given the distance from the UA Yuma campus and the complexity of arranging materials to be delivered. 

Therefore, it is our request to change the grant spending to alter funding set aside for one of the aquaponic systems (UA Yuma Campus) to fund an alternate project that targets an existing campus program. Very generally stated, curriculum enrichment along the lines of environmental sustainability of the program is the baseline goal, and we aim to accomplish this by bridging environmental research and basic computer science skills, but a deeper description follows. 

About the program: 
The Native American Science & Engineering Program (NASEP) is a free, year-long program designed to provide Native American high school students with the necessary resources to enroll in college and pursue a career in a Science Technology Engineering & Mathematics (STEM) field. NASEP participants will gain a wide exposure to these fields through interactions with university experts on many subjects including environmental sustainability. The intention of this program is to expose students to a wide variety of STEM pathways so as to better inform their college pathways in the context of the Native American experience. 

Sustainability has been a focus of this outreach program for the past three years in terms of students’ research projects. Each student has pursued a project focused on water quality, food production or weather and climate. To equip students for this undertaking, the program has given them specific tools in order to conduct basic research in their home communities, a process that is later developed into a professional poster and presentation session on the UA campus; this is an event which has secured between 200 and 300 attendees over the past few years. The program’s goal is to continue to deepen the learning experiences in a manner that continues to prepare students for academic pathways related to environment- and sustainability-related fields. 

Alignment to the UA Green Fund’s Mission: 

Although the UA Office of Early Academic Outreach primarily focuses on pre-college students, there are a number of ways in which this project would enrich the experiences of current college students as well. Two residential advisors and volunteers working during the summer program will gain exposure to all aspects of the curriculum, including the ability to participate in programming. Furthermore, five counselors will support NASEP students’ exploration of applying digital technology to research projects grounded in environmental sustainability. These UA students will see the greatest benefits from this project by receiving training and later supporting the facilitation of this aspect of the research project. Through such exposure, it is conceivable that such learning could somehow impact their academic pathways at the UA. 

This PAR would foster collaboration between units at the UA by facilitating a workshop from the college of engineering on basic computer science so as to orient students toward data collection. The iSpace is another unit on campus that is highly geared to assist with instructional aspects of this project, and could provide students with a helpful working space for learning foundational concepts during the program.  

Lastly, this PAR aims to make the UA a more sustainable place by equipping pre-college students with tangible tools that would allow them to pursue studies in environment- and sustainability-related fields. Furthermore, the impacts would be felt upon presenting the final posters as a culmination of the research that students were able to gain. Attendees at this event have included UA faculty, graduate and undergraduate students who have a shared interest in promoting and pursuing environmental sustainability and STEM fields, thereby further impacting current UA students. 

How the PAR Will Enrich the Program: 
Students will deepen their knowledge of sustainable initiatives by using technology to gain a deeper understanding of the natural environment of their home communities. Such high school-level research will allow students to plan on applying these foundational skills to their selected majors. The research that students would be conducting would allow them to gain a deeper, more technical understanding of the natural systems in which they live in addition to allowing them to have a more technically oriented research project. Skills gained through these experiences (specifically the skill of using an instrument to collect very specific types of data) are transferrable to undergraduate experiences and graduate pursuit of various studies—as is the utilization of such data on a research project and poster. 

If approved, this PAR would allow for long-term program enrichment by securing a total number of Arduino weather & climate kits that can be used by any of the research groups. These would not only be usable for a single year, but they would also be usable for as many years as they remain functional. Furthermore, even though the participants in this program are pre-college students, a discernible tendency is that former participants have previously enrolled in studies at the UA, a trend that is anticipated to continue.  

Budget Alteration: 
Per unit and in sum, the costs for this project are as follows: 

Arduino UNO R3 ($30)
DHT22 temperature-humidity sensor + extras ($12.00)
BMP183 Barometric Pressure/ Altimeter ($12.00)
LM393 Light Sensor ($3.00)
FC-28-D Soil Humidity Moisture Detection Module ($4.00)
Solderless Breadboard ($6.00)
Jumper Wires ($3.00)
Arduino Uno Case Enclosure ($12.00)

$82 per unit

X24 units (one for each participant) = $1968 TOTAL

*Prices anticipate shipping costs

These units will be capable of monitoring temperature, humidity, barometric pressure, altitude, light, and soil humidity. With these tools at hand, students will be able to conduct meaningful data collection in the context of exploring natural ecosystems, a process that can be done either in an abbreviated period of time or during more extended stretches, depending on program needs. 

After the program is completed, the UA Office of Early Academic Outreach would collect the units so as to re-use them for the following year. In order to secure this long-term use, mechanisms such as signed check-out forms and deposits will be used in order to maintain the devices.',
'advice' => 'The Green Fund Committee reviewed this PAR to change the scope of the project. One aquaponic system will not be installed and funds set aside for this will be used for outreach and education. Project categories will not change, just project outcomes. The Green Fund Committee approved this PAR during the open meeting on April 7, 2016.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations of the Green Fund Committee on 04.15.16.',
'vp_approved' => 1,
'created_at' => '2016-03-30 21:50:16.000000',
'timestamp_review' => '2016-04-15 21:35:01.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-04-15 21:37:44.000000',
'netid_vp' => 'rudnick1',
'project_id' => 474,
),
88 => 
array (
'id_old' => 299991,
'netid' => 'madelinebynes',
'changes' => 'Off-Campus Housing Sustainability (OCHS) requests to reallocate the remainder of the FY 16 grant funds to FY 17 budget.  We are planning on using the funding leftover from FY 16 to cover the pay for a student intern for either the whole year or at least the spring semester. The exact amount would be determined after the current OCHS Coordinator (OCHSC) completes work for the semester (last day for the 2015-2016 academic year is 5 May 2016). 
Proposal
Hire one part time student employee as an OCHS intern (OCHSI) for 3-5 hours per week at $8.50/hr.
Intern Responsibilities
•	Develop social media and marketing plans for all fall and/or spring programs
•	Shadow the OCHSC in contacting Featured Lister apartment complexes
•	Shadow the OCHSC on presentations and staff trainings
•	Do various research projects to better inform the OCHSC’s sustainability programming
•	Attend monthly team meetings with the Off-Campus Housing Sustainability Team
•	Develop relationships with on-campus sustainability entities (including but not limited to: the Office of Sustainability, Students for Sustainability, ASUA, ACT, the Women’s Resource Center, Sustainable Built Environments, Compost Cats, etc.)
Reasons for Additional Staffing 
•	OCHS could accomplish more and operate more efficiently with additional student staff. 
o	Additional staffing would allow for assistance with the day-to-day responsibilities that fall on the OCHSC, which would also mean better programming, more efficiency, and a greater chance of development. 
o	With a new OCHSC taking charge of the program, it makes sense to have someone assist with the general maintenance of the program, which leads in to the next point.
•	An addition student worker would give the OCHSC more time to focus on grant writing and programming in the fall and/or spring.
o	The program has developed in leaps and bounds in the past three years and the focus is now less on recruitment and more on maintenance and ensuring the longevity of the program. 
o	An additional position would provide the intern with familiarity with the unique OCHS program, as well as a broad range of professional skills as they take on projects that deal with off campus partners.',
'advice' => 'The UA Green Fund Committee approved this PAR during the open meeting on 04.28.16 to extend the project end date to 06.30.17.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 05.04.16.',
'vp_approved' => 1,
'created_at' => '2016-04-18 16:30:39.000000',
'timestamp_review' => '2016-05-04 23:02:16.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-05-04 23:09:14.000000',
'netid_vp' => 'rudnick1',
'project_id' => 468,
),
89 => 
array (
'id_old' => 299992,
'netid' => 'cfp',
'changes' => 'On Wednesday, April 20th, 2016, we received the following email from UA Facilities Management:

"Hello Paul,

We are shooting to be complete mid-May.

Thanks you,

Thomas Webb,
Project Manager, Sr.
Facilities Management
520-349-1062"

So, while we hope the project will be complete by mid-May and before June 30th, previous project delays from FM suggest that a PAR to extend the deadline to spend all project funds to 12/31/16 would be wise. It is very much our hope that the project will be completed this semester and in time to see the monsoon rains fill the cistern this summer. Yet prior experience has taught us to err on the safe side, and so we request an extension until year\'s end. -Sincerely, Chet Phillips and Paul Elias',
'advice' => 'The UA Green Fund Committee approved this PAR during the open meeting on 04.28.16 to extend the project end date to 12.31.16.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 05.04.16.',
'vp_approved' => 1,
'created_at' => '2016-04-20 20:30:45.000000',
'timestamp_review' => '2016-05-04 23:03:08.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-05-04 23:09:28.000000',
'netid_vp' => 'rudnick1',
'project_id' => 465,
),
90 => 
array (
'id_old' => 299993,
'netid' => 'cfp',
'changes' => 'Greening the Game will have significant funds remaining at the end of June 2016 but is planning for significant project expansion in scope and thus in both operations and labor needs next year. We would like to extend the time to spend remaining FY 2016 funds through June 30, 2017.',
'advice' => 'The UA Green Fund Committee approved this PAR during the open meeting on 04.28.16 to extend the project end date to 06.30.17.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 05.04.16.',
'vp_approved' => 1,
'created_at' => '2016-04-20 23:27:22.000000',
'timestamp_review' => '2016-05-04 23:03:52.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-05-04 23:09:40.000000',
'netid_vp' => 'rudnick1',
'project_id' => 464,
),
91 => 
array (
'id_old' => 299994,
'netid' => 'adamyarnes',
'changes' => 'Due to several complex factors i.e. weather, electrical engineering, location, staffing, and the desire to complete the project well. The James 4-H Outdoor Learning Center would like to request an extension to complete our FY16 project.  The James 4-H Outdoor Learning Center would like to request a completion date of 12/31/16. This time extension will allow us to effectively work with our solar engine to ensure top–notch quality of work while keeping our summer participants safe and away from possible electrical hazards.',
'advice' => 'The UA Green Fund Committee approved this PAR during the open meeting on 04.28.16 to extend the project end date to 12.31.16.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 05.04.16.',
'vp_approved' => 1,
'created_at' => '2016-04-21 17:08:50.000000',
'timestamp_review' => '2016-05-04 23:04:27.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-05-04 23:09:55.000000',
'netid_vp' => 'rudnick1',
'project_id' => 451,
),
92 => 
array (
'id_old' => 299995,
'netid' => 'acec',
'changes' => 'PAR Proposal

The Office of Early Academic Outreach has an excess of funding that could be used to deepen and broaden engagement on the project over a longer period of time. This request is for a timeline extension through December 31, 2016. 

Original Objectives of “Aquaponics and Education” UA Green Fund Grant

This grant was meant to engage undergraduate and graduate students through employment in managing an aquaponic system at the Native American Research and Training Center (NARTC)—in addition to building new aquaponic systems in collaboration with educational partners in the Tucson community and beyond. The student employee budget spending so far reflects employment of four undergraduate students working to maintain the aquaponic system at NARTC, and the graduate student spending measures one graduate student’s employment to oversee and coordinate necessary tasks for optimal system maintenance. 

PAR Changes

Though the above objectives have been largely accomplished at this time, remaining funding could be devoted to extending the impacts of this project in meaningful ways. Due to varying student schedules and holiday breaks, a fair amount of funding remains in the student employee budget category. With this amount of funding, the project could continue through the winter so that students seeking to develop a project around aquaponics would have a chance to continue working with the system. There also exists potential for summer/fall undergraduate interns to connect work done with high school-level students participating in outreach programming by aligning project parameters in such a way that undergraduates could easily serve as examples for pre-college youth developing similar projects around the same time. 

Additionally, operations is a category that contains a significant amount of *current* funding (though a fair amount of this (~$2,000) will be spent before the end of the fiscal year on one remaining aquaponic system build and on Arduino kits for a summer program). The presence of remaining operational funding would allow for a small cushion for incidental maintenance costs of the aquaponic systems so as to ensure quality maintenance when needed for a longer period of time. 

In sum, the extension of the project through December 31st would allow for deeper engagement for any undergraduate hires, as accomplished through a project facilitated by Early Academic Outreach staff, and it would provide adequate time for the project to be completed in a tidy manner given the costs and development of the projects so far.',
'advice' => 'The UA Green Fund Committee approved this PAR during the open meeting on 12.31.16 to extend the project end date to 06.30.17.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 05.04.16.',
'vp_approved' => 1,
'created_at' => '2016-04-22 23:13:57.000000',
'timestamp_review' => '2016-05-04 23:06:48.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-05-04 23:10:13.000000',
'netid_vp' => 'rudnick1',
'project_id' => 474,
),
93 => 
array (
'id_old' => 299996,
'netid' => 'aabrown5',
'changes' => 'We recently learned that we still have around $10,000 in student salary/ERE and $850 in operations. We would like to request a project extension with a new end date of 6/30/17. As our undergrad students are both graduating this year, we would like to use the remaining funds to hire another undergraduate student to monitor, update, and continue to market the green guides.',
'advice' => 'The UA Green Fund Committee approved this PAR during the open meeting on 04.28.16 to extend the project end date to 06.30.17.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 05.04.16.',
'vp_approved' => 1,
'created_at' => '2016-04-25 17:16:22.000000',
'timestamp_review' => '2016-05-04 23:07:29.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-05-04 23:10:31.000000',
'netid_vp' => 'rudnick1',
'project_id' => 431,
),
94 => 
array (
'id_old' => 299997,
'netid' => 'bchampion',
'changes' => 'This PAR is to request an extension of remaining unused project funding (approx. $6,000 - see explanation below) from FY16 to FY17, and to request a change in the scope of work along with this extension.  

Change in Scope of Work: 

The project has been successful in establishing relationships with outside community organizations, and at generating new Green Engagement Guide listings.  Additionally, a number of the organizations contacted were interested in working more closely with the Office of Sustainability to develop new internship programs collaboratively for students.  Among these were the City of Tucson, Pima County, and the Pima Assocation of Governments.

The Office of Sustainability wants to work with City of Tucson, Pima County, and Pima Association of Governments to develop a local governments internship program in sustainability through them.  This will require hiring a full-time coordinator.  

The new position would start early in FY17, and would be responsible for developing a full program.  This would consist of working with the local government agencies to fully describe the internship opportunities, market and recruit the opportunities among UA students, support UA students in on-boarding with these organizations, supervise the internships on the UA side, secure 100% Engagement credentials from UA for the student experiences, and provide administrative support and assessment for the program as a whole.  The coordinator would also provide periodic on-campus programs for the collection of student interns so that they can network with each other, and hopefully develop more collaborative relationships between the local agency departments they serve.

If this is successful, it would likely yield 15-20 high quality student internships year after year, with substantial administrative support and leadership within UA.  It also has potential to become a more substantial program in community governmental leadership training through partnership with the College of Social and Behavioral Science, with additional program elements beyond the internships themselves.  

The Office of Sustainability will be developing significant additional sources of funds to support this new full-time coordinator.  The $6,000 remaining project funds would supplement this effort.  Other sources of funding will be a Haury Fund Seed Grant (applying for $25,000 for FY17 and FY18), RCM tuition funds through intern enrollments in a course section within the College of Social and Behavioral Sciences (variable, depending on enrollments and negotiated rates, but potentially close to $100 per enrolled credit hour), and finally the Office of Sustainability will access departmental emergency reserve funds for any remaining funding necessary for the position, so this shift of funding will save the Office of Sustainability from deficit spending by $6,000.  The total funding required to support the new position is $47,000 in FY17.


Explanation of Remaining Funds/Extension:

As of April 19, our project had approximately $6,900 remaining from the original $18,000 allocated for FY16.  We estimate two more pay periods of expenses for our student employees before the end of this year\'s project activities, an approximate $1,000.  This would leave an approximate $6,000 in funds remaining from this project.

The reason for this level of remaining funds is two-fold: 1) Very little of the $2,500 allocated for project operations was spent, because the project was almost entirely labor intensive and operations expenses for our student ambassadors were not needed; 2) while we estimated 10hrs/week of work by the student ambassadors, in reality each of the ambassadors didn\'t consistently work the entire 10 hours each week, and therefore there were unused student wages of close to $3,500.  

Budget Detail:

The result of approving this PAR would be to allow the transfer of $2,385 from operations to Personnel - Appointed/Faculty Regular category, $2,215 from Personnel - Student Employees to Personnel - Appointed/Faculty Regular, and $1,400 from Personnel - Student Employees to ERE - Appointed/Faculty Regular category.  

The result is the budget spreadsheet as attached.  This spreadsheet also shows our expenses as of April 19.',
'advice' => 'The UA Green Fund Committee approved this PAR during the open meeting on 04.28.16 to extend the project end date to 06.30.17. In addition, all remaining funds in student salary, ERE, and operations at the end of FY2016 will be moved to appointed faculty salary and ERE with a new project end day of 06.30.17.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 05.04.16.',
'vp_approved' => 1,
'created_at' => '2016-04-25 21:27:40.000000',
'timestamp_review' => '2016-05-04 23:18:01.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-05-04 23:18:23.000000',
'netid_vp' => 'rudnick1',
'project_id' => 467,
),
95 => 
array (
'id_old' => 299998,
'netid' => 'bmpryor',
'changes' => 'The MycoCats Recyling Program is actively expanding its student engagement efforts on campus with the development of a very enthusiastic student-run student club, the MycoCats, recognized through the ASUA.  Currently there are 17 members, but these are not the same students previously or currently working as interns or student employees in the Green Fund-supported MycoCats program, although there is some overlap.  With the current expansion of MycoCats outreach efforts on the UA campus, many more students have expressed interest and the membership of the student club membership is expected to grow substantially.  

The current program has previously requested a PAR to move funds from graduate student support to undergraduate student support since the supported graduate student found alternative sources of funding.  Thus, at this point there is currently $17,000 remaining in the employee category targeted for undergraduate support.  Although the MycoCats will employ some undergraduates in summer 2016, there is not enough time to use the entire $17,000 by the end date of the current grant.  Moreover, 2016-2017 funding for the MycoCats has not been established, yet there will still be the need to support some students as employees during 2016-2017 to insure the program continues to grow for years. 

Therefore, the MycoCats are requesting a project extension with a new project end date of 06.30.17. This will provide extra time to properly use all funds within categories (or transfer to other categories), to continue support for both the recycling effort and increased student engagement through the next fiscal year, and to insure all project objectives are completed as proposed.  Thank you.',
'advice' => 'The UA Green Fund Committee approved this PAR during the open meeting on 04.28.16 to extend the project end date to 06.30.17.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 05.04.16.',
'vp_approved' => 1,
'created_at' => '2016-04-26 06:09:26.000000',
'timestamp_review' => '2016-05-04 23:08:06.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-05-04 23:10:47.000000',
'netid_vp' => 'rudnick1',
'project_id' => 470,
),
96 => 
array (
'id_old' => 299999,
'netid' => 'noellee',
'changes' => 'Thus far, the Sky School has implemented the composting and recycling programs proposed in this grant. While the initial design of practices have been working, it is evident that in order for these programs to be continued indefinitely the processes need to go through iterative assessment and refinements. This is ongoing and we would like to request a project extension to June 30, 2017. We feel confident that we will have worked out any inefficiency in the composting and recycling program in that time. In addition, the Sky School will soon have a graphic design student who will be designing new info-graphics for these new waste programs as well as advertising SFS, ASUA and the Green Fund, which has made all of this possible, in our field notebooks and posted info-graphics. Much of this work will take place at the start of next semester and will require the remaining operations funds. Finally, UA Sky School spent this semester coordinating the undergraduate project. This project will create a job for one or two undergraduate students to work with the UA Sky School and Mountain Operations to track the current waste management system and present a sustainability overview to our participating schools. Undergraduate students will be able to apply for this project next semester. This will utilize the remaining scholarship funds awarded in this grant.',
'advice' => 'This PAR was submitted after the PAR end-of-year deadline thus it is not approved. End of project deadline continues to be 06.30.16.',
'approved' => 0,
'vp_comments' => 'This PAR was submitted after the PAR end-of-year deadline thus it is not approved. End of project deadline continues to be 06.30.16.',
'vp_approved' => 0,
'created_at' => '2016-05-10 19:35:46.000000',
'timestamp_review' => '2016-05-23 17:56:04.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-05-23 17:56:26.000000',
'netid_vp' => 'rudnick1',
'project_id' => 453,
),
97 => 
array (
'id_old' => 2999100,
'netid' => 'rudnick1',
'changes' => 'Barbara asked me submit this PAR for her:
From: Barbara Eiswerth [mailto:eiswerth@iskashitaa.org] 
Sent: Wednesday, August 17, 2016 2:12 PM
To: Rudnick, Julia L - (rudnick1) 
Subject: UAGreen Fund Committee, budget change justification



Dear UAGreen Fund Committee: At the end of the 2016 Spring semester we realized that UALEAF had underutilized the designated undergraduate hours as the students could only dedicate less than 10 hours a week due to their academics. We originally had two  1/4 time graduate students budgeted but one dropped out early in the Fall semester thus we thought we had a surplus in student hours to use. We also went from two temporary contracts (terminology could be incorrect) when Melanie Lenart left the program and the university. The hours of the students and accounting tracking were not available to the PI after Dr. Lenart left the project and the project was shifted to SNRE further hampering Eiswerth ability to track funding. 

Since Emily Marderness, undergraduate intern for UALEAF had free time until the end of the fiscal year. We asked Emily to stay utilizing almost full time hours to document best practices, harvesting summer fruiting edible trees, as well as maintaining our Social media presence. The UALEAF facebook continues to grow and educate the student body and beyond. We apologize that this shift in funding was not first authorized by the UAGreen but good money went to a great cause of educating UA Campus on sustainable practices as well as empowering students to take action against rampant food waste.  

Thank you for your understanding in this matter. If you have additional questions please feel free to contact me. 

-- 
Barbara Eiswerth, Ph.D.
Executive Director
(520) 481-3430
Iskashitaa Refugee Network
UALEAF 

Harvesting Hope, Empowering Change
www.iskashitaa.org | facebook.iskashitaa.org |',
'advice' => 'Dear Project Manager,
The Green Fund did not approve your PAR request as it was submitted after the deadline to submit PAR\'s. 
Thank you,
The UA Green Fund Committee',
'approved' => 0,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 10.10.16.',
'vp_approved' => 0,
'created_at' => '2016-08-17 21:44:23.000000',
'timestamp_review' => '2016-10-25 15:45:24.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-10-25 15:46:54.000000',
'netid_vp' => 'rudnick1',
'project_id' => 472,
),
98 => 
array (
'id_old' => 2999101,
'netid' => 'aabrown5',
'changes' => 'I am seeking a change to the grant to enable the hiring of a graduate student to assist with the final marketing and outreach portions of the grant. Brianna Zurita has the connections, passions, and organizational skills to really help greenguides.arizona.edu rocket out into the UA\'s consciousness, helping to make this grant a resounding success.',
'advice' => 'The Green Fund Committee approved this PAR to move remaining funds left in account at the end of FY16 from undergraduate salary/ERE to graduate salary/ERE.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 10.10.16.',
'vp_approved' => 1,
'created_at' => '2016-08-29 17:43:39.000000',
'timestamp_review' => '2016-10-25 15:59:14.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-10-25 15:59:44.000000',
'netid_vp' => 'rudnick1',
'project_id' => 431,
),
99 => 
array (
'id_old' => 2999102,
'netid' => 'cfp',
'changes' => 'At long last, the Highland Cistern project is complete, and the cistern is full of 20,000 gallons of water that will significantly reduce the UA Community Garden\'s groundwater use and irrigation bills. A cistern ribbon cutting event will be held in November to showcase the cistern as a major new sustainability installation on campus, and the Green Fund will be credited with making it possible.

A total of $689.38 remains in the cistern account, and this PAR is to request use of the remainder to help fund Paul Elias, the student who managed the project and saw it to completion, to attend the AASHE Conference in Baltimore in October, where he will present the Highland Cistern project for a national audience.

While $689.38 will not fully fund Paul\'s flight (~500), lodging (~$200 for 4 nights), and conference registration ($200), Students for Sustainability is willing to fund the remainder to ensure that Paul can attend and receive recognition for the outstanding work he has done on the cistern project.',
'advice' => 'The UA Green Fund committee approved this PAR to move remaining funds from operations to travel. The student working on the cistern project will present this project at the AASHE conference in October 2016.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 10.10.16.',
'vp_approved' => 1,
'created_at' => '2016-09-26 16:56:07.000000',
'timestamp_review' => '2016-10-25 16:02:11.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-10-25 16:02:31.000000',
'netid_vp' => 'rudnick1',
'project_id' => 465,
),
100 => 
array (
'id_old' => 2999103,
'netid' => 'bchampion',
'changes' => 'A previously approved PAR had requested to use remaining funds from FY16 in the FY17 year to partially fund a new appointed professional to develop a local government internship program within the Office of Sustainability.  This appointed professional position will not be created, since the plan to do so depended upon receiving half of the position\'s salary from a grant proposal to the Haury seed grant program.  The Haury seed grant was not approved, so the Office of Sustainability has had to adjust its plans.

Instead of hiring an appointed professional, the Office of Sustainability has proceeded to hire a 10 hour per week graduate assistant to help develop the local government internship program. The Office of Sustainability would like to use the funds previously approved in the last PAR for the appointed professional, to instead be used to partially support this graduate assistant.  The GA is doing the same substantive work as the appointed professional would have, but a GA is in a different budget line than appointed professionals, requiring a PAR approval.',
'advice' => 'The UA Green Fund Committee approved this PAR to move staff salary/ERE to graduate student salary/ERE/tuition reimbursement.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 10.10.16.',
'vp_approved' => 1,
'created_at' => '2016-09-26 23:30:03.000000',
'timestamp_review' => '2016-10-25 16:04:31.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-10-25 16:04:47.000000',
'netid_vp' => 'rudnick1',
'project_id' => 467,
),
101 => 
array (
'id_old' => 2999104,
'netid' => 'bchampion',
'changes' => 'The Office of Sustainability would like to shift half of the approved funding for student wages into the graduate assistants category - $7,250.  And likewise with the ERE for student employees to ERE for grad assistants - $150.  

This is being requested to support the graduate assistant hired by the Office of Sustainability to develop the local government internship program.  This program will be a new centerpiece of Office of Sustainability 100% Engagement activities, helping to formalize many important relationships with local community partners, and helping to build capacity for transformative change in local government decision-making through student involvement and leadership.  

The Office of Sustainability is prioritizing development of this program this year, more so than hiring additional undergraduates to perform lower level networking with community organizations.  The new internship program will instead develop and support a network of interns this year with local government agencies and their community partners.  Based on this core work, the Office will then hire additional student support staff in spring semester, fitting with the original scope of this second year of funding for this initiative.  But these students will be able to build on the development of the local government internship program, strengthening the impact of their work.

The reason this funding is being requested is that the Office of Sustainability was unsuccessful in receiving a grant it proposed to the Haury seed fund program for a staff coordinator position for the local government internship program.  Supporting a graduate assistant instead was the fall-back plan that was developed.',
'advice' => 'The UA Green Fund Committee approved this PAR requesting $7,400 be moved from undergraduate salary/ere to graduate salary/ERE/tuition reimbursement.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the UA Green Fund Committees recommendations on 10.10.16.',
'vp_approved' => 1,
'created_at' => '2016-09-26 23:45:02.000000',
'timestamp_review' => '2016-10-25 16:10:01.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-10-25 16:10:15.000000',
'netid_vp' => 'rudnick1',
'project_id' => 484,
),
102 => 
array (
'id_old' => 2999105,
'netid' => 'cuelloj',
'changes' => 'Dear Green Fund Committee,

This is a request for a minor reallocation of funds within the approved budget.

We realize now the critical need for the Classified Temporary Staff to train and monitor (including for safety) the students for (1) the construction/building of the hydroponic growth shelves together with their irrigation/plumbing system and and electrical system for lighting, (2) installation of the HVAC system and solar panel, and (3) operation and management of the crop hydroponic system. All these require the careful supervision and monitoring by the Classified Temporary Staff who has the technical expertise and experience that the students lack.

In the approved budget, the Classified Temporary Staff is allocated a budget of $4,300 plus 13.7% ERE. This amount, however, will last for only 2.5 months for working for 4 hours/day for 6 days a week.

I request that the amounts in the Operations Budget corresponding to the cost of $2,500 for the second shipping container (which is now not needed) and the cost of $2,000 for the Food Bank Installation (which is now not needed since Green Fund approved the installation of only one shipping container on the UA Campus) be reallocated into the salary of the Temporary Classified Staff. The requested reallocation total of $4,500 (or $3,958 + 13.7% ERE of $542)added to the approved salary amount of $4,300 for the Temporary Classified Staff -- resulting in a new total of $8,258 (plus the additional ERE of $542) -- will now allow the Temporary Classified Staff to work for 4.8 months (working 4 hours/day for 6 days a week). This will allow the proper training of the students and ensure the safety of the students.

Many thanks for your consideration, and I hope that you will approve my necessary request.


Sincerely,

Joel L. Cuello, Ph.D.
Professor of Biosystems Engineering',
'advice' => 'The Green Fund Committee approved this PAR on 11.02.16 to move $4,500 from operations to temp classified staff salary/ERE.',
'approved' => 1,
'vp_comments' => 'Melissa Vito approved this PAR on 11.07.16.',
'vp_approved' => 1,
'created_at' => '2016-10-24 23:27:34.000000',
'timestamp_review' => '2016-11-21 17:28:23.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-11-21 17:29:59.000000',
'netid_vp' => 'rudnick1',
'project_id' => 507,
),
103 => 
array (
'id_old' => 2999106,
'netid' => 'spater',
'changes' => 'We find it necessary to make an additional program alteration request (PAR) for the “UA Sierra Vista: Sustainable Landscape Demonstration Garden & Learning Center” project – Board Assigned ID: GF 16.05. The major component of this project is the construction of the outdoor classroom structure. We have received formal approval of a facility use agreement between the UA and the UA South Foundation. This agreement was required before any part of the construction process could begin. Since that time we have installed a 20,000 gallon water harvesting tank and have planted most of the native plants. We have had difficulty finding students to assist with planning and implementation, but we are continuing to recruit. We are currently working with an architect to be able to request bids for the construction of the outdoor classroom, but it will not be possible to complete the project as intended. We request to have the completion deadline extended from December 15, 2016 to June 15, 2017.

Thank you for your consideration of our request.',
'advice' => 'This PAR was approved by the Green Fund committee during the open meeting on 11.30.16.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the Green Fund Committee recommendations to approve this PAR via email on 12.05.16.',
'vp_approved' => 1,
'created_at' => '2016-11-21 03:47:01.000000',
'timestamp_review' => '2016-12-09 20:48:45.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2016-12-09 20:50:09.000000',
'netid_vp' => 'rudnick1',
'project_id' => 455,
),
104 => 
array (
'id_old' => 2999107,
'netid' => 'bchampion',
'changes' => 'We estimate that this project will have approximately $12,500 remaining funds at the end of FY17.  The reasons for this remainder are the following:

•	The development of local government internships through the Office of Sustainability GA position was slower than expected, meaning that the planned paid undergraduate support positions were not yet necessary.  At this point, internships in local government have been marketed to students for spring 2017, and summer/fall 2017 internships are being developed.  However, local government agencies have been slow to hire student applicants for many reasons, mostly having to do with administrative red tape within their agencies.  This is frustrating, but understandable, and requires patience and persistence in order to break through these barriers.

•	Off-campus tours of sustainability sites in southern AZ and beyond were largely unaccomplished.  The student coordinator hired in early fall 2016 got off to a slow start, and eventually quit his position just before the first planned tour to the Solar Zone at UA Techpark.  This sometimes happens with student employment, but is disappointing all the same.  The Office of Sustainability, due to other demands on time and organizing capacity, was unable to seek a new student coordinator in time to organize spring 2017 off-campus tours.

•	Partnership with UA Leadership Programs to sponsor a case competition at their annual conference was a lower priority task, and fell to the wayside as other more pressing challenges emerged in late fall 2016.



This PAR requests an overall extension of all remaining funds from this FY17 grant (approx. $12,500, but may end up being a bit more, depending on several minor program variables).  We would like to use these funds in FY18 for the following purposes:

•	We would like to combine $10,650 of the $12,500 with the FY18 funding Green Fund has already allocated for a .25FTE GA to support the local government internship program.  With the extra $10,650, we will be able to hire a full .5 FTE GA, instead of only a .25 FTE position.  This means 20 hours/week, instead of only 10 hours/week.  This extra 10 hours/week will be used for the following purposes:

o	Increase the scope of the internships program to some local non-profit organizations, in addition to local government.  The program has actually been labeled as a “Sustainable Cities” internship program, rather than focusing the label solely on local government.  So, it would not be difficult to extend the offer of support for recruitment of students to non-profits, as well as involving the interns involved in local non-profits to join the community of student interns working in local government, especially the networking, lecture series, and workshop discussions we will be hosting.
o	Collaborate with the Biosphere2 Ecodorm project, also funded for FY18 by Green Fund.  We hope to help make sure the Green Funds up-front investment in the planning workshop is not wasted, by supporting continued organizing after the workshop to support next steps toward implementation.
o	Organize off-campus tours similar to those intended for FY17.  Since the GA will be paid more and responsible for a wide range of other responsibilities, we hope the person hired will be more reliable than last year’s experience, and will have enough time to organize a few of these off-campus tours.

•	The remaining funds beyond those used for the extra GA time commitment (estimated ~$2,000) would be kept as operational budget, for use in costs associated with the off-campus tours, and for reserving meeting rooms or other similar expenses for facilitating discussions and activities for bonding and community-building among the sustainable cities/local government interns.',
'advice' => 'The Green Fund Committee approved this PAR on 04.03.17. The new project end date has been moved to 06.30.18 and remaining undergraduate salary will be moved to graduate salary/ERE. All operations except $2,000 will be moved to graduate salary/ERE. This new spreadsheet will be loaded at the end of FY17 which will allow time for final expenditures to be accounted for.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 04.06.17.',
'vp_approved' => 1,
'created_at' => '2017-03-17 21:54:04.000000',
'timestamp_review' => '2017-04-11 21:50:53.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-04-14 19:44:34.000000',
'netid_vp' => 'rudnick1',
'project_id' => 484,
),
105 => 
array (
'id_old' => 2999108,
'netid' => 'bchampion',
'changes' => 'None of the funds for this project have yet been spent.  The project was intended to provide program funding for the originally intended energy coordinator in FM, which has evolved into the Energy Manager position.  The new Energy Manager (Michael Hoffman) started on January 31, and he is off to a promising start in on-boarding and becoming knowledgeable about UA’s energy infrastructure.  He has networked with the SFS Energy and Climate committee, and is speaking with a network of students interested in collaborating to analyze UA energy metering data, benchmark UA buildings to determine which are the largest energy consumers, and to audit these facilities to determine specific needs for upgrades, and to develop informed proposals to administration to fund these upgrades based on solid data and analysis.  

There is a lot of opportunity in all of this for students, and now that the plan is taking shape, we would like to propose an extension of the entirety of this project’s grant funding through FY18.  We would like to use the funding for the following specific purposes:
1)	Hire 3 summer interns for 30 hrs/week throughout the summer ($10,000), to help with energy meter data analysis and building benchmarking, as well as some building energy auditing activities.
2)	Hire 3 students for 10 hr/week positions throughout the 2017-18 academic year ($5,000).  One would be an energy data and/or building auditor assistant.  The other would focus on communications/outreach on energy conservation activities with the campus community.  This would foster general awareness among the campus community, and also support student groups interested in collaborating with the work being done.


This entails shifting $10,000 from the operations section of the original grant into student wages/ERE.  The other categories of funding would remain the same, as some travel and some equipment purchases to support energy auditing and/or communications may still be valuable.  The student summer interns would start in the final months of FY17, but continue into FY18, and the students for the following academic year would work throughout FY18.  So, we request that all project funds be given an extension through the end of FY18. 

The attached spreadsheet provides an estimation of the requested new allocation of funds and timeline of use in terms of fiscal years.',
'advice' => 'This PAR submitted in error.',
'approved' => 0,
'vp_comments' => 'This PAR submitted in error.',
'vp_approved' => 0,
'created_at' => '2017-03-17 22:14:30.000000',
'timestamp_review' => '2017-03-20 19:18:48.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-03-20 19:19:22.000000',
'netid_vp' => 'rudnick1',
'project_id' => 485,
),
106 => 
array (
'id_old' => 2999109,
'netid' => 'rudnick1',
'changes' => 'None of the funds for this project have yet been spent. The project was intended to provide program funding for the originally intended energy coordinator in FM, which has evolved into the Energy Manager position. The new Energy Manager (Michael Hoffman) started on January 31, and he is off to a promising start in on-boarding and becoming knowledgeable about UA’s energy infrastructure. He has networked with the SFS Energy and Climate committee, and is speaking with a network of students interested in collaborating to analyze UA energy metering data, benchmark UA buildings to determine which are the largest energy consumers, and to audit these facilities to determine specific needs for upgrades, and to develop informed proposals to administration to fund these upgrades based on solid data and analysis. 

There is a lot of opportunity in all of this for students, and now that the plan is taking shape, we would like to propose an extension of the entirety of this project’s grant funding through FY18. We would like to use the funding for the following specific purposes:
1) Hire 3 summer interns for 30 hrs/week throughout the summer ($10,000), to help with energy meter data analysis and building benchmarking, as well as some building energy auditing activities.
2) Hire 3 students for 10 hr/week positions throughout the 2017-18 academic year ($5,000). One would be an energy data and/or building auditor assistant. The other would focus on communications/outreach on energy conservation activities with the campus community. This would foster general awareness among the campus community, and also support student groups interested in collaborating with the work being done.


This entails shifting $10,000 from the operations section of the original grant into student wages/ERE. The other categories of funding would remain the same, as some travel and some equipment purchases to support energy auditing and/or communications may still be valuable. The student summer interns would start in the final months of FY17, but continue into FY18, and the students for the following academic year would work throughout FY18. So, we request that all project funds be given an extension through the end of FY18. 

The attached spreadsheet provides an estimation of the requested new allocation of funds and timeline of use in terms of fiscal years.',
'advice' => 'The Green Fund Committee approved this PAR on 04.03.17. The new project end date has been moved to 06.30.18 and $10,000 will be moved from operations to student salary/ERE. This new spreadsheet will be loaded at the end of FY17 which will allow time for final expenditures to be accounted for.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 04.06.17.',
'vp_approved' => 1,
'created_at' => '2017-03-20 19:17:56.000000',
'timestamp_review' => '2017-04-11 21:54:04.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-04-14 19:44:52.000000',
'netid_vp' => 'rudnick1',
'project_id' => 485,
),
107 => 
array (
'id_old' => 2999110,
'netid' => 'spater',
'changes' => 'We would like to make a program alteration request. We are amazed at how long it has taken to get to this point, but we have now received bids and will be awarding the project to a contractor to start construction of the outdoor classroom this next week. We have made numerous attempts to try to hire UA students to assist with the project, but have been unsuccessful. We have found that the majority of the students attending at UA Sierra Vista attend evening classes due to having employment during the day. We had budgeted $5,500 to hire students. We would like to request to reallocate these student funds with $3,000 being moved operations to purchase an Elkay #4420BF1U outdoor bottle filler fountain (see attached) that is ADA compliant and will assist in reducing environmental impact of water bottles, and with $2,500 being moved to personnel for a classified temporary position to assist with the project meeting the June 15, 2017 completion date. Thank you for your consideration of our request.',
'advice' => 'The UA Green Fund Committee approved this PAR on 04.03.17 to move $3,000 from student salary to operations and $2,500 from student salary to classified staff/ERE.',
'approved' => 1,
'vp_comments' => 'Dr. Melissa Vito approved this PAR on 04.06.17.',
'vp_approved' => 1,
'created_at' => '2017-03-24 22:21:36.000000',
'timestamp_review' => '2017-04-14 23:19:54.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-04-14 23:21:01.000000',
'netid_vp' => 'rudnick1',
'project_id' => 455,
),
108 => 
array (
'id_old' => 2999111,
'netid' => 'rudnick1',
'changes' => 'Dear Green Fund Committee
As you know, this project received funds that would pay the difference between what the unions normally purchased and what the unions needed to purchase to incorporate in more compostable and recyclable items. This purchasing change allowed us to remove almost all Styrofoam from concession athletic events. More environmentally friendly purchasing also benefited the Green Team and SfS Greening the Game as they worked to increase recycling and composting at UA football, basketball, and softball games. 

Unfortunately, due to timing, we cannot give a specific dollar amount that will be left in the account at the end of the FY17 as the Softball season is not over yet and more purchases will have to be done this May 2017. Currently we have spent about $7,500 (this number changes weekly) of the allocated $11,600.  We do not expect to spend all funds this fiscal year and request any remaining project funds be used to purchase the same items next academic year. Due to the active nature of this project, we request a one-year extension with a new project end date of 06.30.18 with permission to continue spending funds in the same manner in which we spent this year. Funds will remaining in the operation category.

Request specifics- We ask for a new project end date of 06.30.18 and request permission to continue spending remaining funds.',
'advice' => 'The UA Green Fund approved this PAR requesting a new project end date of 06.30.18 and permission to continue spending funds as outlined in grant proposal during FY18.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations made by the UA Green Fund Committee during their open meeting on 04.24.17. Recommendations were approved on 05.11.17 via email.',
'vp_approved' => 1,
'created_at' => '2017-04-04 20:55:15.000000',
'timestamp_review' => '2017-05-16 18:28:19.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-05-16 18:47:07.000000',
'netid_vp' => 'rudnick1',
'project_id' => 503,
),
109 => 
array (
'id_old' => 2999112,
'netid' => 'rudnick1',
'changes' => 'Dear Green Fund Committee,
The E-Waste project was approved for funding but experienced a few roadblocks this academic year. The University of Arizona Bookstores was slated to start this project in the fall; however, the Bookstore went through a major building remodel that pushed the start of the project back to the spring semester. Per the Green Fund Committees recommendation, the project then attempted to expand the scope to include a reuse component that would allow UA students to collect electronic goods, perform repairs if able and resell items to the public. Upon research, it was determined that there are rules on what the UA is able to resell to the public. Essentially, the university cannot sell items in competition with local businesses. It is thought that local business, in this case the three local E-waste centers, would be at a disadvantage due to the universities size and purchasing power. This made project organizers rethink expanding the scope and they have returned to the original intent of the project, to hosting an e-waste drive next spring. 

By the time all information was assessed, too much time had passed and project managers determined that to have an effective and efficient e-waste drive, two complete semesters are needed for planning and preparation. We request another year to spend project funds and work on the project. The new project end date would be 06.30.18 and we will still spend funds in the manner in which the Green Fund Committee approved.',
'advice' => 'The UA Green Fund approved this PAR requesting a new project end date of 06.30.18 and permission to continue spending funds as outlined in grant proposal during FY18.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations made by the UA Green Fund Committee during their open meeting on 04.24.17. Recommendations were approved on 05.11.17 via email.',
'vp_approved' => 1,
'created_at' => '2017-04-04 22:41:05.000000',
'timestamp_review' => '2017-05-16 18:28:46.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-05-16 18:47:19.000000',
'netid_vp' => 'rudnick1',
'project_id' => 439,
),
110 => 
array (
'id_old' => 2999113,
'netid' => 'matthewrein',
'changes' => 'The Green Purchasing Project would like the money leftover, already allocated for this year, to be rolled over to the next year and that the project end date be extended to June 30th, 2018. You can see the exact numbers in the excel sheet, but we\'re under budget in all categories and would find it beneficial to the project for the allocated money to roll over to next year.',
'advice' => 'The UA Green Fund approved this PAR requesting a new project end date of 06.30.18 and permission to continue spending funds as outlined in grant proposal during FY18.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations made by the UA Green Fund Committee during their open meeting on 04.24.17. Recommendations were approved on 05.11.17 via email.',
'vp_approved' => 1,
'created_at' => '2017-04-10 22:07:55.000000',
'timestamp_review' => '2017-05-16 18:29:08.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-05-16 18:47:30.000000',
'netid_vp' => 'rudnick1',
'project_id' => 498,
),
111 => 
array (
'id_old' => 2999114,
'netid' => 'jillb1',
'changes' => 'PAR request GF 17.23
The Footprint to Pawprint Project funded for FY17 faced some challenges this academic year. There has been massive turnover in the project.  The First Off-Campus Housing Sustainability Coordinator (OCHSC) quit in October because he was overcommitted.  It took some time to replace him and get the project back up and running.  The two replacements made progress developing relationships with the complex managers. They began a reusable water bottle project in multiple complexes, connected one apartment to TEP for free efficiency upgrades, and tabled at the Off-Campus Housing fairs. Then, the two replacement workers had to suddenly leave the project in March, leaving no time for rehiring this semester. Therefore, some of the planned events and goals had to be altered, postponed or removed. One of the Residence Life Waste Auditors has stepped in to help wrap up the project, but she will be graduating.  She is in the process of interviewing new student workers to join the team, who will plan events and achieve the goals of this project starting the 2017-2018 academic year.
Our goal for FY18 is to reinstate our original events and goals with some expansions and improvements. With the lack of student workers and some removed events, we came in $2,023 below budget. We are requesting that the remaining FY17 balance to be rolled over to FY18, budgeted in Operating Supplies, to fund an expansion of the Dodge the Dumpster Waste Diversion program. We are excited for this new extension, especially since the sales generated with this project will allow us to gain some funding for the next academic year.  We would like access to our funding until June 30th, 2018.',
'advice' => 'The UA Green Fund approved this PAR requesting a new project end date of 06.30.18 and gave permission to move remaining student salary/ERE of approximately $870 to operations as well as move remaining travel funding of approximately $396 to operations.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations made by the UA Green Fund Committee during their open meeting on 04.24.17. Recommendations were approved on 05.11.17 via email.',
'vp_approved' => 1,
'created_at' => '2017-04-14 19:32:11.000000',
'timestamp_review' => '2017-05-16 18:31:44.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-05-16 18:47:43.000000',
'netid_vp' => 'rudnick1',
'project_id' => 497,
),
112 => 
array (
'id_old' => 2999115,
'netid' => 'lansey',
'changes' => 'As described in our January report, last fall we collaborated with DOE’s Oak Ridge National Lab (ORNL) in performing a preliminary benefit cost analysis of the solar driven pumped storage hydropower system at Biosphere 2 (B2).  Although we had several discussions with the ORNL team last spring, this degree of interest and participation was not anticipated when we submitted our proposal.  The UA student involved in this project, Chris Horstman, and I worked with them on data and analysis including GIS studies and conducting site visits.  In the fall semester, Chris was financially supported through a fellowship and did not draw assistantship support on this project.  In addition to the reconnaissance level evaluation with DOE, the UA team prepared a project implementation proposal.  We recognized that we were very early in the evaluation process but attempted to take advantage of an open DOE funding opportunity.  Not unexpectedly, we were not selected primarily for not having a fully detailed plan.

Although the DOE/UA study results were promising, the analysis had several shortcoming that were described in our earlier report. The primary weakness is that benefit computations were based on the historic energy use pattern; rather than taking advantage of the energy storage potential of the B2 eutectic salt bed that has been in place for 20 years.  The result was that the infrastructure systems, particularly, the tank size, were sized larger than likely necessary.  Utilizing the salt storage should reduce the size of the new infrastructure and improve the net project benefits.  The second weakness of the reconnaissance level DOE study was not considering the regional due to increased real estate values in Oracle and Mammoth.  

While we are not prepared to consider real estate impacts, this semester with B2 staff support we are developing a detailed system dynamics model of the B2 energy system.  The modeling emphasizes B2 heating and cooling that is the largest component of the energy consumption.  With that model, we will optimize B2 operations for a range of conditions as originally proposed.  

This effort will likely not be completed this term.  We foresee the system dynamics model to be at or near completion by June 30 but the optimization step will likely not finished.  The DOE work was an outstanding unanticipated low-cost benefit and gave us substantial insights into the site and potential alternatives.  However, it delayed our proposed modeling work.  Delays, however, had other benefits.  Research conducted independently of this project by other students advised by Lansey progressed and provides improved cost estimates for the pipeline and pump system.  

Since we did not use the allocated RA support in the fall semester, we have funding to continue this effort through most of the fall semester to complete our initial scope of work.  As such, we request that our support be extended through 12/31/17.  We are available for questions at the review committee’s convenience.',
'advice' => 'The UA Green Fund approved this PAR requesting a new project end date of 12.31.17 and permission to continue spending funds as outlined in grant proposal until the new project end date of 12.31.17.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations made by the UA Green Fund Committee during their open meeting on 04.24.17. Recommendations were approved on 05.11.17 via email.',
'vp_approved' => 1,
'created_at' => '2017-04-14 23:50:15.000000',
'timestamp_review' => '2017-05-16 18:32:59.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-05-16 18:48:06.000000',
'netid_vp' => 'rudnick1',
'project_id' => 496,
),
113 => 
array (
'id_old' => 2999116,
'netid' => 'agerardi',
'changes' => 'Compost Cats expenditures as reported by UAccess indicates that Compost Cats has $2,500 left in the travel budget section. We would like to request that these funds be transferred into student wages and extended into the next fiscal year. High contamination levels, increased demand for our bagged compost, and organizational transitions within Compost Cats mean that students are working extra hours that were not originally budgeted for. The extension of these funds into the next fiscal year will aid in the development and future success of the program, and help continue the valuable student leadership training that Compost Cats provides. 

We would also like to request an extension into next fiscal year for the $15,000 allocated for the purchase of a new truck. The process of finding a delivery truck has taken much longer than expected. Our equipment team continues to search extensively, but with recent occurrences and transitions, we may need more time beyond the end of this fiscal year. The equipment team will be able to devote more time to the search process over the summer.',
'advice' => 'The UA Green Fund approved this PAR requesting a new project end date of 06.30.18 and permission to move remaining travel funds of approximately $2,500 to student salary/ERE. The project will continue spending funds as outlined in grant proposal during FY18.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations made by the UA Green Fund Committee during their open meeting on 04.24.17. Recommendations were approved on 05.11.17 via email.',
'vp_approved' => 1,
'created_at' => '2017-04-14 23:51:41.000000',
'timestamp_review' => '2017-05-16 18:35:12.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-05-16 18:48:17.000000',
'netid_vp' => 'rudnick1',
'project_id' => 491,
),
114 => 
array (
'id_old' => 2999117,
'netid' => 'cuelloj',
'changes' => 'Since the effective start of our project was delayed by material procurement issues as well as by the UA Facilities Management officially permitting us to set up our Arizona Green Box shipping container on campus (beside Shantz Building) beginning on February 2017, we will not be able to finish our project by 30 June 2017. Thus, we intend to continue working on our project beyond 30 June 2017 through the summer, and we also need to have access to project funds after 30 June 2017.

Many thanks.',
'advice' => 'The Green Fund Committee approved this PAR during the Open Meeting held on 04.24.17. The new project end date will be 06.30.18.',
'approved' => 1,
'vp_comments' => '',
'vp_approved' => 0,
'created_at' => '2017-04-15 01:51:15.000000',
'timestamp_review' => '2017-08-06 21:07:48.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-05-16 18:48:31.000000',
'netid_vp' => 'rudnick1',
'project_id' => 507,
),
115 => 
array (
'id_old' => 2999118,
'netid' => 'hroth1',
'changes' => 'Green Fund,

We, Greening the Game, are writing this PAR to shift around remaining funds within our 2016-2017 annual grant GF 17.02 budget. Below our requests broken down.
-	Move $2,500 from this year’s Personal Payroll into this year’s Travel Expenses
o	This money will be used to fund 3 students to go to the PAC-12 Green Sport Alliance Conference this summer, where they will learn more about enhancing sustainability efforts within collegiate athletics departments across the nation and engage in sharing and learning about best practices to transform college sports into a platform for environmental progress.
o	These funds will cover airfare to Sacramento, CA, one hotel room for 4 nights, and a small stipend for food expenses. 
-	Keep $800 in this year’s Personal Payroll
o	This money will be used to pay our student workers for the rest of the year’s softball games.
-	Move $1,000 from this year’s Personal Payroll into next year’s annual grant Operations
o	This money will be used to purchase more during game supplies. 
-	Move $200 from this year’s Personal Payroll into this year’s Operations 
o	This money will be used to purchase new tabling materials for education.
-	Remaining funds from this year’s Personal Payroll, ~$1,180, please move into Personal Payroll for next year.

If you as a board are unable to approve any one of these requests, we ask that you please consider moving the requested sum into our FY 2018 Personal Payroll, which will enable us to hire more student employees and engage with more student clubs and programs outside of our own.',
'advice' => 'The UA Green Fund approved this PAR requesting a new project end date of 06.30.18 and permission to move $2,500 student salary/ERE to travel as well as move $2,500 from student salary/ERE to operations.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations made by the UA Green Fund Committee during their open meeting on 04.24.17. Recommendations were approved on 05.11.17 via email.',
'vp_approved' => 1,
'created_at' => '2017-04-15 06:41:23.000000',
'timestamp_review' => '2017-05-16 18:43:28.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-05-16 18:48:43.000000',
'netid_vp' => 'rudnick1',
'project_id' => 481,
),
116 => 
array (
'id_old' => 2999119,
'netid' => 'dtruong96',
'changes' => 'The HydroCats Committee from ASUA Students for Sustainability would like to submit a Program Alteration Request (PAR) in order to retain the funds that were not used yet in G.F. 17.12 in order to see the project to completion in Fall 2017.

In the fall of 2016, The HydroCats Committee, in association with the College of Science, was able to order the installation of an Elkay Water Bottle Refilling Station on the 2nd floor of the Physics and Atmospheric Sciences building in order to provide the convenience of refilling water bottles while at the same time promoting the sustainable reuse of water bottles. The project was awarded $3,500 and the installation came to a total of $3,049, leaving $451 remaining in the account. The last step in the process is to purchase plaques to attach to this station, and other installations funded by the Green Fund, to promote the use of reusable water bottles and advertise the partnership between Students for Sustainability and the UA Green Fund that allowed for the unit to be placed there. Again, we would like to retain the $451 remaining in the grant for use in FY 2018. Thank you for your consideration.',
'advice' => 'The UA Green Fund approved this PAR requesting a new project end date of 06.30.18 and permission to continue spending funds as outlined in grant proposal during FY18.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations made by the UA Green Fund Committee during their open meeting on 04.24.17. Recommendations were approved on 05.11.17 via email.',
'vp_approved' => 1,
'created_at' => '2017-04-16 03:39:17.000000',
'timestamp_review' => '2017-05-16 18:44:32.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-05-16 18:48:53.000000',
'netid_vp' => 'rudnick1',
'project_id' => 494,
),
117 => 
array (
'id_old' => 2999120,
'netid' => 'pkramer',
'changes' => 'The UA Community Garden is requesting a Program Alteration Request to transfer the funds currently allocated to student pay hourly wages, including ERE, in FY18 to the category of student support. In FY17, student employees submitted hourly wage requests in excess of the allocated amount, which caused the account to be overdrawn. Transitioning the funds to student support, which would be distributed in four equal installments to the garden manager position, would ensure that a similar problem does not occur again. The installments would be paid out by the end of May 2018.',
'advice' => 'The UA Green Fund approved this PAR requesting a transfer $2,715 from Salaries/ERE to Operations',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved the recommendations made by the UA Green Fund Committee during their open meeting on 11.03.17. Recommendations were approved on 11.14.17 via email.',
'vp_approved' => 1,
'created_at' => '2017-10-18 02:07:50.000000',
'timestamp_review' => '2017-11-15 20:13:18.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2017-11-15 20:15:59.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 532,
),
118 => 
array (
'id_old' => 2999121,
'netid' => 'kulbeth',
'changes' => 'The Community Outreach in Environmental Learning grant we have received focuses on graduate and undergraduate student worker wages.  Within the proposed purposes of our grant, we have identified the need to allocate some funds to the costs of the sustainability-focused events that our team is putting together on the main UA campus and at the Cooper Center.  Additionally, the Cooper Center project team has identified 90 hours of unused student worker wages from earlier this semester that can be easily re-distributed with no impact on the number of hours spent on the project from this point forward. These hours and the associated funds are available due to slower than expected hiring processes and coordinating working schedules with our new student employees. We propose to reallocate the $900 assigned for this purpose to several events that our team has planned.

About $200 will be immediately needed in November, December, and January to purchase supplies and food for 2 sustainable cooking events, one that we are coordinating with student leaders of all the Greek Life houses, and another smaller one for an entire Greek Life house.  We plan to allocate the remaining $700 toward planned events that include involvement with Environmental Awareness Society, leaders from UA South, and the Community Outreach and Engagement student group.

Please let us know if you have any questions or concerns regarding this request. We look forward to hearing from you.  Thank you for your time and consideration!',
'advice' => 'The UA Green Fund recommended this PAR for approval during the open meeting on 12.01.17. The committee recommends moving $900 from student personnel to operations.',
'approved' => 1,
'vp_comments' => 'Dr. Vito approved this PAR on 12.04.17.',
'vp_approved' => 1,
'created_at' => '2017-11-06 20:30:48.000000',
'timestamp_review' => '2017-12-07 19:05:26.000000',
'netid_review' => 'rudnick1',
'timestamp_vp' => '2017-12-07 19:07:33.000000',
'netid_vp' => 'rudnick1',
'project_id' => 520,
),
119 => 
array (
'id_old' => 2999122,
'netid' => 'mgoode',
'changes' => 'Dear Green Fund Committee:

Regarding the Green Fund project, "Sustaining Lizard Population on the University of Arizona Campus," I received an email from Julia Rudnick indicating that funds used to support student wages had been expended. Julia asked me to clarify what happened. When I calculated how many hours students could work on the project, I made a mistake by thinking that there was $15,000 available in year one. Therefore, I had the students work more hours than they should have. The good news is, all the students are very excited about the project and have agreed to continue working. They have all signed up for guided research credit, which is actually something they wanted to do initially, but they are not allowed to get course credit and be paid at the same time. In the summer of 2018, all student participants will either sign up for credit or volunteer on the project. At least one of the students decided to use part of the project as their honors thesis, and one other is considering to do the same. Therefore, they would not be paid anyway as honors students working on their theses. All this is to say that the project outcomes will not be affected in any way and we are on course to achieve the objectives of the grant.

Julia also wondered why we hadn\'t spent any of the equipment and supplies money. The reason is that I already had some transmitters that we could use to radio track the lizards, so rather than let them go to waste, I used them on the Green Fund project to save money. Last week, I did purchase more refurbished transmitters (approximately $4,000), so that will be reflected in the budget moving forward.

All in all, the project is going great and we anticipate no changes to the scope of work. If I need to make changes to the budget that reflect expenditures more accurately, please let me know and I will do so immediately.

Thank you for your consideration,

Matt Goode, PhD
School of Natural Resources & Environment',
'advice' => 'The Committee approves this PAR.',
'approved' => 1,
'vp_comments' => 'This PAR was approved via email by Melissa Vito March 23rd, 2018.',
'vp_approved' => 1,
'created_at' => '2018-01-30 23:08:49.000000',
'timestamp_review' => '2018-06-11 18:36:52.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-11 18:39:05.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 523,
),
120 => 
array (
'id_old' => 2999123,
'netid' => 'mslagle',
'changes' => 'The E Waste Recycling event is requesting a Program Alteration Request (PAR) to transfer the funds currently allocated to student pay hourly wages, including ERE, in FY18 to the category of student support. This will allow our employees to receive a stipend (instead of hourly wages). Our dedicated student employees have already begun the planning and logistics of this event. We are only allowed to log up to 25 hours within the university, and we all currently hold internal job positions that nearly or completely fill the allowed 25 hours. Many students must obtain work outside the university to supplement their income, therefore to avoid losing our employees to outside work we are requesting this funds transfer. This is a one time event and will require extensive networking, planning, and marketing, which are invaluable skills that will bolster their resumes and future job prospects. We can assure you that these employees are not stretched too thin and they are confident in the success of the event; The team has a proven track record of working together. Transitioning the funds to student support, which would be distributed in one installment after the event is completed, will allow our employees to focus on University of Arizona initiatives without seeking work from the outside. We no longer have the time to find different employees to plan this event for April. This installment will be paid out by the end of May 2018 upon completion of the E-waste event scheduled in late April 2018.',
'advice' => 'The Committee approves this PAR.',
'approved' => 1,
'vp_comments' => 'This PAR was approved via email by Melissa Vito on March 23rd, 2018.',
'vp_approved' => 1,
'created_at' => '2018-03-01 17:15:48.000000',
'timestamp_review' => '2018-06-11 18:36:06.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-11 18:39:32.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 439,
),
121 => 
array (
'id_old' => 2999124,
'netid' => 'kulbeth',
'changes' => 'The Community Outreach in Environmental Learning grant we have received focuses on graduate and undergraduate student worker wages.  Within the proposed purposes of our grant, we have identified the need to allocate some funds to the costs of the sustainability-focused events that our team is putting together on the main UA campus and at the Cooper Center.  Recently, the Cooper Center project team leaders identified 90 hours of unused student worker wages due to slower than expected hiring processes and coordinating working schedules with our new student employees.  We received approval through our previous PAR to re-allocate these funds and have since used $150 for flyer publications and art supplies needed to support upcoming collaborative events.  About $550 of those funds will also be directed toward publicizing and purchasing items needed to involve our campus with other campuses in the Simply Straws Campus Challenge, an educational endeavor that calls for student pledges to not use straws in exchange for a free glass straw!  

Recently, we have had similar experiences with our student employees, leaving us with 109 further unused student worker wages.  Consequently, we have investigated two other ideas that will help us fulfill our purpose and allocate our funds wisely.  First, after hosting one very successful event with 16 UA students hiking and doing yoga at the Cooper Center, we would like to request that $100 of our remaining funds from the previous PAR be allocated into the Operations category of our project so that we can offer payment to our yoga instructor for two additional events this year.  The remaining $100 will be used for supplies as designated.  Please note that while our yoga instructor is already an employee of the University of Arizona, she is not an employee of the Cooper Center.  So, while we needed to quickly figure out if we could re-allocate our funds through this PAR, we are also currently investigating whether her student worker hours are maxed out and if this is an obstacle to these plans.  

Secondly, we realize our team’s structure, growth, and effectiveness could be positively influenced next year if we can provide an exceptional educational and unifying training experience.  Therefore, we would like to allocate the $200 remaining from the previous PAR and the rest of our unused funding (amounting to about $15,000) toward preparation and plans for taking every team member next year to the AASHE Conference in Philadelphia, Pennsylvania in October.  This would include using about $600 per person for plane tickets (about $2400), $400 per student registration fees and additional sessions due by May 31st, 2018 (about $1,600), and of course meals and lodging for each person estimated at about $1,000 total.  There will also be the cost of a suitable presentation poster in the month of August as well as other unprecedented costs for this opportunity.  All in all, we believe this would be a reasonable use of our time and funding, and with this in store, we hope that any funds remaining after the academic year 2017-2018 ends will roll over into our efforts to prepare our team for this experience.

Please let us know if you have any questions or concerns regarding this request. We look forward to hearing from you.  Thank you for your time and consideration!',
'advice' => 'Tabled for next open meeting, April 27th



Need an updated budget',
'approved' => 0,
'vp_comments' => 'PAR was requested to table for next open meeting and Melissa Vito approved this table request via email on April 11, 2018.',
'vp_approved' => 0,
'created_at' => '2018-03-26 17:26:01.000000',
'timestamp_review' => '2018-06-08 19:30:18.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-08 19:31:36.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 520,
),
122 => 
array (
'id_old' => 2999125,
'netid' => 'cfp',
'changes' => 'Compost Cats has not yet received any of our FY 2018 funding, but we received official word from Joel Hauff on March 19th that the transfer of our full FY 2018 Green Fund grant, in the amount of $34,500, will take place very soon.

Given that we will not have had access to this funding until April, 2018, this PAR is to request that we be given an additional fiscal year, until June 30, 2019, to spend the allocated funding. We would keep the expenses in the same categories as they are in the budget you have, with $32,000 allocated to student staff payroll and ERE, and the remaining $2500 for conference travel. Please let us know if any further information is required to process this PAR.',
'advice' => 'Shifting money from last year to next year is fine

Hopefully, they are moving in the right direction.',
'approved' => 1,
'vp_comments' => 'PAR was approved by Melissa Vito via email on April 11th, 2018.',
'vp_approved' => 1,
'created_at' => '2018-03-26 20:26:36.000000',
'timestamp_review' => '2018-06-08 19:32:39.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-08 19:33:12.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 512,
),
123 => 
array (
'id_old' => 2999126,
'netid' => 'beauregard',
'changes' => 'The Wildcat Green Reach Outreach project would like to move $2850 of the student employee budget to the GA assistant category. This project is run by one undergraduate student and one grad student and this change would reflect that reality. Attached is the revised budget for FY18.',
'advice' => 'This is appropriate, as programs shift, people should shift with them.

This will hopefully make a great program even stronger.',
'approved' => 1,
'vp_comments' => 'PAR was approved by Melissa Vito via email on April 11, 2018.',
'vp_approved' => 1,
'created_at' => '2018-03-27 18:16:48.000000',
'timestamp_review' => '2018-06-08 19:33:47.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-08 19:34:18.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 527,
),
124 => 
array (
'id_old' => 2999127,
'netid' => 'mgoode',
'changes' => 'Dear Green Fund Committee:

In my original PAR for the project, "Sustaining Lizard Population on the University of Arizona Campus," I provided an explanation for the expenditure of funds to date, including the use of funds to support student wages. I received an email from Lauren White indicating that more specific information and an updated budget were required for the committee review. Therefore, I have attached a budget that includes actual expenditures for 2017 and projected expenditures for 2018.

In the original budget, I requested $11,00 for wages in 2017, which included $4,000 for faculty and $7,500 for students. Actual personnel expenses for 2017 totaled $14,270, all of which went to student wages. Therefore, I allocated faculty wages and ERE totaling $5,400 to help cover student wages. In addition, I spent less than projected for operations, which freed up funds to cover the remaining costs for student wages.

I plan to use remaining 2017 funds to purchase one receiver/antenna ($1000), $450 to cover costs associated with attending a conference in May where students will present another poster, and $237 for miscellaneous supplies (e.g., printing costs, lizard noose poles, flagging, etc.). In this manner, costs for 2017 will match what I originally proposed for 2017.

Moving forward, I am proposing to move unexpended wages and ERE for faculty from 2017 to 2018. In addition, I plan to purchase equipment and pay for costs associated with attending conferences, as well as unforeseen miscellaneous expenses.

All of the above is reflected in the attached budget. If you have any further questions, please do not hesitate to contact me at your convenience.

Sincerely,

Matt Goode, PhD
School of Natural Resources and Environment',
'advice' => 'The Committee approves this PAR.',
'approved' => 1,
'vp_comments' => 'This PAR was approved via by Melissa Vito',
'vp_approved' => 1,
'created_at' => '2018-04-01 14:24:30.000000',
'timestamp_review' => '2018-06-11 18:41:07.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-11 18:41:52.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 523,
),
125 => 
array (
'id_old' => 2999128,
'netid' => 'colmenares',
'changes' => 'Greening the Game would like to PAR remaining operations budget of $1287 to payroll and $60 to ERE. These funds will cover the remaining payroll needed to staff the softball games at 4 people per game plus any additional administrative work needed to be completed by the Committee Chairs for Greening the Game.',
'advice' => 'The Committee approves this PAR.',
'approved' => 1,
'vp_comments' => 'This PAR was approved by Melissa Vito',
'vp_approved' => 1,
'created_at' => '2018-04-09 19:31:34.000000',
'timestamp_review' => '2018-06-11 18:41:28.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-11 18:42:20.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 533,
),
126 => 
array (
'id_old' => 2999129,
'netid' => 'cuelloj',
'changes' => 'Dear Green Fund Committee,

Since our Cats in the Green Box project\'s Arizona Green Box has been receiving significant attention, having been mentioned in a number of news articles, and has been an effective vehicle for student outreach on sustainability applied to food security, I would like to request for continuation of our project (i.e., through a no-cost extension) until 30 June 2019. We need access to our project funds, however, after 30 June 2018.

We plan to continue working on our UA-patented innovation -- the V-Hive Green Box -- that resulted from this Green Fund project, and continue our student outreach programs.

Also, a planned collaboration with the government of Qatar on having their Green Boxes (to be built through our cooperation) connected online with the Arizona Green Box for our respective students to communicate online and learn from one another is currently being pursued for implementation in the requested no-cost-extension period.

Many thanks for your consideration, and I hope that you will approve this request. Many thanks.


Sincerely,

Joel L. Cuello, Ph.D.
Professor of Biosystems Engineering',
'advice' => 'The Committee found the no-cost extension of the project was a good idea to approve since Cats in the Green Box is getting a lot of attention for the work that they are doing and there would be no-cost to the Green Fund Committee.',
'approved' => 1,
'vp_comments' => 'PAR was approved by Melissa Vito via email on May 3rd, 2018.',
'vp_approved' => 1,
'created_at' => '2018-04-16 21:36:01.000000',
'timestamp_review' => '2018-06-08 19:16:58.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-08 19:17:43.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 507,
),
127 => 
array (
'id_old' => 2999130,
'netid' => 'kulbeth',
'changes' => 'As a correction to our earlier PAR, our total unused funding amount will be $7,100 rather than $15,000, due to a miscalculation in GA tuition benefits.  A revised budget (attached here and provided via email) and itemized AASHE cost breakdown (provided via email) reflecting this new amount have been submitted for your review.  Additionally, some changes have occurred so that we do not need to provide payment to our yoga instructor for the remainder of this year, which means the remaining $200 from the previous PAR will be used to support other events in terms of supplies and rentals instead of operations as originally intended with the previous PAR of February 2018.

As you consider these changes, please know that while we hope approval is obtained for using our remaining funds for AASHE travel money, being able to retain this funding for next year could help support a bevy of activities this fall.  We have organized the potential for hosting an Energy Efficiency series of learning experiences for Greek Life, an Art in the Desert series through the UA Museum of Art, and hope to be able to facilitate a series of Sunset Yoga events at Cooper as well.  While these are intended to be low cost events, we hope you’ll allow the $7,100 to roll over and support next year’s supply and event funding.  Thank you!',
'advice' => 'The Committee found that the request to move the remaining money from student work wages to travel and operations is against the Green Fund rules and laws and cannot fund money for travel. Rather, the Committee believes that the project should allocate remaining money to fund events and projects in order to benefit the Tucson community. They would like the money to stay in the employee category, and for the project coordinators to plan events and resubmit a PAR in the fall semester for additional travel funding. 

The Committee then decided to extend funding for the next fiscal year so that the project coordinators would have time to plan events. The Committee approved this decision.',
'approved' => 0,
'vp_comments' => 'PAR was declined by Melissa Vito via email on May 3rd, 2018.',
'vp_approved' => 0,
'created_at' => '2018-04-18 16:40:06.000000',
'timestamp_review' => '2018-06-08 19:20:34.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-08 19:21:24.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 520,
),
128 => 
array (
'id_old' => 2999131,
'netid' => 'kebonine',
'changes' => 'Proposed updates 18 April 2018

We would like to request a no-cost extension of one year.
We have determined that a large hurdle is the approx. $50K cost to get a large waterline to the available building and have fire sprinkler system installed. 
We need to further develop plans and drawings in order to fundraise for this critical portion. 
Many collaborators ready and willing to help with other labor.
More than a dozen solar panels were donated to the project.
Educational \'work stages\' have been identified as part of the planned building conversion.


ACTUAL ($2306.53 to date, of $7100 award)
Architecture Student 363.00 + 7.99 = $370.99 (of $1000 planned should PAR be approved)
Planning Conference Call = $21.15
Room Rentals 750.00 + 69.04 = $819.04
Workshop Supplies 45.56 + 20.00 + 59.72 + 11.66 = $136.94 (of $200 total budgeted)
Meals, Coffee, Snacks 46 + 233.50 + 28.75 + 195.92 + 18.86 + 51.81 + 383.57 = $958.41
Transportation 0 billed of $450 earmarked
Staff wages 0 billed, see proposed changes below

PROPOSED Budget CHANGES
Add room rental fee of 819.04 above.
$0 for lodging instead of $800 (we did multiple 1-day workshops instead of a single 2-day workshop)
$1K student wages instead of $2K student wages (therefore balance remaining of $629.01)
$2K classified staff wages instead of $1K supplemental comp (yet to be \'billed\')
(ERE would increase by $400 and decrease by $50 for net increase of $350)
$450 for transportation instead of $500
Roll conference call fee into workshop supplies budget of $200, but suggest increasing to $300.
Add another (the 4th overall) group meeting in fall 2018 on UA campus. This would have printing costs, lunch, snacks, coffee. (Approx. $500)
Therefore meal costs reduced from $2K to $1.5K
These changes leave $80.96 unaccounted for which we would like to put toward volunteer participant organization, Solar Guild.

See accompanying spreadsheet.',
'advice' => 'The Committee found that the no-cost extension and a no-cost re-allocation of already awarded money to be transferred into other categories for budget changes will be beneficial for the project.',
'approved' => 1,
'vp_comments' => 'PAR was approved by Melissa Vito via email on May 3rd, 2018.',
'vp_approved' => 1,
'created_at' => '2018-04-19 01:21:07.000000',
'timestamp_review' => '2018-06-08 19:22:10.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-08 19:22:47.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 510,
),
129 => 
array (
'id_old' => 2999132,
'netid' => 'grantmc',
'changes' => 'We would like to extend the student funding component of the project for up 3 months (September 2018). This will allow us to continue employment of our student Project Coordinator who will accomplish additional project marketing and data development goals for the project.

Due to a number of factors, we were unable to bring the student Project Coordinator on board until the end of 2017, so we have only spent a portion of the allocated funds. Of the $5,700 allocated, around $1,700 has been spent, and we expect the expenditure level to be approximately $3,500 by the end of June 2017. 

With a "soft launch" of the Sustainability Web Map Phase 2, anticipated in May 2018, we believe having additional services of our student employee through the extension period will help prepare the application for a full launch, targeted to coincide with the beginning of the Fall semester. This extension will allow us to better respond to feedback during the "beta testing" stage after the soft launch.',
'advice' => 'The Committee found that the request for a no-cost extension and the request to continue the employment over the summer would be beneficial for the project.',
'approved' => 1,
'vp_comments' => 'PAR was approved by Melissa Vito via email on May 3rd, 2018.',
'vp_approved' => 1,
'created_at' => '2018-04-19 18:12:54.000000',
'timestamp_review' => '2018-06-08 19:25:02.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-08 19:25:43.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 535,
),
130 => 
array (
'id_old' => 2999133,
'netid' => 'cuelloj',
'changes' => 'Dear Green Fund Committee,


This is a second PAR that I am submitting for my Cats in the Green Box Project.


This project does not have funding for graduate or supplemental compensation, and the project inadvertently spent $360 for such item. This was an honest oversight. As soon as this was brought to my attention, this was discontinued.


I thus request that funding from Operations be moved to cover for this deficit.


Many thanks for your consideration, and I hope that you will approve this request. Many thanks.


Sincerely,

Joel L. Cuello, Ph.D.
Professor of Biosystems Engineering',
'advice' => 'The Committee found that the request to move $960 out of a category to cover deficit costs showed responsibility that the project coordinators had discovered that there was an oversight in the budget and taking funds from other categories to cover the deficit would be beneficial for the project.',
'approved' => 1,
'vp_comments' => 'PAR was approved by Melissa Vito via email on May 3rd, 2018.',
'vp_approved' => 1,
'created_at' => '2018-04-19 23:47:51.000000',
'timestamp_review' => '2018-06-08 19:18:30.000000',
'netid_review' => 'lishahall2012',
'timestamp_vp' => '2018-06-08 19:19:11.000000',
'netid_vp' => 'lishahall2012',
'project_id' => 507,
),
));
        
        
    }
}