<?php

use Illuminate\Database\Seeder;

class GfSiteUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        
        \DB::table('site_user')->insert(array (
            0 => 
            array (
                'site_id' => 1,
                'user_id' => 34,
            ),
            1 => 
            array (
                'site_id' => 1,
                'user_id' => 27,
            ),
            2 => 
            array (
                'site_id' => 1,
                'user_id' => 37,
            ),
            3 => 
            array (
                'site_id' => 1,
                'user_id' => 28,
            ),
            4 => 
            array (
                'site_id' => 1,
                'user_id' => 30,
            ),
            5 => 
            array (
                'site_id' => 1,
                'user_id' => 36,
            ),
            6 => 
            array (
                'site_id' => 1,
                'user_id' => 33,
            ),
            7 => 
            array (
                'site_id' => 1,
                'user_id' => 22,
            ),
            8 => 
            array (
                'site_id' => 1,
                'user_id' => 26,
            ),
            9 => 
            array (
                'site_id' => 1,
                'user_id' => 40,
            ),
            10 => 
            array (
                'site_id' => 1,
                'user_id' => 38,
            ),
            11 => 
            array (
                'site_id' => 1,
                'user_id' => 39,
            ),
            12 => 
            array (
                'site_id' => 1,
                'user_id' => 31,
            ),
            13 => 
            array (
                'site_id' => 1,
                'user_id' => 35,
            ),
            14 => 
            array (
                'site_id' => 1,
                'user_id' => 32,
            ),
            15 => 
            array (
                'site_id' => 1,
                'user_id' => 23,
            ),
            16 => 
            array (
                'site_id' => 1,
                'user_id' => 25,
            ),
            17 => 
            array (
                'site_id' => 1,
                'user_id' => 29,
            ),
            18 => 
            array (
                'site_id' => 1,
                'user_id' => 24,
            ),
        ));
        
        
    }
}