<?php

use Illuminate\Database\Seeder;

class MgAnswersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        
        \DB::table('answers')->insert(array (
            0 => 
            array (
            'answer_text' => 'These are great questions! As this is our pilot-event, we wanted to focus this particular  project/activity on the students enrolled in the AGTM courses because the activity is tied to the course instructional materials (PV and water pumping), and to receive their feedback on the perceived value of the activity as a lab teaching tool. However, we have used a similar "pump in the bucket"-type activity with our outreach experiences with high school students, Tohono O\'odham students at an Ag Expo (May, 2104), and an American Indian Youth Conference (July, 2014), and an upcoming micro-teaching experience with Navajo students in Kayenta, AZ. 

The concept of the "Creative Component" (students developing a design from whatever materials) adds a time and planning factor--- more than a two-hour presentation. Our idea of having staff from Project WET, a water-education training organization, serving as "judges" is to determine if the activity has "value" for use by outside groups. 

If the activity is successful, we hope to open it up on a larger scale, and possibly seek sponsorship for materials and recognition.',
                'created_at' => '2014-09-26 21:34:55.000000',
                'question_id' => 840,
            ),
            1 => 
            array (
                'answer_text' => 'Throughout the duration of the Hall Leadership Camp Student Retreat, ACT and Eco-Reps will

develop stronger leadership edification skills that will contribute to efficient advocacy back at 

the UA campus. During the participation at the YMCA- Triangle Y Ranch, ACT and Eco-Reps 

will come together for multiple sessions that will provide knowledge and examples of potential 

initiatives for service projects back at the UA campus. The ACT and Eco-Reps will not facilitate 

a service project at the YMCA-Triangle Y Ranch. With the knowledge provided at camp, 

ACT and Eco-Reps will have the resources necessary for planning and accomplishing service 

projects that are projected to benefit both the UA and Tucson community. Eco-Reps even have a 

“Service and Action” committees that will be planning service projects after the Eco-Reps return 

to Tucson. The Hall Leadership Camp Student Retreat will serve as a ground for leadership 

training and collaboration within the ACT and Eco-Reps, enabling them to serve the UA campus 

with greater success.',
                'created_at' => '2014-09-29 17:10:21.000000',
                'question_id' => 841,
            ),
            2 => 
            array (
                'answer_text' => 'The goal for the Break the Bag Habit project is to reduce plastic bag usage on campus in the long term through education about the environmental issues caused by plastic bags.  If the student unions decide to continue offering a discount for reusable bags and begin selling their own reusable bags, the that will continue to reduce plastic bag usage on campus.  Student union employees will ask customers if they have a reusable bag and mention the discount but will still provide plastic bags to customers who want one.  The discount will apply to anybody with a reusable bag, whether or not it is from the project, as long as they buy three or more items.  I don\'t fully understand the question of how are you going to measure it, but the Student Union will keep track of how often somebody uses the discount.  The Student Union Dining Services has agreed to the plan.  The bags will be distributed by volunteers in the UMarts who will also provide customers with information about plastic bags and the Break the Bag Habit project.  Green fund would certainly be able to see the design on the bags prior to them being created.',
                'created_at' => '2014-10-10 14:31:54.000000',
                'question_id' => 842,
            ),
            3 => 
            array (
                'answer_text' => 'Wilson Kong            23053112       $250
Benjamin David Geller  23143027       $250
Eric John Gabriel      23138628       $250
Ryan James O\'connell   23131129       $250
Deepak Sridhar         23192385       $500',
                'created_at' => '2015-01-21 20:12:46.000000',
                'question_id' => 846,
            ),
            4 => 
            array (
                'answer_text' => 'Thank you for your question. This year we propose two themes for the TedTalks: Urban Food Justice and Food & Race. Each speaker will present on their work relating to this issues and future directions. We have not finalized speakers but for the fall talk, but we have reached out to the following speakers:
Morgan Lundy (post-bac U of A student involved heavily with School Garden Program)
Dora Martinez (representative from Flowers and Bullets and the Pima County Food Alliance)
Morgan Apicella (Geography PhD Candidate who has worked with Tierra and Libertad, a Tucson food sovereignty group)

We will soon invite a professor (not confirmed, but likely from Urban Planning or Public Health) and someone from Tucson city who involved with the Urban Agricultural Zoning ordinances which are being revised. 

For the spring, we haven\'t finalized but would like to engage Seth Holmes (a visiting speaker who work on farmer workers and food justice issues), a UA professor from Mexican-American Studies, as well as an undergraduate speaker from a related student group, and a representative from Iskashiita, a refugee network that harvests fruit from Tucson area trees. 

Please let me know if you need further clarification. 

Thanks!

Meg',
                'created_at' => '2015-09-25 18:38:06.000000',
                'question_id' => 847,
            ),
            5 => 
            array (
                'answer_text' => '1. To win the competition, both universities will complete a series of three waste audits, one at the start of the competition, one approximately half way through, and one at the completion of the competition. The first audit will serve as a baseline for each university and will include a sample of trash and recycling which will be sorted to determine the amount of incorrectly recycled materials as well as materials that could have been recycled and were instead thrown away. Percentages will be used to standardize both universities and to determine overall improvements in waste diversion over the course of the competition. The second audit will serve as a check for both universities to determine how effective changes and advertising have been and also to implement any needed changes. The final audit will serve to determine the winner by gauging which university had the largest net increase in proper recycling and reduction in recyclables thrown away. 

2. Both parties are communicating through email and phone calls to ensure fairness on both sides. Pictures will be taken on both sides to serve both as publicity and to ensure both sides are completing waste audits similarly. ASU is implementing similar changes and advertising throughout their main library, Hayden Library.',
                'created_at' => '2016-01-27 18:46:56.000000',
                'question_id' => 848,
            ),
            6 => 
            array (
                'answer_text' => 'The Environmental Arts committee is planning on hosting the community painting day for the mural on Sunday April 1st. This will be the day that the community comes and fills in the mural with a paint by numbers system. The two weekends before the event will be set up by the arts committee. The weekend of the 17th we will paint wash the surface of the cistern and lay down a priming coat of paint. The weekend of the 24th we will lay out a grid and paint the out line of the mural on the priming coat. We will then fill in each section with a number corresponding to a number of a color.',
                'created_at' => '2018-02-21 01:56:30.000000',
                'question_id' => 852,
            ),
            7 => 
            array (
                'answer_text' => 'The event portion of our budget would go down by $50 if we could borrow a speaker for free.',
                'created_at' => '2018-02-21 02:00:11.000000',
                'question_id' => 851,
            ),
            8 => 
            array (
                'answer_text' => 'Last year we marketed to sfs and sent information about the event to multiple listservs. We also had members of the water committee of sfs at the event, participating in the event, having discussions about rain water harvesting. This year we will broaden our reach of who we contact by advertising more through resident life, posting flyers, and advertising in art stores. We will also have a few activities set up under the ramada in the garden this year where hydrocats members will be leading water conservation based activities. This will lead to a more tangible learning experience, and give people an activity to participate in when it is too crowded around the cistern to paint.',
                'created_at' => '2018-02-21 02:08:32.000000',
                'question_id' => 850,
            ),
            9 => 
            array (
                'answer_text' => 'Last year\'s event was successful but I think it can be made better by drawing in more community members through further advertising of the event. We have some paint left from last year, but it has been stored outdoors. The combination of extreme summer heat and imperfect seals has caused the paint to separate and become very watery and translucent. Most of the paint that is left is outside of the color palette we plan on using for the monsoon scene. We used both foam and bristle brushes last year, both of which have become petrified with paint since there is no good way to clean out toxic paint in the community garden.',
                'created_at' => '2018-02-21 02:15:39.000000',
                'question_id' => 849,
            ),
            10 => 
            array (
            'answer_text' => 'Tucson High (TH) will be involved in the gallery by their submission of art work. The Environmental Arts committee has strong relationships with Tucson High art teachers. We will go to the school and discuss issues related to the theme of the gallery, and students make work in response to the discussion. The entire gallery is curated so they are not guaranteed a spot in the show, but in the past TH students have made some of the best work in the show. Their involvement in the gallery breaks a very large social border within the Tucson community that inhibits many local kids from interacting with higher education. This partnership breaks down the educational separation in the community to give resources to youth interested in practicing in art within an academic setting.',
                'created_at' => '2018-02-23 04:02:41.000000',
                'question_id' => 856,
            ),
        ));
        
        
    }
}