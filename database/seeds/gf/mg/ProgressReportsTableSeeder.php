<?php

use Illuminate\Database\Seeder;

class MgProgressReportsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        
        \DB::table('progress_reports')->insert(array (
            0 => 
            array (
                'netid' => 'uafrank0',
                'scope' => 'The purpose of the project is to install a ground mount for a three-module demonstration solar array to provide UA students, and the public through outreach activities, with an opportunity to gain hands-on experience with mounting PV modules, wiring modules, connecting modules to a combiner box, and demonstrating appropriate grounding technique.',
                'outcomes' => '',
                'response' => 'Funding from the Green Fund Mini Grant was used to acquire materials for mixing and pouring concrete to permanently install a ground-mount array. The ground-mount array materials were donated by the UA Campus Agricultural Center, as well as use of a skid-steer auger to excavate the 12 inch diameter holes to install the five inch diameter pipe that serves as the support for the rail. Items purchased included: PV cable to connect to the three modules in series; a pre-wired combiner box; a copper grounding rod, grounding conductor, and connector; a storage box for organizing the PV module clamps, wire leads, combiner box, and tools.

Concrete was mixed and put into place by 24 students enrolled in the AGTM 350 course. The activity took approximately 2 hours to complete. The concrete batch was mixed on site. Students were divided into "teams" and assigned tasks. Tasks included gathering and measuring concrete components, loading the mixer, mixing, pouring into the Sonotube cylinders, making sure the ground mount was level, tamping concrete, screeding, floating, and finishing the concrete. Final tasks included clean up and put away of tools and materials. 

Two days later, the square tubing cross member and z-bar rails were mounted on the installed pipe uprights. A mount for the combiner box was created so the box could be put in place on the mount and removed when not in use and stored. Two pieces of unistrut are bolted to a backing plate attached to the combiner box. The unistrut is attached and held in place using adjustable pipe clamps.

At the request of the UA Campus Agricultural Center, a gravel base was put in place under and around the mount to control weed growth. A frame of 2x4 pressure treated wood border was fabricated and put in place. A weed-barrier cloth was put down in direct contact with the soil. A 3.5 inch deep layer of 3/4 inch-sized gravel was put down on the fabric. The gravel was donated by the UA Campus Agricultural Center.

Finally, a mobile storage box was constructed of OSB board and 2x4 lumber. The box measures 82 inches long, 22 inches wide, and 42 inches high. The box has dividers and holds & stores five PV modules. A fold down door on the end allows students access and removal of the modules. The box is placed on a steel angle iron frame with casters. The container can be moved using a forklift from the lab facility and stored in our PV equipment storage container.  

Labor for installation of array components, combiner box mount,  installation of gravel base,  and construction of the mobile module storage box was by a freshman AGTM 100 student volunteer and a student worker employed by the Department of Agricultural Education.

Unfortunately, the AGTM 350 course schedule, we moved out of the PV unit and into our Small Engine unit before we could take advantage of actually having students mount and practice wiring the system. However, we are now ready to demonstrate this hands-on system for our spring AGTM course, and a SEI workshop we will be hosting in February at our classroom/laboratory facility.',
                'created_at' => '2014-12-18 18:15:47.000000',
                'project_id' => 561,
            ),
            1 => 
            array (
                'netid' => 'uafrank0',
                'scope' => 'The purpose of the activity was to create and conduct an activity where students enrolled in the AGTM 350 laboratory course would plan, create, test, and present a water fountain powered by solar modules. Twenty-four students enrolled in the course would work in pairs. Each two-student group would be provided with a DC-powered marine bilge pump, 20-watt solar modules, PV cables, five-gallon buckets, and a $25.00 budget for materials to create a fountain. Tri-fold presentation boards would be provided for each group to enhance their educational presentation. Groups were instructed in developing a Bill of Material list for the instructor for materials to be purchased from local hardware vendors. Each group was to prepare and submit project documentation on how the project was designed, tested, and use. 
Awards in the form of Wildcat & Block A trophies would be fabricated by AGTM students. The trophies would include engraved plates purchased from Sahuaro Trophy.  Fountain projects would be evaluated on creativity, innovation, use of recyclable materials, and educational presentation.',
                'outcomes' => '',
                'response' => 'Twenty-four students enrolled in the AGTM 350 course worked in pairs to develop an idea for a water fountain.Each student-group met during three different AGTM lab periods to work on their projects. Groups submitted hand-drawn sketches, and a Bill of Material list for their projects. The instructor collected each Bill of Material and ordered materials online from Home Depot and Grainger. Tri-fold presentation boards were purchased from Walmart and distributed to each group to use for their education presentation.

Trophies were fabricated from sheet metal, paint, and Block A decals purchased from Sign-A-Rama. A student volunteer and AGTM student worker provided the labor to fabricate the trophies. Engraved plates were purchased from Sahuaro Trophy and mounted on the finished trophies. A total of eight trophies (one per each student in a group) were fabricated. The trophies recognized: Outstanding Project, Educational Presentation; Creative Project, and Best Use of Recycled Material.

Changes in schedule of activity conflicted with our invited group of judges. The course instructor and an senior agricultural education pre-service student teacher served as judges. The event was conducted on a Monday afternoon in November on the lawn area adjacent to the Agricultural Technology Education Center (ATEC) classroom & laboratory facility. Each group was provided with a table for their presentation.

Following a twenty minute time period to set up and operate the fountains, each group was provided an opportunity to share their project with all of the student groups. Students were encouraged to ask questions about project design and operation. This was followed by an individual evaluation by the judging duo using a project scoring rubric. Tri-fold boards were viewed along with project documentation. 

At the completion of the presentations and demonstrations, students were allowed to disassemble their projects and put away PV modules, pumps, and materials. At the end of the lab, a brief critique was provided by the judges before the results of the judging were announced and trophies awarded.',
                'created_at' => '2014-12-23 16:20:58.000000',
                'project_id' => 559,
            ),
            2 => 
            array (
                'netid' => 'welter',
            'scope' => 'The UA Food Day Committee, under the umbrella of the Well U Partnership, is requesting $1500 (of the total of $4500 budgeted expenses) to help pay for the 2 large tents (total ~ $1900), to house the UA Food Day Fair on October 22, 2014 (www.uafoodday.com).  Visit the national Food Day website at www.foodday.org for more information about this nation-wide celebration, in its fourth year.  UA Food Day Fair will emphasize student learning by bringing together 43 student, faculty, and community organizations to exhibit their programs/projects supporting Food Day priorities, focusing on sustainable food and food-related practices on campus.  
Student organizations that will be participating in the UA Food Day Fair include:
•	Students for Sustainability (UA Campus Garden, Food for All, Green Fund, and Compost Cats)
•	Campus Pantry
•	Student Health Advocacy Council (SHAC)
•	Nutrition Club - Smart Moves Team
•	Microbiology Club
•	MycoCats
•	Palm Oil Awareness Initiative
•	Cooking on Campus
•	Marine Awareness and Conservation Society
•	Controlled Environment Agriculture Student Association (CEASA)
•	Residence Life – Ecco-Reps
Students will be exhibiting projects and programs from their academic studies and extracurricular clubs.  Projects scheduled to be highlighted include:
•	Healthy cooking
•	Food security practices on campus
•	Waste reduction practices
•	Innovative growing practices
•	Locally grown food sources
•	Environmental conservation
•	Marine life conservation and protection
Additional exhibitors will include campus and community-wide gardening opportunities, social justice aspects of food availability, and healthy food resources and preparation (see list attached).  Community organizations, often working with UA students, will provide an additional depth of topics and networking opportunities for students’ future involvement.   
From noon-1:00pm, the UA Food Day Fair will feature a cooking demonstration by UA Dining Services Executive Chef Michael Omo using locally grown foods, paired with a container gardening demonstration by the UA Cooperative Extension Master Gardeners.  
With the growing interest in sustainability and healthy food among UA students, the UA Food Day Fair will again provide a showcase for the expanding efforts in these areas and a forum for learning.  Overall attendance is anticipated to be 1000+.',
                'outcomes' => '',
                'response' => 'UA Food Day Fair will be a great success if the following goals are met:
Exhibitors and Demonstrations = 45+ (30% UA student organizations)
Attendees = 1000+

Qualitatively, we anticipate that UA Food Day exhibits will raise awareness in UA students, staff, faculty and the community about sustainable and healthy food practices:
•	that are currently employed on campus
•	which could be initiated on campus in the future
•	that each person could implement in their personal lives and within their communities

Outcome
We estimate that we were successful in achieving our goals and wish to report the following:

Student Volunteers 
Thirty-three students were directly involved and increased their awareness and knowledge about food-related initiatives, organizations, and projects through their involvement as student coordinators (2) or general UA Food Day Fair volunteers (31).  

Student Exhibitors
Additional students (est. 60) were also directly involved with the fair through the students clubs and organizations that were exhibitors or via UA department exhibits primarily staffed by students.  These students shared their knowledge and passion with attendees, supporting and promoting their organization’s mission and the students’ own value as part of the organization. 

Student Organizations (13)
UA Green Fund, SFS
UA Campus Garden, SFS
Compost Cats, SFS
Food for All, SFS
Residence Life Eco Reps
UA Community Garden
Controlled Environment Agriculture Student Association (CEASA)
Cooking on Campus
Nutrition Club (Smart Moves)
UA Campus Pantry
UA Microbiology Club
UA Palm Oil Awareness
UA Stressbusters

UA Departmental Exhibits Staffed by Students (8)
UA Bookstore
UA Campus Arboretum
UA Campus Harvest
UA Canyon Ranch Center for Prevention and Health Promotion
UA Garden Kitchen
UA Life and Work Connections
UA Myco Cats
UA Food Products and Safety Lab


1. Attendees- Students and Others
While it was difficult to estimate the attendance at the UA Food Day Fair, our cumulative data of water cup use, an increased # of tables (41), a steady stream of attendees at each table over a 4-hour period indicates that at least 1200 people attended.  It is additionally hard to say with precision how many of those attending were students, although it would seem fair, from observation, to estimate that students accounted for about two thirds of the total.  Almost all exhibits were interactive in some way.  The level of attendance and obvious high degree of interaction among all participants and attendees indicates interest, while impacts are difficult to assess.  See #2 and #4 below.  

2.	Did the project increase the percentage of renewable energy used at the UA, increase water efficiency, reduce the amount of waste generated by the UA and/or provide education and outreach to initiatives related to UA environmental sustainability?
The UA Food Day Fair showcased the environmental sustainability efforts that are currently employed on campus and encouraged more people to participate in these efforts.  
While we do not know with certainty, we are optimistic that the Food Day event impacted attendees’ decision making processes regarding healthy food choices and preparation and sustainable food practices in one or more of the following ways:
•	Learned about new changes in quality of food and sustainability practices of UA Dining Services, including the new Smart Moves initiative 
•	Learned about the waste reduction and composting practices at the UA
•	Learned more about plant-based diets
•	Observed a cooking demonstration using sustainable and healthy foods
•	Learned about community-wide gardening opportunities and how to get involved, including our UA Community Garden.
•	Learned about nutritive benefits, uses, and preparation of more obscure local (and beyond) food sources
•	Connected with community groups working toward sustainable, healthy foods for possible future student involvement and jobs
•	Learned more about healthy, nutritious, locally grown food choices 

3.	Did the project progress as planned?
The 2014 UA Food Day Fair planning committee, headed by Sarah Marrs, was comprised of representatives from various UA departments.  Planning began April 2014 and proceeded on schedule.  All involved met their commitments on a timely basis and the event increased the number and variety of exhibitors due to diligent and inclusive outreach efforts by numerous committee members.  


4.	What were the impacts and outcomes of the 2014 UA Food Day Fair?
In addition to the results noted in #2 above, the 2014 UA Food Day Fair committee succeeded in its goals of:

•	Growing this annual celebration from its humble 2011 beginnings
•	Expanding its outreach through the participation of new, innovative exhibitors
•	Generating increased awareness and positive behavior changes 
•	Establishing it as a one of UA’s premiere events focused on sustainability and food-related practices',
                'created_at' => '2015-02-13 01:44:50.000000',
                'project_id' => 562,
            ),
            3 => 
            array (
                'netid' => 'yarias',
                'scope' => 'Due to student space constraints, not all Eco-Reps or ACT Reps were able to attend Hall Leadership Camp, decreasing the number of potential Eco/ACT Rep retreat participants.  There was some time spent with each group on Friday, where larger numbers of students were able to participate.  When Saturday came, some of the Eco-Reps and ACT Reps had to return to campus, shrinking the retreat participant size again.  We had about half of the participants we had hoped for actually stay for the Eco/ACT Rep retreat.  

Project Notes
During this retreat, students participated in activities designed to teach them how to be leaders in the areas of sustainability and social justice.  There was some time spent individually (Eco-Reps and ACT meeting separately), allowing each group to increase cohesion and build trust.  The groups also came together for multiple sessions.  This helped set a foundation for collaboration across sustainability and social justice initiatives in the residence halls.  The Eco-Rep and ACT Co-Directors developed and facilitated content that supports the idea of increasing students’ dedication to sustainability and social justice advocacy on campus.',
                'outcomes' => '',
                'response' => 'Activities
Eco-Reps
The Eco-Reps participated in a myriad of activities designed to help them think critically about sustainability issues as well as build team cohesion.  The Co-Directors hosted an Eco-Continuum activity with questions like “our unsustainable behavior will have a bigger impact on the planet or the people?”  Also, there was a structured conversation about advocacy within your immediate family.   Since this can be particularly difficult in some instances, the students commiserated on successes and failures, and brainstormed ways to be more effective.  They students also participated in a natural disaster remodel project, designed to get people thinking about how to build a sustainable city from scratch.  In addition to these educational activities, they also participated in several team builders that helped bring the group closer together.  The retreat ended with a reflection on what sustainability means to each participant. 

ACT
The ACT Reps participated in social justice-related activities as a group.  One powerful experience was an activity called “Cross the Line,” where members of marginalized groups (women, transgender folks, members of the LGBQ+ community, people of color, people with disabilities, etc.) are able to answer a number of questions about their experience as a member of each group. The point of the activity is not to educate the dominant group, but rather to “speak truth to power” and share their stories. This activity can really only be done in a retreat setting, as it usually gets very emotional and needs to allow as much time as needed. This was a very vulnerable and powerful activity for the ACT members, and helped everyone understand where each person was coming from a little bit better.  In addition to this, ACT discussed program planning (so they can educate other residents in the halls), learned about gender issues, and participated in a “Where I’m From” activity.  

Combined 
There were also times when the Eco-Reps and ACT Reps were learning together.  They participated in a team builder where Eco-Reps and ACT Reps were spread across teams so they could get to know each other a little better.  The group also watched Maura Cullen’s iconic talk “Taking the Adversity out of Diversity.”  There was a lively discussion that followed.  Finally, both groups took part in the “Wholly Frijoles” simulation.  This systems thinking and sustainability education activity helps shed light on consumer mentality and the intersections between social justice and sustainability.  	

Student Evaluations
1.	Knowledge of the intersections between sustainability and social justice improved significantly for almost all participants.  On average, knowledge increased 2.41 points (on a 10 point scale).
2.	Participants felt like the retreat was a valuable use of their time, rating on average 7.55 out of 8 (with 8 representing strongly agree).
3.	Participants felt like the off-campus location contributed to their learning, rating an average of 7.36 out of 8 (with 8 representing strongly agree).
4.	Participants felt like they could not have learned as much on campus.  When asked “I feel I would have leaned just as much if this retreat was on campus,” the average rating was 3.13 out of 8 (with 1 representing strongly disagree).  
5.	Some of the comments include:
a.	This was such a great experience.  I am glad to have been here.
b.	HLC was very beneficial and a great learning experience.
c.	The off-campus location and time alone with the Eco-ACT reps really helped us get closer as a group.  We got a lot more out of the time we spent here.',
                'created_at' => '2015-03-08 00:51:04.000000',
                'project_id' => 565,
            ),
            4 => 
            array (
                'netid' => 'kcseitz',
                'scope' => 'The Buckle Up on Recycling Initiative took expired and or used car seats off the road and disassembled them properly into their components. Once they were disassembled the pieces will be used to make new products for the community. My club, Enactus, is planning on creating a business out of selling these products in hopes of having enough profits to employ single mothers looking for employment. 

One of the criteria for Enactus is to empower people through a sustainable project. This statement is what my club and I are trying to create from the Buckle Up on Recycling Initiative. We plan on continuing this project next year and hope that the Green Fund would like to participate in this project to gain the hands on experience of recycling car seats with my club\'s members.',
                'outcomes' => '',
                'response' => 'The impacts that my club had was we collected 212 car seats from the Tucson public. We brought in car seat technicians to inspect the seats to see if they were still safe for use. Out of the 212 car seats the car seat technicians found 48 seats still safe-for-use. These 48 car seats, through our partnership with the Rio Rico Fire Department, dropped these car seats off in Sonora Mexico. We gave these seats to Mexico because the population has a 13% usage rate of car seats compared to Arizona\'s 93% usage rate. We have impacted 48 children in Mexico by giving them a transportation device that could save their lives one day. 

Other impacts that we had was the other 164 car seats that were detected as not safe-for-use and were disassembled into their components. These 164 car seats would otherwise been sent to a land fill. From the car seat components we plan on creating products for the community. My club has been working with a nonprofit that gives shelter to single mothers. Our plans are to employ these women in creating the products to be sold for a profit. The end goal is to take unsafe car seats off the road, while also employing individuals who could not find work.',
                'created_at' => '2015-04-29 23:36:17.000000',
                'project_id' => 574,
            ),
            5 => 
            array (
                'netid' => 'kkelly4',
            'scope' => 'Palm oil is a saturated oil derived from the mesocarp or fruit pulp of the palm tree (species). Versatile and high-yielding, palm oil is the most widely consumed edible oil today. Palm oil is one of the most sustainable plants around and if grown and distributed sustainably and ethically, can be a vital part of the economy and an important nutritional supplement to the local populations who live near or on plantations where it is grown. However, some plantations and suppliers adopt unsustainable and unethical practices which contribute to the issues described above. They produce what has been called conflict palm oil, which is not traceable to its source (i.e., to the plantation it was harvested from, and/or the supplier that sold it to food manufacturers). However, conflict palm oil still may be labeled as sustainable under the Round Table for Sustainable Palm Oil (RSPO) guidelines. Palm oil, including RSPO certified, can be found in over fifty percent of the products sold in American grocery stores, including products like chocolate, frying oil, baked goods, breakfast cereals, cosmetics, personal care, and cleaning products. Because of its versatility, the demand worldwide has tripled over the last few decades. This has fueled exponential growth of palm oil plantations in places like Indonesia, South America, Malaysia and now in Africa. Development of plantations leads to the destruction of tropical rainforests, which contributes to climate change and the decimation of habitat for critically endangered animals including the Sumatran tiger, Sumatran rhinoceros, Sumatran and Borneo orangutans, and other endangered species such as the Asian elephant. In addition, the conflict palm oil industry has also been implicated in gross human rights abuses of its workers, the destruction of the land and homes of indigenous peoples, and has contributed to the poor health of Western nations by increasing access to unhealthy fat. The Palm Oil Awareness Initiative (POAI) at the University of Arizona (UA) is dedicated to raising awareness about conflict palm oil and educating people about what they can do to help reverse some of these destructive practices through their consumer choices. Here we propose to take our message to a wider audience via an appearance on Mrs. Green’s World Radio Show.',
                'outcomes' => '',
            'response' => 'We proposed that this opportunity would impact sustainability at the University of Arizona in three key ways: 1) increase awareness about conflict palm oil and contribute to altering consumer practices with relation to products containing palm oil; 2) build upon lessons learned from previous POAI activities on the UA campus in 2013-2014 to develop a robust, well-researched, and well-documented radio episode that can be used as an educational and outreach tool for future POAI efforts as well as a resource for ecological, biology, and conservation focused campus classes; and 3) provide a platform for reaching and engaging potential UA partners to decrease the use and sale of conflict palm oil in dining halls, restaurants, and snack bars on campus. 		
Mrs. Green’s World reaches an audience that is critically minded and open to knowing more about issues related to how we can, via our consumer habits and activist efforts, slow the tide of climate change and protect the world’s environment for generations to come. As such, it was the perfect audience for us to present our well-researched educational platform about the issues related to the conflict palm oil industry. Our team worked for several weeks prior to recording the Mrs. Green’s show to develop a message that was insightful, clear, succinct. We paid special attention to not only describing the gravity of the issue and atrocities being committed to the environment, animals, and people, but were also sure to offer rays of hope and suggest mechanisms for the public to become involved, take action and work on this issue themselves. Six team members met weekly to develop messaging for the show and three team members appeared on the show: Nicholas Alexandre, Kim Kelly and Dr. Stacey Tecot. Nicolas served as an expert on the environmental and ecological issues of the conflict palm oil issue, educating listeners about what palm oil is, how it is grown, where it is grown and what its growth means to the people, animals and environment where it is being grown in mass quantities for the international market. Dr. Tecot described the POAI, our activities to date and why, from her perspective as a scientist and professor of primatology, the conflict palm oil issue is one that is so important. Kim Kelly then described the multitude of products that palm oil is in, how to recognize if a product contains palm oil, ways to avoid palm oil (including apps for phones when shopping, wallet cards and so forth) as well as described the regulations in other countries around the labeling of conflict palm oil and how that differs in the US and what consumers can do to help change US policies on this front. The podcast is available in many locations including at this link: The radio show was recorded and is available as a podcast in many places, including: http://www.mrsgreensworld.com/2015/04/04/palm-oil-vs-rainforests-a-tragedy-in-the-making-2/
By highlighting and providing listeners with cogent examples of how the conflict palm oil industry contributes to climate change, destruction of endangered species habitat, abuses against humanity, and health issues caused by so many snack foods containing this saturated fat, we increased consumer awareness around this issue and particularly why it is crucial for Americans to take a stand against the use of conflict palm oil. Our presentation was different from other programs on this issue because we were science focused and presented evidence based facts about the issues as well as meaningful ways consumers can become actively engaged in this issue, from altering their own consumer practices to contributing on a larger level. We provided several ways including writing campaigns, changes in shopping habits, educating themselves and others as well as contacting legislators, and corporations in order to encourage them to make policy and operational changes that can decrease the consumption of conflict palm oil globally. 
Our main proposed outcome and goal was to educate and inspire the general public and UA student body about this issue and to encourage them to make some changes as consumers, namely reducing their purchase of products that contain conflict palm oil. Our team recorded the interview on April 2nd, and it aired on Mrs. Green’s World and her affiliate broadcasting channels on April 4th. We shared the airing of the podcast on our social media accounts, via the anthropology listserves and newsletters and emails to friends and family. After the podcast was made available, we shared this on our social media accounts and website as well as with our friends and family, and the School of Anthropology’s website, newsletter and listserves. Immediately following airing of the radio show and broadcasting of the podcast on our social media, our website received 109 visits and maintained an approximate 142 hits/visits every three days for the past month.  This is extraordinary for our website as prior to this we had experienced significantly less traffic.  Additionally, our facebook page received 20 new ‘likes’ immediately following April 4th.  In addition to this, we were invited to give two lectures to two classes on campus by professors and both of these lectures generated a renewed interest in the issue from students, seven of which of have asked to become new members. This is critical for us as the majority of our membership will be graduating this year. Further, we participated in the School of Anthropology’s 100 years of Food event where we informed visitors about our club and encouraged them to listen to the podcast, providing them with the links to do so. 
We are currently planning future activities and are planning to have cards printed that will provide a QR code with various links to the podcast that people can download and listen to.  We will then pass these cards out at upcoming venues where we plan to continue our educational efforts including the 2014-2015 and 2015-2016 school year, including the Reid Park Zoo, the Food Conspiracy Co-Op, and UA Food Day 2015 events. By providing POAI the opportunity to take our work to a larger audience, we stand to gain name recognition and status as authorities on this issue. We were very careful throughout recording our podcast to mention the UA Green Fund’s funding of this initiative several times. By doing this, we were able to highlight the fact that we are UA student group and have received support from a forward thinking initiative at the UA for encouraging students’ engagement and activism around sustainability issue. This in turn increased listener knowledge about the UA’s leadership in this arena. As UA alumni and future Wildcats were among Mrs. Green’s World audience of listeners, it is possible that our appearance has increased support for the Office of Sustainability’s efforts through future donations and/or organizational support. Additionally, as a club that is co-founded by a professor and graduate student in the School of Anthropology and whose membership is largely comprised of undergraduates majoring in Anthropology and Ecology and Evolutionary Biology, our participation has potentially increased local, state, and national awareness about the UA’s programs and efforts in this area, as well as its expertise and world class leadership in, and commitment to, sustainability issues. These are understandably difficult matrices to measure, and we have no concrete numbers to indicate this, but we will continue to monitor the feedback we receive on facebook, our website and via email to determine if this has occurred.',
            'created_at' => '2015-04-30 21:10:13.000000',
            'project_id' => 586,
        ),
        6 => 
        array (
            'netid' => 'bkleiman',
            'scope' => 'Currently, there is no complete record of the numerous efforts and projects on UA campus related to water harvesting. However, it is well known that there are a number of water harvesting projects that have been completed around campus by faculty, staff and students.  These projects are taking place in various departments across campus. It is critical to know who is doing what, and where so that we can learn from one another, and show the impact that entities across campus have been having when it comes to sustainable water use and landscape management at the University of Arizona. A central and easily accessible catalog of these projects will be invaluable for students, faculty and staff on campus. 
The Green Engagement Guide will be including a centrally located list of projects that have happened or are currently happening on campus for students to engage with; however, this is a recent addition to the Green Engagement Guide and they do not have staff time currently to find water harvesting projects in addition to all the other projects on campus. “Harvesting Water on Campus and in the Community: A Catalogue of UA Projects and Programs” will fill this gap. It will list all efforts and projects related to water harvesting in one location, and thus, be extremely useful for everyone on and off campus. This list will be added to the Green Engagement Guide, enhancing its effectiveness and scope. 
This comprehensive water-harvesting list will have a number of benefits. It will allow students and faculty to easily become involved if they desire. It will aid in any research being done into UA water harvesting efforts (by local news media or campus news, for example). It will also link people and projects and foster collaboration and communication. UA is very dedicated to environment and sustainability. This project will prove this dedication for all to see and give everyone credit for their hard work and efforts to build a more sustainable future. In addition, UA can obtain a higher ranking from the Association for the Advancement of Sustainability in Higher Education (AASHE), American College and University Presidents’ Climate Commitment, Sierra Club, and Natural Resources Defense Council.',
            'outcomes' => '',
            'response' => 'Proposed:
Once this catalog is complete, any student accessing it will learn a great deal about campus sustainability related to water harvesting. The users to the site will be able to click on a project and learn more about it. If the user wants to learn more about the project than is listed, there will be contact information relevant to each individual water harvesting project. The students will easily be able to contact someone related to a project that interests them and get involved, either by visiting the water harvesting project sites or by participating in a project. In addition, there is the visual education component. People can see these projects and learn from them by visiting the sites. The water harvesting projects, as well as the catalog, will build awareness. 
UA strives for 100% student engagement on campus and this catalog will help facilitate that engagement. The availability of water harvesting information will make it much easier for people to get involved with sustainability on campus because the information will be at their fingertips instead of requiring time consuming research, as it does now. Sustainability at UA is already happening; this project is a matter of putting all that is being done on campus into one place for people to easily access and find out more. Perhaps an individual is looking for a specific water harvesting project on campus, accesses the catalog and sees that it has not yet been done. Then this student can start a new project to impact sustainability on campus! Likewise, some people might assume that their bright idea has already been done. This catalog will give people a place to look and see that perhaps it has not been done, and they might be able to do it themselves. This catalog could potentially have a “snowball effect” on on-campus water harvesting projects. The more awareness, and the more accessibility there is, the more sustainability there will be. Anyone interested in anything related to water harvesting at UA can benefit from this project.
Measuring and reporting impact this project will have should be clear and straightforward because the catalog will be an easily accessible online resource. Each water harvesting project, past and future, will be listed and dated. Users of the catalog will clearly be able to see how many UA departments are (or have been) involved in water harvesting, how many students have been involved and how many media hits there have been to the website. Examining these numbers, Green Fund will be able to see if there is a rise in water harvesting projects on campus since the inception of the catalog. Once the catalog is initially created, everything will be well documented in one central location, and continuously updated, making impact of the catalog easy to track.
Actual:
The actual impacts of this project are very close to the originally proposed impacts. The projects listed will be easily accessible online and users will be able to identify different projects. Since this water harvesting catalog is part of a bigger project, the Green Engagement Guide, students will actually have access to a larger variety of campus projects, beyond the scope of water harvesting.  The Guide will draw attention to sustainable practices and design on campus that will serve as examples for students to emulate when they leave campus and go out in the world - part of the living and learning laboratory that the university experience can offer.
The original proposal stated that each project listed would have contact information for an interested student to find out more. However, not every project on our list has a contact person, though the majority of them do. This is one difference between proposed and actual outcomes. The fields to be filled in for each project changed slightly over the course of the semester, but the main points and important information are all there. Each project varies, so not every single project listed will have every single field completed. This was to be expected.  The number of students reached was difficult to obtain for each project, especially if that project was conducted off campus. Many of the listed projects are workshops done with community members or school children. The number of students reached column only applied to UA students. 
Another difference between proposed and actual outcomes is the scope of the projects. We went beyond just water harvesting and went in depth into water sustainability. This was in order to include all the amazing projects that deal with water. This matches the Green Engagement Guide’s sustainability mission. In addition to water harvesting projects on campus, the catalog includes water harvesting workshops and courses, on and off campus. Any affiliate with the UA was included.
Another addition to the catalog was an ID number, given to most of the on campus projects. This is to be better able to keep track of projects in the future, reduce duplicates, and coordinate with a future GIS mapping system of the projects. 
This catalog should meet the goal of getting students and non-students engaged with sustainability on and off campus. There are so many possibilities in Tucson for learning about and implementing water sustainability projects.  Now interested parties can access them all in one place, and find out more.',
            'created_at' => '2015-05-05 18:53:06.000000',
            'project_id' => 580,
        ),
        7 => 
        array (
            'netid' => 'deepaksridhar',
        'scope' => 'This project has dealt with synthesizing activated carbon (AC) from biomass waste available on campus. According to the plan, we did collect biomass waste such as olive seeds, pine cones, saw wood waste (from the U of A, utility center), seed pods and dried fruit bunch. We performed all the initially processing with all the above mentioned biomass, however we could only do the project on a one by one basis due to limited equipment and money we had. Using our scientific intuition, we started with pine cones and we have exciting results. We plan to publish this work very soon. We have also studied the effect of activating agents, surface area of AC, the electrochemical measurements of the supercapacitor electrodes which were made using these AC.

Please see the attached Excell for summary of results.',
            'outcomes' => '',
            'response' => 'Student benefits
Four undergrad students were directly involved with this project. We had more than 15 volunteers (both grad and undergrad) who helped us in collecting the biomass. 

Considering all the conferences in which our work was exhibited we had more than 150 people who listened to our work. (So, I think they come under experiential learners)

This experiment were also demonstrated to a group of few young students who visited the labs (I guess it was open house day).

Conferences/talks (both oral and poster involved) (In these conferences UA green fund was acknowledged)
1.	Sridhar, D.; Muralidharan, K. In AzSEC - Arizona Student Energy Conference,  Fourth Annual Student Conference on Renewable Energy Science, Technology, and Policy Tucson, AZ; Renewable Energy Network, Arizona, 2015.

2.	Sridhar, D.; Muralidharan, K. In SWESx-2015, Tucson, Arizona; Department of Soil, Water, and Environmental Science, University of Arizona, 2015. 

3.	Sridhar, D. UA Grad Slam- 2015, “Supercapacitors and graphene” Tucson. 27 March, 2015. 

4.	Sridhar, D; Raghavan S.; Muralidharan K.; CHEE-2015 symposium. 23 April 2015 ‘Fullerene-boost’ supercapacitors: Performance enhancement via addition of fullerene self-assemblies.',
            'created_at' => '2015-05-07 21:32:50.000000',
            'project_id' => 566,
        ),
        8 => 
        array (
            'netid' => 'tannerjeanlouis',
        'scope' => 'The Grassroots program is an outreach, education, and engagement committee within the ASUA Students for Sustainability (SFS) program with the goal of mobilizing students to take an active part in promoting sustainability across campus and in their community.  Grassroots is leading a campaign called Break the Bag Habit, which aims to reduce plastic bag waste generated by the Arizona Student Union markets across campus.  We plan to do this in cooperation with the Arizona Student Unions by distributing 850 reusable tote bags to U-mart customers, who will receive a small discount for using a reusable bag instead of a disposable one.  The bags will be distributed by trained volunteers in the markets and residence halls who will promote usage of the bags and educate students about how disposable plastic bags impact the environment.

With the $1,500 that the Greenfund provided Break the Bag Habit, 725 bags and 600 information sheets were purchased in March. The reusable bags that were purchased from Cheaptotes.com for $1285 dollars while the information sheets were made by Fast Copy on the University of Arizona campus for $101.38.  We did not spend $113.62 of our funding. 

After receiving the bags, we decided to hold off on distributing all of the bags immediately. Instead, a small number of bags were distributed at the three farmers’ markets on the mall during the last six weeks of the semester. As it was late in the semester, we did not want to give bags to people who were only going to be on campus for a short remainder of the school year. Therefore, while tabling at the farmers’ markets and advertising the distribution of the bags in the fall semester, we only handed out a few bags to people who signed our commitment log and provided their email address in order for us to contact them next semester to remind them to continue using their bag. 

As the vast majority of the bags will be distributed in the very first weeks of the fall semester, we need a plan to distribute our bags to the correct audience in order to receive maximum participation with our first, relatively small, inventory of bags. Our first plan of action will be to give out about 250 bags out to resident assistants during their weeks of training.  This population of students will likely use the bags at a relatively high rate because they will have gone through sustainability training and be better suited to act in a more environmentally aware manner. The other primary manner of distributing the bags will be through tabling at the Park Market and Highland Mart. We have met multiple times with Todd Millay of the Student Union in order to gain approval to table at these two locations. We will also distribute the bags during our general tabling for Students for Sustainability on the mall.',
            'outcomes' => '',
            'response' => 'The primary desired impact of the Break the Bag Habit campaign is to reduce the amount of plastic bags consumed on campus and by doing so lessen the University’s impact of greenhouse emissions and the environment.  To do this, SFS is employing the services of it’s interns and volunteers to educate the Wildcat community on the impacts of single use plastic production on the environment.  Through this educational campaign, SFS hopes to instill change in UA students and create a more sustainable behavior throughout their life.  

Break the Bag Habit also serves as a model for student-led initiatives.  This campaign was started by students working within SFS.  These students have collected signatures, collaborated with Residence Life and the Arizona Students Unions, as well as mobilize a base of volunteers to effectively change the way the UA student population sees single use plastic.  With the Green Fund’s help, these students will continue to be models to the campus community inspiring new environmental projects around campus.

At the student markets and on the mall, we will have a visual detailing the importance of reducing plastic bag usage and a commitment log. At the student markets, we will primarily focus on students that are exiting the markets with bags of groceries, but will also talk to people who are entering the marts as well. In order to determine the people who will get a bag, we will ask students who approach our table a series of questions, such as whether they live on campus or not, if they shop at the markets often, and if they are willing to make a commitment to using the bags. If they seem interested in participating and agree to make the commitment, we will ask them to sign our commitment log and provide their email address, and give them a bag. We will send periodic emails to those who provide contact information in order to remind them to keep using the bags at the markets in order to help our cause. We will also ask that if they do not feel they are using the bags as much as they thought, then they would return the bag to us so we could give the bag to someone who would be able to use it more effectively.
In order to track the bags, Todd Millay was able to arrange a button to be implemented on the registers at Park Mart and Highland Mart so that we can do so electronically. Every time someone uses the bag, the market employee will simply press a button and it will show up in the final reports given to the student unions. At the end of each month, we will send Todd Millay an email to request the recorded bag usage.',
            'created_at' => '2015-05-13 16:49:26.000000',
            'project_id' => 568,
        ),
        9 => 
        array (
            'netid' => 'rudnick1',
            'scope' => 'Green Fund Progress Report: Mini Grant 15.26 TGK Rainwater Harvesting project
1.	Project Scope – project summary from proposal and any alterations
a.	The project involves close collaboration with Watershed Management Group (WMG), who will provide education and training to a UA student administrative co-leader and student volunteers to install a functional demonstration rainwater harvesting system at The Garden Kitchen (TGK). 
b.	Project goals: improve the existing cistern on site, which was damaged shortly after installation two years ago in a windstorm and is not currently functional; integrate the cistern into the garden’s irrigation system; engage students from multiple UA departments to learn more about water harvesting and educate community members; develop curriculum to teach classes on water harvesting to the community at TGK; and to increase the sustainability of the garden by taking advantage of rainwater and reducing dependence on municipal water sources.
c.	Alterations: Once the project had begun, two things became apparent: the existing cistern would have to be fully replaced in order to have a functional system and that it would not be within the scope or budget of this project to connect the rainwater system to the existing irrigation lines at this time. The reasons for the decision to shift the project’s focus from connecting to the irrigation were: that the cost of installing a pump and other plumbing mechanisms was much higher than the allotted $1,500; WMG informed TGK that in order to utilize captured rainwater to support the overall garden, they would need to install multiple catchment barrels (which they do not have the funding or capacity to support at this time); and it was found that the city requires a special permit and inspection in order to connect with their water system via a pump. Despite these alterations, we feel that the project has still had a major impact on sustainability at TGK, increasing student engagement, and will have a continued impact on increasing the community’s knowledge of sustainable water practices. To this end, TGK was able to purchase a new and larger cistern, complete construction on a more involved catchment system, and create a curriculum to educate students and the larger community about water harvesting and sustainability.    
2.	Impacts and Outcomes - desired outcomes from original Green Fund proposal and how you proposed to achieve and measure. Comprehensive summary of actual impacts and outcomes with goal of improving GF committee’s understanding
a.	Desired outcomes: to decrease municipal water use at TGK, leveraging the capacity of the rainwater cistern and to transmit water harvesting related skills and knowledge to UA students and Tucson community members.
i.	We will purchase educational materials and write a curriculum to teach UA students and community members about water harvesting. Additionally, the new water harvesting system at TGK will be used to demonstrate an effective system to students and the community.
ii.	We will gauge a change in municipal water use based on the water bill received at TGK. A monitoring and evaluation plan will be implemented to measure the efficacy of our educational program: we will track the number of UA students and community members who receive training in water harvesting at TGK; we will implement pre-/post- tests to measure knowledge gain around the topic of water harvesting and sustainability. We will periodically follow up with participants to see if they have implemented any of these practices at home.',
            'outcomes' => '',
            'response' => 'b.	Actual impact:
i.	With the help of our student administrative co-leader, TGK has replaced a non-functioning and under-utilized catchment system with a new version that has a much larger capacity. This student has also worked with Pima County to connect the air conditioning runoff to the cistern in order to further increase the water being collected. The water that is collected is now being used to water a row of fruit trees in the garden, significantly reducing the use of drip irrigation in this portion of the garden. 
ii.	In addition to the new cistern, the student co-leader worked with TGK staff, garden volunteers, and WMG staff to plan some additional passive water harvesting features into the garden. This student will develop a portion of these features before completing his internship in August to further direct water to the fruit trees. 
iii.	Several lesson plans and handouts have been created to educate UA students and other community members about rainwater harvesting. These lessons focus on: the benefits of collecting and utilizing rainwater, how to develop and build an effective collection system, passive rainwater features, rebates available to encourage water harvesting; and how to maintain an at-home water harvesting system. The student co-leader has also developed training for TGK staff to maintain and teach using the newly installed demonstration system. 
iv.	An unexpected (but very positive) outcome was the development of a very close working relationship with WMG through this project. Beyond the assistance that was provided to complete this project, WMG and TGK staff have developed a plan to work together, with a group of students from the UA College of Education, to develop an overall water sustainability plan for TGK. Because representatives from WMG and a professor at the College of Education were inspired by the work being done on this cistern project, they identified TGK as a model site for working with students and other communities to increase water sustainability. Through this new partnership, 20-30 students will be working with WMG and TGK in just the next semester (fall 2015) to develop a “water plan” for TGK and to implement some of these features. In addition, these partners plan to bring students from the UA and international visiting students to engage in education and work on these projects at TGK in the future.
v.	The student administrative co-leader involved with the project will have completed over 300 hours by the end of the summer. Through this project, he has had the opportunity to actively learn about both active and passive water harvesting systems. He has also been able to practice teaching about these types of projects. Additionally, he has worked closely with WMG and has developed his own direct relationship with this organization.
vi.	A photography student intern has been recruited to take pictures and help document the new and improved system in the fall. 
vii.	TGK staff is also recruiting a UA student to serve as a one-year garden Americorps volunteer, who will be trained in using these rainwater systems and will help to engage and educate other students and the community.
viii.	Because TGK receives water bill statements quarterly, we have not yet seen whether there has been a measurable change in municipal water use at to date. However, because the fruit trees are being watered using rainwater and A/C runoff it is expected that our next statement will reflect a reduction.
ix.	TGK staff is working with the Arizona Department of Health Services to have the new rainwater system certified so that it can be used on other edible plants in the future.
3.	Student engagement – 
a.	One student administrative co-leader (undergraduate) has been engaged in the project so far. 
b.	More students to be recruited in the fall and spring semesters to work as volunteers on the project.
4.	Budget & Changes to spending – 
a.	The major change to the planned budget was the purchase of a new cistern. This purchase replaced the need for a pump because the decision was made to not connect the system to our existing irrigation at this time (due to the constraints listed above). 
b.	In addition, the educational materials were created using our student administrative co-leader and therefore did not add additional cost (as we had planned in the original budget). These monies ($300) were used to cover costs above what were budgeted for installation and plumbing materials, and the extra cost of the new cistern. All materials will be printed within the budgeted $100 for printing.
c.	Despite those changes, the project was completed on time and just slightly under budget ($1485.58).
d.	Other money secured for the project included $2817.50 from the UA Cooperative Extension to hire a temporary extern to serve as student administrative co-leader on the project. The UA Cooperative Extension has also agreed to purchase signage that will include the Green Fund logo to affix to the cistern.',
            'created_at' => '2015-07-28 22:49:35.000000',
            'project_id' => 602,
        ),
        10 => 
        array (
            'netid' => 'uafrank0',
        'scope' => 'The intent of this activity was to provide the project director, Dr. Edward Franklin, with an opportunity to travel to California to attend and participate in a solar energy professional development workshop, 2015 Solar Schoolhouse Institute for Instructors, during the dates of July 19-24, 2015. During the planning and preparation of this mini-grant proposal, the dates were available for the project director to attend. Unfortunately, it was later (June) determined the dates did conflict with the 2015 Arizona ACTE Summer Conference conducted in Tucson. 
Historically, the Arizona ACTE Summer Conference is conducted during the second week of July (2013 – July 12-17; 2014 – July 11-16). The Industrial Technology Welding Instructors have utilized our classroom and laboratory facility at the UA Campus Agricultural Center for their hands-on welding training for the past three years. My role in this workshop is to serve as the host. This includes taking receipt of welding equipment and materials prior to the workshop, arranging the classroom and laboratory facility, opening and closing the facility each day during conference. 
Unfortunately, due to an apparent scheduling conflict with conference hotel (Westin La Paloma), the Arizona ACTE Summer Conference was pushed back a week later this year (July 17-22, 2015). In my early annual planning, I had penciled out the earlier week (which would have followed the pattern of the previous years, – July 10-15). This was brought to my attention in June; after I had made arrangements to attend the Solar Training Institute. Due to the small size our department (three faculty) and commitments I had made to host a half-day concrete workshop on the Saturday of the conference, and a full-day welding workshop on the Sunday of the conference, I could not ask another individual to step up and host the Welding Instructors. 
I contacted the coordinator of the Solar Institute and explained my situation. A full refund was provided. I then contacted our Business Center and Ms. Julia Rudnick to arrange for a return of the Mini-grant funding.
Following the Summer Conference, I contacted the Arizona ACTE staff to verify the dates of the 2016 summer conference. The coordinator of the Solar Institute contacted me and asked for dates for 2016. We discussed available dates they are considering for 2016. At this time, the 2016 Arizona ACTE Summer Conference will be conducted July 15-20. The Solar Institute plans to conduct their workshop July 24-30.
It is my intent to prepare and submit a new mini-grant proposal to attend the summer Solar Institute to the Green Fund Committee to consider in spring 2016.',
            'outcomes' => '',
            'response' => 'Due to a scheduling conflict, the project director did not attend the Solar Institute.',
            'created_at' => '2015-08-11 21:23:39.000000',
            'project_id' => 594,
        ),
        11 => 
        array (
            'netid' => 'uafrank0',
        'scope' => 'Solar Tools for Future Teachers is about adopting and using tools and materials to help future classroom teachers learn to adopt solar energy teaching tools for classroom instruction to promote solar energy and promote science and math fundamentals. Funding will be used for Dr. Ed Franklin, Department of Agricultural Education to acquire specific solar classroom tools from Solarschoolhouse.org. The solar tools are designed for teachers in grades 3-12 introduce students to solar energy concepts. Dr. Franklin’s role as a teacher-educator is to prepare UA Agricultural Education AGTE majors prepare for careers as high school or community college instructors, and to provide professional development activities to Arizona high school and community college agriculture educators. Dr. Franklin has acquired and uses the “Teaching Solar” (Rahus Institute) reference book to pull out specific activities and used with AED and AGTM courses with success. The Solar Classroom Set comes with all the equipment to allow hands-on exploration of solar power and electricity. The tools include a Solar Cell Classroom Set (includes 30 solar cells; 4 solar modules; 16 motors; 1 radio w/ speaker; 1 digital  multimeter; 10 jumper wires with alligator clips; assorted DC loads: buzzer, fans; and a storage case. The Solar Power Monitor Set is a Plexiglas circuit board with input ports for connecting solar modules, and output ports for attaching direct current loads. It has an ammeter and a voltmeter to simultaneously measure current and voltage. There are three DC loads (a light, fan and water pump) included in the set. Using the monitor, students get a feel for how the sun can produce electricity and what volts, amps and watts are. The Solar Power Monitor Set is an excellent companion to the Solar Cell Classroom Set.  Solar Module, 3V*1A Solar Schoolhouse brand 3 watt (3V*1A) silicon solar module mounted on a fiberglass substrate.  Includes alligator clips. Can be used individually to power model solar cars, or combined to power a solar fountain. Versatile use for solar experimentation and investigation activities.',
            'outcomes' => '',
        'response' => 'Both solar PV teaching sets and additional solar cells were acquired from The Solar Schoolhouse in April/May 2015. The Solar Power Monitor Set was used with two audiences during summer outreach workshops and presentations during the month of May and June. The first workshop was with a group of adults participating in a USDA-funded Solar PV Education Workshop for Cooperative Extension, conducted at the UA Campus Agricultural Center. The workshop was attended by eight adults. The second activity with use of the Solar Power Monitor Set was with approximately 119 middle school students attending the Dodge Middle School Summer Enrichment Program. A hands-on presentation was made to seven (7) separate classes of approximately 17 students per class. Students were able to connect solar PV modules in series and in parallel configurations, and view the resulting DC volts and DC amps on the Power Monitor. Additionally, the effect of shading from clouds in the atmosphere was recognized by viewing the power output gauges.',
            'created_at' => '2015-08-11 22:34:10.000000',
            'project_id' => 560,
        ),
        12 => 
        array (
            'netid' => 'rudnick1',
            'scope' => 'Project Description: American Landscape Art & Environment Field Experience
“So say we the artists, ‘Travelers, let us begin the serious business of re-enchanting the
planet.” – Artists Helen and Newton Harrison
This proposal is for support for an innovative field course (American Landscape, GEOG
407) scheduled for pre-session 2015 that will explore both the physical and cultural
landscape by visiting Western eco-art and land art sites, as well as other important sites in
the Western landscape. In the spirit of experiential learning, student engagement, and the
geographic tradition of field learning, this immersive experience, in which students will
travel, camp, eat, and learn together, is designed for students of geography and the
sciences as well as the arts and is open to twelve students. The course will be a
transformative student experience.
The environmental and sustainability issues we face in the 21st century require us to draw
on all of our faculties—the arts as well as the sciences—to ascertain and imagine our
place in the world and how we might live in better collaboration with the earth. Students
are acutely aware of this, and this project catalyzes the growing student interest in the
connections between art and environment into an innovative and specific course
opportunity. By bringing students directly to sites throughout the West that are on the
leading edge of art & environment projects, this course will also help galvanize
momentum for further development of art and environment student opportunities,
including with the Art & Environment network at UA’s Institute of the Environment that
Magrane has played a key role in developing.
100% of the funding requested from this Mini Grant will go directly to offset course
costs, with the intention of making this pilot course as affordable as possible for students.
A funding request has also been made to the 100% Student Engagement Initiative.',
            'outcomes' => '',
        'response' => 'This mini grant supported an innovative field course (American Landscape, GEOG 407/507) that took place in pre-session 2015. The course explored both the physical and cultural landscape by visiting Western eco-art and land art sites, as well as other important sites in the Western landscape. The course was designed in the spirit of experiential learning, student engagement, and the geographic tradition of field learning.
It was a very successful course. The immersive experience, where the group traveled, camped, and ate together over ten days of the field course, allowed for a different kind of educational experience than that found in a traditional classroom. “This is the first class in a long time that I have learned something new, and not just reviewed content that culminated from other classes,” wrote one student in her evaluation. “My favorite take away would have to be the connections made between art and science or ecology.” The course serves as a baseline for future courses like this that I hope to teach in the future. In addition, the course has helped catalyze momentum for ongoing art & environment student opportunities. UA News wrote a feature piece on the course, accessible online.
Student Involvement/Benefit The course was student-designed and student led (Magrane, Ph.D. candidate, student engagement level one).
At the level of experiential learner, four students took the course (student engagement level 5). My hope in designing the course was that it would be a transformative and keystone experience for students, and the feedback from students indicates that it was. For example, student Allison Koski (as quoted in the UA News feature on the course) said: “This class has been revolutionary from anything I have taken before and is very progressive… I definitely think that learning in this capacity is helpful for getting value out of a student\'s college career. There is something special about the structure of this class that allows us to think and discuss freely while on the road, and I feel that I have learned more valuable concepts in this class than I have in a classroom."
Another student wrote in her evaluation of the course:
“The opportunities to learn would present themselves as we moved physically through the spaces, taking the unexpected in turn and rolling with the punches. Even the speed bumps in the road made for great stories. I hope that experiences like this will be available in the future and that this class will be around for future students. I was glad to be a part of the pioneering run!”
In short, the Mini Grant helped make possible immersive and in-depth experiential learning around the concepts of landscape, art, environment, and sustainability. While the course was originally open to twelve students, having an intimate group of four helped make the “pioneering run” of this course one where the group bonded and became a cohesive whole moving through the expanded classroom of the landscape together.
At the level of extended experiential learner (student engagement level 5), the course has likely had impact on over a hundred students, through their reading about the course. This is indicated by the number of Facebook likes on the UA News piece (currently at over 150, with an estimate that 2/3 or more are students).
In addition, the course has played a role in helping to catalyze student development of an Environmental Arts committee in ASUA Students for Sustainability; Stephanie Choi, the chair of that committee, reached out to Magrane for feedback on forming the committee and its activities after hearing about the course.',
            'created_at' => '2015-08-31 23:00:41.000000',
            'project_id' => 599,
        ),
        13 => 
        array (
            'netid' => 'rudnick1',
            'scope' => 'Project Scope: The UA hosts dozens of large outdoor events every semester, ranging from run-walk fundraisers to large public events such as the Festival of Books and Spring Fling. At many of these events, bottled water is available to participants. 
This Mini Grant used funds to purchase a portable drinking water station (WaterMonster.com) to be used at outdoor events on the UA campus. The goals of this project were 1) to reduce the environmental impact of on-campus events through decreases in water bottle use; 2) to reduce the expense to event organizers who would previously have purchased and provided bottled water; 3) to encourage hydration and healthy behavior at UA-sponsored outdoor events; and 4) to demonstrate the success and utility of water stations at outdoor events through a pilot project that could be expanded to include even more UA events in the future. 
The famous “3 Rs” of environmental sustainability are Reduce, Reuse, and Recycle. There is a reason they are listed in that order: The greatest impacts can be achieved through reducing resource use. This project was able to divert waste (even recyclable waste) by providing a large, reusable drinking water station for public events. The water station we purchased holds 125 gallons of water, which is equivalent to 1000 plastic single-use water bottles. Resembling a large Coleman or Igloo water cooler with 6 spigots, it sits on a sturdy, wide-legged stool base so that the spigots are roughly waist high. Providing a highly visible, reusable, and convenient source for drinking water at outdoor events on the UA campus has offset a considerable amount of plastic bottle use. The temporary drinking water filling station has important benefits to outdoor special events held on the UA campus. Encouraging healthy and environmentally beneficial hydration choices is in line with the UA’s sustainability principles. Offsetting bottled water use at the UA has helped to decreases greenhouse gas emissions, reduced the burden on FM waste collection staff, and reduced the cost that the UA pays for hauling materials to recycling facilities. 
Importantly, the water station meets UA Risk Management, Pima County, and State of Arizona health guidelines for public drinking water stations. To address safety and liability concerns, UA Risk Management utilizes a dedicated food-grade hose that is used only for filling the portable drinking water station. 
The Office of Sustainability staff purchased the drinking water station and the dedicated food-grade filler hose. Facilities Management stores the portable water station. Facilities Management (FM) continues to check out the water station to interested partners. FM continues to encourage departments and groups checking out the Water Monster to provide “water coaches,” who will monitor the water station to ensure it is being used properly, and to wipe off the spigots periodically with a cleaning cloth. FM charges a small fee to groups who check out the Water Monster. This fee covers the overhead cost of storage, cleaning, and transportation. The Water Monster has been tremendously successful. Due to its success and visibility, UA Commencement used the Water Monster and purchased another one for future use. The Water Monster has been used at Freshman Orientation, Fraternity and Sorority Events, Concerts, Student Welcome Back events, UA Races, and Spring Fling to name a few.',
            'outcomes' => '',
            'response' => 'Impacts and Outcomes: UA students, staff, and faculty are among the primary beneficiaries of the bottled water filling station. Students attending the many outdoor events are provided with ample opportunity to interact with and use the bottle filling station. UA student benefits have come in two major categories: Volunteer and Experiential Learner.
The number of Experiential Learners who directly use the drinking water station have numbered in the thousands thus far and the uses are unlimited regarding the duration of the Water Monster life. Given that Spring Fling alone routinely attracts as many as 25,000 guests, and the recent Festival of Books attracted an estimated 120,000 people to campus, a drinking water station will be visible to many tens of thousands of Experiential Learners.',
            'created_at' => '2015-10-15 21:41:26.000000',
            'project_id' => 563,
        ),
        14 => 
        array (
            'netid' => 'rudnick1',
            'scope' => 'Using a Green Fund Mini Grant, the University of Arizona’s Office of Sustainability hired a UA undergraduate student whose course of study included photography. She was able to photograph Green Fund projects, their execution, and their impacts. Many of the high-quality photographs produced have been used to promote the positive aspects of Green Fund projects, including but not limited to: sustainability, innovation, student engagement, aesthetics, school pride, community involvement and outreach. These photographs helped in storytelling about student engagement in sustainability at the UA. 

One of the Office of Sustainability’s campus roles is documenting and promoting all sustainability-themed initiatives occurring with support from the Green Fund.  This storytelling is critically important for outreach, community-building student engagement, and for fostering continued community support for the Green Fund. Additionally, the University of Arizona’s current branding guidelines emphasize high-quality, visually engaging photography. The Office of Sustainability has been approached by other University units in the past with requests for high-quality photographs of sustainability-themed projects occurring on campus and has been unable to provide them. While Green Fund awardees are asked to provide photographs of their projects to the Office of Sustainability, the vast majority of photographs provided are not of sufficient quality to be used in promotional or press materials, such as on websites, in brochures, on banners in press releases, etc., with many photographs blurry and/or taken by cell phone cameras. Most Green Fund awardees do not have advanced photography skills, specialized cameras or time to take high-resolution, good-quality photographs of their projects.

The Office of Sustainability hired a University of Arizona undergraduate student, Natalia Navarro, whose coursework included photography. She took professional-quality, engaging photographs of Green Fund projects and their outcomes. She was able to visit 6 annual grant sites, 3 mini grant sites, UA Spring Fling, and the UA Tree Planting Day. These photographs are available for future promotion of the Green Fund and of sustainability more broadly at the UA. She was able to build a professional portfolio with a wide range of subjects and situations. Sustainability is a priority at the University of Arizona, and having more photographs depicting Green Fund projects available for use by the Office of Sustainability and the University community generally, the Green Fund and sustainability are promoted and supported more frequently by the UA as a whole.',
            'outcomes' => '',
        'response' => 'Desired impact(s): With this Green Fund grant, we will share the successes of the Green Fund with the rest of the UA community and beyond using the high-quality photographs produced. We will also share photographs with other campus units so that promoting the Green Fund and sustainability at the UA more generally becomes the status quo. At the UA, storytelling about sustainability innovations and successes with the assistance of these photographs will, we hope, lead to more awareness of, pride in and support for the Green Fund and its accomplishments.  

Strategy/strategies for achieving desired impacts: This project produced high-quality, attractive photographs documenting Green Fund projects and their outcomes. Photographs are organized on the Office of Sustainability server and used as much as possible in current and future Green Fund and sustainability communications. 
Measuring and reporting impacts achieved: 

The following projects were filmed: 
GF15.02 Turning Footprint to Pawprint
GF15.15 Aquaponics and Native Seeds
GF15.18 city High Urban Activation
GF15.20 LEAF
GF15.31 Mushrooms
GF15.32 Solvent Recycling
GFMG 15.11 Buckle Up to Recycling
GFMG 15.18 Tableware at Spring Fling
GFMG 15.19 Compost at Spring Fling
GFMG 15.20 UA Kike Valet
GFMG 15.21 Recycle at Spring Fling',
            'created_at' => '2015-10-15 22:17:50.000000',
            'project_id' => 571,
        ),
        15 => 
        array (
            'netid' => 'rudnick1',
            'scope' => 'Using a Green Fund Mini Grant, the University of Arizona’s Office of Sustainability hired Jesse Minor, a graduate student to create a Green Fund Presentation Guide. This standardized guide has been used to give presentations to UA students about the Green Fund. Broadly, the goal of this project was to create a tool for informing as many UA students as possible about the Green Fund application process and to ensure that the Green Fund Committee receives high-quality grant applications from as large and diverse a group of students as possible. 

As part of the Never Settle emphasis on student engagement, the Office of Sustainability is committed to encouraging students from across the University to apply for Green Fund grants. Yet many students, particularly undergraduates, have never applied for grants before, and many have never heard of the Green Fund, creating barriers to student access. In the past, Office of Sustainability employees have given presentations on applying to the Green Fund to undergraduate classes, but the number of presentations has been limited by the limited availability of staff with a thorough knowledge of the application process. To make the Green Fund accessible to as many students as possible, the Office of Sustainability created a Green Fund Presentation Guide. This guide has been used in 5 presentations this fiscal year including approximately 100 undergraduate students and 30 staff members and will be used moving forward.

The PowerPoint credits the Green Fund for its financial support and contains its logo. Each talk also highlights the Green Fund, its function and how to apply.',
            'outcomes' => '',
        'response' => 'Impact(s): The first strategic priority listed in the University of Arizona’s 2013-2018 strategic plan is 100 percent student engagement. The Green Fund is a unique way for students to become engaged with sustainability and make a real difference in the world—if students find out about the Green Fund and learn how to apply. This project, produced a Green Fund Presentation Guide that has been used in classrooms and staff discussions, will makes it easier to get the word out to UA students about the Green Fund and how to apply. The desired impact of this project was to significantly increase the number of applications received by the Green Fund Committee and the number and diversity of students applying for Green Fund grants. The application cycle has not started yet so we are not sure if the number of grant requests increased over last year.

The project will produced an easy-to-use, efficient, standardized Green Fund Presentation Guide. This guide functions as a lesson plan that Office of Sustainability staff members and Green Fund Committee Members use to educate students about the Green Fund and how to apply. The use of this guide to conduct presentations, such as in UA classes or residence halls, has led to the desired impacts stated above. 

The Office of Sustainability keeps records of all Green Fund presentations done on campus by office staff. We have conducted more presentation this year than last year and have used the guide to teach other Office of Sustainability student staff how to present as well.',
            'created_at' => '2015-10-15 22:50:45.000000',
            'project_id' => 572,
        ),
        16 => 
        array (
            'netid' => 'rudnick1',
            'scope' => 'Started in 1974, Spring Fling has become an iconic campus event by providing carnival rides, games, food booths and entertainment to both the University of Arizona and Tucson communities.
Spring Fling is the largest student-run carnival in the nation attracting more than 30,000 guests. Spring Fling provides more than 35 rides and games to attendees and more than 20 different food booths ranging in options from corn dogs to crab puffs. Spring Fling is planned and organized by eight student directors who work year round to make sure that it is a great event. Countless volunteers work before and during the event to ensure that it runs smoothly for guests. This year, Spring Fling organizers, with funding by the Green Fund, purchased compostable and recyclable tableware and participating student clubs were required to purchase tableware items from Spring Fling organizers. The proceeds were captured and set aside to purchase tableware during future Spring Fling events. Thus this project is now self-sustaining. 
We received $1,500 to purchase recyclable and compostable tableware. By providing these products we were able to avoid using oil-based plastics, reduced the environmental impact of Spring Fling and enhanced the theme of buying and using more environmentally friendly products. Some of the items that we purchased include plates, bowls, forks, spoons, cups, napkins and nacho tray holders. Most food booths are managed, run and staffed by UA student clubs. These UA student clubs accomplish most of their annual fundraising during Spring Fling by hosting and running a booth. In years past, student groups purchased tableware for their individual booths with no thought to product content or environmental impact. Because of Green Fund support we proved the concept that a small change—using recyclable and compostable tableware—can make a large difference. This year we purchased tableware and then each student group purchase needed items from Spring Fling organizers. This helped ensure compliance with the goal of having almost 100% of tableware items be compostable or recyclable. Bulk purchasing ensured items were priced competitively, and student groups further saved money and cut down on waste as they only purchase what they needed, when they needed it. This system was already in place with other Spring Fling items used by food booths—specifically, Spring Fling organizers currently offer a similar service with water, soda and assorted drinks. Each student group purchases beverages from Spring Fling organizers at a site located on the fair grounds and then sells the items to Spring Fling attendees. 
Additionally, providing tableware helped to ensure we had a closed loop compost system with minimal amounts of contamination. We also had representatives from Compost Cats on-site to provide training on composting to students, staff and fair attendees.',
            'outcomes' => '',
            'response' => 'Metrics
Desired Impacts: Purchasing recyclable and compostable tableware, while it may seem like a small detail, had a very real impact on sustainability at the UA. What we hoped to accomplish with this Mini Grant was twofold. First of all, we hoped that event workers and attendees would see, use and properly dispose of the recyclable and compostable products. This provided a novel behavioral mechanism for many people, who were accustomed to throwing plates and other service items in the garbage at outdoor events such as Spring Fling. The other goal of this Mini Grant was to leverage the buying power of the UA. We were demonstrating the concept that if you had to make a purchase, purchase better. Last Spring Fling we were able to prove that compostable service items were competitively priced, adequately perform their required functions, and were easy to order and obtain in bulk. This year, we intended to work with Chef Omo from the UA Student Union however we were not able to accomplish the connection. We were able to purchase the necessary supplies reasonably.  

1)	Marketing
There are many ways in which marketing played a key role in this project. Student clubs were introduced to the Green Fund during training sessions. The Green Fund advertised via signage and verbally by the Compost Cats/SfS interns during the event as well. In addition, the Green Fund logo was placed on the resources list brochure that was compiled and made available for future Spring Fling committees.',
        'created_at' => '2015-10-15 23:25:13.000000',
        'project_id' => 588,
    ),
    17 => 
    array (
        'netid' => 'rudnick1',
        'scope' => 'The Office of Sustainability received a mini-grant of $1,500 to support compost collection and waste diversion at Spring Fling 2015. This project helped reduce the collective impact of Spring Fling through an increase in composting while also providing a high profile opportunity for UA students to educate the Tucson and UA communities on the merits of composting. Spring Fling served between 10,000 to 15,000 students in addition to 15,000 community members. Spring Fling is a great example of how compost services can be effectively incorporated into large scale university events.',
        'outcomes' => '',
        'response' => 'Due to Spring Fling composting, a staggering 658 pounds of compostable food and material was diverted from the landfill in 2014 and 1,284 pounds was diverted this year. The total waste diversion for Spring Fling 2014 including compost and recycling was 59.3% and during this year we had a diversion rate of 78% diversion rate. Amazing success as reported by the numbers.
The mini-grant was used to provide a one-time payment to the UA Compost Cats. In exchange for these funds, Composts Cats provided compost services and educational outreach around the merit of composting for each day of Spring Fling.  Compost Cats used funds to pay usual and customary business overhead cost for gas, equipment maintenance, equipment insurance, and student employment wages.  One Compost Cat intern served as lead and provided project management and logistical services during the weekend. Another 10 Compost Cats served as ambassadors at the five primary waste stations in the food/clubs section of Spring Fling where they helped attendees identify what was compostable.  They also served to substantially decrease compost contamination. Two students coordinated the collection of compostable waste from Spring Fling as well as were responsible for the collection truck and transport of waste to the Compost Cats worksite. Two students trained clubs and food vendors regarding what items could and could not be composted at required Spring Fling Club meetings before the actually event.
Marketing
The UA Green Fund was recognized in social media and conventional media by Compost Cats and Spring Fling through statements attributing the UA Green Fund as responsible for funding compost services at Spring Fling 2015. Additionally, the UA Green Fund Logo will be listed as a sponsor on the Spring Fling event banner and other promotions.
Measurement and Reporting Plan:
Waste diversion numbers specific to composting will be tracked by Compost Cats in pounds for the entire event.',
        'created_at' => '2015-10-16 00:28:20.000000',
        'project_id' => 573,
    ),
    18 => 
    array (
        'netid' => 'srokuski',
        'scope' => 'We were graciously awarded funding from the Green Fund Committee to support the provision of sustainably-produced food samples as part of the 4th annual UA Food Day Fair on October 14, 2015. We submitted the proposal to highlight our plans and vision to provide healthy, sustainably produced food samples at the event and to provide a sensory learning experience for student attendees. 

Executive Chef Michael Omo from UA Culinary Services prepared an array of toppings and dips using healthy, local ingredients. He made “Mushroom Giardiniera” using mushrooms grown by the UA MycoCats, an “Heirloom Bean Ragout” and an “Heirloom Tomato Relish” using beans and tomatoes from San Xavier Farm (Tucson), and served his creations on house made crackers and flat breads. The samples were portioned in small, compostable serving cups. Per a discussion with one of the Compost Cats, food waste and used serving cups were collected by the UA Compost Cats to make compost. 

UA Culinary Services prepped and provided these samples out of their new food truck, the “Road Runner”. The Road Runner was parked just south of the UA Food Day Fair tents in a high-traffic area. An employee from the student union was responsible for passing out samples and providing information about the ingredients used to make the samples. A poster was developed to demonstrate where the ingredients came from and was placed right next to where the samples were being served.',
        'outcomes' => '',
    'response' => 'Desired impacts and outcomes (taken directly from out mini-grant proposal):
The provision of sustainably produced food samples will be a great success if the following goals are met: 
•	Attendees = 1000+ 
•	The event receives media (print and TV) coverage 
•	Positive evaluation from exhibitors as valuable in spreading the word of their food and sustainability practices
•	Although difficult to measure, we anticipate that the food samples provided at UA Food Day will be a sensory learning experience for students and will encourage UA students to make healthier, more sustainable food choices both on and off campus. Furthermore, they will help to increase knowledge, raise awareness and encourage behavior change in UA  students, staff, faculty and the community regarding sustainable and healthy food practices: 
o	That are currently employed on campus 
o	Which could be initiated on campus in the future 
o	That each person could implement in their personal lives and within their communities 

We measured these outcomes by asking the Student Union how many samples were given out the day of the event and by evaluating exhibitor responses regarding the event as a whole. 

Actual impacts: 
It is estimated that over 1000 people attended the event as a whole and per the Student Union’s report, roughly 1,150 sustainable food samples were handed out. The event received media coverage from the Daily Wildcat, UA News and KVOA Tucson (channel 4). The benefit of having the samples was difficult to measure but based on verbal student feedback and the Student Union’s report, the samples were well received and enjoyed by many. There was a poster directly next to where the samples were bring served that indicated where the ingredients used to make the samples came from and the Student Union employee also explained this information.  The purpose of the poster was to demonstrate that the ingredients were locally sourced, healthy, and did not travel far to be created into the sample they had before them. 

Differences between desired and actual impacts and outcomes:

One element of this project that did not come to fruition was the use of student volunteers to pass out and provide information about the samples.  Instead, an employee of the Student Union was responsible for passing out and providing information to students passing by. If we could go back, we would have involved several more students in this sub-project of the UA Food Day Fair to increase student participation and learning. We also would have asked for help from the UA Compost Cats to ensure that all food and container waste was being put into the proper containers and set aside to be used to make compost. After the event, I was informed that the Compost Cats did collect some of the waste from the samples but a more organized system will need to be in place for similar projects in the future. If we are funded for a similar project in the future, more student volunteers will be involved to educate their peers about the benefits of the food they are sampling and there would be an increased effort to ensure all food waste was collected and handed over to the UA Compost Cats. Lastly, we would develop a short survey to hand out to students who visited the food truck to assess their experience with the food samples and to determine if the samples encouraged them to make more healthy, sustainable food choices.',
        'created_at' => '2015-10-27 21:16:55.000000',
        'project_id' => 606,
    ),
    19 => 
    array (
        'netid' => 'ebitieamughan',
        'scope' => 'On November 14, 2015 the University of Arizona hosted its first-ever “zero waste” football game in partnership with the UA Office of Sustainability, Arizona Athletics, Facilities Management, Arizona Unions, Compost Casts, Students of Sustainability, and the UA Green Fund. The zero waste game was a big step towards creating a better campus environment at UA and Tucson community. 

There were over 100+ volunteers and team members that participated in the zero waste football game. The volunteers were assigned a shift as tailgate ambassadors or post-game cleanup crew. One of the volunteers described her experience below;

“Helping bring awareness to the tailgaters during the zero waste football game was a great experience. Even though it was hard to get 100% participation during such an event, getting people to become a little more environmentally conscious is important and we did our job!"-Rachel Johns, UA Student.',
        'outcomes' => '',
        'response' => 'Overall the event was a success and we are able to divert 69% of waste from landfill. We collected 1.28 tons of compost and 10.65 tons of recycling!! These numbers were significantly higher than the average but it would not have been possible without the support from volunteers. We recruited volunteers from classes and organizations such as GEOS 212 –Intro to Oceanography, SBE 195A –Intro to Sustainability, Freshman Class Council and more.

While we will always strive for 90 -100% of waste diversion, we are proud of the work we accomplished. To achieve our goal next time in the future we will work to increase awareness and market the event weeks in advance to gain more support. They were a few things logistically that could so be improved to increase productivity and flow of the overall event. While 69% was a great first attempt at a zero waste football game, we are already working to increase that number at the next attempt.',
        'created_at' => '2015-11-23 21:51:58.000000',
        'project_id' => 609,
    ),
    20 => 
    array (
        'netid' => 'rudnick1',
        'scope' => 'Introduction: Now in its 40th year, Spring Fling is the largest student-coordinated carnival event in the United States. Spring Fling drew approximately 30,000 attendees last year, half of whom were UA students. Spring Fling will once again be held on the UA Campus this April. The existence of a large campus and community event provides the opportunity to reduce the environmental impact of Spring Fling while simultaneously improving the experience of event attendees. 
This Mini Grant application seeks funds for the staffing and operation of a bike valet at Spring Fling. The goals of this project are 1) to reduce the environmental impact of an on-campus event that sees considerable community participation; 2) to reduce the impact of Spring Fling on adjacent neighborhoods in the form of vehicle traffic and on-street parking; 3) to provide a free and convenient bicycle parking option for event attendees and 4) to improve the student and community experience of Spring Fling. 
The suite of parking and transportation related services run in conjunction with Spring Fling is broad and includes: ZimRide (http://www.zimride.arizona.edu/), a social media rideshare platform initially supported by the Green Fund (GF 13.22: ZimRide Ride-Sharing Program); van service for mobility-impaired festival attendees; expanded apartment shuttle service; CatTran shuttle service; Tucson’s streetcar; and permit-holding bicycle rickshaw operators. For Spring Fling attendees seeking a flexible, healthy, affordable, and sustainable transportation option, the bicycle valet is an unparalleled choice.',
    'outcomes' => '',
    'response' => 'Introduction
Spring Fling, the largest student-coordinated carnival in the United States, held the annual carnival on April 10-12th, 2015. Spring Fling 2015 drew more than 25,000 attendees. Since the carnival moved back to campus 2 years ago, UA student attendance has increased drastically. Since a majority of the increase in event attendance was from UA students, the Green Fund support for Spring Fling sustainability initiatives continues to reach many thousands of UA students and employees, as well as future Wildcats and community members. Large on-campus events provide opportunities to address sustainability through a variety of projects. One of the ways that the sustainability of Spring Fling was improved was through the deployment of a special event bicycle valet station. The bicycle valet station was offered free of charge, and represents a collaboration between UA Parking and Transportation Services, the UA Office of Sustainability, Spring Fling Organizers, and the UA Cycling Club. Parking and Transportation Services supplied pedestrian rail fencing, the bicycle racks, and installation expertise, while the Office of Sustainability provided a paper tag system, maps, and lights for valet station users who did not come prepared with lights. The Cycling Club staffed the station duration the three-day event.

Results and Impacts
A total of 191 cyclists made use of the bicycle valet station over the course of Spring Fling (Table 1). By far, the bulk of the use (55%) occurred on Saturday, which also featured the longest operating hours.',
    'created_at' => '2015-12-14 21:28:49.000000',
    'project_id' => 587,
),
21 => 
array (
    'netid' => 'rudnick1',
'scope' => 'The Office of Sustainability requests a mini-grant of $1500 to provide recycling services to Spring Fling 2015. Funds will be used to pay an ASUA recognized club, Greening the Game signage fees, and UA Facilities Management (FM) fees associated with recycling at Spring Fling 2015. 
The mini-grant of $1500 would accomplish the goals of increased waste diversion and outreach in several ways. First, funding would cover the cost of having Greening the Game workers interact with over 50 clubs and vendors of Spring Fling to know what items are recyclable and how clubs/vendors can engage with attendees to ensure that their materials are disposed of properly in ways that are appropriate for the occasion. In addition to covering engagement, the funding will cover the cost of having paired recycling and waste bins at Spring Fling as provided by FM. The funding will also cover service fees, roll off charges, hauling and employee wages as specified by Facilities Management.',
    'outcomes' => '',
    'response' => 'Diversion Rates:
Compost .64 Tons 
Recycle 6.87 Tons
Landfill: 2.06 Tons
Total Diversion rate: 78%.
Greening the Game (GtG) workers interacted with over 50 clubs and vendors at Spring Fling demonstrating what items were recyclable and compostable. GtG  worked with clubs/vendors and engaged with attendees to ensure that their materials were disposed of properly in ways that were appropriate for the occasion. In addition to covering engagement, the funding covered the cost of having paired recycling and waste bins at Spring Fling as provided by FM. The funding also covered service fees, roll off charges, hauling and employee wages as specified by Facilities Management.',
    'created_at' => '2015-12-14 22:06:47.000000',
    'project_id' => 589,
),
22 => 
array (
    'netid' => 'rudnick1',
    'scope' => 'Description: 
The University of Arizona was pleased to participate in the spring 2015 Green Sports Alliance Pac-12 zero waste basketball competition.  The UA chose the March 5 men’s basketball game versus University of California, Berkeley as its home game for the competition.  The planning and implementation process was highly compressed, given the short timeframe between announcement of the competition and the end of the basketball season.  However, UA staff and students organized rapidly and seamlessly to implement first-ever accomplishment – the first “zero waste” program ever at any UA Athletics event.',
    'outcomes' => '',
'response' => 'Diversion Rate (by Weight in Pounds or Tons) (50%):
-Landfill: .82 tons
-Reuse: 95 pounds (.05 tons)
-Recycling: .89 tons 
-Compost: .25 tons
We are extremely proud of the cross-campus partnership and plan we were able to mobilize within only two short weeks in order to pull off all of the above.  All partners were extremely pleased with the results and look forward to collaborating again for any future Pac12 competition.  Indeed, with more time for planning, we are confident we can improve on these results.',
    'created_at' => '2015-12-14 22:54:42.000000',
    'project_id' => 590,
),
23 => 
array (
    'netid' => 'racheldavidsonk',
'scope' => 'Throughout this retreat, students engaged in activities designed to teach them how to be leaders in the areas of sustainability and social justice.  There was time spent individually (Eco-Reps and ACT meeting separately), allowing for growth in each group, increasing cohesion and building trust.  The groups also came together to set a foundation for collaboration across sustainability and social justice initiatives in the residence halls throughout the year. A requirement for ACT and Eco-Reps second semester is to put on a joint program in their halls, this retreat laid the groundwork that will help them to be successful in putting on that program, through both increasing knowledge of the intersections of sustainability and social justice and growing in relationships with fellow members to gain a network of support. The Eco-Rep and ACT Co-Directors developed and facilitated content that supports the idea of increasing students’ dedication to sustainability and social justice advocacy on campus.  

Overall, the retreat provided ACT and Eco-Reps with the opportunity to learn about the intersections of social justice and sustainability in a safe environment. Being removed from campus created a unique environment that encouraged students to be open minded and receptive to learning. It was pointed out that the Triangle Y facility is not the most accessible for students with disabilities. Similar retreats in the future might benefit from being held at a different location which could be more completely accessed by all students.',
    'outcomes' => '',
    'response' => 'Activities

Eco-Reps 

The Eco-Reps participated in a myriad of activities designed to help them think critically 
about sustainability issues as well as build team cohesion.  The Co-Directors hosted an Eco-Continuum activity with questions like “I do/don’t believe we can invent our way out of the climate crisis,” “I believe our unsustainable actions will do more damage to the planet or to the people,” and  “my worldview is more ego-centric or eco-centric.” Also, there was a structured conversation about advocacy within your immediate family. Since this can be particularly difficult in some instances, the students commiserated on successes and failures, and brainstormed ways to be more effective. In addition to these educational activities, they also participated in several team builders that helped bring the group closer together.  The retreat ended with a reflection on what sustainability means to each participant, how they would share gained knowledge, and which activities members thought were most beneficial. 


ACT 

The ACT Reps participated in social justice-related activities as a group. They began by playing a game called “creepy or cute,” which used different situations such as, “a guy comes to your work with flowers,” which started discussions about  our perceptions. This was followed by a discussion about gender roles in our modern society. ACT also participated in team games which greatly contributed to the cohesion of the group throughout the semester. 

The ACT Reps ended the retreat by writing poems and stories about the “builders and breakers” in their lives. This concept was built upon by Maura Cullen’s talk that was watched during combined time. This was an especially valuable way to end the retreat because it gave students the opportunity to reflect on forces in their lives that helped shape the people they are. 


Combined

There were also times when the Eco-Reps and ACT Reps were learning together. They participated in a team builder where Eco-Reps and ACT Reps were spread across teams so they could get to know each other a little better.  The group also watched Maura Cullen’s iconic talk “Taking the Adversity out of Diversity.”  There was a heart-felt discussion that followed.  Finally, both groups took part in the “Wholly Frijoles” simulation.  This activity makes participants reconsider the prevalent consumer mentality of our society and the intersections between social justice and sustainability. The video was followed by a TED talk by Brene Brown called “The Power of Vulnerability,” which emphasizes the importance of being vulnerable when growing, learning, and loving others. This is such a powerful video that reminds viewers that being imperfect and making mistakes is okay, and an important part of learning.',
    'created_at' => '2016-01-21 23:27:05.000000',
    'project_id' => 604,
),
24 => 
array (
    'netid' => 'ebitieamughan',
    'scope' => 'Hack Arizona requested a mini-grant of $1,500 to purchase refillable water bottles to encourage the use of the Water Monster, a sustainable water solution that reduces waste. This project enabled Hack Arizona to engage in sustainable efforts throughout the event while spreading awareness about the benefits and the ease of utilizing reusable products to make a difference in reducing waste, one bottle at a time. These bottles were made from 100% recycled materials and purchased from Tucson-based promotional design company – Vector Impressions.

The participants received the water bottles when the event began and they used them throughout the event. The water monster was located outside the Science Engineering Library and participants could easily refill their water bottles.',
    'outcomes' => '',
    'response' => 'The water bottles were a huge success and it was a visible display of Hack Arizona’s and the University of Arizona’s commitment to sustainability.  The water monster holds about 125 gallons of water and it was refilled at least twice during the weekend resulting in at least 2,000 plastic water bottles diverted from landfill. The bottles looked great and proudly displayed both the Green Fund and Hack Arizona logo.

Overall the event was a success and we were able to incorporate sustainability in the judging criteria, catering companies, and composting efforts. The winning team of the sustainability category created a musical instrument from empty tea, Red Bull, and soda cans. They were able to hit the triple bottom line while pounding on their triple tower bass.',
    'created_at' => '2016-02-03 19:34:10.000000',
    'project_id' => 627,
),
25 => 
array (
    'netid' => 'uafrank0',
    'scope' => 'Registration for attendance to the Solar Institute for Educators has been invoiced for Dr. Franklin to attend the July 24-29, 2016 event. The cost of the institute has increased since 2015. The registration cost was $1,500.00 In 2016, the cost of registration increased to $1,950.00. The additional costs will covered by the Department of Agricultural Education.',
    'outcomes' => '',
'response' => 'Dr. Franklin plans to return from the Institute with increased knowledge and understanding of teaching and integrating solar photovoltaic concepts, activities, and use of materials into the existing AGTM courses. Additionally, the materials and concepts will be used in the newly-proposed AGTM 200 Solar Photovoltaci Applications course (face-to-face) to be offered during Pre-Session, Summer 2017.',
    'created_at' => '2016-04-25 23:18:29.000000',
    'project_id' => 597,
),
26 => 
array (
    'netid' => 'elg1',
'scope' => 'El Día del Agua y la Atmósfera (El Día) is a student symposium that features presentations and discussion related to climate change in the Southwest. Through this conference, we hope to provide a forum through which student presenters can engage with one another and the community to discuss the problem of climate change and its effects on the Southwest US. 

El Día formed from the recent merging of the Department of Atmospheric Sciences with the Department of Hydrology and Water resources and will take place on 1 April 2016 in the Student Union Memorial Center. El Día will feature student oral and poster presentations that address climate change and its impacts on weather patterns and hydrologic processes in the Southwest. El Día will also include a round table discussion between students, faculty, and professional attendees related to the study of climate change in the Hydrology and Atmospheric Science disciplines. This will include a discussion of the sustainability of water resources in the Southwest.',
    'outcomes' => '',
'response' => 'Our goal was that the conference will encourage discussion and future collaborative research related to climate change in the newly merged department of Hydrology and Atmospheric Sciences (HAS). We intend for this conference to allow university students to present their research to the public, including local hydrology firms, so that they can gain insight on local impacts of climate change and use it to make informed decisions in the future.

What I saw at the conference was an open exchange of ideas between students, faculty, and industry professionals, particularly during the round table discussion and during the poster sessions. The round table went well but would have benefited from a more open and relaxed discussion that remained focused on topics of sustainability without becoming entrenched in the specifics of our research. This could have been achieved by breaking into smaller groups. Gupta was an excellent moderator, we would like to invite him to moderate again.',
    'created_at' => '2016-04-27 00:32:50.000000',
    'project_id' => 636,
),
27 => 
array (
    'netid' => 'michaelomo',
    'scope' => 'The Student Union houses the university production kitchens with 23 walk-in coolers & freezers using
incandescent lighting which is in use 15 hours per day. We have identified an alternative light bulb (3M A19 8W)
that are 8 watts vs the current 100 watt.By Changing out these bulbs we have calculated an annual savings of $7,418 with 
total savings over the life of the bulb of $48,778. We requested $1500 to fund approximately 90 bulbs at the cost of $16.79 each.

The cost of the bulb came out to $20.52 vs the approximated cost of $16.97.The kitchen will still see an annual savings of $7,469 with
a total savings of $49,111 over the lifetime of the bulb.',
    'outcomes' => '',
    'response' => 'We hope to not only save the University of Arizona money but be able to reduce landfill waste. Our current incandescent light bulbs have a life 
of only 4 months, vs the new proposed bulbs which have a life of 6 1/2 years. We plan to achieve these goals with the purchase of the new 8 watt bulbs. 
These savings will help the University of Arizona\'s sustainability efforts and will conserve electricity through the wattage and air conditioning. 
We hope to use this opportunity to educate our 25 student workers on the importance of conserving energy and the impact it can have on the University 
of Arizona\'s sustainability efforts. 
After changing out each lightbulb we will evaluate the feasibility of changing fluorescent bulbs to LED alternatives in additional areas, adding to the Student Union\'s 
sustainability efforts and the University of Arizona as a whole.',
    'created_at' => '2016-04-28 00:24:53.000000',
    'project_id' => 629,
),
28 => 
array (
    'netid' => 'mryder',
'scope' => 'On November 30th from 10am to 2pm, a canvas banner will be displayed on the UA Mall in front of Old Main. The banner will have the phrase “I support the UA acting on climate change“ on the top and a blank space below. SfS interns will hand out infographics (see attached example) and talk to passersby about the UA\'s new Climate Commitment signed by President Hart on October 5th and the COP21 Paris Climate Talks that will be taking place from November 29th to December 11th. They will also ask if students would like to show their support of the UA’s commitment to climate action by either a) adding their handprint to the banner b) tweeting or posting their support for the #UActingOnClimate, or c) simply signing a “Thank You“ card addressed to President Hart that will be delivered to her at the end of the semester. SfS interns will lightly paint the hands of participants who choose to do the first action and provide a washbasin for washing off paint afterwards. An area on campus to prominently display the banner after the event is TBD (suggestions are welcome).

A photographer will be present to document the event via Facebook and Twitter. SfS interns are also reaching out to the Daily Wildcat to cover the event.',
    'outcomes' => '',
'response' => 'This is one of several events the SfS E+C Committee will put on each semester to engage with UA students on how they want to see the UA address climate change. Many students (and faculty and staff) are unaware of the UA’s 2012 Climate Action Plan, and there is no formal or prominent mechanism for student involvement in climate planning at the UA. The SfS E+C Committee hopes to fill that gap by ensuring that the UA’s renewed commitment is highly publicized among students and that students are given the opportunity to contribute to the decision-making process.

Over the 4 hours of the event, Committee interns interacted with over one hundred people and encouraged 85 people to add their handprints to the banner. See the pie chart for a break down of handprint types. No specific goal was set in the Committee’s proposal to the Green Fund in terms of hand prints (only as experiential learners), however the Committee believes that this number of handprints was a success for the first ever Energy + Climate Committee event.

In terms of larger awareness, the Committee’s Facebook page made three posts about the banner event, which reached 1150 people according to Facebook counters. Furthermore, several media groups covered the event including:

Arizona Public Media
The Daily Wildcat
Institute of the Environment

Again, given the relatively short timeframe and newness of the Committee, this event is considered to be a great success by the Committee. In terms of experiential learners in general, this event far surpassed the Committee’s Green Fund proposal estimate (100-200 people).',
    'created_at' => '2016-05-16 22:12:22.000000',
    'project_id' => 620,
),
29 => 
array (
    'netid' => 'chois',
    'scope' => 'The Plating the Desert project worked with the UA and Tucson High ceramics classes to craft plates inspired by food and environment here in the Southwest. To start the project, we brought in six guest speakers – three local environmental artists, and three leaders in local food organizations. The students then spent the semester crafting their plates, drawing upon the background and inspiration received from the speakers. 

The plates were exhibited in the Student Union Memorial Gallery, alongside work we curated from UA and TUSD art students, local artists, the Community Food Bank of Southern Arizona. Several local food, desert, and environment books were also shown.',
    'outcomes' => '',
    'response' => 'The desired impacts listed on the original grant proposal were as follows: 

1.	Cultivate conversations about how we can understand our environment through art and vice versa
2.	Engage and promote UA students in thinking about the intersection of art, environment, and desert food
3.	Provide a community outreach experience for UA students through working with Tucson High students
a.	Create an opportunity for Tucson High and UA students to connect
4.	Engage students in reflection of the place they live in and their roles as artists/students in thinking about environment and place.
5.	Create strong UA partnerships with the community by working with Tucson High, and engaging local artist and organizations. 
6.	Create visibility for Environmental-Arts and specifically engage the UA and greater Tucson community in thinking about art, environment, and desert food. 

The measuring and reporting strategies listed on the original grant proposal were as follows: 

Quantitative: 
•	Pre/post knowledge assessments for students involved
o	Gauge students knowledge on concept of the project (environment, art, and desert foods) before project, and measure growth from after project
o	Post-project surveys collected from students and all project partners to gain feedback on the overall project
o	The committee will report on video views (collected data from youtube posting) and visitors to the exhibit
o	A report will be written by members of the Environmental Arts committee at the end of the spring 2016 semester that will include quantitative results from the assessments/surveys collected throughout the project, as well as personal reflections and best practices for modeling/expanding the project and seek other places to exhibit the plates in the future to continue the dialogue begun with this project
Qualitative:
•	The project will be recorded and documented through video. This will be a way of reporting on the project and creating an historic record for the UA, the community, and those interested in developing similar projects

The Environmental Arts committee is proud of their work on the project and feels that all of their desired impacts were met. While the committee found challenges in the survey measuring mechanism (it was difficult to elicit thorough responses on the pre- and post- survey questions from both the high school and college students; thus, no significant measurements could be made), the number of partners, students, and artists engaged was successfully quantified. The committee correlates the high number of various people and organizations engaged in the project with a successful cultivation of the art-environment conversation, and visibility of the committee. 

The following are links to publicity articles written on the project:

1.	Edible Baja Arizona
2.	UA News
3.	Tucson Weekly
4.	Daily Wildcat

The following organizations were involved:

Tucson High School; the Community Food Bank of Southern Arizona; the University of Arizona Community Garden; the University of Arizona School and Community Garden Class; the University of Arizona Ceramics cLASS

The following people were involved in some capacity throughout the project, as collaborators and/or artists: 

Aurore Chabot; Ellen McMahon; Eric Magrane; Moses Thompson; Julia Rado; Kristen Quinnan; Chet phillips; Jessie Marman; Bobby Long; Giado Gallo; Jill Bennett; Isabelle Diamond; Paul Mirocha; Molly Reed; Holly Schaffer',
    'created_at' => '2016-05-18 22:03:22.000000',
    'project_id' => 605,
),
30 => 
array (
    'netid' => 'tledbetter',
    'scope' => 'Initially proposed as Territorial Waste Wars, this project quickly hit unexpected snags that changed small aspects of the program, including the name. Less than two weeks before we were to begin the competition, ASU pulled out due to bureaucratic red tape and lack of proper backing at an administrative level. Not to have our work go to waste however, we pushed through this issue, changing the name of the project to Wildcat Waste Challenge with the major objective of the project still being a challenge to the student body to improve their recycling habits.

On February 29th, we began implementing our desired changes within the main library. We completed a full audit of all waste from the library, including both trash and recycling. From this audit, a few issues started to arise, however, we continued with the numbers in good faith of the custodial staff, finding the contamination rate of the recycling to be 64% by weight. This meant that the recycling was composed more of trash than recycling, basically rendering it all trash at this level of contamination. Following our audit, we moved to make changes within the library by completely reformatting the waste bin layout of the library. We removed nearly every small trashcan throughout the library and created a system where every trashcan was accompanied by both a blue and green recycling bin to address the issues of inconvenience and apathy toward recycling. That night we also posted flyers throughout the library relevant to the challenge itself (below) as well as advertising how and what to recycle (below). Later in the month of March we were also able to hang a banner advertising the challenge outside of the main library as well as flyers specific to recycling of Starbucks cups. We estimate that approximately 30,000 students were impacted by our efforts via social media advertising, interacting with our flyers or banner, or via our bin restructuring. In addition to our flyers, banner, and bin restructuring, interns were also stationed at a table in the lobby of the main library to directly interact with students to educate them about recycling and the challenge. Through this medium, we directly interacted with approximately 600 students at our table (based off the number of cookies and SfS stickers we gave out). 

On April 13th, we completed our end of challenge waste audit. During this audit, we discovered concrete proof of issues concerning the waste from the main library. To start the morning, we had requested that two piles of waste be made, one of trash and one of recycling. When we arrived, there was only one massive pile of bags and we were forced to eyeball to the best of our ability what was “trash” and what was “recycling.” However, as we began the audit, issues that we had seen at the February 29th audit became even more apparent. The main issue was that it was very obvious that custodial staff were combining bags containing recycling with bags containing trash into the same bag, creating basically a bag of trash. This was a common occurrence at both audits. Half way through this audit however, two custodians brought out a cart filled with bags asking us where they should put them. We asked them if they were trash or recycling to which they stated it was both. After a short exchange, the custodians told us that all custodial employees just place all trash and recycling into the same cart (and sometimes the same bag as our audits discovered) and then arbitrarily put it into the trash or recycling due to their perceived level of contamination and due to the fact that "there are lots of trash cans and lots of recycling bins" throughout the library. Despite this, we decided to finish off the audit, discovering yet again that the contamination rate was 64% by weight.

Throughout the month of March, interns throughout SfS had noted that recycling within the recycling bins appeared to be quite uncontaminated. This fact, coupled with our interaction with the custodial employees led us to complete another audit in which we pulled bags of trash and recycling ourselves so that we could determine the true contamination rate. We completed the final audit of the challenge on April 22nd and unsurprisingly found a much lower contamination rate of 32% by weight, exactly half the contamination rate that the custodial staff was creating by mixing trash and recycling.',
    'outcomes' => '',
    'response' => 'The custodial staff’s uncooperativeness is one factor that no one working on the project expected considering that we had full support from the entire administrative library staff, including the custodial supervisor. While we are unable to quantify the direct impacts we had on student recycling behaviors, we uncovered an even greater barrier to proper disposal of waste within the main library, that being our own custodial staff. We are now moving to address the issues with our custodial staff so that we can ensure proper disposal of recycling and trash from our main library. In addition to this, with the completion of the project, small trashcans have been making reappearances throughout the library, directly undoing a large component of our work. With each of these issues in mind, we fully plan to re-implement the Wildcat Waste Challenge in the coming fall and plan to address these new issues head on, while working to reinforce the overarching goals of the initial program.',
    'created_at' => '2016-05-23 20:43:22.000000',
    'project_id' => 634,
),
31 => 
array (
    'netid' => 'mmillsnovoa',
    'scope' => 'The Food Systems and Social Justice Network is a student-driven organization that leverages previous faculty efforts to develop a UA Food Systems Network. Given the growing cross-campus interest in food systems, we seek to facilitate information sharing and collaboration among food systems and social justice scientists and practitioners at the University of Arizona as well as the broader borderlands community. 

This proposed project consists of two inaugural events for an ongoing bi-annual series of TED-style talks on food systems. These events will consist of five-minute presentations from 5-7 presenters drawn from faculty, students (both undergraduate and graduate) and community members. Each speaker will provide an overview of their work relating to the specified theme, and then field questions as a panel. The question and answer portion will serve as open discussions to foster a space for conversation and build the foundation for future food system research and outreach collaboration. We envision the topics of these talks to be diverse, ranging from food security and sovereignty, climate adaptation in food systems, arid food system challenges, food justice, and methods in foodways research.  The fall theme was “Urban Food Justice” and the spring theme is "Food & Labor."

Food systems and social justice are themes being explored and researched by a broad diversity of UA faculty, students, and community members. Unfortunately, since there is currently not any Center for Food Studies on campus, many of these efforts move in isolation, and fail to generate strong interdisciplinary debates and collaborations. This proposed project will begin to fill this gap through generating important cross-campus learning opportunities on the aforementioned topics.
The inaugural year with two TED-style events for which we are requesting funds will be the first year of an ongoing series of bi-annual events and leverages UA club funding support.',
    'outcomes' => '',
    'response' => 'Proposed Desired Impacts and Outcomes

A.	The desired impact of the Food Security and Social Justice Ted Talks is foster discussion and future collaboration around food research and activism across campus and more broadly within the Tucson Community. There is a great deal of work on and off-­‐campus related to food justice, unfortunately, many of these efforts move in isolation, and fail to generate strong interdisciplinary debates and collaborations. This proposed project will begin to fill this gap through generating important cross-­‐campus and community learning and collaboration opportunities relating to food justice and social justice.
B.	Our principal strategy for achieving this is by inviting undergraduate, graduate, faculty and community presenters to discuss their work relating to selected food justice related themes. The short presentations of these invited speakers will serve as the catalyst for a conversation between panelists and the participants in the audience. It is our hope that these early conversations will begin in this event and continue via the Food Security and Social Justice listserv and through follow-­‐up events and efforts in coming years.
C.	We will have sign-­‐up sheets for all participants at the events. These individuals will be added to the Food Security and Social Justice listserv, which is a platform for announcing future work, sharing resources, and introductions. Additionally, we will offer evaluations at the end of the Ted talks so that people can provide feedback, offer suggestions and propose future Ted talk themes. Through these mechanisms we will have a sense of the impact and quality of these events.

Summary of Actual Impacts

Fall Event: On November 9, 2015, the Food Security and Social Justice Network (FSSJN) held their inaugural event titled “Urban Food Justice.”  This TED-style talk featured a four speaker panel, where each panelist had a ten minute period to discuss how they or an organization they work with understand and are addressing key issues within the broad idea of “urban food justice.” Panelists came from the Iskashitaa Refugee Network, the Bureau of Applied Research in Anthropology, the University of Arizona’s Community and School Gardening Program, and the City of Tucson’s Planning and Development Services.  Presentations were followed by a thirty minute discussion period where the audience was able to ask questions and engage in conversation with the panelists.  
The event was highly well attend, with a total of 83 audience members.  The audience include a mix of both undergraduate and graduate students.  Students in attendance came from a diverse number of majors and academic backgrounds include Urban Planning, the School of Geography and Development, Anthropology, Sustainable Built Environments, Business, Engineering, Criminal Justice, Physiology, and Soil, Water, and Environmental Sciences, among others.  There were also several students involved in Composts Cats, the Bureau of Applied Research in Anthropology, the Community and School Gardening Program and the Institute of the Environment. Additionally, community members attend from several organizations or schools including the Census Bureau, City High School, Iskashitaa Refugee Network, and Flowers & Bullets.  
As a publicly open event, which included a multitude of voices and backgrounds among both the panel and audience, this event was able to accomplish several of its intended goals.  Notably, this event was able to successfully begin building a bridge between university and community initiatives to increase urban food justice in the Tucson area by offering both opportunities for collaboration and critical ideas for improving food security more broadly.  For example, Carolyn Laurie from the City of Tucson’s Planning and Development Services, was able to highlight the importance of citizen engagement in considering how city code impacts access to urban gardening and agriculture.  Furthermore, Dr. Barbara Eiswerth discussed the opportunities for students to contribute to urban foraging efforts in addition to learning about food security from local refugees. Graduate student Morgan Apicella offered an important critical perspective on the notion of “urban food justice.”  He was able to highlight how this concept may obscure significant structural issues which drive food insecurity cycles.  He emphasized how students, community members, organizations, and the government, must address or at least understand food security and justice from a holistic perspective which addresses these wider issues.  Finally, Morgan Lundy was able to demonstrate how a university organization, such as the Bureau of Applied Research in Anthropology (BARA), is able to empower students to reach out to and work with community members.  By highlighting the collaborative work between BARA, the Community Food Bank of Southern Arizona, and Las Milpitas Urban Farm, she was able to discuss how a garden can become an important space for creating a learning environment which brings the community and academic world together. 
Ultimately, this presentation served as a space for shared ideas, where speakers and audience members were able to engage in dialogue which highlighted the importance of critical thinking and applied action in considering ways to address urban food justice.   By being open to graduate and undergraduate students, in addition to community members, it was also able to reach out to wide audience with highly divergent backgrounds, skills, and interests.  This presentation was in no way intended to “solve” urban food insecurities, but it did create a learning environment which promoted and connected the world of academia to the everyday groundwork of food justice movements.  

Spring Event: On November 9, 2015, the Food Security and Social Justice Network (FSSJN) held their second ted-talk titled “Food and Labor.”  This TED-style talk featured a four speaker panel, where each panelist had a ten minute period to discuss how they or an organization they work with understand and are addressing key issues within the broad idea of “Food and Labor.” Panelists came from the University of Arizona’s School of Public Health, University of Arizona’s School of Geography and Development, Caridad Community Kitchen, and Barrio Sustainability Project/Crossborder Food Producers Cooperative. Presentations were followed by a forty-five minute discussion period where the audience was able to ask questions and engage in conversation with the panelists.  
The event was highly well attend, with a total of 55 audience members.  The audience include a mix of both undergraduate and graduate students. Additionally, community members attended from several organizations. It was a lively conversation between our panelists which included a UA administrator (Jill de Zapien), a PhD student (Cristina Greene), a chef (Abby Rosen), and an activist (Cesar Lopez), focusing on food labor issues in Tucson and Sonora. Questions touched on issues of consumer responsibility and strategies for creating change. 

Differences between desired and actual outcomes: One shortcoming acknowledged by the FSSJN organizing committee is that while we did have a diversity of ages and backgrounds among our audience and committee members, we should strive to be inclusive to people of various cultural, racial, and ethnic backgrounds.  Beyond promoting by Listservs to students groups, the organizing committee should make an effort to more personally promote our panel.  Furthermore, the committee should strive to be conscious of culture, race, and ethnicity when choosing panel speakers.  By doing this, the organizing committee would better served FSSJN’s goal of having a diverse audience and creating important links to between various community and scholarly goals.   
In the future, I would also be to the benefit of the organizing committee to ask for an RSVP.  While it would not be required, this would allow an estimated guess at how many people will be attendance.  This is important for ordering food, because we did not have enough food at our first events for all of the attendees.
Finally, we did not carry out a post-event evaluation, which was an original goal in the grant application process.  Using either an online tool or short post-presentation survey, it would be helpful to offer an optional feedback form of some sort.  This would allow the',
    'created_at' => '2016-05-23 23:54:48.000000',
    'project_id' => 608,
),
32 => 
array (
    'netid' => 'srokuski',
    'scope' => 'We were graciously awarded funding from the Green Fund Committee to support Cooking on Campus Food Demonstrations at the spring 2016 UA Farmers’ Markets. The produce and products sold at these markets provide a source of fresh, nutritious, and seasonal food to students, and provide an opportunity to educate the students at the University of Arizona about what it means to eat locally and sustainably. Cooking on Campus hoped to make a larger impact upon the student body by showcasing cooking demonstrations during these markets. The demonstrations utilized fresh ingredients from the Farmers’ Market in order to show students how to prep and cook the ingredients being sold. In addition, Cooking on Campus demonstrations at the UA Farmers’ Market highlighted different seasonal ingredients to educate students about what was in season at the time, and how to shop at the farmer’s market and grocery store. 

The Farmers’ Market Demonstration Manager determined what ingredients and recipes would be featured at each market, which was highly dependent on vendor attendance, vendor offerings, and time of year. She engaged with administrators, staff, faculty, and students from Campus Health, ASUA, the Student Health Advocacy Committee, Students for Sustainability, the Student Union, and Farmer’s Market staff.  In addition, this student served on the Farmers’ Market Coalition board, coordinated booth staffing, was in charge of table/equipment set-up, and overall operations of the cooking demonstrations.',
    'outcomes' => '',
'response' => 'Desired impacts and outcomes (as written in our original proposal):

Student Reach = 500+ students total over the course of the spring semester 
Composting = 150+ students receive composting education 
Food Demos = 300+ students receive seasonal produce education and attend cooking demos

We planned to collect student feedback by using an online Qualtrics survey. This survey had 5 questions using a Likert Scale. This survey was created to measure how well the demonstrations increased student’s knowledge about sustainability, composting, eating in season, culinary skills, and utilizing farmer’s market ingredients. The link to the survey can be found at the following URL: https://uarizona.co1.qualtrics.com/SE/?SID=SV_bqGo9OPhZO5mIHr. 

Actual impacts: 

Student Reach= ~800 students
We exceeded our desired impact for student reach. We measured this outcomes by keeping track of how many students stopped by the booth and by how many samples were handed out. Students had the opportunity to try a sample of the recipe, were given a copy of the recipe, and if they did not get the chance to watch the recipe demonstration they were given step-by-step instructions on how the food was prepared. Each student was directed to the vendors whose ingredients were used in the recipe. They were also educated about where the ingredients came from (what state/region/farm) and about other seasonal produce items. The recipes were very simple and college-student friendly. They were plant-based recipes that were quick, easy, and healthy. 

Composting: unknown 
The original plan was to have the Cooking on Campus demo table right next to the Compost Cats table and for the Compost Cats to provide composting education. Unfortunately the Students for Sustainability/Compost Cats did not have a booth at each market due to lack of student availability/staffing. Cooking on Campus also had staffing issues throughout the semester but thankfully our Farmer’s Market Demo Manager was available for each market. At times it was difficult for her to find volunteers to help her staff the booth. 

Food Demos: ~400 students
This outcome is an estimate. It’s estimated that about half of the students who were reached actually watched the cooking demonstrations and were educated about seasonal produce. One of the unforeseen challenges of this project was gathering a large group of students to watch the demonstrations at one time. Students would approach the booth in groups of about 1-3 and did not want to wait until we could gather a greater number of students to watch the demos. In order to maintain student engagement, the project manager would prepare the recipe in smaller batches but many students would not get the chance to see the food being prepared. Instead, many students would approach the table between actual demos, take a sample, and then volunteers would explain how the sample was prepared and where they could find the ingredients.  Many students who came through the Farmer’s Market and who stopped by our booth were on their way to or from class and were unable to watch a 5-10 minute demonstration. 

Qualtrics Survey:
Before starting the project, the idea of having an online survey available at our booth seemed like the easiest and most sustainable option, however after thinking it through a little more, we realized that it would be very difficult to gather survey responses only having one computer or Ipad available after demonstrations, especially if students were crunched for time and needed to leave right after the demonstrations. Instead, we decided to print our surveys on recycled paper and hand them out after the demonstrations. Due to the unforeseen challenge outlined above in the “Food Demos” section, many students did not actually have the opportunity to watch the demonstrations and therefore, were not given the survey because the survey questions were developed to measure the impacts of the actual demonstrations. Furthermore, many students would come to the table, take a sample, listen to the information offered about the ingredients, recipe, and Cooking on Campus, and would leave before taking the time to fill out the survey. 

Although we were unable to measure the impacts of these demonstrations using the Qualtrics survey, we feel that our overall mission of educating students about what it means to eat locally and sustainably was achieved. We witnessed several students purchase produce from the produce vendor after stopping by our table and trying our recipe. These students received education on sustainably sourced ingredients and cooking techniques. They were provided with the skills necessary to go back to their dorms/apartments/homes, and implement the tools and knowledge provided through our farmer’s market kitchen demos. We also had the opportunity to spread the word about our bi-weekly Cooking on Campus classes, which many students were not aware of. 

The farmer’s market demo manager learned a great deal by coordinating this project. She was able to build and strengthen leadership, time management, and managerial skills. She also had the opportunity to collaborate with several different departments and students groups, develop a portfolio of recipes that she created herself, and build strong presentation skills. The volunteers also benefited by receiving hands-on experience in both operations and teaching roles. This included, but was not limited to cooking, recipe development, nutritional education, sustainably sourced ingredient procurement, and overall leadership experience. 

Overall, this project was a great learning experience. Because it was the very first time a project like this had been done, there was a lot of trial and error involved. If funded for similar project in the future, we plan to change the way we collect student feedback and measure impact. We will also work harder to gather larger groups of students to observe the demonstrations. Despite the unforeseen challenges, we feel that the demonstrations had a positive impact on the UA community and overtime, will increase the sustainability of the University of Arizona campus community and beyond.',
    'created_at' => '2016-05-27 18:37:34.000000',
    'project_id' => 625,
),
33 => 
array (
    'netid' => 'rwiederholt',
'scope' => 'Monarch butterflies and other species of pollinators have experienced declines in recent years.  Monarch butterflies, in particular, are a charismatic species that have a high societal and cultural value. Monarchs have had serious population losses in the last two decades, declining by about 27 million monarchs/year and reaching all-time lows during the last two winters. A main factor driving their decline is the loss of habitat and milkweed plants, which are the primary food source for monarch larvae (caterpillars). Stemming the large declines of habitat is a key restoration strategy, one that benefits not only monarchs but also other pollinators. Urban landscapes and private lands hold great potential for habitat restoration, and the goal of this proposal was to teach the community how to develop suitable habitat for butterflies and pollinators. In coordination with the Arizona Sonora Desert Museum, a Mini Grant from the UA Green Fund was used to develop a butterfly garden on the University of Arizona campus at the Udall Center (801 E 1st Street). The garden will provide habitat for butterflies, and be used as an educational tool for butterfly gardens and habitat restoration. It was also registered as a National Monarch Waystation through the Monarch Watch organization (http://www.monarchwatch.org/waystations/). This project increased the environmental sustainability of UA campus, and provided educational opportunities for students, other university members, and visitors.',
    'outcomes' => '',
    'response' => 'Desired impacts: This garden project increased environmental sustainability by providing pollinator and butterfly habitat on the University of Arizona campus. It also provided multiple training and educational opportunities for graduate and undergraduate students, university staff, and campus visitors.

Strategy: During the installation of a butterfly and pollinator garden, we included a number of educational and training components.  Before garden planting, we visited the Pollinator Garden at the Arizona-Sonoran Desert Museum to teach volunteers about habitat restoration and pollinator biology.  During the garden planting, we trained volunteers during multiple sessions.  Speakers from the School of Natural Resources and the Environment, the Watershed Management Group, and the Arizona-Sonoran Desert Museum educated volunteers on restoration and butterfly ecology, pollinator declines, sustainability, rainwater harvesting and gardening. Student interns also had learning opportunities by maintaining the garden, and organizing educational projects to train additional groups of students and volunteers. 

Realized impacts: The butterfly garden increased environmental sustainability of the UA campus by providing pollinator and butterfly habitat with nectar and milkweed sources for adult butterflies and larvae. We also planted plants that are culturally significant for Native American tribes in the garden. It also provided multiple training and educational opportunities for graduate and undergraduate students, and university staff through a series of workshops led by the School of Natural Resources and the Environment, and the Arizona-Sonoran Desert Museum. For instance, we trained students from multiple UA departments in butterfly biology, pollinator conservation status, rainwater harvesting, and habitat restoration. Students also learned how to plant pollinator/butterfly gardens, and grow milkweed and nectar plants. Finally it provides experiential learning opportunities for campus visitors, students, and UA classes.

We held multiple workshops as planned, and planted the garden in March and April 2016. We had more planting sessions than planned initially, but as a result we had a larger garden. We also had more staff members volunteer than expected in the planting sessions.',
    'created_at' => '2016-07-07 21:12:12.000000',
    'project_id' => 623,
),
34 => 
array (
    'netid' => 'jbeckelheimer',
    'scope' => 'Spring Fling serves anywhere between from 10,000 to 15,000 students in addition to 15,000 community members each year. This is why we believe Spring Fling is a great example of how compost services can be effectively incorporated into large scale events. With the support of the Green Fund, the Office of Sustainability received a mini-grant of $1,500 to support compost collection and waste diversion at Spring Fling 2016. This project helped reduce the collective impact of Spring Fling through an increase in composting while also providing a high profile opportunity for UA students to educate the Tucson and UA communities on the merits of composting.  As a result from this grant we have tripled the amount of composting since starting this project, thus reducing our waste production and increasing composting knowledge.',
    'outcomes' => '',
    'response' => 'In 2014, composting at Spring Fling resulted in a staggering 658 pounds of compostable food and material diverted from the landfill. In 2015, 1,284 pounds was diverted and an impressive 1,679 pounds was diverted this year. The total waste diversion for Spring Fling 2015 including compost and recycling was 78% and during this year we had a total diversion rate of 59.63%. In comparison to previous years, Spring Fling 2016 had a decreased diversion rate in part to Spring Fling maintenance every three years. Spring Fling disposed its old heavy mesh fence which increased the amount of landfill waste at this year’s event. In order to reduce the landfill impact, Green Team and Compost Cats members delved into the roll off bins to recycle as much mesh as they could obtain. The recycled mesh was reused for operation purposes at the Compost Cats farm. 

This mini-grant was used to provide a one-time payment to the UA Compost Cats. In exchange for these funds, Composts Cats provided compost services and educational outreach around the merit of composting for each day of Spring Fling. Compost Cats also used funds to pay usual and customary business overhead cost for gas, equipment maintenance, equipment insurance, and student employment wages. One Compost Cat intern served as lead and provided project management and logistical services during the weekend. Another 10 Compost Cats served as ambassadors and expeditors at the six primary waste stations in the food/clubs section of Spring Fling where they helped attendees identify what was compostable.  Two students coordinated the collection of compostable waste from Spring Fling as well as were responsible for the collection truck and transport of waste to the Compost Cats worksite. Another two students trained clubs, food vendors and other sustainable organizations regarding what items could and could not be composted which overall served  to substantially decrease compost contamination.  

Marketing
The UA Green Fund was recognized in social media and conventional media by Compost Cats and Spring Fling, Sustainable UA, UA Snapchat and FOX news. Many media outlets provided statements attributing the UA Green Fund as responsible for funding compost services at Spring Fling 2016. Additionally, the UA Green Fund Logo was be listed as a sponsor on the Spring Fling event banner and other promotions.

Measurement and Reporting Plan:
Waste diversion numbers specific to composting were tracked by Compost Cats in pounds for the entire event.',
    'created_at' => '2016-07-26 15:58:03.000000',
    'project_id' => 593,
),
35 => 
array (
    'netid' => 'jbeckelheimer',
'scope' => 'The goal of the Succulent Station was to reach out to as many Spring Fling attendees as possible to help them become more aware of efforts that are being made at the University of Arizona towards creating a sustainable campus as well as how to implement sustainability into their daily lives. The University of Arizona Community Garden (UACG) partnered with the Office of Sustainability to help with the project, and they excelled at informing attendees of the ongoing efforts at the University of Arizona and from the Green Fund. Every wave of attendees that approached the booth were informed of the Green Fund, specific projects that are funded by the Green Fund, and ways that they can get involved on campus sustainability.',
    'outcomes' => '',
    'response' => 'The team of UACG students that ran the booth answered questions from hundreds of people that were curious about the booth. Many people were curious as to how to care for the succulents, what was in the soil and compost, and how they could get involved with the community garden. All of these questions were answered thoroughly and everyone that received a plant had care instructions along with information on how to get involved on campus. Participants were also pleasantly surprised that the plants were complimentary through the gracious funding of the Green Gund. 

The Succulent Station was a huge hit. It was open on Spring Fling’s longest and busiest operational day,  Saturday, April 9th. From the time the carnival opened  at 11 AM attendees fled in and the UACG team spoke highly about the Green Fund, the community garden, Compost Cats, and other University of Arizona projects.

With funding, we purchased 416 succulents along with assorted pots, soil and compost (which was donated from Compost Cats). At 3 PM we had reached that same number of people and had zero succulents left. Over 400 recipients , including family members and friends were informed of the Green Fund and sustainability related projects on campus.

Much of the response from attendees was consistent. Most University of Arizona students were unaware of the Green Fund and that part of their tuition goes towards supporting the efforts of the Green Fund. Many attendees were curious as to how to have a project funded, and they were notified that students, faculty and staff are able to submit a proposal for potential funding. After planting a succulent and being informed of the Green Fund, attendees were given a handout that included information about how to get involved on campus, direct access to the Green Fund and Compost Cats website, along with more information about projects on campus and how to care for their new succulent.

The impact that this project made was a great start to bringing more awareness to the Green Fund. The Project members of the Succulent Station thank the Green Fund for their generosity, and we hope to continue the efforts of increasing awareness of the Green Fund across campus.',
    'created_at' => '2016-07-26 16:05:44.000000',
    'project_id' => 640,
),
36 => 
array (
    'netid' => 'jbeckelheimer',
    'scope' => 'UA Parking and Transportation Services was paid for use of pedestrian rail barriers and bicycle racks. These were placed just west of the Spring Fling entrance at the northwest corner of Cherry and University. The Bike Valet was staffed by The University of Arizona Cycling club. The club received a stipend to help pay for club expenses. The club staffed the valet for 26 hours from Friday to Sunday; each shift had 2 to 3 valets on duty. For participants who collected their bike in the evening, we provided free bike lights for those who didn’t have one. This served two purposes – first, to ensure the safety of those leaving spring fling and riding around our community; and secondly, to alleviate any liability concerns related to night time bike light laws.',
    'outcomes' => '',
    'response' => 'The marketing budget was used to purchase high-quality, weatherproof Bike Valet direction posters that can be reused for future events as well. Signage was placed on Cherry St. both north and south of the mall, as well as in front of the bike path between the SUMC and the Administration building. Anecdotal evidence suggested these signs were helpful in both directing people to the bike valet as well as notifying people who were unaware there was a convenient place to leave their bike. 

The Green Team designed and administered an iPad-based survey to collect vital data on participants and the bike valet program. This year, we served over 140 people over the course of three days – almost double the figure from last year (80 participants). Saturday was the most popular day, with 44 percent of all participants utilized the valet that day. 

Lessons Learned
At some shifts, there was a lack of clarity in terms surrounding the process the volunteers were supposed to follow. In the future, these concerns can be alleviated by providing a laminated sheet, taped to the table, that outlines a step-by-step process for the volunteers to follow during bike drop off and retrieval. 
At several points throughout the weekend, there was an aversion among participants to taking the iPad survey. There were also some technological difficulties with the iPad. Next year, a paper survey could be administered. It may be quicker, requires no technology training, and may be easier for participants to submit answers.

Overall, the bike valet program helped empower individuals to use an alternative mode of transportation to and from Spring Fling, while also lowering the overall carbon footprint of the event. The event provided great community-wide visibility for the Office of Sustainability, the Green Fund, and sustainable initiatives at the university.',
    'created_at' => '2016-07-26 16:10:02.000000',
    'project_id' => 641,
),
37 => 
array (
    'netid' => 'hhughes',
    'scope' => 'The  Bike Repurposing Project received 30 worn out bikes from Parking and Transportation. Student athletes were to disassemble, clean, and sand the bikes. It was originally intended that the students would also paint the bikes, however, UA Risk Management determined the activity too hazardous for students. Facilities Management was kind enough to paint the bikes and at the time of this writing, they are ready to be reassembled. The student athletics will reassemble the bikes with the help of the UA Cycling Team before the fall semester starts. The project is running behind schedule and had two roadblocks to overcome. The number of hours students could devote to the project was more limited than anticipated. Also, due to limited funding, the project could not hire a student lead to move the project forward and this also caused the project to fall behind schedule. Student athletes are currently working on 5 bikes and will work more this summer. Students will complete the bike rehab and then take the bikes to local elementary and middle schools where they will teach proper bicycle safety as well as donate some of the bikes. Some bikes will also remain on campus for use by student athletes and UA staff.',
    'outcomes' => '',
    'response' => 'The student athletes who worked on the bike repair process enjoyed this project because they felt engaged in the process, looked forward to sharing the bike rehab process with younger students, and learned skills they felt could serve them later in life.  Student athletes from the UA baseball and swim teams contributed 50 hours cumulatively on these bikes with students contributing between 1-5 hours individually.  Some of the rehabbed bikes will be donated to Tucson elementary and middle schools.  The athletic program is considering making it a part of their regular summer curriculum.. This project will continue to benefit UA student Athletes by providing them with an engaging way to complete community service hours while continuing to learn about sustainability through health and fitness as a life goal.
The biggest reason our actual number of bikes fell below our desired number was because we underestimated how often student athletes would be available to work.  Overall what made this mini grant successful was that it was an effective way to learn through trial and error what needs to be improved on in the project while simultaneously being impressive enough to get supported by UA athletics.  Hopefully this project will continue to grow relationships between the Athletics, Facilities Management, Parking and Transportation and the Office of Sustainability.',
    'created_at' => '2016-07-27 16:20:49.000000',
    'project_id' => 610,
),
38 => 
array (
    'netid' => 'samanthaorwoll',
    'scope' => 'The Move-Out Bins Project aimed to increase diversion rates during campus move out, engage students, and allow new students to save money by purchasing the collected items. The mini grant purchased boxes and other supplies for student employees to use to collect and sort discarded materials from four residence halls. To help ensure the items ended up in the collection bins, student employees were present by the dumpsters outside of the halls and helped promote the project by explaining its purpose. The UA Surplus Store stored the collected items and student employees worked all summer to clean, test, inventory, and tag the items in preparation for the sale to be held on August 19th and 20th, 2016.',
    'outcomes' => '',
    'response' => 'One measurement of the project is the total amount of materials diverted from the landfill. Additionally, the project saved energy and resources that would be used to produce the many items UA students bring to campus such as chairs, plastic storage tubs, and refrigerators. A possible future measurement it to track what is sold at the surplus sale. Another measure is if the money made from the sale will be enough to pay for student wages, which would make the project self-sufficient in future years. 
Many of the impacts have already been measured. Most notably is the 9.108 tons of materials that were diverted from the landfill. The project also helped the Campus Pantry to collect .512 tons of food that might have otherwise been discarded. To make this happen, 27 new student workers were hired in addition to the 4 Green Team and 2 Green Purchasing workers. Throughout the project, these student workers engaged with 1,636 students. These experiential learners were shown that there are other options besides landfill and even if they are finished with their items, someone else may be able to use them. Although the sale hasn’t occurred (so those numbers can’t be calculated), all of the people who are a part of it will also see how sustainability can actually save money. The sale will provided a way for students to help other students and reduce waste. New students will be able to purchase the same items they would have anyway for a reduced cost, which puts sustainability and reusing products in a positive light. It also demonstrates that what’s best for the environment can be cheaper too. New students will be exposed to sustainable purchasing from the very beginning of the semester and this will help foster a sustainable mindset in the future. It may increase their interest in other sustainable efforts on campus. There has been considerable press on both the collection period and the sale, as the project was featured on UA News. This also increases the number of people impacted by the project. 
We predicted 1,641 experiential learners, which was the number of students in the dorms. We came within 5 of that. Because there was a student worker near the dumpster for most of the day, we were able to catch most students discarding materials. Also, the student workers took a very proactive approach and sorted many of the materials themselves, which significantly reduced the amount thrown into the landfill. Additionally, the student workers wore the green Zero Waste vests. This is useful in these type of projects because the student workers are easy to identify and it also shows they are working on a specific project. The project provided a very visible example of sustainability efforts on campus while also showing how sustainability can save money. Finally, it gives students a way to give back to others in a way where they can clearly see the positive impact for other UA students.',
    'created_at' => '2016-08-01 23:39:28.000000',
    'project_id' => 637,
),
39 => 
array (
    'netid' => 'uafrank0',
'scope' => 'If this proposal is approved, funding will be used for Dr. Ed Franklin, Department of Agricultural Education to attend the Solar Schoolhouse (Rahus Institute) in Sebastopol, California July 23-30, 2016. The Institute targets educators in grades 3-12. Teachers attending the summer seminar learn about the science and history of heating, cooling, and powering our homes with the Sun. Participants will build solar cookers, model passive solar homes, solar electric fountains, custom solar panels, Portable Solar Power Station (lunchbox), and more. Cost for the workshop is $1,500.00 and includes instruction, lodging, meals, and materials. Dr. Franklin is a teacher-educator in the Department of Agricultural Education. In addition to preparing UA students to be high school and community college agriculture instructors, he conducts outreach activities for 4-H leaders, FFA advisors & teachers, and cooperative extension personnel. Currently, Dr. Franklin has adopted the solar fountain activity from “Teaching Solar” (Rahus Institute) for hands-on lab activity and outreach training. Dr. Franklin will take the information and tools gained from this workshop and apply it directly to training and helping UA students, and Arizona agriculture science teacher’s to develop solar energy lessons for their classrooms, and for Cooperative Extension personnel (4-H leaders) prepare presentations to 4-H members. Dr. Franklin recently graduated from the Solar Professionals Certificate Program (SPCP) with Solar Energy International (SEI) an international renewable energy education organization with over 30,000 alumni. This training has focused on grid-direct PV systems, battery-based systems, solar instructor training, and solar technical sales and included the completion of a combination of online courses and week-long laboratory workshops and training seminars.  This training will focus on preparing school educators working with elementary and high school age students. Dr. Franklin will utilize other finance sources (Department of Agricultural Education)  for roundtrip airline travel from Tucson, AZ. to Sebastopol, CA. Flights are approximately $500.00 (Tucson to Santa Rosa, CA. or Sacramento, CA.) and secure rental car for ground transportation to the Rahus Institute.',
    'outcomes' => '',
    'response' => 'Attending the Institute provided me an opportunity to interact with fellow Institute attendees --- who are K-12 teachers -- learning about solar energy fundamentals, and how to use solar-related activities in their own classrooms. My inquiry is how teachers perceived the effectiveness of the Institute to provide teachers with knowledge and skills to teach solar energy and use the activities with students. I learned from the teachers which activities they were likely to adopt and use right away in the classrooms, and which activities they would want to learn about if attending a professional development workshop. 

Solar teaching materials created (solar notebook charger, solar whirligig, solar oven), and solar teacher materials provided by the Institute (solar classroom set, solar fountain set, solar reference materials, etc.) will be used in our AGTM laboratory courses beginning in fall 2016 to demonstrate solar energy fundamentals. Additionally, materials will be incorporated into our Solar Energy Education Outreach program. To date, our solar energy education outreach audiences have included middle school students, high school students, UA undergraduate students in ABE 201 engineering course, UA undergraduate students in AGTM courses, high school students participating in College of Engineering Summer Academies, adult beginning farmer groups, high school teachers, Project WET staff and volunteers, and Cooperative Extension agents.',
    'created_at' => '2016-08-10 16:29:33.000000',
    'project_id' => 597,
),
40 => 
array (
    'netid' => 'rudnick1',
'scope' => 'The Office of Sustainability requested a mini-grant of $1500 to provide recycling services during Spring Fling 2016. Funds were used to pay an ASUA recognized club, Greening the Game, signage fees, and UA Facilities Management (FM) fees associated with recycling at Spring Fling 2016. 

With funding, the project accomplish its goals of increasing waste diversion and providing outreach in several ways. First, funding covered the cost of having Greening the Game workers interact with over 50 clubs and vendors of Spring Fling to know what items are recyclable and how clubs/vendors can engage with attendees to ensure that their materials are disposed of properly in ways that are appropriate for the occasion. In addition to covering engagement, funding will cover the cost of having paired recycling and waste bins at Spring Fling as provided by FM. The funding will also cover service fees, roll off charges, hauling and employee wages as specified by Facilities Management.',
    'outcomes' => '',
'response' => 'Greening the Game (GtG) workers interacted with over 50 clubs and vendors at Spring Fling demonstrating what items were recyclable and compostable. GtG worked with clubs/vendors and engaged with attendees to ensure that their materials were disposed of properly in ways that were appropriate for the occasion. In addition to covering engagement, the funding covered the cost of having recycle roll offs at Spring Fling. This is not something that would be done if GtG could not work this event. 

Workers and the general public seemed to have had an adequate handle on what materials were recyclable creating an environment that ran smoothly. Sorting done periodically throughout the Spring Fling weekend helped achieve diversion rate goals. 

Recycling 		Diversion Rate
2014: 2.55 tons	59.3%
2015: 6.87 tons	78%
2016: 1.25 tons	56.93%',
    'created_at' => '2016-08-10 22:30:15.000000',
    'project_id' => 591,
),
41 => 
array (
    'netid' => 'auroras',
'scope' => 'ASUA FORCE (Feminists Organized to Resist, Create, and Empower) and ASUA SfS (Students for Sustainability) conducted an initiative that involved students sewing their own reusable pads out of hemp fabrics, as hemp is very durable, absorbent, and environmentally friendly to grow. Convening at monthly “Stitch n’ Bitch” sessions in the Women’s Resource Center, FORCE and SfS interns provided participants with the materials and knowledge needed to sew their own reusable pads. These lively and widely advertised crafting sessions introduced approximately 50 students to a viable alternative to disposable pads. By combining a fun, social atmosphere with ecological awareness, “Stitch n’ Bitch” helped to popularize and normalize a collective environmental consciousness, contributing to the development of a community of students who make a habit of questioning the environmental cost of expendable products.',
    'outcomes' => '',
    'response' => 'Desired impacts and outcomes: 
Our effort aims to make our campus’ understanding of sustainability more inclusive. Because menstruation is a highly stigmatized subject, it is important to our organizations to work to de-stigmatize menstruation and foster a culture that understands and values sustainability as it pertains to many different people.
This unique program centered on sustainability will strengthen the UA’s campus environmental and feminist efforts, and possibly create a long lasting reusable pads program and collaboration between the Green Fund, FORCE, and SfS. Such a program will shift sustainable efforts at the University to incorporate more issues of gender and class.
FORCE’s and SfS’s plan to market through the use of environmentally friendly menstrual products will work to strengthen FORCE’s Feminist Pharmacy service. The pre-made reusable pads and the menstrual cups will be purchased through a company called Lunapads, which connects female student in Uganda with a free reusable pad for every menstrual pad and cup that is purchased, thus extending our sustainable impact internationally. We believe that providing pre-made reusable menstrual pads with marketing materials on them will introduce many students on campus to a more ecologically-aware lifestyle and break the ice for many students thinking about coming to the events. As well, they will incorporate students who perhaps don’t like to sew into this sustainable effort.
By hand-sewing the pads, using hemp fabrics, and recycling unused fabric at the end of the program, we hope to make the smallest environmental footprint possible. We chose hemp because it requires no pesticides, little water, and does not negatively impact the land it is grown on, making it very sustainable. Each reusable pad sewn and each environmentally friendly product distributed through the Feminist Pharmacy will reduce UA students’ personal environmental impacts.

Measuring and Reporting:
The number of participants and how many pads they sew are important in quantitatively analyzing the impact. After we have the number of participants/pads we can see how much it costs to provide each student with a reusable pad and how that compares to the average cost of disposable menstrual products per year.


Actual impacts: 
Over the course of four events, approximately 55 students were directly supported by this project, and at least 80 pads were sewn. Our project encouraged students to question the environmental cost of expendable products and we did so with a feminist bent in order to decrease the stigma attached to menstruation by openly discussing menstrual justice. Unfortunately, we do not have access to precise data regarding a reduction in the amount of waste on campus. Just through personal conversations, we do know for certain that several students, reported satisfaction with the reusable pads. This project, therefore, did reduce waste.
Many students expressed their thanks for the sewing, environmental, and feminist education. The project leaders feel confident that the project mobilized feminist and environmental goals through open discussion, and intend to re-implement the project during the next academic year.

Differences between anticipated and actual outcomes: 
After our first very successful event, we did experience a lower turnout than expected. Despite adjusting the format of our subsequent event to include small group discussions for those not interested in sewing, the numbers of participants continued to decline. This could be due to students becoming increasingly busy as the semester wore on, and therefore unable to attend after-hours events. Other possibilities include insufficient publicity, or perhaps we had underestimated the extent to which the stigmatization of menstruation (the very problem this event was trying to combat) would prevent students from attending the event. It’s possible, of course, that all of these factors contributed to lower numbers of attendees.   
Another way we diverged from our initial plan was our decision not to furnish the Feminist Pharmacy with reusable pads. We made this decision because of the low attendance at our Stitch-n-Bitch events. Based on feedback from the general feminist community and trends we’d observed on a national scale, we decided to fill our fourth and final event with a discussion and presentation on menstrual cups along with a sewing session. Similar to menstrual pads, menstrual cups significantly reduce waste. All students in attendance received a free menstrual cup, which was covered by the FORCE ASUA budget. Training and marketing was completed as planned. This event was more widely attended and very well-received.',
    'created_at' => '2016-08-23 22:38:02.000000',
    'project_id' => 622,
),
42 => 
array (
    'netid' => 'uafrank0',
'scope' => 'In October 2016,  I met with nine high school agriculture instructors at a meeting to discuss and determine the activities and skills to include in the 2017 State Agricultural Mechanics Career Development Event (CDE) which will be conducted at the University of Arizona Campus Agricultural Center on Friday March 3, 2017. Twenty-five high school agricultural mechanics CDE teams will qualify to compete in the State activity. All teams must compete at their District-level event before qualifying for the State-level event. There are nine Districts in Arizona. I proposed to the teachers present about a solar-powered water pumping demonstration activity for the Team Activity. We have the necessary materials and submitted a mini-grant funding application to seek additional materials. 

The teachers agreed to the suggestion of the activity. However, it was noted that teachers and local schools hosting the District-Level events do not have the necessary materials to conduct the Team Activity at their local sites and requested I be able come to each district event and facilitate the Team Activity. Each District event is conducted during the month of February. Approximately four to nine teams compete at each District-level event.',
    'outcomes' => '',
'response' => 'To date, reference materials to assist teachers and coaches to prepare their students for the activity have been collected and emailed to Arizona FFA for posting on the state website for all teachers to access. Materials include solar module information, 12 volt DC pump information, digital multimeter operators manual, a solar fountain reference provided by The Rahus Institute, and a list of skills each team will be expected to be able to perform. These skills include: 1. Reading and interpreting a solar module spec sheet; 2. Measuring open-circuit voltage; 3. Measuring short-circuit current; 4. Connecting multiple modules in series; 5. Connecting multiple modules in parallel; 6. Measuring current and voltage under load; 7. Assembling and operating a solar fountain; 8. Calculating system power (watts) output. 

A pre/post activity assessment is being developed to gather data on student perceptions of the activity. A research proposal is being prepared and submitted to UA Human Subjects Protection for consideration and approval.

A graduate student is being recruited to assist with this activity.',
    'created_at' => '2016-10-25 15:33:57.000000',
    'project_id' => 644,
),
43 => 
array (
    'netid' => 'uafrank0',
    'scope' => 'Funds were used to acquire tools, solar modules, non-contact thermometers, angle & pitch finders, PV cable, five-gallon buckets, and PVC pipe materials. UA students enrolled in the AGTM 330 course assembled the materials for 25 systems.  During a lab session, AGTM 330 students setup and tested the systems with materials we had on hand in our solar PV teaching inventory. Students in both the AGTM 330 and AGTM 350 courses set up the outside physical area for the event, assisted with contestant check-in, and served as event judges.',
    'outcomes' => '',
'response' => 'We assembled 25 solar water pumping systems complete with a 12 volt DC pump, tubing, two solar modules (per system 50 total), digital multimeters, non-contact thermometers, angle & pitch finders, and pre-cut & drilled PVC pipe and components. During the month of February, eight District-level FFA Agriculture Mechanics Career Development Events (CDE) were conducted. Teachers facilitating events contacted the Project Manager (Dr. Edward Franklin) to arrange to borrow materials. Materials were sent to four different District events for students to use in their competition. These were scheduled at a time when the project manager was unable to attend. The other four District events the Project Manager did attend and conducted face-to-face instruction and demonstration with the high school contestants. These events were conducted at Arizona Western College (Yuma), Central Arizona College (Casa Grande), Arizona State University Poly-Tech Campus (Mesa), and Round Valley HS (Springerville). Number of teams varied from four to eight at each event.

During the state-level event hosted by the Department of Agricultural Education at the UA Campus Agricultural Center, we hosted 25 teams of four students (100 contestants). Teams provided their own tools to measure, cut, and drill PVC pipe. Each team assembled a solar fountain, wired it to a 12 volt pump, and connected the fountain to a single solar module, took energy measurements (short-circuit current, open-circuit voltage, operational current, solar cell temperature, and optimum module tilt angle) with provided tools.

Total number of on-UA students impacted by this project: 100',
    'created_at' => '2017-03-19 19:17:48.000000',
    'project_id' => 644,
),
44 => 
array (
    'netid' => 'chois',
    'scope' => 'The Women in Green Leadership Panel Series is an initiative from SFS and FORCE. Already aligned in the mission of empowering students to effectively organize for change on campus and in the community, SFS and FORCE found that both their organizations are also aligned in the specific goal of empowering more women into “green” jobs that work for economic, environmental, and social justice; jobs that transform our current economic, social, and environmental systems into ones that create sustainable development of the world. 

In addressing the global challenge of climate change, the United Nations has recognized that “women are more vulnerable to the effects of climate change than men – primarily as they constitute the majority of the world’s poor and are more dependent for their livelihood on natural resources that are threatened by climate change…and face social economic, and political barriers that limit their coping capacity”.  Empowering women around the world is crucial to sustaining natural resources, adapting to climate change, achieving more access to clean water and sanitation, and ultimately, bring more people from poverty to prosperity. 

As a world-renowned research and public land-grant institution, the University of Arizona shapes students to think globally about the big visions of the UN’s Millennium Development goal, and empowers them to act locally through 100% engagement programs like SFS and FORCE. The Women in Green Leadership Panel Series will be a monthly panel over November – April (excluding the month of January) consisting of all female panelists from different disciplines, working in various roles across the environment and sustainability field. The students managing the project within SFS and FORCE will select three panelists for each month and organize the panel events, which will take place in the WRC space on the 4th floor of the Student Union. Each month will be focused on a general theme and sector of the field; a projected vision of each month’s theme, though subject to change, is as follows: 

November - Government/politics 
December- Business/private sector
February- Community organizers and non-profit organizations
March- Independent artists and activists
April- Education/ STEM fields 

The panel events will occur each first Friday of each respective month, from 9-10:30am. University of Arizona students will be specifically targeted in marketing and advertising, but the events will be open to the public. The goal of this initiative is for students to recognize the crucial intersection of people, planet, and prosperity; and, to inspire action towards more economic, social, and environmental justice on campus, in the Tucson community, and around the world.',
    'outcomes' => '',
    'response' => '1.	Cultivate a strong partnership between SFS and FORCE to lay a collaborative foundation for current and future students in each respective program. 

This program allowed us to partner with FORCE very directly and 6 students from FORCE and SFS worked collaboratively all year together to plan these events.  

2.	Educate the UA campus and Tucson community on the intersection of people, planet, and prosperity through the specific goal of women’s empowerment and sustainability.

All of the 15 female speakers we had throughout the year educated and empowered each other, and the audience through their work and perspectives on their work. Overall, approximately 200 students were reached.  

4. 	Inspire action for more projects, programs, and initiative that work for economic, social, and environmental sustainability – on our campus, in Tucson, and the around the globe.

Many of the speakers on our panels are apart of organizations running amazing programs, such as nutrition and gardening programs with refugees, school gardens, and divestment campaigns. They inspired the audience on the work they do, and many students followed up with the speakers on ways they can get involved. 

5.   Increase the presence and awareness of environmental justice on the UA campus.

Bringing women who work in community organizations and in environmental justice activism to campus naturally increases the presence of voices for environmental justice on campus. Having the women speak about their experiences means a lot to both our campus and to them, and their organizations.',
    'created_at' => '2017-04-30 00:08:53.000000',
    'project_id' => 647,
),
45 => 
array (
    'netid' => 'elg1',
'scope' => 'El Día del Agua y la Atmósfera (El Día) is a student symposium that features presentations and discussion related to climate change in the Southwest. Through this conference, we hope to provide a forum through which student presenters can engage with one another and the community to discuss the problem of climate change and its effects on the Southwest US. El Día is sponsored by the Department of Hydrology and Atmospheric Sciences (HAS) and is run by the HAS Student Association (HASSA).

This year, El Día was on March 27, 2017 SUMC Grand Ballroom. El Día featured student oral and poster presentations that address climate change and its impacts on weather patterns and hydrologic processes in the Southwest. El Día also included a roundtable discussion between students, faculty, and professional attendees. This year’s roundtable topic focused on the theme of communicating science: do we as scientists need to do anything differently to communicate our knowledge to decision makers and the general public? 

Our goal is that this conference encourages discussion and future collaborative research related to climate change within our department. We intend for this conference to allow university students to present their research to the public, including local hydrology and meteorology firms, so that they can gain insight on local impacts of climate change and use it to make informed decisions in the future.',
    'outcomes' => '',
'response' => 'This year\'s El Dia was a huge success, in part due to the Green Fund\'s contribution and to the hard work of the organizing committee. This year\'s roundtable prompt ("Do we as scientists need to do anything differently to communicate our knowledge to decision makers and the general public?") was echoed by the luncheon speaker\'s address (delivered by Amber Sullins, ABC Phoenix cheif meteorologist, on communicating climate change to the public in non-polarizing ways) and by the keynote address (delivered by climate research scientist Scott Denning, on effective communication to sometimes hostile crowds).

The roundtables themselves were logistically much improved from last year\'s arrangement. The roundtable had its own allotted time and did not overlap with poster or oral presentations, so most conference attendees were present at one of the four mini roundtables. Each table was led by two HAS department faculty and attended by multiple students. Discussion ranged from the meaning of "trans-disciplinary" to the shifting landscape of funding to changing public perception on the importance of scince in our daily lives, and more. Notes from the discussions have been compiled and will be made available on the HAS department web page. They will also be dispersed to the HAS students via email.',
    'created_at' => '2017-05-03 19:43:55.000000',
    'project_id' => 692,
),
46 => 
array (
    'netid' => 'erinscott',
    'scope' => 'The Highland Cistern Mural is a community oriented project aimed at bringing individuals into the community garden space to meet each other, learn about rainwater harvesting, and have fun. The mural will be executed by the public and Students for Sustainability in a public event using a paint-by-numbers system, so anyone regardless of their artistic ability can join in. This is an all inclusive painting event, welcoming both families, students, and Tucson locals.
The mural design will feature a desert scene with native flora and fauna, and will be painted by community members during an event day (See figures 1 & 2.) Above the desert scene we will paint “UA Community Garden” to increase visibility from the street and promote use of the garden to local communities. Environmental Arts would like to leave a large part of the cistern unpainted, and come back annually to have Students for Sustainability fill the rest of the space with different designs over time. This will leave room for further community engagement in the garden space, and more themes for Students for Sustainability to have conversations with the public. 
During the event, members of Students for Sustainability will engage with the public to highlight the importance rain water harvesting in Arizona. Students will hold conversations educating the public of rainwater harvesting cisterns, as well as native drought tolerant plants, and how the two impact the natural ecosystems in Arizona. Using these ideas, the community will have a better understanding of how precious water is in the desert and how they can help contribute to water conservation by utilizing rainwater harvesting and native plants in landscaping.',
    'outcomes' => '',
    'response' => '-Involve community members of both UA and Tucson in the Community Garden space.
- Encourage the public to actively think about rainwater harvesting and conservation through artistic expression.
- Inspire people to think freely about art and the environment 

I wish that SFS\'s water committee was able to attended to add more depth to our conversation on water conservation. In the future I would like to partner with more groups to give a wider view of the particular topic we choose for future mural projects.',
    'created_at' => '2017-05-11 01:19:22.000000',
    'project_id' => 683,
),
47 => 
array (
    'netid' => 'meganmchugh',
    'scope' => 'Based on the proposal timeline, the project progressed as planned. A solar Stirling engine was constructed over the course of the school year at the same time as the research paper on the subject was being written. The paper was then summarized in a poster and both that and the completed engine were presented at the SBE senior capstone showcase in May 2017. Two presentations were given on the UA campus, and the solar Stirling engine remaining in the AME department for further research along with the poster and thesis uploaded to the UA library repository means there is potential to continue influencing students as originally intended by the project. By repurposing scrap metal materials in the AME department machine shop, the construction of the engine came out to be less than anticipated in the budget spending only $494.99 of the $900 awarded in funds. The only change from what was mentioned in the proposal is that the UA ASME Stirling Engine Interest Group has not yet put together the design competition for high schoolers so it could not be demonstrated to them this year. However, members of the club have access to the engine through Dr. Cho Lik Chan and will be able to present research to them at the appropriate time.',
    'outcomes' => '',
'response' => 'The solar powered Stirling engine project provided education and outreach initiatives related to UA environmental sustainability. It generated interest in the ASME club towards both Stirling engines and the concept of alternative energy technologies of the future that are not the standard ideas of renewable energy that exist today. Solar Stirling engines are more efficient in energy production (26%) than photovoltaic panels (16%) that use rare earth materials (such as indium) in their construction, yet do not receive the same amount of attention by the renewable energy industry or environmentalists due to the lack of scientific research and data available to make these technologies viable. Increasing the awareness of engineers and people already invested in sustainability on these alternative methods of generating energy in sustainable ways is important in clarifying the direction renewable technology development takes. Although a limited number of students and faculty were able to experience the presentation of the solar powered Stirling engine so far, it has led to a network of students in the ASME club who will carry on the research and continue pushing sustainable technologies forward.
Desired impacts and outcomes of the project were accomplished as outlined above. However, further optimization of the solar Stirling engine remains to be done. Although fully constructed under budget, the engine runs into efficiency problems and alignment issues due to the low tolerances of the design. More time is necessary to do the research and run the experiments to make a better solar Stirling engine, but scientific research always builds on what has already been done so there will always be more to work to be done for the project. Dr. Chan’s influence as a mentor to many students will continue and result in the advancement of the solar Stirling engine project beyond the original scope. Megan McHugh will be continuing research on similar topics such as building energy consumption/reduction calculations and automation systems (such as the Stirling engine) for her Sustainable Systems engineering graduate degree at the University of Texas at Austin.',
    'created_at' => '2017-05-11 19:04:54.000000',
    'project_id' => 660,
),
48 => 
array (
    'netid' => 'imollaneda',
'scope' => 'The title of the project is Land and Liberation: Cultivating Connections and Solidarity in the Garden. Gardening and growing one’s own food is a concept that is often lost in today’s world of fast food and grocery stores. This project envisions a space where societally marginalized student communities (and the broader campus community) can not only take part in growing food, but take part in a movement of making and fostering connections between themselves, their culture, their land, and their food. The six cultural and resource centers on campus will all be invited to plant, tend to, and eventually, harvest crops of their choosing at the UA Community Garden. 

The goal of the project is threefold: first, to create space for dialogue around food and its importance to identity; second, to make the UA Community Garden more of an accessible and available place for students who frequent any resource or cultural center; third, to create and maintain meaningful connections among the centers and ultimately promote solidarity among diverse identities. The plan for the project would be to have a kick-off event in February in which students from all centers can bring any food item, there would be a brief discussion on the goals and structure of the project over food, and then a workday would ensue. The centers would be invited to publicize their plot within their centers so as to increase student participation. They would also be invited to have a team of people to coordinate center-specific programming in the garden, potentially using the pre-existing garden workdays as a platform. The project will culminate in an end of the year potluck in which centers would use their harvest to create a dish to bring to the garden and celebrate land, food, and solidarity.',
    'outcomes' => '',
    'response' => 'Through programming such as Solidarity in the Garden, GRAPES, Stories in the Garden, and garden workdays, the cultural centers were exposed to the UA Community Garden and gardening in general. Though there could have been more participation from all the centers, the project reached over 50 students in all. Money from the grant was used to purchase an extension to one of the plots so that more programming can occur over the next school year.',
    'created_at' => '2017-05-30 01:20:53.000000',
    'project_id' => 664,
),
49 => 
array (
    'netid' => 'alambert',
    'scope' => 'After the successful launch of Plating the Desert, which combined ceramics and the topic of food waste, the Environmental Arts Committee focuses on a new topic, water. Following a similar path and working with both last project’s faculty and new teachers at Tucson High, they will collaborate to create a new exhibit around sustainability and water, focused on the Southwest. 

As the main theme revolves around water, our vision for this project is to engage students with the idea of using their talent and minds to create art that teaches about water and advocates for water sustainability. Students will create pieces in mediums ranging from painting to ceramics, and each piece will explore the crucial bond between life and water. Examples will include illustrations inspired by water, photographs depicting the process of how water is stored and used in the southwest, and ceramic sculptures influenced by local environmental artists. Environmental science topics around water, such as aquifers and the water cycle, can be made more accessible to the public through art. The students will be encouraged to focus on one or more scientific topics of water, and use their art to explain and/or enhance understanding of that topic. Ultimately, the final exhibit will explain the state of water in the Southwest through art, and emphasize the importance of sustainability. 

The project will collaborate with 4-5 guest speakers - both artists and hydrologists - who will work together to inspire and guide the students in making their artwork. Speakers will both discuss and demonstrate their work for the students. For example, an artist will demonstrate an art technique, and a scientist may engage in modeling a hydrologic process. Potential guest speakers include Bobby Long, Gregg Garfin, Kathleen Velo, Jonathan Marquis, Holly Hilburn, and Alison Hawthorne-Deming. Speakers will be contacted in early October.

The exhibit will be in the Student Union Memorial Gallery for two weeks in February 2017. This exposure of the student’s work will be quite beneficial to the University to showcase work not normally featured in the space. It will also give viewers a new perspective on environmental topics through art which can connect to the individual and start a conversation on these crucial topics. The project leaders are currently looking into other exhibit spaces that the pieces could travel to as well.',
    'outcomes' => '',
    'response' => 'Desired impacts include inspiring university and high school students in thinking about environment and art, encourage conversations between communities to collaborate together, pursue important environmental issues using art as an education medium while expressing creativity, engage Students for Sustainability, Environmental Arts Committee with communities to create strong relationships for future projects and encourage Tucson High students can work with university level professors and have opportunities outside of Tucson High to expand their knowledge. 

After the project was executed we did complete our desired impacts. By working with both Tucson High and the University of Arizona, two groups of students were included in this project and collaborated with the final exhibit. Some benefits from this project was the exposure of our committee Environmental Arts and Students for Sustainability. Promoting the interdisciplinary practices of art and sustainability was highlighted throughout and displayed for the Tucson community to get engaged. 

Projects like this encourage the community to come together and provide education and exposure to environmental concerns like water scarcity. Art helps create a space where these important topics are easier and more inviting to understand vs a strictly science based project.',
    'created_at' => '2017-06-12 20:10:25.000000',
    'project_id' => 646,
),
50 => 
array (
    'netid' => 'mmillsnovoa',
'scope' => 'Given the growing cross-campus interest in food systems, the Food Security and Social Justice Network facilitates information sharing and collaboration among food systems scholars and practitioners at the University of Arizona and the broader borderlands community. This proposed project consists of two components: 1) the renewal of the highly successful biannual series of TED-style talks on food systems and 2) an inaugural food justice short course for undergraduate students.

Component 1: The TED-style talks consist of ten-minute presentations from 4-5 presenters drawn from faculty, students (both undergraduate and graduate) and community members. Each speaker will provide an overview of their work relating to the specified theme, and then field questions as a panel. The question and answer portion will serve as open discussions to foster a space for conversation and build the foundation for future food system research and outreach collaboration. Last year our events drew together nearly 300 undergraduates, graduate students, faculty, and community members. The fall semester theme this year is “Food Sovereignty” and the spring semester theme is “Community Food Organizing.” 

Component 2: As graduate students engaged in food systems research, we propose an
exploratory two-day short course with undergraduates focused on building knowledge of social science methods relevant to food systems like mapping, participant action research, and ethnography. The first day of the short course, “Introduction to Critical Food Studies,” will be a participatory workshop with graduate students teaching hands-on field methods and theory. The second day, “Food in the Field,” will integrate classroom learning with key borderlands foods site visits (e.g. Las Milpitas, Mission Garden, Tucson Village Farm, Food City, St. Phillips Farmers Market, Thunder Canyon Brewery). Students will apply their
newly acquired research skills and learn from local practitioners. Through this short course we hope to build graduate-undergraduate connections around food systems scholarship with potential for ongoing collaboration.',
'outcomes' => '',
'response' => 'The Food Security and Social Justice network is steered by graduate students as a means to strengthen our research efforts, build collaborative relationships, and create opportunities for professional development. We saw the proposed engagements for 2016-2017, continued Ted-X style food-themed talks and a Critical Food Studies Short Course, as opportunities to create learning spaces for undergraduates and graduates alike.

Component 1: 
The 2016-2017 Speaker Series, which was supported by the Green Fund, was hugely successful, bringing together over 130 participants. Graduate students facilitated the panel discussions. We hoped to build on past successes by engaging graduate student and undergraduates from the School of Geography and Development School and Community Garden Program, Students for Sustainability, the Social Justice courses offered in Mexican-American Studies, and the Civic Engagement Programs offered through the Honors College to further engage undergraduate students. Within each TED talk we reserved space for both graduate and undergraduate students to participate. Presenters for the topic of Food Sovereignty included one graduate student and FSSJN member, while presenters for the Community Food Organizing panel came primarily from community organizations, with one UA faculty member. Nonetheless, these panel discussions drew many undergraduate and graduate students, who spurred lively conversations around topics ranging from seeds and intellectual property to anger about poverty as a powerful force for galvanizing action. 

In order to measure our success in achieving these goals, we maintained a sign-in sheet
detailing the names and contacts of participants at each event. We aimed to have a
minimum of 60 people (experiential learners) at each Ted Talk event in addition to the 5
Presenters. Presenters have not consented to filming of the events, so we have instead posted notes about their presentations and the ensuing discussion on our website, https://fssjn.wordpress.com/.

We had envisioned engaging 5-10 undergraduate students in work, volunteer and experiential learning positions specific to the TED-style panel talks. While we hoped to involve these undergraduate students in this aspect of our work, the organizing committee remained comprised largely of graduate students due to our own limitations in capacity for outreach. However, through the Short Course (detailed below in Component 2), we have been able to identify energized undergraduates who are interested in joining the organizing committee next year. We look forward to their involvement and further collaboration.



Component 2:

Objectives for Student Involvement in the Food Short Course included:
1. Recruit 25 undergraduates as participants and 6 graduates as facilitators.
2. Undergraduate students will be able to identify how their understanding of and capacity
to intervene in their food system requires a framework which broadly examines how food
is connected to social, political, ecological and economic relations.
3. Undergraduate students will benefit both academically and in practice from the
theoretical approaches and field experiences of graduate students focused on critical food
systems research.
4. Graduates participating in the curriculum planning process will gain an important
academic experience by developing and executing a course which is relevant to their
research interests, while also providing a foundation for developing future food-based
short courses.
5. Graduate and undergraduate students will be able to engage in a mutually beneficial
dialogue, which draws on diverse experiences and perspectives for critically examining
the food system and opportunities for constructive intervention.

Measuring Impact
1. Undergraduate Pre-Survey: We administered a pre-survey using Google Forms as part of the application process for interested undergraduates. The application included an open-ended questionnaire that asked why students opted to enroll in the course, what they hoped to learn from the course, and relevant internship or volunteer experience they had working with food-related organizations or activism.

2. Undergraduate Post-Survey: Following the second day of the short course we asked students to provide constructive feedback they can offer the instructors and course designers as well as identify stronger and weaker components. The anonymous evaluations were overwhelming positive. 

3. Organizing Committee Debrief: Graduate students who participated in the
development and teaching of the short course met two weeks following the short course to discuss future course development, while also identifying both successes and needed improvements for future course planning and execution. Graduate students reflected that the course had gone overwhelmingly well, and felt that there is now a solid foundation to build on for the future. 

Actual Impacts and Outcomes

The 2017 Short Course, “Introduction to Critical Food Studies,” met all of its goals. 

“This was amazing and I feel fortunate to have seen the flyer. This is one of the best engaged activities I have been involved in all year.” - Short Course Participant Evaluation 

Objective 1: We registered approximately 20 students, however, 12 students actually participated in the short course. In future iterations of the short course, we will over-enroll students since we did not realize how many students would drop at the last minute. The 12 undergraduate and 6 graduate students that did participate were phenomenal. 

Objective 2: Day 1 of the course (see attached schedule) enabled students to work through some of the key concepts in food studies such as food sheds, food system components, food security, food sovereignty, and food enterprises. Students conducted both food system mapping and foodshed mapping where the connected  social, political, ecological and economic issues with our local food system. These interactive activities and key concepts were reinforced with three themed field trips that connected these overarching concepts to Tucson issues and important food system sites. 

“I am so grateful for the time and space to talk about these issues and ideas. I feel more knowledgeable and hopeful about my future and career choice, as well as the future of my local/regional/global foodsheds.” - Short Course Participant Evaluation

Objective 3: The curriculum for the Short Course included both theory and methods. We endeavored to make sections of the course that took place in a classroom setting as interactive and engaging as possible, and therefore broke up sections on theory and methods with partner and small group activities. The field trips allowed students to apply what they had just learned about both theory, and particularly methods, in a real-world setting. The experiences of the graduate organizing committee in both theoretical and methodological field training did indeed benefit the students who participated in the short course, both academically and in practice. 

Objective 4: Graduates participating in the curriculum planning process will gain an important academic experience by The Short Course provided an excellent opportunity graduate students to develop and execute a course which was relevant to our research interests, while also providing a foundation for further refining future food-based
short courses, which we hope to continue on an annual or a biannual basis.

Objective 5: The Short Course provided the space, both in classroom and field visit components, for graduate and undergraduate students to engage in a mutually beneficial dialogue. These conversations drew on all participants’ diverse experiences and perspectives in critically examining the food system and spotting opportunities for constructive intervention. The small number of participants also allowed for easier interpersonal connections between participants, and many students noted that they enjoyed the size of the course.',
'created_at' => '2017-06-14 19:22:49.000000',
'project_id' => 655,
),
51 => 
array (
'netid' => 'jaclynmendelson',
'scope' => 'The University of Arizona Community Garden (UACG) has always strived to be an intersectional place where we can bring community members and students together to learn and implement sustainable efforts in gardening and food culture. We are always looking to further our impact on the community and therefore are requesting funding for a program to support a cross cultural workshop focusing on preserving traditional agricultural methods from other regions as well as teaching people how to adapt their methods in new regions of the world. The UA Students for Sustainability garden committee is collaborating with Tucson International Rescue Committee (IRC) to organize an event, where refugees will present information about growing practices from their homes and respective cultures. The presentation of information will follow up with live demonstration of their traditional methods in the UACG, which allows for hands-on learning for all attendees. The workshop will promote cross-cultural relations and agricultural education. In exchange for the information that the refuges present to us, we will also host a workshop, teaching them some of our agricultural knowledge as locals of the Tucson region. In doing so, we can help teach them about sustainable agriculture methods for growing in the Arizona region (their new home) and how to improve food security in a food desert. This project will impact both community members and students positively through the promotion of global sustainability techniques as well as promoting social justice through sustainability. The refugee community is an integral part of Tucson and through this workshop we can empower women in this community to teach a part of their lifestyle to students at the University, work on presentation skills, gain valuable community outreach, and get paid for their efforts. We hope that this project can continue to expand our community reach to all members of the Tucson community and build new bridges in our area.',
'outcomes' => '',
'response' => 'Desired Impacts:
•	This IRC collaboration event hopes to improve environmental sustainability at the U of A by spreading awareness about the UA Community Garden and sharing knowledge of culturally diverse sustainable cultivation practices.
•	Encourage use of the UA Community Garden while also empowering UA students, staff, and community members to grow their own food using traditional sustainable gardening methods/techniques from different cultures.
•	By facilitating learning of different cultural cultivation practices and utilizing the garden space, the IRC collaboration event will encourage students and staff to lower their dependence on non-local foods to decrease the negative environmental impact of their diets.
•	Connect students to refugees by creating a safe and tolerant space for sharing knowledge
•	Empower refugees to gain crucial professional skills that will increase employability while also making them feel welcome in Tucson
•	Pay refugees a stipend for their work to increase sustainability of their lifestyle and to support them for the work they share with us

Strategies:
Two refugee presenters from two different countries will lead a discussion with a group of students and community members on agricultural practices specific to their home. Attendees will be encouraged to take notes during the presentations. After the presentations, attendees and presenters will apply their shared knowledge in hands-on garden work in order to solidify the lessons and further the promotion of culturally diverse agriculture.

Measuring/Reporting:
In order to measure the impact of this event, we will record the number of participants in attendance. A report detailing the shared knowledge between refugees and attendees that will take form during the event will be drafted and shared with all participants and the Green Fund committee.

Administrative co-leader: This is the UACG manager, who oversees all programs that occur in the garden, and is connecting directly with the IRC to conduct this event. This undergraduate student will gain crucial event planning skills. 

Project Coordinator:
There are four coordinators working on specific details of the event under the direction of the administrative co-leader. They will gain managing, planning, leadership, and decision-making skills. In addition, the coordinators will learn more about sustainable gardening practices and food diversity as they participate of the IRC’s workshops. The coordinators will also engage with the garden community. 

Student volunteers: 
These are the students that make up the remainder of the SFS garden committee and will provide support to the coordinator team as well as learn from the refugee speakers that lead the different presentations. They will engage with the attendees to encourage participation and integration and assume leadership roles during the hands-on aspects of the workshops where needed. These students will gain knowledge about sustainable gardening and food insecurity, and most importantly how these are connected. 

Experiential Learners:
These are the attendees of the event (i.e. UA students, refugees, neighbors, plot tenants, etc.). Through this set of workshops the event attendees will be inspired to learn more about culturally diverse crops and about distinct growing practices. Attendees will be exposed to the practices of sustainable gardening and will gain hands-on experience. They will also expand their knowledge of food crops and diversity in our region. Attendees will also learn about the IRC’s mission and about the Tucson refugee community, helping to create a welcoming and safe space.

Benefit to refugee presenters: 
Through the proposed project the IRC speakers will further their professional training by leading the different presentations. They will have the opportunity to expand their creativity and planning skills as they co-plan the event with the garden team. They will benefit from having a safe & open space in the UA Community Garden to share their knowledge about cultivation practices to students and community members. They will also be able to promote the IRC’s nationwide mission to the event attendees, and as a result spread social awareness of Tucson’s diverse community, the local refugee community, and collaboration opportunities. In addition, having a partnership between the IRC and the UA Community Garden will open the door to many future projects that promote refugee empowerment, sustainable practices, cultural integration, crop diversification, and community engagement. 

Actual impacts:
UACG invited in more than just UA students to welcome IRC refugees and give them space to share their knowledge. Around 35 people participated, and together learned through presentation and hands-on gardening lessons. The UACG also benefitted then by having 2 rows planted by IRC refugees and participants, yielding more than 50 squash in the summer season. Had this event been marketed more, there could have been more impact. Marketing in the future could include a wider reach to more students and community organizations, faith-based and not.',
'created_at' => '2017-07-27 18:34:27.000000',
'project_id' => 699,
),
52 => 
array (
'netid' => 'mbohlman1',
'scope' => 'My overall goal for Davis Elementary is to improve the capacity of the garden to provide more teaching and learning opportunities for students, teachers and UA interns. This capacity expansion will include impacts not only on the school children but also on the interns with long lasting impressions. I wish to improve the garden space so that new ways of teaching and learning can occur for all parties involved: school kids, interns, teachers and families. Davis Bilingual Elementary School has been a central part of the UA Community School and Garden Workshop class since its infancy in 2009, bringing new and returning interns to the school every semester since then. These students commit hours during their week to use the garden as a learning tool for teaching experience and to give back to school through small projects. One proposed project is paint the wooden picnic tables that were recently purchased at Home Depot. Another project is to accumulate more supplies to expand upon the vermaculture unit, which includes bins, compost, coconut cores. In addition, the chicken coop has been a learning tool for at least 5 years and needs help. Wood and supplies for chicken maintenance, including food, are required to expand the space as the chicken count continues to grow. Another component involves both Aquaponic and Garden clubs, who have been cooking and creating recipes with the garden harvest and need purchases of additional ingredients and drinkable water. Lastly, the Davis Garden is constantly in need of mulch, compost, and soil amendments to ensure that we can maximize our organic food production for Davis students, and families. Good soil also allows us to create the best learning environment possible for both Davis and UA students.',
'outcomes' => '',
'response' => 'Desired Impacts: The single goal of funding improvements with the Green Fund for the Davis School Garden is to strengthen the relationship of the University of Arizona with Davis as a model for Tucson community and school sustainability support.
How to achieve impacts: These projects can be achieved by working with current UA student interns and the garden coordinator to implement these projects immediately. Most projects already exist or have started, therefore the funding allows for them to continue and thrive.
Measurements of success: The success of this objective can be measured through the community garden celebration that celebrates the completion of these projects in April of 2017. The UA and the public can engage with these projects and the progress will be updated on the school garden’s website as documentation.
Actual Impacts: The UA and Davis Elementary have a long relationship based in the UA Community and School Garden Program. There are between ten and twenty UA interns who are integral parts of the Davis school garden each year. The Davis Garden hosts monthly garden work-days in collaboration with the Community and School Garden Program, as well as UA education classes, UA Green Academy meetings, and community/university celebrations. The above 5 projects that I listed are all improvements that required coordination and participation from the UA interns and Davis students, so that the garden maintains its functionality. An enhanced garden space allows for new and different kinds of lessons to be taught or perhaps interns will learn different skills by supporting the implementation of these changes. With these improvements, UA students and interns were encouraged to return to this space and continue fostering learning environments for the Davis students. School was in effect until May 25, 2017 and the construction and painting of picnic tables, soil amendments, and chicken coop extension projects began back in late April. “Cooking with the Garden” is already scheduled for every Friday for both Aquaponics and Garden clubs, so supplies were put into immediate effect. “Worm Chore Day” is already scheduled for every Monday for Garden club, so supplies were put into immediate effect.
I feel that the goal was met by the end of the funding period and that the Green Fund Committee can take great pleasure in knowing that the school garden has undergone major transformations on behalf of them. Unfortunately, the timing of everything is a little off, since by the time we received the grant, school was almost over, so I would be cognizant of that for future projects.',
'created_at' => '2017-07-30 18:29:05.000000',
'project_id' => 670,
),
53 => 
array (
'netid' => 'slb4',
'scope' => 'The proposal that was accepted by the Green Fund was to fund the Environmental Health Panel. The event brought three faculty members from the College of Public Health to hold an open discussion and answer questions regarding current and future topics about how human health will be impacted by environmental changes.',
'outcomes' => '',
'response' => 'The desired impacts of the event was to introduce the concepts of environmental health to the UA community of students and faculty. The panel brought together a group of professionals that research how the environment is affecting human health, both with present conditions and future changes. We hoped that attendees would get an additional view of sustainability in the context of community health and environmental justice. This benefits students by providing yet another informational tool to further sustainability efforts and be advocates for environments that are healthful for everyone. The actual impacts of the project reflected the goals and desired outcomes based on the feedback from attendees and questions raised during the event. In the future, I think the Green Fund should fund panel projects because they are an interesting way to introduce new sustainability concepts and bring in professional insight and knowledge.',
'created_at' => '2017-08-04 23:09:17.000000',
'project_id' => 649,
),
54 => 
array (
'netid' => 'tledbetter',
'scope' => 'Celebrated annually across the United States and the globe, Earth Day seeks to encourage
everyday people to make more sustainable, environmentally aware choices to reduce their environmental impacts. This project aims to bring Earth Day back to the University of Arizona campus in order to more effectively engage students under a theme of Earth Day, Everyday. Our overarching theme is to create a beginning this Earth Day, rather than a celebration, by educating attendees on local issues in order to build a foundation for environmental and climate literacy. Earth Day needs to be about more than just taking one day out of the year to plant a tree or recycle, it needs to be the start to a lasting commitment to reduce one’s water consumption, carbon footprint, food waste, and more by tying these issues to local examples.

We are inviting UA and Tucson organizations, clubs, and colleges focused on food security,
wildlife conservation, social justice, water conservation, sustainable transportation, health, infrastructure, and more in order to effectively engage students, staff, and faculty of all interests and educate them on local environmental issues and solutions to build this foundation for progress. Each of these organizations will be encouraged to utilize an engaging, interactive approach to attract and retain student interest in tangible, local environmental issues and solutions. We will also showcase what is already happening on campus, areas of improvement, and involvement opportunities via advertising of the Green Fund, Students for Sustainability, environmental clubs, and job, internship, and externship opportunities. Throughout the tabling area, we will post a variety of informational materials relating local problems to solutions, strategically placed near organizations with foci in that area. We are reaching out to alternate funding sources to provide a large tent for the area to shade all participants, as well as local food vendors to encourage passerby interaction with groups tabling for the
event. Recent policy trends show a decreasing support for environmental programs and education at the governmental level, making events such as this all the more crucial to provide a venue for environmental education and engagement to everyday students, faculty, and staff.',
'outcomes' => '',
'response' => 'We successfully pulled off the large scale event with ~40 local companies, NGOs, and organizations who promote sustainability and can provide volunteer or job opportunities for students. This further strengthens our relationship with these organizations and allows us connections for working together in the future.

We also exposed students who normally wouldn’t be interested in sustainability to many of the different aspects that go into this topic. Due to the central location on the mall, we were able to capture a wider audience of students than we are normally engaged with during our programs. In total, we attracted ~500 UA students to the various tables at the event. 

In the future, we would like to engage faculty who may be interested in collaboration more heavily. We received positive feedback from nearly all of the participating organizations. To improve future events, we should avoid holding the event on a Friday and should also look into funding for a large tent like that at UA Food Day to avoid the issue of heat/sun exposure. 

•',
'created_at' => '2017-08-14 17:04:46.000000',
'project_id' => 698,
),
55 => 
array (
'netid' => 'jverwys',
'scope' => 'The Office of Sustainability requests a mini-grant of $1,500 to support its latest innovation in a
continuing effort to make the University of Arizona’s athletic events truly Zero Waste Games.
The UA is committed to improving the current processes of reducing waste at football games
(and eventually other athletic games) by diverting more recyclables and compostable material
from landfills.
UA Athletics, the Office of Sustainability, Greening the Game, Students for Sustainability, and
Green Team has identified the home game against Stanford on Saturday October 29, 2016 as
being ideal to try a new pilot project. This game will be strictly monitored as we will use numbers
to enter into the PAC-12 Zero Waste Competition. During this competition last year we diverted
69% waste from going to landfill. We were awarded a 3rd Place finish which was very
respectable considering the other universities in the PAC-12. We will have much more
competition this year as ASU was in the middle of stadium renovations last year and could not
fully participate,. This year their new facility is fully operational and loaded with sustainable
features. We can do a lot better than 69% and this mini grant will help get the supplies we need
to reach the next milestone.
The mini-grant would be used to purchase necessary supplies for a post-sorting process the
day after the homecoming game. While the Office of Sustainability understands the Green Fund
has awarded funds for the purpose of supplies at Zero Waste Games in the past, the assistance
is greatly needed again as we launch a new way to significantly raise our diversion rates, save
future costs, increase safety measures and create a process we can use again and again in
games to come.
Our pilot back-of-the-house sorting process will ensure every piece of trash generated at the
event will be touched and sorted. This will increase our diversion rate generously and provide us
with tools that can be reused in the future. Currently, after each Zero Waste game, the 
Sustainable UA crew swarms the stadium picking up compost and recycle material. The crew
only has about 2 hours to pick through the stadium before they have to turn off the stadium
lights. We had about 100 workers last year but still was not able to fully touch everything in the
stadium. We also were not able to sort tailgating waste for compost material. This new system
will allow us to process all stadium waste and tailgating waste. We are looking to purchase metal tabletops with drains that will be used to hold trash while student volunteers sort it for
recycling, compost or landfill. These tabletops will filter liquid waste that we hope to transform
into compost as well. If this new process improves our abilities to sort through waste, it will
ultimately save us money and provide us with tools and a method that we can continue to use.
We believe there is no reason that all of the UA’s athletic games can’t be Zero Waste Games at
some point in the future.
Our post game sorting setup will be made up of stations consisting of tables and supplies
for the sorting itself. Along with the metal table tops, the mini-grant will fund tarps and safety
supplies to process glass items. This year one of our largest focuses in community engagement
is cultivating a choice in guests to opt for cans instead of glass bottles. We are looking to
purchase glass safe gloves and aprons to ensure the safety of student volunteers who will
undoubtedly come into contact with glass.
We view this post-sorting process as a proof of concept practice to determine if this is a way to
increase our impact at athletic events in the future. Post-sorting will allow us to divert higher
numbers of recycled items, as well as allow us to track the volume and types of trash generated
more fully. As always, there is a wonderful opportunity to educate students, faculty and the
community in attendance on sustainable practices.',
'outcomes' => '',
'response' => 'Desired impacts:
- Increase waste diversion at October 29 homecoming football game. Last year we had a
69% diversion rate. We expect to do much better this year.
- Drive guests to opt for safer canned beverages over glass bottles during tailgate
outreach
- Determine how to run post sorting practices more cost effectively.
- Increase impact by collecting compostable liquids.
- Create tools and a post-sort process which are reusable.

A group of about 15 volunteers from Greening the Game and the Green Team met back at the recycling and composting roll offs at 6:30 a.m. to open bags of recyclables collected the night before during the Homecoming Game. The team sorted each bag to separate compost and recyclable items from landfill to increase diversion rates. We diverted 4620 pounds of recyclables and 5940 pounds of compost, for a total diversion rate of  69.66%. While this is not much higher than the 2015 diversion rate, we do consider this a success as we collected more compost, including liquids, than ever before. 

We also tested out a new sorting table created by a UA student in the theater program. The table was designed to aid in the separation of compost, recycle and landfill waste. This was a pilot/test run of this design and we found that it did not help our post sorting efforts much. The design was not quite right for collecting liquid compost from piles of unsorted trash. This sorting table was something totally new and not something many other colleges have attempted. We learned a lot from this attempt, and believe that while the table might not have served the purpose we hoped it would, it has given us ideas to try during post sorts after UA athletic events in the future. 

We are constantly working to improve our diversion rates at UA athletic events. We do believe this project and mini grant provided us valuable insight into our collection processes, post-sorts and streamlining waste collection. We are utilizing compost buckets, and are implementing better management and planning to ensure higher diversion rates. Zero waste efforts are becoming a normative and expected part of UA athletic events. This is one of our largest continuing goals.',
'created_at' => '2017-08-15 19:40:22.000000',
'project_id' => 650,
),
56 => 
array (
'netid' => 'jverwys',
'scope' => 'Spring Fling is the largest student-coordinated carnival event in the United States. Spring Fling
drew approximately 32,000 attendees last year, with a majority of those guests being UA
students. Spring Fling will once again be held on the UA Campus this April. An event of this
size has a large environmental impact through increased use of water, electricity, increased waste
among other things. Conversely, this event provides an opportunity for the UA campus
community including the UA Green Fund to demonstrate its desire to use sustainable practices to
decrease the environmental impact of Spring Fling while simultaneously improving the
experience of event attendees. This Mini Grant seeks $750 for the staffing and operation of a
bike valet at Spring Fling. The goals of this project are 1) to reduce the environmental impact of
an on-campus event that sees considerable community participation; 2) to reduce the impact of
Spring Fling on adjacent neighborhoods in the form of vehicle traffic and on-street parking; 3) to
provide a free and convenient bicycle parking option for event attendees; 4) to improve the
student and community experience of Spring Fling; and 5) build on the enormous successes of
the previous year’s bike valet program. For Spring Fling attendees seeking a flexible, healthy,
affordable, and sustainable transportation option, the bicycle valet is an unparalleled choice.
Having a bike valet keeps the valuable bikes of guests safe, encouraging more guests to ride their
bikes. For guests and students who rely on their bike for transportation, or prefer it to modes of
transportation that have high emissions, the bike valet has become something expected. We
believe it is important to make green initiatives normative on campus, and the way to ensure that
is to continue to bring in these green initiatives to campus events.
We consider last year’s Spring Fling bike valet a succes. We served over 140 people over the
course of three days – almost double the figure from the previous year (80 participants). Saturday
was the most popular day, with 44 percent of all participants utilizing the valet that day.
The college has been incredibly supportive of the bike valet, and various campus media and
marketing has included the bike valet in news articles and marketing. The bike valet is a selling
point to guests and a source of pride. While the bike valet might not be the biggest draw of the
event, it has been a huge part of Spring Fling’s return to campus. The same year Spring Fling
returned to the UA Mall was the first time the bike valet was featured. We consider the valet to
be a part of the evolution of the Spring Fling and dream of improving it each year Spring Fling
takes place. After the debut of the bike valet in 2014, Mark Napier, Associate Director for PTS
said in a post-event email:
“We were very pleased to be able to help. From our experience, we believe that each year the
Bike Valet is offered, it will be more successful. Having Spring Fling back on campus was new
and Bike Valet as a concept is new. People know that this is a service that will be present now.
It most certainly adds value to the event and to our collective commitment to sustainability.',
'outcomes' => '',
'response' => 'The desired impact of this project is to provide Spring Fling guests a safe place to
keep their bikes while they enjoy the event. This also furthers the UA’s support of sustainable,
healthy modes of transportation. This continues Sustainable UA’s goal to keep campus events
sustainable and as green as possible. Working off the successes of both last year and 2015, we
hope to make the bike valet a normative feature of Spring Fling and continue to improve.

During the 2017 Spring Fling, guests who used the bike valet areas were asked to fill out a survey and were also counted by those staffing the valets. We counted 92 people who used the bike valet, though we do expect the number is actually higher, due to problems taking accurate counts during busy times. Of the 52 who filled out the survey, the majority of them were new to using the bike valet during Spring Fling. The majority of riders were UA students, 37, but community members, 14, also used this service. 

Lessons learned: 
At times, reporting/counting of guests was inconsistent. This year we switched back to a paper survey, rather than one on an Ipad, but of all the guests who used the service, 36 didn\'t fill out the survey. Moving forward, maybe there is a way for us to condense the survey down so that more people will be more likely to fill them out. The next time we do bike valet, there will be an increased focus in training employees the proper way to count people using the bike valet and gathering survey information. 

Parking and Transportation is a great supporter of this project, and we look forward to continuing to work with them at Spring Flings, and other events to come. Overall, this project did encourage students and community members to ride to a signature UA event, which is healthy and sustainable. Not only did we reduce the carbon footprint of Spring Fling, we also made the collaborative efforts of Sustainable UA known, just by our presence. Student employees provided guests with information about UA sustainability.',
'created_at' => '2017-08-15 19:56:19.000000',
'project_id' => 643,
),
57 => 
array (
'netid' => 'jverwys',
'scope' => 'First Food Bank has found that Arizona has a heightened 17.8% of residents that are food
insecure compared to the national average of 15.9%. At the University of Arizona specifically,
every 5 in 8 students are food insecure. The Campus Pantry, located on the University of
Arizona campus, has a goal to combat that food insecurity in our community. The UA Campus
Pantry serves over 130 people each week, students and faculty included.
Project Campus Pantry seeks to assist the Campus Pantry and another sustainability based
program, the Move Out Project, in their latest endeavor; reducing food waste during trash
diversion at student move out at the end of the semester. Food waste is a contributing factor in
the larger scope of food insecurity.
The Move Out Project is designed to divert items thrown out by students moving out of
their dorms. Many times these items are in good condition and they get collected to be resold at a
low cost to other students who can’t afford to buy brand new supplies for their dorms, homes or
school semesters. Among the many things student employees collect are food items.
The Move Out Project is expanding from the Park District dorms to include the Highland
District (14 dorms total). It is expected that three times as much food will be collected in
donations and diverted from the dumpsters than last year. Campus Pantry would love to take the
food collected during the Move Out Project and ensure it is distributed to hungry UA students
and staff. However, with the increase in size of the Move Out Project, more students will be 
needed to collect the staggering amount of food collected in a relatively short amount of time.
This is where Project Campus Pantry comes in; to provide essential support in food rescue.
Project Campus Pantry will hire 4 student workers during the Move Out Project to help
with this process. The student employees of the project will pick up food once a day from the 15
halls selected for the Move Out Project and take it directly to the pantry and its space in the
Union warehouse. Campus Pantry has ample space for increased amounts of food in the Union
warehouse, and the Union has offered to provide additional space for food if needed. We
anticipate picking up over 6000 Lbs. of food during move out, which will all need sorted.
Campus Pantry can take all food items that are not expired and any additional overflow can be
donated to Community Food Bank or another local pantry close to campus. The bulk of what
Project Campus Pantry employees will do is sorting through this large amount of food. Neither
Campus Pantry or Move Out currently has employees free to tackle the time-consuming task of
checking for damage nor expiration dates on each individual piece of food. Without this project,
there is no way to ensure food gathered during and by Move Out employees will successfully
reach the Pantry, get sorted and distributed. In those over 6000 Lbs of food is hope; countless
meals for someone struggling just to nourish themselves. It’s so hard to chase your dreams when
you go to bed hungry, and we don’t want any of our fellow Wildcats to be hindered by hunger.
A mini grant of $1,500 is requested for this project. These funds will be used to pay the
four student hires for 3 hours a day for 8 days at $10 an hour. The Move Out Project and Campus
Pantry will hire these students collaboratively so that we can provide knowledgeable participants
that will be able to execute Project Campus Pantry to its best potential. Funds will also be used to
purchase special refrigerated boxes that will be used to collect, transport and keep food items
fresh.',
'outcomes' => '',
'response' => 'The project’s desired impacts are to better help the thousands of students and faculty at the
University of Arizona that are food insecure. The Move Out Project provides the perfect
opportunity to reach nearly 5,000 students and even more family members to showcase what
Campus Pantry is for and to collect items to build that valuable asset of the UA. The project aims 
to advertise for the Campus Pantry and share with students who are leaving for the summer what
an amazing resource is available to them to help combat this food insecurity. 
The collected food items will be weighed daily once picked up from the 15 halls and tracked to
accurately obtain results to show the significance in food that would have otherwise been wasted. 

During the Move out/Dodge the Dumpster project, 3840 pounds of food were gathered from the participating residence halls for Campus Pantry. The student employee hired to help transport food collected during Move Out was necessary to take in this larger than usual donation to the pantry. The student employee worked throughout June to go through each food item to ensure quality and check expiration dates. As the Dodge the Dumpster project continues to grow, we hope to one day include all residence halls. In total, 9950 pounds of food were collected by Move Out staff, with food not going to the pantry being donated to local food banks/charities. Student employees of the Dodge the Dumpster program and the employee hired specifically by this grant to aid in Campus Pantry collection both informed students and their families during move out about the resource available in Campus Pantry. UA students and their parents were interested in efforts to alleviate food insecurity on campus. 

The food collected during Dodge the Dumpster will be available to the UA community during Campus Pantry\'s first distribution of the Fall semester on Aug. 25. For the first time, Campus Pantry will also be giving out additional items collected during move out, including cleaning supplies, laundry detergent, etc. Campus Pantry anticipates that the new items will go quickly. 

This was a very successful project, both in the volume of food and other items going to prevent and aid those on campus facing food insecurity and exposing more students to Campus Pantry.',
'created_at' => '2017-08-15 20:26:00.000000',
'project_id' => 703,
),
58 => 
array (
'netid' => 'jillb1',
'scope' => 'Earth Hour is an international event that encourages individuals to turn off all electricity and unplug for one hour to raise awareness about our climate crisis. The UA Res-Life Eco Reps plan this event to be held on the UA Mall. Our Program is meant to provide a fun event for students/UA personnel to attend for the duration of Earth Hour. We are holding our program from 8 PM to 10 PM so that everyone can be electricity free from 8:30 PM to 9:30 PM, which is our actual allotment for Earth Hour. We are also sure to remind residence halls that they cannot use candles in their halls, which makes attending this event optimal. We intend to show solidarity around the climate crisis. We also intend to provide a learning experience about conservation of energy while having a great time. We will have entertainment acts that do not require electricity, such as on campus a cappella groups. We will also be working with a local pizza company to provide pizza at the event without using electricity to keep the food warm. It is our ultimate goal to have 250 people attend the event to pass our attendance from last year. There will also be a clothing exchange held in the residence halls leading up to the event and at the event.',
'outcomes' => '',
'response' => 'This is an important event because it raises awareness and helps a cause. By shutting off our power for an hour around the world we are limiting total energy usage worldwide. We are promoting sustainability and conservation through the decrease of our energy use. This event serves as a way to teach others how to reduce and conserve energy. Promotional posters will be created and posted around campus prior to the event to encourage people to turn off their lights before coming to the event. Every year we have even had Residence Life Resident Advisors bring their wings to the event. Resident Advisors check all of the lights in their wing to ensure they are turned off before leaving for the event. Throughout the event there will also be periodic announcement about different ways to live sustainably in-between acts. Lining the food table will be passive signage that encourages sustainability. The goal of the program is to get students interested in sustainability by mixing and mingling with other students interested in the same cause. Even though we hope to have a high attendance rate at our event, there will not be enough people present to notice a change in local energy usage during Earth Hour. However, we do have attendees sign in to document their presence and receive recognition for participating in our month long Recycle Mania program. We intend to use this, and visual cues to determine how many attended the program.

In the end, about 200 people attended this year’s Earth Hour celebration.  Once again, we were able to secure a $25 carbon offset donation from Papa Johns.  We hung 12 different passive education signs around the event, including information about conservation and sustainable food.  We made announcements between acts about ways to live more sustainably.  We invited Compost Cats to speak at the event, but a last minute emergency kept them from doing so.  We ran a clothing swap that was successful, redistributing about six large bags of clothing and donating three more.',
'created_at' => '2017-08-18 17:15:10.000000',
'project_id' => 675,
),
59 => 
array (
'netid' => 'mmheard',
'scope' => 'From October 19-21, 2016, the University of Arizona’s Arts & Environment Network, in partnership with Students for Sustainability, the College of Fine Arts, the Carson Scholars Program, and the Honors College, hosted artist-scientist Andrew Yang.  We requested $1500 from the UA Green Fund to cover the cost of an honorarium and food at events and activities during Andrew Yang’s visit.  

Yang, an award-winning artist and scientist, is associate professor at the School of the Art Institute of Chicago and research associate at the Field Museum of Natural History. He holds an MFA in visual arts, a PhD in biology, and a BS in Chemistry and Biology. With expertise combining field biology, creative writing, and visual arts, Yang is an excellent resource for anyone interested in arts-environment intersections. Yang presented on environmental art in the Anthropocene. 

Yang is especially interested while at the UA in meeting with Students for Sustainability.  Yang’s schedule included a presentation with honors college students; a public lecture that is open to the general public; a class visit with College of Fine Arts students; a meeting with Students for Sustainability; a meeting with Carson Fellows; and a social event with members of the Arts, Environment & Humanities Network. 

Experiential learners were given with Yang\'s visit the opportunity to learn about and engage in dialogue about ways that complex environmental issues such as climate change can be addressed through art; Learners also had the opportunity to learn about his process and craft as an artist, scientist, and writer.',
'outcomes' => '',
'response' => '	Attendees of lectures and events left with an increased understanding of the role of the arts in tackling complex environmental issues;
	UA community members, particularly students interested in arts and the environment, learned from Yang’s expertise in bridging disciplinary divides;
	Events received positive media coverage; and
	Faculty and staff at IE and across the UA were inspired to apply for interdisciplinary funding to pursue work or teach courses that bridge disciplinary divides, continuing these conversations.  

Individuals from across campus, from many fields in science and the arts, were able to benefit from Yang\'s visit.  The variety of events (Public Lecture, faculty/staff/grad student lunch, open discussion/lunch with students for sustainability, as well as numerous conversations with Yang held campus-wide) allowed by support from this grant meant that Yang\'s influence reached far and wide.',
'created_at' => '2017-08-30 17:23:49.000000',
'project_id' => 652,
),
60 => 
array (
'netid' => 'cmichellesun',
'scope' => 'The University of Arizona Campus Pantry (UACP) is a student-run organization that provides food to students, staff, and adjunct faculty members in need. Since its founding in 2012, UACP has been powered entirely by 4-5 students volunteering to collect donations and then distribute food twice a week to food insecure members of the campus community. Despite herculean student volunteer efforts, the effectiveness of UACP is limited by several factors: unpaid coordinators; inaccessible location; no fresh foods due to lack of cold storage. Campus food insecurity far exceeds UACP’s current capacity to address it. In Fall 2015, UACP, UA Student Unions, Students for Sustainability (SfS), and Compost Cats formed a new partnership to bolster UACP’s capacity to feed those in need on campus. 

The Student Union provided a more accessible space for UACP along with cold storage for dairy and fresh produce. The Student Union also worked with its existing vendor network to line up regular donations of dairy products for UACP distribution. SFS, UACP, and the Student Unions will continue to increase capacity with collaboration on a new online marketing campaign to spread the word about UACP and its new distribution of fruits, vegetables, and dairy. UACP staff members will also work closely with SFS and Compost Cats to help coordinate distributions and spread the word. Compost Cats has access to fresh produce from local grocery stores that could be supplied to UACP now that it has cold storage. Compost Cats has already begun transporting fresh produce from Whole Foods on River Rd to UACP on Friday mornings prior to UACP weekly distributions (12-3 pm each Friday). Compost Cats will continue to work with its network of partners to provide regular produce donations for UACP of produce that would otherwise have gone to composting or landfill. Students for Sustainability has provided office space for UACP in its cubicle area on the 4th floor of the student union as well, and the SFS Food for All Committee has committed to providing regular volunteer labor support as part of its central mission each semester. Chet Phillips, ASUA Sustainability Coordinator, has agreed to supervise the UACP student coordinator and assistant as well, and ASUA has agreed to take UACP on in some form. These are all significant steps toward making UACP a sustainable enterprise capable of providing help for all the students, adjunct faculty, and staff who need help having enough healthy, nutritious food for themselves and their families. Yet the labor required to keep UACP operating smoothly has increased as more people need its services and UACP has begun distributing food once per week rather than once every other week as in past years. Volunteers will remain essential to UACP operation, but the work is more than what can reasonably be expected from volunteers alone. Requested funding will provide 10 hours/week of consistent student labor support for UACP and cover extra labor incurred in coordination work with Compost Cats in diverting produce to UACP. 

In the interest of total transparency, this mini grant will be followed by one more in the spring semester to continue modest paid support for two part time student UACP employees. This support will be absolutely critical to ensure that the longtime UACP student coordinator, Berkley Harris, is able to train a successor to take over UACP operation after her graduation in May 2017. Consultations with the 6 cultural centers on campus are also underway to determine whether some or all of them can also provide pantry support. This mini-grant proposal and the one to follow in spring will serve a vital gap bridging function though, allowing the broad collaboration already underway to continue and building a better foundation for UACP to sustain food insecure members of the UA community, most of them students, for years to come. The goal of UACP is to end campus hunger. This unique collaboration diverts produce that once would have been waste into fresh, nutritious food for those in need. More importantly though, it helps to sustain under-resourced members of our community who, in the absence of adequate nutrition, may struggle to succeed at the UA as well as those whose nutritional needs are well provided for.',
'outcomes' => '',
'response' => 'Desired Impacts: Food security is a sustainability issue. Members of the campus community lacking enough to eat are unlikely to perform well in their other campus roles. What’s more, the thousands of tons of fresh food that go to southern Arizona landfills each year create greenhouse gas emissions, environmental justice problems (someone has to live near landfills), and mean that many of the environmental costs of agriculture are wasted when up to 40% of food produced never feeds anyone. Yet a lot of this wasted food could still feed people, including the UA campus, if we put in place a distribution system to do so. By improving food security on campus, the UA can be a healthier, more sustainable place for students and other members of the campus community who lack access to enough food. 

The main problem is that not everyone who should know about UACP does know about it. Project partners will address this lack of awareness through a coordinated marketing campaign at the student unions, online, and around campus. UACP will use Green Fund support to provide a modest amount of paid labor, divided between one coordinator and one other student assistant, allowing them to do larger food collections and distributions with new access to produce and dairy products. Additional interns will provide unpaid but credited labor support so that more UA students, staff, and adjunct faculty know about and can access UACP food. In addition to providing extra support where needed for UACP, UACP staff and SFS will work with Compost Cats to help spread the word on campus about keeping fresh food out of local landfills and getting it to members of the campus community who suffer from food insecurity. 

Strategies: Paid UACP staff will work with the Student Unions and SfS to raise awareness through various means (online media, tabling, flyers, and word of mouth). Paid student staff and interns will also feel empowered to make real change on campus that benefits those in need and promotes behavioral change on campus regarding food waste. In addition, simply having a prominent location in the Student Union Memorial Center and cold storage to keep fresh food for distribution will dramatically increase the ability of UACP to meet campus food security needs. Along with increased capacity comes increased labor needs, and this is why UACP and its partners are seeking paid student labor support. 

Measuring Success: The UACP measures its success by the number of campus community members it is able to assist. As the number of people who learn about the organization increases, the number of people in need that can be served will increase as well, and thus the number of people without enough to eat will decrease. With the additional help provided by SfS and the Student Unions, more students and other campus community members will learn about where and when they can get food assistance, volunteer, and/or work to reduce food insecurity on campus. The success of this introductory project will be determined by the number of people it is able to feed, as well as the total quantity of food served to campus community members. Once a week, at each distribution, the number of people being served and the amount of food given away will be accounted for, with the goal being to see both of those values increase over the course of the semester. Additionally, the quantity of fresh produce given out that otherwise would have gone to landfill or compost will be measured each week. Adding fresh vegetables to UACP distributions will improve the nutritional value of UACP food provided to those who need it most. By Fall semester’s end in December, it is expected that the number of students, staff and adjunct faculty members attending UACP distributions will increase, along with overall campus awareness of the organization. The UACP has already seen an exponential increase in attendees since their move to the Student Union in the spring, and each week the number of attendees continues to increase (now surpassing 100 a week).

As desired, the attendance for UACP distributions increased as a result of this Green Fund Mini Grant. Over 3000 students utilized the services provided at distributions during the 2016-2017 academic year. With the help of this grant, staff was able to grow the services provided and raise awareness of the organization. 

In addition to reaching more students in need, UACP was able to provide a wider variety of fresh foods and produce due to collaborations with the Union and Students for Sustainability. While food insecurity continues to be an issue on campus, this grant was able to provide much needed services to the community, help staff the organization appropriately, and help UACP be a more sustainable operation.',
'created_at' => '2017-08-31 02:06:34.000000',
'project_id' => 632,
),
61 => 
array (
'netid' => 'alambert',
'scope' => 'This project is a collaboration between UA Students for Sustainability and Laura Kassmann, a UA School of Art graduate student in graphic design. Laura’s art and design project, Garden In A Box, uses recycled wood to create small planter boxes for herbs and/or small produce. Laura creates small graphically illustrated booklets that teach people about the different herbs and produce they can plant in the boxes and how to care for them. Laura would like to teach UA students how to make the planter boxes and distribute her illustrated booklets. She has partnered with the UA Students for Sustainability Art and Garden committees to conduct three workshops at the UA community garden where she will teach students how to make the boxes, paint them, and plant them with their choice of in-season herbs and produce seeds from Native Seeds Search. Laura has already discussed 10 varieties of native plant seeds that can be planted in the boxes during February and March. Native Seeds Search will donate the varieties to Laura for this project.',
'outcomes' => '',
'response' => 'The workshops are planned for February 19, March 5, and April 2. The workshops will be advertised online and around campus. A link to sign up for respective dates of the workshop will also be included on advertising materials. Laura and members of the Art and Garden committees will prep some of the materials, like wood, before the workshops so that each workshop will not be longer than three hours. During the workshops, students will learn woodshop construction and painting skills. Laura, and other UA Students for Sustainability Art committee members, will be able to work directly with students to guide them through these processes. The students will also learn about planting seasons and different herbs and vegetables from UA Students for Sustainability Garden members. At the end of the workshop, each student will receive one of Laura’s illustrated herb and plant care booklets. Enough materials will be purchased for 50-60 students. 
This collaborative project is an opportunity for many students to gain art, craft, and sustainability knowledge. Students will also have a lasting planter box that they can use for growing their own food.',
'created_at' => '2017-08-31 17:58:30.000000',
'project_id' => 666,
),
62 => 
array (
'netid' => 'jillb1',
'scope' => 'As the University of Arizona continues to expand and explore its sustainability programming, the groups working on campus for this cause must think of new ways to address green initiatives. Sometimes, a tried and true, more traditional process of the past can help breathe new life into things that might have been thrown away. The UA’s Eco Reps, a sustainability program through Residence Life, is hoping to take a skill once taught in schools to create something new out of UA branded banners taking up space in piles or landfills. Sustainability in a Stitch will become one of Residence Life’s newest sustainability programs, engaging students living not only in the residence halls, but also broader campus. We would like to find new uses for some old discarded UA banners and repurpose them by sewing them into wallets, credit card holders, little pouches for reusable silverware, laptop cases, shopping tote bags and whatever else we can dream up and pattern. Though many students today don’t know how to sew, it remains a useful skill for people of all ages; and sewing can help reduce the amount of clothes, curtains and more from ending up in the landfill. We are requesting $1,500 to fund this project, which will be housed entirely within Residence Life. We plan to spend $1000 on general supplies, such as thread, cotton webbing for totes, needles, scissors, reusable bamboo silverware, Velcro, and more. Approximately $500 will be spent on three heavy duty sewing machines, which are built to last, rely on a very simple bobbin system and are basic and simple to use. These particular models were vetted through a local sewing shop, selected for their simplicity and durability. There is an endless supply of UA banners for us to use, and UA Community Relations is working with us to ensure the banners come to this project instead of being wasted. They have been excited to help us imagine the new purpose each of these banners might fill. Everything about this project is sustainable, including its longevity. Additional funding from the Green Fund will not required once we buy initial supplies. Funding from Residence Life will be used to pay for any additional thread, bobbins, straps, etc. Julia Rudnick and Jill Burchell will teach members of the Eco-Reps how to use the sewing machines and sew the various types of projects (laptop bags, shopping bags, silverware holders, etc). Once the Eco-Reps have learned, they will pass this skill on to RAs and other residents. The Eco-Reps and Res Life will distribute these repurposed bags and assorted items to students in the Residence Halls to encourage greener lifestyle choices and educate students about ways to reduce waste in their own lives. Though the sewing machines and supplies will “live” in Residence Life, they can be checked out by anyone on campus. Whether a group of students wants to sew their own clothes, or a club wants to make bags for an event on the Mall, Residence Life will be able to check out the machines to anyone who needs them. When someone checks out a sewing machine, they will also receive brief instructions for use and patterns for the projects we’ve come up with. All of the current patterns have been vetted and tested out for ease, and we are confident anyone on campus can successfully learn to sew on these machines. Check out will include signing the machine and any associated supplies out, signing a check out agreement, a short lesson on use of the machine and choosing a pattern(s) to keep if a group or person needs them. Sewing isn’t a dying art. We think this is an applicable skill for students trying to save money on new items, repurpose old ones, and generally discard less in the macro sense of the world. As the project moves forward, we may very well find other materials, like the UA banners, which don\'t have to get thrown away. We can turn these items into something new, something that is a testament to our commitment to our campus and wasting the least amount possible.',
'outcomes' => '',
'response' => 'The desired impacts of this project are:
1. Repurpose UA branded banners to reduce waste.
2. Motivate UA students to find new and interesting ways to repurpose trash
into usable goods.
3. Teach UA students the valuable skill of sewing.
4. Encourage greener lifestyles for students living in Residence Halls.
5. Continue to build up and expand Residence Life’s sustainability
initiatives, as well as overall UA sustainability initiatives.
6. Provide students with inexpensive items with real, practical uses such as
laptop bags, reusable grocery bags and reusable silverware pouches.
7. Support campus creativity through providing sewing machines as a tool.

The Sustainability in a Stitch program is moving along its timeline just as we had planned it. Three sewing machines have been purchased, as well as additional supplies. Jill Burchell of Residence Life has been presenting this project to Residence Hall employees and all RAs have been informed that sewing machines are available to check out in their halls. The sewing patterns selected for this program are very simple, which is a key trait of this project’s success. Each item that can be made from recycled UA banners or other materials can be easily created by any student on campus in about six segments. Once Eco Reps begin again this semester, training will begin for them and they will learn how to use the sewing machines, as well as the checking out process. The Eco Reps will also learn how to track success rates of this project, and will encourage any “borrowers” of the sewing machines to share their stories with us through photos, short videos and written descriptions of their creations/projects. Residence Life, the Office of Sustainability and UA Community Relations will begin to get the word out to the campus at large once the semester starts and this project will be included on the Residence Life website under the sustainability tab.',
'created_at' => '2017-08-31 22:34:19.000000',
'project_id' => 595,
),
63 => 
array (
'netid' => 'rlhornby',
'scope' => 'Project Description:
Native bat populations are losing their habitats at an alarming rate due to human expansion into their native ranges.  Per the Bat Conservation International (BCI) many species are protected by the federal Endangered Species Act and Arizona state law.  Bats are often misunderstood and are harmless to people in general, however the benefit they give to us on a regular basis is grossly underestimated.  Bats are important to the environment on many levels; they are pollinators, are a huge insect controller (consuming their body weight in insects daily) and they also produce a rich, natural fertilizer.  The plan will be implemented by College of Architecture, Planning, and Landscape Architecture (CAPLA) students building a box, and placing it in the UA Community Garden.  The project coordinator has attained a full set of plans from the BCI that can easily be constructed in the wood shop available through CAPLA.  To bring awareness to the Green Fund, the Green Fund logo will be placed on the box itself.  Efforts will help impact sustainability through various means:  lowering the use of pesticides, protecting people from mosquitos that may carry disease, aiding in the pollination of various plants on campus and surrounding areas, helping to protect the bat population, and hiring fellow students to implement the project.  In addition, the garden can use the bat droppings to help add nutrients to their composting efforts.  This project will also help to educate visitors about the ecological services that bats provide in our region. 

UA Student Involvement/Benefit

Administrative Co-Leader (1):  Project coordinator, Rachelle Hornby, managed and planned the implementation of the project.   After the project was approved, per the Green Fund Committee, she contacted the Edge Committee and set a time to meet with them.  She presented the project to the Committee to attain support and approval from them.  They were very receptive and excited about the project, and indicated that depending on the outcome of the use of the rocket box they could possibly implement more boxes throughout campus to aid in mitigating challenges they face with bats within the football stadium.  During the meeting, a connection was made with the Vice President of Facilities Management, Mr. Kopach.  He was in total support of the project and indicated that facilities management would be happy assist in implementing the project, and to make the installation safe and viable.  A few days later Mr. Kopach, a team of gentlemen from facilities management, the project coordinator, and her professor Omar Youseff met at the UA Community Garden and discussed strategies to make the project safe and successful.  They discussed the best materials to use for the project and not only ordered them, but also delivered them directly to CAPLA.  In addition, they discussed that the implementation of the pole would be done through facilities management due to safety issues, and that they would be happy to participate by the transfer of the remaining money the grant had provided after other finances were taken into account.  Facilities management did indicate that their services would surpass this amount, but were willing to do the work regardless.   The project coordinator then organized the building of the rocket box with fellow students.   In addition, she contacted the UA Community Garden and worked with them to determine the best location to support their efforts.  Facilities Management installed the pole in the community garden.  A Green Fund Logo was created to affix on the box, however the box was installed before this was implemented.  The coordinator will contact facilities management in order to determine how the logo can be affixed in the near future.  Due to finances and other extenuating circumstances the final information sign was not implemented, however students at CAPLA will create a sign in the near future and absorb the costs in order to fulfill the obligation of the Green Fund agreement. 

Student Involvement (5):  Two UA student workers within the College of Architecture, Planning, & Landscape Architecture, Patricia Horn and the coordinator, Rachelle Hornby, along with two additional volunteers, Colin Kenyon and Jeff Tolin constructed the rocket box at CAPLA.  After construction facilities management attained the box to install it.  The coordinator worked with members of the UA Community Garden who were very receptive of the idea and were willing to collaborate with the coordinators efforts to aid in the project development.  Conversations as well as support were interchanged between the coordinator and members of the Sustainable Built Environments Club, Students for Sustainability, and the UA Community Garden.  (Jaclyn Mendelson, jaclynmendelson@email.arizona.edu, and Trevor Ledbetter tledbetter@email.arizona.edu).  In addition, Preston Kramer, garden manager, aided in approving where the box could be installed.',
'outcomes' => '',
'response' => 'Desired Impacts
The desired impact of this project is to raise awareness of the benefits bats actually bring to the decrease in insect population, their ability to pollinate various plants, as well as our efforts to help to support a species that is protected by state and federal governments.  The hope is also to decrease the insect population and therefore lowering the use of any possible pesticides being implemented on campus.  In addition, the hope is to also decrease the exposure to insects that could possible carry disease that could have the potential to make people ill.  An additional hope is that insects that threaten the UA Garden would be reduced as well the possibility of the pollination of various vegetation on campus and in the surrounding area.  Inviting such an intriguing guest and project into the area should draw attention to not only the bats, but also the support of the Green Fund program and their efforts to support sustainable projects on campus and throughout the community.  Funding of this project will help to show that the UA values native pollinators and sustainable solutions to pest management while also supporting wildlife populations that are under threat due to human influences.

Outcomes thus far:
Sustainability encompasses social and community efforts, and this project encompassed this aspect.  It was amazing to see facilities management going above and beyond to support a student’s project.  In addition, seeing the various UA clubs working together was also a great thing to witness as well.  Due in part to its size the box drew a lot of attention at CAPLA, which started many conversations which lead to educating students on bats, their benefits, and the benefits the box can potentially bring in the future.  The implementation of this project has the potential to reach well beyond the UA campus as the coordinator passed along the knowledge she attained to students, faculty, and various staff on how they could implement boxes at their own homes.  In addition, depending on the future success of this box, it is possible more will be implemented around campus in the future.  This project has the potential to protect several bats, and to cut back on the mosquito population without the use of chemicals not only on campus, but throughout the community.
Economic and environmental impacts cannot be measured at this time, as we have to wait for the bats to inhabit the box.  According to the IBC this can take up to a year, but hopefully much sooner.   The project coordinator will contact the community garden in the near future in order to provide knowledge and support about the box.  It is anticipated that they will have people curious about the box and its functions, and it is important they have answers, as they will have the most interaction with the box from here on out due to the fact it is in the garden.
The support that was given by the Green Fund Committee was much appreciated.  Guiding the coordinator in making connections with the correct departments in order to implement the project were instrumental in the progress and completion of the project.',
'created_at' => '2017-09-01 02:49:47.000000',
'project_id' => 688,
),
64 => 
array (
'netid' => 'craigbal',
'scope' => 'As of today, our project is complete and fully functional. We have created a workshop that is designed to educate students on hands on S.T.E.M. learning. We have designed and fabricated a system that is mobile and able to be used in a variety of conditions. At this point, we have not presented the intended workshop to any students, due to the summer holidays. We hope to impact students this fall with workshops on the mall and in local high school classes, possibly presenting at professional organization meetings, such as the Society for Range Management. Demonstrating the ease with which S.T.E.M can be used in an educational setting to teach about renewable resources and their benefits to society, the environment, and our campus will aid the Green Fund’s overall goal of a more sustainable future while bettering students’ skillset for building useful tools. This will be a great tool for students to see and understand renewable energy and the S.T.E.M. behind it.',
'outcomes' => '',
'response' => 'Impacts- No known impact, due to lack of presenting availability. During this spring, our project will be presented inside ten agriculture classrooms in Arizona high schools. We anticipate over 100 students per school will be exposed to this project, which will mean at least 1,000 students will learn about the goals and principles of renewable energy use, which we hope will transfer into scholarship at the University of Arizona, pursuing education that will allow them to start their careers with sustainability in mind.    Outcomes- Creation of system in a step-by-step guide for others to utilize. Creation of a list of potential beneficiaries. Goal to reach many educational institutions and extension agencies to further literacy of renewable energy use that can be applied in real world situations.',
'created_at' => '2018-01-18 01:29:35.000000',
'project_id' => 663,
),
65 => 
array (
'netid' => 'erinscott',
'scope' => 'After the successful first two installments of Environmental Art’s annual gallery Plating the Desert (2016) and Just Add Water (2017) the Environmental Arts Committee focuses on a new topic, borders. The annual gallery aims to curate a collection of mediums from artists across Tucson communities to create a broad range of voices centered around a particular subject in the broadness that is sustainability. Following a similar path and working with the two last project’s faculty from the University of Arizona, Tucson High, and the local Tucson art community, they will collaborate to create a new exhibit around sustainability and borders, focused on the Southwest. 
This year’s gallery is centered on disrupting psychological, physical and environmental borders that compartmentalize individuals and alter ecosystems. We will host the gallery in a “pop-up” fashion (only open one night) in the UA Community Garden to attract people to the event and community space. Our goal is to engage students with the idea of borders in every aspect of their lives. Students are encouraged to create artworks in mediums ranging from performance art to ceramics; each piece will explore the crucial bonds held in ecosystems and in human connection, as well as the fragility of those bonds when segmented by arbitrary borders. Examples will include illustrations inspired by physical borders, photographs depicting examples of social segregation in the southwest, and ceramic sculptures influenced by local environmental artists.
The public will access via art important environmental science topics surrounding borders, such as disrupted ecosystems. The students are encouraged to focus on one or more scientific or social topics of borders, and use their art to explain and/or enhance understanding of that topic. Ultimately, the final exhibit will explain the state of borders in the Southwest through art, and emphasize the importance of sustainability. At the end of the night the Environmental Social Justice Committee from Students for Sustainability will facilitate a discussion of the artwork and the borders theme to solidify the concepts presented for the attendees.',
'outcomes' => '',
'response' => 'Inspire University and high school students to think about their environment and art
Encourage conversations and collaboration between communities
Pursue important environmental issues using art as a creative, educational medium
Create strong relationships between Students for Sustainability, Environmental Arts Committee and local communities for future projects
Provide the unique opportunity for Tucson High School students to venture into a professional collaboration with University level professors.',
'created_at' => '2018-04-30 17:13:09.000000',
'project_id' => 748,
),
66 => 
array (
'netid' => 'jiuliano',
'scope' => 'Spring Fling is the largest student-coordinated carnival event in the United States. Spring Fling draws over 30,000 participants each year, with a majority of those guests being UA students. An event of this size has a large environmental impact through increased use of water, electricity, waste, and transportation. However, Spring Fling provides the UA a chance to demonstrate its desire to decrease the environmental impact of Spring Fling while simultaneously improving the experience of event attendees. This Mini Grant seeks $750 for the staffing and operation of a bike valet across from the entrance at Spring Fling. The goals of this project are 1) to reduce the impact of vehicle parking from Spring Fling on adjacent neighborhoods; 2) to provide a free, convenient, and secure bicycle parking option for attendees; 3) to improve the student and community experience of Spring Fling; and 4) build on the successes of the previous year’s bike valet program. For Spring Fling attendees seeking a flexible, healthy, affordable, secure, and sustainable transportation option, the bicycle valet is an unparalleled choice. We believe it is important to make green initiatives normative on campus, and the way to ensure that is to continue to bring in these green initiatives to campus events.

Last year we served over 90 people during the three days. 56 users participated in our survey. We found 40 of these users to be new valet users; 37 users were UA students and 14 were community member; and finally users cycled 222 miles roundtrip to reach the Spring Fling. We acknowledge there are bicycle racks nearby. However, this requires people to bring a lock, which can be difficult to do since bags are not allowed at Spring Fling, and increases the risk of theft or vandalism during the evening hours. The valet provides a secure and easy to use service- no locks required and the risk of theft is eliminated.',
'outcomes' => '',
'response' => '1.) Reduce the impact of vehicle parking from Spring Fling on adjacent neighborhoods. Each bicycle will be counted and each user will be asked to fill out a short survey asking where they rode from, how they heard about the valet, their purpose for coming to Spring Fling, and if they used the valet in previous years. Each bicycle parked at the valet represents one fewer car driven to, and parked at, the event.
63 people used the valet over the weekend. Unfortunately the tally sheet disappeared during clean up and transition. These numbers were generated by asking members who worked the valet approximately how many bikes they saw present. However, these numbers are a bit lower compared with previous year’s usage 80 used the valet in 2016 and 140 used it in 2017. The decline in usage between 2018 and 2017 could be attributed to the lack of good marketing. 
Survey response was spotty during the event. This may have been due to windy conditions, the optional nature of the survey,  and other factors. However, those who did complete the survey biked at least half a mile to reach the event, were predominately students, and attended for a mix of reasons. There was also little consistency in how users found the valet the most common being through PTS or just riding by it. This indicates we need a more focused marketing plan for the valet.  


2.) Provide a free, convenient, and secure bicycle parking option for attendees. The bicycle racks and ped rail will be provided from PTS. Tent, chairs, table, and banners will be provided from ASUA. Marketing will be through social media and Daily Wildcat via the Office of Sustainability and UA Cycling Team. These in-kind efforts keep the cost of the valet low. The $800 will be used to pay for the hours staffing the valet. Over 80 hours (2-3 students over 28 hours) will be worked during the Spring Fling. The valet fits with two of the overall missions for Spring Fling, which are to operate as a fundraiser and leadership opportunity for student organizations. Ours goes a step further by helping the event lower its environmental impact. 
The majority of the time we had two members present to help run the valet, ensure bikes remained safe, and answer any questions. Funds will be used to pay each the two clubs- the UA Cycling Team and UA Triathlon Team- for the hours worked. Each team will receive $400 and will be utilized for event promotion in the Tucson area. This helps get more people on bikes in our region. 


3.) Improve the student and community experience of Spring Fling. The existence of the valet demonstrates to community members that the UA cares about their health, the safety of their belongings, and the environment. 
The existence of the valet demonstrates to community members that the UA cares about their health, the safety of their belongings, and the environment. We noted numerous repeat customers over the weekend who value the valet because it gives them an additional positive experience with Spring Fling. 

4.) Build on the successes of the previous year’s bike valet program. This will be done through increased marketing and onsite signage. 
We utilized banners on the valet racks, the tent, and A-frame message boards to grab people’s attention. Members who worked this event in the past commented on seeing repeat users from previous years. This demonstrates that the valet has become ingrained into the community- people look for it and expect it to be there. 

Digital survey to ensure higher response and use less paper. This could be done utilizing an iPad or other tablet device. This would also make compiling and analyzing the results easier. 
Require survey to use the valet. This will give us an accurate count and better statistics. 
Apply for grant funding earlier in the year. We received notice of funding the week of Spring Fling, which gave us a very short marketing window. By applying for funding in February or March we would be able to market the valet better. 
Develop a better way to ensure bikes aren’t left sitting too long after closing. Often this can result in our members at the valet past 11:30pm on Saturday or well past closed on Sunday.',
'created_at' => '2018-04-30 17:17:13.000000',
'project_id' => 758,
),
67 => 
array (
'netid' => 'pijanowski',
'scope' => 'The Green Career Fair is a Students for Sustainability initiative to provide guidance and resources for students in environmental related majors or interested in sustainability.  Although the University of Arizona holds a general career fair, and individual colleges sometimes host events, there is no sustainability/environmental-specific event for students.  The University of Arizona offers over 250 Green Degrees, as advertised on the Institute of the Environment’s Green Degree Guide website, which overall sums to a significant portion of the student body.  Currently, the direct career-oriented needs of these students aren’t being properly met, but the Green Career Fair seeks to fill this gap by giving students necessary in-person exposure to volunteer, internship, and job opportunities, as well as a chance to network with potential employers.
We have already formed contact with many companies and organizations who are eager to participate.  Unfortunately, many could not afford to pay an entrance fee of $150-250 to cover operational costs because they are small non-profits and locally based government organizations.  In subsidizing or waiving the fee, we will be able to host a wide variety of organizations including Arizona Game and Fish Department, City of Tucson Environmental Services, Biosphere 2, the Center for Biological Diversity, Sky Island Alliance, Saguaro National Park, and other excellent organizations that promote the same sustainability values and environmental passions the University of Arizona also endorses.
We hope to make this the first year of many Green Career Fairs to come that expands students’ awareness of career prospects, and helps them bolster their resume.  Students will learn about sustainability related fields for work after college, gain resources that allow these students to become more competitive applicants, and put them in the best position to achieve their career goals.',
'outcomes' => '',
'response' => 'Administrative Co-Leaders (1): Courtney Pijanowski
Co-director for Students for Sustainability (SFS), Courtney Pijanowski, will have an 
active role in planning, organizing and facilitating cooperation with Student Engagement and Career Development, the companies and the coordinators to ensure the project timeline is met in a timely and professional manner. 
Coordinators (7): 
Seven other students from SFS will help plan and organize the event, contact potential 
businesses and assist with grant proposal writing. Student coordinators will strengthen their networking and communication skills by reaching out with prominent leaders within the community. Coordinators will also gain experience writing a grant proposal. 	
Student Volunteer (15): 
The administrative leader, coordinators and members from SFS will be available to 
facilitate the event. The student volunteers will assist the coordinators with setting up, cleaning up and organizing the food. The student volunteers will learn how to facilitate a public event and interact with people in a professional setting. 
Experiential Learner (50):
Students attending the career fair will develop a deeper understanding of a variety of businesses that focus on sustainable practices. Students who are interested in working with an eco-friendly company can learn about companies with similar ideologies of sustainability. Undergraduates and graduate students looking for employment can be exposed to new companies.  

There were 100 students who attended the career fair and I think all of them were thrilled with it. All of the employers offered great feedback for the fair, so hopefully we can make it even bigger next time.',
'created_at' => '2018-05-04 19:07:13.000000',
'project_id' => 735,
),
68 => 
array (
'netid' => 'abbylohr',
'scope' => '“What do we want?” A water bottle refill station
A MEZCOPH College Environment Committee (CEC) survey distributed to students, faculty, APs, and staff overwhelming ranked refillable water bottle stations as the number one physical space change they would like to see made to Drachman Hall A. The station is located on the first floor near the classrooms where there is the greatest student traffic in the building including 1475 undergraduate and 354 graduate public health students as well as many pharmacy and nursing students.

We did not alter our project summary.',
'outcomes' => '',
'response' => 'In our application, we said our desired impacts and outcomes were to promote the use of the water bottle re-fill station.  Using a pre-post survey, we measured changes in student behaviors. The station was installed on February 28, 2018.  We administered the pre survey one week before the installation of the water re-fill station and the post survey two months after.  Our goal was to have 40 students complete both the pre and the post survey.  We are excited to report that 98 students completed both surveys. 
Results: 
Demographics: In our survey results, over half of the students identified as undergraduate students and 40% said they were graduate students. The remaining respondents affiliated themselves as both staff and students or other.  The majority of respondents (96%) work or study at the College of Public Health.  Consequently, the most respondents (84% pre, 88% post) said they spent most of their time in Drachman Hall A.  
Behavior Change: In the pre survey, 49% of respondents said when they are on campus, they drink water most often compared to 56% in the post survey. Twenty eight percent of pre survey respondents compared to 38% of post survey respondents said they refilled their water bottles at the Health Sciences Campus one to three times per week. When asked where they refill their water bottles, in the pre survey, 62% said Drachman Hall A compared to 85% in the post survey. We also asked what respondents used to fill their water bottle. In the pre survey 61% said they use a standard water fountain and 28% said they use a refillable water bottle station. In the post survey, 13% said they use a standard water fountain and 80% said they use a refillable water bottle station.  According to the digital display in our water refill station, since installation in late February, 3300 bottles have been refilled there.

In addition to the quantitative questions, we provided space for qualitative responses by asking respondents if they had additional comments.  Some of the comments include:
“So happy about the re fillable water station in Drachman A side!”

“I love our refillable water station. Thank you!!!”

“Many of the students at MEZCOPH carry refillable water bottles with them every day and using a traditional water fountain does not allow for efficient filling. The new fountain is great for students who do have refillable water bottles and I know that my peers and I are grateful for the new resource.”

“I am an MPH student but work in the College of Pharmacy. Though I sometimes refill my bottle at the drinking fountain in Drachman B, I MUCH prefer to use the water station in Drachman A. I will go out of my way to use it if I have time.”

“Thank you so much for adding the refillable water bottle station. It has made drinking water on campus much more pleasurable.”

“I love that all over the place besides on [main] campus that more facilities are implementing refillable water bottle stations. This makes it so easy to always keep my hydro flask with me!”

Discussion: We were overwhelmed by the positive response to the refillable water station in Drachman Hall.  Students were excited and happy to have access to this new resource.  We recognize that we surveyed a small portion of the student population and that these results may not be generalizable across the entire College of Public Health student population.  Nonetheless, we were pleasantly surprised to see the dramatic behavior changes reported in our survey, especially the jump from 28% to 80% refillable water bottle station users.  We believe that the new refillable water bottle station will continue to encourage College of Public Health students to use reusable bottles, drink water instead of other less healthy options, and stay hydrated in our desert environment for years to come.',
'created_at' => '2018-05-10 18:58:17.000000',
'project_id' => 736,
),
69 => 
array (
'netid' => 'lossanna',
'scope' => 'The Decolonizing Sustainability Panel Series was a collaborative event between five UA Cultural and Resource Centers and the EcoSoJo committee of Students for Sustainability.  The only changes to the project summary in the proposal was that 1) NASA unfortunately could not collaborate with us, so we only had five panels instead of 6; 2) some panelists preferred to have their stipend go towards food costs; 3) some panels had only two panelists instead of three.  The project summary from the proposal is given below.


Traditionally, the sustainability movement has been predominantly focused towards
economically, socially, and educationally privileged populations, which excludes and invalidates the valuable experiences from marginalized communities. These communities are marginalized as a consequence of white imperialism and the resulting colonization. Evidence of these historical events can still be seen today in many facets of our society, including the sustainability movement. The Decolonizing Sustainability Panel Series will educate and empower individuals by addressing cultural ignorance through a collaborative project between ASUA Students for Sustainability and the UA cultural centers: African American Student Affairs, Asian Pacific American Student Affairs, Guerrero Student Center, LGBTQ+ Resource Center, Native American Student Affairs, and Women’s Resource Center. Each cultural center will host a panel event discussing what sustainability/environmentalism means to their respective communities and identities; how they have been traditionally included and excluded from the sustainability movement, and other intersections between the environment and their cultures. 

Each center will choose 3 speakers: a student, staff/faculty member, and community member to present on their panel. Because these speakers are generally unpaid activist volunteers, it is important to provide a $50 compensation for each of the three speakers for each event. This compensation will ensure quality speakers and express gratitude and support for their work. We are following the model of a $50 compensation for panelists set forth by the Women in Green Leadership Panel from last year. This series was highly successful because the compensation attracted a high-caliber, diverse group of speakers.
The panel series will be open to the UA and Tucson community, and will occur monthly in Spring 2018 for a total of six events. The anticipated schedule (subject to change) is as follows:
January - WRC
February - GSC
March - NASA
March - LGBTQ+
April - APASA
May - AASA

Decolonizing Sustainability will challenge all participants to consider sustainability issues at the UA and in the Tucson community through a wider, more inclusive lens, thus inspiring students to further integrate diversity and intersectionality in our sustainability efforts. Students from all cultural centers and the student body will be encouraged to attend each event, to familiarize themselves with the cultural center’s efforts, and better foster intercultural solidarity. Moving forward, we hope that these events will inspire future projects between cultural centers and Students for Sustainability.',
'outcomes' => '',
'response' => 'One of our main goals with this project was to build a better working relationship with all the cultural centers, which was successfully achieved except for with NASA, who unfortunately did not have the time to host a panel this semester.  SFS plans to hold more collaborative events and projects with the centers in the future.  Our other main goal was to shift the mindset of the sustainability movement away from one that focused only on natural resources and the environment, but rather a movement that considered how marginalized communities are disproportionately affected.  SFS members who are more familiar with sustainability learned about the specific environmental problems different communities and identities face, such as gentrification and environmental racism.  Most SFS members had never visited a cultural center’s space previously, but through this project they also learned about these resources offered on campus.  People associated with cultural centers learned how the sustainability movement can better serve a broader audience, including themselves, and how there are people within the movement fighting to make it more inclusive and diverse.

One area that could have used improvement was attendance of the events.  With more marketing resources, we could have reached more people who weren’t associated with SFS or a cultural center, which would have increased the impact of this project.  Although we had wonderful discussions at all the events, they could have been more meaningful if more people who were less aware about sustainability and social justice also attended.',
'created_at' => '2018-05-10 19:57:18.000000',
'project_id' => 737,
),
70 => 
array (
'netid' => 'pkramer',
'scope' => 'The University of Arizona Seed Library is a joint venture between the University of Arizona Libraries system and the University of Arizona Community Garden and will be the first Seed Library on the U of A campus. The impetus for the creation of the seed library was the declaration of the university and the surrounding area as a food desert. The Seed library seeks to alleviate the negative aspects of the food desert by providing access to free fruit and vegetable seeds, educational materials concerning gardening practices, recipes, and seed saving, and workshops on sustainable agriculture and seed saving in the community garden. The mission of the library is to increase access to healthy, nutritious food in the university community and educate people of all ages on sustainable agriculture and gardening practices. If successful, the garden will render the University community more sustainable by contributing to reduced occurrences of food insecurity, increased production and consumption of local produce that will reduce the level of carbon emissions associated with Tucson community food consumption.
The Seed Library has developed a partnership with Native Seeds/SEARCH, a local seed store devoted to providing sustainable, arid land adapted seeds to Southern Arizona, and has received an initial donation of 30 seed packs from the group to begin the library seed inventory.
The library will be housed in the Science and Engineering library on the U of A main campus, and will be managed by library front desk staff. Informational pamphlets will be printed and placed near the library for users and passers-by. The pamphlets will contain information on sustainable gardening practices in the Tucson climate, growing seasons, and seed saving. Along with the pamphlets, the seed library will have a companion website that contains additional information on growing and saving specific seeds, preparing healthy meals, and sustainable gardening.',
'outcomes' => '',
'response' => 'With the first set of seed donations and seed purchased with Green Fund assets, the UA Seed Library hopes to have 300 individuals utilize the seed library in the first year. Students will be the main focus of any advertising, so ideally at least 30% of users will be students. The seed library also hopes to have 500 individuals access the informational website and use informational brochures in the same timeframe.  
The UA seed library will be housed in the Science and Engineering Library on the main campus, readily visible to all who enter the building. This visibility will hopefully generate use. The UA Community Garden will advertise the seed library to the 1,000+ individuals who follow the garden on social media platforms. The garden will also host workshops in the community garden space in the name of the library to increase awareness. Student workers will generate social media accounts for the seed library and place flyers for the library in dorms to specifically reach out to students. 
The library system has allocated two electronic tablets to be placed at the seed library. These tablets will have a survey open where patrons will list their names, what seeds they are utilizing, and their affiliation to the university (student, faculty, community member). This data will not only be used to measure how many people utilize the library and the percentage of users who are students, but also be used to track which seeds are most often withdrawn. This data will help student workers decide what seed to request from seed donation groups in the future. Traffic to the website will be automatically tracked to determine the number of visitors, unfortunately this data will not differentiate between students and others
The seed library was more popular than originally hoped. The manager of the seed library initially put out 300 seed baggies. They were all rented within six weeks. Over 60 individuals checked out seeds. They checked out between 1 and 10 baggies at a time. Over 50% of the users were students, which exceeded our expectations. Around 45% of users were U of A staff and faculty, and another 5% were unaffiliated with the university. The seed library companion website is up and running and accessible through the U of A libraries main website. The social media accounts have not yet been created for the seed library, but the seed library has been extremely popular in spite of this lack',
'created_at' => '2018-06-04 15:57:21.000000',
'project_id' => 722,
),
71 => 
array (
'netid' => 'margob',
'scope' => 'This proposal requested funding to run a zero waste effort for the catered lunch area on the UA Mall during Admitted Student Day on April 14, 2018. The funding was used to pay for the additional cost of compostable paper plates for serving food. This enabled the event staff to more comprehensively compost almost all of the waste from the catered lunch. Event staff stationed at lunch were trained to separate non-compostable plastic cutlery from the plates/food which were compostable. Compost bins used for zero waste basketball games in McKale Arena were used for this event. The event staff managing waste stations were student volunteers as part of the overall Admitted Student Day event.

Admitted Student Day was an all-new event at UA this year. There were over 5,000 prospective students and parents in attendance, with a large rally in McKale in the morning. Guests then enjoyed booths and activities on the UA Mall, as well as speakers and campus tours throughout campus throughout the day. This included a catered buffet-style lunch on the mall for all attendees.',
'outcomes' => '',
'response' => 'At Admitted Student Day alone, there were over 5,000 students and parents. Of those, 4433 checked in and received tickets for lunch. There were additionally over 200 volunteers, presenters and college staff that attended lunch. UA had a compost rolloff on-site as part of the Spring Fling zero waste activities. All compostable waste from the Admitted Student Day lunch also went into this roll-off. Therefore, we are unable to quantify the total amount of waste composted from this event alone, but we will have aggregate weight for the entire weekend’s combined zero waste activities. 

Admitted Student Day was also a test event for the Student Union\'s new project with Campus Pantry. From the leftover food, we were able to give 100 hot dogs, 100 hamburgers and 200 cookies to the campus pantry. 

Lastly, Admitted Student Day also served as a way of provided student engagement, and exposure for both the Green Fund and sustainability efforts on campus. Prior to the event, the Green Fund logo was placed on the Admitted Student Day website, and the event app. Then of the total number guests, 1,938 of those were prospective students and over 100 were current student volunteers who were able to engage in the composting process. This event also served as a way for students to be engaged with the Office of Sustainability, which provided a tour and presentation as part of day\'s activities.',
'created_at' => '2018-06-06 17:34:15.000000',
'project_id' => 761,
),
72 => 
array (
'netid' => 'tplasse',
'scope' => 'The project proceeded as proposed. We have constructed and installed an autonomous, solar powered humidification system that will allow us to more efficiently produce mushrooms, a sustainable protein source.

In using the success of this production space we will be able to move forward with a student internship program focused on mushroom production. These interns will learn how to pasteurize, inoculate, and fruit mushrooms which will bolster their knowledge of controlled environment food production as well as efficient, and therefore sustainable protein production.',
'outcomes' => '',
'response' => 'By installing this solar-powered system, the many interns, externs, and student workers that pass through Tucson Village Farm each year will be able to see a sustainably powered food production space and learn about how to grow food with minimal impacts. Roughly 30 students per spend a semester or more at the farm helping to facilitate programming, which this new grow space is now a part of. 

When the fall semester begins, we will enfold this space into our K-6 field trip program, and utilize these student workers to teach attendees of the field trip program about sustainable food production and solar power. In teaching youth, these student workers will gain a better understanding of the systems involved in sustainable food production.

In the fall semester we also will be re-starting our partnership with the student mushroom club, The MycoCats. These students will have the opportunity to volunteer or intern in our mushroom production program—facilitated by these Green Fund Monies—and experience vertically-intergraded mushroom production.

Lastly, the public facing component of this project is the distribution of mushrooms to the public through Tucson Village Farm\'s U-Pick market. By purchasing these mushrooms as a culinary product, Tucson residents experience a novel, nutritious food grown on site. They have the opportunity to visit the mushroom production shed and see, much like our student workers, how food can be produced using renewable resources. 

Much of these impacts will occur starting in the fall semester and going on in perpetuity. Such a time will be when the largest number of students and members of the public will be directly impacted by this project. This summer, the clearest direct impact was the help that our externs here at Tucson Village Farm contributed to the installation of this project. We currently have 5 student externs working with us at the farm, and their help in getting this project up and running has allowed them to learn about sustainable energy. The number below reflect these students, but not the students that will be impacted during the upcoming semester and beyond.',
'created_at' => '2018-06-28 17:53:12.000000',
'project_id' => 717,
),
));
        
        
    }
}