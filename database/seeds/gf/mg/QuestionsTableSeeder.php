<?php

use Illuminate\Database\Seeder;

class MgQuestionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        
        \DB::table('questions')->insert(array (
            0 => 
            array (
                'id_old' => 39991,
                'question_text' => 'Dear Ed,

We as a committee have a couple of questions about this project. Is the fair open to other students outside of the event? If so, what groups would you reach out to in the campus and in the community to involve them in the fair?

Thanks,

The Green Fund Committee',
                'netid' => 'natalieramos',
                'deadline' => '2014-09-29 20:33:47.000000',
                'created_at' => '2014-09-26 20:33:47.000000',
                'questionable_id' => 559,
                'questionable_type' => 'App\\Project',
            ),
            1 => 
            array (
                'id_old' => 39992,
                'question_text' => 'Dear Yvonne,

We as a committee have a question about your project. Do you have plans for a service project while at a campus?

Thanks,

The Green Fund Committee',
                'netid' => 'natalieramos',
                'deadline' => '2014-09-29 20:36:08.000000',
                'created_at' => '2014-09-26 20:36:08.000000',
                'questionable_id' => 565,
                'questionable_type' => 'App\\Project',
            ),
            2 => 
            array (
                'id_old' => 39993,
                'question_text' => 'Dear Tanner Jean-Louis,

We as a committee have a few questions about your project. 
- What is your long-term plan for reducing plastic bags in the union?
- Are you planning on training employees to give out less bags?
- Will anyone get a discount using a backpack or one at home? Do you only get the discount if you were to use the specific bag given out?
- Do you get the discount for single items when not using a bag?
- How are you going to specifically measure it?
- Has the UMart already agreed to this?
- How are you going to distribute it?
- Can we see the design prior to the bags being created?

Thanks,

The Green Fund Committee',
                'netid' => 'natalieramos',
                'deadline' => '2014-10-10 22:41:58.000000',
                'created_at' => '2014-10-07 22:41:58.000000',
                'questionable_id' => 568,
                'questionable_type' => 'App\\Project',
            ),
            3 => 
            array (
                'id_old' => 39994,
                'question_text' => 'Dear Maya,

We as a committee have a couple of questions about your project "Green Fund Photographer":
1. How much would you pay these students?
2. Would they be graduate or undergraduate?

Thank you,

The Green Fund Committee',
                'netid' => 'natalieramos',
                'deadline' => '2014-10-25 22:53:16.000000',
                'created_at' => '2014-10-22 22:53:16.000000',
                'questionable_id' => 571,
                'questionable_type' => 'App\\Project',
            ),
            4 => 
            array (
                'id_old' => 39995,
                'question_text' => 'Dear Maya,

We as a committee have a couple of questions about your project "Green Fund Presentation Guide":
1. What are the deliverables for this student?
2. How did you estimate the value of this student?

Thank you,

The Green Fund Committee',
                'netid' => 'natalieramos',
                'deadline' => '2014-10-25 22:55:43.000000',
                'created_at' => '2014-10-22 22:55:43.000000',
                'questionable_id' => 572,
                'questionable_type' => 'App\\Project',
            ),
            5 => 
            array (
                'id_old' => 39996,
                'question_text' => 'Dear KC,

We as a committee have a couple of questions about your project "Buckle Up to Recycling":
1. What are other funding sources that your are looking at? We ask because your budget does not show the costs of the professional that could come out. 
2. You have listed student workers; are you planning to pay them? If so, what is the funding source?
3. What tools do you need? Do you need to purchase these tools or can you borrow them from individuals? How will these tools be used in the future?
4. Can you explain how students are being engaged aside from the numbers you gave?
5. We would like a budget justification that explains detailed math for how you came up with the numbers for your budget. 

Please make sure you review the mini grant guides when you respond to these questions(http://www.studentaffairs.arizona.edu/greenfund/documents/green_fund_mini_grant_guide.pdf).

Thank you,

The Green Fund Committee',
                'netid' => 'natalieramos',
                'deadline' => '2014-10-25 23:00:42.000000',
                'created_at' => '2014-10-22 23:00:42.000000',
                'questionable_id' => 574,
                'questionable_type' => 'App\\Project',
            ),
            6 => 
            array (
                'id_old' => 39997,
                'question_text' => 'Dear Applicant,
The Green Fund Committee would like to know how funds will be distributed; specifically who will get paid from the stipend and how much will each student receive? Thank you for your interest in the Green Fund.',
                'netid' => 'rudnick1',
                'deadline' => '2015-01-23 21:33:56.000000',
                'created_at' => '2015-01-20 21:33:56.000000',
                'questionable_id' => 566,
                'questionable_type' => 'App\\Project',
            ),
            7 => 
            array (
                'id_old' => 39998,
                'question_text' => 'Dear Project Manager,
The Green Fund Committee would like to know if you have a list of potential speakers and possible topics they might discuss? Thank you for your time.
Link will expire in 4 days.
Julia',
                'netid' => 'rudnick1',
                'deadline' => '2015-09-29 18:24:36.000000',
                'created_at' => '2015-09-25 18:24:36.000000',
                'questionable_id' => 608,
                'questionable_type' => 'App\\Project',
            ),
            8 => 
            array (
                'id_old' => 39999,
                'question_text' => 'Dear Trevor Ledbetter,

We as a committee have a couple of questions regarding your project proposal "Territorial Waste Wars". The questions are as follows:

1. What is the criteria for winning the competition?
2. How will the competition work at ASU? Will it be held in their main library as well?

Thank you for your time, 

The Green Fund Committee',
                'netid' => 'natalieramos',
                'deadline' => '2016-01-31 17:16:21.000000',
                'created_at' => '2016-01-27 17:16:21.000000',
                'questionable_id' => 634,
                'questionable_type' => 'App\\Project',
            ),
            9 => 
            array (
                'id_old' => 399910,
                'question_text' => 'The Green Fund Committee would like to thank you for your mini-grant application.  It was reviewed during last Friday’s closed meeting, and the Committee has a few additional follow-up questions below.  

This project received Green Fund funding last year.  How did last year’s event go, and are there still materials (paints, brushes, etc.) available for your use this year?

Please provide your brief written response, via the email link you received, before the expiration date and time listed.  Thank you!',
                'netid' => 'lishahall2012',
                'deadline' => '2018-02-23 21:18:26.000000',
                'created_at' => '2018-02-20 21:18:26.000000',
                'questionable_id' => 747,
                'questionable_type' => 'App\\Project',
            ),
            10 => 
            array (
                'id_old' => 399911,
                'question_text' => 'How did you engage and educate the UA campus community last year, and how do you propose to continue and expand that involvement this year?  Can you provide a bit more detail about how the sustainability “conversations” will be incorporated?',
                'netid' => 'lishahall2012',
                'deadline' => '2018-02-23 21:19:01.000000',
                'created_at' => '2018-02-20 21:19:01.000000',
                'questionable_id' => 747,
                'questionable_type' => 'App\\Project',
            ),
            11 => 
            array (
                'id_old' => 399912,
                'question_text' => 'For awareness, ASUA already has an audio speaker available to borrow for free.  How would that change your requested budget?',
                'netid' => 'lishahall2012',
                'deadline' => '2018-02-23 21:19:41.000000',
                'created_at' => '2018-02-20 21:19:41.000000',
                'questionable_id' => 747,
                'questionable_type' => 'App\\Project',
            ),
            12 => 
            array (
                'id_old' => 399913,
                'question_text' => 'The Green Fund Committee would like a bit more detail about your planning process and the timing for this event.  Could you provide a brief, basic timeline for this project?  Do you have an event date in mind?',
                'netid' => 'lishahall2012',
                'deadline' => '2018-02-23 21:20:16.000000',
                'created_at' => '2018-02-20 21:20:16.000000',
                'questionable_id' => 747,
                'questionable_type' => 'App\\Project',
            ),
            13 => 
            array (
                'id_old' => 399914,
                'question_text' => 'The Green Fund Committee would like to thank you for your mini-grant application.  It was reviewed during last Friday’s closed meeting, and the Committee has a few additional follow-up questions below.

There are several UA organizations already collecting waste (Compost Cats, UA Community Garden, etc.).  Have you been in touch with any of these organizations to discuss potentially collaborating with them?  

Please provide your brief written response, via the email link you received, before the expiration date and time listed.  Thank you!',
                'netid' => 'lishahall2012',
                'deadline' => '2018-02-23 21:31:36.000000',
                'created_at' => '2018-02-20 21:31:36.000000',
                'questionable_id' => 745,
                'questionable_type' => 'App\\Project',
            ),
            14 => 
            array (
                'id_old' => 399915,
                'question_text' => 'Do you have the infrastructure set up to receive UA green waste and convert it into biochar already?',
                'netid' => 'lishahall2012',
                'deadline' => '2018-02-23 21:32:14.000000',
                'created_at' => '2018-02-20 21:32:14.000000',
                'questionable_id' => 745,
                'questionable_type' => 'App\\Project',
            ),
            15 => 
            array (
                'id_old' => 399916,
                'question_text' => 'Can you provide a basic map, schematic or sketch of what the proposed project would look like?',
                'netid' => 'lishahall2012',
                'deadline' => '2018-02-23 21:33:04.000000',
                'created_at' => '2018-02-20 21:33:04.000000',
                'questionable_id' => 745,
                'questionable_type' => 'App\\Project',
            ),
            16 => 
            array (
                'id_old' => 399917,
                'question_text' => 'One additional question:  How would Tucson High be involved/integrated in this project?  The Committee recalled that TH students provided art in past years, and would like a bit more detail regarding your plans for engaging TS.',
                'netid' => 'lishahall2012',
                'deadline' => '2018-02-25 17:34:36.000000',
                'created_at' => '2018-02-22 17:34:36.000000',
                'questionable_id' => 748,
                'questionable_type' => 'App\\Project',
            ),
        ));
        
        
    }
}