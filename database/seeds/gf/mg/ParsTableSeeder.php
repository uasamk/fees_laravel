<?php

use Illuminate\Database\Seeder;

class MgParsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        
        \DB::table('pars')->insert(array (
            0 => 
            array (
                'id_old' => 39991,
                'netid' => 'chois',
                'changes' => 'Dear UA Green Fund,

The Plating the Desert partner team appreciates your support in our project. Since submitting our initial proposal, we’ve realized that the Associated Students for the University of Arizona (ASUA), under which Students for Sustainability is housed as a program, has a marketing department with film and videography capacity. The Multimedia Director, Brett Turner, oversees these services and has committed his team to working on the film component of our project. This change is very beneficial since we will now be able to work with a ASUA, and the services are free. 

These changes will be reflected in following language under “Project Member’s List” in the revised proposal:

Associated Students for the University of Arizona (ASUA) Marketing, Multimedia
asuamarketing@email.arizona.edu

The ASUA marketing team, under the direction of Multimedia Director, Brett Turner, will take video footage of the following: the plate-making process, interviews with all involved in the project (artists, professors, committee members, students, and the public as they interact with the plates), the opening event, and exhibit spaces. This footage will be used to create video clips and updates to promote the project along its 4-5 month duration. After the final exhibit, the film team will create a 5-7 minute video capturing the project in its entirety. 

We have rearranged our budget after considering our extra $170, and the adjustments include the following: increasing the amount of money our speakers will receive, and in addition, including another speaker; moving $100 from materials to marketing as we have plans to market extensively for this project and are estimated higher costs; and, adding money to the "Food and Table Ware" category. 

Speakers are as follows: Eric Magrane (poet), Paul Mirocha (illustrator), Bobby Long (illustrator), Kelly Watters (food co-op), Chet Phillips (ASUA Sustainability Coordinator) and a representative from the Tucson Village Farm. 

An additional adjustment to the project regards the Green Fund’s wonderful suggestion of giving the plates to Campus Pantry. The professors/teachers have all agreed that this is a great idea, and they will give the students the option to donate their plates. Katie Marascio is working as the liaison between this project and the Campus Pantry, so she will be communicating and coordinating the logistics of this. 

The revised budget is attached.

Thank you very much,

Plating the Desert Team',
                'advice' => 'This PAR submitted in error.',
                'approved' => 0,
                'vp_comments' => 'This PAR submitted in error.',
                'vp_approved' => 0,
                'created_at' => '2015-10-23 07:17:51.000000',
                'timestamp_review' => '2017-10-19 22:38:41.000000',
                'netid_review' => 'rudnick1',
                'timestamp_vp' => '2017-10-19 22:39:13.000000',
                'netid_vp' => 'rudnick1',
                'project_id' => 605,
            ),
            1 => 
            array (
                'id_old' => 39992,
                'netid' => 'dmartinezlugo',
                'changes' => 'I would like to change the budget categories and extend the time available to use some of the funds we received for the Public Political Ecology Lab Climate Justice Workshop. 

We held the workshop yesterday (April 18, 2018) in Tucson at Changemaker High School. The event was very successful, but the format was slightly different than initially proposed. We had more attendees (over 40), which made honorariums for community groups not appropriate. We also offered a full dinner because most community organizations were more available on a weeknight evening, and we wanted to make sure it was possible for people who work to attend. We therefore used more funding for catering and no funding for honorariums. 

We would like to use a small amount of remaining funds to pay a graduate student to write a follow-up report and share this with attendees and other community organizations that could not be there. After the event it was clear that a thorough synthesis of the discussion and workshop is necessary. We had an excellent notetaker, but a few follow up steps are required (these will require substantial time):
- Synthesize notes with clear next steps.
- Create a Google Group listserv to support ongoing communication between community and University groups working for climate justice.
- Encourage participation with the listserv and create momentum by using the list to share relevant events, notes, and research ideas. 

On behalf of the Public Political Ecology Lab, I would like to propose the following use of our $650 Green Fund Grant:
- Dinner Catering (for 40 attendees): $475
- Graduate Student Support to write a synthesis report, create Climate Justice Network Listserv, and follow up with participants: $175 (10 hours @ $15/hr + 13% ERE)

Finally, we would like to use the $175 Graduate Student Support funding by June 15, 2018. This will allow plenty of time for synthesis and follow-up to get the Climate Justice Network listerv going and support the early stages of the Climate Justice Science Shop. 

Thank you for considering this PAR.',
            'advice' => 'The Committee approved changing budget categories to use the remainder $650 for dinner catering at the workshop ($475) and to pay a Graduate student to write synthesis report ($175).',
                'approved' => 1,
                'vp_comments' => 'PAR approved by Melissa Vito via Email on May 3rd, 2018.',
                'vp_approved' => 1,
                'created_at' => '2018-04-19 23:46:59.000000',
                'timestamp_review' => '2018-06-08 19:09:59.000000',
                'netid_review' => 'lishahall2012',
                'timestamp_vp' => '2018-06-08 19:15:01.000000',
                'netid_vp' => 'lishahall2012',
                'project_id' => 724,
            ),
        ));
        
        
    }
}