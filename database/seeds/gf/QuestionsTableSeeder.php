<?php

use Illuminate\Database\Seeder;

class GfQuestionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        
        \DB::table('questions')->insert(array (
            0 => 
            array (
                'id_old' => 29991,
                'question_text' => 'This is a test question to see if the Green Fund website backend software is sending questions to applicants. This was sent from Joe Abraham to Joe Abraham',
                'netid' => 'jabraham',
                'deadline' => '2011-03-05 22:10:58.000000',
                'created_at' => '2011-03-01 22:10:58.000000',
                'questionable_id' => 333,
                'questionable_type' => 'App\\Project',
            ),
            1 => 
            array (
                'id_old' => 29992,
                'question_text' => 'Dear Dr. Kacira,

While reviewing your recent proposal submission to the Green Fund, the Committee wondered whether the excess energy from your solar panel might be directed towards offseting other on-campus demand rather than returned to TEP via their net metering program. Also, do you have any estimate of how much excess energy you plan to generate?

Thanks for your prompt reply, 

Matt Scholz',
                'netid' => 'scholz',
                'deadline' => '2011-03-08 02:58:16.000000',
                'created_at' => '2011-03-04 02:58:16.000000',
                'questionable_id' => 303,
                'questionable_type' => 'App\\Project',
            ),
            2 => 
            array (
                'id_old' => 29993,
                'question_text' => 'Can you please provide a more detailed budget with specific line items and parts if available for the summary activities like PV subsystem installation, electrical design and equipment, composting system materials and installation, hydroponics greenhouse supplies, and sensors and instrumentation for monitoring climatic variables?',
                'netid' => 'paburns',
                'deadline' => '2011-03-08 22:38:14.000000',
                'created_at' => '2011-03-04 22:38:14.000000',
                'questionable_id' => 303,
                'questionable_type' => 'App\\Project',
            ),
            3 => 
            array (
                'id_old' => 29994,
                'question_text' => 'Will the various installations in the proposal be done in-house or by an outside vendor? If done by an outside vendor, do you have quotes available?',
                'netid' => 'paburns',
                'deadline' => '2011-03-08 22:39:07.000000',
                'created_at' => '2011-03-04 22:39:07.000000',
                'questionable_id' => 303,
                'questionable_type' => 'App\\Project',
            ),
            4 => 
            array (
                'id_old' => 29995,
            'question_text' => 'Thank you for your application. Regarding the 2 raincatchers, the link to the model at Silverbell Nursery seems to be down. Also, regarding the math, 2 raincatchers at $763 each are totaled to $1400. Is there a discount for this purchase or do you need $1,526 ($763 x 2) for them?',
                'netid' => 'paburns',
                'deadline' => '2011-03-12 00:14:37.000000',
                'created_at' => '2011-03-08 00:14:37.000000',
                'questionable_id' => 334,
                'questionable_type' => 'App\\Project',
            ),
            5 => 
            array (
                'id_old' => 29996,
                'question_text' => 'Would it be possible to get a little further explanation of the "Smart Phones" budget line before tomorrow\'s meeting? Is there a specific phone that is being purchased in conjunction with the i-Tree Streets suite?',
                'netid' => 'ntheisen',
                'deadline' => '2011-03-12 03:38:29.000000',
                'created_at' => '2011-03-08 03:38:29.000000',
                'questionable_id' => 302,
                'questionable_type' => 'App\\Project',
            ),
            6 => 
            array (
                'id_old' => 299969,
                'question_text' => 'September 18, 2012

Dear Project Manager,

Sincerely, 


The University of Arizona Green Fund Committee',
                'netid' => 'rudnick1',
                'deadline' => '2012-09-22 17:24:41.000000',
                'created_at' => '2012-09-18 17:24:41.000000',
                'questionable_id' => 312,
                'questionable_type' => 'App\\Project',
            ),
            7 => 
            array (
                'id_old' => 299970,
                'question_text' => 'Good Afternoon,
Your PAR asks to "move $3,000 of the current balance of $3,507.75 in Personnel and $550 of the current balance of $644.40 in ERE to Operations. This will facilitate continued involvement of one key graduate student who graduated in May 2012 and one key undergraduate student who graduated in December 2011." Before our open meeting next week, we would like to ask if you could please clarify if you are moving funds from Operations to ERE and Personnel in order to continue the involvement of the graduate and undergraduate students or if you are moving funds from ERE and Personnel to Operations. If you are moving the funds to Operations we ask that you please clarify what operations these funds will be used for. Thank you for your time.',
                'netid' => 'cerdelyi',
                'deadline' => '2012-09-29 23:35:35.000000',
                'created_at' => '2012-09-25 23:35:35.000000',
                'questionable_id' => 66,
                'questionable_type' => 'App\\Par',
            ),
            8 => 
            array (
                'id_old' => 29998,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:26:10.000000',
                'created_at' => '2012-02-08 20:26:10.000000',
                'questionable_id' => 338,
                'questionable_type' => 'App\\Project',
            ),
            9 => 
            array (
                'id_old' => 29999,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:26:30.000000',
                'created_at' => '2012-02-08 20:26:30.000000',
                'questionable_id' => 351,
                'questionable_type' => 'App\\Project',
            ),
            10 => 
            array (
                'id_old' => 299910,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:26:47.000000',
                'created_at' => '2012-02-08 20:26:47.000000',
                'questionable_id' => 355,
                'questionable_type' => 'App\\Project',
            ),
            11 => 
            array (
                'id_old' => 299911,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:26:56.000000',
                'created_at' => '2012-02-08 20:26:56.000000',
                'questionable_id' => 367,
                'questionable_type' => 'App\\Project',
            ),
            12 => 
            array (
                'id_old' => 299912,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:27:14.000000',
                'created_at' => '2012-02-08 20:27:14.000000',
                'questionable_id' => 365,
                'questionable_type' => 'App\\Project',
            ),
            13 => 
            array (
                'id_old' => 299913,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:27:23.000000',
                'created_at' => '2012-02-08 20:27:23.000000',
                'questionable_id' => 344,
                'questionable_type' => 'App\\Project',
            ),
            14 => 
            array (
                'id_old' => 299914,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:27:36.000000',
                'created_at' => '2012-02-08 20:27:36.000000',
                'questionable_id' => 364,
                'questionable_type' => 'App\\Project',
            ),
            15 => 
            array (
                'id_old' => 299915,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:27:46.000000',
                'created_at' => '2012-02-08 20:27:46.000000',
                'questionable_id' => 337,
                'questionable_type' => 'App\\Project',
            ),
            16 => 
            array (
                'id_old' => 299916,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:27:58.000000',
                'created_at' => '2012-02-08 20:27:58.000000',
                'questionable_id' => 363,
                'questionable_type' => 'App\\Project',
            ),
            17 => 
            array (
                'id_old' => 299917,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:28:09.000000',
                'created_at' => '2012-02-08 20:28:09.000000',
                'questionable_id' => 354,
                'questionable_type' => 'App\\Project',
            ),
            18 => 
            array (
                'id_old' => 299918,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:28:20.000000',
                'created_at' => '2012-02-08 20:28:20.000000',
                'questionable_id' => 345,
                'questionable_type' => 'App\\Project',
            ),
            19 => 
            array (
                'id_old' => 299919,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:28:32.000000',
                'created_at' => '2012-02-08 20:28:32.000000',
                'questionable_id' => 350,
                'questionable_type' => 'App\\Project',
            ),
            20 => 
            array (
                'id_old' => 299920,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:28:44.000000',
                'created_at' => '2012-02-08 20:28:44.000000',
                'questionable_id' => 358,
                'questionable_type' => 'App\\Project',
            ),
            21 => 
            array (
                'id_old' => 299921,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:30:09.000000',
                'created_at' => '2012-02-08 20:30:09.000000',
                'questionable_id' => 339,
                'questionable_type' => 'App\\Project',
            ),
            22 => 
            array (
                'id_old' => 299922,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:30:20.000000',
                'created_at' => '2012-02-08 20:30:20.000000',
                'questionable_id' => 348,
                'questionable_type' => 'App\\Project',
            ),
            23 => 
            array (
                'id_old' => 299923,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:30:33.000000',
                'created_at' => '2012-02-08 20:30:33.000000',
                'questionable_id' => 353,
                'questionable_type' => 'App\\Project',
            ),
            24 => 
            array (
                'id_old' => 299924,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:30:47.000000',
                'created_at' => '2012-02-08 20:30:47.000000',
                'questionable_id' => 357,
                'questionable_type' => 'App\\Project',
            ),
            25 => 
            array (
                'id_old' => 299925,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:30:57.000000',
                'created_at' => '2012-02-08 20:30:57.000000',
                'questionable_id' => 356,
                'questionable_type' => 'App\\Project',
            ),
            26 => 
            array (
                'id_old' => 299926,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:31:09.000000',
                'created_at' => '2012-02-08 20:31:09.000000',
                'questionable_id' => 353,
                'questionable_type' => 'App\\Project',
            ),
            27 => 
            array (
                'id_old' => 299927,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:31:26.000000',
                'created_at' => '2012-02-08 20:31:26.000000',
                'questionable_id' => 349,
                'questionable_type' => 'App\\Project',
            ),
            28 => 
            array (
                'id_old' => 299928,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:31:37.000000',
                'created_at' => '2012-02-08 20:31:37.000000',
                'questionable_id' => 341,
                'questionable_type' => 'App\\Project',
            ),
            29 => 
            array (
                'id_old' => 299929,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:31:48.000000',
                'created_at' => '2012-02-08 20:31:48.000000',
                'questionable_id' => 342,
                'questionable_type' => 'App\\Project',
            ),
            30 => 
            array (
                'id_old' => 299930,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:32:04.000000',
                'created_at' => '2012-02-08 20:32:04.000000',
                'questionable_id' => 347,
                'questionable_type' => 'App\\Project',
            ),
            31 => 
            array (
                'id_old' => 299931,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:32:15.000000',
                'created_at' => '2012-02-08 20:32:15.000000',
                'questionable_id' => 352,
                'questionable_type' => 'App\\Project',
            ),
            32 => 
            array (
                'id_old' => 299932,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:32:26.000000',
                'created_at' => '2012-02-08 20:32:26.000000',
                'questionable_id' => 362,
                'questionable_type' => 'App\\Project',
            ),
            33 => 
            array (
                'id_old' => 299933,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:32:37.000000',
                'created_at' => '2012-02-08 20:32:37.000000',
                'questionable_id' => 336,
                'questionable_type' => 'App\\Project',
            ),
            34 => 
            array (
                'id_old' => 299934,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:32:48.000000',
                'created_at' => '2012-02-08 20:32:48.000000',
                'questionable_id' => 340,
                'questionable_type' => 'App\\Project',
            ),
            35 => 
            array (
                'id_old' => 299935,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:32:58.000000',
                'created_at' => '2012-02-08 20:32:58.000000',
                'questionable_id' => 299,
                'questionable_type' => 'App\\Project',
            ),
            36 => 
            array (
                'id_old' => 299936,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:33:11.000000',
                'created_at' => '2012-02-08 20:33:11.000000',
                'questionable_id' => 346,
                'questionable_type' => 'App\\Project',
            ),
            37 => 
            array (
                'id_old' => 299937,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:33:25.000000',
                'created_at' => '2012-02-08 20:33:25.000000',
                'questionable_id' => 368,
                'questionable_type' => 'App\\Project',
            ),
            38 => 
            array (
                'id_old' => 299938,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:33:37.000000',
                'created_at' => '2012-02-08 20:33:37.000000',
                'questionable_id' => 360,
                'questionable_type' => 'App\\Project',
            ),
            39 => 
            array (
                'id_old' => 299939,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:33:47.000000',
                'created_at' => '2012-02-08 20:33:47.000000',
                'questionable_id' => 343,
                'questionable_type' => 'App\\Project',
            ),
            40 => 
            array (
                'id_old' => 299940,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:33:56.000000',
                'created_at' => '2012-02-08 20:33:56.000000',
                'questionable_id' => 316,
                'questionable_type' => 'App\\Project',
            ),
            41 => 
            array (
                'id_old' => 299941,
                'question_text' => 'Dear Applicant, 
With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-12 20:34:05.000000',
                'created_at' => '2012-02-08 20:34:05.000000',
                'questionable_id' => 320,
                'questionable_type' => 'App\\Project',
            ),
            42 => 
            array (
                'id_old' => 299967,
                'question_text' => 'Good Evening,
You PAR asks for more time- please provide the committee with a formal project end date. We would like to have this information in order to discuss you PAR request. Thank you
-Lauren Erdelyi',
                'netid' => 'cerdelyi',
                'deadline' => '2012-09-22 02:02:54.000000',
                'created_at' => '2012-09-18 02:02:54.000000',
                'questionable_id' => 62,
                'questionable_type' => 'App\\Par',
            ),
            43 => 
            array (
                'id_old' => 299968,
                'question_text' => 'Good Evening,
You PAR asks for more time- please provide the committee with a formal project end date. We would like to have this information in order to discuss you PAR request. Thank you
-Lauren Erdelyi',
                'netid' => 'cerdelyi',
                'deadline' => '2012-09-22 02:03:59.000000',
                'created_at' => '2012-09-18 02:03:59.000000',
                'questionable_id' => 64,
                'questionable_type' => 'App\\Par',
            ),
            44 => 
            array (
                'id_old' => 299943,
                'question_text' => 'With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-13 17:53:53.000000',
                'created_at' => '2012-02-09 17:53:53.000000',
                'questionable_id' => 359,
                'questionable_type' => 'App\\Project',
            ),
            45 => 
            array (
                'id_old' => 299944,
                'question_text' => 'With 34 proposals submitted requesting over $1.1M, we have decided to provide proposal authors with the option to submit supplemental guidance that would provide the Green Fund Committee with direction on the best way to provide partial funding for their projects.

The Committee reserves the right to recommend funding, full or partial, at its discretion. Providing guidance does not create an incentive for the Committee to recommend partially fund your project. Rather, such guidance may allow the Committee to more effectively provide partial funding if it determines that is necessary. Choosing to not submit guidance will not impact the Committee\'s consideration of fully-funding your proposal, but may impact its ability to recommend the best partial funding option.

The Committee has begun reviewing proposals. Please submit guidance at your earliest convenience to Julia Rudnick (rudnick1@email.arizona.edu). There will not be another opportunity to provide this information. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'rudnick1',
                'deadline' => '2012-02-13 17:56:57.000000',
                'created_at' => '2012-02-09 17:56:57.000000',
                'questionable_id' => 366,
                'questionable_type' => 'App\\Project',
            ),
            46 => 
            array (
                'id_old' => 299945,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a question.

1. The proposal describes applying for a NSF grant in July 2012. How would your project be affected if only the first budgeted year was funded, while you await a response from the NSF?

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-16 00:09:00.000000',
                'created_at' => '2012-02-12 00:09:00.000000',
                'questionable_id' => 338,
                'questionable_type' => 'App\\Project',
            ),
            47 => 
            array (
                'id_old' => 299946,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a few questions.

1. Can Parking and Transportation Services produce a letter containing a future funding commitment if the prototype becomes viable?

2. Can you provide a design of the bike shelter to the Committee and what is the bike capacity?

3. In the "Additional Comments" section there is $425 for "(2) 85 Watt photovoltaic panels", how does this differ from the $3,700 for the "2 photovoltaic panels" under the "Total ERE" section? Please clarify these budget numbers.

4.Would you be willing to move this prototype project to a space serving the greatest number of students? (e.g. Koffler or the main library)

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-16 00:12:13.000000',
                'created_at' => '2012-02-12 00:12:13.000000',
                'questionable_id' => 351,
                'questionable_type' => 'App\\Project',
            ),
            48 => 
            array (
                'id_old' => 299947,
                'question_text' => 'Dear Applicant, 

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a question.

1. How does this funding request relate to funding awarded last year that will be used in the second year of the project? (Is it possible to incorporate this project into last year\'s funded project?)

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-16 00:14:37.000000',
                'created_at' => '2012-02-12 00:14:37.000000',
                'questionable_id' => 355,
                'questionable_type' => 'App\\Project',
            ),
            49 => 
            array (
                'id_old' => 299948,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a question.

1. How does the limitation of no funding before mid July 2012 impact this project?

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-16 00:15:49.000000',
                'created_at' => '2012-02-12 00:15:49.000000',
                'questionable_id' => 365,
                'questionable_type' => 'App\\Project',
            ),
            50 => 
            array (
                'id_old' => 299949,
                'question_text' => 'Dear Applicant, 

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a question.

1. How does the limitation of no funding before mid July 2012 impact this project?

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-16 00:16:42.000000',
                'created_at' => '2012-02-12 00:16:42.000000',
                'questionable_id' => 363,
                'questionable_type' => 'App\\Project',
            ),
            51 => 
            array (
                'id_old' => 299950,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a question.

1. The proposal describes applying for a NSF grant in July 2012. How would your project be affected if only the first budgeted year was funded, while you await a response from the NSF?

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-18 17:54:42.000000',
                'created_at' => '2012-02-14 17:54:42.000000',
                'questionable_id' => 338,
                'questionable_type' => 'App\\Project',
            ),
            52 => 
            array (
                'id_old' => 299951,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a few questions.

1. Regarding your budget, can you please clarify the “Other Projected Sources of Funding” table? (Why is $5,500 added instead of subtracted?)
2. If your project is approved, funding will not be dispersed until mid July. How will this impact your project?

* A PAR will need to be submitted in order to roll over funds from a previous year

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-21 23:17:59.000000',
                'created_at' => '2012-02-17 23:17:59.000000',
                'questionable_id' => 354,
                'questionable_type' => 'App\\Project',
            ),
            53 => 
            array (
                'id_old' => 299952,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a few questions.

1. Can OSCR and UITS provide the Green Fund with documentation expressing their commitment and support for this project?
2. How does this software work? (e.g. Does it shut down computers at night?)
3. Does this project have a plan for implementing and installing 2,200 licenses onto the computers?

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-21 23:26:59.000000',
                'created_at' => '2012-02-17 23:26:59.000000',
                'questionable_id' => 350,
                'questionable_type' => 'App\\Project',
            ),
            54 => 
            array (
                'id_old' => 299953,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a question.

1. Does this project have a plan for marketing and implementing this app once it’s completed?

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-21 23:28:06.000000',
                'created_at' => '2012-02-17 23:28:06.000000',
                'questionable_id' => 353,
                'questionable_type' => 'App\\Project',
            ),
            55 => 
            array (
                'id_old' => 299954,
                'question_text' => 'Dear Applicant, 

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a few questions.

1. What is the long-term goal of this project?
2. What is the likelihood of FM and/or the Athletics Department cosponsoring this project in the years to come? Has communication for this begun?

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-21 23:29:35.000000',
                'created_at' => '2012-02-17 23:29:35.000000',
                'questionable_id' => 357,
                'questionable_type' => 'App\\Project',
            ),
            56 => 
            array (
                'id_old' => 299955,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a few questions.

1. Is there an ethical way to reverse engineer someone else’s product into your own with intensions to sell that product? What repercussions can this have on the University of Arizona?
2. What is the project’s timeline for Phase 1?
3. What are the project’s plans for Phase 2 and 3?

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-21 23:34:29.000000',
                'created_at' => '2012-02-17 23:34:29.000000',
                'questionable_id' => 356,
                'questionable_type' => 'App\\Project',
            ),
            57 => 
            array (
                'id_old' => 299956,
                'question_text' => 'Dear Applicant, 

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a few questions.

1. Please provide a breakdown of the $12,500 requested for laboratory tests.
2. If the project deems the water to be unsafe, what will happen to the infrastructure of this project?

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-28 17:05:49.000000',
                'created_at' => '2012-02-24 17:05:49.000000',
                'questionable_id' => 366,
                'questionable_type' => 'App\\Project',
            ),
            58 => 
            array (
                'id_old' => 299957,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a few questions.

1. How do you plan on getting the compost from AZ Valley Compost and transporting it to the garden for the first year?
2. Do you see any more marketing or branding for the University of Arizona that could be imposed on this project, if additional funding was provided?

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-28 17:06:52.000000',
                'created_at' => '2012-02-24 17:06:52.000000',
                'questionable_id' => 347,
                'questionable_type' => 'App\\Project',
            ),
            59 => 
            array (
                'id_old' => 299958,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a few questions.

1. Please justify spending $24,000 on a new truck.
2. What type of truck, specifically, are you referring to? (make/model/etc)
3. Can you justify eliminating the bagging machine from the funding compared to the truck? (Would the bagging machine be a better step towards making the compost project self-sustaining?)

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-28 17:08:09.000000',
                'created_at' => '2012-02-24 17:08:09.000000',
                'questionable_id' => 362,
                'questionable_type' => 'App\\Project',
            ),
            60 => 
            array (
                'id_old' => 299959,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a question.

1. What type of labor is actually going to be put into this project, specifically related to the student and consulting work? Please include the estimated total hours and hourly wage for these workers.

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-02-28 17:09:07.000000',
                'created_at' => '2012-02-24 17:09:07.000000',
                'questionable_id' => 336,
                'questionable_type' => 'App\\Project',
            ),
            61 => 
            array (
                'id_old' => 299960,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a question.

1. If the research proves that implementing a solar updraft tower on campus would be beneficial, how would you go about installing the tower? Have you made contact with proper services (such as Facilities Management, planning/design/construction, etc)? Where would this be installed?

Please submit your response by March 7th, 2012. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-03-06 04:25:05.000000',
                'created_at' => '2012-03-02 04:25:05.000000',
                'questionable_id' => 340,
                'questionable_type' => 'App\\Project',
            ),
            62 => 
            array (
                'id_old' => 299961,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a question.

Could this project still be implemented with the following conditions?

1. No funding support for a new computer
2. The total amount of money allocated from the Green Fund not exceed $10,000

Please submit your response by March 7th, 2012. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-03-06 04:26:14.000000',
                'created_at' => '2012-03-02 04:26:14.000000',
                'questionable_id' => 299,
                'questionable_type' => 'App\\Project',
            ),
            63 => 
            array (
                'id_old' => 299962,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a request.

1. Please submit your project PAR for fiscal year 2012 by Monday, March 12, 2012.

Sincerely,

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-03-06 04:28:51.000000',
                'created_at' => '2012-03-02 04:28:51.000000',
                'questionable_id' => 360,
                'questionable_type' => 'App\\Project',
            ),
            64 => 
            array (
                'id_old' => 299963,
                'question_text' => 'Dear Applicant,

The Committee would like to thank you for your proposal. We have recently discussed your proposal and have a question.

1. What effects would this project have on the Tucson water table, and the spreading of trichloroethylene (TCE) contamination?

Please submit your response within a week. Thank you.

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-03-06 04:29:41.000000',
                'created_at' => '2012-03-02 04:29:41.000000',
                'questionable_id' => 343,
                'questionable_type' => 'App\\Project',
            ),
            65 => 
            array (
                'id_old' => 299964,
                'question_text' => 'Hello Jill,

Thank you for submitting your PAR. The Committee has recently discussed your PAR request and has one question.

Could you provide the Committee with an updated budget with the estimated cost breakdown of the Sunny Portal system? (We understand money will be redirected from the originally budgeted Lucid system but a cost breakdown is helpful)

Please email me your updated budget spreadsheet.

Thank you for your time.

Sincerely,

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-04-20 00:15:52.000000',
                'created_at' => '2012-04-16 00:15:52.000000',
                'questionable_id' => 53,
                'questionable_type' => 'App\\Par',
            ),
            66 => 
            array (
                'id_old' => 299965,
                'question_text' => 'Mr. Powers,

Joe Abraham, one of the Green Fund Committee\'s administrative advisers, recently informed us that the TEP rebate for your Computer Power Management Software project is now unavailable. While the Committee approved this project and remains supportive, this situation may put some of the project\'s planned outcomes in question.

The Committee is currently accepting program alteration requests (PARs) until April 20th at 1:00 p.m. and will have its final public meeting of this academic year on April 26th, which is the last opportunity for the Committee make adjustments to project recommendations before the next fiscal year. We believe this situation should be addressed through this process. More information about PARs including how to submit can be found on the Student Affairs Green Fund website at http://www.studentaffairs.arizona.edu/greenfund

The Committee appreciates your best efforts prior to submitting a PAR to identify and assess all possible sources of funding to address the $15,061 project budget shortfall. If additional funds are not identified, the Committee\'s ability to consider how to best support this project will benefit from a clear and concise explanation of how the budget shortfall will impact the project and outcomes.

Sincerely,

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-04-20 16:30:17.000000',
                'created_at' => '2012-04-16 16:30:17.000000',
                'questionable_id' => 350,
                'questionable_type' => 'App\\Project',
            ),
            67 => 
            array (
                'id_old' => 299966,
                'question_text' => 'Mr. Powers,

Thank you for submitting a PAR for your Computer Power Management Software project. The Committee has recently discussed your request and has a couple of questions about the future of this project. Please provide individual responses to the following questions by Wednesday, April 25th.

- Assuming the pilot project departments will not realize financial savings from the software, what happens to this project after the first year if they do not accept fiscal responsibility for vendor support and maintenance? If they do accept fiscal responsibility, will departments need to pay the vendor individually or will FM coordinate (I.e., is FM entering into an agreement with the vendor or is each department?)
- Can you confirm any likelihood that the TEP rebate will be available before the Green Fund\'s FY 2013-2014 allocation?
- Will have all other funding sources been exhausted before approaching next year\'s Green Fund Committee?

Sincerely,

Alex Harris
Committee Chair
UA Green Fund
aharris1@email.arizona.edu',
                'netid' => 'aharris1',
                'deadline' => '2012-04-28 04:13:13.000000',
                'created_at' => '2012-04-24 04:13:13.000000',
                'questionable_id' => 54,
                'questionable_type' => 'App\\Par',
            ),
            68 => 
            array (
                'id_old' => 299971,
                'question_text' => 'Hi Nate, Your PAR asks for a project extension until 10.01.12. Is this the correct date you would like the committee to deliberate on in tomorrow’s open meeting? Are you asking for more time to spend the money allocated to your project or more time to finish the work of the project or both? Thank you.',
                'netid' => 'rudnick1',
                'deadline' => '2012-10-05 18:00:25.000000',
                'created_at' => '2012-10-01 18:00:25.000000',
                'questionable_id' => 300,
                'questionable_type' => 'App\\Project',
            ),
            69 => 
            array (
                'id_old' => 299972,
                'question_text' => 'Hi Wil,
The committee will be considering your PAR during its open meeting tomorrow at 1:00 p.m. For clarification, what is the exact end date of the GF 12.25 project? Are you requesting to extend the work of the project, the spending of funds allocated to your project, or both? Please respond promptly as the committee cannot formally consider the PAR without this information. Thank you.',
                'netid' => 'rudnick1',
                'deadline' => '2012-10-05 18:02:22.000000',
                'created_at' => '2012-10-01 18:02:22.000000',
                'questionable_id' => 319,
                'questionable_type' => 'App\\Project',
            ),
            70 => 
            array (
                'id_old' => 299973,
                'question_text' => 'Is your project different than what was submitted at funding to account for the change in payee salary from student wages to faculty wages?',
                'netid' => 'rudnick1',
                'deadline' => '2012-11-30 19:22:42.000000',
                'created_at' => '2012-11-26 19:22:42.000000',
                'questionable_id' => 70,
                'questionable_type' => 'App\\Par',
            ),
            71 => 
            array (
                'id_old' => 299974,
                'question_text' => 'Dear Dr. Ogden, As a committee we would like to thank you for your diligence in implementing your project as well as your timely responses to our questions regarding your recently submitted request. At this time, the committee still has some questions regarding your desire to reallocate funds. As a result, our chair, Lauren Erdelyi, will be contacting you within the week to discuss these concerns with you and to answer any questions you may havel. We will also will be having our next open meeting on December 11 at 1 pm in the SUMC Madera Room if you would like to attend. Again thank very much for your cooperation.

Respectfully,
Lauren Erdelyi',
                'netid' => 'cerdelyi',
                'deadline' => '2012-12-07 19:27:21.000000',
                'created_at' => '2012-12-03 19:27:21.000000',
                'questionable_id' => 70,
                'questionable_type' => 'App\\Par',
            ),
            72 => 
            array (
                'id_old' => 299975,
                'question_text' => 'My apologies for the delay. There was a misunderstanding in the question submittable. 
Dr. Ogden,
First, please understand that The UA Green Fund enthusiastically supports your "Improving Sustainability with Cooling Water Recirculation" project. We appreciate your timely responses to our questions and are very pleased that the project has been implemented so quickly and effectively. It is always great to see a project meet success, and we commend your team for installing the recirculation system.

As you know, the Green Fund is supported with student tuition dollars. That funding source drives the Green Fund\'s need for transparency in terms of how student dollars are spent. We have several questions relating to the PAR for your ongoing project.

1) Regarding the transfer of funds from student wages to faculty, would these funds be spent out as faculty salary, or as a supplemental compensation position?

2) The initial proposal describes three courses that will use the recirculation system. Do any of these courses contain course fees that support the maintenance of course equipment?
Respectfully,
Lauren Erdelyi',
        'netid' => 'cerdelyi',
        'deadline' => '2012-12-08 14:54:34.000000',
        'created_at' => '2012-12-04 14:54:34.000000',
        'questionable_id' => 70,
        'questionable_type' => 'App\\Par',
    ),
    73 => 
    array (
        'id_old' => 299976,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:42:40.000000',
        'created_at' => '2012-12-30 19:42:40.000000',
        'questionable_id' => 313,
        'questionable_type' => 'App\\Project',
    ),
    74 => 
    array (
        'id_old' => 299977,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:43:06.000000',
        'created_at' => '2012-12-30 19:43:06.000000',
        'questionable_id' => 310,
        'questionable_type' => 'App\\Project',
    ),
    75 => 
    array (
        'id_old' => 299978,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:43:35.000000',
        'created_at' => '2012-12-30 19:43:35.000000',
        'questionable_id' => 322,
        'questionable_type' => 'App\\Project',
    ),
    76 => 
    array (
        'id_old' => 299979,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:44:28.000000',
        'created_at' => '2012-12-30 19:44:28.000000',
        'questionable_id' => 328,
        'questionable_type' => 'App\\Project',
    ),
    77 => 
    array (
        'id_old' => 299980,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:45:10.000000',
        'created_at' => '2012-12-30 19:45:10.000000',
        'questionable_id' => 300,
        'questionable_type' => 'App\\Project',
    ),
    78 => 
    array (
        'id_old' => 299981,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:45:27.000000',
        'created_at' => '2012-12-30 19:45:27.000000',
        'questionable_id' => 319,
        'questionable_type' => 'App\\Project',
    ),
    79 => 
    array (
        'id_old' => 299982,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:45:43.000000',
        'created_at' => '2012-12-30 19:45:43.000000',
        'questionable_id' => 335,
        'questionable_type' => 'App\\Project',
    ),
    80 => 
    array (
        'id_old' => 299983,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:46:12.000000',
        'created_at' => '2012-12-30 19:46:12.000000',
        'questionable_id' => 321,
        'questionable_type' => 'App\\Project',
    ),
    81 => 
    array (
        'id_old' => 299984,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:47:11.000000',
        'created_at' => '2012-12-30 19:47:11.000000',
        'questionable_id' => 365,
        'questionable_type' => 'App\\Project',
    ),
    82 => 
    array (
        'id_old' => 299985,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:47:42.000000',
        'created_at' => '2012-12-30 19:47:42.000000',
        'questionable_id' => 344,
        'questionable_type' => 'App\\Project',
    ),
    83 => 
    array (
        'id_old' => 299986,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:47:53.000000',
        'created_at' => '2012-12-30 19:47:53.000000',
        'questionable_id' => 363,
        'questionable_type' => 'App\\Project',
    ),
    84 => 
    array (
        'id_old' => 299987,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:48:29.000000',
        'created_at' => '2012-12-30 19:48:29.000000',
        'questionable_id' => 357,
        'questionable_type' => 'App\\Project',
    ),
    85 => 
    array (
        'id_old' => 299988,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:48:42.000000',
        'created_at' => '2012-12-30 19:48:42.000000',
        'questionable_id' => 341,
        'questionable_type' => 'App\\Project',
    ),
    86 => 
    array (
        'id_old' => 299989,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:48:57.000000',
        'created_at' => '2012-12-30 19:48:57.000000',
        'questionable_id' => 342,
        'questionable_type' => 'App\\Project',
    ),
    87 => 
    array (
        'id_old' => 299990,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:49:09.000000',
        'created_at' => '2012-12-30 19:49:09.000000',
        'questionable_id' => 347,
        'questionable_type' => 'App\\Project',
    ),
    88 => 
    array (
        'id_old' => 299991,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:49:21.000000',
        'created_at' => '2012-12-30 19:49:21.000000',
        'questionable_id' => 362,
        'questionable_type' => 'App\\Project',
    ),
    89 => 
    array (
        'id_old' => 299992,
        'question_text' => 'Reminder: Project update and/or final report due 12.31.12. Please let me know if you will be unable to furnish this report by the due date. Thanks, Julia',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-03 19:49:38.000000',
        'created_at' => '2012-12-30 19:49:38.000000',
        'questionable_id' => 360,
        'questionable_type' => 'App\\Project',
    ),
    90 => 
    array (
        'id_old' => 299993,
        'question_text' => 'The PAR for the Garden Project asks for money to be moved from the equipment category to the operations category due to changing the type of fence to be installed around the community garden. This will result in a cost savings. Will the remaining money be spent on a new item not currently budgeted for or will the money be spent on an unplanned operating expense? Thank you.',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-19 22:16:24.000000',
        'created_at' => '2013-01-15 22:16:24.000000',
        'questionable_id' => 73,
        'questionable_type' => 'App\\Par',
    ),
    91 => 
    array (
        'id_old' => 299994,
        'question_text' => 'The Committee has a few questions to ask before they can make a formal decision:
-	In reference to moving the PV installation to the Animal Science Research Site, is the 25 kW system already built or is the Green Fund funding an additional 25kW to be added to the current site that has an existing PV system? If this is an additional build, what total kW will the system have after the additional instillation?
-	Is the cost per kW more for the 25kW system than it was for the 5kW system? If it cost more per kW, can you identify why?',
        'netid' => 'rudnick1',
        'deadline' => '2013-01-21 22:18:13.000000',
        'created_at' => '2013-01-17 22:18:13.000000',
        'questionable_id' => 71,
        'questionable_type' => 'App\\Par',
    ),
    92 => 
    array (
        'id_old' => 299995,
        'question_text' => 'Tee Shirts are included in the new project request for Greening the Game. If funding is given for this budget item, the Committee would request the Green Fund Logo is added to the tee shirt. Can you fulfill this request? Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
        'netid' => 'rudnick1',
        'deadline' => '2013-02-13 16:46:20.000000',
        'created_at' => '2013-02-09 16:46:20.000000',
        'questionable_id' => 397,
        'questionable_type' => 'App\\Project',
    ),
    93 => 
    array (
        'id_old' => 299996,
    'question_text' => '1)      What will happen to the Community Garden if the Green Fund does not provide funding for FY2014? Will the Community Garden close?
2)      Given plot rental fee income, and operational costs, what is the cost to sustain or maintain the Community Garden project if additional improvements are not factored in? 
3)      Signage is included in the new Community Garden budget. If funding is given for this budget item, the Committee would request the Green Fund Logo is added to the sign. Can you fulfill this request?
Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-13 16:48:36.000000',
'created_at' => '2013-02-09 16:48:36.000000',
'questionable_id' => 409,
'questionable_type' => 'App\\Project',
),
94 => 
array (
'id_old' => 299997,
'question_text' => '1)      If this project gets funded, a letter of support from UA Facilities management will be necessary before the financial transfer will occur.
2)      Can you do the Harvesting Rainwater project as a smaller test pilot project? Can you do the project without installing permanent equipment?  
3)      What will happen to the cisterns and associated equipment if the study reveals the rainwater cannot be used to irrigate editable foods? 
4)      How will you market this project?
Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-13 16:49:58.000000',
'created_at' => '2013-02-09 16:49:58.000000',
'questionable_id' => 377,
'questionable_type' => 'App\\Project',
),
95 => 
array (
'id_old' => 299998,
'question_text' => 'This project file is incomplete. Please submit a spreadsheet statement showing project expenditures. The Committee is unable to consider this project for funding unless a financial spreadsheet is received by Thursday 02.14.13 at 1:00pm.',
'netid' => 'rudnick1',
'deadline' => '2013-02-13 16:52:05.000000',
'created_at' => '2013-02-09 16:52:05.000000',
'questionable_id' => 411,
'questionable_type' => 'App\\Project',
),
96 => 
array (
'id_old' => 299999,
'question_text' => 'Can this project be accomplished with less funding for Graduate Assistant Salary? If so, please detail how this change will affect the project.  Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-13 16:54:25.000000',
'created_at' => '2013-02-09 16:54:25.000000',
'questionable_id' => 372,
'questionable_type' => 'App\\Project',
),
97 => 
array (
'id_old' => 2999100,
'question_text' => '1)      Can this project be accomplished if you reduced salary?
2)      Would this project succeed if students were hired as the only paid workers on the project? If so, please detail how this change will affect the project. 

Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-13 16:56:10.000000',
'created_at' => '2013-02-09 16:56:10.000000',
'questionable_id' => 390,
'questionable_type' => 'App\\Project',
),
98 => 
array (
'id_old' => 2999101,
'question_text' => '1)      Fiscal year 2014 projects are not financially funded until mid-July 2013. Can this project be accomplished after this date during the summer of 2013? If possible, please submit your revised time line.
2)      How many students and staff will be paid if this project is funded?
Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-13 16:57:57.000000',
'created_at' => '2013-02-09 16:57:57.000000',
'questionable_id' => 394,
'questionable_type' => 'App\\Project',
),
99 => 
array (
'id_old' => 2999102,
'question_text' => 'Does the project manager and associates know of any studies that show the risks of consuming food harvested from trees that were irrigated with reclaimed water?
Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-13 16:59:06.000000',
'created_at' => '2013-02-09 16:59:06.000000',
'questionable_id' => 396,
'questionable_type' => 'App\\Project',
),
100 => 
array (
'id_old' => 2999103,
'question_text' => 'Q: This project file is incomplete. Please submit a spreadsheet statement showing project expenditures. The Committee is unable to consider this project for funding unless a financial spreadsheet is received by Thursday 02.14.13 at 1:00pm.',
'netid' => 'rudnick1',
'deadline' => '2013-02-17 20:48:14.000000',
'created_at' => '2013-02-13 20:48:14.000000',
'questionable_id' => 411,
'questionable_type' => 'App\\Project',
),
101 => 
array (
'id_old' => 2999104,
'question_text' => 'Reminder:
Q: Tee Shirts are included in the new project request for Greening the Game. If funding is given for this budget item, the Committee would request the Green Fund Logo is added to the tee shirt. Can you fulfill this request? Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-17 20:51:54.000000',
'created_at' => '2013-02-13 20:51:54.000000',
'questionable_id' => 397,
'questionable_type' => 'App\\Project',
),
102 => 
array (
'id_old' => 2999105,
'question_text' => 'Reminder:
Q: 1) What will happen to the Community Garden if the Green Fund does not provide funding for FY2014? Will the Community Garden close?
2) Given plot rental fee income, and operational costs, what is the cost to sustain or maintain the Community Garden project if additional improvements are not factored in? 
3) Signage is included in the new Community Garden budget. If funding is given for this budget item, the Committee would request the Green Fund Logo is added to the sign. Can you fulfill this request?
Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-17 20:52:49.000000',
'created_at' => '2013-02-13 20:52:49.000000',
'questionable_id' => 409,
'questionable_type' => 'App\\Project',
),
103 => 
array (
'id_old' => 2999106,
'question_text' => 'Reminder:
Q: 1) If this project gets funded, a letter of support from UA Facilities management will be necessary before the financial transfer will occur.
2) Can you do the Harvesting Rainwater project as a smaller test pilot project? Can you do the project without installing permanent equipment? 
3) What will happen to the cisterns and associated equipment if the study reveals the rainwater cannot be used to irrigate editable foods? 
4) How will you market this project?
Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-17 20:53:22.000000',
'created_at' => '2013-02-13 20:53:22.000000',
'questionable_id' => 377,
'questionable_type' => 'App\\Project',
),
104 => 
array (
'id_old' => 2999107,
'question_text' => 'Reminder:
Q: Does the project manager and associates know of any studies that show the risks of consuming food harvested from trees that were irrigated with reclaimed water?
Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-17 20:54:10.000000',
'created_at' => '2013-02-13 20:54:10.000000',
'questionable_id' => 396,
'questionable_type' => 'App\\Project',
),
105 => 
array (
'id_old' => 2999108,
'question_text' => 'Natalie,
Q: 1) What will happen to the Community Garden if the Green Fund does not provide funding for FY2014? Will the Community Garden close?
2) Given plot rental fee income, and operational costs, what is the cost to sustain or maintain the Community Garden project if additional improvements are not factored in? 
3) Signage is included in the new Community Garden budget. If funding is given for this budget item, the Committee would request the Green Fund Logo is added to the sign. Can you fulfill this request?
Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-17 21:41:56.000000',
'created_at' => '2013-02-13 21:41:56.000000',
'questionable_id' => 409,
'questionable_type' => 'App\\Project',
),
106 => 
array (
'id_old' => 2999109,
'question_text' => 'Q: Tee Shirts are included in the new project request for Greening the Game. If funding is given for this budget item, the Committee would request the Green Fund Logo is added to the tee shirt. Can you fulfill this request? Answers due by Thursday 02.14.13 at 1:00pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-17 21:42:27.000000',
'created_at' => '2013-02-13 21:42:27.000000',
'questionable_id' => 397,
'questionable_type' => 'App\\Project',
),
107 => 
array (
'id_old' => 2999110,
'question_text' => 'This answer was placed in the incorrect project. Here is the answer to the questins the Green Fund asked:',
'netid' => 'rudnick1',
'deadline' => '2013-02-18 22:04:28.000000',
'created_at' => '2013-02-14 22:04:28.000000',
'questionable_id' => 377,
'questionable_type' => 'App\\Project',
),
108 => 
array (
'id_old' => 2999111,
'question_text' => '1)	If this project gets funded, a letter of support from UA Facilities management will be necessary before the financial transfer will occur.
2)	As a question of interest, where do the most bike accidents occur on or around the UA campus? Please list specific streets and/or intersections.

Answers due by Thursday 02.21.13 at 1:00 pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-22 15:31:52.000000',
'created_at' => '2013-02-18 15:31:52.000000',
'questionable_id' => 402,
'questionable_type' => 'App\\Project',
),
109 => 
array (
'id_old' => 2999112,
'question_text' => 'The project mentions designing an acoustic screen combined with shading device – is this proven technology and is it currently in use elsewhere? Please elaborate on screen history.

Answers due by Thursday 02.21.13 at 1:00 pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-22 15:33:07.000000',
'created_at' => '2013-02-18 15:33:07.000000',
'questionable_id' => 382,
'questionable_type' => 'App\\Project',
),
110 => 
array (
'id_old' => 2999113,
'question_text' => '1)	Please provide the exact number of students working on the project.
2)	Would the project progress if the Green Fund opted to fund all budget items except appointed personnel, faculty personnel, classified personnel and corresponding ERE?

Answers due by Thursday 02.21.13 at 1:00 pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-22 15:33:58.000000',
'created_at' => '2013-02-18 15:33:58.000000',
'questionable_id' => 374,
'questionable_type' => 'App\\Project',
),
111 => 
array (
'id_old' => 2999114,
'question_text' => 'In regards to the Green Fund spreadsheet- specifically line item 36, who is the Payee that will work for 80 hours @ 120 an hour? 

Answers due by Thursday 02.21.13 at 1:00 pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-22 15:34:36.000000',
'created_at' => '2013-02-18 15:34:36.000000',
'questionable_id' => 379,
'questionable_type' => 'App\\Project',
),
112 => 
array (
'id_old' => 2999115,
'question_text' => 'Has this project applied for funding with WEES currently or in the past?

Answers due by Thursday 02.21.13 at 1:00 pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-22 15:35:10.000000',
'created_at' => '2013-02-18 15:35:10.000000',
'questionable_id' => 414,
'questionable_type' => 'App\\Project',
),
113 => 
array (
'id_old' => 2999116,
'question_text' => 'The spreadsheet submitted with this proposal says the project is asking for $2,900 and $3,900. How much money are you asking for to complete this project? Specific line items are $500 for software development, $400 for travel out of state and $2,000 for the Image Capture Devices. Does your budget include any other items and expenses? Please review the spreadsheet you submitted, make corrections, and resubmit.

Answers due by Thursday 02.21.13 at 1:00 pm. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-02-22 15:36:51.000000',
'created_at' => '2013-02-18 15:36:51.000000',
'questionable_id' => 380,
'questionable_type' => 'App\\Project',
),
114 => 
array (
'id_old' => 2999117,
'question_text' => 'Please answer the following questions: 
1) If the project is approved for funding, a letter of support from departments who participate in the project will need to be submitted before funds would be transferred. 
2) Have any participating departments agreed to take on the project license fee after the project is completed? 
3) Is the project buying the licenses with a discounted rate due to the number being purchased? 4) There might be another UA funding source that provides funds for IT projects. Would you like us to forward your proposal to the committee for consideration?',
'netid' => 'rudnick1',
'deadline' => '2013-03-01 18:30:00.000000',
'created_at' => '2013-02-25 18:30:00.000000',
'questionable_id' => 392,
'questionable_type' => 'App\\Project',
),
115 => 
array (
'id_old' => 2999118,
'question_text' => 'The financial spreadsheet lists software and printing. Pleae provide details on what software would be purchased and what the project will print. If funding is provided to this project, the committee would do so with the expectation that the Green Fund logo would be added to the site as being funded by the Green Fund.',
'netid' => 'rudnick1',
'deadline' => '2013-03-01 18:30:25.000000',
'created_at' => '2013-02-25 18:30:25.000000',
'questionable_id' => 404,
'questionable_type' => 'App\\Project',
),
116 => 
array (
'id_old' => 2999119,
'question_text' => 'There is another committee on campus that might have additional money to allocate. Would you like us to forward your proposal to them for further consideration?',
'netid' => 'rudnick1',
'deadline' => '2013-03-01 18:30:44.000000',
'created_at' => '2013-02-25 18:30:44.000000',
'questionable_id' => 376,
'questionable_type' => 'App\\Project',
),
117 => 
array (
'id_old' => 2999120,
'question_text' => 'If the Green Fund committee decided to fund everything in the project except the signs, would you be able to complete the project?',
'netid' => 'rudnick1',
'deadline' => '2013-03-01 18:31:04.000000',
'created_at' => '2013-02-25 18:31:04.000000',
'questionable_id' => 413,
'questionable_type' => 'App\\Project',
),
118 => 
array (
'id_old' => 2999121,
'question_text' => 'The third item your PAR is requesting is to transfer $2993 from operations to personnel. There are three sections of personnel. Do you want to use the operations money on appointed & faculty salary, classified salary or student employee personnel?',
'netid' => 'rudnick1',
'deadline' => '2013-03-01 20:37:23.000000',
'created_at' => '2013-02-25 20:37:23.000000',
'questionable_id' => 75,
'questionable_type' => 'App\\Par',
),
119 => 
array (
'id_old' => 2999122,
'question_text' => 'In regards to this request for a project extension, will the project be done by June 30, 2013 or December 30, 2013?',
'netid' => 'rudnick1',
'deadline' => '2013-03-02 18:05:43.000000',
'created_at' => '2013-02-26 18:05:43.000000',
'questionable_id' => 78,
'questionable_type' => 'App\\Par',
),
120 => 
array (
'id_old' => 2999123,
'question_text' => 'In regards to this request for a project extension, will the project be done by June 30, 2013 or December 30, 2013?',
'netid' => 'rudnick1',
'deadline' => '2013-03-02 18:05:44.000000',
'created_at' => '2013-02-26 18:05:44.000000',
'questionable_id' => 78,
'questionable_type' => 'App\\Par',
),
121 => 
array (
'id_old' => 2999124,
'question_text' => 'In regards to this request for a project extension, will the project be done by June 30, 2013 or December 30, 2013?',
'netid' => 'rudnick1',
'deadline' => '2013-03-02 18:05:48.000000',
'created_at' => '2013-02-26 18:05:48.000000',
'questionable_id' => 78,
'questionable_type' => 'App\\Par',
),
122 => 
array (
'id_old' => 2999125,
'question_text' => 'Hi Bill, 
In regards to this request for a project extension, will the project be done by June 30, 2013 or December 30, 2013?






--------------------------------------------------------------------------------',
'netid' => 'rudnick1',
'deadline' => '2013-03-03 22:22:39.000000',
'created_at' => '2013-02-27 22:22:39.000000',
'questionable_id' => 78,
'questionable_type' => 'App\\Par',
),
123 => 
array (
'id_old' => 2999126,
'question_text' => 'Second emial. Please answer the following questions: 
1) If the project is approved for funding, a letter of support from departments who participate in the project will need to be submitted before funds would be transferred. 
2) Have any participating departments agreed to take on the project license fee after the project is completed? 
3) Is the project buying the licenses with a discounted rate due to the number being purchased? 4) There might be another UA funding source that provides funds for IT projects. Would you like us to forward your proposal to the committee for consideration?
Response due by 02.28.13 at 2:00 pm.',
'netid' => 'rudnick1',
'deadline' => '2013-03-03 22:47:09.000000',
'created_at' => '2013-02-27 22:47:09.000000',
'questionable_id' => 392,
'questionable_type' => 'App\\Project',
),
124 => 
array (
'id_old' => 2999127,
'question_text' => '1) Have you located a production site in which you can make Biochar?  2) If the project is approved for funding, a letter of support from departments who participate in the project will need to be submitted before funds would be transferred. 3) If this project is funded, how would you show students who benefit from the use of the Biochar that the Green Fund provided financial support?',
'netid' => 'rudnick1',
'deadline' => '2013-03-05 21:22:35.000000',
'created_at' => '2013-03-01 21:22:35.000000',
'questionable_id' => 378,
'questionable_type' => 'App\\Project',
),
125 => 
array (
'id_old' => 2999128,
'question_text' => 'Please answer the following questions. 1) What is the estimated Return on Investment (ROI) of this program? 2) If the project is approved for funding, a letter of support from departments who participate in the project will need to be submitted before funds would be transferred. 3) Your report says the U.S. Department of Energy estimates that a vestibule would save 0.6% to 5% of energy consumption in a building depending on the opening door frequency. Can you please give the committee a better approximation of what this particular building and project would save in regards to energy consumption. 4) If this project is funded, how would you tell people who access the building through the vestibule that the Green Fund provided financial support and the environmental benefits of the remodel?',
'netid' => 'rudnick1',
'deadline' => '2013-03-05 21:23:02.000000',
'created_at' => '2013-03-01 21:23:02.000000',
'questionable_id' => 408,
'questionable_type' => 'App\\Project',
),
126 => 
array (
'id_old' => 2999129,
'question_text' => 'If this project is funded, how would you promote the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-05 21:23:25.000000',
'created_at' => '2013-03-01 21:23:25.000000',
'questionable_id' => 391,
'questionable_type' => 'App\\Project',
),
127 => 
array (
'id_old' => 2999130,
'question_text' => 'If this project is funded, how would you show students who benefit from the use of the welding simulator not only the environmental benefits but also that the Green Fund provided financial support for the equipment?',
'netid' => 'rudnick1',
'deadline' => '2013-03-05 21:23:57.000000',
'created_at' => '2013-03-01 21:23:57.000000',
'questionable_id' => 371,
'questionable_type' => 'App\\Project',
),
128 => 
array (
'id_old' => 2999131,
'question_text' => 'Please answer the following questions. 1)  If the project is approved for funding, a letter of support from departments who participate in the project will need to be submitted before funds would be transferred. 2) Is the project asking for project funding of $5,400? There is money requested for Travel, but it is not described in the proposal. 3) The project outlines a plan to donate sale proceeds to a charity. Can the project keep a portion of the proceeds and use the collections for future project growth to assure longer-term project sustainability? 4) If this project is funded, how would you show students who volunteer for the workshop and purchase the goods that the Green Fund provided financial support for the project?',
'netid' => 'rudnick1',
'deadline' => '2013-03-05 21:24:32.000000',
'created_at' => '2013-03-01 21:24:32.000000',
'questionable_id' => 403,
'questionable_type' => 'App\\Project',
),
129 => 
array (
'id_old' => 2999132,
'question_text' => 'If this project is funded, how would you promote the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-05 21:25:00.000000',
'created_at' => '2013-03-01 21:25:00.000000',
'questionable_id' => 384,
'questionable_type' => 'App\\Project',
),
130 => 
array (
'id_old' => 2999133,
'question_text' => 'If this project is funded, how would you promote the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-05 21:25:15.000000',
'created_at' => '2013-03-01 21:25:15.000000',
'questionable_id' => 389,
'questionable_type' => 'App\\Project',
),
131 => 
array (
'id_old' => 2999134,
'question_text' => 'Please answer the following questions. 1) You attached two spreadsheets to the proposal, one requesting funding of $19,262 and one requesting $32,573 - which budget is correct, and how much money are you asking for regarding this Green Fund proposal? 2) If this project is funded, how would you promote the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-05 21:26:21.000000',
'created_at' => '2013-03-01 21:26:21.000000',
'questionable_id' => 399,
'questionable_type' => 'App\\Project',
),
132 => 
array (
'id_old' => 2999135,
'question_text' => 'If this project is funded, how would you promote the environmental impact of the project and how would you identify the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-08 17:16:58.000000',
'created_at' => '2013-03-04 17:16:58.000000',
'questionable_id' => 393,
'questionable_type' => 'App\\Project',
),
133 => 
array (
'id_old' => 2999136,
'question_text' => 'If this project is funded, how would you promote the environmental impact of the project and how would you identify the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-08 17:17:16.000000',
'created_at' => '2013-03-04 17:17:16.000000',
'questionable_id' => 398,
'questionable_type' => 'App\\Project',
),
134 => 
array (
'id_old' => 2999137,
'question_text' => 'If this project is funded, how would you promote the environmental impact of the project and how would you identify the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-08 17:18:00.000000',
'created_at' => '2013-03-04 17:18:00.000000',
'questionable_id' => 385,
'questionable_type' => 'App\\Project',
),
135 => 
array (
'id_old' => 2999138,
'question_text' => 'Second request.
If this project is funded, how would you promote the environmental impact of the project and how would you identify the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-08 17:19:46.000000',
'created_at' => '2013-03-04 17:19:46.000000',
'questionable_id' => 391,
'questionable_type' => 'App\\Project',
),
136 => 
array (
'id_old' => 2999139,
'question_text' => 'If this project is funded, how would you promote the environmental impact of the project and how would you identify the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-08 17:20:21.000000',
'created_at' => '2013-03-04 17:20:21.000000',
'questionable_id' => 375,
'questionable_type' => 'App\\Project',
),
137 => 
array (
'id_old' => 2999140,
'question_text' => 'If this project is funded, how would you promote the environmental impact of the project and how would you identify the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-08 17:22:02.000000',
'created_at' => '2013-03-04 17:22:02.000000',
'questionable_id' => 401,
'questionable_type' => 'App\\Project',
),
138 => 
array (
'id_old' => 2999141,
'question_text' => 'If this project is funded, how would you promote the environmental impact of the project and how would you identify the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-08 17:23:04.000000',
'created_at' => '2013-03-04 17:23:04.000000',
'questionable_id' => 406,
'questionable_type' => 'App\\Project',
),
139 => 
array (
'id_old' => 2999142,
'question_text' => 'If this project is funded, how would you promote the environmental impact of the project and how would you identify the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-08 17:23:16.000000',
'created_at' => '2013-03-04 17:23:16.000000',
'questionable_id' => 369,
'questionable_type' => 'App\\Project',
),
140 => 
array (
'id_old' => 2999143,
'question_text' => 'If this project is funded, how would you promote the environmental impact of the project and how would you identify the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-08 17:23:36.000000',
'created_at' => '2013-03-04 17:23:36.000000',
'questionable_id' => 383,
'questionable_type' => 'App\\Project',
),
141 => 
array (
'id_old' => 2999144,
'question_text' => 'If this project is funded, how would you promote the environmental impact of the project and how would you identify the Green Fund as your financial supporter?',
'netid' => 'rudnick1',
'deadline' => '2013-03-08 17:23:56.000000',
'created_at' => '2013-03-04 17:23:56.000000',
'questionable_id' => 370,
'questionable_type' => 'App\\Project',
),
142 => 
array (
'id_old' => 2999145,
'question_text' => 'Q: Does the project manager and associates know of any studies that show the risks of consuming food harvested from trees that were irrigated with reclaimed water?',
'netid' => 'rudnick1',
'deadline' => '2013-03-09 18:53:26.000000',
'created_at' => '2013-03-05 18:53:26.000000',
'questionable_id' => 396,
'questionable_type' => 'App\\Project',
),
143 => 
array (
'id_old' => 2999146,
'question_text' => 'Hi Mitch,
For clarification, what exact project end date are you requesting in this PAR, December 30, 2013 or June 30, 2014?',
'netid' => 'rudnick1',
'deadline' => '2013-04-27 19:05:24.000000',
'created_at' => '2013-04-23 19:05:24.000000',
'questionable_id' => 79,
'questionable_type' => 'App\\Par',
),
144 => 
array (
'id_old' => 2999147,
'question_text' => 'Hi Bill,
For clarification, what exact project end date are you requesting in this PAR, December 30, 2013 or June 30, 2014?',
'netid' => 'rudnick1',
'deadline' => '2013-04-27 19:11:12.000000',
'created_at' => '2013-04-23 19:11:12.000000',
'questionable_id' => 81,
'questionable_type' => 'App\\Par',
),
145 => 
array (
'id_old' => 2999148,
'question_text' => 'Hi Bill,
For clarification, what exact project end date are you requesting in this PAR, December 30, 2013 or June 30, 2014?',
'netid' => 'rudnick1',
'deadline' => '2013-04-27 19:11:21.000000',
'created_at' => '2013-04-23 19:11:21.000000',
'questionable_id' => 82,
'questionable_type' => 'App\\Par',
),
146 => 
array (
'id_old' => 2999149,
'question_text' => 'Hi Steven,
For clarification, what exact project end date are you requesting in this PAR, December 30, 2013 or June 30, 2014?',
'netid' => 'rudnick1',
'deadline' => '2013-04-27 19:12:19.000000',
'created_at' => '2013-04-23 19:12:19.000000',
'questionable_id' => 83,
'questionable_type' => 'App\\Par',
),
147 => 
array (
'id_old' => 2999150,
'question_text' => 'Hi Kyle, I am in receipt of your Letter of Agreement. The account number listed on the contract is currently being used. You may use this account but a subaccount needs to be set up. Please contact Maria Galo and ask her to set up a sub account called GF14.01. Let me know when this has occurred and if you have any questions, Julia',
'netid' => 'rudnick1',
'deadline' => '2013-07-13 19:31:25.000000',
'created_at' => '2013-07-09 19:31:25.000000',
'questionable_id' => 393,
'questionable_type' => 'App\\Project',
),
148 => 
array (
'id_old' => 2999151,
'question_text' => 'Dear Project Manager,
The Green Fund is getting ready to release funding for FY2014 projects. Before funding can be released, a signed Letter of Agreement needs to be returned to the Office of Sustainability along with a reduced funding note. Thank you. Julia',
'netid' => 'rudnick1',
'deadline' => '2013-07-13 21:46:50.000000',
'created_at' => '2013-07-09 21:46:50.000000',
'questionable_id' => 409,
'questionable_type' => 'App\\Project',
),
149 => 
array (
'id_old' => 2999152,
'question_text' => 'Dear Project Manager,
The Green Fund is getting ready to release funding for FY2014 projects. Before funding can be released, a signed Letter of Agreement needs to be returned to the Office of Sustainability along with a reduced funding note. Thank you. Julia',
'netid' => 'rudnick1',
'deadline' => '2013-07-13 21:47:32.000000',
'created_at' => '2013-07-09 21:47:32.000000',
'questionable_id' => 402,
'questionable_type' => 'App\\Project',
),
150 => 
array (
'id_old' => 2999153,
'question_text' => 'Dear Project Manager,
The Green Fund is getting ready to release funding for FY2014 projects. Before funding can be released, a signed Letter of Agreement needs to be returned to the Office of Sustainability. Thank you. Julia',
'netid' => 'rudnick1',
'deadline' => '2013-07-13 21:50:17.000000',
'created_at' => '2013-07-09 21:50:17.000000',
'questionable_id' => 392,
'questionable_type' => 'App\\Project',
),
151 => 
array (
'id_old' => 2999154,
'question_text' => 'Dear Project Manager,
The Green Fund is getting ready to release funding for FY2014 projects. Before funding can be released, a signed Letter of Agreement needs to be returned to the Office of Sustainability. Thank you. Julia',
'netid' => 'rudnick1',
'deadline' => '2013-07-13 21:50:36.000000',
'created_at' => '2013-07-09 21:50:36.000000',
'questionable_id' => 397,
'questionable_type' => 'App\\Project',
),
152 => 
array (
'id_old' => 2999155,
'question_text' => 'Dear Project Manager,
The Green Fund is getting ready to release funding for FY2014 projects. Before funding can be released, a signed Letter of Agreement needs to be returned to the Office of Sustainability along with a reduced funding note. Thank you. Julia',
'netid' => 'rudnick1',
'deadline' => '2013-07-13 21:50:48.000000',
'created_at' => '2013-07-09 21:50:48.000000',
'questionable_id' => 411,
'questionable_type' => 'App\\Project',
),
153 => 
array (
'id_old' => 2999156,
'question_text' => 'Dear Project Manager,
Final project reports are due this Wednesday, 07.31.13. Please write your report in Word, save the document as a plain text and paste answers into the Green Fund site. If you have unspent funds, please have your business manager transfer them back to the Green Fund account- 2208200. If you are unable to complete your final report as per your signed contract, let Julia Rudnick (rudnick1@email.arizona.edu) know as soon as possible. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-08-02 17:45:57.000000',
'created_at' => '2013-07-29 17:45:57.000000',
'questionable_id' => 303,
'questionable_type' => 'App\\Project',
),
154 => 
array (
'id_old' => 2999157,
'question_text' => 'Dear Project Manager,
Project report updates are due this Wednesday, 07.31.13. Please write your report in Word, save the document as a plain text and paste answers into the Green Fund site.  If you are unable to complete your report as per your signed contract, let Julia Rudnick (rudnick1@email.arizona.edu) know as soon as possible. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-08-02 17:47:51.000000',
'created_at' => '2013-07-29 17:47:51.000000',
'questionable_id' => 310,
'questionable_type' => 'App\\Project',
),
155 => 
array (
'id_old' => 2999158,
'question_text' => 'Dear Project Manager,
Project report updates are due this Wednesday, 07.31.13. Please write your report in Word, save the document as a plain text and paste answers into the Green Fund site.  If you are unable to complete your report as per your signed contract, let Julia Rudnick (rudnick1@email.arizona.edu) know as soon as possible. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-08-02 17:49:13.000000',
'created_at' => '2013-07-29 17:49:13.000000',
'questionable_id' => 335,
'questionable_type' => 'App\\Project',
),
156 => 
array (
'id_old' => 2999159,
'question_text' => 'Dear Project Manager,
Project report updates are due this Wednesday, 07.31.13. Please write your report in Word, save the document as a plain text and paste answers into the Green Fund site.  If you are unable to complete your report as per your signed contract, let Julia Rudnick (rudnick1@email.arizona.edu) know as soon as possible. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-08-02 17:49:59.000000',
'created_at' => '2013-07-29 17:49:59.000000',
'questionable_id' => 315,
'questionable_type' => 'App\\Project',
),
157 => 
array (
'id_old' => 2999160,
'question_text' => 'Dear Project Manager,
Final project reports are due this Wednesday, 07.31.13. Please write your report in Word, save the document as a plain text and paste answers into the Green Fund site. If you have unspent funds, please have your business manager transfer them back to the Green Fund account- 2208200.  If you are unable to complete your final report as per your signed contract, let Julia Rudnick (rudnick1@email.arizona.edu) know as soon as possible. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-08-02 17:50:35.000000',
'created_at' => '2013-07-29 17:50:35.000000',
'questionable_id' => 321,
'questionable_type' => 'App\\Project',
),
158 => 
array (
'id_old' => 2999161,
'question_text' => 'Dear Project Manager,
Project report updates are due this Wednesday, 07.31.13. Please write your report in Word, save the document as a plain text and paste answers into the Green Fund site.  If you are unable to complete your report as per your signed contract, let Julia Rudnick (rudnick1@email.arizona.edu) know as soon as possible. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-08-02 17:51:23.000000',
'created_at' => '2013-07-29 17:51:23.000000',
'questionable_id' => 338,
'questionable_type' => 'App\\Project',
),
159 => 
array (
'id_old' => 2999162,
'question_text' => 'Dear Project Manager,
Final project reports are due this Wednesday, 07.31.13. Please write your report in Word, save the document as a plain text and paste answers into the Green Fund site. If you have unspent funds, please have your business manager transfer them back to the Green Fund account- 2208200.  If you are unable to complete your final report as per your signed contract, let Julia Rudnick (rudnick1@email.arizona.edu) know as soon as possible. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-08-02 17:52:07.000000',
'created_at' => '2013-07-29 17:52:07.000000',
'questionable_id' => 365,
'questionable_type' => 'App\\Project',
),
160 => 
array (
'id_old' => 2999163,
'question_text' => 'Dear Project Manager,
Project report updates are due this Wednesday, 07.31.13. Please write your report in Word, save the document as a plain text and paste answers into the Green Fund site.  If you are unable to complete your report as per your signed contract, let Julia Rudnick (rudnick1@email.arizona.edu) know as soon as possible. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-08-02 17:52:25.000000',
'created_at' => '2013-07-29 17:52:25.000000',
'questionable_id' => 363,
'questionable_type' => 'App\\Project',
),
161 => 
array (
'id_old' => 2999164,
'question_text' => 'Dear Project Manager,
Final project reports are due this Wednesday, 07.31.13. Please write your report in Word, save the document as a plain text and paste answers into the Green Fund site. If you have unspent funds, please have your business manager transfer them back to the Green Fund account- 2208200.  If you are unable to complete your final report as per your signed contract, let Julia Rudnick (rudnick1@email.arizona.edu) know as soon as possible. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-08-02 17:53:07.000000',
'created_at' => '2013-07-29 17:53:07.000000',
'questionable_id' => 357,
'questionable_type' => 'App\\Project',
),
162 => 
array (
'id_old' => 2999165,
'question_text' => 'Dear Project Manager,
Final project reports are due this Wednesday, 07.31.13. Please write your report in Word, save the document as a plain text and paste answers into the Green Fund site. If you have unspent funds, please have your business manager transfer them back to the Green Fund account- 2208200.  If you are unable to complete your final report as per your signed contract, let Julia Rudnick (rudnick1@email.arizona.edu) know as soon as possible. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-08-02 17:53:37.000000',
'created_at' => '2013-07-29 17:53:37.000000',
'questionable_id' => 362,
'questionable_type' => 'App\\Project',
),
163 => 
array (
'id_old' => 2999166,
'question_text' => 'Dear Project Manager,
Final project reports are due this Wednesday, 07.31.13. Please write your report in Word, save the document as a plain text and paste answers into the Green Fund site. If you have unspent funds, please have your business manager transfer them back to the Green Fund account- 2208200.  If you are unable to complete your final report as per your signed contract, let Julia Rudnick (rudnick1@email.arizona.edu) know as soon as possible. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2013-08-02 17:54:50.000000',
'created_at' => '2013-07-29 17:54:50.000000',
'questionable_id' => 360,
'questionable_type' => 'App\\Project',
),
164 => 
array (
'id_old' => 2999167,
'question_text' => 'Hi James,
I was reviewing your budget spreadsheet for the Green Fund Grant proposal. The spreadsheet list $16,000 for demonstration equipment and is listed in the operations field. Is this one piece of equipment or several less expensive pieces of equipment? Any single piece of equipment costing over $5,000 should be listed under equipment - not operations. Is your spreadsheet correct or should this get listed differently? Thanks, Julia',
'netid' => 'rudnick1',
'deadline' => '2014-02-02 21:53:10.000000',
'created_at' => '2014-01-28 21:53:10.000000',
'questionable_id' => 419,
'questionable_type' => 'App\\Project',
),
165 => 
array (
'id_old' => 2999168,
'question_text' => 'Dear Tanya,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:05-5:10 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:09:51.000000',
'created_at' => '2014-01-29 23:09:51.000000',
'questionable_id' => 429,
'questionable_type' => 'App\\Project',
),
166 => 
array (
'id_old' => 2999169,
'question_text' => 'Dear Rudy,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:10-5:15 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:11:57.000000',
'created_at' => '2014-01-29 23:11:57.000000',
'questionable_id' => 447,
'questionable_type' => 'App\\Project',
),
167 => 
array (
'id_old' => 2999170,
'question_text' => 'Dear Melanie,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:20-5:25 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:13:18.000000',
'created_at' => '2014-01-29 23:13:18.000000',
'questionable_id' => 448,
'questionable_type' => 'App\\Project',
),
168 => 
array (
'id_old' => 2999171,
'question_text' => 'Dear Robert,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:25-5:30 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:14:33.000000',
'created_at' => '2014-01-29 23:14:33.000000',
'questionable_id' => 426,
'questionable_type' => 'App\\Project',
),
169 => 
array (
'id_old' => 2999172,
'question_text' => 'Dear Donald,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:35-5:40 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:15:36.000000',
'created_at' => '2014-01-29 23:15:36.000000',
'questionable_id' => 428,
'questionable_type' => 'App\\Project',
),
170 => 
array (
'id_old' => 2999173,
'question_text' => 'Dear Donald,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:40-5:45 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:17:12.000000',
'created_at' => '2014-01-29 23:17:12.000000',
'questionable_id' => 438,
'questionable_type' => 'App\\Project',
),
171 => 
array (
'id_old' => 2999174,
'question_text' => 'Hi Chet, I intended to address the last question to you. Sorry about that. The information is correct and your time for the Community Garden is from 5:40-5:45pm on 02.04.14. It is ok if you want to send another student to speak about this project. Thanks, Julia',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:20:35.000000',
'created_at' => '2014-01-29 23:20:35.000000',
'questionable_id' => 424,
'questionable_type' => 'App\\Project',
),
172 => 
array (
'id_old' => 2999175,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:50-5:55 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:21:36.000000',
'created_at' => '2014-01-29 23:21:36.000000',
'questionable_id' => 421,
'questionable_type' => 'App\\Project',
),
173 => 
array (
'id_old' => 2999176,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 11, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 11, 2014 
Time: 5:05-5:10 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:23:19.000000',
'created_at' => '2014-01-29 23:23:19.000000',
'questionable_id' => 418,
'questionable_type' => 'App\\Project',
),
174 => 
array (
'id_old' => 2999177,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 11, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 11, 2014 
Time: 5:10-5:15 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:24:47.000000',
'created_at' => '2014-01-29 23:24:47.000000',
'questionable_id' => 442,
'questionable_type' => 'App\\Project',
),
175 => 
array (
'id_old' => 2999178,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 11, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 11, 2014 
Time: 5:20-5:25 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:25:32.000000',
'created_at' => '2014-01-29 23:25:32.000000',
'questionable_id' => 423,
'questionable_type' => 'App\\Project',
),
176 => 
array (
'id_old' => 2999179,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 11, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 11, 2014 
Time: 5:25-5:30 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:26:10.000000',
'created_at' => '2014-01-29 23:26:10.000000',
'questionable_id' => 436,
'questionable_type' => 'App\\Project',
),
177 => 
array (
'id_old' => 2999180,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 11, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 11, 2014 
Time: 5:35-5:40 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:26:51.000000',
'created_at' => '2014-01-29 23:26:51.000000',
'questionable_id' => 437,
'questionable_type' => 'App\\Project',
),
178 => 
array (
'id_old' => 2999181,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 11, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 11, 2014 
Time: 5:40-5:45 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:27:35.000000',
'created_at' => '2014-01-29 23:27:35.000000',
'questionable_id' => 431,
'questionable_type' => 'App\\Project',
),
179 => 
array (
'id_old' => 2999182,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 11, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 11, 2014 
Time: 5:50-5:55 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:28:21.000000',
'created_at' => '2014-01-29 23:28:21.000000',
'questionable_id' => 432,
'questionable_type' => 'App\\Project',
),
180 => 
array (
'id_old' => 2999183,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 11, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 11, 2014 
Time: 5:55-6:00 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:29:05.000000',
'created_at' => '2014-01-29 23:29:05.000000',
'questionable_id' => 420,
'questionable_type' => 'App\\Project',
),
181 => 
array (
'id_old' => 2999184,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 18, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 18, 2014 
Time: 5:05-5:10 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:30:22.000000',
'created_at' => '2014-01-29 23:30:22.000000',
'questionable_id' => 434,
'questionable_type' => 'App\\Project',
),
182 => 
array (
'id_old' => 2999185,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 18, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 18, 2014 
Time: 5:10-5:15 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:31:24.000000',
'created_at' => '2014-01-29 23:31:24.000000',
'questionable_id' => 445,
'questionable_type' => 'App\\Project',
),
183 => 
array (
'id_old' => 2999186,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 18, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 18, 2014 
Time: 5:20-5:25 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:32:00.000000',
'created_at' => '2014-01-29 23:32:00.000000',
'questionable_id' => 440,
'questionable_type' => 'App\\Project',
),
184 => 
array (
'id_old' => 2999187,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 18, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 18, 2014 
Time: 5:25-5:30 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:32:44.000000',
'created_at' => '2014-01-29 23:32:44.000000',
'questionable_id' => 443,
'questionable_type' => 'App\\Project',
),
185 => 
array (
'id_old' => 2999188,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 18, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 18, 2014 
Time: 5:35-5:40 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:33:21.000000',
'created_at' => '2014-01-29 23:33:21.000000',
'questionable_id' => 449,
'questionable_type' => 'App\\Project',
),
186 => 
array (
'id_old' => 2999189,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 18, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 18, 2014 
Time: 5:40-5:45 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:34:20.000000',
'created_at' => '2014-01-29 23:34:20.000000',
'questionable_id' => 425,
'questionable_type' => 'App\\Project',
),
187 => 
array (
'id_old' => 2999190,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 18, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 18, 2014 
Time: 5:50-5:55 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:35:07.000000',
'created_at' => '2014-01-29 23:35:07.000000',
'questionable_id' => 446,
'questionable_type' => 'App\\Project',
),
188 => 
array (
'id_old' => 2999191,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 25, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 25, 2014 
Time: 5:05-5:10 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:36:15.000000',
'created_at' => '2014-01-29 23:36:15.000000',
'questionable_id' => 419,
'questionable_type' => 'App\\Project',
),
189 => 
array (
'id_old' => 2999192,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 25, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 25, 2014 
Time: 5:10-5:15 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:37:01.000000',
'created_at' => '2014-01-29 23:37:01.000000',
'questionable_id' => 441,
'questionable_type' => 'App\\Project',
),
190 => 
array (
'id_old' => 2999193,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 25, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 25, 2014 
Time: 5:20-5:25 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:37:44.000000',
'created_at' => '2014-01-29 23:37:44.000000',
'questionable_id' => 444,
'questionable_type' => 'App\\Project',
),
191 => 
array (
'id_old' => 2999194,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 25, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 25, 2014 
Time: 5:25-5:30 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:38:29.000000',
'created_at' => '2014-01-29 23:38:29.000000',
'questionable_id' => 424,
'questionable_type' => 'App\\Project',
),
192 => 
array (
'id_old' => 2999195,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 25, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 25, 2014 
Time: 5:35-5:40 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:39:05.000000',
'created_at' => '2014-01-29 23:39:05.000000',
'questionable_id' => 422,
'questionable_type' => 'App\\Project',
),
193 => 
array (
'id_old' => 2999196,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 25, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 25, 2014 
Time: 5:40-5:45 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-03 23:39:36.000000',
'created_at' => '2014-01-29 23:39:36.000000',
'questionable_id' => 433,
'questionable_type' => 'App\\Project',
),
194 => 
array (
'id_old' => 2999197,
'question_text' => 'Hi Ed,
In regards to your time slot with the GF committee, you are welcome to send a student to represent the project. Thanks, Julia',
'netid' => 'rudnick1',
'deadline' => '2014-02-04 21:46:13.000000',
'created_at' => '2014-01-30 21:46:13.000000',
'questionable_id' => 423,
'questionable_type' => 'App\\Project',
),
195 => 
array (
'id_old' => 2999198,
'question_text' => 'Hi Sarah,
In regards to your time slot with the GF committee, you are welcome to bring additional project participants. Thanks, Julia',
'netid' => 'rudnick1',
'deadline' => '2014-02-04 21:49:08.000000',
'created_at' => '2014-01-30 21:49:08.000000',
'questionable_id' => 432,
'questionable_type' => 'App\\Project',
),
196 => 
array (
'id_old' => 2999199,
'question_text' => 'Hi Linda,
You are welcome to bring other project participants to the meeting. I caution you in terms of how many should attend as each project is only given 5 minutes. Thanks, Julia',
'netid' => 'rudnick1',
'deadline' => '2014-02-04 21:51:14.000000',
'created_at' => '2014-01-30 21:51:14.000000',
'questionable_id' => 445,
'questionable_type' => 'App\\Project',
),
197 => 
array (
'id_old' => 2999200,
'question_text' => 'Hi Ron,
In regards to your time slot with the GF committee, you are welcome to bring additional project participants. Thanks, Julia',
'netid' => 'rudnick1',
'deadline' => '2014-02-04 21:52:34.000000',
'created_at' => '2014-01-30 21:52:34.000000',
'questionable_id' => 440,
'questionable_type' => 'App\\Project',
),
198 => 
array (
'id_old' => 2999201,
'question_text' => 'Hi Hamish,
In regards to your time slot with the GF committee, you are welcome to ask another project participant to present in your place. Thanks, Julia',
'netid' => 'rudnick1',
'deadline' => '2014-02-04 21:57:32.000000',
'created_at' => '2014-01-30 21:57:32.000000',
'questionable_id' => 422,
'questionable_type' => 'App\\Project',
),
199 => 
array (
'id_old' => 2999202,
'question_text' => 'Hi Mark,
Your application indicates you are requesting funding in the amount of $135,600 but your budget indicates you are requesting $115,763.00. Can you please let the committee what amount you are requesting?
Thanks, Julia',
'netid' => 'rudnick1',
'deadline' => '2014-02-05 23:06:29.000000',
'created_at' => '2014-01-31 23:06:29.000000',
'questionable_id' => 418,
'questionable_type' => 'App\\Project',
),
200 => 
array (
'id_old' => 2999203,
'question_text' => 'Dear Rudy,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:10-5:15 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-08 15:19:11.000000',
'created_at' => '2014-02-03 15:19:11.000000',
'questionable_id' => 447,
'questionable_type' => 'App\\Project',
),
201 => 
array (
'id_old' => 2999204,
'question_text' => 'Dear Robert,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:25-5:30 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-08 15:19:47.000000',
'created_at' => '2014-02-03 15:19:47.000000',
'questionable_id' => 426,
'questionable_type' => 'App\\Project',
),
202 => 
array (
'id_old' => 2999205,
'question_text' => 'Dear Donald,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:35-5:40 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-08 15:20:17.000000',
'created_at' => '2014-02-03 15:20:17.000000',
'questionable_id' => 428,
'questionable_type' => 'App\\Project',
),
203 => 
array (
'id_old' => 2999206,
'question_text' => 'Dear Chet,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:40-5:45 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-08 15:21:09.000000',
'created_at' => '2014-02-03 15:21:09.000000',
'questionable_id' => 438,
'questionable_type' => 'App\\Project',
),
204 => 
array (
'id_old' => 2999207,
'question_text' => 'Dear Barry,
The Green Fund will be reviewing your proposal on Tuesday, February 4, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 4, 2014 
Time: 5:50-5:55 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-08 15:21:59.000000',
'created_at' => '2014-02-03 15:21:59.000000',
'questionable_id' => 421,
'questionable_type' => 'App\\Project',
),
205 => 
array (
'id_old' => 2999208,
'question_text' => 'Dear Renee,
The Green Fund will be reviewing your proposal on Tuesday, February 11, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 11, 2014 
Time: 5:35-5:40 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-08 15:23:33.000000',
'created_at' => '2014-02-03 15:23:33.000000',
'questionable_id' => 437,
'questionable_type' => 'App\\Project',
),
206 => 
array (
'id_old' => 2999209,
'question_text' => 'Dear Diane,
The Green Fund will be reviewing your proposal on Tuesday, February 11, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 11, 2014 
Time: 5:55-6:00 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-08 15:30:45.000000',
'created_at' => '2014-02-03 15:30:45.000000',
'questionable_id' => 420,
'questionable_type' => 'App\\Project',
),
207 => 
array (
'id_old' => 2999210,
'question_text' => 'Dear Project Manager,
The Green Fund will be reviewing your proposal on Tuesday, February 18, 2014. The committee has set aside a few minutes in which you can meet with them and say something about your project that they might not know from reading your proposal. The committee has suggested you spend 2-3 minutes speaking and then allow them the opportunity to ask you clarifying questions they might have after reading your proposal request. A total of 5 minutes has been set aside for each proposal. This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. The meeting time frame will be strictly adhered to in order to respect the value of everyone’s time. Your meeting has been scheduled for:
Date: Tuesday, February 18, 2014 
Time: 5:25-5:30 pm
Location: El Portal Ocotillo Conference Room
Please let the committee know if you will be attending this meeting.',
'netid' => 'rudnick1',
'deadline' => '2014-02-08 15:33:32.000000',
'created_at' => '2014-02-03 15:33:32.000000',
'questionable_id' => 443,
'questionable_type' => 'App\\Project',
),
208 => 
array (
'id_old' => 2999211,
'question_text' => 'Hi Rudy,
Upon review of your proposal, the committee noted your timeline start date as of May 2014. This is a fiscal year grant and projects that get approval for funding will not receive monies until the beginning of August. Will you be able to accomplish your project with the start date moved back? Please explain.',
'netid' => 'rudnick1',
'deadline' => '2014-02-08 20:34:09.000000',
'created_at' => '2014-02-03 20:34:09.000000',
'questionable_id' => 447,
'questionable_type' => 'App\\Project',
),
209 => 
array (
'id_old' => 2999212,
'question_text' => 'Is this where the Green Fund would ask questions? Does this show up in the Q&A area?

Basically, this question is a test.',
'netid' => 'jminor',
'deadline' => '2014-02-11 18:17:40.000000',
'created_at' => '2014-02-06 18:17:40.000000',
'questionable_id' => 417,
'questionable_type' => 'App\\Project',
),
210 => 
array (
'id_old' => 2999213,
'question_text' => 'Hello Robert,
The Green Fund Committee has several questions after meeting with you and reviewing your proposal.

1) You are requesting $700 for irrigation supplies; the committee is curious what type of irrigation system you plan on implementing? Will it be water efficient?

2)Have you confirmed with Professor Margaret Livingston that the conceptional design will be designed by a student?',
'netid' => 'jminor',
'deadline' => '2014-02-11 20:15:33.000000',
'created_at' => '2014-02-06 20:15:33.000000',
'questionable_id' => 426,
'questionable_type' => 'App\\Project',
),
211 => 
array (
'id_old' => 2999214,
'question_text' => 'Hello Tanya,
After reviewing your proposal on Tuesday, the Green Fund Committee has several questions.

1. Do you have (or can you provide) a list of the professors and their respective courses who are committed (or likely) to incorporating the signs into coursework?

2. Your proposal describes how you have tracked website visitation- how do you plan to track usage of the signs in the future?

3. Your budget requests a digital camera but this is not explained in detail in your proposal- can you please justify the need for the camera and briefly explain how/why this is necessary for the project?

4. If we were unable to fund all of the signs you\'re requesting, is there a way for the project to be successful with a smaller number of signs?',
'netid' => 'jminor',
'deadline' => '2014-02-11 20:18:48.000000',
'created_at' => '2014-02-06 20:18:48.000000',
'questionable_id' => 429,
'questionable_type' => 'App\\Project',
),
212 => 
array (
'id_old' => 2999215,
'question_text' => 'Hello Dr. Pryor,
Thank you for meeting with the Green Fund Committee on Tuesday. After hearing from you and reviewing your proposal, the Committee would like clarification on the following question.

1)You explained that 5% of the project income goes to the Food Bank as a donation. Additional proceeds will pay for project operational costs. Eventually, this project is expected to \'break even\' and then start generating profit. As the operations become profitable, what university department(s) and/or program(s) would be supported by these profits?',
'netid' => 'jminor',
'deadline' => '2014-02-12 18:01:31.000000',
'created_at' => '2014-02-07 18:01:31.000000',
'questionable_id' => 421,
'questionable_type' => 'App\\Project',
),
213 => 
array (
'id_old' => 2999216,
'question_text' => 'Hello Rudy,
Thank you for meeting with the Green Fund Committee last Tuesday. After reviewing your proposal, they do have several questions.

1. If the Green Fund Committee does not fund the travel expenses for the two students to attend the AISES National Conference, are there other organizations which may deal specifically with conference expenses that you could contact? Which specific organizations would you apply to in this case? Additionally the Green Fund Committee would like to know if the two students attending the conference would be graduates or undergraduates as this may affect how easily other funding could be gained.  

2. Is there a plan in place for this aquaponics projgram to become a self-sustaining program in the future? What specific steps would be taken if the Green Fund were no longer available to supply funds for this project in the future? The Green Fund Committee is interested to know what other sources of funding are available for this project and what those specific sources would be. 

3. What steps are being taken to ensure that the aquaponics systems will continue to be used and maintained? Who will maintain the systems and how? What will be done with the systems after this year? If the systems do fulfill their use, how will they be disposed of? 

4. How will this project attract high school students to higher education? Is there a procedure to monitor the effects of this project on interest and engagement for higher education?',
'netid' => 'jminor',
'deadline' => '2014-02-15 02:04:00.000000',
'created_at' => '2014-02-10 02:04:00.000000',
'questionable_id' => 447,
'questionable_type' => 'App\\Project',
),
214 => 
array (
'id_old' => 2999217,
'question_text' => 'Hello Thomas,
After reviewing your proposal last night, the Green Fund Committee has several questions. Please reply to these no later than Tuesday, Feb 18th at 12:00 pm. 

1. How will IE maintain the guides and keep them updated?  

2. Who would be in charge of maintaining and updating the guide?

3. How would IE moderate postings and requests by community members?

4. Please explain in further detail how IE plans to measure the impact of these engagement guides?  Will you track the number of people visiting the site, posting to the site, and/or the number of students that get jobs/internships from this site?

5.  How many people do you anticipate attending the half-day workshop?',
'netid' => 'jminor',
'deadline' => '2014-02-17 19:03:34.000000',
'created_at' => '2014-02-12 19:03:34.000000',
'questionable_id' => 431,
'questionable_type' => 'App\\Project',
),
215 => 
array (
'id_old' => 2999218,
'question_text' => 'Hello Ed,
Thank you for sending a representative to talk to the Green Fund last night. After reviewing your proposal, the Green Fund Committee would like to ask you for clarification on the following questions. Please respond by no later than 12:00 pm on Tuesday, February 18th. 

1. In the "Travel for Outreach to Kayenta, AZ" section of your project budget narrative, the cost of the vehicles, fuel, and lodging doesn\'t add up to the listed $4635. Could you expand on how you reached the total this section?

2. Would you consider supporting the UA Motor Pool by renting project-related vehicles from Motor Pool? [Student fee money used to support UA operations rather than a private vendor.]

3. Do you feel that attending the Micro-Hydro workshop at Solar Energy International is essential to the execution of the micro-hydro portion of the project?',
'netid' => 'jminor',
'deadline' => '2014-02-17 23:52:46.000000',
'created_at' => '2014-02-12 23:52:46.000000',
'questionable_id' => 423,
'questionable_type' => 'App\\Project',
),
216 => 
array (
'id_old' => 2999219,
'question_text' => 'Hello Diane,
The Green Fund Committee met to review your proposal last night. After reading and discussing the proposal, they have several questions for you. Please respond no by 12:00 pm on Tuesday, Feb 18th.

1) How far-reaching do you envision these modules to become? Please elaborate further on how you will disseminate findings that are useful for other youth-led groups.

2) How did you select those 5 campus groups? Are they committed to attending the workshops if this proposal is funded?',
'netid' => 'jminor',
'deadline' => '2014-02-18 02:47:45.000000',
'created_at' => '2014-02-13 02:47:45.000000',
'questionable_id' => 420,
'questionable_type' => 'App\\Project',
),
217 => 
array (
'id_old' => 2999220,
'question_text' => 'Hello Mr. Gaschler,
Thank you for meeting with the Green Fund Committee last week. Upon reviewing your proposal, the Committee would like to ask you a few clarifying questions.

Please respond to this as soon as you are able to. 

1. We understand that the goal of the ticket giveaway is to fill the stands. Are there strategies for a ticket giveaway that will attract 600 new fans to the game rather than regular attendees? 

2. The Green Fund Committee would like to request that the designs for the promotional t-shirts and "green" warmup jerseys be sent to the Green Fund for review. Is review of shirt designs possible within the additional constraints of IMG and UA Athletics?',
'netid' => 'jminor',
'deadline' => '2014-02-20 22:08:29.000000',
'created_at' => '2014-02-15 22:08:29.000000',
'questionable_id' => 436,
'questionable_type' => 'App\\Project',
),
218 => 
array (
'id_old' => 2999221,
'question_text' => 'Hello Diane,
The Green Fund Committee met to review your proposal last night. After reading and discussing the proposal, they have several questions for you. 

1) How far-reaching do you envision these modules to become? Please elaborate further on how you will disseminate findings that are useful for other youth-led groups.

2) How did you select those 5 campus groups? Are they committed to attending the workshops if this proposal is funded?',
'netid' => 'rudnick1',
'deadline' => '2014-02-23 18:29:48.000000',
'created_at' => '2014-02-18 18:29:48.000000',
'questionable_id' => 420,
'questionable_type' => 'App\\Project',
),
219 => 
array (
'id_old' => 2999222,
'question_text' => 'Hello Dr. Pryor,
Thank you for meeting with the Green Fund Committee on 02.04.14. After hearing from you and reviewing your proposal, the Committee would like clarification on the following question.

You explained that 5% of the project income goes to the Food Bank as a donation. Additional proceeds will pay for project operational costs. Eventually, this project is expected to \'break even\' and then start generating profit. As the operations become profitable, what university department(s) and/or program(s) would be supported by these profits?',
'netid' => 'rudnick1',
'deadline' => '2014-02-23 18:35:27.000000',
'created_at' => '2014-02-18 18:35:27.000000',
'questionable_id' => 421,
'questionable_type' => 'App\\Project',
),
220 => 
array (
'id_old' => 2999223,
'question_text' => 'Hello Sarah,
Thank you for meeting with the Green Fund Committee last Tuesday. After reviewing your proposal, the Committee would like to ask you a few clarifying questions.

Please respond as soon as possible.

This year\'s allocation requests to the Green Fund exceed the Green Fund\'s budget by a little more than 2:1. The Green Fund is soliciting feedback from proposal authors on how projects might be successful if partially funded. 

1) If only partial funding is available for your project, please specify which project elements are most central to overall success of your goals. 

2) We understand that a partially funded project must also necessarily reduce its goals and desired impacts. Considering your answer to question 1, please discuss how reducing the scope of the project would affect project deliverables. 

3) Does your team have plans to apply for other funding that might cover some of the cost associated with your project (i.e. invited speaker grants from WEES, GPSC, and so forth)?',
'netid' => 'jminor',
'deadline' => '2014-02-23 19:31:58.000000',
'created_at' => '2014-02-18 19:31:58.000000',
'questionable_id' => 432,
'questionable_type' => 'App\\Project',
),
221 => 
array (
'id_old' => 2999224,
'question_text' => 'Hello Linda,
Thank you for meeting with the Green Fund Committee last night. After talking with you and reviewing your proposal, the Committee has requested clarification on several points.

Please respond by Tuesday, Feb 25 by noon.

1. The two buildings owned by City High School are tall and might block sunlight in the alleyway. The committee would like more details on your structural plans for ensuring the garden plants will receive adequate sunlight to grow in the alleyway.

2. Funding is requested for prototypes to be built as models for the garden in the alleyway. The Green Fund Committee is curious how the prototypes will be utilized after the garden is completely installed and prototypes are no longer needed at the location.

3. Can you provide a photograph of the current status of the alleyway? Also, are you able to provide any pictures or example sketches for how the alleyway may look after the program is installed?

4. Can you provide further details explaining the items listed in operations in your provided budget? We are especially curious about the software and environmental measurement tools.',
'netid' => 'jminor',
'deadline' => '2014-02-24 16:38:17.000000',
'created_at' => '2014-02-19 16:38:17.000000',
'questionable_id' => 445,
'questionable_type' => 'App\\Project',
),
222 => 
array (
'id_old' => 2999225,
'question_text' => 'Hello Dr. Ogden, Manuel and Juan,
Thank you for meeting with the Green Fund Committee last night. After speaking with you and reviewing the proposal, the Committee is requesting clarification on several points.

Please respond by Tuesday, Feb 25 by noon.

1. Could this project potentially produce enough algae biofuel that the UA could use to satisfy some (or all) of its fuel demand?

2. The proposal budget lists a computer control system used to monitor the algae reactor.  Could you please tell us more about this system (i.e. the components of it and how it functions etc.)?

3. The proposal states that if funded, the Green Fund will be accredited with a plaque.  In addition, it states that marketing videos will be made monthly; however, these components are not listed in the proposal budget.  Will these aspects be supported through other means of funding? Or through the Green Fund?',
'netid' => 'jminor',
'deadline' => '2014-02-24 19:27:17.000000',
'created_at' => '2014-02-19 19:27:17.000000',
'questionable_id' => 434,
'questionable_type' => 'App\\Project',
),
223 => 
array (
'id_old' => 2999226,
'question_text' => 'Hello Matt,
Thank you for meeting with the Green Fund Committee on Tuesday. The Committee would like to ask for some clarification on several questions. Please respond by next Tuesday.

1. While the Green Fund is aware that this project has been running for several years, we would like to know what other sources of funding have been provided in the last two years since the time that the Green Fund accepted your first proposal. Have these other sources also been applied to this year? Has there been an attempt to leverage the Green Fund\'s acceptance of your very first proposal towards external grants? 

2. If the Green Fund was only able to allocate money for the first year of the project, which specific sources would you go to to ensure that the project was kept running? ',
'netid' => 'jminor',
'deadline' => '2014-02-25 20:32:09.000000',
'created_at' => '2014-02-20 20:32:09.000000',
'questionable_id' => 449,
'questionable_type' => 'App\\Project',
),
224 => 
array (
'id_old' => 2999227,
'question_text' => 'Q: Hello Sarah,
Thank you for meeting with the Green Fund Committee last Tuesday. After reviewing your proposal, the Committee would like to ask you a few clarifying questions.

Please respond as soon as possible.

This year\'s allocation requests to the Green Fund exceed the Green Fund\'s budget by a little more than 2:1. The Green Fund is soliciting feedback from proposal authors on how projects might be successful if partially funded.

1) If only partial funding is available for your project, please specify which project elements are most central to overall success of your goals.

2) We understand that a partially funded project must also necessarily reduce its goals and desired impacts. Considering your answer to question 1, please discuss how reducing the scope of the project would affect project deliverables.

3) Does your team have plans to apply for other funding that might cover some of the cost associated with your project (i.e. invited speaker grants from WEES, GPSC, and so forth)?',
'netid' => 'jminor',
'deadline' => '2014-03-01 23:48:30.000000',
'created_at' => '2014-02-24 23:48:30.000000',
'questionable_id' => 432,
'questionable_type' => 'App\\Project',
),
225 => 
array (
'id_old' => 2999228,
'question_text' => 'Hello Dr. Pryor,
The Green Fund Committee has twice attempted to ask you for clarification on a point related to your proposal, with no response. This is the last opportunity you will have to respond to the question. 

The Green Fund Committee will announce allocation decisions next week, and respectfully requests a response no later than 3:00pm today, Feb 25, if you wish to expand on the point below. 

The question that has been previously posed is posted below:

Q: Hello Dr. Pryor,
Thank you for meeting with the Green Fund Committee on 02.04.14. After hearing from you and reviewing your proposal, the Committee would like clarification on the following question.

You explained that 5% of the project income goes to the Food Bank as a donation. Additional proceeds will pay for project operational costs. Eventually, this project is expected to \'break even\' and then start generating profit. As the operations become profitable, what university department(s) and/or program(s) would be supported by these profits?',
'netid' => 'jminor',
'deadline' => '2014-03-02 18:06:22.000000',
'created_at' => '2014-02-25 18:06:22.000000',
'questionable_id' => 421,
'questionable_type' => 'App\\Project',
),
226 => 
array (
'id_old' => 2999229,
'question_text' => 'Hello Linda,
Thank you for meeting with the Green Fund Committee last night. After talking with you and reviewing your proposal, the Committee has requested clarification on several points.

Please respond by Tuesday, Feb 25 by noon.

1. The two buildings owned by City High School are tall and might block sunlight in the alleyway. The committee would like more details on your structural plans for ensuring the garden plants will receive adequate sunlight to grow in the alleyway.

2. Funding is requested for prototypes to be built as models for the garden in the alleyway. The Green Fund Committee is curious how the prototypes will be utilized after the garden is completely installed and prototypes are no longer needed at the location.

3. Can you provide a photograph of the current status of the alleyway? Also, are you able to provide any pictures or example sketches for how the alleyway may look after the program is installed?

4. Can you provide further details explaining the items listed in operations in your provided budget? We are especially curious about the software and environmental measurement tools.',
'netid' => 'rudnick1',
'deadline' => '2014-03-02 19:26:34.000000',
'created_at' => '2014-02-25 19:26:34.000000',
'questionable_id' => 445,
'questionable_type' => 'App\\Project',
),
227 => 
array (
'id_old' => 2999230,
'question_text' => 'Hello Matt,
Thank you for meeting with the Green Fund Committee on Tuesday. The Committee would like to ask for some clarification on several questions. Please respond by next Tuesday.

1. While the Green Fund is aware that this project has been running for several years, we would like to know what other sources of funding have been provided in the last two years since the time that the Green Fund accepted your first proposal. Have these other sources also been applied to this year? Has there been an attempt to leverage the Green Fund\'s acceptance of your very first proposal towards external grants? 

2. If the Green Fund was only able to allocate money for the first year of the project, which specific sources would you go to to ensure that the project was kept running?',
'netid' => 'rudnick1',
'deadline' => '2014-03-02 19:35:48.000000',
'created_at' => '2014-02-25 19:35:48.000000',
'questionable_id' => 449,
'questionable_type' => 'App\\Project',
),
228 => 
array (
'id_old' => 2999231,
'question_text' => 'Hello Dr. Pryor,
The Green Fund Committee has twice attempted to ask you for clarification on a point related to your proposal, with no response. This is the last opportunity you will have to respond to the question. 

The Green Fund Committee will announce allocation decisions next week, and respectfully requests a response no later than 3:00pm today, Feb 25, if you wish to expand on the point below. 

The question that has been previously posed is posted below:

Q: Hello Dr. Pryor,
Thank you for meeting with the Green Fund Committee on 02.04.14. After hearing from you and reviewing your proposal, the Committee would like clarification on the following question.

You explained that 5% of the project income goes to the Food Bank as a donation. Additional proceeds will pay for project operational costs. Eventually, this project is expected to \'break even\' and then start generating profit. As the operations become profitable, what university department(s) and/or program(s) would be supported by these profits?',
'netid' => 'rudnick1',
'deadline' => '2014-03-02 22:57:25.000000',
'created_at' => '2014-02-25 22:57:25.000000',
'questionable_id' => 421,
'questionable_type' => 'App\\Project',
),
229 => 
array (
'id_old' => 2999232,
'question_text' => 'Hello Ashley,
Thank you for meeting with the Green Fund Committee tonight. After talking with you and reviewing your proposal, the Committee is requesting clarification on several points. 

Please respond as soon as possible, as the open allocation meeting is next week.


We are interested in understanding the higher costs than some cheaper alternatives we are aware of.

1. Could you please provide more detailed cost breakdown estimates for the installation, components, and cisterns themselves? 

2. Could you provide the cistern model number(s) you are looking at?',
'netid' => 'jminor',
'deadline' => '2014-03-03 02:31:54.000000',
'created_at' => '2014-02-26 02:31:54.000000',
'questionable_id' => 444,
'questionable_type' => 'App\\Project',
),
230 => 
array (
'id_old' => 2999233,
'question_text' => 'Dear Hamish,
The Green Fund Committee had one remaining question. Do you feel like you could market this solvent recycler to the broader UA community?  In other words, is there a potential to offer acetone recycling services to other departments to further decrease acetone waste on campus? Thank You.',
'netid' => 'rudnick1',
'deadline' => '2014-03-03 17:36:53.000000',
'created_at' => '2014-02-26 17:36:53.000000',
'questionable_id' => 422,
'questionable_type' => 'App\\Project',
),
231 => 
array (
'id_old' => 2999234,
'question_text' => 'Hi Monkia, 
According to my records your project end date was 12.31.13. Please follow this link to submit your final report along with your project financials. Please let me know if you have any questions. Thanks, Julia',
'netid' => 'rudnick1',
'deadline' => '2014-03-03 19:51:10.000000',
'created_at' => '2014-02-26 19:51:10.000000',
'questionable_id' => 315,
'questionable_type' => 'App\\Project',
),
232 => 
array (
'id_old' => 2999235,
'question_text' => 'Hello James,
Thank you for meeting with the Green Fund Committee on Tuesday. After speaking with you and reviewing your proposal, they are requesting feedback on the question below.

The allocation meeting is next Tuesday, so please reply to this question no later than 10:00 am on Tuesday, March 4th.


1. The Green Fund requests more information regarding your previous engagements and discussions with Facilities Management to approve the use of reclaimed water on UA facilities and campus.',
'netid' => 'jminor',
'deadline' => '2014-03-04 16:04:22.000000',
'created_at' => '2014-02-27 16:04:22.000000',
'questionable_id' => 419,
'questionable_type' => 'App\\Project',
),
233 => 
array (
'id_old' => 2999236,
'question_text' => 'Hello Nate, Thank you for meeting with the Green Fund on Tuesday. After speaking with you and reviewing your proposal, the committee requests clarification on the question below.

The open allocation meeting is next week, so please reply to this question no later than 10:00 am on Tuesday, March 4th.

1. The Green Fund requests more information regarding the Rainwater Harvesting System Design Certificate course. Does the start-up of the certificate program rely completely on the green fund or is the program scheduled to start with the help of other resources?',
'netid' => 'jminor',
'deadline' => '2014-03-04 16:06:18.000000',
'created_at' => '2014-02-27 16:06:18.000000',
'questionable_id' => 433,
'questionable_type' => 'App\\Project',
),
234 => 
array (
'id_old' => 2999237,
'question_text' => 'Q: Hello James,
Thank you for meeting with the Green Fund Committee on Tuesday. After speaking with you and reviewing your proposal, they are requesting feedback on the question below.

The allocation meeting is next Tuesday, so please reply to this question no later than 10:00 am on Tuesday, March 4th.


1. The Green Fund requests more information regarding your previous engagements and discussions with Facilities Management to approve the use of reclaimed water on UA facilities and campus.',
'netid' => 'rudnick1',
'deadline' => '2014-03-09 16:22:58.000000',
'created_at' => '2014-03-04 16:22:58.000000',
'questionable_id' => 419,
'questionable_type' => 'App\\Project',
),
235 => 
array (
'id_old' => 2999238,
'question_text' => 'Hi Sarah,
I am reviewing your proposal and see you are a graduate student. Each Green Fund Proposal must have an employee that is a project. Please identify someone from the school of Geography and Development that would be willing to oversee this project. After you attain their consent, email me their name, email address and title. Thanks, Julia Rudnick',
'netid' => 'rudnick1',
'deadline' => '2014-03-10 21:46:17.000000',
'created_at' => '2014-03-05 21:46:17.000000',
'questionable_id' => 432,
'questionable_type' => 'App\\Project',
),
236 => 
array (
'id_old' => 2999239,
'question_text' => 'Hello Mark,
We have received your PAR, and the Green Fund Committee will consider this request at their next meeting. 

If there is any chance that this project will not entirely wrap up by June 30th, please consider amending your PAR to request project extension through December 31, 2014. If the PAR is approved as it reads now, and moneys are not expended by June 30th, they will have to be returned to the Green Fund. A project extension through December provides financial flexibility for summer extern workers.

If you do not need a project extension, please disregard this question.
-Jesse Minor
Interim Manager, Office of Sustainability',
'netid' => 'jminor',
'deadline' => '2014-04-07 18:56:40.000000',
'created_at' => '2014-04-02 18:56:40.000000',
'questionable_id' => 94,
'questionable_type' => 'App\\Par',
),
237 => 
array (
'id_old' => 2999240,
'question_text' => 'Hi Tim,
The Green Fund received your PAR asking for a one year project extension. Are you also requesting a one year project extension in which to spend moneys allocated to this project? 
Julia Rudnick',
'netid' => 'rudnick1',
'deadline' => '2014-05-03 16:45:16.000000',
'created_at' => '2014-04-28 16:45:16.000000',
'questionable_id' => 393,
'questionable_type' => 'App\\Project',
),
238 => 
array (
'id_old' => 2999241,
'question_text' => 'Hi Melanie,
I was reviewing your financials and see there was a PO payment to Dan Dorsey for $1,000– looks like he works for the Water Resource Research Center (WRRC) here on campus. I am not sure how this fits in with your budget and can’t see where your original proposal says you are going to hire staff to work on the project. Can you let the Green Fund know how this work fit in with your project plan and scope?
Thanks, Julia',
'netid' => 'rudnick1',
'deadline' => '2014-06-23 20:13:37.000000',
'created_at' => '2014-06-16 20:13:37.000000',
'questionable_id' => 396,
'questionable_type' => 'App\\Project',
),
239 => 
array (
'id_old' => 2999242,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:24:24.000000',
'created_at' => '2014-07-21 16:24:24.000000',
'questionable_id' => 418,
'questionable_type' => 'App\\Project',
),
240 => 
array (
'id_old' => 2999243,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:26:34.000000',
'created_at' => '2014-07-21 16:26:34.000000',
'questionable_id' => 442,
'questionable_type' => 'App\\Project',
),
241 => 
array (
'id_old' => 2999244,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:26:41.000000',
'created_at' => '2014-07-21 16:26:41.000000',
'questionable_id' => 429,
'questionable_type' => 'App\\Project',
),
242 => 
array (
'id_old' => 2999245,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:26:50.000000',
'created_at' => '2014-07-21 16:26:50.000000',
'questionable_id' => 434,
'questionable_type' => 'App\\Project',
),
243 => 
array (
'id_old' => 2999246,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:26:59.000000',
'created_at' => '2014-07-21 16:26:59.000000',
'questionable_id' => 437,
'questionable_type' => 'App\\Project',
),
244 => 
array (
'id_old' => 2999247,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:27:05.000000',
'created_at' => '2014-07-21 16:27:05.000000',
'questionable_id' => 431,
'questionable_type' => 'App\\Project',
),
245 => 
array (
'id_old' => 2999248,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:27:11.000000',
'created_at' => '2014-07-21 16:27:11.000000',
'questionable_id' => 447,
'questionable_type' => 'App\\Project',
),
246 => 
array (
'id_old' => 2999249,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:27:18.000000',
'created_at' => '2014-07-21 16:27:18.000000',
'questionable_id' => 445,
'questionable_type' => 'App\\Project',
),
247 => 
array (
'id_old' => 2999250,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:27:24.000000',
'created_at' => '2014-07-21 16:27:24.000000',
'questionable_id' => 448,
'questionable_type' => 'App\\Project',
),
248 => 
array (
'id_old' => 2999251,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:27:34.000000',
'created_at' => '2014-07-21 16:27:34.000000',
'questionable_id' => 426,
'questionable_type' => 'App\\Project',
),
249 => 
array (
'id_old' => 2999252,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:27:41.000000',
'created_at' => '2014-07-21 16:27:41.000000',
'questionable_id' => 441,
'questionable_type' => 'App\\Project',
),
250 => 
array (
'id_old' => 2999253,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:29:26.000000',
'created_at' => '2014-07-21 16:29:26.000000',
'questionable_id' => 444,
'questionable_type' => 'App\\Project',
),
251 => 
array (
'id_old' => 2999254,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:29:33.000000',
'created_at' => '2014-07-21 16:29:33.000000',
'questionable_id' => 424,
'questionable_type' => 'App\\Project',
),
252 => 
array (
'id_old' => 2999255,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:29:39.000000',
'created_at' => '2014-07-21 16:29:39.000000',
'questionable_id' => 421,
'questionable_type' => 'App\\Project',
),
253 => 
array (
'id_old' => 2999256,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:29:45.000000',
'created_at' => '2014-07-21 16:29:45.000000',
'questionable_id' => 422,
'questionable_type' => 'App\\Project',
),
254 => 
array (
'id_old' => 2999257,
'question_text' => 'Dear Project Manager,
The Office of Sustainability was recently realigned under the Dean of Students. As a result, all accounts are in the process of being created and switched over to the new department. This will cause a slight delay in funding Green Fund projects.   If you have any questions please contact Julia Rudnick at Rudnick1@email.arizona.edu. You do not need to respond to this notice. Thank you for your patience.  Julia',
'netid' => 'rudnick1',
'deadline' => '2014-07-28 16:30:05.000000',
'created_at' => '2014-07-21 16:30:05.000000',
'questionable_id' => 446,
'questionable_type' => 'App\\Project',
),
255 => 
array (
'id_old' => 2999258,
'question_text' => 'Dear Mr. Astroth,
The Green Fund Committee requests more information regarding employee work time. During FY15, the Green Fund funded a composting toilet, low flow shower heads, and a water and energy efficient dishwasher and icemaker. The committee is interested in an estimate of the number of UA students who have been served by the camp, what projects still need to be completed and an estimate on how much it will cost in time/labor to install the composting toilet. Thank you.',
'netid' => 'rudnick1',
'deadline' => '2015-01-24 22:01:15.000000',
'created_at' => '2015-01-20 22:01:15.000000',
'questionable_id' => 109,
'questionable_type' => 'App\\Par',
),
256 => 
array (
'id_old' => 2999259,
'question_text' => 'Hi Gale,

Thank you for your attendance at last night\'s meeting, we enjoyed learning more about your proposal. After discussing your project, we came up with the following additional question. 

1. Do you have plans to include Compost Cats, Facilities Management, and others to make the event more sustainable?

Thank you for taking the time to answer our question.

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-09 17:23:22.000000',
'created_at' => '2015-02-04 17:23:22.000000',
'questionable_id' => 456,
'questionable_type' => 'App\\Project',
),
257 => 
array (
'id_old' => 2999260,
'question_text' => 'Hi Stephanie,

Thank you for your attendance at last night\'s meeting, we enjoyed learning more about your proposal. After discussing your project, we came up with the following additional questions. 

1. Will there be plots for different groups or one area communally? 
2. Is there any way for Jill to increase programming from once a month to twice a month for these dorms?
3. Can we get a 5 year plan for the garden? (justify subsequent year estimate budget).

Thank you for taking the time to answer our questions.

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-09 17:28:50.000000',
'created_at' => '2015-02-04 17:28:50.000000',
'questionable_id' => 460,
'questionable_type' => 'App\\Project',
),
258 => 
array (
'id_old' => 2999261,
'question_text' => 'Dear Susan,

Thank you for taking the time to apply for a green fund annual grant, we appreciate your efforts in making the UA a more sustainable institution. After discussing your project, we came up with the following questions. 

1. Do you have a vision for sustainability related projects? 
2. Where do you see the campus going in regard to sustainability? 
3. Do you have approval from your facilities for the projects?

Thank you for taking the time to answer our questions.

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-09 17:32:37.000000',
'created_at' => '2015-02-04 17:32:37.000000',
'questionable_id' => 455,
'questionable_type' => 'App\\Project',
),
259 => 
array (
'id_old' => 2999262,
'question_text' => 'Dear Barry,

Thank you for your attendance at last night\'s meeting, we enjoyed learning more about your proposal. After discussing your project, we came up with the following additional questions. 

1. Do you have plans to sell your mushrooms to restaurants and other entities? If so, who would you sell them to?
2. Will there be people working over the summer? If you are aiming to make $1000 a week, working during the summer will be necessary to meet this goal.
3. You state that you make at least $300 per week, please revise your budget to reflect your revenues. 

Thank you for taking the time to answer our questions.

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-09 17:36:46.000000',
'created_at' => '2015-02-04 17:36:46.000000',
'questionable_id' => 470,
'questionable_type' => 'App\\Project',
),
260 => 
array (
'id_old' => 2999263,
'question_text' => 'Dear Melanie,

Thank you for your attendance at last night\'s meeting, we enjoyed learning more about your proposal. After discussing your project, we came up with the following additional questions. 

1. Is the GA position for an academic or fiscal year?
2. What justifies the increase from last year to this year?

Thank you for taking the time to answer our questions.

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-09 17:38:35.000000',
'created_at' => '2015-02-04 17:38:35.000000',
'questionable_id' => 472,
'questionable_type' => 'App\\Project',
),
261 => 
array (
'id_old' => 2999264,
'question_text' => 'Stephanie, As per your email I am sending you another question request. This will allow you the opportunity to access the site again. Julia',
'netid' => 'rudnick1',
'deadline' => '2015-02-14 15:38:20.000000',
'created_at' => '2015-02-09 15:38:20.000000',
'questionable_id' => 460,
'questionable_type' => 'App\\Project',
),
262 => 
array (
'id_old' => 2999265,
'question_text' => 'Dear Melanie Lenart,

Thank you for taking the time to share more about your project with the Green Fund Committee last week. After discussion, we have come up with a couple more questions for you.

1. Is the GA position for an academic year or a fiscal year?
2. What justifies the budget increase from last year to this year?

Thank you for your time, have a good week. 

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-14 16:27:49.000000',
'created_at' => '2015-02-09 16:27:49.000000',
'questionable_id' => 472,
'questionable_type' => 'App\\Project',
),
263 => 
array (
'id_old' => 2999266,
'question_text' => 'Dear Susan Pater,

Thank you for taking the time to share your project with the Green Fund Committee. After discussion, we have come up with a couple of questions for you in regards to your proposal.

1. Do you have a vision for sustainability related projects?
2. Where do you see the campus going in regards to sustainability?
3. Do you have approval from your facilities for the projects?

Thank you for your time, have a good week. 

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-14 16:30:59.000000',
'created_at' => '2015-02-09 16:30:59.000000',
'questionable_id' => 455,
'questionable_type' => 'App\\Project',
),
264 => 
array (
'id_old' => 2999267,
'question_text' => 'Dear Andrew,

Thank you for taking the time to share more about your proposal with the Green Fund Committee at last night\'s meeting. Upon further discussion, we have come up with a couple of additional questions regarding your project. 

1. How will you connect the cistern to the garden irrigation?
2. Will water go to both the hedgerow and the garden plots?
3. Cost installation per day for FM, how much could be installed in one day?
4. Can you describe the expected costs associated with maintaining the cisterns and who would be responsible for paying for these?

Thank you for your time. 

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-16 20:14:54.000000',
'created_at' => '2015-02-11 20:14:54.000000',
'questionable_id' => 465,
'questionable_type' => 'App\\Project',
),
265 => 
array (
'id_old' => 2999268,
'question_text' => 'Dear Grant, 

Thank you for taking the time to share more about your proposal with the Green Fund Committee at last night\'s meeting. Upon further discussion, we have come up with a couple of additional questions regarding your project. 

1. Please elaborate on why the costs for materials and signage are so high.
2. We need a letter from the city and FM to show that this project should be formally approved. 
3. Is the water harvesting workshop on the mall an actual workshop or more like tabling?

Thank you for your time. 

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-16 20:18:19.000000',
'created_at' => '2015-02-11 20:18:19.000000',
'questionable_id' => 469,
'questionable_type' => 'App\\Project',
),
266 => 
array (
'id_old' => 2999269,
'question_text' => 'Dear Ace,

Thank you for taking the time to share more about your proposal with the Green Fund Committee at last night\'s meeting. Upon further discussion, we have come up with a couple of additional questions regarding your project. 


1.Can you explain how you will participate in the conference a bit more? Is there a competition associated with this conference?

Thank you for your time. 

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-16 20:20:01.000000',
'created_at' => '2015-02-11 20:20:01.000000',
'questionable_id' => 474,
'questionable_type' => 'App\\Project',
),
267 => 
array (
'id_old' => 2999270,
'question_text' => 'Dear Ed,

Thank you for taking the time to share more about your proposal with the Green Fund Committee at last night\'s meeting. Upon further discussion, we have come up with a couple of additional questions regarding your project. 

1. What are the existing course lab fees and what do they cover?
2. How will the savings from this project ultimately save students money in the future?

Thank you for your time. 

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-16 20:21:23.000000',
'created_at' => '2015-02-11 20:21:23.000000',
'questionable_id' => 452,
'questionable_type' => 'App\\Project',
),
268 => 
array (
'id_old' => 2999271,
'question_text' => 'Dear Kirk,

Thank you for taking the time to share more about your proposal with the Green Fund Committee at last night\'s meeting. Upon further discussion, we have come up with an additional question regarding your project. 

1. Have you reached out to ASU at all to advertise for the retreats to the camp?

Thank you for your time. 

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-16 20:23:08.000000',
'created_at' => '2015-02-11 20:23:08.000000',
'questionable_id' => 451,
'questionable_type' => 'App\\Project',
),
269 => 
array (
'id_old' => 2999272,
'question_text' => 'Dear Melanie,

Please answer the following questions submitted to you by the Green Fund Committee on 02.04.15 and 02.09.05. 

1. Is the GA position for an academic or fiscal year?
2. What justifies the increase from last year to this year?

Thank you for your time.

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-21 20:27:09.000000',
'created_at' => '2015-02-16 20:27:09.000000',
'questionable_id' => 472,
'questionable_type' => 'App\\Project',
),
270 => 
array (
'id_old' => 2999273,
'question_text' => 'Dear Andrew,

Please answer the following questions submitted to you by the Green Fund Committee on 02.11.05. 

1. How will you connect the cistern to the garden irrigation?
2. Will water go to both the hedgerow and the garden plots?
3. Cost installation per day for FM, how much could be installed in one day?
4. Can you describe the expected costs associated with maintaining the cisterns and who would be responsible for paying for these?

Thank you for your time.

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-21 20:28:59.000000',
'created_at' => '2015-02-16 20:28:59.000000',
'questionable_id' => 465,
'questionable_type' => 'App\\Project',
),
271 => 
array (
'id_old' => 2999274,
'question_text' => 'Dear Grent,

Please answer the following questions submitted to you by the Green Fund Committee on 02.11.15. 

1. Please elaborate on why the costs for materials and signage are so high.
2. We need a letter from the city and FM to show that this project should be formally approved. 
3. Is the water harvesting workshop on the mall an actual workshop or more like tabling?

Thank you for your time.

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-21 20:32:30.000000',
'created_at' => '2015-02-16 20:32:30.000000',
'questionable_id' => 469,
'questionable_type' => 'App\\Project',
),
272 => 
array (
'id_old' => 2999275,
'question_text' => 'Dear Ace,

Please answer the following question submitted to you by the Green Fund Committee on 02.11.15. 

1.Can you explain how you will participate in the conference a bit more? Is there a competition associated with this conference?

Thank you for your time.

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-21 20:33:39.000000',
'created_at' => '2015-02-16 20:33:39.000000',
'questionable_id' => 474,
'questionable_type' => 'App\\Project',
),
273 => 
array (
'id_old' => 2999276,
'question_text' => 'Dear Noelle,

Thank you for contributing your time and effort into making the UA a more sustainable institution. Upon further review of your proposal, we as a committee would appreciate your answers to the following questions. 

1. Can you tell us more about the Sustainability Graduate Fellow position, and why that is necessary to the success of the program? Will they be focusing on this curriculum or doing other things for sky school?

2. How will you select the intern, and how will the position be advertised?

3. Could the project possibly be completed without an extra grad fellow? What would change if this were the case?

4. Do you envision this to be a one-time position or something that would continue? If it would be an ongoing position, who would fund it in the future? 

5. Who will be responsible for transporting the compost and recycling after year 1? How will transporting compost and recycling materials affect your current operations? 

6. Have you thought long-term about other structural and facility changes that are needed and where to get funding for those?

Thank you.

Sincerely,

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2015-02-23 17:11:10.000000',
'created_at' => '2015-02-18 17:11:10.000000',
'questionable_id' => 453,
'questionable_type' => 'App\\Project',
),
274 => 
array (
'id_old' => 2999277,
'question_text' => 'Dear Project Manager,
The Green Fund committee has a few questions in regards to your recent PAR. Please answer them as soon as possible as they will be making decisions regarding you PAR during their last open meeting next Tuesday, 05.02.15. Thank you.
-How were you going to pay for items identified in your PAR if your request to move $7,500  to operations is denied and are these items (organic seal) in your original plan/proposal?',
'netid' => 'rudnick1',
'deadline' => '2015-05-03 16:33:22.000000',
'created_at' => '2015-04-30 16:33:22.000000',
'questionable_id' => 424,
'questionable_type' => 'App\\Project',
),
275 => 
array (
'id_old' => 2999278,
'question_text' => 'Dear Chet, 

Please answer the following questions regarding your project "Compost Cats: A Collaborative Model for Harvesting the Waste of Nations and Training Student Leaders"

- Could you provide a more detailed budget so we are clear on exactly where the money would be going?


Sincerely, 

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2016-02-17 20:14:09.000000',
'created_at' => '2016-02-12 20:14:09.000000',
'questionable_id' => 491,
'questionable_type' => 'App\\Project',
),
276 => 
array (
'id_old' => 2999279,
'question_text' => 'Dear Chet, 

Please answer the following questions regarding your project "UA Community Garden"

- The total budget amounts to more than the requested amount of money. What is the source of that additional funding? Is there a way that it could fund more of the project? (extra $2,100). 


Sincerely, 

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2016-02-17 20:15:42.000000',
'created_at' => '2016-02-12 20:15:42.000000',
'questionable_id' => 501,
'questionable_type' => 'App\\Project',
),
277 => 
array (
'id_old' => 2999280,
'question_text' => 'Dear Joel, 

Please answer the following questions regarding your project "Cats in the Green Box"

- Is it possible to think about moving this to a location that could have a greater student impact? 
-Is there an energy storage plan for consistent lighting even if it is cloudy?

Sincerely, 

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2016-02-17 20:17:20.000000',
'created_at' => '2016-02-12 20:17:20.000000',
'questionable_id' => 507,
'questionable_type' => 'App\\Project',
),
278 => 
array (
'id_old' => 2999281,
'question_text' => 'Dear Colin, 

Please answer the following questions regarding your project "Community Outreach in Environmental Learning"

- We get the sense that the camp does a lot and encompasses a lot of programs, can we get specifics of what these programs are and how they are meant to translate back to the University campus?
-What does the Cooper Center do? What would these paid positions be doing specifically? Please provide a layout more than a timeline of what it will look like. 


Sincerely, 

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2016-02-17 20:21:11.000000',
'created_at' => '2016-02-12 20:21:11.000000',
'questionable_id' => 479,
'questionable_type' => 'App\\Project',
),
279 => 
array (
'id_old' => 2999282,
'question_text' => 'Dear Ebitie, 

After discussing your project "E-Waste Recycling" , we as a committee have come up with a couple of questions for you. They are as follows:

Would it be possible for you to keep a strong focus on reusing items that are reusable and still in good condition? There may be a lot of perfectly good items that can be reused before breaking them down, and that can increase sales. 

Thank you for taking the time to make the University of Arizona a better place to live, work, and learn. 

Sincerely, 

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2016-02-24 17:15:20.000000',
'created_at' => '2016-02-19 17:15:20.000000',
'questionable_id' => 439,
'questionable_type' => 'App\\Project',
),
280 => 
array (
'id_old' => 2999283,
'question_text' => 'Dear Chris, 

After discussing your project "UA Sustainability Campus Campaigns Coordinator" , we as a committee have come up with a couple of questions for you. They are as follows:

What background would the ideal candidate for this position have?

Thank you for taking the time to make the University of Arizona a better place to live, work, and learn. 

Sincerely, 

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2016-02-24 17:18:19.000000',
'created_at' => '2016-02-19 17:18:19.000000',
'questionable_id' => 487,
'questionable_type' => 'App\\Project',
),
281 => 
array (
'id_old' => 2999284,
'question_text' => 'Q: Dear Chet, 

Please answer the following questions regarding your project "Compost Cats: A Collaborative Model for Harvesting the Waste of Nations and Training Student Leaders"

- Could you provide a more detailed budget so we are clear on exactly where the money would be going?


Sincerely, 

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2016-02-27 17:13:49.000000',
'created_at' => '2016-02-22 17:13:49.000000',
'questionable_id' => 491,
'questionable_type' => 'App\\Project',
),
282 => 
array (
'id_old' => 2999285,
'question_text' => 'Dear Michael,  

Please answer the following questions regarding your project "Signs for Sustainability"

- What bins are you specifically wanting to change and where would they be placed?

Sincerely, 

The Green Fund Committee',
'netid' => 'natalieramos',
'deadline' => '2016-02-27 17:16:57.000000',
'created_at' => '2016-02-22 17:16:57.000000',
'questionable_id' => 483,
'questionable_type' => 'App\\Project',
),
283 => 
array (
'id_old' => 2999286,
'question_text' => 'Hi Ed, 
In regards to this PAR, I am uncertain what you are asking for. The normal project end date is 06.30.16. Your PAR indicates you will be done with the project by that time. Are you requesting something else from the Green Fund Committee?
Thanks, Julia',
'netid' => 'rudnick1',
'deadline' => '2016-04-03 19:16:50.000000',
'created_at' => '2016-03-29 19:16:50.000000',
'questionable_id' => 134,
'questionable_type' => 'App\\Par',
),
284 => 
array (
'id_old' => 2999287,
'question_text' => 'Dear Project Manager,
In regards to the PAR submitted on 03.29.16, are you requesting to transfer $747.00 from the travel category to the operations category?
Thank you.',
'netid' => 'rudnick1',
'deadline' => '2016-04-04 16:15:45.000000',
'created_at' => '2016-03-30 16:15:45.000000',
'questionable_id' => 459,
'questionable_type' => 'App\\Project',
),
285 => 
array (
'id_old' => 2999288,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you. 

Your meeting has been scheduled for: 
Date: Monday January 30, 2017
Time: 5:05pm-5:10pm
Location: DOS- Nugent Building- main conference room.

Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'rudnick1',
'deadline' => '2017-01-31 22:16:42.000000',
'created_at' => '2017-01-26 22:16:42.000000',
'questionable_id' => 524,
'questionable_type' => 'App\\Project',
),
286 => 
array (
'id_old' => 2999289,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you. 

Your meeting has been scheduled for: 
Date: Monday January 30, 2017
Time: 5:10pm-5:15pm
Location: DOS- Nugent Building- main conference room.

Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'rudnick1',
'deadline' => '2017-01-31 22:17:17.000000',
'created_at' => '2017-01-26 22:17:17.000000',
'questionable_id' => 510,
'questionable_type' => 'App\\Project',
),
287 => 
array (
'id_old' => 2999290,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you. 

Your meeting has been scheduled for: 
Date: Monday January 30, 2017
Time: 5:20pm-5:25pm
Location: DOS- Nugent Building- main conference room.

Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'rudnick1',
'deadline' => '2017-01-31 22:17:57.000000',
'created_at' => '2017-01-26 22:17:57.000000',
'questionable_id' => 525,
'questionable_type' => 'App\\Project',
),
288 => 
array (
'id_old' => 2999291,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you. 

Your meeting has been scheduled for: 
Date: Monday January 30, 2017
Time: 5:25pm-5:30pm
Location: DOS- Nugent Building- main conference room.

Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'rudnick1',
'deadline' => '2017-01-31 22:18:48.000000',
'created_at' => '2017-01-26 22:18:48.000000',
'questionable_id' => 532,
'questionable_type' => 'App\\Project',
),
289 => 
array (
'id_old' => 2999292,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you. 

Your meeting has been scheduled for: 
Date: Monday January 30, 2017
Time: 5:35pm-5:40pm
Location: DOS- Nugent Building- main conference room.

Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'rudnick1',
'deadline' => '2017-01-31 22:19:28.000000',
'created_at' => '2017-01-26 22:19:28.000000',
'questionable_id' => 533,
'questionable_type' => 'App\\Project',
),
290 => 
array (
'id_old' => 2999293,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you. 

Your meeting has been scheduled for: 
Date: Monday January 30, 2017
Time: 5:40pm-5:45pm
Location: DOS- Nugent Building- main conference room.

Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'rudnick1',
'deadline' => '2017-01-31 22:20:39.000000',
'created_at' => '2017-01-26 22:20:39.000000',
'questionable_id' => 536,
'questionable_type' => 'App\\Project',
),
291 => 
array (
'id_old' => 2999294,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you. 

Your meeting has been scheduled for: 
Date: Monday January 30, 2017
Time: 5:50pm-5:55pm
Location: DOS- Nugent Building- main conference room.

Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'rudnick1',
'deadline' => '2017-01-31 22:22:18.000000',
'created_at' => '2017-01-26 22:22:18.000000',
'questionable_id' => 537,
'questionable_type' => 'App\\Project',
),
292 => 
array (
'id_old' => 2999295,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 6, 2017
Time: 5:05pm-5:10pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:26:18.000000',
'created_at' => '2017-01-31 16:26:18.000000',
'questionable_id' => 450,
'questionable_type' => 'App\\Project',
),
293 => 
array (
'id_old' => 2999296,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 6, 2017
Time: 5:10pm-5:15pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:26:53.000000',
'created_at' => '2017-01-31 16:26:53.000000',
'questionable_id' => 528,
'questionable_type' => 'App\\Project',
),
294 => 
array (
'id_old' => 2999297,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 6, 2017
Time: 5:20pm-5:25pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:27:38.000000',
'created_at' => '2017-01-31 16:27:38.000000',
'questionable_id' => 521,
'questionable_type' => 'App\\Project',
),
295 => 
array (
'id_old' => 2999298,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 6, 2017
Time: 5:25pm-5:30pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:28:10.000000',
'created_at' => '2017-01-31 16:28:10.000000',
'questionable_id' => 529,
'questionable_type' => 'App\\Project',
),
296 => 
array (
'id_old' => 2999299,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 6, 2017
Time: 5:35pm-5:40pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:28:42.000000',
'created_at' => '2017-01-31 16:28:42.000000',
'questionable_id' => 522,
'questionable_type' => 'App\\Project',
),
297 => 
array (
'id_old' => 2999300,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 6, 2017
Time: 5:40pm-5:45pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:29:21.000000',
'created_at' => '2017-01-31 16:29:21.000000',
'questionable_id' => 526,
'questionable_type' => 'App\\Project',
),
298 => 
array (
'id_old' => 2999301,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 6, 2017
Time: 5:50pm-5:55pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:29:52.000000',
'created_at' => '2017-01-31 16:29:52.000000',
'questionable_id' => 527,
'questionable_type' => 'App\\Project',
),
299 => 
array (
'id_old' => 2999302,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 6, 2017
Time: 5:55pm-6:00pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:30:21.000000',
'created_at' => '2017-01-31 16:30:21.000000',
'questionable_id' => 530,
'questionable_type' => 'App\\Project',
),
300 => 
array (
'id_old' => 2999303,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 13, 2017
Time: 5:05pm-5:10pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:32:16.000000',
'created_at' => '2017-01-31 16:32:16.000000',
'questionable_id' => 535,
'questionable_type' => 'App\\Project',
),
301 => 
array (
'id_old' => 2999304,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 13, 2017
Time: 5:10pm-5:15pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:32:44.000000',
'created_at' => '2017-01-31 16:32:44.000000',
'questionable_id' => 519,
'questionable_type' => 'App\\Project',
),
302 => 
array (
'id_old' => 2999305,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 13, 2017
Time: 5:20pm-5:25pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:33:09.000000',
'created_at' => '2017-01-31 16:33:09.000000',
'questionable_id' => 531,
'questionable_type' => 'App\\Project',
),
303 => 
array (
'id_old' => 2999306,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 13, 2017
Time: 5:25pm-5:30pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:33:41.000000',
'created_at' => '2017-01-31 16:33:41.000000',
'questionable_id' => 513,
'questionable_type' => 'App\\Project',
),
304 => 
array (
'id_old' => 2999307,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 13, 2017
Time: 5:35pm-5:40pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:34:11.000000',
'created_at' => '2017-01-31 16:34:11.000000',
'questionable_id' => 516,
'questionable_type' => 'App\\Project',
),
305 => 
array (
'id_old' => 2999308,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 13, 2017
Time: 5:40pm-5:45pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:34:41.000000',
'created_at' => '2017-01-31 16:34:41.000000',
'questionable_id' => 518,
'questionable_type' => 'App\\Project',
),
306 => 
array (
'id_old' => 2999309,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 13, 2017
Time: 5:50pm-5:55pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-05 16:35:10.000000',
'created_at' => '2017-01-31 16:35:10.000000',
'questionable_id' => 517,
'questionable_type' => 'App\\Project',
),
307 => 
array (
'id_old' => 2999310,
'question_text' => 'Dear Project Manager,
The Green Fund committee had five more questions. Please respond using the link(s) provided. You will receive five questions in total with five separate answer links. The links will expire in 5 days. Thanks!
1) Would the intention of the project be diminished if mobile cooking cart were not funded?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 18:49:51.000000',
'created_at' => '2017-01-31 18:49:51.000000',
'questionable_id' => 524,
'questionable_type' => 'App\\Project',
),
308 => 
array (
'id_old' => 2999311,
'question_text' => 'Dear Project Manager,
The Green Fund committee had five more questions. Please respond using the link(s) provided. You will receive five questions in total with five separate answer links. The links will expire in 5 days. Thanks!
2) Will tables from Pangaea patio be removed to accommodate garden?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 18:50:25.000000',
'created_at' => '2017-01-31 18:50:25.000000',
'questionable_id' => 524,
'questionable_type' => 'App\\Project',
),
309 => 
array (
'id_old' => 2999312,
'question_text' => 'Dear Project Manager,
The Green Fund committee had five more questions. Please respond using the link(s) provided. You will receive five questions in total with five separate answer links. The links will expire in 5 days. Thanks!
3) Can the goals of the project be accomplished with only one garden location?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 18:50:46.000000',
'created_at' => '2017-01-31 18:50:46.000000',
'questionable_id' => 524,
'questionable_type' => 'App\\Project',
),
310 => 
array (
'id_old' => 2999313,
'question_text' => 'Dear Project Manager,
The Green Fund committee had five more questions. Please respond using the link(s) provided. You will receive five questions in total with five separate answer links. The links will expire in 5 days. Thanks!
4) Has and will the SUMC collaborate with the UA Community Garden?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 18:51:15.000000',
'created_at' => '2017-01-31 18:51:15.000000',
'questionable_id' => 524,
'questionable_type' => 'App\\Project',
),
311 => 
array (
'id_old' => 2999314,
'question_text' => 'Dear Project Manager,
The Green Fund committee had five more questions. Please respond using the link(s) provided. You will receive five questions in total with five separate answer links. The links will expire in 5 days. Thanks!
5) Will the mobile cooking unit be available for use by other university programs and units?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 18:51:34.000000',
'created_at' => '2017-01-31 18:51:34.000000',
'questionable_id' => 524,
'questionable_type' => 'App\\Project',
),
312 => 
array (
'id_old' => 2999315,
'question_text' => 'Dear Project Manager,
The Green Fund committee as a few more questions. Please respond using the link provided.  The links will expire in 5 days. Thanks!
Do you have solid leads on funding items for the project once it is designed?
The project outlines hiring students for a total pay of $2000. Who will you hire and what is  the major job function?
Thanks!',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 18:56:33.000000',
'created_at' => '2017-01-31 18:56:33.000000',
'questionable_id' => 510,
'questionable_type' => 'App\\Project',
),
313 => 
array (
'id_old' => 2999316,
'question_text' => 'Dear Project Manager,
The Green Fund committee as a few more questions. Please respond using the link provided.  The links will expire in 5 days. Thanks!
1) Will you integrate this workshop with other workshops you currently offer? 
2) What is the overall picture of the workshops? Do you take the workshops in a series?
3) Specifically, who and how many people will be impacted by these workshops?

Thanks!',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 18:59:27.000000',
'created_at' => '2017-01-31 18:59:27.000000',
'questionable_id' => 525,
'questionable_type' => 'App\\Project',
),
314 => 
array (
'id_old' => 2999317,
'question_text' => 'Dear Project Manager,
The Green Fund committee had a few more questions. Please respond using the link(s) provided.  The links will expire in 5 days. Thanks!
1) Have you considered applying for ASUA stipend staff money to pay for the garden manager position?
2) Do you foresee this project being funded by the Green Fund every year?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 19:02:42.000000',
'created_at' => '2017-01-31 19:02:42.000000',
'questionable_id' => 532,
'questionable_type' => 'App\\Project',
),
315 => 
array (
'id_old' => 2999318,
'question_text' => 'The Green Fund committee had a few more questions. Please respond using the link(s) provided. The links will expire in 5 days. Thanks!
1) Do you see a time in the near future in which athletics will pay for the program? 
2) What will be your pitch for the new athletic director to get him/her engaged and financially committed to the program?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 19:13:10.000000',
'created_at' => '2017-01-31 19:13:10.000000',
'questionable_id' => 533,
'questionable_type' => 'App\\Project',
),
316 => 
array (
'id_old' => 2999319,
'question_text' => 'The Green Fund committee had a question regarding this project. Please respond using the link(s) provided. The links will expire in 5 days. Thanks!
This project is requesting two filling stations. How would the project be impacted if only one water refill station was funded?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 19:17:28.000000',
'created_at' => '2017-01-31 19:17:28.000000',
'questionable_id' => 536,
'questionable_type' => 'App\\Project',
),
317 => 
array (
'id_old' => 2999320,
'question_text' => 'The Green Fund committee had six more questions. Please respond using the link(s) provided. You will receive six questions in total with six separate answer links. The links will expire in 5 days. Thanks!
1) Has this project been planned any further since the request was submitted and you have a quote or have you priced the project for solar panels, racks, and inverters?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 19:23:55.000000',
'created_at' => '2017-01-31 19:23:55.000000',
'questionable_id' => 537,
'questionable_type' => 'App\\Project',
),
318 => 
array (
'id_old' => 2999321,
'question_text' => 'The Green Fund committee had six more questions. Please respond using the link(s) provided. You will receive six questions in total with six separate answer links. The links will expire in 5 days. Thanks!
2) Have you made contact with Facilities Management (FM) or Planning, Design, & Construction (PD&C) and have approval for the project from these units?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 19:26:43.000000',
'created_at' => '2017-01-31 19:26:43.000000',
'questionable_id' => 537,
'questionable_type' => 'App\\Project',
),
319 => 
array (
'id_old' => 2999322,
'question_text' => 'The Green Fund committee had six more questions. Please respond using the link(s) provided. You will receive six questions in total with six separate answer links. The links will expire in 5 days. Thanks!
3) In regards to the ENR2 building, what percent of power are you trying to offset with this solar project and how many panels will you have to use to accomplish this goal?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 19:29:11.000000',
'created_at' => '2017-01-31 19:29:11.000000',
'questionable_id' => 537,
'questionable_type' => 'App\\Project',
),
320 => 
array (
'id_old' => 2999323,
'question_text' => 'The Green Fund committee had six more questions. Please respond using the link(s) provided. You will receive six questions in total with six separate answer links. The links will expire in 5 days. Thanks!
4) What the intended size of the array and have you settled on a project design?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 19:31:16.000000',
'created_at' => '2017-01-31 19:31:16.000000',
'questionable_id' => 537,
'questionable_type' => 'App\\Project',
),
321 => 
array (
'id_old' => 2999324,
'question_text' => 'The Green Fund committee had six more questions. Please respond using the link(s) provided. You will receive six questions in total with six separate answer links. The links will expire in 5 days. Thanks!
5) Will the system be grid tied or require a battery backup?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 19:32:28.000000',
'created_at' => '2017-01-31 19:32:28.000000',
'questionable_id' => 537,
'questionable_type' => 'App\\Project',
),
322 => 
array (
'id_old' => 2999325,
'question_text' => 'The Green Fund committee had six more questions. Please respond using the link(s) provided. You will receive six questions in total with six separate answer links. The links will expire in 5 days. Thanks!
6) Is the building electrical system designed for this array and who will pay to replace the inverter and provide system maintenance (such as occasionally clean the panels)?',
'netid' => 'rudnick1',
'deadline' => '2017-02-05 19:36:29.000000',
'created_at' => '2017-01-31 19:36:29.000000',
'questionable_id' => 537,
'questionable_type' => 'App\\Project',
),
323 => 
array (
'id_old' => 2999326,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 20, 2017
Time: 5:05pm-5:10pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-07 17:19:02.000000',
'created_at' => '2017-02-02 17:19:02.000000',
'questionable_id' => 523,
'questionable_type' => 'App\\Project',
),
324 => 
array (
'id_old' => 2999327,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 20, 2017
Time: 5:10pm-5:15pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-07 17:19:31.000000',
'created_at' => '2017-02-02 17:19:31.000000',
'questionable_id' => 520,
'questionable_type' => 'App\\Project',
),
325 => 
array (
'id_old' => 2999328,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 20, 2017
Time: 5:20pm-5:25pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-07 17:19:56.000000',
'created_at' => '2017-02-02 17:19:56.000000',
'questionable_id' => 514,
'questionable_type' => 'App\\Project',
),
326 => 
array (
'id_old' => 2999329,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 20, 2017
Time: 5:25pm-5:30pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-07 17:20:26.000000',
'created_at' => '2017-02-02 17:20:26.000000',
'questionable_id' => 534,
'questionable_type' => 'App\\Project',
),
327 => 
array (
'id_old' => 2999330,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 20, 2017
Time: 5:35pm-5:40pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-07 17:21:02.000000',
'created_at' => '2017-02-02 17:21:02.000000',
'questionable_id' => 511,
'questionable_type' => 'App\\Project',
),
328 => 
array (
'id_old' => 2999331,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 20, 2017
Time: 5:40pm-5:45pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-07 17:21:29.000000',
'created_at' => '2017-02-02 17:21:29.000000',
'questionable_id' => 515,
'questionable_type' => 'App\\Project',
),
329 => 
array (
'id_old' => 2999332,
'question_text' => 'Dear Applicant,
In the past, the Green Fund Committee has found it very beneficial to personally meet with proposal authors. This meeting allows the committee to ask clarifying questions as well as provides project managers a few moments to address the committee. The Green Fund Committee would like to meet with you to discuss your project. Unfortunately, due to time limitations they will only have 5 minutes to speak to you regarding your project and will have to maintain a strict time schedule.  If you are able to attend the meeting the committee will ask you to introduce yourself, they will ask you specific questions regarding your proposal, and if time permits, will ask you to speak to the merits of your proposal. Please bear in mind the committee has an intimate knowledge of your proposed work; consider telling them something they did not read in your proposal.
This meeting is not mandatory for funding consideration and this meeting cannot be moved from its scheduled time slot. Thank you.

Your meeting has been scheduled for: 
Date: Monday February 20, 2017
Time: 5:50pm-5:55pm
Location: DOS- Nugent Building- main conference room.
Please use the following link to let the committee know if you will be attending the meeting.',
'netid' => 'martinez1',
'deadline' => '2017-02-07 17:22:00.000000',
'created_at' => '2017-02-02 17:22:00.000000',
'questionable_id' => 512,
'questionable_type' => 'App\\Project',
),
330 => 
array (
'id_old' => 2999333,
'question_text' => 'Regarding AASHE, Who would be going and what exactly would they be doing? Or was this just a mistake to include it in the budget?',
'netid' => 'martinez1',
'deadline' => '2017-02-14 17:20:48.000000',
'created_at' => '2017-02-09 17:20:48.000000',
'questionable_id' => 528,
'questionable_type' => 'App\\Project',
),
331 => 
array (
'id_old' => 2999334,
'question_text' => 'What is the incentive for Greek Houses to be a part?',
'netid' => 'martinez1',
'deadline' => '2017-02-14 17:21:23.000000',
'created_at' => '2017-02-09 17:21:23.000000',
'questionable_id' => 529,
'questionable_type' => 'App\\Project',
),
332 => 
array (
'id_old' => 2999335,
'question_text' => 'How many are you hoping to fund (1 or 2)?',
'netid' => 'martinez1',
'deadline' => '2017-02-14 17:21:51.000000',
'created_at' => '2017-02-09 17:21:51.000000',
'questionable_id' => 522,
'questionable_type' => 'App\\Project',
),
333 => 
array (
'id_old' => 2999336,
'question_text' => 'Could you send us more details on classes attending? Numbers?',
'netid' => 'martinez1',
'deadline' => '2017-02-14 17:22:14.000000',
'created_at' => '2017-02-09 17:22:14.000000',
'questionable_id' => 526,
'questionable_type' => 'App\\Project',
),
334 => 
array (
'id_old' => 2999337,
'question_text' => 'Could you be given a certificate or diploma for completing the program or for getting a grant funded?
How will all TUSD students know about applying?',
'netid' => 'martinez1',
'deadline' => '2017-02-14 17:22:49.000000',
'created_at' => '2017-02-09 17:22:49.000000',
'questionable_id' => 527,
'questionable_type' => 'App\\Project',
),
335 => 
array (
'id_old' => 2999338,
'question_text' => 'Dear PM,
The Green Fund Committee had five more questions regarding your recent proposal. These questions will all be sent separately. The site will allow responses for the next five days. After this time, the site will not allow you to submit answers. Thank you again for participating in this process.

Question 1. It is our understanding Facilities Management does not release energy usage data. How will you measure energy usage data if FM does not release information?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:07:32.000000',
'created_at' => '2017-02-16 18:07:32.000000',
'questionable_id' => 535,
'questionable_type' => 'App\\Project',
),
336 => 
array (
'id_old' => 2999339,
'question_text' => 'Question 2. For on-going and up-coming events, what kind of information will the site provide? Will contact information and information on how to get involved be included?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:08:20.000000',
'created_at' => '2017-02-16 18:08:20.000000',
'questionable_id' => 535,
'questionable_type' => 'App\\Project',
),
337 => 
array (
'id_old' => 2999340,
'question_text' => 'Question 3. Who are your sustainability contacts?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:10:03.000000',
'created_at' => '2017-02-16 18:10:03.000000',
'questionable_id' => 535,
'questionable_type' => 'App\\Project',
),
338 => 
array (
'id_old' => 2999341,
'question_text' => 'Question 4. Will you be including research done on campus? Is the site only including what will directly affect campus or will it impact the community more broadly?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:11:10.000000',
'created_at' => '2017-02-16 18:11:10.000000',
'questionable_id' => 535,
'questionable_type' => 'App\\Project',
),
339 => 
array (
'id_old' => 2999342,
'question_text' => 'Question 5. Are you adding more information to each icon and updating the site to make it more user friendly?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:12:35.000000',
'created_at' => '2017-02-16 18:12:35.000000',
'questionable_id' => 535,
'questionable_type' => 'App\\Project',
),
340 => 
array (
'id_old' => 2999343,
'question_text' => 'Dear PM,
The Green Fund Committee had one question regarding your recent proposal. The site will allow responses for the next five days. After this time, the site will not allow you to submit answers. Thank you again for participating in this process.

Do you anticipate a time in which CALS might provide funding for this project in the future and have you requested other funding from other sources?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:15:33.000000',
'created_at' => '2017-02-16 18:15:33.000000',
'questionable_id' => 519,
'questionable_type' => 'App\\Project',
),
341 => 
array (
'id_old' => 2999344,
'question_text' => 'Dear PM,
The Green Fund Committee had five more questions regarding your recent proposal. These questions will all be sent separately. The site will allow responses for the next five days. After this time, the site will not allow you to submit answers. Thank you again for participating in this process.
Question 1. How are you going to have access to teach elementary students and who are you working with to reach teachers to get classroom teaching time?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:18:06.000000',
'created_at' => '2017-02-16 18:18:06.000000',
'questionable_id' => 513,
'questionable_type' => 'App\\Project',
),
342 => 
array (
'id_old' => 2999345,
'question_text' => 'Question 2. How did the project manage without funding and how has the project changed as a result of not being funded this AY?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:19:16.000000',
'created_at' => '2017-02-16 18:19:16.000000',
'questionable_id' => 513,
'questionable_type' => 'App\\Project',
),
343 => 
array (
'id_old' => 2999346,
'question_text' => 'Question 3. Can you give a list of the materials you will purchase with $4,000?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:19:57.000000',
'created_at' => '2017-02-16 18:19:57.000000',
'questionable_id' => 513,
'questionable_type' => 'App\\Project',
),
344 => 
array (
'id_old' => 2999347,
'question_text' => 'Question 4. How is the curriculum for elementary students being developed?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:20:20.000000',
'created_at' => '2017-02-16 18:20:20.000000',
'questionable_id' => 513,
'questionable_type' => 'App\\Project',
),
345 => 
array (
'id_old' => 2999348,
'question_text' => 'Question 5. The committee has some concern about the waste created during the process of growing mushrooms. What do you do with the substrate(sterile spores) bags after the mushroom are harvested?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:21:59.000000',
'created_at' => '2017-02-16 18:21:59.000000',
'questionable_id' => 513,
'questionable_type' => 'App\\Project',
),
346 => 
array (
'id_old' => 2999349,
'question_text' => 'Dear PM,
The Green Fund Committee had four questions regarding your recent proposal. These questions will all be sent separately. The site will allow responses for the next five days. After this time, the site will not allow you to submit answers. Thank you again for participating in this process.

Question 1. Would this employee work with athletics only or would they work with other campus events and departments such as the Unions or Spring Fling?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:24:26.000000',
'created_at' => '2017-02-16 18:24:26.000000',
'questionable_id' => 516,
'questionable_type' => 'App\\Project',
),
347 => 
array (
'id_old' => 2999350,
'question_text' => 'Question 2. How would this employee ensure that FM doesn’t take over the narrative and would you be able to make certain they would provide continued student engagement opportunities?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:26:48.000000',
'created_at' => '2017-02-16 18:26:48.000000',
'questionable_id' => 516,
'questionable_type' => 'App\\Project',
),
348 => 
array (
'id_old' => 2999351,
'question_text' => 'Question 3. In reference to your proposal, what does the agreement FM made to “take responsibility for funding” mean?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:27:40.000000',
'created_at' => '2017-02-16 18:27:40.000000',
'questionable_id' => 516,
'questionable_type' => 'App\\Project',
),
349 => 
array (
'id_old' => 2999352,
'question_text' => 'Question 4. Has FM committed and budgeted to fund this position in FY2019 and if so, why don\'t they fund this position sooner?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:28:52.000000',
'created_at' => '2017-02-16 18:28:52.000000',
'questionable_id' => 516,
'questionable_type' => 'App\\Project',
),
350 => 
array (
'id_old' => 2999353,
'question_text' => 'Dear PM,
The Green Fund Committee had three questions regarding your recent proposal. These questions will all be sent separately. The site will allow responses for the next five days. After this time, the site will not allow you to submit answers. Thank you again for participating in this process.

Question 1. Elaborate further on what program expenses ($10,000) would be spent on and be used for?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:36:36.000000',
'created_at' => '2017-02-16 18:36:36.000000',
'questionable_id' => 518,
'questionable_type' => 'App\\Project',
),
351 => 
array (
'id_old' => 2999354,
'question_text' => 'Question 2. We are aware that the new energy manager was just hired (6 months later than expected) and so the project should have unspent funding. Can you submit a PAR to use the funding as described in the proposal and how do you know additional funding will be needed?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:40:00.000000',
'created_at' => '2017-02-16 18:40:00.000000',
'questionable_id' => 518,
'questionable_type' => 'App\\Project',
),
352 => 
array (
'id_old' => 2999355,
'question_text' => 'Question 3. Have there been efforts to collaborate with sustainable work being done on campus  with student groups such as the energy climate committee and green purchasing team?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:41:02.000000',
'created_at' => '2017-02-16 18:41:02.000000',
'questionable_id' => 518,
'questionable_type' => 'App\\Project',
),
353 => 
array (
'id_old' => 2999356,
'question_text' => 'Dear PM,
The Green Fund Committee had two more questions regarding your recent proposal. These questions will be sent separately. The site will allow responses for the next five days. After this time, the site will not allow you to submit answers. Thank you again for participating in this process.

Question 1. Why is grad tuition in the budget show reimbursement for only 1 semester for 1 GA?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:42:57.000000',
'created_at' => '2017-02-16 18:42:57.000000',
'questionable_id' => 517,
'questionable_type' => 'App\\Project',
),
354 => 
array (
'id_old' => 2999357,
'question_text' => 'Question 2. There have been several significant setbacks during the last year, why is an additional year of funding necessary?',
'netid' => 'rudnick1',
'deadline' => '2017-02-21 18:43:54.000000',
'created_at' => '2017-02-16 18:43:54.000000',
'questionable_id' => 517,
'questionable_type' => 'App\\Project',
),
355 => 
array (
'id_old' => 2999358,
'question_text' => 'Dear Project Manager,
The Green Fund Committee is in receipt of your recent PAR which indicates you would like an extension. The committee needs a specific project end date. The two most common end dates that correlate with important Green Fund reporting are 12.31.17 or 06.30.18. Which end date would you like to extend your project to?
Thank you.',
'netid' => 'rudnick1',
'deadline' => '2017-04-22 22:04:33.000000',
'created_at' => '2017-04-17 22:04:33.000000',
'questionable_id' => 165,
'questionable_type' => 'App\\Par',
),
356 => 
array (
'id_old' => 2999359,
'question_text' => 'Dear Project Manager,
Your PAR seems to request moving all funds from student employment/ERE to operations. Funds in operations can be used to provide student stipends. It is my understanding you will be receiving stipends instead of a payroll paycheck. Is this what this PAR is requesting?',
'netid' => 'rudnick1',
'deadline' => '2017-10-25 15:44:49.000000',
'created_at' => '2017-10-20 15:44:49.000000',
'questionable_id' => 168,
'questionable_type' => 'App\\Par',
),
357 => 
array (
'id_old' => 2999360,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has an additional follow-up question:  

Would it be possible and/or preferable to support the Students for Sustainability program with an Advisor – for example, through ASUA – instead of funding a Graduate Assistant?  Why or why not?

Please provide your written response by 5:30pm on Wednesday, 2/7/2018.  Thank you!',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 00:30:29.000000',
'created_at' => '2018-02-06 00:30:29.000000',
'questionable_id' => 539,
'questionable_type' => 'App\\Project',
),
358 => 
array (
'id_old' => 2999361,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has several additional follow-up questions.  These will be sent individually, with the first question below:  

Could this program be successful if it were partially funded?  How would that impact the implementation of this project?  For example, if you received funding to install the technology in five residence halls instead of ten, what would be the benefits and drawbacks of that reduced project scale?

Please provide your written responses by 5:45pm on Wednesday, 2/7/2018.  Thank you!',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 00:34:44.000000',
'created_at' => '2018-02-06 00:34:44.000000',
'questionable_id' => 541,
'questionable_type' => 'App\\Project',
),
359 => 
array (
'id_old' => 2999362,
'question_text' => 'Have you looked into other schools using this technology, and inquired about their experiences?  If so, could you please provide us with other school’s feedback?',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 00:35:47.000000',
'created_at' => '2018-02-06 00:35:47.000000',
'questionable_id' => 541,
'questionable_type' => 'App\\Project',
),
360 => 
array (
'id_old' => 2999363,
'question_text' => 'Could you provide a bit more detail about how you plan to execute your outreach campaign/marketing strategy?',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 00:36:36.000000',
'created_at' => '2018-02-06 00:36:36.000000',
'questionable_id' => 541,
'questionable_type' => 'App\\Project',
),
361 => 
array (
'id_old' => 2999364,
'question_text' => 'Could you provide more detail in your budget spreadsheet, or a more thorough explanation, of the specific costs of the technology and marketing items you listed?',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 00:41:59.000000',
'created_at' => '2018-02-06 00:41:59.000000',
'questionable_id' => 541,
'questionable_type' => 'App\\Project',
),
362 => 
array (
'id_old' => 2999365,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has two additional follow-up questions.  These will be sent to you individually, with the first question below:  

On the last page of your application, you mentioned that you are trying to “expand your organizational connections and volunteer experiences” into partnerships that will result in an additional 5-6 annual student events.  What kind of events?

Please provide your written responses by 5:45pm on Wednesday, 2/7/2018.  Thank you!',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 00:45:19.000000',
'created_at' => '2018-02-06 00:45:19.000000',
'questionable_id' => 541,
'questionable_type' => 'App\\Project',
),
363 => 
array (
'id_old' => 2999366,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has two additional follow-up questions. These will be sent to you individually, with the first question below: 

On the last page of your application, you mentioned that you are trying to “expand your organizational connections and volunteer experiences” into partnerships that will result in an additional 5-6 annual student events. What kind of events?

Please provide your written responses by 6:00pm on Wednesday, 2/7/2018. Thank you!',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 01:01:07.000000',
'created_at' => '2018-02-06 01:01:07.000000',
'questionable_id' => 543,
'questionable_type' => 'App\\Project',
),
364 => 
array (
'id_old' => 2999367,
'question_text' => 'How do you plan on continuing outreach activities and events, and how can these be self-sustaining in the future?',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 01:01:45.000000',
'created_at' => '2018-02-06 01:01:45.000000',
'questionable_id' => 543,
'questionable_type' => 'App\\Project',
),
365 => 
array (
'id_old' => 2999368,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has three additional follow-up questions. These will be sent to you individually, with the first question below: 

Could you clarify the distinction between the Green Team, Greening the Game, and Students for Sustainability?  In what areas does your work overlap, and in what areas is it unique?

Please provide your written responses by 6:15pm on Wednesday, 2/7/2018. Thank you!',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 01:05:37.000000',
'created_at' => '2018-02-06 01:05:37.000000',
'questionable_id' => 543,
'questionable_type' => 'App\\Project',
),
366 => 
array (
'id_old' => 2999369,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has several additional follow-up questions. These will be sent to you individually, with the first question below: 

Could you clarify the distinction between the Green Team, Greening the Game, and Students for Sustainability? In what areas does your work overlap, and in what areas is it unique?

Please provide your written responses by 6:30pm on Wednesday, 2/7/2018. Thank you!',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 01:15:50.000000',
'created_at' => '2018-02-06 01:15:50.000000',
'questionable_id' => 544,
'questionable_type' => 'App\\Project',
),
367 => 
array (
'id_old' => 2999370,
'question_text' => 'What is the benefit of attendance at the AASHE conference?  Please describe what you learned from AASHE conferences in the past, and what skills or knowledge you were able to bring back to the UA community.  What would be the impact of having only 1-2 Green Team members attend AASHE each year?  If fully funded, how many members are you intending to send this year?',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 01:17:17.000000',
'created_at' => '2018-02-06 01:17:17.000000',
'questionable_id' => 544,
'questionable_type' => 'App\\Project',
),
368 => 
array (
'id_old' => 2999371,
'question_text' => 'Could you provide a bit more detail about your budget, regarding funding for general supplies ($3000) vs. zero waste purchasing (also $3000)?  What are some examples of your general supply and zero waste purchasing needs?',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 01:18:21.000000',
'created_at' => '2018-02-06 01:18:21.000000',
'questionable_id' => 544,
'questionable_type' => 'App\\Project',
),
369 => 
array (
'id_old' => 2999372,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has three additional follow-up questions. These will be sent to you individually, with the first question below: 

The bulk of each year’s grant goes toward personnel pay.  Are these personnel positions part-time, and do the positions already exist or are these new positions?

Please provide your written responses by 6:45pm on Wednesday, 2/7/2018. Thank you!',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 01:21:22.000000',
'created_at' => '2018-02-06 01:21:22.000000',
'questionable_id' => 545,
'questionable_type' => 'App\\Project',
),
370 => 
array (
'id_old' => 2999373,
'question_text' => 'What are the specific criteria you use to select student interns?',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 01:21:51.000000',
'created_at' => '2018-02-06 01:21:51.000000',
'questionable_id' => 545,
'questionable_type' => 'App\\Project',
),
371 => 
array (
'id_old' => 2999374,
'question_text' => 'Could this program be successful if it were partially funded? How would partial funding impact the implementation of this project?',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-11 01:22:37.000000',
'created_at' => '2018-02-06 01:22:37.000000',
'questionable_id' => 545,
'questionable_type' => 'App\\Project',
),
372 => 
array (
'id_old' => 2999375,
'question_text' => 'The Green Fund Committee would like to thank you for your attendance at last Friday\'s annual grants meeting.  We offered most groups the opportunity to give a 2-minute presentation at the beginning of their 5 minute allocated time; however, your group was not given this opportunity.  The Committee would like to invite you to return for this optional presentation during the Green Fund Committee meeting this Friday, February 9.  You will be receiving an email invitation to present for 2 minutes at 3:05pm.  

Please rsvp by 5:30pm this Thursday, February 8 to indicate whether you will be attending.  Thank you!',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-12 00:17:49.000000',
'created_at' => '2018-02-07 00:17:49.000000',
'questionable_id' => 538,
'questionable_type' => 'App\\Project',
),
373 => 
array (
'id_old' => 2999376,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has several additional follow-up questions.  These will be sent to you individually, with the first question below:

Have members of your project team attended AASHE conferences in the past?  If so, please describe what you learned from these conferences, and what skills or knowledge you were able to bring back to the UA community as a result.

Please provide your written responses by 11:30 am on Saturday, February 10th, 2018.  Thank you!',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-13 18:15:02.000000',
'created_at' => '2018-02-08 18:15:02.000000',
'questionable_id' => 542,
'questionable_type' => 'App\\Project',
),
374 => 
array (
'id_old' => 2999377,
'question_text' => 'How many student attendees would this year’s AASHE conference travel funding be used to support?',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-13 18:15:49.000000',
'created_at' => '2018-02-08 18:15:49.000000',
'questionable_id' => 542,
'questionable_type' => 'App\\Project',
),
375 => 
array (
'id_old' => 2999378,
'question_text' => 'What housing complexes are you working with right now, and what kind of projects are you currently working on with them?',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-13 18:16:16.000000',
'created_at' => '2018-02-08 18:16:16.000000',
'questionable_id' => 542,
'questionable_type' => 'App\\Project',
),
376 => 
array (
'id_old' => 2999379,
'question_text' => 'Are you confident with your current partnerships?  What kinds of ideas have been generated with your collaborators?',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-13 18:16:53.000000',
'created_at' => '2018-02-08 18:16:53.000000',
'questionable_id' => 542,
'questionable_type' => 'App\\Project',
),
377 => 
array (
'id_old' => 2999380,
'question_text' => 'Since this program has been around since 2013, could you provide a month-by-month breakdown or similar summary of your activities, describing how you’ve reached 7500 students?  The Committee would like more information about your outreach and tracking methods.',
'netid' => 'laurenmwhite',
'deadline' => '2018-02-13 18:17:26.000000',
'created_at' => '2018-02-08 18:17:26.000000',
'questionable_id' => 542,
'questionable_type' => 'App\\Project',
),
378 => 
array (
'id_old' => 2999381,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has one additional follow-up question below.  

Could you please provide an edited/updated budget that reflects the changes in fencing costs, security cameras, etc.?

Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 16:23:24.000000',
'created_at' => '2018-02-15 16:23:24.000000',
'questionable_id' => 546,
'questionable_type' => 'App\\Project',
),
379 => 
array (
'id_old' => 2999382,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has one additional follow-up question below.  

If this project were to be partially funded, how would that impact your work?  Which parts of the budget would receive priority?

Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 16:27:52.000000',
'created_at' => '2018-02-15 16:27:52.000000',
'questionable_id' => 547,
'questionable_type' => 'App\\Project',
),
380 => 
array (
'id_old' => 2999383,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has four additional follow-up question(s) below.  

How many students in total would receive funding for their work on this project?

Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 16:37:49.000000',
'created_at' => '2018-02-15 16:37:49.000000',
'questionable_id' => 548,
'questionable_type' => 'App\\Project',
),
381 => 
array (
'id_old' => 2999384,
'question_text' => 'Could you provide a bit more detail about the timeline for this project?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 16:39:40.000000',
'created_at' => '2018-02-15 16:39:40.000000',
'questionable_id' => 548,
'questionable_type' => 'App\\Project',
),
382 => 
array (
'id_old' => 2999385,
'question_text' => 'If the Committee were to offer partial funding for this project, what would be your highest priorities for those funds?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 16:40:46.000000',
'created_at' => '2018-02-15 16:40:46.000000',
'questionable_id' => 548,
'questionable_type' => 'App\\Project',
),
383 => 
array (
'id_old' => 2999386,
'question_text' => 'What’s the breadth of impact for this second grant proposal, as opposed to the first (Beerworms)?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 16:46:16.000000',
'created_at' => '2018-02-15 16:46:16.000000',
'questionable_id' => 548,
'questionable_type' => 'App\\Project',
),
384 => 
array (
'id_old' => 2999387,
'question_text' => 'This question would be for both grants:

If you could receive funding for only one of the two proposed projects (Beerworms or Beetle Grubs), which one would you choose, and why?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 16:47:45.000000',
'created_at' => '2018-02-15 16:47:45.000000',
'questionable_id' => 547,
'questionable_type' => 'App\\Project',
),
385 => 
array (
'id_old' => 2999388,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has 4 additional follow-up question(s) below.  

Who would be tasked with management and stewardship of this project during the summer and winter breaks?

Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 16:54:09.000000',
'created_at' => '2018-02-15 16:54:09.000000',
'questionable_id' => 549,
'questionable_type' => 'App\\Project',
),
386 => 
array (
'id_old' => 2999389,
'question_text' => 'What are the implications of the turnover in student leadership each year, and how would you manage this effectively?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 16:54:53.000000',
'created_at' => '2018-02-15 16:54:53.000000',
'questionable_id' => 549,
'questionable_type' => 'App\\Project',
),
387 => 
array (
'id_old' => 2999390,
'question_text' => 'Could you please provide a bit more information about how you calculated the amounts of food you could realistically produce?  The Committee would like to understand how you derived these estimates.',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 16:55:51.000000',
'created_at' => '2018-02-15 16:55:51.000000',
'questionable_id' => 549,
'questionable_type' => 'App\\Project',
),
388 => 
array (
'id_old' => 2999391,
'question_text' => 'How would your team deal with unexpected setbacks – for example, outbreaks of pathogens or pests that damage the crops and negatively impact the yields?  What would be the financial implications of such setbacks?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 16:59:42.000000',
'created_at' => '2018-02-15 16:59:42.000000',
'questionable_id' => 549,
'questionable_type' => 'App\\Project',
),
389 => 
array (
'id_old' => 2999392,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has three additional follow-up question(s) below.

What are your thoughts on creating an externship symposium, panel or similar outreach event to bring the participants together at the UA Main Campus to share their experiences, and engage prospective new students?  Has such an undertaking been tried before, and if so, what was the outcome?

Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 17:01:22.000000',
'created_at' => '2018-02-15 17:01:22.000000',
'questionable_id' => 550,
'questionable_type' => 'App\\Project',
),
390 => 
array (
'id_old' => 2999393,
'question_text' => 'Could you please expand on the status of this summer’s externships?  Where will these be located, and by what partners and/or organization(s) will they be supported?  In other words, can you provide a list of connections already solidified for this summer’s projects?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 17:02:20.000000',
'created_at' => '2018-02-15 17:02:20.000000',
'questionable_id' => 550,
'questionable_type' => 'App\\Project',
),
391 => 
array (
'id_old' => 2999394,
'question_text' => 'Could you provide a bit more detail about the marketing process for this program, and could the Committee please see a copy of the application you circulate to prospective participants?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 17:02:51.000000',
'created_at' => '2018-02-15 17:02:51.000000',
'questionable_id' => 550,
'questionable_type' => 'App\\Project',
),
392 => 
array (
'id_old' => 2999395,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has four additional follow-up question(s) below. 

In your presentation, you mentioned a likely recruitment of between 15 – 50 students.  How will you recruit these students, and can you be a bit more specific in narrowing down how many students you can realistically accommodate?  Will undergraduate and graduate students both be involved? Which will be targeted?

Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 17:06:50.000000',
'created_at' => '2018-02-15 17:06:50.000000',
'questionable_id' => 551,
'questionable_type' => 'App\\Project',
),
393 => 
array (
'id_old' => 2999396,
'question_text' => 'Who is/are the Faculty members getting paid with the ‘Appointed and Faculty Regular’ funds line item in your budget?  What work is being done by the Faculty members, and can it be done by a student?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 17:07:51.000000',
'created_at' => '2018-02-15 17:07:51.000000',
'questionable_id' => 551,
'questionable_type' => 'App\\Project',
),
394 => 
array (
'id_old' => 2999397,
'question_text' => 'If the Committee were to offer partial funding for this project, what would be the potential impacts?  What would be your highest priorities for funding?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 17:08:31.000000',
'created_at' => '2018-02-15 17:08:31.000000',
'questionable_id' => 551,
'questionable_type' => 'App\\Project',
),
395 => 
array (
'id_old' => 2999398,
'question_text' => 'Would this be possible to undertake as a one-year project?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-18 17:09:01.000000',
'created_at' => '2018-02-15 17:09:01.000000',
'questionable_id' => 551,
'questionable_type' => 'App\\Project',
),
396 => 
array (
'id_old' => 2999399,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has five additional follow-up questions below. 

Could you please provide a bit more information about the roles of the project partners (PD&C, the ENR2 Building Manager, and Facilities Management)?  Does Facilities Management have the necessary expertise/qualifications to manage and support this kind of project?

Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 16:00:47.000000',
'created_at' => '2018-02-20 16:00:47.000000',
'questionable_id' => 552,
'questionable_type' => 'App\\Project',
),
397 => 
array (
'id_old' => 2999400,
'question_text' => 'Who will hold FM accountable and maintain the lines of communication between research, maintenance and the project partners?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 16:01:38.000000',
'created_at' => '2018-02-20 16:01:38.000000',
'questionable_id' => 552,
'questionable_type' => 'App\\Project',
),
398 => 
array (
'id_old' => 2999401,
'question_text' => 'How will you advertise this project to students and ensure its visibility?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 16:02:13.000000',
'created_at' => '2018-02-20 16:02:13.000000',
'questionable_id' => 552,
'questionable_type' => 'App\\Project',
),
399 => 
array (
'id_old' => 2999402,
'question_text' => 'If this project were to be partially funded, how would that impact your work?  Which parts of the budget would receive priority?  For example, there is a $1,000 line item for a ribbon cutting ceremony.  Please submit a modified budget that prioritizes your projected expenditures.',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 16:02:56.000000',
'created_at' => '2018-02-20 16:02:56.000000',
'questionable_id' => 552,
'questionable_type' => 'App\\Project',
),
400 => 
array (
'id_old' => 2999403,
'question_text' => 'Did you obtain price estimates from any other companies besides TFS?  If not, why not, and would it be possible to do so?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 16:03:33.000000',
'created_at' => '2018-02-20 16:03:33.000000',
'questionable_id' => 552,
'questionable_type' => 'App\\Project',
),
401 => 
array (
'id_old' => 2999404,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has three additional follow-up questions below. 

Would it be possible to fund the Graduate Assistant position through the College of Engineering, Biosphere 2 research endowment, and/or other non-Green Fund source(s)?



Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 16:08:20.000000',
'created_at' => '2018-02-20 16:08:20.000000',
'questionable_id' => 553,
'questionable_type' => 'App\\Project',
),
402 => 
array (
'id_old' => 2999405,
'question_text' => 'How will this project engage the broader student body and campus community?  What is the reach, and do you plan on including an education/outreach component as part of the graduate assistant-ship?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 16:09:03.000000',
'created_at' => '2018-02-20 16:09:03.000000',
'questionable_id' => 553,
'questionable_type' => 'App\\Project',
),
403 => 
array (
'id_old' => 2999406,
'question_text' => 'If you meet or exceed your break-even ratio for energy generation, what will you do with extra energy?  Will it be stored?  Will it go toward facilities?  Please expand on this.',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 16:09:44.000000',
'created_at' => '2018-02-20 16:09:44.000000',
'questionable_id' => 553,
'questionable_type' => 'App\\Project',
),
404 => 
array (
'id_old' => 2999407,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has five additional follow-up questions below.  

Could you provide more detail about the check-out system for helmets, lights and other supplies?  How will these supplies be secured?

Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 20:56:24.000000',
'created_at' => '2018-02-20 20:56:24.000000',
'questionable_id' => 554,
'questionable_type' => 'App\\Project',
),
405 => 
array (
'id_old' => 2999408,
'question_text' => 'The Committee would like to see data on “empty” bike times.  What is the current demand for bikes, when are the empty times, and during those empty times, are bikes available at other nearby locations?  In terms of overall usage, what is the breakdown of student vs. staff usage, and unique vs. repeated usage?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 20:57:30.000000',
'created_at' => '2018-02-20 20:57:30.000000',
'questionable_id' => 554,
'questionable_type' => 'App\\Project',
),
406 => 
array (
'id_old' => 2999409,
'question_text' => 'If this project were to be partially funded, how would that impact your work?  Which parts of your budget would receive priority?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 20:59:42.000000',
'created_at' => '2018-02-20 20:59:42.000000',
'questionable_id' => 554,
'questionable_type' => 'App\\Project',
),
407 => 
array (
'id_old' => 2999410,
'question_text' => 'How will you ensure that the safety and integrity of the helmets is not compromised?  For example, a user might crash while wearing the loaner helmet, or the helmet could begin to break down from being stored in the sunshine, etc.  The Committee would like more information about how you plan to protect the helmets.',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 21:05:17.000000',
'created_at' => '2018-02-20 21:05:17.000000',
'questionable_id' => 554,
'questionable_type' => 'App\\Project',
),
408 => 
array (
'id_old' => 2999411,
'question_text' => 'In the event that it is necessary to purchase new helmets regularly – for example, each year – what funding source would you use for those purchases?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 21:06:19.000000',
'created_at' => '2018-02-20 21:06:19.000000',
'questionable_id' => 554,
'questionable_type' => 'App\\Project',
),
409 => 
array (
'id_old' => 2999412,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has one additional follow-up question below.  

What would be the ramifications to Compost Cats if this project were not selected for funding? 

Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 21:13:36.000000',
'created_at' => '2018-02-20 21:13:36.000000',
'questionable_id' => 555,
'questionable_type' => 'App\\Project',
),
410 => 
array (
'id_old' => 2999413,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has three additional follow-up questions below.  

Have you applied for and/or obtained additional funding from other sources?  Have you considered Tech Launch Arizona?

Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 21:15:12.000000',
'created_at' => '2018-02-20 21:15:12.000000',
'questionable_id' => 557,
'questionable_type' => 'App\\Project',
),
411 => 
array (
'id_old' => 2999414,
'question_text' => 'How will this project engage the UA and Tucson communities?  Can you expand on how this project would benefit students?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 21:15:44.000000',
'created_at' => '2018-02-20 21:15:44.000000',
'questionable_id' => 557,
'questionable_type' => 'App\\Project',
),
412 => 
array (
'id_old' => 2999415,
'question_text' => 'Could you please submit a basic, estimated timeline for this project?',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 21:16:16.000000',
'created_at' => '2018-02-20 21:16:16.000000',
'questionable_id' => 557,
'questionable_type' => 'App\\Project',
),
413 => 
array (
'id_old' => 2999416,
'question_text' => 'The Green Fund Committee would like to thank you for your presentation during Friday\'s annual grants meeting, and has one additional follow-up question below.  

How will you market this project to UA students to ensure their involvement and engagement?

Please provide your written response, via the email link you received, before the expiration date and time listed.  Thank you!',
'netid' => 'lishahall2012',
'deadline' => '2018-02-23 21:17:18.000000',
'created_at' => '2018-02-20 21:17:18.000000',
'questionable_id' => 558,
'questionable_type' => 'App\\Project',
),
414 => 
array (
'id_old' => 2999417,
'question_text' => 'The Green Fund Committee has a few follow-up questions in regard to your PAR for GF 18.33 Community Outreach in Environmental Learning:  

$15,000 would be an unusually large travel request, and is a higher amount than what the Committee would typically approve for travel in any annual grant proposal.  It would require unusual or exceptional circumstances in order to justify moving this large of a sum from unused student wages into the travel category; we notice that travel was not in the original budget.  Would it be possible to have the unused funding support a more limited amount of people attending the AASHE conference (for example, 1-2 people instead of 4), to reduce travel costs?  

Since any funds that are not used before the end of this fiscal year could potentially be rolled over to support your student wages and programming during the following year, the Committee would generally be more in favor of using the funds for those purposes, rather than for travel – unless there is a significant reason why the AASHE conference should receive priority.

Additionally, could your team please submit an updated, itemized budget so that the Committee can review and understand how your estimated travel costs were calculated?

Thank you very much!  The Committee looks forward to receiving your responses.  If you have any questions, please feel free to reach out to us.',
'netid' => 'laurenmwhite',
'deadline' => '2018-04-06 21:57:31.000000',
'created_at' => '2018-04-03 21:57:31.000000',
'questionable_id' => 172,
'questionable_type' => 'App\\Par',
),
));
        
        
    }
}