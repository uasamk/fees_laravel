<?php

use Illuminate\Database\Seeder;

class GfAnswersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        
        \DB::table('answers')->insert(array (
            0 => 
            array (
                'answer_text' => 'Dear UA GreenFund Committee,

First of all, I would like to thank to the committee for the interest shown towards our project proposal and also for the question. 

The excess energy from the small solar array for the greenhouse project and CEAC cannot easily be transmitted to main UA campus. The proposed project site for installation of the 30 kW Photovoltaic system will be at the Controlled Environment Agriculture Center (CEAC) located about 4 miles north of main UA campus. Thus, we can and will be directing excess energy generated towards offsetting the energy used at the CEAC facilities. It is worth to mention that CEAC offices, lecture halls, labs and associated research greenhouses (other than two high tunnel greenhouses proposed) use approximately $15,000 worth of electricity per year. Faculty, students, staff, extension faculty and visitors utilize these facilities to conduct research and teach information that provides fundamental knowledge which allows for greater food production using less resource inputs. Greater food production per unit area greatly reduces greenhouse gasses, while vastly increasing productivity. Therefore, we would like to use the excess energy from PV system to offset other electricity demand at the CEAC.   

One of the proposed high tunnel greenhouses will be off-grid connection requiring about 2.8 kW power from the installed PV system during the research periods only. Based on the yearly average solar insolation value for Tucson, we expect that the output of the installed fixed tilt (south facing) PV system will be 66,300 kWh annually. Thus, assuming the cost of energy to be $0.10/kWh the installed PV system will annually produce $6,300 worth of electricity (calculations are based on NREL PVWatts calculator). If the cost of energy is $0.11/kWh, then we can expect $7,956 savings for CEAC electricity bills. Thus, any excess power generated will be used internally within the CEAC electrical grid. No net power is expected to be returned to the TEP grid.

Sincerely,
Murat Kacira',
                'created_at' => '2011-03-07 00:31:55.000000',
                'question_id' => 426,
            ),
            1 => 
            array (
                'answer_text' => 'Dear UA Green Fund Committee,

First of all, I would like to thank to the committee for the interest shown towards our project proposal and also for the question. 

Based on the request, the following are the detailed budget for specific line items and parts as requested: 

1. PV SUBSYSTEM INSTALLATION, ELECTRICAL DESIGN AND EQUIPMENT ($45,000 REQUESTED): Base shade structure materials will be donated by College of Agriculture and Life Sciences, however, funds requested for Photovoltaic (PV) system racking and mounts for 30 kW system:  $15,000 (solar program estimates at $0.50/Watt). PV Wiring Design, Inverters, Isolation Switches, Fuse Box, Meter (including Supervision, Inspection) for 30 kW system: $30,000 (solar program estimates at $1.00/Watt). 

2. COMPOSTING SYSTEM MATERIALS AND INSTALLATION ($5,000 REQUESTED): The requested funds are for building supplies (concrete base floor, liner cover, and wood) to construct the structure, air blower, perforated heavy duty plastic piping for aeration and timers. The cost also includes hauling of compost material, compost analysis.

3. HYDROPONICS GREENHOUSE SUPPLIES ($7,500 REQUESTED): The funds are requested for hydroponics greenhouse supplies to be used in two high tunnel greenhouse production systems for one year period. The items for one greenhouse include: Seedlings ($300), drip emitters and laterals ($500), Dosatron nutrient injectors ($750), Fertilizers ($750), IPM and Bees ($400), Hooks, strings and wire ($500), shipping cost for supplies ($550). Thus, the total cost per greenhouse is $3,750.  

4. SENSORS AND INSTRUMENTATION FOR MONITORING CLIMATIC VARIABLES ($ 3,500 REQUESTED): The itemized list of sensors and instrumentation for real-time measurements and the budget are; Datalogger ($1,500), electrical conductivity and pH sensor for greenhouse growing media ($600), greenhouse air temperature and relative humidity sensor ($150), solar radiation sensor (greenhouse interior) ($150), thermocouples (composting media temperatures) ($100). The total is $3,500.  In addition to these sensors and instrumentation, the capabilities of Dr. Kacira’s sensor and instrumentation research lab will also be used for the project.',
                'created_at' => '2011-03-07 22:04:46.000000',
                'question_id' => 427,
            ),
            2 => 
            array (
                'answer_text' => 'Dear UA Green Fund Committee,

First of all, I would like to thank to the committee for the interest shown towards our project proposal and also for the question. 

The University of Arizona Campus Agricultural Center will provide 100 man hours ($4,000 value) as in-kind support to erect the base structure (for shaded parking lot) for the PV system. High tunnel greenhouse structural installation will be done by external labor hire through Campus Agricultural Center. However, graduate and undergraduate students will also help with the installations of the greenhouse. The requested fund for high tunnel greenhouse installation is based on 125 man hours estimate at a cost of ($5,000). The price quote from an external vendor is not available for this installation but the estimate is rather based on previous labor hiring.      

As mentioned above the installation of the base structure (for shaded parking lot) for the PV system will be done in-house, however the installation of the Photovoltaic (PV) system racking and mounts for 30 kW system and the installations of the inverters, isolation switches, fuse box, meter, PV wiring design, and inspection will be performed by an outside contractor. The requested funds are estimated costs at $0.50/Watt ($15,000 total) for PV racking and mounts above base (shade) structure, and $1.00/Watt ($30,000) for PV Wiring Design, Inverters, Isolation Switches, Fuse Box, Meter (including Supervision, Inspection).  

Sincerely,
Murat Kacira',
                'created_at' => '2011-03-07 22:22:19.000000',
                'question_id' => 428,
            ),
            3 => 
            array (
                'answer_text' => 'i-Tree data collection programs are developed to run on the Windows Mobile OS < 6.5.
Urban foresters use the Trimble Juno  or Trimble Nomad data loggers which are priced starting at $1600. Recently, Smart phones, which run on the same OS and are compatible with the software have been used successfully in place of the Trimble units. They are priced considerably less ranging from $50-$400. Many smart phones will work for our purposes. However, a possible limitation in using the smart phones will likely involve difficulty in finding a less expensive model that does not use the most recent Windows Mobile 7.0, which has not been demonstrated compatible with i-tree software. I anticipate challenges too finding a smart phone that does not require an expensive (and unnecessary data plan) but still has a screen that is large enough for easy visibility and data logging.',
                'created_at' => '2011-03-08 15:20:30.000000',
                'question_id' => 430,
            ),
            4 => 
            array (
                'answer_text' => 'Apologies for the discrepancy.  We would need $1400 plus the the additional $126 to cover the 9% tax on the two raincatchers.  

I will email a mock up of the water harvesting demonstration so that you can see what the raincatchers look like, since the Silverbell Nursery site is still down.',
                'created_at' => '2011-03-08 16:23:51.000000',
                'question_id' => 429,
            ),
            5 => 
            array (
            'answer_text' => 'Our proposed budget (29,500 with $1,500 in alumni donations) is a minimial budget.  It would be very hard to complete the project at a lower funding level.',
                'created_at' => '2012-02-08 20:40:07.000000',
                'question_id' => 453,
            ),
            6 => 
            array (
                'answer_text' => 'Dear Green Fund Committee:

As my proposal requests funding for research assistants, any money awarded---full or partial---will go directly to them.  As mentioned in my proposal, I am actively seeking additional outside funding sources for the Adagio research project, and hopefully I can make up the difference in that way.  If partial funding were to occur, at some point I would have to reduce the number of research assistants paid for by the Green Fund from two to one.  Again, hopefully I will make up the difference with outside funds, but all of those proposals are pending.',
                'created_at' => '2012-02-08 20:47:16.000000',
                'question_id' => 456,
            ),
            7 => 
            array (
                'answer_text' => 'Partial funding for this project could done in several ways.  First, and most preferable, would be to reduce the number of interns sent per year over the proposed two years from 4 to 3. This would reduce the funding needed to $14,200 dollars.  Sending fewer than 3 interns is not preferable, due to the amount of work that would be required for developing curriculum and the workshops proposed.  The project could also be done for only one year, as opposed to two, which would reduce the funding needed to $8500, though we would likely apply to continue the project again next year, as it will be important to do a second round of development after getting feedback from teachers and University students on the effectiveness of the materials developed and the workshops.',
                'created_at' => '2012-02-08 20:48:27.000000',
                'question_id' => 437,
            ),
            8 => 
            array (
            'answer_text' => 'The best way to partially fund this initiative is to simply reduce the number of educational messages we deliver.  Instead of three a semester, it could be only one or two.  If we did one over the summer, Fall semester, and Spring semester, the cost could be as little as $1525 (assuming the Public Service Ad Partnership funding is approved).  To reduce to one in the summer and two in each semester, the cost could be as low as $3150.',
                'created_at' => '2012-02-08 20:55:34.000000',
                'question_id' => 445,
            ),
            9 => 
            array (
            'answer_text' => 'With regard to our proposal, BRINGING SUSTAINABILITY TO ARIZONA’S COMMUNITIES THROUGH COOPERATIVE EXTENSION AND “EXTERNSHIPS”, partial funding would allow us to move forward and would result in providing support to a reduced number of county projects/student externs than originally proposed by our Extension colleagues.  One particular externship project (Pima County) is proposing to use three externs, but if partial funding is a possibility, then funding only one extern at that project location would be reasonable, thereby reducing our request by approximately $3,900.',
                'created_at' => '2012-02-08 21:00:47.000000',
                'question_id' => 442,
            ),
            10 => 
            array (
            'answer_text' => 'The easiest way to partially fund this initiative is to remove to promotional materials ($300).  Unfortunately, the rest of the funding is essential to making this project work.  I’ve spoken with our IT staff and they believe $22.50 an hour is a conservative number for a website developer.  We do not want to sacrifice quality because we didn’t have enough money to pay the student.  You could cut the student research wages to $10 an hour, saving $150.  This brings the total down to $5350.',
                'created_at' => '2012-02-08 21:05:03.000000',
                'question_id' => 451,
            ),
            11 => 
            array (
                'answer_text' => 'I would like to submit the final report by December 31st. Although funds have run out, I am continuing to conduct fieldwork until my research subjects become inactive, which is typically in mid-November.  This will allow me to include the entire active season in my final report.

Thank you,

Matt Goode',
                'created_at' => '2012-09-18 06:20:47.000000',
                'question_id' => 467,
            ),
            12 => 
            array (
                'answer_text' => 'If a true cost neutral fully funded option is not acceptable, I would like to see at least a 50% support figure as theis helps count toward the University Initiatives in decreasing carbon footprint.

I can also be reached on my cell if you wish!

360-3525

thanks',
                'created_at' => '2012-02-08 22:28:44.000000',
                'question_id' => 440,
            ),
            13 => 
            array (
                'answer_text' => 'If I understand the question in your statement above, you\'re asking whether we would accept partial funding of the amount requested in our application.  The answer is "yes" since we have a number of components.  We request a minimum amount of $514 to cover the cost of the compost collection service for a year.  In terms of priorities, the next most important item is the cost of a student intern at roughly $5,030.  The second phase, with a budget amount of $3000 towards the cost of creating a medicinal garden using compost, is the third priority.',
                'created_at' => '2012-02-08 22:31:26.000000',
                'question_id' => 455,
            ),
            14 => 
            array (
                'answer_text' => 'We have an alternative quote for putting a wind turbine on the UA mall.
The estimated cost would be $22,000.
The same quote would probably apply if the turbine was placed on the UA farm land on north Mountain Ave.
The UA mall has poor wind energy capture (turbine is in building shadow) but good visibility (good publicity).
The UA farm land location would require students to take the car. Wind conditions are unknown. The publicity factor is low.

The roof of the AME building has good wind, good visibility, and provides easy access for engineering students.
We are willing to compromise but prefer the AME location.',
                'created_at' => '2012-02-08 22:31:59.000000',
                'question_id' => 466,
            ),
            15 => 
            array (
            'answer_text' => 'Dear UA Green Fund Committee: We are indeed thankful to you for your interest in our project. Our project proposes to implement a rainwater harvesting system and solar panels to provide water and electricity to our greenhouses and a LED lighting system to study its effect on plant metabolite (potential pharmaceutical) production. We have requested funding for two years to fully implement these projects. Although we would prefer to implement these projects as proposed, we believe that we will be able to partially implement some of these projects. For example, we could harvest rain water from one roof instead of two roofs or install solar panels to provide electricity for only some equipment in our greenhouses. Please let me know if you require any additional clarification.',
                'created_at' => '2012-02-08 23:19:03.000000',
                'question_id' => 458,
            ),
            16 => 
            array (
                'answer_text' => 'We can implement a smaller pilot project which would require the purchase of fewer licenses based upon available funding. Our initial goal was for 6000 licenses, but we proposed purchasing 2250 licenses because of the size of potential pilot departments. The risk of a smaller pilot is that we will not reach enough colleges and campus population to create sufficient momentum and interest for further implementation of computer power management software.  I believe the benefit to the university of a larger long-term implementation is clear.',
                'created_at' => '2012-02-08 23:27:33.000000',
                'question_id' => 444,
            ),
            17 => 
            array (
                'answer_text' => 'Funding was requested for a solar electric charged electric-pedal hybrid vehicle and
solar charging station equipment at a location that would not have AC power grid connection.  The battery packs are lightweight (20 Lbs.) The solar charged feature
could be replaced with removable battery pack that is AC charged from power grid.
The solar charged feature could be removed and still fund an eco-friendly vehicle
for on campus use that does not use toxic lead acid battery technology.  Eliminating the solar part would reduce the total for equipment from $10000 to about $6000.',
                'created_at' => '2012-02-08 23:57:25.000000',
                'question_id' => 452,
            ),
            18 => 
            array (
            'answer_text' => 'Thank you for this opportunity. Three of the five tasks on our list are related to improvements to our facility or instructional program and will provide an immediate effect. The remaining two tasks would provide an opportunity for our faculty to attend two separate week-long professional development workshops in the Tucson area. The long-term effect for our program will be seen in the development and improvement of our existing course curriculum. The dates of these workshops were primarily selected because the workshops are to be conducted in Tucson, occured when spring semester classes are not in session and are within the this calendar year of the grant cycle. However, the timing (for example, Rainwater Harvesting Certification - Spring bBreak, 2012) conflicts with the time schedule for the grant committee to make a decision. Other workshop dates would be considered, if the grant project could be extended into a second year, should the Committee found this to be acceptable.',
                'created_at' => '2012-02-09 00:56:28.000000',
                'question_id' => 438,
            ),
            19 => 
            array (
                'answer_text' => 'Of the $11,000 requested, we could reduce the number of residence halls that will receive programming and reduce the number of student instructors that are trained.  With this scope reduction we estimate the cost of the project to be $8,000.',
                'created_at' => '2012-02-09 05:48:11.000000',
                'question_id' => 446,
            ),
            20 => 
            array (
            'answer_text' => 'For the Greening the Game Green Fund proposal the primary items that would need to be funded for this to function in the way that it needs to would be the hiring of the student club ($1,500.00) and then the student employees that would need to be hired for the pre/during game work ($4,000.00). Other items are not as critical to this project. Without the staff though it cannot run as effectively as it needs to. The savings in landfill costs will cover most of these costs in the end because recycling fees are significantly lower then landfill fees.',
                'created_at' => '2012-02-09 08:02:13.000000',
                'question_id' => 449,
            ),
            21 => 
            array (
                'answer_text' => 'Our budget has two items in it: a half-time GA and a computer for this individual to use in their research. Without the half-time GA, we would be unable to do any aspect of the project. It is not possible for staff to pick up any of the research for this project and we would not conduct this occupational research and information development without a fully funded GA position. We do have aging, dated computer equipment that could be re-purposed for this individual to use, but it is slow and would make the research more cumbersome.',
                'created_at' => '2012-02-09 17:40:52.000000',
                'question_id' => 460,
            ),
            22 => 
            array (
                'answer_text' => 'The proposed project cannot be practically scaled down in terms of amount of water harvested.  Therefore, reducing the budget would mean that some project components would have to be eliminated or greatly modified.

The onsite weather station with data streamed live to the web could be replaced with an onsite rain gauge, and other weather parameters harvested from a nearby weather station.  This would save about $600 in the cost of the weather station, but would require about $200 more in staff time for programming, for a net savings of $400.

The analog/digital water meters could be replaced with analog meters that were read periodically (probably weekly).  This somewhat reduces the value of the project as a test case for other buildings on campus, and reduces the data visualized on the website.  Savings would be only about $100.

A simpler system than the water feature could be used to condition the condensate before it is delivered to the cisterns.  This would substantially reduce the project’s aesthetics, and make attracting people to the outdoor signage more of a challenge.  Basically, the system components would all be out of site, in Flandrau’s sub-basement or below ground.  The estimated savings in operations for the outdoor feature and associated pump, plumbing, wiring, plus a reduction in student labor, is about $2,200.  It might be possible to come up with a drastically simplified “water feature” so there is still visible moving water to attract people to the educational signage, but that would require adding back the sump, pump, and some plumbing.  Savings might be about $1,200.

Simpler, cheaper outdoor signage and a simpler indoor display would save about $500 in operations and staff time, but could negatively impact the educational benefits.

Please let me know if you need additional information.

Best, Gary Woodard, gwoodard@email.arizona.edu',
                'created_at' => '2012-02-09 18:36:18.000000',
                'question_id' => 441,
            ),
            23 => 
            array (
                'answer_text' => 'Much of the adjustability in our budget could be in salaries:
-we could reduce the support for technicians (classified regular) down to zero if needed
-the graduate student support could be changed from assistantships to hourly wages.  This would be less desirable, as hourly wages wouldn’t cover tuition and it would be harder to recruit a student specifically for this project.  If we were to reduce to wages, each year I would want to include one graduate student at 0.25 FTE during the two semesters and 0.75 time in the summer – this would sum to 750 hours per year – at a rate of $13.50 per hour a total of $10,125 per year (plus 3.4% FTE).  Another option is to fund the student with an assistantship in year one and with hourly wages in the other.

While I would not want to adjust the design of the installation and monitoring (we need that number of basins, etc. to have a statistically valid and scientifically meaningful study), we could reduce the number of sampling events – we requested 5 sampling events – we could do 3 or 4, which would reduce the budget by $1600 or $800 respectively (from the ‘plant chemical analysis’ line).',
                'created_at' => '2012-02-09 20:13:52.000000',
                'question_id' => 433,
            ),
            24 => 
            array (
                'answer_text' => 'The best way to partially fund this project would be to change from a half-time to a quarter-time RA. That would, of course, narrow the scope of the project.',
                'created_at' => '2012-02-09 22:22:07.000000',
                'question_id' => 435,
            ),
            25 => 
            array (
                'answer_text' => 'AZ Bake Oven needs 100% of the requested $2500.00
Thank-you.',
                'created_at' => '2012-02-09 22:38:35.000000',
                'question_id' => 450,
            ),
            26 => 
            array (
                'answer_text' => 'While the bagging machine will be a key part of our being able to run a retail composting operation, unfortunately our need for a truck and lift gate are higher equipment priorities. While not glamorous, we must have a truck in better repair and which gets better gas mileage than the one the Campus Agricultural Center currently provides us with. The V10 engine in our current truck is costly both in terms of gas and carbon emissions. And the lift gate is essential to ensuring our staff\'s safety. The staff labor costs are also essential to continuing a successful operation as we move toward self sufficiency.

If something must be cut, and we hope this will not be necessary, we urge you to eliminate $25,000 from our budget by cutting the funds for a bagging machine. We appreciate the green fund\'s support.',
                'created_at' => '2012-02-09 23:08:22.000000',
                'question_id' => 457,
            ),
            27 => 
            array (
                'answer_text' => 'Thank you for the opportunity to provide more information.  Regarding our request to fund the ZimRide service, we are only asking for partial funding.  We have signed a three year agreement with ZimRide to bring this service to our campus.  The annual cost of the service is $13,000.  We requested $12,000 in funding from the Green Fund.  This will leave PTS still contributing a total of $27,000 over the course of the three year agreement.  Further, it is our intent to continue this service beyond the three year agreement.  This service has been very successful on other campuses in making ride sharing much more widely utilized by faculty, staff and students.  We firmly believe it will be successful at the U of A.  ZimRide has a modern social networking interface that students find particularly attractive.  We requesting less than one third of the cost of the program be supported with Green Fund funding.  We know there are a lot of competing interests for this funding and wanted to be very reasonable with our request from the very outset.

We respectively ask for Green Fund support of this program which has a clear sustainability nexus.

Thank you',
                'created_at' => '2012-02-10 00:28:32.000000',
                'question_id' => 454,
            ),
            28 => 
            array (
                'answer_text' => 'We have attempted to keep our requested budget lean through our cost share for the first year RA and are setting a relatively ambitious schedule.

The least impact cut to our proposal is funding for the undergraduate student in the second year.  The role of that person is to provide support to examine alternative infrastructure strategies.  We would reduce the number of alternatives considered.  

A slight decrease in the research scientist salary may be possible but his effort on execution and guidance is critical.',
                'created_at' => '2012-02-10 22:31:59.000000',
                'question_id' => 464,
            ),
            29 => 
            array (
                'answer_text' => 'The Sustain Abroad research group consists of three individuals. In the case that this research project was only partially funded, we have decided that it would be possible to complete our research and data collection in Rwanda with two participants. Therefore, the price of one plane ticket may be subtracted from the budget we have submitted. We believe this is an important project and will yield many future results for the sustainability of UA students studying abroad. This group can, and will if necessary, complete the project together again once back from Rwanda. We would like to research this thesis with three people mainly for safety purposes in Rwanda, and also to be able to closely follow the Primate Studies Field School each day, as well as inquire local Rwandans about recycling programs.',
                'created_at' => '2012-02-11 21:05:36.000000',
                'question_id' => 461,
            ),
            30 => 
            array (
                'answer_text' => 'Dear Green Fund Committee,

After discussing the matter, we have decided to provide additional information regarding partial funding of our project on the solar tower. The following is a brief guideline that may be used to partially fund the proposal if deemed necessary.

The total requested amount to run the project consists of $92,550. A decrease in funding would result in the reduction of the number of students involved in the project by one, bringing the total amount down to $87,450. A further decrease in funding would cut the total number of students involved by two, reducing the initial requested amount to $82,350. 

Thank you for your interest and consideration.

Sincerely,
Oybek Kholiqov',
                'created_at' => '2012-02-12 08:15:11.000000',
                'question_id' => 459,
            ),
            31 => 
            array (
                'answer_text' => 'In response to guidance for providing partial funding to our original request, we could reduce the scope of the project, yet still pursue project goals, as follows:

1. Reduce the length of the proposed bicycle shelter by 10%, designing a more compact bicycle parking system with bikes parked in a staggered configuration, in conjunction with prototyping a unique vertical bike storage configuration.

2. Reduce the number of LED lamps and solar PV panels from 18 LEDs to 9 LEDs and from 170 watts of solar photovoltaic capacity to 85 watts, reducing the illumination over a horizontal distance to better suit the shorter length of the shelter.

The above change in project scope would reduce our request from $26,775.00 to $22,647.50.',
                'created_at' => '2012-02-12 16:39:42.000000',
                'question_id' => 434,
            ),
            32 => 
            array (
                'answer_text' => 'Greenfund Committee:

The information below addresses your request for supplemental guidance on the best way to provide partial funding for our proposal entitled "The College of Architecture and Landscape Architecture (CALA) Environmental Audit”.

As discussed in the full proposal this project involves three phases(Phase I: Performance Benchmarking, Phase II: Mitigation Implementation, and Phase III: Performance Evaluation).  During Phase I the team will monitor and record data concerning the waste generation and water and energy consumption of the CALA buildings.  This phase involves extensive collaboration between the team members and facilities management, which has already begun.  The data gathered during this year long process will be compiled and evaluated to establish benchmarks of the measured waste, water and energy metrics observed in CALA buildings. This will establish the necessary benchmark and baseline conditions from which to evaluate the impact of implemented mitigation strategies during subsequent phases of this project.

Phase II will involve an intense analysis of the observed data gathered from Phase I. This data will be evaluated and utilized to formulate a robust series of measures intended to target waste, water, and energy inefficiencies within the CALA buildings.  An interim report detailing the information gathered and identifying mitigation strategies to reduce consumption to be implementedwill be compiled as part of Phase II.  

Phase III of this project involves A final round of monitoring of the metrics inventoried during Phase I. With the Phase II conservation strategies in place, the team will continue to monitor the waste, water and energy metrics of the CALA building, for a full year.  With a second year’s data in hand the team will utilize the information gathered to compare and contrast to the benchmarks of consumption established during Phases I and II and provide an evaluation regarding the relative effectiveness of the mitigation strategies to be performed.  A final report will highlight the effectiveness of the mitigation strategies identified in Phase II and implemented in the beginning of Phase III and provide lessons learned guidance for replicating this effort for other University buildings and beyond.  

Given the phasing of this project, we believe the best way to provide partial funding, while still maintaining the integrity of the project is to initially fund Phase I and II.  If these phases are funded, we will seek funding for Phase III in a subsequent proposal and Greenfund request in the future.  Removing Phase III from this proposal will result in a partial savings to the 2012 UA Greenfund of $23, 357. This brings the total partial funds requested to, $34,438.  Partially funding the proposal in this way will allow for the project to begin immediately as planned and will allow team members to provide a detailed report of progress upon seeking funds for completing Phase III of this project.

Original Project Budget:
Phase I:               $27,613
Phase II:              $ 6,825
Phase III:             $23,357
Total Project Request: $57,795

Revised Partially Funded Project Budget:

Original Request:                 $57,795
Minus Phase III:                  $23,357
Partially Funded Project Request: $34,438


Please do not hesitate to contact us if there are any additional questions.

Thank you for your time and consideration,

Sally Harris, Project Student PI
Ryan Perkl, PhD. Project Faculty Advisor',
                'created_at' => '2012-02-12 17:53:34.000000',
                'question_id' => 462,
            ),
            33 => 
            array (
                'answer_text' => 'Harvested Rainwater for Crop and Landscape Irrigation

We would like to emphasize that our project and budget were drafted as a whole,
and each component is highly dependent on the others. However, per your request, we
have provided additional detail on the most necessary components of our project.
We would like to emphasize the importance and necessity of a 2-cistern
rainwater-capture system for our Highland Garage Project. Cutting one would
significantly change the scope, and quality of the captured water for use in the
community garden. While they are equal in size and cost, each of the two cisterns
have a unique function. The two cisterns will work in series, with the first cistern capturing the “first flush” of stormwater runoff, which contains the majority of nonpoint source pollutants (oils, greases, debris, etc.). Upon the first cistern reaching capacity,the second cistern will capture the subsequent stormwater runoff, which is expect to be of a higher water quality.
As designed, our project would analyze the different water samples from each
cistern, and compare the health of the plant tissue irrigated in the community gardens. If reduced to a one-cistern design, we would: (1) dilute the overall quality of the water captured,(2) be unable to analyze the water quality of the “first flush” of stormwater vs. subsequent flushes,(3) be uncertain about water quality issue for future stormwater capture projects in coming years.
Our least urgent source of funding is to reduction of student labor costs, but this
may impact the scope and rate of progress of the project. Fewer labor hours would
decrease our chances of getting valid and detailed research data.',
                'created_at' => '2012-02-13 00:46:19.000000',
                'question_id' => 470,
            ),
            34 => 
            array (
                'answer_text' => 'Dear Mr. Harris and Green Fund Committee:

The initial project task is to begin measuring condensate output from Flandrau\'s AC system and related weather variables.  Ideally, we would begin measuring these at least two weeks prior to onset of the monsoon and continue through the end of the monsoon.  Monsoon onset is measured by increased dew point, and typically occurs around July 4.  But it can occur any time between late June and late July.  The end of the monsoon is less definite, but usually occurs around the end of September.

Therefore, we ideally would begin measuring condensate and weather factors by mid-June.  Metering the condensate might take a week to set up and test.

If we know well in advance that the project is going to be funded, we might be able to borrow a used analog meter as a stopgap method for measuring condensate.  Or receiving a small percentage of project funding in May would be even better.

Please let me know if you still have questions.

Best,

Gary Woodard
gwoodard@email.arizona.edu',
                'created_at' => '2012-02-13 20:50:46.000000',
                'question_id' => 475,
            ),
            35 => 
            array (
            'answer_text' => 'Thank you for your questions. The funding that we were awarded last year pays for our current graduate student coordinator and will do so next academic year also. He works 20 hours/week with the schools, other garden sites and the interns to make sure that all participants are completing their obligations and that things are running smoothly. Because the project has been so successful in increasing enrollment in the workshop (it is almost double over last year) and expanding to other sites, the graduate coordinator must use all of that time in facilitating the internships. In order to be able to apply for external funding, and/or attract permanent donors, we need to complete a systematic program review. The funding we have requested for next year would allow us to do that by hiring a graduate research assistant. Further, it would allow us to increase the skills of interns by training them in program assessment. The projects are thus closely linked, but we require additional personnel to adequately complete both of them. Please let me know if I can clarify further.',
                'created_at' => '2012-02-14 17:52:24.000000',
                'question_id' => 473,
            ),
            36 => 
            array (
                'answer_text' => 'With the funding coming in that late in the year, we would push back the start date of the project to the summer of 2013.',
                'created_at' => '2012-02-15 20:26:29.000000',
                'question_id' => 474,
            ),
            37 => 
            array (
            'answer_text' => 'The pending NSF preproposal was written before this project was proposed, and focuses more on basin design and impacts on water quality (through pollutant removals) and biogeochemistry. It is designed to primarily support research in GI basins in Tucson proper – we would be able to request some funds from NSF if asked to submit a full proposal to support some chemical analysis of the basin experiment at B2.  

The funds requested from the Green Fund for Y1 were primarily for basin installation, and in Y2 were primarily to support the monitoring of efficacy of design.  If we were only funded for the first year, it would leave us with fewer resources to follow through with studying the efficacy of basin design for supporting plant growth; ideally we would want to follow the fate of the plants through a summer. This falls into Y2 the way we have set up design and installation of the project given the anticipated date of arrival of funding.   We could work with timing and frequency of sampling to better fit the needs of the committee.',
                'created_at' => '2012-02-15 23:09:00.000000',
                'question_id' => 476,
            ),
            38 => 
            array (
                'answer_text' => '1. Looking at what others have done, whether it be a product or a painting, is common practice. It might be better to say \'deconstructing\' and yes it\'s ethical. In our case, we will be looking at various products already out there to see how they are fabricated-not to steal any patented designs-which will let us know what we need to do different and better. A bigger concern will be companies wanting to copy us!
2. The timeline for Phase 1 is two academic semesters...so this spring and next fall.
3. Phase 2 and 3 will depend on what happens in Phase 1. If we are successful, then we have something to take to Eller. At that point, we will be locked into their timelines.',
                'created_at' => '2012-02-17 23:50:55.000000',
                'question_id' => 481,
            ),
            39 => 
            array (
                'answer_text' => 'The $5,500 under other projected sources of funding refers to the amount of Green Fund money we would like to carry forward from our current award that hasn\'t been expended. This amount will also help us off-set some of our extern salaries starting at, the earliest, the week of the May 28th pay period.  Last year, we were allowed to incur pre-award costs which we anticipate doing again this summer since projects are planned to start shortly after students are finished with spring semester.',
                'created_at' => '2012-02-18 03:00:08.000000',
                'question_id' => 477,
            ),
            40 => 
            array (
            'answer_text' => 'Excellent question, and I’m sorry I was not more clear.  Once the rooms are complete, the plan is to begin sharing the software with other sustainability-related departments/organizations on campus (i.e. Students for Sustainability, FM, the Office of Sustainability, and anyone else who would like to add the link to their website).  These different organizations have different populations of users, and so the information should spread widely fairly quickly.  Also, we will feature the website on the Residence Life website and news stories.  This will help capture the many of the newest additions to the Wildcat community.  We will complete a press release and include UA Now and the Wildcat, as well as local media.  Finally, the hope it to purchase some 100% post consumer recycled paper fliers from a “green” printer I use  in town.  These fliers, with a QR code and url, would go up in the residence halls as well as at kiosks around campus.',
                'created_at' => '2012-02-20 15:47:51.000000',
                'question_id' => 479,
            ),
            41 => 
            array (
                'answer_text' => '1. First of all, Compost Cats does not currently own its own truck. We borrow a truck from the Campus Agricultural Center. The truck they have loaned us gets 7 mpg and, in six months of having it, has already cost us $2500 in repair and substitute rental costs, interrupted or canceled three compost runs, and stranded two of our staff in the middle of Campbell Ave once, and continues to have problems. So we have a lemon now that also guzzles gas. We must have a long-bed, full size pickup to transport all our food waste efficiently. It will have to be diesel to run on biodiesel. Running our truck on biodiesel can, according to Dr. Mark Riley, reduce our carbon emissions by 56% for our vehicle use. \\r\\n\\r\\n2. We have partnered with the green funded biodiesel project to begin running a truck on their biodiesel this summer. To do this we will need a full sized diesel truck in good condition. $24000 is not a high price for a 1/2 or 3/4 ton diesel pickup, whether Dodge, Chevy, or Ford, in good condition. If we can get one cheaper, we absolutely will. We will try to find a used one.\\r\\n\\r\\n3. We will eventually have to have a bagging machine to sell retail compost. If forced to choose though, Compost Cats cannot exist at all with a truck that breaks down regularly, has trouble starting despite going to the shop for this, and forcing us to cancel compost runs and/or rent a truck from motorpool, and costs so much just to keep running raggedly. A decent truck is not glamorous, but it is essential.',
                'created_at' => '2012-02-24 20:38:25.000000',
                'question_id' => 484,
            ),
            42 => 
            array (
                'answer_text' => 'Dear UA Green Fund Committee,

We very much appreciate your continued interest in our proposal. Provided below is our response to your question. Please don\'t hesitate to let me know if you require any additional information.

1. Student participants of the project - type of labor, estimated total hours and hourly wage: During the first year of this project, three undergraduate students will work at CEAC, each of them devoting 10 hours per week. Two of them will work under the guidance of Drs. Kacira (co-PI) and Gruener (Consultant) on the solar energy evaluation part of the project and one of them will work under the guidance of Drs. Kubota (co-PI) and Gruener (Consultant) on evaluation of the use of light emitting diode (LED) system. During the last 13 weeks of the year 1 of this project, these students will train two undergraduate students who will implement their findings at the Natural Product Center. These two students will work under the guidance of Drs. Gunatilaka (PI), Gruener (Consultant), and Gunaherath (Post-doctoral Research Associate). Thus, a total of five undergraduate students will participate in the project. In order to attract dedicated students who are willing to travel at their own expense to CEAC and NPC, both of which are off-campus facilities, we have calculated their wages at the rate of $ 10/hour.

2. Technical Consultant of the project - type of labor, estimated total hours and hourly wage: Dr. Gruener together with the PI will coordinate and implement all NPC green house activities, including seed germination, transplantation, and plant data assessment (growth rates as a function of experimental conditions). In addition, he will supervise the installation of the rain water harvesting system, solar panels, and LED systems. Dr. Gruener has the necessary experience as he has already installed a green house and a rain water harvesting system at his residence, taken essential courses in controlled environment agriculture, and has worked as a volunteer at both NPC and CEAC. He will also advice students and train them in analyzing data, preparing comprehensive reports and any peer-reviewed publications resulting from this work. He will work 6 hours/week (2 hours each on Mondays, Wednesdays, and Fridays) on this project. He will devote 312 hours each year to this project. He has kindly agreed to work 212 hours each year at no cost to the project, and this service is considered as in kind contribution to the project. He will be paid for 100 hours each year at the rate of $ 50 per hour.',
                'created_at' => '2012-02-27 19:59:52.000000',
                'question_id' => 485,
            ),
            43 => 
            array (
                'answer_text' => 'The water harvested will be sent to three different laboratories for analysis. These labs include Dr. Riley’s, Dr. Maier’s, and a commercial lab. Dr. Riley’s lab will be performing tests for E. Coli and coliforms, Dr. Maier’s lab will be analyzing petrochemical contaminants, and the commercial lab will be testing for oil and grease. Water quality tests will also be performed in labs on the water in the cistern. The $12,500 was estimated by Dr. Riley as a sufficient amount for all 4 laboratory tests, allocated as necessary between each laboratory for personnel and lab materials. 
In the event that the water is too contaminated to be used directly on the Garden plots the water would be directed to the Garden’s vegetation that is not intended for human consumption (e.g. surrounding landscaping).',
                'created_at' => '2012-02-28 05:36:10.000000',
                'question_id' => 482,
            ),
            44 => 
            array (
                'answer_text' => 'A great deal of change has occurred in the UA Community Garden since last year in March when $16,300 was appropriated to the project by the Green Fund. The contents of this PAR will explain how the money was spent and will also indicate how GITD would like to spend the remainder of the funding.
In June of 2011, a month before the funds were allocated, the garden project managers were beginning to discuss the implementation of irrigation and the excavation of remainder of the beds with Grant McCormick, a staff member for Planning, Design and Construction. It became apparent at this time that students and volunteers would not be able to dig and place the beds, or put in the irrigation due to the fact that it would be a larger work load than could be taken on, and there would be a possibility that it would not be done correctly. All parties wanted to avoid this. Therefore, with the recommendation of Facilities Management; Planning, Design, and Construction, and the advisers of ASUA; the Garden in the Desert (GITD) group decided that the beds and irrigation should be installed by facilities management. The problem that was faced with this decision was that it meant allotting nearly all the money that was allocated to the garden to installing irrigation, a shed and beds, with little to no funding remaining for other projects that needed to be completed in the garden. After much deliberation, it was decided that the money should be spent on the irrigation, shed and beds with the conclusion that it was the only what the construction of the garden was going to take place. The irrigation installation and excavation of the beds costs $15,670.00 leaving the garden with $630.00 for other projects. There was also some money left over from the Student Affairs funding that was originally obtained for the garden.
The remaining money, along with the Student Affairs funding was used on water ($268.29), a shed ($1,704.17), and other irrigation parts such as drip tape ($410.00). There is currently approximately $1,600.00 remaining from the Student Services Fee which will be used for tools and such things as organization shelves, and fencing. Overall, there are 45 beds, installed irrigation, a shed, tools, and miscellaneous items such as hoses, a shifter, and branch cutters. The remainder of the money will be spent on a financial system that will be able to keep track of payments and rentals for garden plots. This will provide security to the GITD and SFS as organizations since a formal way needs to exist to keep track of the money that will be collected for the renting of the plots. This financial system is a computer program that allows management to organize the information of the renters, such as when payment was made as well as which plots are being rented. The cost of the program runs around $800.00. In addition, a substantial quantity of tools will need to be purchased in the near future, as tools originally borrowed from Community Gardens of Tucson, will need to be returned.
Not only has the management of the garden been changed but an understanding of a need for and implementation of a new structure has been enacted. The current project managers of the UA Community Garden are Laura Hanson and Taryn Contento. The original leader of the project, Caleb Weaver, had done quite a bit of outreach to other groups such as the Community Gardens of Tucson has graduated and left the project. Laura “Sofia” Montes and Amy Moller took the lead as the project managers at that time.  The initial group that was involved in the garden plans at the beginning of the summer was Planning, Design and Construction, the Office of Sustainability, and the Community Gardens of Tucson (CGT). However, part of the original group, the partnership between CGT and the GITD, was never fully realized and defined. This became a conflict when it came time to discussing who was managing the plots and their owners. Chet Phillips and the ASUA staff began a process to develop a Memorandum of Understanding (MOU) between the GITD and CGT  in order to define the relationship, roles and responsibilities of each group.  Although compromises were considered neither group could come to an agreement. At that point, the partnership was terminated between the groups.
In order for the GITD to continue to receive guidance, and to keep stakeholders involved, a UA Community Garden Oversight Committee was established. This committee currently meets at 9:00am on Wednesdays. Currently involved in the committee are a faculty advisor, Tom Wilson, the three Students for Sustainability Directors (Natalie Lucas, Michele MacMillan and Chet Phillips), and the two current project managers of the garden (Taryn Contento and Laura Hanson). There is also communication between this committee and  Facilities Management and Planning, Design and Construction to have one of their staff members become a member of the committee. Additionally, as relations are built with the community, a community member will be invited to sit on the board. GITD would also like organizations that have an understanding of garden basics and with a wide range of gardening expertise to sit on the committee. Once the project is up and running the committee will no longer meet weekly, but rather every other week or monthly but currently weekly meetings are necessary in order to organize and keep everyone up to date on events such as a planting day that will be happening on March 17th and the ribbon cutting Grand Opening March 26th. Other critical components of the project are also discussed by the committee, such as getting a fence to protect the garden and a sign to define and draw attention to the garden. Committee meetings are open for all people to attend, and the group GITD also holds weekly meetings in order to engage outside students in the project.
There are a few things currently planned for the future of the garden financially. We were not able to put in all of the items listed in the original Green Fund proposal, and there has also been changes in how we are proceeding with the garden as you can see from this short history. We plan on putting in a fence, sign, benches, a Ramada, picnic tables, beautification plants and composting units as we move forward. Each mini project for the garden will have to be presented to the committee and then to Facilities Management/ Planning, Design and Construction for approval. Each of these components are necessary for the garden to come together and feel like a community where people will want to be. We will be pursuing the implementation of these components within the next year.
If the Green Fund Committee has any further question on why the money was spent in the way it was, how the garden is being currently run, or what plans we have in the future please feel free to contact me. We appreciate your patience and support as we have developed this project.

Natalie Lucas
(928)600-7844
nrlucas@email.arizona.edu',
                'created_at' => '2012-03-04 05:24:08.000000',
                'question_id' => 488,
            ),
            45 => 
            array (
                'answer_text' => 'Dear Committee,

We have not worked with facilities management as we understand their time and energies are highly constrained by meeting the needs of campus. Our modeling goal is to develop a robust model, with the help of the world’s foremost expert on solar updraft towers, which would be able to create configurations that would work at several different locations on campus. As the models develop, about 3 months into the project, we would then begin working with facilities management with a much more focused view regarding what is and is not possible. At the early stages of this effort with our preliminary work, it would have wasted the time of the facilities management staff.
However, we became aware of the new building that will be constructed between the 6th Street Garage and the USGS building on Park Avenue.  If funded, we would immediately begin a dialogue with the architects and facilities management about designing part of the structure to house the solar tower since the building is to be dedicated to renewable programs. As a worst case scenario, the College of Engineering has announced that it is cleared by the Board of Regents to begin fundraising for a new engineering building to be constructed beside the Aerospace and Mechanical Engineering building. While this effort is 2-4 years out for fundraising reasons, we would speak with the Dean’s office and architects to determine the suitability of this new building to serve as another test bed for a solar updraft tower.
Our modeling efforts would support working with any and all of these groups as time progresses so that, possibly, multiple versions of the power generating system could be integrated with new and existing buildings, with external funding providing the impetus after this grant expires.

Sincerely,
Oybek Kholiqov',
                'created_at' => '2012-03-04 09:17:56.000000',
                'question_id' => 486,
            ),
            46 => 
            array (
                'answer_text' => 'Yes, we can still implement this project for $10,000, which can provide us with a .25 GA and her ERE. We will need to make some changes in the scope of the project since we will have half the research/writing time. I anticipate being able to research and develop the web pages with information on Green careers. Instead of trying to provide this information by major, we will focus on broader Industry areas. We may not be able to have some of the detail such as salary information, which is very time-intensive to research, but we will have enough information to assist students in their decision-making and provide links for further research on the student’s part. We will not be able to develop the mobile version as this time, but will still be able to develop handouts. Career Services will be able to contribute the in-kind portions consisting of the marketing of the information and providing presentations to campus advisors. Thank you for continuing to consider us for this grant.',
                'created_at' => '2012-03-05 21:09:03.000000',
                'question_id' => 487,
            ),
            47 => 
            array (
                'answer_text' => 'To respond to these well-founded questions, I consulted with our collaborators in the Utilities Management and Services group of Facilities Management to develop the responses below.  

First, in response to the water table changes in the vicinity of the University due to pumping changes, Tucson Water has made a conscious effort to raise water levels in the central part of the city and has reduced pumping in their proximal wells.  As a result, UA FM has seen a rebound in the static water levels.  For example from UA measured data, the Park Avenue well appears to be rising at a rate of about 1 foot per year.  This is consistent with the USGS study documented in the Tucson Citizen that shows an increasing trend without quantification.  I was unable to produce a report from the USGS site to give firmer numbers.
http://tucsoncitizen.com/wryheat/2012/01/30/trends-in-groundwater-levels-around-tucson/

As a result, we conclude that the impact on aquifer levels will be minimal unless Tucson Water returns to pumping nearby wells.

In regards to plume spreading, for clarification, the nearby plume is actually Tetrachloroethylene, (or Perchloroethylene (PCE)) rather than Trichloroethylene (TCE).  Water in the plume contains minor traces of TCE and Dichloroethylene (DCE) degradation products but the primary contaminant is dry-cleaning fluid (PCE).  For details of the Park/Euclid contamination site, see http://www.azdeq.gov/environ/waste/sps/download/tucson/parkeuclid.pdf  and
http://www.azdeq.gov/environ/waste/sps/download/tucson/parkfact.pdf
http://www.azdeq.gov/environ/waste/sps/images/parkeuclidmap.pdf provides a map of the plume relative to campus.

Considering possible contamination of the UA wells, hydrogen and oxygen isotope studies by Dr. Chris Eastoe of the Geosciences Department show that the two largest producing UA wells tap a deeper part of the regional aquifer that appears to be recharged along mountain-front faults, and appears to have a higher static water level than the overlying aquifer.  Thus, these wells will likely not be impacted by the contaminant plume.

In addition, three “sentinel” wells south of campus are located between UA wells and the plume.  Of those wells, only one has consistently shown detectable levels of PCE but it has held constant at just under the 5 mg/l drinking water MCL for PCE for several years.  ADEQ regularly monitors those wells, and all UA operating wells south of Speedway for PCE and related contaminants.  If the levels begin to rise, pumping plans will be re-evaluated.

To more accurately access the influence of pumping on the aquifer, a groundwater model would be necessary.  ADEQ contractors have done some modeling on the aquifer in the area, as referred to in http://www.azdeq.gov/environ/waste/sps/meetings/reg2008/0626mpe.pdf (see page 2).  However, I have not been able to find further details on results or possible access to the computer model. Our team would be capable to running the model to analyze pumping impact if it was available.

Lastly we have a change on our project team status, since we submitted this proposal Dr. Doosun Kang has interviewed for, was offered and has returned to Korea to take a faculty position.  The funding that I reserved for his salary from another project will be used to support a new post-doc to be hired this summer.  It is expected that this person will take responsibility for the first year tasks noted for Dr. Kang with emphasis on demand modeling on this project.  I will take responsibility for the second year tasks so the $2500 salary plus benefits originally designated for Dr. Kang in the second project year can be removed from the budget.',
                'created_at' => '2012-03-06 00:32:56.000000',
                'question_id' => 489,
            ),
            48 => 
            array (
                'answer_text' => 'The total funds of $38,900 requested from the Green Fund committee for Computer Power Management software will remain unchanged.  The proposed pilot will still  implement approximately 2,250 computer management software licenses throughout the university and assess the results.  The accepted budget also is unchanged in year one of the project.  The loss of the TEP rebate will only effect year two of the pilot project.  The TEP rebate was to fund $1,000 or 80 hours of student labor plus $14,061 for three years of subsequent year maintenance and support.  Facilities Management will provide 80 hours of student labor in year two of the project.  The options available to provide additional maintenance are for the participating departments to accept fiscal responsibility after the initial year, for the project team to submit a new proposal which includes maintenance for the purchased software licenses to the Green Fund committee for 2013-14, or some combination of the first two options.  Under the current circumstances without any subsequent changes, participating pilot project departments will need to accept fiscal responsibility for vendor support and maintenance after the initial year.',
                'created_at' => '2012-04-17 23:47:57.000000',
                'question_id' => 491,
            ),
            49 => 
            array (
                'answer_text' => 'Budget Spreadsheet sent to aharris1@email.arizona.edu.',
                'created_at' => '2012-04-19 21:08:31.000000',
                'question_id' => 490,
            ),
            50 => 
            array (
            'answer_text' => 'Initial computer power management software expenditures will still pay for itself in reduced energy costs in the same amount of time (estimated at 12 months).  Savings over a multi-year period will drop due to the increase in maintenance and support costs in lieu of a TEP rebate.  The departments will not realize the financial savings directly unless they pay for their own energy costs regardless.

Pending a final decision, we could make selection of a pilot department dependent upon that department accepting fiscal responsibility after the first year.  The result of not having maintenance and support differs between vendors.  In most cases, the departments would continue to own the software and can continue to use it without support, but they would not receive any updates.  The software we have looked at has no volume discount for maintenance so that there would be no advantage to pooling support payments between departments.

David Kasper of TEP wrote on April 24th that “Currently all funding for the TEP rebate program has been suspended (spent).”  I have sent him a follow-up question asking if the program might be reinstated.

I will explore and revisit alternative funding sources before next year’s Green Fund Committee meetings.',
                'created_at' => '2012-04-24 15:21:31.000000',
                'question_id' => 492,
            ),
            51 => 
            array (
                'answer_text' => 'I am asking to move the funds out of personnel and ERE and into operations so that I can pay the former students who graduated.  Because they are intimately familiar with the project, it makes sense for them to continue being involved rather than than hiring new students.  I can no longer pay them as UA students, so the increased operations funds will be used to pay them as non-UA contractors.',
                'created_at' => '2012-09-26 22:52:52.000000',
                'question_id' => 432,
            ),
            52 => 
            array (
                'answer_text' => 'UA Green Fund committee, 

The date of 10.01.12 was an estimated completion date selected without understanding the timeline for PAR review. 

To clarify, I am requesting an extension of funding to complete this project by 12.31.12.

Thank you for your consideration.',
                'created_at' => '2012-10-01 18:10:13.000000',
                'question_id' => 493,
            ),
            53 => 
            array (
                'answer_text' => 'UA Green Fund,
re GF12.25

Adjustments to the settings of the equipment by UA facilities dept. are ongoing, and will incur additional costs.  We expect to complete the project within budget by December 1st 2012.  No costs will be incurred after this date.  Per our grant proposal, we plan to track the energy savings for a period of one year after implementation.',
                'created_at' => '2012-10-02 02:53:47.000000',
                'question_id' => 494,
            ),
            54 => 
            array (
                'answer_text' => 'During the summer 2012, we hired two undergraduates to work on the water recirculation project.  These two students were hired as part time students and successfully completed the major installation activities-this included plumbing water feed lines, return water lines and pressure testing the system.  They also worked diligently to clean the existing tank and associated equipment.

Following the completion of the major installation and initial startup and the end of the summer break, there were no additional large-scale tasks and the students stopped working on the project.
During the course of the semester, the chilled water unit was extensively utilized by the undergraduate unit operations laboratory courses it was designed to support.  And while it appears to be functional, there are minor aspects that need periodic adjustment-for example:  Cleaning the tank filters and optimizing the pressure control switches.  Additional work on optimizing the pressure/flow relationship for the return lines also needs to be evaluated.  This activities are common when installing new large-scale process equipment and are typically accomplished incrementally-spending short periods of time (say ½ hour/day a few days a week) tweaking the system.

Due to the intermittent and uncertain time requirements for these activities, it is not feasible to hire students to complete these tasks-the Department currently does not have any student employees working on department-related “as needed” activities for similar reasons.  It would not be meaningful to students to do these activities.  Also, the students originally hired to work on the project do not have the engineering field-experience needed to safely tune the pressure controls-whereas the faculty PI is experienced in shakedown and tuning of equipment in the field.',
                'created_at' => '2012-11-27 16:02:36.000000',
                'question_id' => 495,
            ),
            55 => 
            array (
                'answer_text' => 'I sort of understand.  I am not sure if there was a "question" in the statement above that this format is asking me to respond to.  I will await formal questions from Lauren.',
                'created_at' => '2012-12-03 20:13:01.000000',
                'question_id' => 496,
            ),
            56 => 
            array (
                'answer_text' => 'The project funding for faculty is considered supplemental compensation

The three courses that will make use of the chilled water reciruclation system, ChEE 301A, ChEE301B and ChEE401A currently do not have course fees.',
                'created_at' => '2012-12-04 15:09:04.000000',
                'question_id' => 497,
            ),
            57 => 
            array (
                'answer_text' => 'I\'m trying to finish it, but it looks like it might take an extra week or two. I have too many end-of-year deadlines, so an extra week or two would be greatly appreciated!',
                'created_at' => '2012-12-30 19:50:11.000000',
                'question_id' => 498,
            ),
            58 => 
            array (
                'answer_text' => 'I will not have all available data until campus opens up.',
                'created_at' => '2012-12-30 21:22:11.000000',
                'question_id' => 499,
            ),
            59 => 
            array (
                'answer_text' => 'Julia, 
The final report is still being prepared; it will not be ready by December 31, but should be completed sometime in January.
Best,

Wil',
                'created_at' => '2012-12-31 23:06:41.000000',
                'question_id' => 503,
            ),
            60 => 
            array (
            'answer_text' => 'Given the recent funding change, it will be hard to complete this (as a final report).  I just got this notification.  I can provide an update shortly.   Not sure of the format for the report and how we work out the "ongoing activities" planned for spring 2013',
                'created_at' => '2013-01-02 17:26:22.000000',
                'question_id' => 510,
            ),
            61 => 
            array (
                'answer_text' => 'Still waiting on Paige for financial updates so that I can submit my progress report. Sent her another reminder today, so hopefully I\'ll be able to submit my report by Friday.',
                'created_at' => '2013-01-02 22:51:02.000000',
                'question_id' => 505,
            ),
            62 => 
            array (
                'answer_text' => 'AGTM GREEN TEACHING LAB

Project Status Report

1. AGTM Laboratory Lighting Retrofit: Project completed by Campua Agricultural Center facility maintenance crew. Existing laboratory lights and fixtures removed and replaced with newer, energy-efficient lighting. Additional lighting installed over welding booths. Result: Improved lighting quality especially in student work areas where welding takes place.

2. Model Photovoltaic Demonstration Trainer: First stage was to acquire necessary materials to construct the wooden-framed structure. To accomodate the 100-watt solar panels donated by the CALS Experiment Station, the completed structure measures eight feet in length and six feet in width and includes a sloped shingled-roof. The structure is mounted on six locking casters. Two solar panels are mounted on rails on the shingled roof. Solar components including a charge controller, inverter, and deep charge battery were purchased. Final stage is mounting various disconnect boxes, service panels, conduit, and fasteners, pulling wire and connecting components.

3. Rainwater Harvesting System: Six-inch seamless metal gutter, 110-feet in lengh was purchased and installed on the south-facing roof of the AGTM teaching laboratory facility. Students in the AGTM 330 course (spring 2013) will be instructed in the installation of the six-foot diamter galvanized cistern. Cistern materials will be purchased from a local vendor. Students will excavate area, measure and construct concrete form, install over flow pipe fittings, outflow pipe, rebar & dobies over a period of three laboratory sessions prior to mixing, pouring, and finishing the concrete in a fourth laboratory session. This is scheduled to occur in April 2013.

4. Professional Development: Prior to the release of funds for the project, the project director completed a 60-hour hands-on train course on rainwater harvesting collection systems. During the fall 2012 semester, the project director served as a co-instructor of SWES 310 Residential Rainwater Harvesting course. In March 2013, the project director will attend and participate in a week-long Solar Electric Design & Installation (Grid-Direct) course, sponsored by Solar Energy International. The workshop will be held in Tucson.',
                'created_at' => '2013-01-03 18:27:08.000000',
                'question_id' => 507,
            ),
            63 => 
            array (
                'answer_text' => 'Since submitting this PAR, the UACG Committee reviewed an estimate from Facilities Management for the chain link fence of $11,205, considerably higher than we expected. We have decided to seek other estimates from private companies for comparison, but at this time we request that the Green Fund Committee table our PAR request until we determine how much money we will actually need for the fence',
                'created_at' => '2013-01-19 19:32:40.000000',
                'question_id' => 515,
            ),
            64 => 
            array (
                'answer_text' => 'We explicitly put that we would recruit students by June 26 2013 for start in July 2013 after funding comes in; m mastvch from the Kellogg Foundation endowment gives me bridging funding til this Green Fund support potentially starts so I may be able to start one or all even earlier.

Four high scool or U A erntry students from low income communities in Santa Cruz county plus three undergrads who have already had UA classes will be funded. In addition I will fund thru UA SW Center and/or Borderlands Habitat Restoration nitiative grad student Dave Siebert and alumni Caleb Weaver (likely to renter UA this year as unclassified grad student) as part of my match.',
                'created_at' => '2013-02-10 16:26:20.000000',
                'question_id' => 523,
            ),
            65 => 
            array (
                'answer_text' => 'Revised budget and work plan for Green Fund proposal ‘Hoop House Community Garden Management Training and Research’:
Option 1: Reduce graduate student salary by 36% for a total project cost of $20,438. With this budget we will build the hoop house as proposed, present 7 workshops over 2 semesters and train UA Community Garden Managers 4 hours per week during spring and fall semester.
Option 2: Reduce graduate student salary by 50% for a total project cost of $15,404. With this budget we will build the hoop house as proposed, do 5 workshops over one semester and train UA Community Garden Managers 2 hours per week during spring and fall semester or 4 hours per week during one semester.',
                'created_at' => '2013-02-13 15:23:41.000000',
                'question_id' => 521,
            ),
            66 => 
            array (
                'answer_text' => 'Thanks for providing me the opportunity to address these concerns.  Regarding the first question, yes, the project can be accomplished with a reduced salary line.  Of course the optimal budget is as we projected, but with a reduced budget we will still be able to accomplish most, if not all, of the stated goals, with perhaps a slightly slower project initiation phase.  However, with this in mind, we will likely reorganize slightly some of our immediate tasks to accommodate a slightly modified budget line.

Regarding the second question, the answer is again yes.  We originally budgeted for a research specialist because this person could provide a robust focus on the project in the initial phase to insure the project takes off rapidly.  However, considering we would be considering some slight reorganization of the project\'s initial start-up, due in part to a reduced salary budget line, this would not be so much an issue.  In our original plan, we would be developing mushroom growing facilities at the School of Plant Sciences, the Controlled Environment Ag Center, and the Tucson Village Farms simultaneously.  With a modified plan, we would likely bring up production in each of these centers sequentially.  If we did not hire a research specialist, we would like replace this position with another 0.50 FTE graduate student, if that would be possible.  Alternatively, we could fill this position with an increased number of undergraduate research assistants.

I hope this addresses all of your concerns, and I would be happy to continue to provide you more information if necessary during this proposal review process.  Best regards.

Barry Pryor',
                'created_at' => '2013-02-13 16:18:38.000000',
                'question_id' => 522,
            ),
            67 => 
            array (
                'answer_text' => 'Yes, we would be able to easily fulfill this request',
                'created_at' => '2013-02-13 23:46:13.000000',
                'question_id' => 531,
            ),
            68 => 
            array (
                'answer_text' => '1. FM and other staff departments are aware and supportive of this project through our interaction with the Surface Water Working Group. A letter should be easy to secure.

2. We could build fewer cisterns, but the cisterns are part of the project\'s long term value to the UA, decreasing campus dependency on pumped groundwater in diminishing aquifers. Installing no cisterns at all might or might not be possible, but doing so would eliminate the permanent, green infrastructure that is part of this project\'s lasting value to the UA (the other part being the research and disseminated results). 

3. If "first flush" filtration makes the water safe for edible garden crops, we can make this filtration standard practice. Whether it does is part of what the project will evaluate. If this simple filtration technique still doesn\'t render the water safe for garden crops, cistern water will still be very valuable for tree crops (trees filter most residues out), the pollinator hedgerow around the garden, and other landscaping. No water would go to waste. We need to utilize rainwater harvesting more widely on campus as part of the campus "water budget." This project would be a lasting step in that direction.

4. Marketing steps: on site signage, tours of the community garden including cisterns, coverage in local print, radio, and tv media, tours of the project as part of SWES 454/554 courses every spring (already discussed with course instructor Hazel Cox). Research results would also be disseminated through a prepared paper to be submitted for publication and power point presentation to be given on campus and at relevant conferences like the American Association for Sustainability in Higher Education (AASHE) or the Soil and Water Conservation Society (SWCS). We would also be open to additional marketing suggestions from the committee.',
                'created_at' => '2013-02-14 00:15:17.000000',
                'question_id' => 525,
            ),
            69 => 
            array (
                'answer_text' => 'This year\'s Compost Cats Green Fund request is not as well organized as in the two years previous for reasons explained here. Yet the tremendous record of accomplishment in waste reduction and campus education and the great potential for growth and sustainable revenue are not diminished by the difficulty in projecting our exact needs for 2013-14. 

Since Campus Agricultural Center management moved us to a much smaller, dustier site at the West Ag Center in early fall 2012, we have been unable to expand production as we planned to, and this has meant less selling of compost than we hoped for. In recent weeks, we have had no bobcat (for use adding materials to our windrows) despite needing one daily. We will submit a PAR request soon to address this need through either lease or purchase. The salient fact here though is that we clearly cannot continue indefinitely at the Campus Agricultural Center due to profound worldview differences with head management there. We would likely already have been shut down if not for our well publicized successes and support from other departments at the UA and well beyond in the Tucson Community. As an example, after demonstrating our competence in collecting compost and recycling at the Tucson Meet Yourself festival this past October, visited by over 100,000 people, we earned commendations from Mayor Jonathan Rothschild, the Pima County Board of Supervisors, and the festival organizers, along with a commitment from the county and festival to eliminate styrofoam from the festival in the future and further promote recycling and composting there.

In seeking to proactively address our site situation, Compost Cats and the Community Food Bank have agreed in principle to partner at a new site. Pima County officials have also expressed support in helping us find a new site, but we do not know when this move will take place. We hope it will happen this spring. The food bank is much more philosophically and temperamentally aligned with our project, and they also possess the equipment we need to compost in the same manner we have been without having to spend money buying heavy equipment. It is hard even to project our needs for the coming year, beyond labor, with a large move on the horizon, and we ask for the Green Fund committee\'s understanding. In any case, engaged student labor is likely to be our single largest need in any new partnership, with increasing student team engagement in public education about composting and waste reduction as well as doing the basic compost runs.

As our budget attests, despite our site limitation, we have continued to bring in new sources of revenue. With 6 campus Greek houses slated to begin fee-for-service composting programs this semester, and compost sales brisk, we know that we have a model to build on, but our 2010 projections of being there by now were overly optimistic. At a new site, compost production capacity should increase dramatically, and this will also increase our revenue from sales. 

A final important part of the picture though is one we may turn to the Green Fund for assistance with. Some Green Funded programs, like the UA Community Garden, will never be entirely self-supporting but add much to the environmental and agroecological projects of our land grant university. Compost Cats has greater potential for self-support, but a critical part of this puzzle will be requesting that departments like Facilities Management and Student Union Dining Services, who save landfill fee money when we divert waste for composting, put some of the savings back into our budget. Institutionalized support for worthwhile programs will be critical to the long-term success of Green Fund projects that are not meant to expire after a single year. I understand that there may be hesitation on the part of committee members to fund green student jobs and programs indefinitely. Yet worthy projects, not just this one, would currently be unable to continue without the Green Fund\'s substantial help. So it seems that shared consideration is due to how the Green Fund Committee and Office of Sustainability can support institutionalizing certain projects without losing its ability to fund worthy new projects as well. 

Perhaps a separate fund or capped part of existing funds should be allotted for operating budgets for long-term projects the Green Fund has seem ongoing success from? A basic operating budget for certain projects could be automatic, not requiring annual grant approval, and only new endeavors or expansions requiring new infrastructure would be submitted as new grant proposals? Another possibility would be Green Fund and Office of Sustainability formal assistance with lobbying for financial support for long term projects from non-Green Fund sources: campus arboretum and Plant Sciences Dept. support for the community garden, for example, or FM and Dining Services support for Compost Cats? Whatever the solution, we believe the Green Fund has obligations to continue to support ongoing projects that show good results and to lend support to worthy start ups. How to do so deserves further thought and collaboration.

The fundamental project of Compost Cats is two-fold: 1) the transformation of physical waste from liability into a local asset for sustainable agriculture and landscaping, and 2) the transformation of campus community views on the proper uses of food, paper, and other organic material discards. On both counts, we have already achieved tremendous success in two years of operation, and on both counts, much remains to be done in a society where, according to the EPA, over 60% of what goes into landfills could instead be transformed into an asset for building local soil fertility through composting.',
            'created_at' => '2013-02-14 02:40:35.000000',
            'question_id' => 525,
        ),
        70 => 
        array (
        'answer_text' => '1) What will happen to the Community Garden if the Green Fund does not provide funding for FY2014? Will the Community Garden close?

Students for Sustainability has a solid investment in the garden, and will do what it takes to keep it running regardless of whether it has funding from the Green Fund or not. However, there are several items that are absolutely necessary that must be funded in one way or another. They include irrigation, a student worker for the summer, and maintenance, which allows the UACG to maintain pipes and other irrigation pieces in the garden. Students for Sustainability and ASUA would work to cover these costs, but it would be difficult considering the many other projects that will need to be supported by Students for Sustainability and ASUA. Beyond these necessities, there is one vital improvement necessary for the garden to develop as a part of the UA campus.  Being denied funding for this improvement will not force the garden to shut down, however it is fairly necessary to establish the garden as a part of the community that is desired. This improvement entails having the garden become American Disabilities Act (ADA) compatible. To fully welcome all members of the Wildcat community to be owners and contributors to UACG, we will need to make certain adjustments to abide by ADA requirements. Without the assistance of the Green Fund, the garden would not be able to fully join the rest of the UA campus in being inclusive with all community members.


2) Given plot rental fee income, and operational costs, what is the cost to sustain or maintain the Community Garden project if additional improvements are not factored in? 

Rental fees: 102 rentalable plots at $40/year = $4080
Irrigation fees: $400/month for 1 year = $4800
Maintenance fees: $2000

(Maintenance fees might not be this high, but it is important to plan for mishaps especially since the garden has only been in operation for a year now.)

To maintain the garden would be approximately: $6,800/year 

With rental fees covering quite a bit of it what would not be budgeted for would be approximately: 
Total: $2,720/year

3) Signage is included in the new Community Garden budget. If funding is given for this budget item, the Committee would request the Green Fund Logo is added to the sign. Can you fulfill this request?

Yes, we would be able to  fulfill this request',
'created_at' => '2013-02-14 06:33:50.000000',
'question_id' => 530,
),
71 => 
array (
'answer_text' => '1. FM and other staff departments are aware and supportive of this project through our interaction with the Surface Water Working Group. A letter should be easy to secure.

2. We could build fewer cisterns, but the cisterns are part of the project\'s long term value to the UA, decreasing campus dependency on pumped groundwater in diminishing aquifers. Installing no cisterns at all might or might not be possible, but doing so would eliminate the permanent, green infrastructure that is part of this project\'s lasting value to the UA (the other part being the research and disseminated results). 

3. If "first flush" filtration makes the water safe for edible garden crops, we can make this filtration standard practice. Whether it does is part of what the project will evaluate. If this simple filtration technique still doesn\'t render the water safe for garden crops, cistern water will still be very valuable for tree crops (trees filter most residues out), the pollinator hedgerow around the garden, and other landscaping. No water would go to waste. We need to utilize rainwater harvesting more widely on campus as part of the campus "water budget." This project would be a lasting step in that direction.

4. Marketing steps: on site signage, tours of the community garden including cisterns, coverage in local print, radio, and tv media, tours of the project as part of SWES 454/554 courses every spring (already discussed with course instructor Hazel Cox). Research results would also be disseminated through a prepared paper to be submitted for publication and power point presentation to be given on campus and at relevant conferences like the American Association for Sustainability in Higher Education (AASHE) or the Soil and Water Conservation Society (SWCS). We would also be open to additional marketing suggestions from the committee.',
'created_at' => '2013-02-14 22:05:20.000000',
'question_id' => 532,
),
72 => 
array (
'answer_text' => '1) 5 students
2) Yes, responsible parties have agreed to put in volunteer time, which is acceptable',
'created_at' => '2013-02-19 23:25:55.000000',
'question_id' => 535,
),
73 => 
array (
'answer_text' => '1.  Agreed.  We plan to work closely with UA Facilities Management on implementation of the lane marking project.
2.  While there are unfortunately reported accidents at most intersections at the perimeter of campus shown in the University of Arizona Area Bicycle and Pedestrian Study (Dec 2012), the greatest concentration along bikeways to campus occur along Mountain Ave at the north side of campus.  The proposed are of study along Mountain Ave from Helen to 2nd Street has the potential to offer the most positive impact for the investment.',
'created_at' => '2013-02-20 19:18:54.000000',
'question_id' => 533,
),
74 => 
array (
'answer_text' => 'Kindly note that the specified item is line item 39, as for the required budget, it is what CAPLA charges for work to be done inside the workshop, and the fees include the salary of a faculty supervisor, and the 80 hours is the time needed to complete the production of the project inside CAPLAs\' workshop. The total amount of 80 hours @ 120$ an hour was not calculated by the project team, but was obtained from the CAPLA workshop and was listed under personell since it includes the supervisor.',
'created_at' => '2013-02-21 05:01:40.000000',
'question_id' => 538,
),
75 => 
array (
'answer_text' => 'Kindly note that the final total is 3900$.
$500 for software development, $400 for travel out of state and $3,000 for the Image Capture Devices.
Our budget does not include any other items or expenses.

We have made corrections to the spreadsheet please advice where we can resubmit the edited one.',
'created_at' => '2013-02-21 05:08:51.000000',
'question_id' => 538,
),
76 => 
array (
'answer_text' => 'Dear committee, 

This project has not applied for funding with WEES currently or in the past. 
Please let us know if you have further questions.

Thank you, 
Oybek Kholiqov',
'created_at' => '2013-02-21 14:44:03.000000',
'question_id' => 537,
),
77 => 
array (
'answer_text' => 'Dear committee,

We have not applied for funding with WEES currently or in the past. 
Please let us know if you have further questions.

Thank you,
Oybek Kholiqov',
'created_at' => '2013-02-21 17:40:10.000000',
'question_id' => 537,
),
78 => 
array (
'answer_text' => 'Perforated metal sheet working as a shading device or solar screen is commonly seen in Tucson. The brief introduction in Wikipedia putting that: “Perforated metal is also an excellent means of achieving sustainable design objectives. Perforated sunshades and sunscreens provide privacy for building occupants without blocking the view. And they offer a comfortable level of natural lighting during daylight hours while deflecting heat to reduce the load on the HVAC system. Perforated canopies, façades and cladding can also be used to help control interior climate and save energy” basically elucidate its application. There is actually a certain number of buildings on campus that use this device to shade the window, glass façade, porch, etc., both as a screen and overhang. These buildings include Student Union Memorial Center, New Residence Hall, Chemical Sciences Building, and Eller dance theater.

Is perforated metal that damps the noise an existing technique? The answer is yes. The sound transmissivity of rigid perforated metal sheet is governed by three parameters: perforation hole size, material thickness, and percentage of open area. According to Charles M. Salter\'s book ACOUSTICS, the thinnest material, the smallest hole size and the greatest open area (the greatest number of holes) can result in the largest  transmissivity  of the sound waves. In this case, the metal sheet works only as a sound-transparent surface which should be combined with the use of other acoustic material (either reflective or absorptive). Otherwise, a certain percentage of sound waves will be reflected by the metal sheet.

Technically speaking, there are two main applications in using the perforated metal sheet in terms of noise damping property. One is regarding it as a sound transparent screen while acoustic absorptive material would be mounted somewhere behind the screen, for example, another layer of fiberglass could be mounted behind the screen (metallic sheet as an overhang), or in the frame of the shading device (metallic sheet as an vertical screen, mounting details may have many variations), thus enabling the whole device acoustically effective. The other is making the screen or overhang become absorptive as a resonator in a narrow band of frequency, which later will be combined with other acoustic material to work together to achieve acoustic damping effect, considering the fact that the traffic noise may vary in a wide range of sound frequency. There is yet another less used scenario, in which the metal sheet will work as an air diffuser to break up the turbulence in air flow. According to by Theodore J. Schultz, this type of usage is highly technical and less marketable. In order to determine which strategies would be use, we will audit the real situation of how the noise coming from the speedway Boulevard to the studio inside distributes among each frequency band by a sound level meter with a filter. If the frequency we received in studio concentrated in a narrow frequency region, we will probably use application 2. The calculation of the perforated rate, size of the hole and the thickness can be done according to a group of formula. Otherwise, we will case-sensitively use both application 1 and application  2.',
'created_at' => '2013-02-21 19:30:45.000000',
'question_id' => 534,
),
79 => 
array (
'answer_text' => 'That sounds great - please go ahead. Let me know if you need any further information.',
'created_at' => '2013-02-25 18:32:06.000000',
'question_id' => 541,
),
80 => 
array (
'answer_text' => 'Unfortunately,  no. Replacing the signs was the primary impetus for the project. Further, while only 50 new signs are needed for the mobile application and geocache, replacing only some of the signs would introduce further concerns about sign uniformity and marketing impact(many trees have multiple signs all with different information - QR coded signs are needed to streamline content and unify signage)',
'created_at' => '2013-02-25 18:43:01.000000',
'question_id' => 542,
),
81 => 
array (
'answer_text' => '30% is needed for faculty, and 70% for staff. The reason for not including student employee salaries is that the students on this project will graduate in May 2013. After graduation, to finish the project, they will be kept on the project as staff.',
'created_at' => '2013-02-26 17:40:36.000000',
'question_id' => 543,
),
82 => 
array (
'answer_text' => 'The needed software is VE-Pro ($500) which is a cutting-edge suite of building performance simulation tools. Used by leading sustainable design experts across the globe, it creates understanding of the performance impacts of different low-energy design strategies.
Four brochures; one for the existing course and three additional for new ones have to be printed.  For each batch of 500 count $300 is needed.(Total1=$1,200)
Also four hard copies of course books is needed. Each of them will be $75. (Total2=$300) | Total Printing = $1,500
It is completely acceptable that if Green Fund provides the funding for this project, its logo will be on display on the webpage of each course.',
'created_at' => '2013-02-27 00:42:13.000000',
'question_id' => 540,
),
83 => 
array (
'answer_text' => '1.  Letters can be provided whenever required from the known participants.
2.  They have not yet been asked since the project has not yet been actualized.
3.  No.  Discounts for the vendor used began at 4000 licenses.
4.  The Office of Sustainability and UITS has encouraged us to submit this proposal to your committee.',
'created_at' => '2013-02-28 15:15:38.000000',
'question_id' => 548,
),
84 => 
array (
'answer_text' => 'Hello, the goal is to be operational, harvesting rain water by 30 June, but we request to have a season of use before declaring it complete. We expect completion to be by Dec. 30. Thank you.
Bill',
'created_at' => '2013-02-28 17:33:28.000000',
'question_id' => 547,
),
85 => 
array (
'answer_text' => 'Hello,
I only have the 32 k one.  I don\'t know what the 19 k budget is for so maybe something got copied over electronically or appended to the submissions I made?
So, the 32 k one is the correct one.

If the class were supported, the Green Fund would be acknowledged in any promotional materials developed to recruit students in the course.  The fund would also be aknowledged in the syllabus as the source of funds.  Any pedagogical publications on the development or offering of the course would also contain acknowledged.

Let me know if you have any other questions,
Paul',
'created_at' => '2013-03-01 21:40:50.000000',
'question_id' => 556,
),
86 => 
array (
'answer_text' => '1. Yes, I am confident I can secure the letters of support from participating departments.

2. Yes, the total amount the project needs is $5400

3. I apologize, I do not need money for travel, I am not sure where it appears in the proposal.

4. If proceeds are to be donated, they would be donated to the UA President\'s Fund for Excellence- only. Yes, the project can keep a pportion of the 
proceeds and use the collections for future project growth. -OR- in year one, all proceeds can stay to support the project and the donations can start year 2.

5. The clarification of support from the Green Fund would be part of every promotional vehicle, ie. FB, eblasts, flyers, emails, promo materials and
we would create a sign that would be prominent at every workshop outlining the project and naming the source of the funding. The promo sign would be present at every event where products were sold, and a promo card about the project and the source of funding will be included with every product.',
'created_at' => '2013-03-01 22:00:31.000000',
'question_id' => 553,
),
87 => 
array (
'answer_text' => 'As with our previous awards for our Externship program from the Green Fund, we promote the Fund in any press releases about any one particular project, and have budgeted funding for signage for each project. In addition, we have an article approved and awaiting publication about our program in the Journal of Extension and one already published in the Journal of Sustainability Education (see: http://www.jsedimensions.org/wordpress/content/the-accidental-sustainability-agent_2013_02/) that highlights the UA Green Fund. Moreover, we have had a news piece about the Green Fund and our program generated and published on the UA Environment and Sustainability web portal (see: http://portal.environment.arizona.edu/content/students-extension-agents-team-across-arizona). We will continue to take advantage of any opportunity to promote the UA Green Fund as the engine behind our Externship program.',
'created_at' => '2013-03-02 02:23:15.000000',
'question_id' => 554,
),
88 => 
array (
'answer_text' => 'We will discuss with our students the economic impact of providing welding training to students in educational institutions. This will include the expense of purchasing and preparing steel coupons, welding electrodes & shielding gas, personal protection equipment (PPE), potable water used to cool hot metal, as well as electricity to power welders, and fume extraction. The key to successful welding skill development is repetition and practice and with it building self-confidence in the student\'s ability to perform quality welds. Students new to welding may experience anxiety about going into the laboratory welding booth for the first time. We hope to measure this and observe the effect of simulator training before students actually go into the booth for the first time.  will have a plaque made and posted in our teaching laboratory in the location of the welding simulator letting all students know that funding for the welding simulator was made by the UA Green Fund Committee in support of our effort to promote sustainable energy technology.',
'created_at' => '2013-03-02 15:40:15.000000',
'question_id' => 552,
),
89 => 
array (
'answer_text' => 'In our public presentations we will discuss and compare the benefits and costs of traditional diesel versus biodiesel. We hope to show or demonstrate that there is little difference in energy potential between the two types of fuels. We also will have poster boards constructed to display with our trailer where support for our project comes from.',
'created_at' => '2013-03-04 17:28:51.000000',
'question_id' => 564,
),
90 => 
array (
'answer_text' => 'If this Green Fund project were to be funded I would promote the Green Fund as the financial supporter in several ways. Handouts, PowerPoint presentations, and any other written document would all contain the Green Fund Logo. I also plan to organize and work a UA Road Show wherein I show people who purchase items green alternatives. I would promote the Green Fund during the shows by word of mouth as well as by print medium.',
'created_at' => '2013-03-04 17:53:56.000000',
'question_id' => 563,
),
91 => 
array (
'answer_text' => 'As a finished product, the photovoltaic trainers should demonstrate how energy from the sun is converted to both Direct Current (DC) and Alternating Current (AC). Students will be able to measure energy produced using multimeters. Signs with Green Fund logo and a statement about funding for construction of each trainer will be mounted on each trainer.',
'created_at' => '2013-03-04 19:47:03.000000',
'question_id' => 566,
),
92 => 
array (
'answer_text' => 'This project will provide opportunities to acknowledge UA Green Fund as the funding source in various ways. As described in the proposal, the project will include the formation of a committee (PLITE) to evaluate environmentally friendly plant lighting technology and practices and through the surveys the committee plans to conduct, to communicate with our university community, we will use the UA Green Fund name/logo and link to the Green Fund site wherever possible. The committee, consisting of Plant Sciences faculty, staff and student members, will develop an information website for disseminating the project findings and plant growth chamber user guidelines. The website will be linked appropriately to the UA Green Fund website. We will also have opportunities to communicate with stakeholders and peers outside the university in professional meetings and working groups. For such occasions, UA Green Fund will be acknowledged as the funding source for related work. Research publications will mention UA Green Fund and the formal project number under acknowledgement sections. Finally, where appropriate, signage acknowledging the Green Fund will be affixed to any chamber receiving upgrades as a result of UA Green Fund support.',
'created_at' => '2013-03-04 20:27:02.000000',
'question_id' => 555,
),
93 => 
array (
'answer_text' => '1. Through the online course materials for the class.
2. By making postings on our Departmental website.

Our Fuels, building materials, clothing, plastics, paints, electronic components, and medicines are all examples of materials we depend on in our modern world.  Our understanding of chemistry has enabled us to have these materials, and now must be used to develop new processes that continue to meet our demands, yet reduce negative environmental impacts.  Additionally, chemistry will play an important role in solving many of the environmental problems that we currently face.

1. The educational impact of these new laboratory experiments will be felt by a large number of students.  More than 1000 students will perform these new experiments every year, as part of their Organic Chemistry coursework.  One of the learning goals for the new experiments is for students to gain a better understanding of the impact of chemical processes on the environment.   The online laboratory materials for these experiments will explain the direct benefits of waste reduction in these specific experiments, leading to a broader discussion about the ideas of “Green Chemisty”.  The UA Green Fund will be promoted and acknowledged for its financial support of the development of this aspect of our program.

2. Acknowledgement of the Green Fund\'s financial support will be prominently posted in the "Department News" area of the Department of Chemistry and Biochemistry\'s main webpage (http://www.cbc.arizona.edu/).  Our Department is quite large and many students, faculty, and staff regularly view that page.',
'created_at' => '2013-03-04 20:32:22.000000',
'question_id' => 551,
),
94 => 
array (
'answer_text' => '1. Through the online course materials for the class.
2. By making postings on our Departmental website.

Our Fuels, building materials, clothing, plastics, paints, electronic components, and medicines are all examples of materials we depend on in our modern world.  Our understanding of chemistry has enabled us to have these materials, and now must be used to develop new processes that continue to meet our demands, yet reduce negative environmental impacts.  Additionally, chemistry will play an important role in solving many of the environmental problems that we currently face.  

1. The educational impact of these new laboratory experiments will be felt by a large number of students.  More than 1000 students will perform these new experiments every year, as part of their Organic Chemistry coursework.  One of the learning goals for the new experiments is for students to gain a better understanding of the impact of chemical processes on the environment.   The online laboratory materials for these experiments will explain the direct benefits of waste reduction in these specific experiments, leading to a broader discussion about the ideas of “Green Chemisty”.  The UA Green Fund will be promoted and acknowledged for its financial support of the development of this aspect of our program.

2. Acknowledgement of the Green Fund\'s financial support will be prominently posted in the "Department News" area of the Department of Chemistry and Biochemistry\'s main webpage (http://www.cbc.arizona.edu/).  Our Department is quite large and many students, faculty, and staff regularly view that page.',
'created_at' => '2013-03-04 20:33:17.000000',
'question_id' => 560,
),
95 => 
array (
'answer_text' => '#1: The estimated Return on Investment of this program pending on numbers generated by the simulation. My guess is that it would save in the 2-3% range on the use. This proposal will provide baseline data for future airlock retrofits on more campus buildings which would be a large amount of saving for the whole campus.  
#2: If the project is approved for funding, building Directors in Collaboration with Facilities Management will be involved.
#3: According to the Department of Energy, Institution buildings typically consumes annually 180-220 KBTU/ft², this is about $6.4/ft².  For an average savings of 3% a $0.19 energy cost savings is achieved.  If the building is 40,000 ft² the savings is estimated at $7,730 annually.
#4: An information board can be placed at the entrance of the building that explains the strategy, the savings, and the project sponsors i.e. Green Funds.',
'created_at' => '2013-03-04 20:56:17.000000',
'question_id' => 550,
),
96 => 
array (
'answer_text' => 'After talking to Julia Rudnick, I wanted to get an answer in here before the UA Green Fund meets today (March 5) -- but I will do further research to supplement my answer before the March 9 deadline indicated on the email message containing the question. 

Using reclaimed water for trees is generally a recommended practice, as it contains the important fertilizer nitrogen. It can be problematic to apply reclaimed water or compost from human waste to crops that produce food near the ground, where edible portions might touch the reclaimed material, and research in the University of Arizona\'s Department of Soil, Water and Environmental Science suggests application of compost from human waste should not be done within 30 days of potential harvest even if the edible portion does not touch the ground. (For instance, please see the story and related story by Ruth Hook, one of the students in my 2011 writing class.) The situation may be different for reclaimed water, however.  

Also, trees seem to be in a different category because the edible portions that we would be harvesting are so high off the ground. Research by H. Al-Hamaidedeh and M. Bino (2010) found that the chemical properties of irrigated olive trees were not affected by application of greywater in a project in Jordan. Again, reclaimed water might be somewhat different, and we will need to explore that question more thoroughly.

Incidentally, this is exactly the sort of question we would expect the student interns to investigate before recommending harvesting of specific food trees. In the short term, however, I will do some additional preliminary research to provide more details on this question. 

Many thanks for your consideration of our project.    

Hook, Ruth, 2011. UA researchers find treated biosolids safe as fertilizers. http://ag.arizona.edu/swes/environmental_writing/stories/2011/hook.htm 

Al-Hamaiedeh, H., and M. Bino, 2010. Effect of treated grey water reuse in irrigation on soil and plants. Desalination 256 (1-3) 115-119.',
'created_at' => '2013-03-05 19:19:17.000000',
'question_id' => 567,
),
97 => 
array (
'answer_text' => 'We can promote the positive environmental impacts of the camp through several ways.  First, the immediate impact of this green retrofit project at the James 4-H Camp would be realized in the number of gallons of diesel saved through solar improvements and the number of gallons of water saved through water harvesting improvements. We plan to erect educational signs indicating that—at the site of the current generator and at the dining hall bulletin board which every camper has a chance to read as they wait in line for meals. UA Green Fund support will be displayed prominently on these educational exhibits with a link to your website and your logo. Second, we plan to market the camp using this information to others who might be concerned about the environment.  Publicizing the environmentally-friendliness of the camp through mailings, website, and our Facebook page will help others understand how we can “walk the talk” of being sustainable.  The UA Green Fund would be prominent in these communications as the source of the retrofit that made this all happen.  Finally, those who visit the camp, especially the campers, student researchers, and UA faculty, would experience the comfort and uniqueness of living “off-the-grid” and we hope to capture their thoughts of how that feels and what they think about retrofit projects in general to provide feedback to the UA Green Fund. We have approximately 1200 youth at the camp each summer; many of them are Arizona 4-H members and we consider their 4-H experience to be their First Class at the U of A. What better way to start off your university experience than with an environmentally-sustainable week at a University of Arizona/Green Fund supported camp?

Because of our satellite location in the Prescott, AZ area and representation on the UA campus, we are positioned to extend the promotion of the camp’s environmental impact successes across a wide geographic area.
●	After our first year of camp ownership, we are beginning to network with the approximately 20 other youth camps in the Prescott area. Most camps, because of their remote locations, stand to benefit from sustainability-focused retrofits.  We will be sure to include the UA Green Fund as the major sponsor.
●	Our camp and the surrounding communities of Prescott, Prescott Valley, Jerome, and Cottonwood on both sides of Mingus Mountain are all part of the Upper Verde River Watershed community. By decreasing our environmental impact, especially the amount of water used at camp through water harvesting and smart water usage policies and education, we will be helping in the preservation of Arizona’s only Wild and Scenic River - the Verde. There are hundreds of organizations and individuals that stand to benefit from this outcome, including government agencies, wildlife protection groups, and the agricultural community. Using university research on sustainability practices, we would share the positive outcomes of green retrofitting in areas of water concern with the local community.
●	These positive outcomes would also be integrated into camp curriculum through educational signage at each green retrofit project, as well as, through camp lesson plans and activities.
●	At the UA, there are many paths of promotion. We would supply updates and outcomes of our retrofit project through the CALS newsletter and university media outlets—especially UA News. There are also opportunities for publication of information related to a camp green retrofit in the Journal of Extension and through publications related to the 4-H State and National communities.

Due to the need to keep potential camp clients (renter’s) and youth participants and their families connected and informed about camping programs, the reach of camp-related information is also statewide. The content for our marketing and outreach efforts, as well as, educational content at camp and in the form of Extension-published factsheets, would all be tagged with the line “The James 4-H Camp & Outdoor Learning Center - A green camp made possible through the UA Green Fund”. We would also emphasize that we are an extension of a green campus at UA.

All of our camp information, updates, and news are distributed through social networks including Facebook, Twitter, and a camp blog. 

Our Facebook: https://www.facebook.com/James4HCamp
To share with UA Facebook: https://www.facebook.com/uarizona

Our Twitter: https://twitter.com/James4HCamp
To share with UA Twitter: https://twitter.com/UofA

Our new blog: http://az4hcamps.blogspot.com/
To include updates on retrofit project installation, use, environmental impacts, and more.',
'created_at' => '2013-03-05 20:32:14.000000',
'question_id' => 565,
),
98 => 
array (
'answer_text' => 'The following are answers to the three questions asked by the Green Fund.  
1.) I have located a production site in which I can make the biochar.  I will produce the biochar at the ERL (Environmental Research Laboratory) facility at the airport.  The ERL is operated by the SWES (Soil Water and Environmental Science) Department.  There is a biochar oven located at this facility.  
2.) If this project is funded, I will be able to obtain letters of support from the departments who participate in this project.  I will have these letters sent to the UA Green Fund before any funds are transferred.  
3.) If this project is funded, I will put the Green Fund logo on information that is handed out regarding the project.  I can create informative brochures that could be placed in a box near the location of my plots so that people can take one of these brochures.  I will create plaques that will be placed where my plots are located.  I would also hope that information on this project could be placed on various university websites, and I would try and work with the relevant UA units to get information about this project on their websites.  I would also explore putting information about this project in The Daily Wildcat.',
'created_at' => '2013-03-05 20:57:49.000000',
'question_id' => 549,
),
99 => 
array (
'answer_text' => 'If this project is funded I would promote the environmental impact by publishing documents, create informational plaques (similar to the ones in the Underwood Garden) and suggest outdoor classes and seminars.

The Green Fund would be identified as the financial supporter by credit in publications and your name and Logo can be implicated into the physical design of the cool tower.

Thank you for your consideration, 
I look forward to hearing from you

Regards,
Shaun Hallissey',
'created_at' => '2013-03-06 01:36:29.000000',
'question_id' => 561,
),
100 => 
array (
'answer_text' => 'The Green Fund, as the financial supporter, will be promoted with signage at the garden as well as on the neighborhood website and through social media. The Green Fund\'s financial support will be announced at the Rincon Heights Neighborhood Associations\' quarterly meeting. Likewise, the neighborhood will be inform our city counsel members\' office of the Green Fund\'s support should funding be awarded. Ward six regularly publicizes events of significance occurring in the area. The neighborhood could also inform local media contacts of the Green Fund\'s financial support should funding be awarded.
We will use varied means to promote the environmental impact of the project. We will promote the environmental impact on our website as a way of educating neighborhood residents, UA students and the public and to recruit more gardeners. We will promote the projects impact by educating neighborhood residents about the impacts of sustainable living that the garden offers at neighborhood meetings, in articles in the neighborhood newsletter, on the website and social media. More importantly, the garden committee will continue to involve academic partners from the UA as part of the long-term strategy to promote the environmental impact of the project. Lastly, the garden committee will work to see that the project is executed and run in a sustainable manner as a way of ensuring longevity and thereby promoting its environmental impact into the future.',
'created_at' => '2013-03-06 03:08:37.000000',
'question_id' => 562,
),
101 => 
array (
'answer_text' => 'To promote the environmental impacts of this project, we have several instructors, including Dr. Paul Blowers and Dr. John Pollard, on board to develop homework and/or test problems that would use the data gathered from the golf carts. This would give exposure to the project from an undergraduate audience. After analyzing the results of monitoring the carts, SolarCats would contact the Daily Wildcat about running a story on the project and also contact ASUA about reporting the data and findings on their website.
To identify the Green Fund as the financial supporter of the project, SolarCats could discuss with Parking and Transportation about getting a sticker put on the retrofitted golf carts saying the carts are powered from a project funded by the Green Fund.  The club could also speak with the professors who plan to use the data for class problems to see if they are willing to include either the Green Fund logo or a note that the project from which the data was collected was sponsored by the Green Fund.',
'created_at' => '2013-03-07 02:57:09.000000',
'question_id' => 557,
),
102 => 
array (
'answer_text' => 'The system incorporates on line management platforms(reports)that gernerate carbon offsets and CO2 reduction that can be utilized for sustainability reporting by the University. PTS can use this information to submit articles and news feeds to media outlets as well as the UA Sustainability office and their website, UA students for Sustainability and UA Eco Ops website. This information would also be posted on social media like Facebook and Twitter. PTS would also incorporate this information into the yearly UA Travel Reduction Plan submitted to Pima Association of Government. In addition to placing verbiage that this project was funded by Green Funds in every marketing venue we print or post, we would also display the Green Fund Logo on all of our articles and media outreaches(including social media) as well as display the GF logo at each of the five bike station locations (as well as place the logo on each bicycle funded by the project). PTS\'s full time marketing manager would ensure the promotion of the environmental impact of the project as well as identifying the Green Fund as the financial supporter of the project.',
'created_at' => '2013-03-07 17:23:45.000000',
'question_id' => 559,
),
103 => 
array (
'answer_text' => 'December 30, 2013 would be my requested end date in this PAR',
'created_at' => '2013-04-23 20:53:45.000000',
'question_id' => 568,
),
104 => 
array (
'answer_text' => 'I think June 30th, 2014 will give us the best opportunity to make the project self-sustaining for continued opperation.',
'created_at' => '2013-04-23 21:07:19.000000',
'question_id' => 571,
),
105 => 
array (
'answer_text' => 'Hi, we are awaiting a meeting from Facilities Management and Planning and Design to address the schedule of the project, as we are not aware of all of their ongoing projects and potential conflicts that might dictate the schedule of work. I was requesting December 30, 2013 but if it makes more sense to request June 30, 2014 to be safe and keep to a typical Green Fund project cycle we can use that date. Thank you.',
'created_at' => '2013-04-23 21:39:07.000000',
'question_id' => 569,
),
106 => 
array (
'answer_text' => 'Hi, we are awaiting a meeting from Facilities Management and Planning and Design to address the schedule of the project, as we are not aware of all of their ongoing projects and potential conflicts that might dictate the schedule of work. I was requesting December 30, 2013 but if it makes more sense to request June 30, 2014 to be safe and keep to a typical Green Fund project cycle we can use that date. Thank you.',
'created_at' => '2013-04-23 21:39:21.000000',
'question_id' => 570,
),
107 => 
array (
'answer_text' => 'Maria Galo in the PTS department has been contacted about making a sub account for GF 14.01 and Julia has been cc\'d on the email.  A follow up email will be provided, if necessary, to give further information about the sub account number to Julia and the Green Fund.

-Kyle Marshall 
SolarCats Treasurer',
'created_at' => '2013-07-11 03:38:01.000000',
'question_id' => 572,
),
108 => 
array (
'answer_text' => 'I am awaiting documents from business office and will submit as soon as I have them.
Thanks,
Bill',
'created_at' => '2013-07-29 18:30:01.000000',
'question_id' => 580,
),
109 => 
array (
'answer_text' => 'I am awaiting documents from business office and will submit as soon as I have them.
Thanks,
Bill',
'created_at' => '2013-07-29 18:30:14.000000',
'question_id' => 585,
),
110 => 
array (
'answer_text' => 'We received an extension of the project to December 31, 2013. We are currently testing the last time point of storage, the 24 months time point, which will be analyzed in October 2013. Upon collecting the last data sets, we will compare and analyze all data sets for the final report.',
'created_at' => '2013-07-30 23:43:01.000000',
'question_id' => 581,
),
111 => 
array (
'answer_text' => 'The final report will be submitted using the GreenFunds site.

Murat Kacira
Project PI',
'created_at' => '2013-08-01 03:13:40.000000',
'question_id' => 578,
),
112 => 
array (
'answer_text' => 'Yes, I will be able to attend. I appreciate a chance to speak with the UA Green Fund committee directly to talk briefly about the project and answer any questions.',
'created_at' => '2014-01-29 23:21:01.000000',
'question_id' => 592,
),
113 => 
array (
'answer_text' => 'I appreciate the invitation to meet and discuss our project proposal. Unfortunately, I will be unable to attend as I will be out of state Feb 10-13 participating in a solar workshop. Our workshop is scheduled to end each day at 5:00 pm. I will be back in Tucson on a Friday.',
'created_at' => '2014-01-29 23:39:13.000000',
'question_id' => 600,
),
114 => 
array (
'answer_text' => 'Yes, I would be happy to speak to the Green Fund Committee at that time. Since about 70% of the requested funding is for student fabrication (labor) of the Green Roof components, may I bring a student along to assist in answering questions?  If I do not hear back from you I will assume that I am to come alone.

Ron Stoltz',
'created_at' => '2014-01-29 23:43:58.000000',
'question_id' => 608,
),
115 => 
array (
'answer_text' => 'Yes, I will attend the meeting.',
'created_at' => '2014-01-29 23:51:16.000000',
'question_id' => 610,
),
116 => 
array (
'answer_text' => 'Hi,

I would very much like to attend the meeting.  However, I am the instructor for a U of A chemistry lecture class that meets on Tuesdays and Thursdays from 5-6:15 p (followed by a discussion section from 6:20-7:20 p on those days
). So I cannot possibly attend my scheduled time slot.  If the schedule is completely inflexible, can I send a representative? (the person I have in mind is not listed as being part of the project)

Thank you.',
'created_at' => '2014-01-30 00:05:21.000000',
'question_id' => 617,
),
117 => 
array (
'answer_text' => 'I will be attending this meeting.',
'created_at' => '2014-01-30 01:55:15.000000',
'question_id' => 613,
),
118 => 
array (
'answer_text' => 'Sure. I\'d be happy to speak with the committee Tuesday afternoon. 
Best regards,
Tanya',
'created_at' => '2014-01-30 02:33:44.000000',
'question_id' => 590,
),
119 => 
array (
'answer_text' => 'I will be happy to attend this meeting. Please let me know if I am able to bring the students and the community partners we hope to work with on this project. Thank you!',
'created_at' => '2014-01-30 03:42:07.000000',
'question_id' => 607,
),
120 => 
array (
'answer_text' => 'I will gladly take the opportunity to speak to the Green Fund committee February 25th at 5:20pm.',
'created_at' => '2014-01-30 05:01:00.000000',
'question_id' => 615,
),
121 => 
array (
'answer_text' => 'Hello,
Thank you for the invitation. Yes, I will be able to make this time. Is it ok to invite the other project coordinators to join me?
Best,,
Sarah',
'created_at' => '2014-01-30 06:17:46.000000',
'question_id' => 604,
),
122 => 
array (
'answer_text' => 'Yes - I will be able to attend this meeting.',
'created_at' => '2014-01-30 15:25:50.000000',
'question_id' => 598,
),
123 => 
array (
'answer_text' => 'Yes, I will be attending.',
'created_at' => '2014-01-30 16:32:21.000000',
'question_id' => 599,
),
124 => 
array (
'answer_text' => 'I will be in attendance for the 5 minute meeting on 2/11. Thank you for the opportunity.',
'created_at' => '2014-01-30 20:31:01.000000',
'question_id' => 601,
),
125 => 
array (
'answer_text' => 'Dear the Committee,
Yes, we definitely will be there. 


Kind Regards,
Elmira Shojaei',
'created_at' => '2014-01-30 20:52:46.000000',
'question_id' => 611,
),
126 => 
array (
'answer_text' => 'Thank you Julia,

I will be coming with one CAPLA student.

Ron',
'created_at' => '2014-01-30 22:52:17.000000',
'question_id' => 622,
),
127 => 
array (
'answer_text' => 'Thanks, Julia. I may bring a student who worked on the grant with me, or come by myself.',
'created_at' => '2014-01-30 23:03:47.000000',
'question_id' => 621,
),
128 => 
array (
'answer_text' => 'Thank you for your generous offer to allow the Institute of the Environment a few moments to talk with the Green Fund Committee about the Green Engagement Guide proposal. Unfortunately, we will not be able to join the Committee on Feb 11 from 5:40-5:45. Should any questions arise during the review process, please feel free to email/call and we\'ll happily endeavor to clarify our thoughts.',
'created_at' => '2014-01-31 16:53:58.000000',
'question_id' => 603,
),
129 => 
array (
'answer_text' => 'Julia,
The spreadsheet is correct.  There will be no equipment over 5,000 in that budget.  

thanks,
James',
'created_at' => '2014-01-31 17:16:03.000000',
'question_id' => 589,
),
130 => 
array (
'answer_text' => 'Dear Review Committee

I will be able to attend the meeting.

Dr. Ogden',
'created_at' => '2014-01-31 18:33:52.000000',
'question_id' => 606,
),
131 => 
array (
'answer_text' => 'Yes, I will attend. Thank you.',
'created_at' => '2014-01-31 22:21:56.000000',
'question_id' => 618,
),
132 => 
array (
'answer_text' => 'Hi Julia - I\'m not sure where the $135,600 is coming from, but it should be the $115,763 for three years. I just double checked my final proposal and my budget spreadsheet and they are all $115,763. Hope this helps. Mark',
'created_at' => '2014-02-02 01:41:33.000000',
'question_id' => 624,
),
133 => 
array (
'answer_text' => 'Yes, Tuesday Feb 4, 2014, 5:25-5:30, El Portal Ocotillo Conference Room.',
'created_at' => '2014-02-03 15:25:49.000000',
'question_id' => 626,
),
134 => 
array (
'answer_text' => 'Yes.  I will attend this meeting.',
'created_at' => '2014-02-03 15:43:19.000000',
'question_id' => 627,
),
135 => 
array (
'answer_text' => 'Yes.  I will attend this meeting.',
'created_at' => '2014-02-03 15:44:51.000000',
'question_id' => 594,
),
136 => 
array (
'answer_text' => 'Good morning Green Fund,

I will be attending this meeting on 2/18/14 from 5:50-5:55pm.

Thank you for the opportunity,

Kristin Wisneski-Blum',
'created_at' => '2014-02-03 15:54:14.000000',
'question_id' => 612,
),
137 => 
array (
'answer_text' => 'Yes Committee members I will attend this meeting.  Thank you, see you soon

Shaun Hallissey',
'created_at' => '2014-02-03 16:00:11.000000',
'question_id' => 632,
),
138 => 
array (
'answer_text' => 'This notice is to confirm that I will be attending the scheduled meeting regarding our recent proposal submission on  Tuesday, February 4, 5:50-5:55 pm.  We are so excited about the prospect of continued funding for our program, which has now developed into an increasingly recognized program hear at the UA: the MycoCats.  The vision of the MycoCats is both practical in its application, recycling using mushrooms, and far-reaching in its vision, to educate and energize students and the community about the importance of mushrooms (and all fungi) for nutrient/substrate recycling in the environment, as a nutritious component of our diet and health living, and for the development of a more sustainable society.  Considering the collective value mushrooms/fungi provide, our vision also includes the development of the MycoCats into a self-sustaining economically viable green enterprise.  The Green Fund support is highly valued and we acknowledge this support in all of our many outreach activities, informational news spots, and publications.  I look forward to speaking with the committee on Tuesday.',
'created_at' => '2014-02-03 18:17:57.000000',
'question_id' => 597,
),
139 => 
array (
'answer_text' => 'I will send a representative to my meeting time.',
'created_at' => '2014-02-03 20:39:05.000000',
'question_id' => 623,
),
140 => 
array (
'answer_text' => 'Yes, I will be attending the committee meeting on February 11th.',
'created_at' => '2014-02-03 23:04:30.000000',
'question_id' => 630,
),
141 => 
array (
'answer_text' => 'Green Fund Committee,

I would like to thank you very much for considering the Bear Down - Draft Tower Green Fund proposal for funding, and allowing me this opportunity to speak with you to answer any questions. I will be attending this meeting on Tuesday February 18th at 5:25 along with my faculty project manager Professor Colby Moeller.

Thank you,
Shaun Hallissey',
'created_at' => '2014-02-03 23:11:50.000000',
'question_id' => 609,
),
142 => 
array (
'answer_text' => 'Thanks for the invitation to speak to the committee. Unfortunately I will not be able to attend at this time because of a prior engagement. 

Diane Austin',
'created_at' => '2014-02-04 02:34:21.000000',
'question_id' => 631,
),
143 => 
array (
'answer_text' => 'I will attend or will send Taylor Sanders, who is currently the student manager of the UA Community Garden.',
'created_at' => '2014-02-04 15:39:01.000000',
'question_id' => 628,
),
144 => 
array (
'answer_text' => 'Thank you for your invitation to today\'s proposal review from 5:10-5:15 pm in El Portal Ocotillo Conference Room.  Both Ace Charette and I will be available to give our brief 3 minute presentation and to answer any questions that the group may have.  

Rudy',
'created_at' => '2014-02-04 18:12:39.000000',
'question_id' => 625,
),
145 => 
array (
'answer_text' => 'The timeline stated in the grant proposal has slight flexibility. 

Our intent is to integrate the existing NASEP Program into the work involved with AISES and the Green Fund and NASEP occurs the second week of June. The grant proposal suggested a Train the Trainer event in the third week of May. We have spoken to the vendor that would provide parts for the aquaponics build and feel that this could be shifted to the first week of June. 

In either situation, supplies and staffing would be needed prior to arrival of grant funding. We have a state account that would allow us to make these expenses to start the project and we could work with our Business Manager to reimburse the state account with Green Funds dollars once those are received in August. 

We have contemplated shifting the project\'s timeline in other ways, but we find that these options also straddle multiple fiscal years. Only the "reimbursement option" allows us to keep true to the Aquaponics and Native Seeds project\'s goals and reach.',
'created_at' => '2014-02-04 23:13:03.000000',
'question_id' => 633,
),
146 => 
array (
'answer_text' => 'I believe this is a repeat message and that I have already responded.  Please let me know if I am mistaken.  Thanks.',
'created_at' => '2014-02-05 22:45:39.000000',
'question_id' => 629,
),
147 => 
array (
'answer_text' => 'Course Instructors:
Melanie Lenart/Grant McCormack SWES454-Water Harvesting (~50 students/year); Jim Knight AED297 UA Heritage and Traditions (~500); Barry Pryor PLP305-Plant Pathology (~50); Margaret Livingston LAR420-Plant Materials (~40); Tanya Quist PLS170-Plants and Our World, PLS354-Sustainable Horticulture, PLS235-Introductory Horticulture (~300)

Tracking Use:
Google Analytics is currently used to track visits to the Campus Arboretum website pages and can also be used to track visits to the species description pages by building a custom web script to separate out visits to the site by QR linked mobile user.  

Camera Justification:
The digital camera is needed to capture high resolution images that will populate the species description pages on the website. We have only 1600 of the needed 4000 images. These images create visual impact and extend the educational value of the signs by providing botanical detail that would allow users to identify unmarked plants at other locations. 

Sign Materials:
Certainly. However, I would need to produce a sub-set of the needed signs since the estimate in the proposal provided assumes use of the least expensive sign materials and re-use of existing sign posts. Producing a sub-set of the signs also raises a concern about sign uniformity, which UA External Relations feels reflect poorly on our organization. The last option is to raise additional funds since matching and in-kind contributions already committed in the budget maximize resources available. This may be possible but difficult in a short time frame. I hope the value in the proposal with such a conservative budget and such visibility and tremendous potential for return on investment will merit the full budget requested for sign materials.',
'created_at' => '2014-02-11 00:01:10.000000',
'question_id' => 636,
),
148 => 
array (
'answer_text' => 'Please also see: http://www.arboretum.purdue.edu/purdue-arboretum-explorer/about/ for examples of the benefits of these signs to interpretation of UA Sustainability initiatives. 

Course Instructors:
Melanie Lenart/Grant McCormack SWES454-Water Harvesting (~50 students/year); Jim Knight AED297 UA Heritage and Traditions (~500); Barry Pryor PLP305-Plant Pathology (~50); Margaret Livingston LAR420-Plant Materials (~40); Tanya Quist PLS170-Plants and Our World, PLS354-Sustainable Horticulture, PLS235-Introductory Horticulture (~300)

Tracking Use:
Google Analytics is currently used to track visits to the Campus Arboretum website pages and can also be used to track visits to the species description pages by building a custom web script to separate out visits to the site by QR linked mobile user.  

Camera Justification:
The digital camera is needed to capture high resolution images that will populate the species description pages on the website. We have only 1600 of the needed 4000 images. These images create visual impact and extend the educational value of the signs by providing botanical detail that would allow users to identify unmarked plants at other locations. 

Sign Materials:
Certainly. However, I would need to produce a sub-set of the needed signs since the estimate in the proposal provided assumes use of the least expensive sign materials and re-use of existing sign posts. Producing a sub-set of the signs also raises a concern about sign uniformity, which UA External Relations feels reflect poorly on our organization. The last option is to raise additional funds since matching and in-kind contributions already committed in the budget maximize resources available. This may be possible but difficult in a short time frame. I hope the value in the proposal with such a conservative budget and such visibility and tremendous potential for return on investment will merit the full budget requested for sign materials.',
'created_at' => '2014-02-11 00:02:43.000000',
'question_id' => 636,
),
149 => 
array (
'answer_text' => '1. We plan to implement a zoned drip system, calibrated for the type of plants in each zone. Since the whole garden site slopes down towards the proposed herb and pollinator zone, we will reduce water use because of runoff flowing into it, and we will include moisture sensors in the system so irrigation is triggered only when needed. Thus, we are proposing a water-efficient system. In connection to this point, we would like to add that the $3500 annual water bill is probably an over-estimate of the irrigation cost. We arrived at this number to account for the initial set-up of irrigation. Once plants establish large root systems and our system is fine-tuned we may be able to substantially reduce the water bill through water efficiency.

2. In e-mail correspondence (Feb. 7), Professor Livingston has promised the commitment of two graduate students who will meet with our garden committee sometime during the week of Feb. 10. These students will be available during the summer, as planned on our project timeline.',
'created_at' => '2014-02-11 04:24:20.000000',
'question_id' => 635,
),
150 => 
array (
'answer_text' => '1a.  The national AISES organization has provided travel scholarships for their national conference in the past.  However, we understand from the organization that those scholarships have and will continue to decrease over the next few years due to the national organization\'s funding cuts.  For example, last year considerably fewer AISES students received travel scholarships as compared to previous years.

1b.  Our requested funding is meant to fund undergraduate travel to the AISES conference.  For our graduate student, we believe we might be able to receive funding from the Graduate and Professional Student Council, who funds such opportunities for graduate students.

2a.  The aquaponics project as it is described is a project with many sustainable components.  We believe the six aquaponic stations that will be built, 2 on-campus and 4 in the community, will serve each site for multiple years.  We also believe that the partnerships that we can establish through the grant between the University campus, aquaponics faculty, our sites, and the broader community all have sustainable benefits.

2b.  We believe that "sustainability" is an important topic to include as we work with the UA chapter of AISES and in our work with our Native American Science & Engineering Program.  Funding is always important, but we believe that we can sustain the topic of "sustainability" in our work with these two groups.  The level to which this is allowed, may be limited to budget.  However, we believe our proposal to be relatively low cost, so we would hope that other funding industry or community funding sources might allow us to continue to expand our focus in working with these students.  Our office has been fortunate to develop community, tribal and federal partnerships that we hope will allow for expansion.

2c.  As noted in our proposal, matched dollars would come from staff positions that would put time into the initiative, as well as NASEP support from the College of Optical Sciences. Details of these contributions are in the "Other Projected Sources of Funding for Aquaponics and Native Seeds."  These matched dollars account for 25% of the overall cost of the project.

3a&b.  Each site has been selected because of the opportunity for integration into the educational curriculum of the site.  We have both site leaders and site managers.  We believe site leaders will assist us in sharing the relevance and importance of the aquaponic systems to the community, while the site managers will assist with integration into the educational curriculum (math, science, health) and the specific cultural group.  K-12 students will be engaged with the maintenance of the systems that includes near daily fish feedings; measurements of water quality on a weekly or bi-weekly basis, depending on the density of fish in the tank, and food harvesting as needed.  

3c.  The systems will be kept at each site beyond the first year.  Each site understands that we believe that the aquaponics systems could have a 3-5 year life cycle, with minimal maintenance.  We are encouraging an economic model that could help each site create their own "Green Fund" to allow for system maintenance or expanded sustainability efforts at each school.

3d.  Aquaponic system disposal has been investigated.  Local recycling centers have been identified that would accept the steel frame of the units, and the HDPE plastic liner would also be recyclable.

4a.  The Office of Early Academic Outreach, since 1984, has developed events and programs meant to attract high school students to higher education.  The office\'s mission is to increase the number of low-income, minority and first-generation college bound students aspire toward and are eligible to enroll in a university-degree program.  In our Native American Science and Engineering Program alone, near 40% of high school senior participants have mentioned that they have applied and are admitted to The University of Arizona.  Many have received scholarships and we are encouraging them to apply for federal financial aid.

4b.  Early Academic Outreach is working with the Student Affairs assessment office to further improve  assessment of our EAO programs to tell the story of our program, our work, and our impact.  For example NASEP students last year reported average increases of 55-58% in their presentation/public speaking skills, research & critical thinking skills, and networking & professional skills.  These are measurements of engagement.  Near 50% of NASEP high school senior participants self-report that they have applied and been admitted to The University of Arizona.  This is a measurement of attraction to higher education.  Many have also received scholarships and we are encouraging them to apply for federal financial aid. 

Our proposal did not speak, however, to the increased interest and engagement in higher education at the four community sites – Ha:San Preparatory and Leadership School, Hiaki High School, Amphitheater’s Native American Education Program and Sunnyside’s Native American Education Program.  We would like to share an exciting example of how the Green Fund is already impacting students interest and engagement in higher education.

- Ace Charette meets with Dustin Williams at Ha:San Preparatory and Leadership School and secures commitment for school to participate in Green Fund partnership.  Ace offers to provide college preparation workshops and date is set for presentation to juniors and seniors at the school.
- An opportunity opens up for a school to participate in an on-campus Raytheon MathMovesU event.  Ace reaches out to Dustin to offer Ha:San the opportunity.  Dustin confirms that Ha:San will participate.
- On Wednesday, February 12, Rudy McCormick and Ace Charette facilitate Planning & Paying for College sessions for juniors and seniors.  Ryan Huna Smith, school counselor, asks if we can do similar presentation for freshmen and sophomore students.  We agree.  Date has yet to be set.
- On Thursday, February 13, 21 students and 2 chaperones participated in the Raytheon MathMovesU event.  Students were led by Raytheon engineers (a 5 student to 1 engineer ratio) through the complex building of a Galileoscope telescope.  Students were able to keep both the telescope and gained a tripod as an educational instrument that they now own.  Students had a chance to ask questions of a panel of academic deans, Jeff Goldberg (Engineering) and Elliot Cheu (Science), Raytheon engineers, and current engineering undergraduates.  At the event, we introduced school staff and students to members of the National Optical Astronomy Observatory (NOAO).  NOAO staff have offered to host a Star Party for the Ha:San participants at the Tohono  O’odham Community College in Sells, Arizona.  Date has yet to be set.

This is just one unique and exciting example of how the partnerships forged through the Green Fund can have an extensive impact on both higher education interest and engagement of K-12 students not only in NASEP, but also at our four community education sites as well.',
'created_at' => '2014-02-15 01:23:14.000000',
'question_id' => 638,
),
151 => 
array (
'answer_text' => '1. How will IE maintain the guides and keep them updated? 
IE already maintains the Green Course Guide twice a year by updating the class listings for the forthcoming semester. IE will maintain the currently-in-development Green Degree Guide in much the same fashion - checking in with colleges, departments, etc. on a regular basis to see if information has changed. Career Services maintains the Green Career Guide. For the Green Engagement Guide covered in this proposal, the process is purposefully fluid at this time. As noted in the proposal, IE hopes to allow the guide to be self-sustaining. However, depending on feedback and both technical and programmatic developments that arise during the GEG\'s creation, the method may be altered to best suit the needs of the Guide.

2. Who would be in charge of maintaining and updating the guide?
As noted in the proposal, staff members at the Institute of the Environment will maintain and update the Green Engagement Guide as they do with the current guides.

3. How would IE moderate postings and requests by community members?
Technical moderation and control will be performed via the capabilities of Drupal (the website basis for all UA websites). This might typically be via an IE staff member reviewing and then allowing non-spam posts to appear on the GEG. Programmatically, IE will determine what level of content control to exert according to the feedback from constituents during the Guide\'s creation and updated according to the real-time usage once the GEG is in place.

4. Please explain in further detail how IE plans to measure the impact of these engagement guides? Will you track the number of people visiting the site, posting to the site, and/or the number of students that get jobs/internships from this site?
Currently the plan is to get qualitative examples, or stories of success, for marketing of people that have used the guide in order to find engagement opportunities. However, it is unlikely we will obtain quantitative data other than site visits and site posts to gage the success of the Green Engagement Guide. A system to track the benefits gained by students as a result of this guide may be possible in the future as engagement becomes a requirement for graduation, but as of right now there is not a quantitative way to track who uses the guide, and whether it results in jobs/internships for those students.

5. How many people do you anticipate attending the half-day workshop?
Depending on feedback, interest, and logistical challenges, the workshop attendance could range from 15-25 people.',
'created_at' => '2014-02-17 17:23:00.000000',
'question_id' => 639,
),
152 => 
array (
'answer_text' => '1. Good catch! Information was inadvertently left out of the Travel portion of the project budget. Additional planning and logistics have provided additional opportunities for our students.

A three-day, two-night trip includes travel from the UA Campus Agricultural Center on a Wednesday morning to Tonalea, AZ, just north of Tuba City on the Navajo Reservation (approximately a 5.5 hour drive) to visit and tour the rural residence of Mr. & Mrs. Willie and Sylvia Moore; a home that is “Off the Grid”, powered by a battery-based solar system and wind system. The Moore’s engage in rainwater harvesting, and haul their potable water and store it in a cistern beneath their home.  
We will continue our travel  to Kayenta, AZ (approximately 51 miles), and meet with the Mr. & Mrs. Clyde and Elissa McBride, agriculture education instructors, Career and Technical Education (CTE) Director, and FFA Advisors of Monument Valley High School (MVHS). We will meet with student members and officers of the Monument Valley FFA Chapter. This will be an opportunity for UA students to learn about the local instructional program, tour the unique animal health care-related teaching facilities, and learn of the Navajo Indian culture from the MVHS students. We will set up our teaching materials in the facility classrooms and prepare to teach multiple renewable energy lessons including: using tools to measure energy output of PV modules; how hydrogen fuel cells function; principles of micro-hydroelectric energy; biofuel engine demonstration; and wind energy.
On Thursday, our students engage in presentation of classroom lessons to the agriculture science students at Monument Valley High School and any invited guests. Also, this will be an opportunity to meet with MVHS students who express an interest in learning more about attending the University of Arizona. 
Revised budget based on use of UA Motor Pool for use of vehicles, estimated fuel costs, estimated room rates for lodging at Monument Valley Inn (from website: https://www.myfidelio.net/webui/AvailabilitySearch.aspx?chain=IQ&property=USKM). 

VEHICLE RENTALS

Vehicle rental from UA Motor Pool:
1-14-passenger van - $58.96/day x 3 days = $176.88
1-Suburban to tow our cargo trailer - $58.65/day x 3 days = $175.95
UA Motor Pool Per mile Cost: 
12-Passenger Van - $0.33/mile x 825 miles = $272.25
Suburban - $0.28/mile x 825 miles = $231.00

*Estimated round trip: Tucson to Tonalea, AZ, to  Kayenta, AZ and return is approximately 825 miles

VEHICLE FUEL
Fuel for UA Vehicles (3) $3.25/gal 50 gal/vehicle = $487.50
Third vehicle is department pickup to pull trailer, haul student luggage (estimated)

LODGING - Monument Valley Inn:
Lodging for 15 UA Students and two Faculty (two-nights, six rooms, 2 double beds @ Monument Valley Inn, Kayenta ($150.00*/ room X 6 rooms/night x 2 nights) + taxes & fees. = $2176.62

*Rate and fees come from Monument Valley Inn website. No discounts or special rates have been applied.

MEALS:
Meals for UA students  $30.00/day/student X 3 days x 15 students = $1,350.00

Total Travel/Lodging portion of Project Budget: $4,870.20


2. Our Department has been using an off-campus vendor (Enterprise) for our department travel needs as we are charged a day use rate only with unlimited mileage. The budget figures in the travel section are for vehicles from the UA Motor Pool.

3. Good timing! Our Department is a partner on a Department of Energy (DOE) grant with Solar Energy International. As a partner, I as an instructor am afforded the opportunity to take courses (either online or hands-on workshops at the Paonia, CO lab facility) for little or no cost. This a cost-matching value of $799.00 for online courses and $1,499.00 for week-long hands-on lab workshops. I learned just recently that this applies only to the solar-related courses. Unfortunately, the Micro_Hydro course is not covered in this grant. With that being said, I will focus on participating in the battery-based workshops(both online and hands-on lab workshop) this summer. The hands-on workshop is scheduled for Aug 4-8. I would be traveling and attending from August 3 - 9. The DOE grant provides a $500.00 travel stipend. This amount would cover my airfare (US Airways, $498.60 - Tucson to Grand Junction, CO) only.  A rental car would be needed as Paonia, CO is about a 90 minute drive from Grand Junction, plus lodging for a week.',
'created_at' => '2014-02-17 17:31:45.000000',
'question_id' => 640,
),
153 => 
array (
'answer_text' => '1. We understand that the goal of the ticket giveaway is to fill the stands. Are there strategies for a ticket giveaway that will attract 600 new fans to the game rather than regular attendees? 

UA Athletics and the various teams are always trying to attract new fans to our games. We have a number of tools at our disposal to reach new fans. One thing that is important to note is that we have not been given the Fall 2014 soccer schedule yet. Depending on the day and time of the game, as well as the opponent, we might choose to implement any number of the strategies listed below.

1) Use student attendance at other UA sports events to attract current student fans to this “green” soccer game. This captures students who are already invested in UA sports and directs them to this special event by way of the promotional tickets.
2) Give tickets to students as they board the special Cat Tran from campus to the stadium. 
3) Use UA Athletics student interns to promote the game through various on-campus events.
4) Use campus appearances with Tony Amato, Paul Nagy, and Kylie Louw, the Head and Assistant Soccer coaches to promote the game and give tickets to students. 
5) Use the UA Soccer team in their on-campus appearances to reach students and give away tickets to new fans.
6) The video that UA Athletics is producing with the UA Women’s Soccer team (as an in-kind contribution to this grant) will also be used to increase excitement about this special event, and is expected to spur participation.

2. The Green Fund Committee would like to request that the designs for the promotional t-shirts and "green" warmup jerseys be sent to the Green Fund for review. Is review of shirt designs possible within the additional constraints of IMG and UA Athletics?

This is definitely possible. Promotional shirts and warmup jerseys must first meet the UA and IMG design standards. We will design the shirts with the Green Fund logo from the outset, and once preliminary designs have been internally approved, send them to the Green Fund for review.',
'created_at' => '2014-02-17 18:28:13.000000',
'question_id' => 642,
),
154 => 
array (
'answer_text' => '1) How far-reaching do you envision these modules to become? Please elaborate further on how you will disseminate findings that are useful for other youth-led groups.

We envision that these modules will be incorporated into a wide variety of UA-led youth programs and will establish a standard and roadmap for incorporating sustainability into those programs. 
Why do we expect the modules will establish a standard? The modules will be developed to meet the specific needs and circumstances of actual UA-led programs. During the assessment phase, we will learn not only about the goals, objectives, and operations of the youth programs, but we also will learn where and how change might occur. We will tailor the information, actions, and activities so they can be integrated into the programs and will pilot the modules with four programs of different types and durations, and aimed at different audiences, so we can identify where and how to adapt the modules to best serve the campus community.  We will conduct workshops for the leaders and staffs of the programs in order to ensure that they have the information they need to successfully carry out the modules and to learn about the successes and challenges they face in implementing the modules. We will use this information in the design of the modules and of the outreach.

How do we expect to get the information out? During the assessment we also will learn how the leaders and organizers of the youth programs get information, make decisions, and train their staffs. We will therefore target our outreach to reach as many leaders and youth program staff members as possible. We know that we will share information about the modules and the assessment with members of the campus community through at least 3 oral presentations, a written report, and a website, but we also are likely to learn about other opportunities as we engage with the youth program leaders and staff. For example, we will identify any orientation sessions and other mechanisms for reaching program leaders, as well as other websites to which we will link our website. Because BARA’s internship program has been in continuous existence since the 1990s, and most of our initiatives extend over many years, we expect that we will continue to modify, adapt, and share information about the modules and their implementation for years to come.

2) How did you select those 5 campus groups? Are they committed to attending the workshops if this proposal is funded?

We will pilot the sustainability modules with at least four BARA-led youth programs. We selected to do so because BARA operates a variety of youth programs both on and off campus and we can ensure that the leaders and staffs of these programs will participate in the assessments and workshops. We will select two on-campus and two off-campus programs in Month 7 of the project, after we have a good understanding of the range of UA-led youth programs and their needs and circumstances, to ensure that the programs we select to pilot the modules represent that range. Some of these programs will address topics into which broader questions of sustainability can easily be incorporated while others will address what are apparently unrelated topics. Thus, while we cannot specify at this point which programs we will select, we offer the following as possibilities:

1.	High school camp to help Tohono O’odham Nation (TON) youth develop research and study skills. This one-week camp is held off-campus on the TON reservation in conjunction with the Southeastern Arizona Area Health Education Center (SEAHEC) and the TON.
2.	Middle school camp to help refugee and immigrant youth develop knowledge and skills necessary for success in high school. This four-week camp is held on-campus in partnership with UA student organizations and Tucson organizations serving the refugee and immigrant community.
3.	Workshops aimed at middle and high school youth to introduce them to environmental technologies (e.g., rainwater harvesting, solar cookstoves) designed to address specific water, air, energy, and material resource problems in the U.S.-Mexico border region. These half- and one-day workshops are held off-campus at the Centro de Capacitación para el Trabajo Industrial118 (CECATI 118) in Nogales, Sonora in partnership with members of the Asociación de Reforestación en Ambos Nogales
4.	Workshops/activities aimed at middle and high school youth to introduce them to the application of anthropological concepts and principles to address local and regional problems. These ten- to thirty-minute workshops/activities are held at Science City, in conjunction with the Tucson Festival of Books.',
'created_at' => '2014-02-18 18:30:40.000000',
'question_id' => 643,
),
155 => 
array (
'answer_text' => '1. Unfortunately not, the amount made by the power plant will be small and we do not have the equipment to convert the oil into a regulation fuel. The technology exists, but it is primarily done at a petroleum refinery.
2. The computer control system includes 1) a data logger which continuously records temperature, dissolved oxygen, pH, dissolved CO2, and conductivity; 2) the probes to measure each component; and 3) a pH controller system which essentially allows the CO2 from the power plant out gas to enter the algae reactor as it is consumed. The last part includes some valves and switches.

3. I believe that there is $2000 in the budget for this under operations - display boards.',
'created_at' => '2014-02-19 19:35:15.000000',
'question_id' => 647,
),
156 => 
array (
'answer_text' => 'Hello,

We are compiling our answers to the above questions. It doesn\'t seem as though I can submit a pdf via this site, only text response. Can you provide an appropriate email address or additional format for us to provide requested photographs and other images? Thank you!

Linda',
'created_at' => '2014-02-22 21:40:09.000000',
'question_id' => 646,
),
157 => 
array (
'answer_text' => 'Hi Julia and Green Fund Committee,

We are answering your latest questions in verbal and visual format. Attached please find a pdf that:
1.	Shows several images of the alley in full or partial sunlight
2.	Identifies locations for solar exposure, water harvesting, gardening, social space, and culinary programming
3.	Documents recent uses and activities in the alley, some generated by the newly restored façade on the adjacent City Middle School building
4.	Rendered solar analysis that shows shadow and sun patterns at numerous times of the year and times of day
5.	A quick rendering of first thoughts on how gardening, teaching, learning, and gathering might happen in the alley space
6.	An overview of the technology as requested
As you can see in the attached document, the alley receives light throughout the year and throughout the day. In our hot, arid environment, the amount of shade it does receive provides excellent conditions for a range of plant types. Shade is a high value commodity in Tucson. We also hope to capture the potential of the full height of the walls by attaching vertical gardening components. 

The prototypes will be developed similar to individual raised beds with a more sustainable mission. In addition to allowing the students and student teachers to learn about soils, drainage, plant growth, etc., they will also incorporate water harvesting, possibly solar collection, and some testing of temperature, heat gain, evaporation, etc. They will then either be incorporated into the garden or used as learning modules to share at other schools or for other university courses (Professor Marston\'s community gardening course, for example). They may also be utilized on Pennington Street to foster the physical connection between UAD and CHS and to create more of a "green street" atmosphere that simultaneously teaches sustainable principles to passing pedestrians. 

Photos of the alley are included in the attached pdf. It is active throughout the day with students and teachers and visible by the large public population the eats and works on Pennington and parks in the Pennington Street parking garage. 

Though we have provided preliminary sketches to illustrate design potential, the intention of the project itself is to explore design opportunities in much greater specificity and with a much more extensive process. Design is a lengthy, iterative, invested process. The faculty and students will draw on years of expertise and months of on site analysis and interaction to develop a design appropriate for this site and aimed at successfully fulfilling the programs proposed. 

The construction, agriculture, shade, water harvesting and renewable energy costs will go towards the necessary materials to build out the design we develop for the space. The environmental measuring tools, as further explained in the attached pdf, will equip the participating students with quantitative measuring and projective devices and software to evaluate the measurable sustainability objectives incorporated in the alley redesign. The students may, for example, measure heat island effect in the alley space over a series of months, then evaluate their alley redesign to assess its ability to reduce heat island effect through humidity and temperature changes due to material and spatial adjustments. This equipment could become part of a larger sustainability measurement kit utilized in k-12 and/or UAD educational opportunities. 

We are thrilled to have this opportunity to provide you with further information. Please feel free to ask should you need anything additional. Best of luck as you make the final decisions!

Linda + team',
'created_at' => '2014-02-25 19:27:45.000000',
'question_id' => 651,
),
158 => 
array (
'answer_text' => '1) If only partial funding is available for your project, please specify which project elements are most central to overall success of your goals.
Our project seeks to develop a comprehensive approach and sustained dialogue in regards to art-based civic engagement and environmental inquiry. In this regard, the speaker, workshop, and dialogue series are most central to the overall success of our project goals. We see the speaker and workshop series as being the fundamental process through which we will build a UA methodology for this method of sustainable practice. The digital storytelling workshop would provide a practical hands-on opportunity for real skill building, but it is the component most easy to remove without compromising the goals and intended effects of our proposal.
The only other area we see as feasible to cut, if absolutely necessary, would be reducing the speaker funds. However, this would limit our ability to bring in national practitioners; instead we would rely on local and regional actors.

2) We understand that a partially funded project must also necessarily reduce its goals and desired impacts. Considering your answer to question 1, please discuss how reducing the scope of the project would affect project deliverables.
Not funding the digital storytelling workshop would limit the practical skill building among emerging sustainability practitioners. It would also decrease the project visibility and collaboration with Biosphere 2 that we detailed in the project proposal. Depending on the funding supplied, we could coordinate with Biosphere 2 to schedule a day-long workshop instead and still arrange an exhibit of final products to be shown at Biosphere 2.

3) Does your team have plans to apply for other funding that might cover some of the cost associated with your project (i.e. invited speaker grants from WEES, GPSC, and so forth)?
Yes, we intend to apply for invited speaker grants from WEES and GPSC to augment speaker costs. If we receive this funding, we could extend the speaker series or return some of the allotted speaker series funds to the Green Fund.',
'created_at' => '2014-02-25 20:17:30.000000',
'question_id' => 649,
),
159 => 
array (
'answer_text' => '1.  Over the past two years, I have managed to sustain the project by employing work-study students to continue to radio track rattlesnakes, conduct surveys to find new rattlesnakes and Gila monsters, obtain data from all snakes captured, maintain a database of all data collected on Tumamoc Hill, and use Google Earth and GIS to map locations.  I applied for a grant through the Community Connections Program at UA, but the grant was not selected for funding.  However, it did rank 13th out of over 100 proposals submitted, but they only funded 8 projects.  We were invited to re-apply, which we will do this year.  I am also planning on submitting a proposal to the Arizona Game and Fish Department Heritage Fund to dovetail with the research we are conducting.  It is important to note that while our current proposal builds on the one we received in 2011, we have added several substantial new objectives.  I am confident that we will be able to obtain additional funding from extramural sources, especially with the opportunity to obtain more data and develop our outreach component.  In addition, I have contributed thousands of dollars in equipment and in-kind salary to maintaining and building upon the initial investment by the Green Fund.

2.  I have already identified a likely source for funding to bolster the Green Fund project.  The Arizona Game and Fish Department Heritage Fund has an Urban Wildlife Program that funds project, which are an excellent fit for the work we are doing on Tumamoc Hill.  I have received several AGFD grants in the past, and I am confident that issues associated with sustainable urban ecology are of great interest to AGFD.  In addition, I plan to submit a proposal to the USFS Urban Forestry Program, as well as several other organizations that support this kind of research and outreach.  In short, I have a proven track record of obtaining funding for my research, and working on Tumamoc Hill with Green Fund support will set the stage for obtaining additional funding in the future.',
'created_at' => '2014-02-25 20:26:58.000000',
'question_id' => 652,
),
160 => 
array (
'answer_text' => 'The MycoCats are dedicated to donating 5% of all mushroom production to non-profit agencies such as the Southern Arizona Food Bank.  In addition, our community outreach effort already underway at Tucson Village Farms will provide free mushrooms to community participants (they will take home the fruits of their labors!).  All other output from our production operation will be focused toward cost recovery and student employment.

This year we hope to produce 500 lbs of blue oyster mushrooms (a 100% bioefficiency based upon 500 lbs of pods already collected in 2013).  Most of the production to date (>200 lbs mushrooms) has been donated or provided as gifts to participants or persons associated with the project.  However, we do have a sales and service account established in the School of Plant Sciences and we have recorded a modest $150 in sales to date.  This was only after 6 months of effort, most of which was devoted to logistics, not sales. We currently are not a retail outlet, however, a formalized business plan is expected to be developed during Spring 2014.  We also expect a strong need to collaborate with the Norton School of Business to insure our business practices are appropriate as our business footprint increases.

In 2014-2015, our target pod collection volume is 5000 lbs (a 10 fold increase from 2013 and which we believe is very attainable).  With a 100% bioefficiency in our mushroom production process, this would translate to 5000 lbs of mushrooms.  Production of mushrooms grown on coffee grounds and pizza boxes will add to this total.  And with a standard retail value of $10/lbs for blue oyster mushrooms, we anticipate a gross profit of >$50,000 by 2015.  This hopefully will be achieved through direct sales to the UA Food Services group, which has expressed interest in our product, and sales through local Farmers Markets, of which there are many.  Sales to student on the UA Mall will be reduced to encourage the nutritional consumption of mushrooms by students.

Our projection for a self-sustaining income for the MycoCats program is $75,000, this through a combination of direct mushroom sales and sale of mushroom spawn for small business and home mushroom production projects (we already have inquiries for spawn!).  This would allow us to financially support 3-4 exemplary students for program management and a much larger number of interns for program execution.  In addition, this would leave $25,000 for yearly supplies and equipment upgrades for insured program viability.

If we generate funds above this amount, many opportunities open up.  To achieve the projected gross income would be a terrific testament to the economics provided by green industries.  We envision that income achieved above our targeted $75,000/year would likely become available for development of additional programs targeted toward additional green industries.  These may be related to mushroom production or perhaps they may support more diverse industries.  Thank you for consideration of funding the MycoCats for one additional year.  

Barry Pryor',
'created_at' => '2014-02-25 22:58:09.000000',
'question_id' => 653,
),
161 => 
array (
'answer_text' => 'Hi Julia,
the project is successfully finished. The Progress report is ready to go to, I had to wait for our Business Office to provide the financial Report, which I just received. I will submit the final Report next week.
thanks
Monika',
'created_at' => '2014-02-27 16:01:27.000000',
'question_id' => 656,
),
162 => 
array (
'answer_text' => 'The funding request would cover the costs of two students to participate in the course. We need 7 participants total to enroll for the course to go ahead. These other participants will be enrolling themselves and paying their own way. 

If the course does not fill this spring, the next course is being planned for winter session. The spring course not filling will not cause a delay in the installation of the rainwater tank lab installation.',
'created_at' => '2014-02-27 16:59:00.000000',
'question_id' => 658,
),
163 => 
array (
'answer_text' => '1) $5,000 will be for plumbing (extension from Highland to the cisterns, connecting the cisterns, and connecting to UA Community Garden irrigation).
$3,000 to install a concrete pad to support cisterns (i.e. supplies for pouring and setting the base, as well as labor costs)
$54,000 is calculated by 1 gallon of water being worth $2. We then multiply that number by 27,000 (total gallons of water collected). We have received word from water harvesting companies that they offer discounts to student groups, and therefore did not include an installation fee.
We are at a stage where only costs estimates are available. However, if we find ourselves over the proposed budget, ASUA Students for Sustainability will be able to compensate that difference.

2) As we are finalizing our plans for installations, we have not yet obtained cistern model numbers. The set-up of the cisterns will be unique - not only with handling 27,000 gallons of water, but also filtering out any contaminates. The material of the cisterns will be of corrugated metal - design life of 75 years and the most cost-effective.',
'created_at' => '2014-02-28 00:13:15.000000',
'question_id' => 654,
),
164 => 
array (
'answer_text' => 'In response to the question directed to the MycoCats on 2/7/14:

The MycoCats are dedicated to donating 5% of all mushroom production to non-profit agencies such as the Southern Arizona Food Bank.  In 2013 we donated 5 lbs of blue oyster mushrooms and they were enthusiastically incorporated to the culinary program at the Caridad Community Kitchen.  We hope to increase these contributions in 2014 with our increasing production output.  In addition, our community outreach effort already underway at Tucson Village Farms will provide free mushrooms to community participants (they will take home the fruits of their labors!).  All other output from our production operation will be focused toward cost recovery and student employment.

This year we hope to produce 500 lbs of blue oyster mushrooms (a 100% bioefficiency based upon 500 lbs of pods already collected in 2013).  Most of the production to date (>200 lbs mushrooms) has been donated or provided as gifts to participants or persons associated with the project.  However, we do have a sales and service account established in the School of Plant Sciences and we have recorded a modest $150 in sales to date.  This was only after 6 months of effort, most of which was devoted to logistics, not sales. We currently are not a retail outlet, however, a formalized business plan is expected to be developed during Spring 2014.  As such, we expect a strong need to collaborate with other groups on campus to insure our business practices are appropriate as our business footprint increases.

In 2014-2015, our target pod collection volume is 5000 lbs (a 10 fold increase from 2013 and which we believe is very attainable).  With a 100% bioefficiency in our mushroom production process, this would translate to 5000 lbs of mushrooms.  Production of mushrooms grown on coffee grounds and pizza boxes will add to this total.  And with a standard retail value of $10/lbs for blue oyster mushrooms, we anticipate a gross profit of >$50,000 by 2015.  This hopefully will be achieved through direct sales to the UA Food Services group, which has expressed interest in our product, and sales through local Farmers Markets, of which there are many.  Sales to student on the UA Mall will be at reduced prices to encourage the nutritional consumption of mushrooms by students.

Our projection income requirement for a self-sustaining MycoCats program is $75,000, this through a combination of direct mushroom sales and sale of mushroom spawn for small business and home mushroom production projects (we already have inquiries for spawn!).  This would allow us to financially support 3-4 exemplary students for program management and a much larger number of interns for program execution.  In addition, this would leave funds for yearly supplies and equipment upgrades for insured program viability.

If  we generate funds above this amount, many opportunities open up.  To achieve the projected gross income would be a terrific testament to the economics provided by green industries.  We envision that income achieved above our target would likely become available for development of additional programs targeted toward additional green industries.  These may initially be related to mushroom production, perhaps in new locations or using novel substrates for growth, but we also wish to support more diverse green industries not directly related to mushroom production.  Thank you for consideration of funding the MycoCats for one additional year. I are confident this program will be a huge success and will have impacts and recognition far beyond the University of Arizona.

Barry Pryor
Professor of Mycology and Plant Pathology
University of Arizona',
'created_at' => '2014-02-28 08:46:24.000000',
'question_id' => 650,
),
165 => 
array (
'answer_text' => 'This is a great question.  The answer is yes, we would be very interested in making this recycling opportunity available to others on campus.  We expect that the capacity of the machine will allow for additional recycling, on top of that needed by the Chemistry and Biochemistry (CBC) Department undergraduate labs.  Also, due to the straightforward operation of the machine, providing that service should not create a particularly large burden on the staff responsible for operating it, here in CBC.

Our initial thoughts for this project were based on the idea that the undergraduate Chemistry labs are likely the biggest user of acetone on campus.  However, with the recycler operational, we will investigate expanding the project.  By communicating with other Departments on campus, we can determine which might produce significant quantities of acetone waste.  A productive approach will be to coordinate with Risk Management; to both identify potential users and to advertise the service.

We have also considered that some research groups, certainly in Chemistry and Biochemistry, could make use of this machine, leading to a substantial increase in the amount of acetone recycling on campus.',
'created_at' => '2014-03-03 17:18:04.000000',
'question_id' => 655,
),
166 => 
array (
'answer_text' => 'Due to some inadvertent communications breakdowns during the proposal process, it had been unclear until now exactly how FM would be engaged.  James MacAdam followed up directly with Chris Kopach over the past week to share the details of the proposal and clarify FM\'s involvement.  Mr. Kopach indicated that FM is supportive of the initiative "where feasible and where funds are available without the quality of service to the campus being affected."  

Mr. Kopach indicated that he would like himself specifically to be added to the Project Members list, and that FM staff should be included in a key role throughout the project.  Mr. Kopach shared some concerns, including the feasibility of utilizing reclaimed water in cooling towers, and costs associated with increasing reclaimed water use.  We agreed on an approach of using this first year of partnership to develop a pilot project, while simultaneously developing a longer term plan that identifies strategies and possible funding sources for a larger transformation of the system (as indicated in the proposal).  

Mr. Kopach also recommended a formal MOU or reclaimed water usage agreement between the UA and Tucson Water.  

In an email, Mr. Kopach wrote:
"The Facilities Management Department has engaged students on many sustainable projects in the past and we look forward to this partnership; however, we need to make sure the integrity of our campus utility system and water supply to our buildings is not adversely affected.  My UA Utility staff will need to play a key role in this partnership with students and key members of the project."

Additionally, Associate Dean Jeff Silvertooth has indicated his interest and willingness to use the Agricultural Experiment Station as a site for demonstration, as indicated in the proposal.  Mr. Kopach has already identified some creative ways that we could make some near term changes to the AES site. 

With Mr. Kopach\'s support, we are confident that Facilities Management will be a strong, engaged, and supportive partner on this project.',
'created_at' => '2014-03-04 16:59:17.000000',
'question_id' => 659,
),
167 => 
array (
'answer_text' => 'Hi Julia,

Dr. Tracey Osborne (tosborne@email.arizona.edu) is the faculty member overseeing the project. I thought they recommended that our project not be funded, correct?

Thanks,
Sarah',
'created_at' => '2014-03-05 21:49:06.000000',
'question_id' => 660,
),
168 => 
array (
'answer_text' => 'Hi Jesse,
As you are probably aware, our Externship program is summer season-based (May-August) and straddles two fiscal years. We budgeted and were awarded funds in the FY14 request to cover the beginning of our FY15 extern program (May-June) until our FY15 funds kick in on July 1.  I don\'t believe we will be returning any funds from FY14. Thanks for your help on this. Mark',
'created_at' => '2014-04-02 19:23:29.000000',
'question_id' => 661,
),
169 => 
array (
'answer_text' => 'Technically, we already spent our budget and would only be potentially respending refunded money. I\'m not sure if that requires an extension to spend money our not, but I\'m guessing it probably does?',
'created_at' => '2014-04-28 22:00:32.000000',
'question_id' => 662,
),
170 => 
array (
'answer_text' => 'Technically, we already spent our budget and would only be potentially respending refunded money. I\'m not sure if that requires an extension to spend money our not, but I\'m guessing it probably does?',
'created_at' => '2014-04-28 22:00:53.000000',
'question_id' => 662,
),
171 => 
array (
'answer_text' => 'Thank you for the update.',
'created_at' => '2014-07-21 16:49:23.000000',
'question_id' => 678,
),
172 => 
array (
'answer_text' => 'During the current cycle, we had 57 University of Arizona students involved in our camp.  The composting toilet still needs to be installed (delivery is set for mid-March) and it will take approximately 2 days of labor to install it.  This would be two people @ $20/hour X 160 hours = $3,200. But we have other projects that need labor too.The energy efficient dishwasher still has to be ordered (seeking 3 bids) but we have decided to forgo the icemaker and are requesting that those funds be allowed to be used to pay for the labor of installing GF projects.',
'created_at' => '2015-01-20 22:46:09.000000',
'question_id' => 680,
),
173 => 
array (
'answer_text' => 'We have plans to utilize the new “Water Monster” provided by Facilities Management at next year’s event to cut back on the use of disposable water bottles and campus waste. We would also like to work with Compost Cats to collect food waste at the event, however we have not discussed this idea with them quite yet. It has been our understanding that Facilities Management and Culinary Services have both implemented sustainable practices with Compost Cats and in general. But if there are additional ways we can work with them at Food Day we would be happy to do that. With regard to Facilities Management and Culinary Services, as well as other departments, we are still in the very early stages of planning and are certainly open to any ideas or suggestions that would make the event more sustainable. ',
'created_at' => '2015-02-06 18:24:58.000000',
'question_id' => 681,
),
174 => 
array (
'answer_text' => '1. One of the main goals for this garden is to build community within and between the Historic Lane Residents. Thus, the garden will be one communal plot that will contribute to building community--a key Residence Life value. 

2. We anticipate at least two programs a month since there are several student groups directly involved (i.e. Eco-Reps, Historic Lane Residence Hall Council, Resident Assistants/Community Directors). These groups will have access to the garden to plan programs, which will be additional to the programs the Student Co-leaders organize with the student volunteers. In addition, we anticipate at least one garden work day a week, which will serve as its own program for allowing any resident interested the opportunity to learn more and help out with the garden. The Historic Lane Faculty Fellows have also already expressed interest in utilizing the space for their programming to engage with the residents of their respective buildings. 

3. The garden will be sustained through Residence Life Sustainability Programs and The Historic Lane Hall Council budgets. 

Each year, the garden will be budgeted into the budgets of these two organizations. 

Residence Life Sustainability Programs:
*Compost and Soil: $400
*Tools and Maintenance: $100
*New plants and seeds: $50

Historic Lane Hall Council:
*Programming: $300

The Student Co-leader position will be funded as a stipend from Residence Life Sustainability Programs after the first year of the garden. During the following year, the Co-leader position will not involve all of the initial time-consuming and devoted work that will be required during the design, construction, and program implementation phases of the garden. Therefore, a $400 yearly stipend from resident life will compensate the Co-leaders in future years. Stephanie and Alanna will remain the Co-leaders through year 2, at which point a a current or previous Eco-rep or dedicated garden volunteer will be selected and assume the position. The succession of students for the position will follow the same pattern as students graduate/move on.  

**note the picture was taken first thing in the morning, around 8 am—but there is direct sunlight throughout most of the day.',
'created_at' => '2015-02-08 20:30:12.000000',
'question_id' => 682,
),
175 => 
array (
'answer_text' => 'First, I would like to thank the Green Fund for their continued support.  Without this support, we would not have accomplished what we have to date, which is considerable in only 1.5 years.  Thanks!!  Next, I didn’t have the opportunity to tell you about new and exciting activities the MycoCats are engaged in (as was suggested in the email sent notifying me about the Green Fund Committee meeting).  So before I answer the three questions posed, I would like to take this unique opportunity to quickly mention one activity now.  We are developing a new substrate for growing mushrooms…..buffelgrass!!  Rather than simply throw into the landfill all the buffelgrass pulled up in the buffelgrass eradication campaign, we are partnering with the Southern Arizona Buffelgrass Coordination Center to use some of what they pull up as part of our fruiting substrate instead of using straw (which we mix with mesquite pods but have to buy).  And the buffelgrass is free!  A win-win-win situation.

And now to the questions.  Regarding the first question, we do not plan to sell our mushrooms to other entities outside of the University because we would be directly competing with upstart businesses that are trying to become established.  In addition to developing a recycling program at the UA, one of our missions is to assist other businesses throughout Arizona in developing their own mushroom production operations.  To date, we have helped launch four other mushroom production companies in Tucson and Phoenix.  In addition to providing Arizona with high quality mushrooms locally grown, these businesses also buy mushroom production products directly from us, namely spawn (used to inoculate production substrate) and fully colonized production substrate (bags ready to be fruited).  These products they could purchase out-of-state but for a much higher price.  These are the same products we use to produce our own mushrooms for sale to dining services, but by selling these products to other mushrooms producers, a portion of the potential production profits can be realized by community businesses.  Importantly, by selling our fully colonized production substrate to community producers, we are continuing to recycle the products we would normally recycle but are now including community businesses in the final recycling process, the fruiting.  After fruiting, these mushroom producers can us the spent substrate in their own composting programs.

Regarding the second question, yes, there will be persons working over the summer as there were last year, our first summer production.  But because of the absence of student taking Internship for credit as we have during the school year (we are trying to arrange for summer internships), we have to pay the students as employees.  Since wages are a primary expense, we cannot hire many students and our production does slow down.  However, with recent development of outlets for our mushroom products for sale, we are realizing cost recovery and these funds will help support a good student work force during Summer 2015.  Since October 3, 2014, (our first sale) we have sold $3600 in mushrooms or mushrooms products, which averages to $225/week over those four months.  Our sales to UA Dining Services goes down during the summer (less catering performed), but our sales to outside businesses will likely remain steady.  And if we are successful in helping develop more mushroom growing businesses in Arizona, our customer base will grow as well, generating more income and enabling the hiring of more students.

Regarding the third question, a new budget has been generated.  We will use as a basis for cost-recovered income the conservative $225/week stated above, and project this over 52 weeks ($11,700).  These funds will be used to support three additional student employees, both during the school year and during the summer ($9900), and for some travel to promote the business and science of recycling with mushrooms (in-state visits to other mushroom operations and out-of-state travel to mycological conferences, $1800).',
'created_at' => '2015-02-09 08:44:28.000000',
'question_id' => 684,
),
176 => 
array (
'answer_text' => '1. One of the main goals for this garden is to build community within and between the Historic Lane Residents. Thus, the garden will be one communal plot that will contribute to building community--a key Residence Life value. 

2. We anticipate at least two programs a month since there are several student groups directly involved (i.e. Eco-Reps, Historic Lane Residence Hall Council, Resident Assistants/Community Directors). These groups will have access to the garden to plan programs, which will be additional to the programs the Student Co-leaders organize with the student volunteers. In addition, we anticipate at least one garden work day a week, which will serve as its own program for allowing any resident interested the opportunity to learn more and help out with the garden. The Historic Lane Faculty Fellows have also already expressed interest in utilizing the space for their programming to engage with the residents of their respective buildings. 

3. The garden will be sustained through Residence Life Sustainability Programs and The Historic Lane Hall Council budgets. 

Each year, the garden will be budgeted into the budgets of these two organizations. 

Residence Life Sustainability Programs:
*Compost and Soil: $400
*Tools and Maintenance: $100
*New plants and seeds: $50

Historic Lane Hall Council:
*Programming: $300

The Student Co-leader position will be funded as a stipend from Residence Life Sustainability Programs after the first year of the garden. During the following year, the Co-leader position will not involve all of the initial time-consuming and devoted work that will be required during the design, construction, and program implementation phases of the garden. Therefore, a $400 yearly stipend from resident life will compensate the Co-leaders in future years. Stephanie and Alanna will remain the Co-leaders through year 2, at which point a a current or previous Eco-rep or dedicated garden volunteer will be selected and assume the position. The succession of students for the position will follow the same pattern as students graduate/move on.  

**note the picture was taken first thing in the morning, around 8 am—but there is direct sunlight throughout most of the day.',
'created_at' => '2015-02-09 16:21:22.000000',
'question_id' => 686,
),
177 => 
array (
'answer_text' => '1. Do you have a vision for sustainability related projects? 
This first year sustainability projects will focus on promoting action and awareness through education and demonstration activities. These projects will include water harvesting and native plant demonstrations to conserve water resources on campus and various types of food gardens will be developed and workshops will be conducted to address local food production. Our vision is to start out with projects that are fun and interesting opportunities for students, staff, and the local community members to contribute to. From these early projects we can begin to develop some capacity to address campus sustainability and collectively identify future projects. The Foundation is looking to develop a long term master plan for the UA Sierra Vista campus so now is the perfect time to have conversatons on integrating increased sustainability. The funding requested to hire students will be important to identifying and developing future sustainability projects that are meaningful to the local campus community.

2. Where do you see the campus going in regard to sustainability? 
To our knowledge there has been little overall discussion on the UA Sierra Vista campus regarding sustainability. The aim is for this project to serve as an example of projects that can be done, to increase awareness of green fund opportunities, and to serve as a catalyst for future development and integration of sustainability on campus. A long term goal is for the campus to exemplify sustainable practices and serve as teaching tools for the entire campus and local community where they can see sustainable concepts put into action.

3. Do you have approval from your facilities for the projects? 
The overall project has been discussed with the University South Foundation Director of Finance and Administration and the Dean of UASouth. Both are supportive of the project. The learning center shade structure will be presented at the Foundation Board’s March meeting to request formal approval. The in kind donation of the 5,000 gallon rainwater harvesting tank will also be presented for approval at that time. All other items are within the existing scope of the current plant science center. If the Green Fund Committee is unable to approve funding due to the lack of formal approval to date for the shade structure, we would like to request continued consideration for the remaining portions of the project ($20,500).

Thank you for your time and consideration of our project.',
'created_at' => '2015-02-09 16:32:09.000000',
'question_id' => 683,
),
178 => 
array (
'answer_text' => '1. We have four (4) AGTM laboratory courses currently offered (AGTM 100, AGTM 330, AGTM 350, and AGT 351) which are 3 unit courses, with one hour of lecture and six hours of laboratory per week. Current lab course fees for each course are, and include:
AGTM 100 - $50.00, materials for fabrication of projects that students take home with them at the end of the course including a welded Wilbur Hand truck, a wood sawhorse, a plasma-cut Block A wall mount. Current cost of materials and supplies to run this course is approximately $138.00 per student.
AGTM 350 - $50.00. - Fees cover cost of materials for electrical wiring exercises (Romex 14-gauge conductor), small engine reassembly materials (gaskets, fuel, oil)
AGTM 330 - $75.00 - Fees cover cost of multiple field trips (UA vans), materials for plumbing and sprinkler projects (students construct and take home). 
AGTM 351 - $50.00 - Fees cover cost of small engine and and large engine maintenance lab activity materials (oil, fuel, gaskets, spark plugs, filters, replacement tools).

2. Facility utility fees (Electricity provided by TEP; gas for heaters, etc.) cannot be charged to student lab fees. If they were allowed, the AGTM 100 course (where welding activities occur) would have the largest fees associated. Past TEP billing suggests over a three month period (Sept-Nov),this amount to be equal to $1000.00. If this cost was charged to the 24 students enrolled in AGTM 100, would equate to an additional $42.00 to the lab fee.

Budget reductions to the University and to CALS results in less funds for Departments to operate. Our building is an older facility. We have attempted to improve energy efficiency (modifying interior lighting, for example). 

Having a grid-connected solar array provides opportunity to reduce the amount of utility-supplied electrical energy (kWh), and eliminate the use of older gas-powered heaters.',
'created_at' => '2015-02-11 20:52:10.000000',
'question_id' => 692,
),
179 => 
array (
'answer_text' => 'No, we have not reached out to ASU at this point but we could consider it.  We have reached out to NAU and Prescott College.

Kirk',
'created_at' => '2015-02-12 00:19:08.000000',
'question_id' => 693,
),
180 => 
array (
'answer_text' => 'It looks like a previous submission timed out. Below is a re-iteration of what was submitted: 

The AISES National Conference (http://www.aises.org/conference) is a gathering of national higher education organizations in STEM fields with a focus specifically on the intersection of Native culture and STEM. As one component of this conference, there is an undergraduate research showcase in which students who have completed any form of project or research to submit their work to be viewed by all attendees. In order to do this, a review committee from AISES reviews all potential submissions for this research showcase and selects different projects to be represented. As a part of this process, there is an award presented for the best poster or presentation, as determined by a panel of judges. 

In addition to presenting on the undergraduate project, any student presenter(s) would attend the full conference, including workshops other presentations, browse sessions, etc.',
'created_at' => '2015-02-16 20:43:54.000000',
'question_id' => 697,
),
181 => 
array (
'answer_text' => 'I Submitted these answers below earlier today in response to the initial inquiry/link. Were they not received? And was there a new question in addition to these?

Thanks,

Grant



1. Please elaborate on why the costs for materials and signage are so high.

Our belief is the basin costs are toward the lower end of typical costs for such improvements done as cooperative neighborhood projects. The per-basin cost would be higher if it were done as a straight contractor project with no student involvement. Included are rocks, mulch, plants, mechanical excavation, curb cuts, permits, and any tools that are not able to be provided by the UA, WMG, or the neighborhood. This is based on project team member\'s experience with many related neighborhood projects, projects funded through the C2E program, and UA campus projects. Regarding signs, the intent is to create several 11"x17" or larger interpretive signs to be installed at basin sites, which we feel is appropriate given the educational focus of the project. This budget line is intended to also cover creation, printing, and distribution of promotional and other support materials for workshops and work days.



2. We need a letter from the city and FM to show that this project should be formally approved. 

We have contacted FM and the City requesting letters of support, and indications are there will be no problem getting this. We may not, however, receive the letters for several days, so they will be provided ASAP.



3. Is the water harvesting workshop on the mall an actual workshop or more like tabling?

This will be an interactive workshop with active presentations / instruction provided to both invitees and others that learn about it through different forms of marketing (as well as by  those seeing it as they pass by on the Mall). It is NOT intended as just a staffed table with leaflets and other info for people passing by to pick up.',
'created_at' => '2015-02-16 21:16:39.000000',
'question_id' => 696,
),
182 => 
array (
'answer_text' => 'If our PAR is denied, we will end up with surplus payroll and in a deficit that ASUA will have to make up in the operations category. We don\'t need more money, only to switch some funds between categories. As for organic certification, our market research and careful cost/benefit analysis have revealed that we can get a significantly higher price per bag of compost sold if the compost is certified organic. We did this research working with a team of Eller students last semester, too late to get the data into our original FY 2015 proposal. I think real cost/benefit analysis and incorporating market research into our project planning should be encouraged. In addition, some of the need for operations money is just due to unanticipated vehicle repair expenses. Fortunately, we are doing better than expected in the payroll category, and so we simply need to move money from one category to another. The organic certification is something that will demonstrably decrease our dependence on the Green Fund though, and so approving the PAR is a good investment in the project\'s future.',
'created_at' => '2015-04-30 17:03:26.000000',
'question_id' => 699,
),
183 => 
array (
'answer_text' => 'To answer the first question, I would invite you to view our program information on the Cooper website at this address:  http://coopercenter.arizona.edu/content/teachers-0. This page links to information about our day and single night "Traditional Programs" and our multi-night experiences. We have unique programs for all grade levels K-8 that include hands-on, discovery-based experiences for each student and basic ecological understandings. If you would like me to elaborate on each program, please let me know and I will be happy to do so.

With regards to translation back to the University, our activities and experiences are designed to take place anywhere, from our trails in the Sonoran Desert to an open area on campus. The majority of students on our campus are unfamiliar with the natural systems that support our use of energy and natural resources, and so activities that are designed for younger learners can provide a fun and interactive way to share the reasons behind our sustainable behaviors. For example, one of our activities focuses on interrelationships between living and non-living things. Students become plants and animals in a natural system and use cords to connect to their resources. It creates a web of life that serves as a visible representation of relationships in the environment. Pollution is introduced into the system, breaking connections and affecting living things throughout the web. Learners of all ages benefit from hands-on experiences that connect humans to the world that supports their style of life, and an understanding of these connections on campus will lead to motivated students who are invested in sustainability projects.

With regards to what the Cooper Center does, we provide field trip opportunities for Southern Arizona schools and unique learning opportunities in the field of science education for University of Arizona students. Our center\'s overnight experiences are unique in the Sonoran Desert and inspire learners of all ages to live more sustainably in our community.

The paid positions of this proposal would work with Cooper Center staff to gain experience in leading environmental education programs. They would work with K-8 students at the Center and in the surrounding Sonoran Desert, and assist with program completion at local schools. They would also work with campus clubs and organizations in on campus events, workshops, and classes to share environmental education experiences with students across campus. The students funded by this proposal would organize volunteer experiences at the Cooper Center during the spring semester, evaluate the success of this pilot project, and then create plans for sustaining the project for 2017-18 and beyond.

I am happy to add to these comments as needed. Again, thank you for the opportunity to create new and exciting connections between Cooper Center and the larger UA community.',
'created_at' => '2016-02-12 20:50:00.000000',
'question_id' => 703,
),
184 => 
array (
'answer_text' => '-- Yes, it is quite all right to move the AZ Green Box to a location on campus that could have grater student impact, such as perhaps nearby a UA dormitory area, etc.

-- Yes, the PV panel will be connected to a battery, which should last for about two days without recharging (that is, when the sun is not shining). As back up to the supply of solar-generated electricity, we have provision for connecting the AZ Green Box to grid electricity as needed to ensure consistent lighting.',
'created_at' => '2016-02-13 01:04:55.000000',
'question_id' => 702,
),
185 => 
array (
'answer_text' => 'I have also sent an Excel spreadsheet via Julia Rudnick, but here is a text explanation:

Additional Explanation of budget as requested by Green Fund Committee: Our total project budget is $123,567. If you subtract the amount we are asking for from Green Fund ($48,200)+the amount of the project director\'s salary ($39000) and the director\'s ERE ($13,533), our remaining funds from other sources come to $22,834.							
We are not actually asking Green Fund for any of our regular equipment maintenance and repair costs. We are also only asking Green Fund to cover ~60% of payroll. Our entire estimated payroll will be $49, 148, which is $19,148 over the $30,000 we are asking of Green Fund. So $22,834-$19,148 is $3686, which will then be used for equipment maintenance and repairs, a very conservative number. We actually hope to generate more revenue in compost sales to help cover additional maintenance and repair costs.							
Note: If the Green Fund prefers to pay only for student staff pay, rather than for a new truck and the US Composting Council Conference Trip, you could fund us in the exact same amount as requested ($48,200) and it would pretty much cover our entire payroll (estimated at $49,148). Then we would instead fund our trip and new truck from our other revenue sources. This is up to the committee, of course, but it should be clear here that Compost Cats is not requesting more money than we need to cover our expenses.',
'created_at' => '2016-02-22 18:06:55.000000',
'question_id' => 706,
),
186 => 
array (
'answer_text' => 'The signs for sustainability project deals with outside waste disposal and the creation of universal permanent signage. There is no plan to change bins but to rearrange outside bin placement so that places with a high number of trash cans are either replaced by recycle bins or paired with recycle bins. This aspect of the project is being worked on now with Facilities Management. Once outside trash cans and recycle bins are arranged in a more efficient fashion, permanent signage will be employed to help dispel confusion regarding what can be recycled etc. Facilities has a metal working facility that can make permanent signs and posts.',
'created_at' => '2016-02-22 18:32:38.000000',
'question_id' => 707,
),
187 => 
array (
'answer_text' => 'Dear Green Fund Committee,

Thank you for the inquiry. The main focus of this grant is proper disposal of electronic waste components not just a collection of electronic items. There are other services that accept usable items for compensation and this project is not in a position to rival those services. It would be outside the scope of this project to collect items for reuse and donation. Rather, the focus is on sustainably disposing materials that are no longer working or outdated. As technology continues to rapidly evolve, this event aims to collect items that no longer support current operation systems or are damaged beyond repair such as water damage. As per the committee’s suggestion, the project will encourage participants to donate their working items to their neighborhood thrift store on all promotional media for the event.

Best regards,
Ebitie & Brian',
'created_at' => '2016-02-23 15:19:20.000000',
'question_id' => 704,
),
188 => 
array (
'answer_text' => 'Email I received indicated we had to have the funds spent in April. Otherwise we would not have access to the funds before the June completion date.',
'created_at' => '2016-03-29 19:26:23.000000',
'question_id' => 708,
),
189 => 
array (
'answer_text' => 'Responding to the content of this email:

He Ed and Elizabeth,
I\'m checking in to see how the Green Funded solar pump project is going. Ed, I think you had installed the pump models and were demoing them in some of your classes last time I checked. Is everything still going as planned?

Elizabeth, you were still preparing for the install at Tucson Village Farms. How did the installation go?

Let me know if you have any questions for me or if you have concerns about finishing the project before June 30th. Remember that if you have not used the Green Fund funds by June 30, you must submit a PAR by April 25th to extend your access to those funds. If you think there\'s even a slight chance, then go ahead and submit the PAR!

Covering our bases in case we dont get the project installed by June 30.',
'created_at' => '2016-03-29 19:41:26.000000',
'question_id' => 708,
),
190 => 
array (
'answer_text' => 'Yes, I am asking to move $747.00 from travel to operations category.',
'created_at' => '2016-03-30 23:00:48.000000',
'question_id' => 709,
),
191 => 
array (
'answer_text' => 'I will definitely be there!',
'created_at' => '2017-01-26 22:50:14.000000',
'question_id' => 715,
),
192 => 
array (
'answer_text' => 'I plan to attend. Thank you.',
'created_at' => '2017-01-26 23:11:26.000000',
'question_id' => 711,
),
193 => 
array (
'answer_text' => 'I teach a lab course at the UA Campus Agricultural Center on Mondays which ends at 5:00 pm. With work traffic, it may be a challenge to make the scheduled meeting time of 5:20 pm. 

Thank you,

Ed Franklin',
'created_at' => '2017-01-27 00:27:39.000000',
'question_id' => 712,
),
194 => 
array (
'answer_text' => 'I personally cannot make the scheduled time due to the fact that I have a physics lab at that moment, but Julia Rudnick and Jamie Verwys will be there filling in for me.',
'created_at' => '2017-01-31 17:25:09.000000',
'question_id' => 719,
),
195 => 
array (
'answer_text' => 'The funding for only one water refill station would not take too much of a toll on the project. The second refill station was more of a convenience for the residents of the ENR2 building that are placed on the higher floors. Funding one refill station (quoted at $4,335) would be more than enough to promote more sustainable use of reusable water bottles throughout campus. Thank you.',
'created_at' => '2017-01-31 20:27:47.000000',
'question_id' => 741,
),
196 => 
array (
'answer_text' => 'I apologize, but due to parental duties and our distance from Tucson (75 miles), I am unable to attend the meeting that evening, but would be happy to answer any questions via email ahead of time or afterwards.  Thank you for your consideration of our proposal.  Mark Apel',
'created_at' => '2017-01-31 21:10:00.000000',
'question_id' => 726,
),
197 => 
array (
'answer_text' => 'I will be attending the meeting.',
'created_at' => '2017-01-31 21:53:11.000000',
'question_id' => 723,
),
198 => 
array (
'answer_text' => '31 January 2017
Thank you for the follow-up questions.

Q1: Do you have solid leads on funding items for the project once it is designed?
For supplies and materials, we have some items (solar panels, pipes and gutters, aquaponics unit, etc.) on hand and plenty of leads on places & people to ask for contributions. One of the expected outcomes is for UA students, being invested and inspired, to help solicit contributions to make the Eco Dorm viable with solar panels, water harvesting, grey water, aquaponics, composting toilet, outdoor shower, and more. Biosphere 2 facilities budget can accommodate much of the costs of installation using existing staff with guidance from outside experts. We can also cover costs of general building upgrades. Staff for programming once up and running will be from Biosphere 2 team and their compensation partially offset by program fees.

Q2: The project outlines hiring students for a total pay of $2000. Who will you hire and what is the major job function?
We want to hire five students that will serve as lead planning project manager, that can help coordinate the planning workshop, that can correspond with invitees and participants, that will engage with stakeholders, and that will follow up with execution of plan by engaged students and appropriate community organizations. The individual students have not been identified, but we will seek them out through UA-affiliated stakeholder groups. Several Biosphere 2 staff will also be important supporters and partners along the way.

Thank you,
Biosphere 2',
'created_at' => '2017-01-31 22:14:52.000000',
'question_id' => 737,
),
199 => 
array (
'answer_text' => 'I can attend, thank you!',
'created_at' => '2017-02-01 16:52:29.000000',
'question_id' => 727,
),
200 => 
array (
'answer_text' => '1) Will you integrate this workshop with other workshops you currently offer?

Requested funding will be used to acquire tools and materials to demonstrate the process of commissioning a solar PV system using an I-V Curve tracer, and troubleshooting a solar PV system. Additionally, we will include solar micro inverter technology, and a ballast-mount system in our solar PV curriculum.

2) What is the overall picture of the workshops? Do you take the workshops in a series?

Our workshop format varies based on the specific audience. We have conducted both 20 minute rotational activities, to full day (8 hour) workshops.  Shorter workshops feature solar PV water pumping systems wired in parallel and in series, use of digital multimeters to measure voltage and current, use of a pyranometer to measure solar irradiance, use of a non-contact thermometer to measure solar cell temperature. Workshop participants assemble and operate working systems. The longer workshops will have attendees building, testing, and taking systems back to their schools, or extension centers. We hope to develop a series of workshops to include solar PV fundamentals, direct systems, battery-based systems, and grid-connected systems, as well as solar-powered wells.

3) Specifically, who and how many people will be impacted by these workshops?

Here is a list of our audiences we have conducted solar PV workshops for over the past two years:
•	Elementary school students – Dodge Magnet School, Summer Science Academy, - presentation to seven classes of middle school students.
•	High school students – Baboquivari HS, Sells, AZ; Monument Valley HS, Kayenta, AZ; Rio Rico HS, Rio Rico, AZ, San Luis HS, San Luis, AZ; Highland HS, Gilbert, AZ; 
•	Southwest Ag Expo – 120 high school students rotating through five groups, Arizona Western College, Yuma, AZ (Feb 2015, 2016)
•	UA college student – Presentations/demonstrations to ABE 210 Introduction to Engineering course (fall 2015, 2016)
•	AGTM 350 course – solar PV instructional unit, 24 students, fall semester (2013, 2014, 2015, 2016)
•	Teachers – Arizona Agriculture Teachers Association (AATA), professional development workshop, (fall 2105); California Agriculture Teachers Association (CATA) New Professionals Conference (fall 2016), approximately 75 teachers, Fresno, CA; 
•	Farmers & Ranchers – Beginning Farmer Workshop – Florence HS, Florence, AZ (2016)
•	Cooperative Extension personnel – Cochise Co. (2015),  Pima Co. (2016), Maricopa Co. (2015)  Yavapai Co. (2016)
•	General public – Accepted invitation to set up our solar PV water pumping demonstration at the Tuba City Garden Expo, Tuba City, AZ (April 2016); Farm Food Safety Conference, Presentation of Solar PV energy fundamentals; installed and demonstrated a solar-powered submersible potable water well (2016)
We have contacted cooperative extension centers in Santa Cruz County, Pinal County, La Paz County, and Coconino County to set up dates for solar PV energy system demonstration presentations during spring and summer 2017. 
We will be hosting a solar PV water pumping demonstration activity with 25 high school FFA teams (100 students) on March 3, 2017, as part of the State FFA Career Development Event (CDE) Field Day',
'created_at' => '2017-02-01 18:53:26.000000',
'question_id' => 738,
),
201 => 
array (
'answer_text' => 'Hello, 

My name is Jamie Verwys and i\'m the creator of this grant. I will be there at 5:05 p.m. to answer any questions the Green Fund Committee might have.',
'created_at' => '2017-02-02 18:11:48.000000',
'question_id' => 717,
),
202 => 
array (
'answer_text' => 'Hello, 

My name is Jamie Verwys, the creator of this grant proposal, and I will be attending the meeting on Monday. 

Thank you.',
'created_at' => '2017-02-02 18:13:29.000000',
'question_id' => 721,
),
203 => 
array (
'answer_text' => 'Hello, 

My name is Jamie Verwys and I will be attending the meeting to answer any questions the Green Fund Committee might have in regards to this grant proposal. 

Thanks, 
Jamie',
'created_at' => '2017-02-02 18:14:38.000000',
'question_id' => 724,
),
204 => 
array (
'answer_text' => 'I will attend. Thank you for the opportunity.',
'created_at' => '2017-02-02 18:30:02.000000',
'question_id' => 749,
),
205 => 
array (
'answer_text' => 'I plan to attend.',
'created_at' => '2017-02-03 19:17:55.000000',
'question_id' => 753,
),
206 => 
array (
'answer_text' => 'Hello, 

I plan to be in attendance at the Green Fund meeting on February 20th for the given time. I did not see any specific questions in this form, however, if there are any I can answer before the meeting, please let me know. 

Best Regards,
Trevor Ledbetter',
'created_at' => '2017-02-03 20:58:24.000000',
'question_id' => 751,
),
207 => 
array (
'answer_text' => 'I would love the opportunity to discuss my proposal with the Green Fund Committee, but unfortunately, I will be out of town on the 20th. It looks as if you cannot change the meeting time, but if you can, I can meet before I leave on the 15th, or after I return on the 21st. I would also be happy to discuss the proposal over the phone, if that is a possibility.

I will take this opportunity to say that I think my proposed project will give a lot of students a chance to be exposed to real research. I find this to be hard to come by for a lot of undergraduates at UA. I recently received a grant from the Wallace Research Foundation to develop an undergraduate research fellowship, and this project would dovetail perfectly with that, allowing us to leverage support from both entities. I am committed to providing robust opportunities to undergraduates, and I think they can make they difference when it comes to getting jobs or going on to graduate school. I also think that this project will be high-profile, and our goal will be to create a buzz across campus. I can\'t tell you how many people are interested in lizard behaviors. I am always fielding questions about what lizards are present, and what their curious behaviors are all about. I also think the signs can be a really cool addition to campus. I really want to make sure that ecological sustainability is a part of environmental sustainability. After all, the two are intimately connected.',
'created_at' => '2017-02-03 22:35:28.000000',
'question_id' => 748,
),
208 => 
array (
'answer_text' => 'The tables would remain.  The garden would be built around the tables and likely on the perimeter so those sitting at the tables can enjoy the garden.  Many students utilize the seating there so we would not want to remove it but allow for garden to be built around the seating.',
'created_at' => '2017-02-04 16:17:04.000000',
'question_id' => 736,
),
209 => 
array (
'answer_text' => 'The purpose of the mobile cooking cart is 2-fold.  First, it allows us to be nimble in our ability to offer cooking demonstrations and nutrition workshops in a variety of locations.  If we had the cart we would not have to take various pieces of cooking equipment to remote locations or only offer such demonstrations in one stationary location.  Second, because of the mirror on the cart, it allows for larger groups to see the demonstration so we would potentially be able to reach more students in one demonstration or workshop.  The intention would live without the cart but the ability to reach more students effectively may be affected.',
'created_at' => '2017-02-04 16:19:04.000000',
'question_id' => 736,
),
210 => 
array (
'answer_text' => 'The Student Union has been collaborating with UA Master Gardeners and does hope to collaborate with the UA Community Garden in the future.  The Student Union recognizes that the UA Community Garden has valuable experience in crop selection, care of crops, and harvesting of the crops.  Like the Master Gardeners, the UA Community Garden\'s expertise will be a welcomed addition to collaborate with the SUMC gardens.',
'created_at' => '2017-02-04 17:35:30.000000',
'question_id' => 735,
),
211 => 
array (
'answer_text' => 'Sorry if this a repeat response - I received a blank screen when I submitted my response.

The Student Union is collaborating with UA Master Gardeners and does hope to collaborate with UA Community Garden in the future.  The Student Union recognizes that the UA Community Garden has valuable experience in crop selection, care of crops, and harvesting of the crops.  Like the UA Master Gardeners, the UA Community Garden\'s expertise will be welcomed as a collaboration for the SUMC garden.',
'created_at' => '2017-02-04 17:39:44.000000',
'question_id' => 735,
),
212 => 
array (
'answer_text' => 'Currently, the rooftop garden is very small and not viewable to the general student body to enjoy.  We would like to still use the rooftop for some gardening but then also hope to include the Pangea patio so students are able to see and enjoy the garden.  The other benefit to including the patio is that concerns of weight bearing on the roof are eliminated.  The rooftop garden case competition (that is kicking off this month) also includes the food stall on the Mall north of the Nugent building as an option as well.  Different types of garden infrastructure could be used at each location allowing for a variety of sustainable options to be utilized.',
'created_at' => '2017-02-04 17:47:05.000000',
'question_id' => 735,
),
213 => 
array (
'answer_text' => 'The mobile cooking unit could be used for other cooking classes or nutrition workshops given by the Student Union.  The hope would be that we could use the cart within SUMC,throughout campus in areas including the mall or canyon, and other buildings including Park Student Union.',
'created_at' => '2017-02-04 17:52:22.000000',
'question_id' => 735,
),
214 => 
array (
'answer_text' => 'I will attend. -Chet',
'created_at' => '2017-02-05 02:42:38.000000',
'question_id' => 754,
),
215 => 
array (
'answer_text' => '1) Do you see a time in the near future in which athletics will pay for the program?

We are hoping to find outside sponsors that will be the ones actually paying for the program as soon as next year, and we have a list going of which companies might be interested. Unfortunately, based on other schools\' experience and what we have seen from our own Athletics Department, unless we have outside funding to Athletics for green initiatives, it will not be funded. We would be working with the Office of Sustainability Green Team to have both of our groups funded to continue our Zero Waste Collaboration.
On a better note, our work at the athletic events is being noticed. Quite frequently at this season’s basketball games, we have had a large amount of students and fans come up to us and thank us for what we do. Along with this, many of them are asking us questions like, what happens to the recycling at the game you don\'t work? Or even why isn\'t there more composting on campus? Students and fans look for us at these games and make sure to bring us their waste. It is amazing to see their face light up when they prove to you they know how to sort their waste. With what we do being clearly important to paying fans and all other PAC-12 schools starting and beating us in their zero-waste programs, it is only a matter of time. In order for the UA to keep their fan base up and compete competitively for students, they need to get onboard with our program.



2) What will be your pitch for the new athletic director to get him/her engaged and financially committed to the program?

One idea we have to pitch this concept, besides the monetary supply we would have to bring in, is that this is something students are interested in. One fact of our generation is that less people feel a part of the football community. So, if Athletics provides the people and stations of Zero Waste games, they would appeal to people in the crowd who care for the environment, making the stadium more welcoming to them. This could increase ticket sales and diversify the football audience, while also eventually spreading environmental awareness to the rest of the fans.
I think it is important to stress the observations I stated in question one. The Athletic Director do not interact with the students and fans as we do. I think it is important to show how important we are not just in terms of sustainability, but in business. The new Athletic Director needs to know that what we do is important to the students and fans. We also need to rub in his/her face that we are behind. ASU is kicking our butt in zero-waste and once we put it in terms of it being a competition with other PAC-12 schools, it is only matter of time till UA gets pressure to join in.
It won\'t matter if she/he feels sustainability is important, it will just simply be a very wise business plan for her/him. sustainability, sadly, isn\'t important to all. The way to make sustainability stick with others is to make it personal. Whether that is by saving money, or bringing in more money, as long as it aligns with their goals, they will take it on. We will just need to make sure to marketing what we do in all different directions to assure that the new Athletic Director gets that what we do is important and can help her/his goals.',
'created_at' => '2017-02-05 02:45:30.000000',
'question_id' => 740,
),
216 => 
array (
'answer_text' => 'Yes we will be available to present to the Green Fund Committee at 5:20pm on February 20.',
'created_at' => '2017-02-06 17:42:56.000000',
'question_id' => 750,
),
217 => 
array (
'answer_text' => 'We are hoping to fund two, but we would be quite happy with funding for one.',
'created_at' => '2017-02-09 17:49:05.000000',
'question_id' => 757,
),
218 => 
array (
'answer_text' => 'We hope to send the move-out project student lead to AASHE to present on our program.  A self-funding waste diversion program is not commonplace.  Also, there is a new off-campus piece we\'re trying to start which is very unique that would also be part of the presentation.  By tying the two pieces together, we feel this is a program that no one else in the nation is running.  We hope to share that information so other schools can participate.',
'created_at' => '2017-02-09 18:00:11.000000',
'question_id' => 758,
),
219 => 
array (
'answer_text' => 'The incentive for Greek houses to participate is that they are receiving new low-flow showerheads, aerators and CFL lights.  The executive board that runs the house also learns about practices they can implement to reduce their resource use which is a need that has been expressed to me.  If that doesn\'t answer your question please let me know and if you have any other questions please let me know.',
'created_at' => '2017-02-10 02:21:23.000000',
'question_id' => 756,
),
220 => 
array (
'answer_text' => 'We are working on adding a new, innovative piece to the move-out project (Ditch the Dumpster).  We hope to add an off-campus component (mini grant pending), coordinated in part by the Green Fund supported Off-Campus Housing Sustainability program.  We hope to present about this exciting project in its entirety.  The goal would be to have the on-campus move-out student lead attend and share his knowledge about the move-out program planning process.  We hope to also send one of the Off-Campus Housing sustainability Coordinators to speak to the off-campus piece as well.  Finally, we will invite Jeff Wilson to come, as he is our partner at the Surplus Store.  We believe this program, particularly since we\'re adding the off-campus component, is unique enough to be accepted as a presentation at AASHE.  While Julia and I can discuss the project, it is important to us to give students an opportunity to add national presenting to their skill sets and resumes.',
'created_at' => '2017-02-10 22:51:29.000000',
'question_id' => 755,
),
221 => 
array (
'answer_text' => 'A certificate for completing the program or getting a grant funded is definitely something we can do. Disseminating information through the avenues of the UA Community and School Garden Program will be the primary way of letting students know about the grant program. There will be presentations at Tucson High as well to inform students and answer any questions they may have. Other conventional methods of spreading information would also be used; emails, posters etc.',
'created_at' => '2017-02-12 20:31:13.000000',
'question_id' => 759,
),
222 => 
array (
'answer_text' => 'We reached out to 12 classes that we found the on green course guide, 5 of which responded and welcomed us to present. The classes visited had a sustainability-related focus from sustainable cities with Professor Pivo to sustainability in education with Professor Johnson. The total number of students reached thus far is about 500, with one more presentation planned for March, adding an additional 105 students.',
'created_at' => '2017-02-13 19:16:54.000000',
'question_id' => 758,
),
223 => 
array (
'answer_text' => 'I have explored other sources of funding, including potential sources through the University of Arizona Foundation. Unfortunately, the Foundation wasn\'t able to match our program with a potential donor. However, I have been talking with the Student Engagement Coordinator for CALS about long-term funding for our program. While we are not able to secure funding currently, there is a keen interest among CALS administrators in providing this experiential opportunity to students. She notes "The fact that the Externships program places students in experiential learning opportunities throughout AZ is a real draw. Everyone I talk with – in CALS administration, in the central Office of Student Engagement, in UA Marketing – sees the value of the Externships program." I am committed to pursuing longer-term, permanent funding working with the Office of Student Engagement.',
'created_at' => '2017-02-16 18:34:33.000000',
'question_id' => 765,
),
224 => 
array (
'answer_text' => 'As the proposal states, the employee would be integrated into FM\'s overall event planning team, so that they can work to integrate zero waste efforts as much as possible into as many events as possible.  This includes Spring Fling, the Tucson Festival of Books, and other smaller campus events on the Mall and in other locations.  It\'s hard to say how many events, and how quickly, the position could integrate zero waste efforts.',
'created_at' => '2017-02-16 19:50:36.000000',
'question_id' => 771,
),
225 => 
array (
'answer_text' => 'There is no certainty in any of this.  FM can be encouraged to cite Green Fund as its source of funding, but FM will be supervising this position.  However, FM will need to work with many other groups to successfully accomplish any zero waste events.  A one-person zero waste staff will not get much done, without working with student initiatives, Office of Sustainability, and other partners.  This proposal is merely asking for funding for a coordinator within FM to work with these groups in a dedicated way.  These partners will be in a position to influence the overall "narrative" through their work with FM, and with any communications platforms that result from those efforts.',
'created_at' => '2017-02-16 19:53:57.000000',
'question_id' => 771,
),
226 => 
array (
'answer_text' => 'Chris Kopach, assistant VP for FM, has committed to sustaining funding for the position after Green Fund funding ceases.',
'created_at' => '2017-02-16 19:54:54.000000',
'question_id' => 771,
),
227 => 
array (
'answer_text' => 'FY19 budgeting will happen in FY18, so they technically cannot have "budgeted" for the position at this time.  Additionally, FM does not currently have a funding model to fund this new position.  They are committing to develop such a funding model for the position over the next two years.  It is often easier to justify creation of a new position to upper administrators after they can see what that position looks like and what it accomplishes.  This is what Green Fund is doing - providing the seed funding for FM to show proof of concept and value to the university, to justify proposals for funding from the general university budget in the future.',
'created_at' => '2017-02-16 19:57:19.000000',
'question_id' => 771,
),
228 => 
array (
'answer_text' => 'I hope that the energy manager will be able to spend the money allocated for the current fiscal year by the end of the fiscal year.  However, this may not be feasible, and if it is not, I will work with the energy manager to submit a PAR to extend into next fiscal year.  

If the energy manager does spend all the money, or if the need for funds exceeds what we can carry over into next fiscal year with remaining funding, this additional allocation will provide the funding for those efforts.',
'created_at' => '2017-02-16 20:00:28.000000',
'question_id' => 771,
),
229 => 
array (
'answer_text' => 'There is no plan as yet for the program expenses, unfortunately.  The energy manager had not started yet by the time the proposal was due, so it was submitted without a plan.  At this point, the energy manager is still "drinking from a firehose" learning many basics about the UA energy/utilities infrastructure.  He is aware of this funding, and I\'ll be working with him to develop ideas/plans for this funding moving forward.  However, he nor I have specific plans at this time, and won\'t until he has done some benchmarking exercises to identify areas of need/opportunity within campus infrastructure.

So, if you want to fund this, it will be on the basis of speculation and desire to generally support the initiative, not based on specific activities or planned outcomes.',
'created_at' => '2017-02-16 20:03:42.000000',
'question_id' => 771,
),
230 => 
array (
'answer_text' => 'The spreadsheet description in parentheses isn\'t the whole story.  The amount listed there assumes a full 20hr/week 0.5FTE GA position.  I\'m proposing a 10hr/week, 0.25FTE GA position.  That means, half the tuition reimbursement.  So, the proposed tuition reimbursement is for the entire year, at a half-time (10hr/week) rate.',
'created_at' => '2017-02-17 00:12:06.000000',
'question_id' => 778,
),
231 => 
array (
'answer_text' => 'The setbacks from this year mostly involve directly hiring students for community outreach, as well as organizing off-campus tours.  These elements of this year\'s funding have been beyond my/our time availability to coordinate.  I overestimated our capacities when I proposed this project last year.  However, the aspect that is moving forward successfully, though a bit later than originally planned, is the part involving internships with local government.  This year\'s GA is successfully establishing, recruiting, and overseeing internships of students for spring semester.  He is also organizing some guest talks on UA campus by some of the hosts of these internships to increase interest among students in potentially seeking internships with them in the future.  This will also help with overall campus awareness of sustainability implementation in the broader Tucson community.

Without Green Fund allocation, there will be no funding to support a GA to continue this program next year.  If Green Fund declines to fund this proposal, I will seek a PAR to realign unused funds from this year for hiring a GA next year.  If Green Fund agrees to fund this proposal, I will likely return all or most of the unspent funds from this fiscal year, although may submit a PAR to keep a small portion over to further support the local government intern program.  If Green Fund declines to fund this proposal, and rejects a PAR to extend current funding for a GA for next year, then I\'ll shut down the program.',
'created_at' => '2017-02-17 00:18:14.000000',
'question_id' => 778,
),
232 => 
array (
'answer_text' => 'The MycoCats club has a "mushroom shed" at the Tucson Village Farm, where multiple Tucson elementary school classes go for field trips every week. We will coordinate with this farm, a subset of Pima County Cooperative Extension, to program mushroom sustainability field trips. 

In addition, we plan on participating in the UA Community and School Garden Program, which brings gardening workshops to local schools.',
'created_at' => '2017-02-21 00:20:15.000000',
'question_id' => 766,
),
233 => 
array (
'answer_text' => 'The project described in the proposal actually has not been done before. In the past, Mycocats simply focused on experimenting with sustainable mushroom cultivation. Therefore, we haven\'t attempted the project described in the proposal without funding.',
'created_at' => '2017-02-21 00:27:57.000000',
'question_id' => 767,
),
234 => 
array (
'answer_text' => 'The curriculum will be developed from concepts and data from primary sources(journal articles and letters) and textbooks. We will use Arizona Department of Education standards for science education to determine how to best cater our presentations to each grade we are teaching.',
'created_at' => '2017-02-21 00:55:16.000000',
'question_id' => 769,
),
235 => 
array (
'answer_text' => 'We are actually now requesting $2800. The reason for this change will be explained below. We are requesting mushroom growing supplies in order to provide workshop attendees with ready-to-grow substrate bags, bags pre-inoculated with spawn that will grow with little maintenance. We have subtracted $1200 from the amount requested because we no longer need to purchase an industrial shredder. We stumbled upon an abandoned industrial shredder at Tucson Village Farm. The materials we are requesting along with their prices are listed below:
150 Petri dishes - $150.00 
Grain and straw - $200.00 
Gloves, masks, aprons - $50.00 
Dehydrator - $2,000.00 
Fungal growth media - $300.00 
300 Substrate bags - $100',
'created_at' => '2017-02-21 01:44:45.000000',
'question_id' => 770,
),
236 => 
array (
'answer_text' => 'Spent substrate, which is the material on which mushrooms have grown, has shown to improve soil and compost quality. In fact, the spent substrate that we end up with is composted at Tuscon Village Farm. We will encourage everyone who participates in our workshops to compost their spent substrate by directing them to places where they can compost their substrate around campus. In addition, we will provide information about composting to the elementary school students who participate in our program and their parents.',
'created_at' => '2017-02-21 01:57:32.000000',
'question_id' => 770,
),
237 => 
array (
'answer_text' => 'I think I misunderstood the question in my first response to this question. The committee\'s concern with plastic waste is valid. It is difficult to find a more sustainable way of containing substrate because we need a pliable, non-porous material that can be heat-treated. So far, the only material that we have found to fit these criteria is polypropylene. However, we will do our best to find more eco-friendly materials. A model we might be able to follow for a compostable muhsroom kit is that of Uncommon Goods, a company whose mushroom kit has mushrooms growing out of a cardboard box.',
'created_at' => '2017-02-21 02:59:33.000000',
'question_id' => 769,
),
238 => 
array (
'answer_text' => 'Given the past success of the Department of Planning, Design & Construction working with Facilities Management, we are optimistic that we can create a solution to this issue. In-person meetings have previously been effective at describing and obtaining needed data. We have approached the newly hired Energy Conservation Manager at FM, a position supported by the Green Fund, and he was receptive to working with us on Phase 2.',
'created_at' => '2017-02-21 17:54:13.000000',
'question_id' => 760,
),
239 => 
array (
'answer_text' => 'The information provided about ongoing and upcoming events will be as detailed as possible. We aim to include times, locations, relevant organizations, specific topics, and more for each event. Contact information can be included or excluded on a case-by-case basis, if for some reason a host does not want to provide this information. The idea is to provide views of events the current day, upcoming events, and recent past events. The concept is to provide similar info as with typical event calendars, but through a spatial view. You would be able to see what symposia or colloquia are happening over the lunch hour in the area you are on campus on a particular day, for example.',
'created_at' => '2017-02-21 17:55:50.000000',
'question_id' => 761,
),
240 => 
array (
'answer_text' => 'Throughout Phase 1 of this project, we have created a wide network of contacts involved in Sustainability. For project guidance, there have been direct, formal ties to the Office of Sustainability (Benjamin Champion, Julia Rudnick), Students for Sustainability (Chet Phillips), the Institute of the Environment (Angie Brown, Stephanie Doster), the Water Resources Research Center (Claire Zucker), Residence Life (Jill Burchell), the Department of Planning, Design & Construction (Grant McCormick, Drennen Brown), and the College of Agriculture and Life Sciences (Matt Rahr). There are additionally many connections between project members and members within other UA organizations (Department of Soil, Water, and Environmental Science; School of Natural Resources and the Environment; Department of Hydrology, etc.). We also have contacts with a number of sustainability organizations in the public and no-profit sectors in Tucson.',
'created_at' => '2017-02-21 17:57:08.000000',
'question_id' => 762,
),
241 => 
array (
'answer_text' => 'The map will (and does a bit already) cover research being done on campus. The limitations here are obtaining information that is not readily available – we hope that by establishing a quality application, we will encourage those performing the research to contribute a description and photo directly to us, streamlining the process of keeping the database and map up to date. This subject will be developed much more once we add the “people” component since research endeavors are often tied to a particular team or PI.

There are many features on the map that are related to community-level impacts (like Community Gardens and the CompostCats effort throughout the region)  and with the inclusion of events in the future we hope to extend our community reach even further. For example, sustainability programs abroad affiliated with the UA could be shown on the map.',
'created_at' => '2017-02-21 17:58:23.000000',
'question_id' => 763,
),
242 => 
array (
'answer_text' => 'The application is being polished to squash bugs and improve user friendliness constantly, and will continue to be updated and fixed as necessary so long as funding allows. The current version was optimized for mobile devices, so while it works on tablets and desktop computers, it does not look/work like typical applications for those devices. The intent of Phase 2 is to adapt the app so that when opened on a tablet or desktop, the app will respond to the particular device to provide the optimal experience for that device. Regarding information that is provided in the window after clicking on an icon, there is much room for growth in the content provided. This is not limited by the technology, rather, we can readily add more content as it comes available.',
'created_at' => '2017-02-21 18:00:24.000000',
'question_id' => 764,
),
243 => 
array (
'answer_text' => 'Yes, the PAR is requesting moving funds currently allocated in student employment/ERE to operations to provide student stipends.',
'created_at' => '2017-10-23 18:13:49.000000',
'question_id' => 781,
),
244 => 
array (
'answer_text' => 'Great question! We are currently networking with academic advisors in multiple university departments (geology, SNRE, and environmental studies) to introduce a newly formatted volunteer program, situated through the Cooper Center, for students with an interest in science or informal teaching. This would be an ongoing event for many students throughout the school year. We are also connecting with the geology department to have retreats and professional developments out with all that lovely access to rocks of various kinds and hiking as far as they might want to explore.

Additionally, we have begun a relationship with the curator and program coordinator at the UA Museum of Art to host environmental art events for UA students at the Cooper Center.  We are starting a yoga program there as well, and are planning to collaborate and host future versions of these environmental art and health (yoga) programs with related committees in Students for Sustainability.  We also  have a connection to the teacher of the Environmental Journalism class who is considering a field trip opportunity at the Cooper Center or an environmental poetry event in the fall.  The cultural centers on campus have also opened doors to us, and we will be helping them with painting pots for clean air plant gardens throughout their locations. A few honorary student clubs and groups will be hiking out at Cooper and experiencing, and hopefully appreciating, access to quality Native foods as well!

Furthermore, we are using two of our current events, the sustainable kitchen events with Greek Life, to spark the potential of a campus-oriented, points-based competition between Greek Houses. We are currently preparing a survey that will be presented to the house representatives at these events that will inspire greater leaps on their journeys toward being sustainable units in our sustainable campus community. It will also give us the information we need to know how to operate and support them in terms of where they are. We believe that success in this arena will open doors to multiple sustainable and environmentally-friendly teaching events as well as be a fun event in itself!  All of these events are operating in the direction of helping to grow sustainability initiatives on campus through both education and experience!',
'created_at' => '2018-02-06 01:13:54.000000',
'question_id' => 787,
),
245 => 
array (
'answer_text' => 'Hey, I am not Jill, and while the words in the question are related to my application and the question is relevant to my proposal, the Open Energy Monitoring Software in Residence Halls is not my initiative.  Please clarify?  Thank you!',
'created_at' => '2018-02-06 01:16:54.000000',
'question_id' => 787,
),
246 => 
array (
'answer_text' => 'Great question! We are currently networking with academic advisors in multiple university departments (geology, SNRE, and environmental studies) to introduce a newly formatted volunteer program, situated through the Cooper Center, for students with an interest in science or informal teaching. This would be an ongoing event for many students throughout the school year. We are also connecting with the geology department to have retreats and professional developments out with all that lovely access to rocks of various kinds and hiking as far as they might want to explore.

Additionally, we have begun a relationship with the curator and program coordinator at the UA Museum of Art to host environmental art events for UA students at the Cooper Center.  We are starting a yoga program there as well, and are planning to collaborate and host future versions of these environmental art and health (yoga) programs with related committees in Students for Sustainability.  We also  have a connection to the teacher of the Environmental Journalism class who is considering a field trip opportunity at the Cooper Center or an environmental poetry event in the fall.  The cultural centers on campus have also opened doors to us, and we will be helping them with painting pots for clean air plant gardens throughout their locations. A few honorary student clubs and groups will be hiking out at Cooper and experiencing, and hopefully appreciating, access to quality Native foods as well!

Furthermore, we are using two of our current events, the sustainable kitchen events with Greek Life, to spark the potential of a campus-oriented, points-based competition between Greek Houses. We are currently preparing a survey that will be presented to the house representatives at these events that will inspire greater leaps on their journeys toward being sustainable units in our sustainable campus community. It will also give us the information we need to know how to operate and support them in terms of where they are. We believe that success in this arena will open doors to multiple sustainable and environmentally-friendly teaching events as well as be a fun event in itself!  All of these events are operating in the direction of helping to grow sustainability initiatives on campus through both education and experience!',
'created_at' => '2018-02-06 01:17:24.000000',
'question_id' => 788,
),
247 => 
array (
'answer_text' => 'I really appreciate this question since I have often been considering the need for the results of this project, intended to be funded for 5 consecutive years, to continue on beyond the 5 year mark in a self-sustaining way.  Here are some things I have concluded with the help of my supervisors, counterparts, and coworkers at the Cooper Center.

First, our work this year has built several strongholds and even if I am the only one who remains from this years\' team on next year\'s projects, there is something to be said for continuity. No team member has been on this project for more than 1 year, so the continuation of any staff members on this project will be in our advantage in continuing the growth of this project and making its contributions to campus self-sustaining.

To explain this point further, I have found that a well-selected small team of student workers learns quickly about how to share and shape sustainable lifestyles on campus. We have worked hard to maintain our parameters of focus as each project should fall within the realm of education and be connected to the Cooper Center. If team members remain with this project throughout next year, our experience will allow our efforts and timeline to become more efficient. That efficiency and good use of resources will create many more events and activities, and we will work within our campus community with increased knowledge, awareness, and determination for our jobs. If team members do not remain, we have left a "paper trail" online for future team members to follow and learn from our experiences.

Much of that paper trail includes the lessons we have learned this year. It also includes the building up of our contact list through networking, a database of ideas and resources for teaching projects, and an efficient way of recording our efforts in a collaborative online manner.  We have also learned to match our ideas with the right people and guide them toward the most relevant sustainable education projects, including both teachers and student organizations. Additionally, we have improved the root efforts, including formalizing job expectations for our student workers and formatting our sustainable and educational ideas professionally and communicably, which has improved our teamwork and outreach capacity. We hope to become a campus resource so that student organizations can rely on the Cooper Center as well as our team\'s collection of ideas and educational resources to help them fulfill their sustainability initiatives as well. 

Most important for the self-sustaining parts of this project is to help the Cooper Center become known throughout UA campus as an educational resource for students, organizations, and teachers alike. Education is central to learning and changing, and people who experience the Cooper Center\'s concepts of earth education, which are science-based and realistic while its programs are delivered in a way relevant to how people learn psychologically, recognize the value of its existence. We hope to increase the value of this kind of knowledge across campus by introducing as many UA organizations as possible to the Cooper Center and connecting them to a program at Cooper that correlates to their field of interest. Examples of these events and this effort are in the answer to another question within this system. 

Initial success of these events, while this project is in motion over the next year, is paramount to events becoming self-sustained, but so also is figuring out how best to reach the groups with the ideal resource, event, or activity that they will want to pursue over time with us. This is where our survey effort, currently geared to Greek Life, comes into play. As we continue networking throughout UA departments and campus organizations, we may also apply a survey to them so that we know best how to serve and orient a Cooper Center or environmental learning event to their needs. Recognizing that this survey method is not safe from failure, there are other ways, such as the most in-person way of collaborating through meetings and efficiently moving from ideas that do not work toward ideas that do.  Finally, we are happy to say we have maintained good follow-up communication with our contacts and project partners, which is really important to maintaining these partnerships and building them into self-sustaining events and activities.',
'created_at' => '2018-02-06 02:36:04.000000',
'question_id' => 789,
),
248 => 
array (
'answer_text' => 'Students for Sustainability is currently supported with a full-time advisor through ASUA. ASUA advisors have multiple program and service areas that they oversee. Due to the many competing demands of the current ASUA advisor who oversees SFS as well as the impact and breadth of programming that incorporates SFS, the advisor is unable to dedicate the time and attention required to fully support SFS and its initiatives such as attending meetings, identifying outside funding sources, creating and enhancing new and existing programs. We (Student Governance and Programs/ASUA/SFS) are requesting a graduate assistant to assist in supporting SFS members and directors who deserve an advocate that is able to provide the time and dedication to the program for it to continue to thrive in addressing sustainability issues on campus and in the Tucson community. Please see the position description attached as a supplemental document for more information on who the GA reports to and the office that this position would fall under.

To provide further clarification, Student Governance and Programs is the department that oversees ASUA and GPSC. The staff of Student Governance and Programs advises and supervises all programs and services within ASUA and GPSC, including Students for Sustainability.',
'created_at' => '2018-02-06 23:05:27.000000',
'question_id' => 782,
),
249 => 
array (
'answer_text' => 'Marketing expenses:
1. Fliers (18x22 100% recycled content): $50
2. Door Hangers (1351 rooms): $450

Technology expenses (replicated 10 times for each set-up):
1. Computer and sensors (temperature and humidity): $225
2. Flexible CT sensor ($250, need 3 per location): $750',
'created_at' => '2018-02-07 23:45:45.000000',
'question_id' => 786,
),
250 => 
array (
'answer_text' => 'The money for marketing and outreach will be used to teach students about the software.  People must know where to get the app, how to get it set-up for their building, etc.  We discussed making door hangers that promoted the app on one side and gave tips for how to reduce energy use on the other.  We also want to post a small number of larger fliers in hall common rooms (places they would be holding hall council meetings, for example) so folks have access to the name of the app and instructions no matter where they are.  These would go out at the same time that the Housing funded Battle of the Utilities marketing was being disseminated.  We hope they will work in conjunction with one another.',
'created_at' => '2018-02-07 23:46:22.000000',
'question_id' => 785,
),
251 => 
array (
'answer_text' => 'This software is used residentially to commercially, all across the globe.   That being said, we could not find universities in the US using this software in the forums.  However, there were some in South America and France.  I did not reach out to them specifically based on some clear barriers.  However, in one of the forums, someone from the engineering school in Brittany, France spoke of his search to find products that could measure, store, and track the energy consumption data of a typical university campus.  After doing much research , including many proprietary products, he found “a very powerful, web-connected energy monitoring system.”  The entry concluded with “The entire team working on the project at Telecom Bretagne is convinced of the great quality of the openenergymonitor project and we hope our work may be useful in any way.”
Looking at some of the proprietary energy monitoring software, like Lucid Designs, we find repeated evidence of schools using this type of software to enhance energy reduction competitions.  Gonzaga, for example, saved over $20,000 in three weeks by reducing over 15% of total energy expenditures in 77 buildings.',
'created_at' => '2018-02-07 23:46:57.000000',
'question_id' => 784,
),
252 => 
array (
'answer_text' => 'It is possible that this program could be successful if partially funded, but it will likely be more difficult to achieve the successes.  Consider the first use for the software: enhancing Battle of the Utilities.  The competition is set up to run between all buildings in a district.  Therefore, if only some of the buildings have the new software, those buildings will have distinct advantages during the competition.  They will be able to engage their residents in real time, bring the competition into their hands and programs through the app, and have results immediately at the end of the month.  In contrast, the other buildings will be unable to participate in the same ways.  This will lead to the need for different types of coaching and marketing between the buildings and their staffs.  I believe students in buildings without the software will feel they are at an unfair disadvantage. 
Another potential pitfall is allowing Housing to pilot fewer types of buildings.  We have older buildings in the Highland district, through one of our newest.  There are buildings with both two and four pipe systems in the district.  There are even a couple of buildings that function as complexes.  In short, Housing can test the impact of temperature and humidity sensors in many different types and sizes of buildings.  They can learn from this pilot. It will likely be easier to make the case to Housing to expand (and fund) the program to the entire campus if they have tested the equipment in all types of environments and they are confident in the efficacy, no matter what the building type.',
'created_at' => '2018-02-07 23:47:34.000000',
'question_id' => 783,
),
253 => 
array (
'answer_text' => 'The intern application process consists of three components: 1) submitting an online application; 2) submitting a resume; and 3) participating in a phone interview.  The information collected through these three mechanisms is evaluated by program staff based on the following criteria:

•	Experience and/or interest in environmental science outreach
•	Knowledge of environmental science in general and biodiversity in particular, or a clear commitment to gain the knowledge necessary to be successful
•	Ability to be self-guided and to work independently, as well as ability to work cooperatively in a group setting
•	Proven or expressed commitment to fostering diverse and inclusive communities and educational spaces
•	Experience participating in social justice-based activities (i.e., activities that aim to create more equitable distribution of resources, wealth, and opportunities in society and/or that address existing inequalities)
•	Relationship between the internship objectives and framework and the student’s overarching career/personal goals and objectives
•	Availability to attended weekly intern meetings and engage in weekly outreach activities at schools',
'created_at' => '2018-02-09 18:07:56.000000',
'question_id' => 795,
),
254 => 
array (
'answer_text' => 'All of the proposed positions are part-time positions. The classified staff position currently exists and is funded via grant funding, but only through June 2018. The position outlined in the proposal, would increase the position from .5FTE to .75FTE.  The student worker positions do not currently exist. Funding would enable us to employ 2-4 work study eligible students at 8-15 hours per week each during the academic year.  The supplemental comp would fund the Program Director’s position for approximately 12.5 hours/week for 8 weeks during the summer as the person who currently holds this role is on an academic appointment that only extends from mid-August-mid-May.  Additional details on the positions and associated responsibilities are below.

Classified Staff Regular: This position is for a .75 FTE Program Coordinator who oversees all of the logistics of the program.  This position currently exists (via grant funding that ends June 2018) at .5 FTE.  We are requesting funds to both sustain this position and increase it to .75 FTE because additional time/work will be required to expand the scope of the program in terms of number of student interns and number of outreach sites.  The general duties of this employee include: 
•	Serve as the primary point of contact between interns, K-12 partner teachers, and partner organizations
•	Co-teach the weekly internship class on the UA campus
•	Coordinate fieldtrips and classroom visits to and from partner organization sites for interns and K-12 classes
•	Collect pre/post-program data from K-12 student participants twice per semester
•	Recruit intern applicants each semester by engaging in campus-wide outreach
•	Participate in the intern review and selection committee
•	Match intern and partner teacher schedules so that each intern is placed in a K-12 classroom in accordance with their own school and work schedule, as well as the teacher’s classroom schedule
•	Provide weekly oversight and guidance to interns, including performing periodic classroom observations and giving interns individualized feedback on their teaching strategies
•	Provide interns with a resume and cover letter writing workshop in order to enable them to utilize their intern experience to obtain other jobs and relevant experience
•	Build and sustain relationships with partner departments and units across the university campus

Student Employees: We are requesting $3000 annually that we will use to fund part-time (8-15 hours per week) student employees as part of the expansion of the program.  These are new positions and will be for work-study eligible students in accordance with our overarching programmatic goals of expanding work experience and opportunity to students from groups traditionally underrepresented in the environmental sciences – which includes a disproportionate number of students from lower-income backgrounds.  We aim to fund anywhere from 2-4 students per academic year; the total number of student workers will depend on their availability.  These student worker positions will provide an additional way for UA students to participate in the Bio/Diversity Program and gain important training, mentorship, and work experience that will contribute to their development as environmental leaders and advocates. The primary responsibilities of these student workers will be:
•	Design, organize, and implement campus wide events 2-4 times per year that help increase awareness about: urban biodiversity and biodiversity conservation; UA and local sustainability efforts that foster biodiverse environments; environmental science-related job and career opportunities; and the importance of fostering diversity in the environmental sciences. These events could include: bioblitzes, career panels, and symposiums about diversity and environmentalism.
•	Collaborate with staff at partner organizations on campus-wide events and community outreach events 
•	Develop relationships with cultural centers and student groups across campus to raise awareness of the program and collaborate on campus-wide events
•	Support program interns as needed 

Supplemental Comp: We are requesting $4000 annually for supplemental compensation for the program Director (who is on an academic year appointment). The $4000 requested will be used to cover part-time summer employment for the Director for 8 weeks, as programmatic responsibilities extend beyond the academic year timeline. The primary duties during this time will be:
•	Organizing and analyzing data collected during the academic year (this includes pre/post testing with K-12 students and interviews with university level students)
•	Using data collected to write program reports and funding applications in collaboration with partner organizations.  
•	Updating program website and curriculum materials in response to evaluation data collected
•	Participation in internship application review and selection committee
•	Supervision of Program Coordinator and other program staff',
'created_at' => '2018-02-09 18:12:15.000000',
'question_id' => 794,
),
255 => 
array (
'answer_text' => 'The proposed model of expansion of the program would not be possible without the full funding requested. However, there are two different avenues we think are feasible with partial funding.

Possibility 1: We could sustain the program at its current level (23-25 UA student interns per year, with potential growth to 30 students annually) and develop 2 annual on-campus events with partial funding, increasing the overall UA student impact. In this situation we could reduce the Program Coordinator salary to .5FTE (a reduction of about $11,000 annually between salary and ERE) and reduce the student worker positions to $2000 annually. This would also allow us to reduce the number of teacher participants each year by 2-3 so that would bring down the amount requested for teacher stipends by $200-300 annually.  Total estimated reduction: $13,250/year, $39,750 over three years.

Possibility 2: We could expand the program over 1-2 years instead of the requested 3 years of funding. This would reduce the overall student impact and potentially present some challenges to achieving the long-term economic sustainability of the program (due to a more consolidated timeline in which to collect outcome data and prepare funding proposals). However, we believe that the program would still be successful in achieving the overarching goals of increasing environmental knowledge, fostering environmental stewardship, and increasing diversity and we would still engage in the fundraising/grant writing activities outlined in the proposal with the goal of achieving long-term sustainability.  Total estimated reduction: $48,988-$96,880 depending on the number of years funded.',
'created_at' => '2018-02-09 18:19:46.000000',
'question_id' => 796,
),
256 => 
array (
'answer_text' => 'Green Fund Committee,
I would be glad to organize an externship symposium, but would need some assistance from the Office of Sustainability in setting up the date, venue and promotion of the event, since I am located off campus. We have done this a few times and didn\'t have a very good turn-out. Nonetheless, if promoted well, it would be a great opportunity to engage new students and give the externs the chance to talk about their projects.',
'created_at' => '2018-02-15 18:07:44.000000',
'question_id' => 814,
),
257 => 
array (
'answer_text' => 'Green Fund Committee:
Here is the list of this summer\'s projects and the project manager for each one:
1.	Local Food and Beyond - Growing the Connections in Graham County: Safford, Graham County Extension
contact: Bill Brandau (email: wbrandau@cals.arizona.edu)
2.	Tucson Village Farm Seed to Table Program (4 positions): Tucson, Pima County Extension
contact: Elizabeth Sparks (email: esparks@cals.arizona.edu)
3.	Kitchen Garden: Flagstaff, Coconino County Extension
contact: Hattie Braun (email: hbraun@cals.arizona.edu )
4.	Yavapai Farm and Food Education, Prescott, Yavapai County Extension
contact: Hope Wilson (email: hopewilson@email.arizona.edu)
5.	Water Wise, Sierra Vista Cochise County Extension
contact: MaryAnn Capehart (email: capehart@email.arizona.edu)
6.	Technical assistance and programming for local Farmers Markets and Community Gardens, Tucson 
contact: Natalia Santos (email: nataliasantos@email.arizona.edu)

Project managers are in the process of recruiting students for their projects which typically start around the end of May or beginning of June. The community partners include Farmers Market managers, Tucson summer programs that take advantage of Tucson Village Farm, our Neighbor\'s Farm and Food Bank in Safford, and businesses in Sierra Vista that have rainwater tanks for monitoring. I am happy to send more detailed descriptions of the projects to Lauren White should you need it.',
'created_at' => '2018-02-15 18:15:12.000000',
'question_id' => 815,
),
258 => 
array (
'answer_text' => 'Dear Green Fund Committee,
Project managers use the services of the Student Engagement Coordinator for the College of Ag and Life Sciences (CALS), Gabrielle Sykes-Casavant, who helps county Extension personnel with a list of UA students in their community, based on home address, that they can reach out to directly to recruit for the project.  I will gladly send the application we use as well as the spreadsheet describing the projects for summer of 2018 to Lauren White. Mark Apel',
'created_at' => '2018-02-15 18:21:05.000000',
'question_id' => 816,
),
259 => 
array (
'answer_text' => 'This project will fund one Masters student for one year: summer 2018, Fall 2018 and Spring 2019.
The undergraduate budget has been calculated for 20 hours/week for two semesters and 40 hours a week for the summer. How many undergraduates will depend on the number of hours they want to work. In theory, all this can go to one student. In practice, similar budget calculations on past grants have led to 3-5 undergraduates employed.',
'created_at' => '2018-02-16 10:52:15.000000',
'question_id' => 805,
),
260 => 
array (
'answer_text' => 'If I had to choose between the two projects I would choose the Beer Worms. A number of considerations lead to this decision. First, the Beetle Grub project is the Masters thesis of Meck Slagle. Funding for this project will allow her to devote all her time to the project, thereby increasing its scope. However, without funding the work will still be done. This is not the case of the Beer Worms. Without funding this project would not happen.

Second, the Beer Worms will bring in revenue streams much quicker than the Beetle Grubs, which will help fund other projects.

Third, the Beer Worms will be used in teaching the undergraduate edible insect courses. As mentioned in the proposal, it is my intention to make the UA the leading US university for edible insects. Having edible beer worms as part of the curriculum will increase the popularity and attraction of the edible insect courses.',
'created_at' => '2018-02-16 10:54:29.000000',
'question_id' => 805,
),
261 => 
array (
'answer_text' => 'The impact of partial funding will depend on the degree of partial funding. The highest priority is salary and tuition fees for the graduate student. I would protect this component as much as possible. Depending on the degree of partial funding I would first reduce some of the funds for packaging, materials and supplies. I would then consider eliminating the drying oven (about $7,000). This is a specialized oven for drying insects that reduces the drying time from 2 hours to ten minutes which is invaluable at large scale production, but less essential when we are still ramping up production.  The second largest component of the budget is undergraduates and I would have to reduce student hours if the budget is reduced significantly.

In actuality, I would likely reduce all components of the budget in parallel depending on the degree of the partial funding. The one exception is the graduate student.',
'created_at' => '2018-02-16 10:56:28.000000',
'question_id' => 805,
),
262 => 
array (
'answer_text' => 'It is not clear what is meant by breadth of impact so I am not sure this will answer the question. Both projects will generate edible insects. The mealworms will be quicker to set up and quicker to generate revenue. In addition, the mealworms will be used in teaching whereas the beetle grubs will not.',
'created_at' => '2018-02-16 15:58:52.000000',
'question_id' => 809,
),
263 => 
array (
'answer_text' => 'The highest priority is graduate student salary and tuition funding. The second priority is undergraduate funding. The lowest priority would be the funds for compost and wood chips because it is the smallest component of the budget and would be easiest to cover from other sources. With partial funding I would eliminate funding for compost and wood chips as well as undergraduate hours.',
'created_at' => '2018-02-16 15:59:53.000000',
'question_id' => 807,
),
264 => 
array (
'answer_text' => 'The beetle grub timeline is still unclear as we are still learning about the life cycle of the beetle. We started the project last August and have started harvesting grubs in February. On the surface it seems that the life cycle is six months however, there was no heating in the building October through December so their growth was slow. 
The literature says the lifecycle is about three months and we may be able to reduce that with higher temperatures and good quality compost. 
The expected timeline is to collect pupae from the current generation and work to set up a lab colony of the beetles. We have about 500 grubs that we are devoting to this purpose. In the summer we will collect adults from the wild, have them lay eggs and use the eggs in experiments testing the various food substrates. During the Fall and Spring semesters we will collect growth and survivorship data and begin product development-how to prepare the grubs for market.',
'created_at' => '2018-02-16 16:10:15.000000',
'question_id' => 806,
),
265 => 
array (
'answer_text' => 'The range of 15–50 students is the number of students that can be directly and intimately involved in the project from start to finish. This range depends on two things: 1) the grant cycle and thus project duration (see answers below), and 2) student response to advertisements and recruiting efforts.

First, we intend for this to be a two-year project with potential for additional students to participate at various points in the project, thus reaching a broader representation of the student body and expanding opportunities to undertake research and outreach on Tumamoc Hill. Second, despite a plan for aggressive advertisement using numerous platforms (see below), we cannot guarantee response levels we will receive. In our past experiences it has not been difficult to recruit interested and capable students for similar programs implemented by project staff. 

The upper bound 50 is based on the maximum number of students we could have in the field at any one point due to concerns of degradation of the resource and the total number of students we could accommodate in our lab space for instructional workshops and other programs. Importantly, the number of students reached by our project will be much greater than 50 through various outreach, symposia, and other mechanisms we will employ to disseminate results and provide students opportunities to present their work. This project will be open to both undergraduate and graduate students. Graduate students will be targeted for the primary leadership and paid positions but motivated undergrads will be capable of serving in these capacities. At a minimum we hope to have at least 40 students involved in the project from start to finish.

We will use multiple strategies to recruit students. First, we will post advertisements for paid positions, work-studies, independent-study opportunities, and volunteer positions linked to student’s individualized current coursework and graduate studies in various departmental list-serves and newsletters.  These will include Ecology and Evolutionary Biology, School of Natural Resources and the Environment, Soil/Water and Environmental Science, Interdisciplinary Programs, and other departments and centers.  Second, we will contact specific faculty members and colleagues on campus that are teaching relevant courses and ask them to make these opportunities known to students. Third, we will contact various extracurricular clubs through the Club Resource Center (e.g., Wildlife Society Student Chapter) and distribute materials at biannual Club Fairs on campus. Forth, we will list positions on UA web sites including the UA Jobs site and sites listing opportunities for work study with help from academic coordinators in various related departments, and post on high traffic electronic and other bulletin boards on campus. Finally, we will post opportunities and promotional materials on Tumamoc social media sites and on the bulletin board at the base of the Hill, which are seen by thousands of people and many students each week.',
'created_at' => '2018-02-16 22:00:55.000000',
'question_id' => 817,
),
266 => 
array (
'answer_text' => 'Dr. Aaron D. Flesch and Dr. Matt Goode, appointed faculty in the School of Natural Resources and the Environment, are resident experts in the subjects at the core of research linked to this project. They will be recipients of this funding, which provides one month of support in each of two fiscal years. Dr. Flesch and Dr. Goode are both in “soft money” positions and thus their salary is derived from grant funding not permanent lines in the University. Accordingly, the time they are requesting for this project will directly support their leadership of the proposed work. Dr. Ben Wilder, Director of Tumamoc Hill, and Cynthia Anson, Tumamoc Program Coordinator, will provide substantial in-kind effort with support provided by the College of Science. Likewise, Tumamoc Research Director Dr. Larry Venable, and other tenure track faculty, will also provide in-kind support by interacting with and supporting the student participants in all aspects of the project. Dr. Flesch and Dr. Goode will be responsible for developing all curricula, implementing instructional workshops on study design, training those involved in field sampling, analyses, and provide guidance and feedback on reporting and presenting, work with students in the field, supervise student workers, employees, and work-studies, and provide office hours and oversight of all aspects of the project. These activities are extensive and likely to exceed the one month of support requested. Dr. Flesch and Dr. Goode are widely recognized as leading experts in their fields and their participation will create a high-quality experiential learning opportunity for participating students, some of which who will also receive compensation for their efforts.',
'created_at' => '2018-02-16 22:03:04.000000',
'question_id' => 818,
),
267 => 
array (
'answer_text' => 'This project would still be possible with partial funding. Specifically, we revisited our equipment and materials budget reducing Year 1 costs by $2,300 without too much impact on the project. That impact would be reduced by supplementing efforts with personal equipment and by limiting the scope to not address small mammals (or borrowed traps could be sought from faculty on campus given sufficient student interest). The remaining material costs are essential to provide the tools needed for the work proposed. A possible re-budget of material costs is below (we can send an Excel File if its not readable as pasted below):

Equip	proposed	new	Diff
GPS receivers (5 @ $125 ea.)	 $            625 	 $            375 	
Binoculars (5 @ $200 ea.)	            1,000 	               400 	
Trap Materials (hardware cloth, stakes, etc.)	            1,000 	            1,000 	
Small Mammal Traps (50 @ $20 ea.) 	            1,000 	 	
Infrared-Triggered Cameras (4 @ $250 ea.)	            1,000 	            1,000 	
PIT-Tags for Marking (200 @ $2 ea.)	               400 	               400 	
Miscellaneous Field/Lab Supplies	            1,500 	            1,250 	
Materials For Outreach	               500 	               400 	
Materials for Symposium	               500 	               400 	
$              7,525 	 $              5,225 	 $    2,300 

Reducing salaries of the primary project staff would affect the amount of support and mentorship we could provide to students. Such a change is much less desirable and would result in a potentially large group of students being supported by 1 to no faculty during a much greater period of the project, and reduce the quality of the project for students.  If such a change was to occur we would need to increase the student budget and assure 1-2 highly motivated graduate or other students were available to fill the resulting gaps, which is hard to guarantee without prior recruitment.  Regardless, the hourly rate of our two principal project staff ($25/hr) is highly competitive especially for Ph.D\'s with 20+ years experience in the field that already anticipate volunteering many hours to this effort.',
'created_at' => '2018-02-16 22:05:49.000000',
'question_id' => 819,
),
268 => 
array (
'answer_text' => 'The answer to this question depends on the grant cycle. If the grant cycle is based on the fiscal year, July 1 to June 30, the nature of the project and the timing of the fiscal year, academic semester, and ecological sampling schedules, conspire to make it difficult to achieve the project goals in one year. Students need to have sufficient background and training before sampling begins, which occurs mainly during spring and summer only when the organisms are active and present to be sampled. The workshops and trainings in the Fall semester will prepare students for spring and summer research. Analyses, talks and other project products after data are collected in summer will be undertaken during the following Fall semester, with some arrangement made for students working in spring to report results at the end of the same spring semester where needed. Thus, if the grant cycle is from Jan 1 to December 31st, it is possible to undertake this project in one year, yet for only one cohort of students. If the grant follows the fiscal year calendar two years will be needed, though multiple cohorts of students will be able to participate.',
'created_at' => '2018-02-16 22:07:22.000000',
'question_id' => 820,
),
269 => 
array (
'answer_text' => 'The Green Fund is valuable because it will allow for proof of concept for our recirculating hydroponic bed filters.  Since there is a high likelihood of intellectual property being developed through this project, it makes sense that Tech Launch Arizona will be our next step. We believe the Green Fund project we have proposed can be leveraged to attain funds from Tech Launch Arizona as well as numerous other grants.   However, attaining many of these potential grants will be difficult to attain without proof of concept and University support, which we can achieve through the Green Fund initiative.',
'created_at' => '2018-02-23 03:01:50.000000',
'question_id' => 835,
),
270 => 
array (
'answer_text' => '2018:
April - Reassess contaminant profiles of secondary effluent and reclaimed water in Tucson 

May - Germinate plants and procure materials

June - Set up mini hydroponic bed trials

July-August - Run mini bed tests and build larger scale recirculating hydroponic treatment beds

August-December - Run tests on large scale treatment beds 

2019:
January - Analyze data 

February-March - Optimize design for commercial use in Tucson and surrounding area

March - Present project results to public',
'created_at' => '2018-02-23 08:22:23.000000',
'question_id' => 837,
),
271 => 
array (
'answer_text' => 'This project will benefit students through both employment and volunteer opportunities

It will benefit the local community as volunteers (students) give presentations about the project to local schools and groups.  What we learn from the project we hope to share with a wide group of people including agricultural operations, extension agents and likely a broad range of groups who stand to benefit from removing contaminants from the water they use.

The project will be coordinating with the University of Arizona Water & Energy Sustainability Technology (WEST) Center and the University of Arizona Controlled Environment Agriculture Center (CEAC) to gain maximum exposure within the Tucson community.  We also believe that publicizing this project and its results to the fullest extent will maximize its impact.',
'created_at' => '2018-02-23 08:34:18.000000',
'question_id' => 837,
),
272 => 
array (
'answer_text' => 'The ENR2 Building Manager is a key stakeholder and represents the users of the building, ensuring that this project does not disrupt or otherwise cause inconvenience for building users. Under the current scope of the project, Facilities Management (FM) and Planning, Design, & Construction (PDC) will collaborate in the managing of this project. It is currently anticipated that FM will provide project management and PDC design review and engineering support. Both organizations have extensive experience managing projects of this kind of scope and are more than capable of providing the support necessary for this project.',
'created_at' => '2018-02-23 15:22:10.000000',
'question_id' => 821,
),
273 => 
array (
'answer_text' => 'FM is a project partner alongside all the stakeholder groups involved in this project and is intent on full cooperation and collaboration throughout the project. Project management at all phases will be conducted with complete transparency to all stakeholder groups. The committee, consisting of representatives from FM, PDC, Business Affairs Sustainability, Students for Sustainability, Solar Cats, and the Community & School Gardens Program, will be directly involved throughout the life of the project to successfully achieve project goals, with each stakeholder working collaboratively to maintain open lines of communication as well as accountability for all.',
'created_at' => '2018-02-23 15:22:53.000000',
'question_id' => 822,
),
274 => 
array (
'answer_text' => 'We plan to advertise this project to students and the community in the following ways in order to ensure its visibility and use:
Advertising to media via UANews, Lo Que Pasa, UA@Work, the Daily Wildcat, and local Tucson new organizations to drum up hype leading up to the ribbon cutting, encouraging campus and community members to visit the installation after its completion; 
Recruitment of UA Marketing & Branding, upon approval, to document the process from planning to construction to completion and beyond in video form for wide distribution and marketing;
Permanent display of real-time information (daily and cumulative energy production, associated reduction in greenhouse gases, etc.) on each of the TVs in Slot Canyon Cafe, with associated information directing viewers to the roof; 
Engagement with faculty that teach courses relevant to the project (College of Architecture and Landscape Architecture, College of Ag & Life Sciences, College of Engineering, College of Science, College of Social and Behavioral Sciences), providing them with photos, video, as well as data and further resources for use in the classroom with encouragement to visit the physical site;
Advertising across UA sites including FM, PDC, Business Affairs, the Institute of the Environment, Students for Sustainability, ASUA, the Community & School Gardens Program, and more. 
Semesterly visits by Students for Sustainability (100+ students per semester) to demonstrate the power of students to move forward sustainability projects on campus.',
'created_at' => '2018-02-23 15:23:51.000000',
'question_id' => 825,
),
275 => 
array (
'answer_text' => 'The outright completion of this project can be maintained at a funding level of approximately $99,400.  
Priorities as follows: Solar Panels ($84,700); Agro Materials & Sensors ($9,700); Project Contingency ($5,000)
We believe that in order to maintain the integrity of the project as it stands that we cannot compromise funding for the Solar Panels or Agro Materials and Sensors along with an associated contingency fund for both. 
The overall Project Contingency line item ($12,000) was added at the advice of PDC and FM to ensure timely and efficient completion of the project in the event of unforeseen circumstances (i.e. water connection ends up costing $5,000 rather than $2,400). They advised 10%-15% (standard practice) and we applied a contingency of 11%. Again, any unused contingency funds will be returned to the Green Fund at the completion of the project. In the interest of cost cutting, maintaining a contingency of at least 5% (~$5,000) would be ideal to ensure timely completion. 
Additional line items within our initial budget proposal are flexible or removable and have no significant barrier to the outright completion of this project, though may affect less vital areas of the project. These items are as follows: Signage; UITS; Funds for Ribbon Cutting; Solar Panel Tariff Contingency; Project Contingency; Student Employee.
Signage ($1,500) is a critical item for the ability of visitors to interact with the array, however, the amount could be reduced and/or our team can work to identify another small funding source. 
Any required UITS services ($500) could be donated or may fall within the above noted contingency. 
Funds for Ribbon Cutting ($1,000) are intended to create fanfare for the array and recognize the work of students, those involved on our team as well as those on the Green Fund, in advancing sustainability on our campus. The amount requested could be reduced without compromising this goal. 
The Solar Panel Tariff Contingency line item ($2,500) is likely no longer of need:
If TFS were chosen as the solar installer for the project, they have stated on their website that the solar trade tariff is something that they planned for and as a result, would likely not affect projects planned through the end of 2018. If TFS is not chosen, most solar installers anticipated this tariff and are likewise positioned to avoid significant price increases. 
Funding for a Student Employee ($4,200) would create a student employment opportunity, which aligns with the Green Fund’s priorities, however, the removal of funds for such a position would not remove the ability for the Community and School Garden Program to engage with students in the same way and would not affect student engagement or leadership possibilities.',
'created_at' => '2018-02-23 15:24:39.000000',
'question_id' => 825,
),
276 => 
array (
'answer_text' => 'We did not obtain price estimates from any other companies beyond Technicians for Sustainability for this stage of the project due to the relatively straightforward nature of the solar part of this proposal. This meaning that the quote we received from TFS should be representative of those that we may receive from other companies. We chose TFS for the initial, non-binding proposal because they are a local company, based out of Tucson with an A+ rating from the Better Business Bureau, with strong community engagement and customer reviews, and is awarded on the City of Tucson Energy Services Performance Contract and is thus considered a pre-qualified vendor for the UA. TFS also agreed to engage with students throughout the process if chosen as the solar installer for the project, something that many other solar companies, especially those not local to Tucson, may not agree to.
If the Green Fund chooses to fund this project proposal, we will thoroughly vet TFS and similar companies through a Request for Proposals (RFP) and we will select the company that provides the best mix of high-efficiency solar panels for the lowest cost possible and is willing to involve students where appropriate.',
'created_at' => '2018-02-23 15:25:08.000000',
'question_id' => 825,
),
277 => 
array (
'answer_text' => 'We did not obtain price estimates from any other companies beyond Technicians for Sustainability for this stage of the project due to the relatively straightforward nature of the solar part of this proposal. This meaning that the quote we received from TFS should be representative of those that we may receive from other companies. We chose TFS for the initial, non-binding proposal because they are a local company, based out of Tucson with an A+ rating from the Better Business Bureau, with strong community engagement and customer reviews, and is awarded on the City of Tucson Energy Services Performance Contract and is thus considered a pre-qualified vendor for the UA. TFS also agreed to engage with students throughout the process if chosen as the solar installer for the project, something that many other solar companies, especially those not local to Tucson, may not agree to.
If the Green Fund chooses to fund this project proposal, we will thoroughly vet TFS and similar companies through a Request for Proposals (RFP) and we will select the company that provides the best mix of high-efficiency solar panels for the lowest cost possible and is willing to involve students where appropriate.',
'created_at' => '2018-02-23 15:26:10.000000',
'question_id' => 825,
),
278 => 
array (
'answer_text' => 'The outright completion of this project can be maintained at a funding level of approximately $99,400.  
Priorities as follows: Solar Panels ($84,700); Agro Materials & Sensors ($9,700); Project Contingency ($5,000)
We believe that in order to maintain the integrity of the project as it stands that we cannot compromise funding for the Solar Panels or Agro Materials and Sensors along with an associated contingency fund for both. 
The overall Project Contingency line item ($12,000) was added at the advice of PDC and FM to ensure timely and efficient completion of the project in the event of unforeseen circumstances (i.e. water connection ends up costing $5,000 rather than $2,400). They advised 10%-15% (standard practice) and we applied a contingency of 11%. Again, any unused contingency funds will be returned to the Green Fund at the completion of the project. In the interest of cost cutting, maintaining a contingency of at least 5% (~$5,000) would be ideal to ensure timely completion. 
Additional line items within our initial budget proposal are flexible or removable and have no significant barrier to the outright completion of this project, though may affect less vital areas of the project. These items are as follows: Signage; UITS; Funds for Ribbon Cutting; Solar Panel Tariff Contingency; Project Contingency; Student Employee.
Signage ($1,500) is a critical item for the ability of visitors to interact with the array, however, the amount could be reduced and/or our team can work to identify another small funding source. 
Any required UITS services ($500) could be donated or may fall within the above noted contingency. 
Funds for Ribbon Cutting ($1,000) are intended to create fanfare for the array and recognize the work of students, those involved on our team as well as those on the Green Fund, in advancing sustainability on our campus. The amount requested could be reduced without compromising this goal. 
The Solar Panel Tariff Contingency line item ($2,500) is likely no longer of need:
If TFS were chosen as the solar installer for the project, they have stated on their website that the solar trade tariff is something that they planned for and as a result, would likely not affect projects planned through the end of 2018. If TFS is not chosen, most solar installers anticipated this tariff and are likewise positioned to avoid significant price increases. 
Funding for a Student Employee ($4,200) would create a student employment opportunity, which aligns with the Green Fund’s priorities, however, the removal of funds for such a position would not remove the ability for the Community and School Garden Program to engage with students in the same way and would not affect student engagement or leadership possibilities.',
'created_at' => '2018-02-23 15:31:01.000000',
'question_id' => 824,
),
279 => 
array (
'answer_text' => 'We plan to advertise this project to students and the community in the following ways in order to ensure its visibility and use:
Advertising to media via UANews, Lo Que Pasa, UA@Work, the Daily Wildcat, and local Tucson new organizations to drum up hype leading up to the ribbon cutting, encouraging campus and community members to visit the installation after its completion; 
Recruitment of UA Marketing & Branding, upon approval, to document the process from planning to construction to completion and beyond in video form for wide distribution and marketing;
Permanent display of real-time information (daily and cumulative energy production, associated reduction in greenhouse gases, etc.) on each of the TVs in Slot Canyon Cafe, with associated information directing viewers to the roof; 
Engagement with faculty that teach courses relevant to the project (College of Architecture and Landscape Architecture, College of Ag & Life Sciences, College of Engineering, College of Science, College of Social and Behavioral Sciences), providing them with photos, video, as well as data and further resources for use in the classroom with encouragement to visit the physical site;
Advertising across UA sites including FM, PDC, Business Affairs, the Institute of the Environment, Students for Sustainability, ASUA, the Community & School Gardens Program, and more. 
Semesterly visits by Students for Sustainability (100+ students per semester) to demonstrate the power of students to move forward sustainability projects on campus.',
'created_at' => '2018-02-23 15:31:27.000000',
'question_id' => 821,
),
280 => 
array (
'answer_text' => 'FM is a project partner alongside all the stakeholder groups involved in this project, and is intent on full cooperation and collaboration throughout the project. Project management at all phases will be conducted with complete transparency to all stakeholder groups. The committee, consisting of representatives from FM, PDC, Business Affairs Sustainability, Students for Sustainability, Solar Cats, and the Community & School Gardens Program, will be directly involved throughout the life of the project to successfully achieve project goals, with each stakeholder working collaboratively to maintain open lines of communication as well as accountability for all.',
'created_at' => '2018-02-23 15:31:39.000000',
'question_id' => 821,
),
281 => 
array (
'answer_text' => 'The ENR2 Building Manager is a key stakeholder and represents the users of the building, ensuring that this project does not disrupt or otherwise cause inconvenience for building users. Under the current scope of the project, Facilities Management (FM) and Planning, Design, & Construction (PDC) will collaborate in the managing of this project. It is currently anticipated that FM will provide project management and PDC design review and engineering support. Both organizations have extensive experience managing projects of this kind of scope and are more than capable of providing the support necessary for this project.',
'created_at' => '2018-02-23 15:32:09.000000',
'question_id' => 821,
),
282 => 
array (
'answer_text' => 'The bike lights and helmets will be kept in our cashier booths in the garages and at the bike valet areas when we expand the program. We will add features to our Cat Wheels application to track which users check out these items. We currently track overdue bikes and charge late fees to the user’s bursar’s account and we would also charge fees for lost or damaged lights and helmets using the same system. The iPads will be returned into the office each night.',
'created_at' => '2018-02-23 18:04:15.000000',
'question_id' => 829,
),
283 => 
array (
'answer_text' => 'Hello there.  Thank you for your question!  I understand that the $15,000 is an unusually large travel request, uncommon in the terms of what is typically approved, so it makes sense that something unusual or exceptional would be required to justify this much money being re-allocated toward travel.  

Regarding the AASHE conference, we see the opportunity of attending AASHE as an entire team experience.  Since our team is likely to remain small and close-knit with only 4 or 5 people next year, we thought sending our whole team would be more valuable.  Each individual team member would be strengthened by the experience, and the whole team would be more fully unified and inspired in our cause as a result of the experience.  Also, if the whole team attended, no one would feel left out of the experience, and we would avoid certain pitfalls that could happen as a result of the competition and selection that would have to happen if we sent only a few.  If we were to not send the whole team, we would need to supplement the missed opportunity for the rest of the team, and we would have to explore ways to select one or two members that would be fair and viewed positively.

Nevertheless, I realize that for something like AASHE, it is more common or traditional for representatives of groups, rather than whole groups, to attend, and that sending 1 or 2 people might seem more acceptable in terms of funding.  Sending fewer people, and not our whole team, would definitely be an option and we are willing to direct a more permissible portion of the unused funding toward sending 1 or 2 people.  We are also definitely willing to rollover any remaining fiscal funds from this year toward supporting our events as well as student workers next year.  We could take 2 people to AASHE with $4,372, per our budget breakdown, and then consider either expanding our team’s size as well as how to direct funds toward the events we have created to take place next year. 

In terms of looking on AASHE as a priority, we understand that this is an important national sustainability conference that will help us connect our work to the work being done on other campuses.  The budget we send according to your request will illustrate how travel costs are calculated for 5 individuals—myself and 4 team members.  We believe making these connections together will inspire our efforts as a team, augment our events, and contribute to the professionalism, goals, and resilience that we employ in our educational sustainable endeavors on campus.  We are a unique team working within the scope of the sustainable campus community, and we would be excited to see what impact a whole team entity attending AASHE would have on the growth of sustainability on our campus.

I have formalized the itemized budget as requested, however, submitting it via this text box is not ideal for maintaining the integrity of the format.  How do you wish to receive it?  Please let me know.  Thanks!',
'created_at' => '2018-04-06 05:13:47.000000',
'question_id' => 839,
),
));
        
        
    }
}