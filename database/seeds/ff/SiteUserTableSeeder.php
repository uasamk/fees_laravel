<?php

use Illuminate\Database\Seeder;

class FfSiteUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        
        \DB::table('site_user')->insert(array (
            0 => 
            array (
                'site_id' => 3,
                'user_id' => 51,
            ),
            1 => 
            array (
                'site_id' => 3,
                'user_id' => 49,
            ),
            2 => 
            array (
                'site_id' => 3,
                'user_id' => 57,
            ),
            3 => 
            array (
                'site_id' => 3,
                'user_id' => 48,
            ),
            4 => 
            array (
                'site_id' => 3,
                'user_id' => 55,
            ),
            5 => 
            array (
                'site_id' => 3,
                'user_id' => 43,
            ),
            6 => 
            array (
                'site_id' => 3,
                'user_id' => 44,
            ),
            7 => 
            array (
                'site_id' => 3,
                'user_id' => 50,
            ),
            8 => 
            array (
                'site_id' => 3,
                'user_id' => 56,
            ),
            9 => 
            array (
                'site_id' => 3,
                'user_id' => 47,
            ),
            10 => 
            array (
                'site_id' => 3,
                'user_id' => 41,
            ),
            11 => 
            array (
                'site_id' => 3,
                'user_id' => 45,
            ),
            12 => 
            array (
                'site_id' => 3,
                'user_id' => 46,
            ),
            13 => 
            array (
                'site_id' => 3,
                'user_id' => 52,
            ),
            14 => 
            array (
                'site_id' => 3,
                'user_id' => 54,
            ),
            15 => 
            array (
                'site_id' => 3,
                'user_id' => 42,
            ),
            16 => 
            array (
                'site_id' => 3,
                'user_id' => 53,
            ),
        ));
        
        
    }
}