<?php

use Illuminate\Database\Seeder;

class FfQuestionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        
        \DB::table('questions')->insert(array (
            0 => 
            array (
                'id_old' => 49991,
                'question_text' => 'What changes are you planning to make to improve the student experience at Freshman Convocation and what are you doing to ensure that as many freshmen as possible attend?',
                'netid' => 'altomare',
                'deadline' => '2011-03-14 03:13:10.000000',
                'created_at' => '2011-03-07 03:13:10.000000',
                'questionable_id' => 767,
                'questionable_type' => 'App\\Project',
            ),
            1 => 
            array (
                'id_old' => 49992,
                'question_text' => 'Is it possible to increase the amount of whistles to be distributed?  If yes, how would you distribute them?  What will you do to ensure that the door hangers do not get thrown away?',
                'netid' => 'altomare',
                'deadline' => '2011-03-14 03:13:43.000000',
                'created_at' => '2011-03-07 03:13:43.000000',
                'questionable_id' => 771,
                'questionable_type' => 'App\\Project',
            ),
            2 => 
            array (
                'id_old' => 49993,
                'question_text' => 'Please clarify "Other Camp Expenses" in the budget.',
                'netid' => 'altomare',
                'deadline' => '2012-04-20 18:40:20.000000',
                'created_at' => '2012-04-19 18:40:20.000000',
                'questionable_id' => 773,
                'questionable_type' => 'App\\Project',
            ),
            3 => 
            array (
                'id_old' => 49994,
                'question_text' => 'How will Bear Down Camp be marketed this year, especially as compared to last year?',
                'netid' => 'altomare',
                'deadline' => '2012-04-20 18:41:42.000000',
                'created_at' => '2012-04-19 18:41:42.000000',
                'questionable_id' => 773,
                'questionable_type' => 'App\\Project',
            ),
            4 => 
            array (
                'id_old' => 49995,
                'question_text' => 'Will the BDC camp location be changing? If so, where will it be located?',
                'netid' => 'jevans91',
                'deadline' => '2013-03-23 21:32:35.000000',
                'created_at' => '2013-03-22 21:32:35.000000',
                'questionable_id' => 794,
                'questionable_type' => 'App\\Project',
            ),
            5 => 
            array (
                'id_old' => 49996,
                'question_text' => 'What all is included in the marketing section of your budget?',
                'netid' => 'jevans91',
                'deadline' => '2013-03-29 21:51:03.000000',
                'created_at' => '2013-03-28 21:51:03.000000',
                'questionable_id' => 785,
                'questionable_type' => 'App\\Project',
            ),
            6 => 
            array (
                'id_old' => 49997,
            'question_text' => 'Can we please get a breakdown of the Total ERE costs (i.e. Non-capitalized furnishings, operating supplies, and MISC services). The board would like to see more details pertaining to the costs.',
                'netid' => 'jordanstrang',
                'deadline' => '2018-03-31 21:14:43.000000',
                'created_at' => '2018-03-30 21:14:43.000000',
                'questionable_id' => 818,
                'questionable_type' => 'App\\Project',
            ),
            7 => 
            array (
                'id_old' => 49998,
                'question_text' => 'Has there been a increase in traffic in this area where the flow gets impeded? How will this effect every freshman on the campus?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-03-31 21:15:56.000000',
                'created_at' => '2018-03-30 21:15:56.000000',
                'questionable_id' => 818,
                'questionable_type' => 'App\\Project',
            ),
            8 => 
            array (
                'id_old' => 49999,
                'question_text' => 'How many people on staff do you have already? If so, what does their job description entail?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-03-31 21:25:41.000000',
                'created_at' => '2018-03-30 21:25:41.000000',
                'questionable_id' => 835,
                'questionable_type' => 'App\\Project',
            ),
            9 => 
            array (
                'id_old' => 499910,
                'question_text' => 'Have you utilized other resources such as volunteers, fraternal organizations, or professional clubs to host these type of events to save on costs?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-03-31 21:27:38.000000',
                'created_at' => '2018-03-30 21:27:38.000000',
                'questionable_id' => 835,
                'questionable_type' => 'App\\Project',
            ),
            10 => 
            array (
                'id_old' => 499911,
                'question_text' => 'Will the four students have enough of an impact to reach your 1,000 students reach goal?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-03-31 21:28:15.000000',
                'created_at' => '2018-03-30 21:28:15.000000',
                'questionable_id' => 835,
                'questionable_type' => 'App\\Project',
            ),
            11 => 
            array (
                'id_old' => 499912,
                'question_text' => 'How many people attend the overall kickoff event in general, and how many students attend this event in general? What has been the attendance of this event over the past few years?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-03-31 21:38:38.000000',
                'created_at' => '2018-03-30 21:38:38.000000',
                'questionable_id' => 837,
                'questionable_type' => 'App\\Project',
            ),
            12 => 
            array (
                'id_old' => 499913,
                'question_text' => 'What is the overall marketing strategy to reach your target freshman?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-03-31 21:39:22.000000',
                'created_at' => '2018-03-30 21:39:22.000000',
                'questionable_id' => 837,
                'questionable_type' => 'App\\Project',
            ),
            13 => 
            array (
                'id_old' => 499914,
                'question_text' => 'Have you looked into allocating fundings from other resources?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-03-31 21:47:02.000000',
                'created_at' => '2018-03-30 21:47:02.000000',
                'questionable_id' => 841,
                'questionable_type' => 'App\\Project',
            ),
            14 => 
            array (
                'id_old' => 499915,
                'question_text' => 'Is there an agenda for the entire day to see the entire breakdown for the day?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-03-31 21:47:35.000000',
                'created_at' => '2018-03-30 21:47:35.000000',
                'questionable_id' => 841,
                'questionable_type' => 'App\\Project',
            ),
            15 => 
            array (
                'id_old' => 499916,
                'question_text' => 'As this being the first event, what marketing tactics will attract the first-gen students?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-03-31 21:50:55.000000',
                'created_at' => '2018-03-30 21:50:55.000000',
                'questionable_id' => 841,
                'questionable_type' => 'App\\Project',
            ),
            16 => 
            array (
                'id_old' => 499917,
                'question_text' => 'This Question was resubmitted by Teresa Whetzel to allow the project contact an opportunity to provide additional detail.  

Can we please get a breakdown of the Total ERE costs (i.e. Non-capitalized furnishings, operating supplies, and MISC services). The board would like to see more details pertaining to the costs.',
                'netid' => 'twhetzel',
                'deadline' => '2018-04-07 16:48:39.000000',
                'created_at' => '2018-04-06 16:48:39.000000',
                'questionable_id' => 818,
                'questionable_type' => 'App\\Project',
            ),
            17 => 
            array (
                'id_old' => 499918,
                'question_text' => 'This questions was resubmitted by Teresa Whetzel to allow the project contact ans opportunity to provide additional detail.  

Q: Has there been a increase in traffic in this area where the flow gets impeded? How will this effect every freshman on the campus?',
                'netid' => 'twhetzel',
                'deadline' => '2018-04-07 16:50:51.000000',
                'created_at' => '2018-04-06 16:50:51.000000',
                'questionable_id' => 818,
                'questionable_type' => 'App\\Project',
            ),
            18 => 
            array (
                'id_old' => 499919,
                'question_text' => 'Question resubmitted by Teresa Whetzel
Question #2
Submitted: Fri, March 30, 2018, 2:27:38 PM

Q: Have you utilized other resources such as volunteers, fraternal organizations, or professional clubs to host these type of events to save on costs?',
                'netid' => 'twhetzel',
                'deadline' => '2018-04-12 17:05:22.000000',
                'created_at' => '2018-04-06 17:05:22.000000',
                'questionable_id' => 835,
                'questionable_type' => 'App\\Project',
            ),
            19 => 
            array (
                'id_old' => 499920,
                'question_text' => 'Question Resubmitted by Teresa Whetzel

Q: Will the four students have enough of an impact to reach your 1,000 students reach goal?',
                'netid' => 'twhetzel',
                'deadline' => '2018-04-12 17:06:05.000000',
                'created_at' => '2018-04-06 17:06:05.000000',
                'questionable_id' => 835,
                'questionable_type' => 'App\\Project',
            ),
            20 => 
            array (
                'id_old' => 499921,
                'question_text' => 'Question resubmitted by Teresa Whetzel

Q: As this being the first event, what marketing tactics will attract the first-gen students?',
                'netid' => 'twhetzel',
                'deadline' => '2018-04-12 17:11:55.000000',
                'created_at' => '2018-04-06 17:11:55.000000',
                'questionable_id' => 841,
                'questionable_type' => 'App\\Project',
            ),
            21 => 
            array (
                'id_old' => 499922,
                'question_text' => 'Is this event specifically tailored towards freshman only, or will other grade classes be able to participate?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-04-12 21:38:21.000000',
                'created_at' => '2018-04-06 21:38:21.000000',
                'questionable_id' => 834,
                'questionable_type' => 'App\\Project',
            ),
            22 => 
            array (
                'id_old' => 499923,
                'question_text' => 'Can we get more clarification from the upper class-men mentoring system, will they be able to participate in the event only as mentors to first-year students or have a team of their own?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-04-12 21:41:11.000000',
                'created_at' => '2018-04-06 21:41:11.000000',
                'questionable_id' => 834,
                'questionable_type' => 'App\\Project',
            ),
            23 => 
            array (
                'id_old' => 499924,
                'question_text' => 'What does line 57 on the budget mean? Does it mean that New Services Fee pick up the rest of the total budget if First Year Student Fee does not provide full funding?',
                'netid' => 'jordanstrang',
                'deadline' => '2018-04-12 21:51:08.000000',
                'created_at' => '2018-04-06 21:51:08.000000',
                'questionable_id' => 838,
                'questionable_type' => 'App\\Project',
            ),
        ));
        
        
    }
}