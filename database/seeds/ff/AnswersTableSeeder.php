<?php

use Illuminate\Database\Seeder;

class FfAnswersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        
        \DB::table('answers')->insert(array (
            0 => 
            array (
                'answer_text' => 'In 2011, Freshmen Convocation will be a celebration designed to teach new students what it means to be a Wildcat.  Convocation will focus on UA traditions, and show the incoming class why they should be proud of wearing Red and Blue.  The UA cheerleaders, Wilbur and Wilma will teach freshmen the cheers they need to know to be ready for ZonaZoo, including learning all of the words to Bear Down!  The program will also feature video clips from members of the incoming class answering the question ‘why are you excited to be a Wildcat’ which will be recorded during the summer orientation season.

Freshmen Convocation is more than what happens in McKale, it’s also about connecting students to each other at the after party.  Student will get a chance to take pictures with Wilbur and Wilma, have UA athletes and coaches sign schedule posters, and have another chance to make new friends before classes begin the next day.  The party is an extension of the celebration that takes place in McKale, a chance to define the class of 2015.  

The event cannot be a celebration of the class of 2015 without members of the class in attendance.  We are enhancing marketing at orientation, including presenting the common view movie, working with residence life to encourage students to attend with their roommates and hall friends and creating a foursquare badge for New Student Convocation.  Additionally, we are planning a welcome event targeted to off campus freshmen immediately preceding Convocation which will have the same celebratory feel.  It is important to connect students who are living off campus to the university and to make sure they feel Wildcat pride in the same ways as students who live on campus.  Part of this event will be building a connection between the students and their outreach facilitator who serves in a ‘digital RA’ role, providing off campus students with someone they can turn to with questions about UA.',
                'created_at' => '2011-03-09 23:43:12.000000',
                'question_id' => 857,
            ),
            1 => 
            array (
            'answer_text' => '1. Is it possible to increase the amount of whistles to be distributed? We can definitely order more whistles. It will cost $3,550 for 3,000 and  $7,100 for 6,000. Of course the quantities can change. If yes, how would you distribute them? SafeCats has developed a strong presence on campus, as well as cooperative relationships with campus organizations that fall under the campus safety umbrella (i.e. OASIS, CAPS, ASUA Safe Ride, Women\'s Resource Center, STEP UP! Be a Leader). It would depend on the final total of whistles the board would be interested in. If the full 6,000, we would distribute them to students in residence life, where the majority of students are freshmen and would be sure EVERY freshman got one. Regardless of the quantity, these will be for first year students only.

2. What will you do to ensure that the door hangers do not get thrown away? Please see our "Be A Friend. Do Something"  website (link on original proposal) and you will see that it\'s not your everyday slapstick campaign. The campaign uses creative but extremely provocative images and text to broach some of the toughest, yet common situations students encounter during their college years. This includes things like getting drunk and making a fool of yourself and, even worse, going home with someone you shouldn\'t, also known as the drunken hookup. It also touches on issues like depression and violent behavior. In the words of Prof. Stephen Rains of the UA Department of Communication, who specializes in social influence and health communication, “These definitely have the potential to get our students talking.” The door tags will be approached in the same manner and branded with the same design. We think this is going to something students will want to hold onto, even if they don\'t leave it on their door. The goal is to get first year students great information as first responders to many situations in a unique and innovative way (catchy images, phrases,etc) to reduce campus incidents or encourage early reporting so we can help our first year students be successful even through challenging times. Can we “ensure” they will not get thrown away? No, but we think it will peak the interest of students enough to read and keep for reference. We will send PDF copies of some of the posters to Dr. Humphrey to share because they contain information not found on the website. 

Thank you for the opportunity to respond. If you have other questions, please let me know.',
                'created_at' => '2011-03-12 14:07:51.000000',
                'question_id' => 858,
            ),
            2 => 
            array (
                'answer_text' => 'The other camp expenses are held for last minute, unexpected expenses that come up after the fee is submitted and disbursed. They would also work toward covering any potential exploratory expenses for the new director to visit Fish Camp at Texas A&M, our "father" camp, to better understand their inter-workings and help BDC grow and expand toward a brighter future.',
                'created_at' => '2012-04-20 03:32:39.000000',
                'question_id' => 859,
            ),
            3 => 
            array (
            'answer_text' => 'Last year, no marketing was accomplished for Bear Down Camp outside of attending all New Student Orientation programs held on campus. This year, flyers are being sent out to all incoming freshmen who will be living in the Residence Halls on campus. These flyers will reach over 7,500 students over the course of this semester and the beginning of summer. In addition, the Bear Down Camp Executive Director attended the first out-of-state orientation program in Chicago to market Bear Down Camp and other representatives will do the same at the New York and Seattle orientations as well. BDC will also be represented at every on campus orientation throughout the summer and is now mentioned on campus tours with flyers available in Old Main for pick up. This time last year, we had two campers (of our 100 projected) registered for camp. Now, after registration being open for only two weeks with only one orientation and one set of flyers sent out, we have 47 campers fully registered and set to attend. We have also broadened our social media horizons and can now be found through multiple Facebook facets, Twitter, YouTube, and more. We also have two promotional videos up that can be found through YouTube and our website and are played throughout New Student Orientation programming.',
                'created_at' => '2012-04-20 03:43:49.000000',
                'question_id' => 860,
            ),
            4 => 
            array (
                'answer_text' => 'Yes, Bear Down Camp will be using a new campsite for BDC 2013. We plan on attending Camp Yavapines in Prescott, AZ. We chose this new location due to it offering both a larger area with more housing & meeting space and lower pricing. 
We are continuing to travel to Prescott, AZ due to the fact that campsites located closer to the university are unable to accommodate for our size of a program (320 people).',
                'created_at' => '2013-03-23 00:16:44.000000',
                'question_id' => 861,
            ),
            5 => 
            array (
                'answer_text' => 'The Finding Community Welcome marketing budget will consist of multiple avenues to not only recruit students to the event, but to also to sustain the message of the event. To recruit students to the event, we will be utilizing the following mediums:
•11 x 17 Posters
•Bookmark Handbills
•3D Memos
•Advertisements in the Daily Wildcat
One of our main goals of this event is for students to learn about and find their own community within this large university. For this, our committee has planned to provide take-away promotional materials with the communities’ logos or information printed on them so they have a continual reminder of the inclusivity of the University of Arizona campus.',
                'created_at' => '2013-03-29 19:48:15.000000',
                'question_id' => 862,
            ),
            6 => 
            array (
            'answer_text' => 'We attempt to utilize volunteers and program participants whenever possible to mitigate costs, for example, with tabling events where extensive training is not required. However, the nature of this position will require significant training on career education resources, career-based technologies, and paraprofessional counseling skills. Additionally, volunteer teams do not provide the stability required to reliably serve our campus partners as many students cannot make such a significant time commitment without being financially compensated. Finally, we employ student staff because we are working with student data that is protected by the federal Family Educational Rights and Privacy Act (FERPA). This information can only be accessed by employees of the University of Arizona.

We have partnered extensively with academic and student affairs departments in the past, including a pilot program with Fraternity & Sorority Programs. While we will continue to do so, we recognize the need for services that target first-year students exclusively as they have different needs than older students including the need to find Work Study employment, to explore or confirm their majors, and to create four-year plans that allow them to connect with the broader campus community in a way that serves their academic, social, and career development interests. To be clear, we recognize the value of professional student organizations and what they provide for their members, however, their purposes are significantly different than the services our office provides in regards to career education and planning.',
                'created_at' => '2018-03-30 22:36:20.000000',
                'question_id' => 865,
            ),
            7 => 
            array (
                'answer_text' => 'We are confident that with your support to fund these four positions, we will be able to reach an additional 1,000 students with in-person services such as 1:1 appointments, small group coaching, and first-year walk-in hours that we could otherwise not provide. This Peer Education team would work closely with our professional staff who would be responsible for helping them make connections to academic and student affairs units who are requesting our services. This team will provide direct service to students, rather than administrative support unlike other student roles in our office. We look forward to partnering with the First Year Student Fee Board to expand our impact and more strategically serve first year students.',
                'created_at' => '2018-03-30 22:38:14.000000',
                'question_id' => 865,
            ),
            8 => 
            array (
                'answer_text' => 'We currently have a team of six Peer Educators, including one student lead. While similar in title, these Peer Educators have significantly different responsibilities. They are responsible for staffing our front desk, providing administrative support for the office, and providing resume reviews in our daily Resume Lab. On occasion and based upon availability, these Peer Educators also present to student clubs and organizations, however, we have found that their potential for the kinds of outreach detailed in our First Year Student Fee proposal are extremely limited once these other priorities are taken into account. If funded our First Year Peer Educators would be dedicated exclusively to outreach within our academic colleges, student organizations, and at other SECD related events. Their time would not be devoted to supporting the administrative needs of our office, but would instead be fully committed to outreach efforts.',
                'created_at' => '2018-03-30 22:42:22.000000',
                'question_id' => 865,
            ),
            9 => 
            array (
                'answer_text' => 'There has not been a increase in traffic flow. The walk areas will not be impeded. 
All students, beyond all freshman, that use the Highland Market or pass through the area will be impacted.',
                'created_at' => '2018-03-31 01:32:04.000000',
                'question_id' => 864,
            ),
            10 => 
            array (
                'answer_text' => 'There has not been a increase in traffic flow. The walk areas will not be impeded. 
All students, beyond all freshman, that use the Highland Market or pass through the area will be impacted.',
                'created_at' => '2018-03-31 01:32:19.000000',
                'question_id' => 864,
            ),
            11 => 
            array (
                'answer_text' => 'I can get this information to you, but not prior to the expiration of this link, Sat. March 31. I am out of the office without access to a computer until April 5th and will have access to all my notes then. I hope this will not impede the review of this application.',
                'created_at' => '2018-03-31 01:45:52.000000',
                'question_id' => 863,
            ),
            12 => 
            array (
            'answer_text' => 'Our marketing strategy involves using the SASG- and Admissions-generated data on incoming first-year and transfer students, to reach out using the list-servs, work through Housing and Res Life by distributing flyers to all RAs to be posted in every wing on campus, ASUA and theTransfer Center promote the event, the theme communities on campus, and collaboration with Arizona Student Media with print, on-line, and social media ads. People have come to expect Finding Community Welcome and we use all our available cultural clubs and centers to spread the word through the summer and particularly in August. We also use social media accounts (Facebook, Snapchat, Twitter, and Instagram) to promote the event. Finally, we table throughout the summer and provide information to students through campus Orientations and feature the event in Cultural and Resource Center Booklets that will be given to each incoming transfer and first-year student as part of their orientation packets starting in April 2018 and through August 2018.',
                'created_at' => '2018-03-31 15:41:09.000000',
                'question_id' => 869,
            ),
            13 => 
            array (
                'answer_text' => 'Our attendance in Fall 2015 was 237 students, Fall 2016 was 291 students, and Fall 2017 was 405. In 2015 there were 47 clubs, 2016 we had 49 clubs and resources tabling and in 2017 we increased this number to 61.',
                'created_at' => '2018-03-31 16:01:57.000000',
                'question_id' => 869,
            ),
            14 => 
            array (
                'answer_text' => 'Funding within Academic Success & Achievement is dedicated to specific programs and initiatives. Most of our 2018-2019 funding is already committed to employ students and provide direct student services/support to our UA students, so we are not easily able to re-allocate funding for other pursposes and new initiatives given our funding structure. 

Understanding the importance of your question, we have thought of how to continue and sustain the First-gen Family Welcome past this year should we receive funding from First Year Student Fee. We plan to use the 2018-2019 academic year to seek other sources of funding for first-generation college student initiatives. Part of this process is being able to show some success with programs that we have hosted. We are hoping funding for the First-gen Family Welcome will allow us to show success to our campus partners and possible donors so they might be inclined to partner and fund the event in the future. College partners would make an excellent source of additional funding, but we must first show success in our program to gain their buy-in. 

We do view the First Year Student Fee as "seed" money to help us start this initiative/program, and to provide us a platform to be able to go to administration for funding after next year, gain partners, and to seek grants and donors in the future.',
                'created_at' => '2018-03-31 21:30:36.000000',
                'question_id' => 870,
            ),
            15 => 
            array (
                'answer_text' => 'As this is the first time we would be hosting the event, we do not have a formal agenda created yet. If we were to receive funding from the First Year Student Fee board this would still provide us approximately five months prior to the event, so we can plan with great detail throughout the month of June. 

In planning meetings with Parent and Family Programs, we have discussed a half day event that would start four hours prior to the football game. A general and drafted agenda is provided below:

FORMAL PROGRAM (2 hours)
Welcome from first-gen faculty and current UA students
Academic Success & Achievement - supporting your student through college (also highlights resources on campus)
Parent and Family Programs Presentation - how to get involved 
Parent/family/support panel 
Question & Answer Session

TAILGATE MIXER with food (90 minutes to 2 hours)
Informal and allow students and guests to take a break and mingle
Have faculty, staff, and current UA students available to speak with students and guests

FOOTBALL GAME
Have already talked to athletics about having the families sit together and be recognized at the game
(we do not anticipate all families will attend this portion, and have requested funding for less families than we anticipate attending the event)',
                'created_at' => '2018-03-31 21:45:46.000000',
                'question_id' => 871,
            ),
            16 => 
            array (
                'answer_text' => 'Personal outreach and connections are most important for working with first-generation families. This involves personal touch and invitations at orientation and during move-in. Additionally, calling families to personally invite them and explain the event is critical. It is also important to have Spanish speakers participating, and to advertise the Spanish session. Emails will always be used as a first-line, but mail invitations and phone calls along with those opportunities to connect in-person will be the most critical points of communication. First-gen students want their families involved, but they do not always know how to connect them, so the UA making an effort to outreach through multiple methods will be new to their experience.',
                'created_at' => '2018-03-31 21:51:39.000000',
                'question_id' => 871,
            ),
            17 => 
            array (
            'answer_text' => 'We attempt to utilize volunteers and program participants whenever possible to mitigate costs, for example, with tabling events where extensive training is not required. However, the nature of this position requires significant training on career education resources, career-based technologies, and paraprofessional counseling skills. Additionally, volunteer teams do not provide the stability required to reliably serve our campus partners as many students cannot make such a significant time commitment without being financially compensated. Finally, we employ student staff because we are working with student data that is protected by the Family Educational Rights and Privacy Act (FERPA). This information can only be accessed by employees of the University of Arizona.

We have partnered extensively with academic and student affairs departments in the past, including a pilot program with Fraternity & Sorority Programs. While we have been happy with these relationships, we recognize the need for services that target first-year students exclusively as they have different needs than older students including the need to find Work Study employment, to confirm their majors, and to create four-year plans that allow them to connect with the broader campus community in a way that serves their academic, social, and career development interests. To be clear, we recognize the value of professional student organizations and what they provide for their members, however, their purposes are significantly different than the services our office provides in regards to career education and planning.',
                'created_at' => '2018-04-06 17:15:07.000000',
                'question_id' => 875,
            ),
            18 => 
            array (
                'answer_text' => 'We are confident that with your support to fund these four positions, we will be able to reach an additional 1,000 students with in-person services such as 1:1 appointments, small group coaching, and first-year walk-in hours that we could otherwise not provide. This Peer Education team would work closely with our professional staff who would be responsible for helping them make connections to academic and student affairs units and manage related scheduling logistics, thus reserving their time for direct student interaction. This team will provide direct service to students, rather than administrative support unlike other student roles in our office. We look forward to partnering with the First Year Student Fee Board to expand our impact and more strategically serve first year students.',
                'created_at' => '2018-04-06 17:17:01.000000',
                'question_id' => 875,
            ),
            19 => 
            array (
                'answer_text' => 'We are confident that with your support to fund these four positions, we will be able to reach an additional 1,000 students with in-person services such as 1:1 appointments, small group coaching, and first-year walk-in hours that we could otherwise not provide. This Peer Education team would work closely with our professional staff who would be responsible for helping them make connections to academic and student affairs units who are requesting our services and managing scheduling logistics, thus reserving Peer Education time for direct student interaction. This team will provide direct service to students, rather than administrative support unlike other student roles in our office. We look forward to partnering with the First Year Student Fee Board to expand our impact and more strategically serve first year students.',
                'created_at' => '2018-04-06 17:19:11.000000',
                'question_id' => 876,
            ),
            20 => 
            array (
                'answer_text' => 'In marketing the event we will have multiple approaches:
1. We have been very successful in marketing to first-gen students through social media. We have a team of first-gen students that creates content. The social media team approaches content from their experience, and they would continue to play an instrumental role in reaching out to their peers. 
2. We plan to send a mass email to students and families, as well as regular USPS mail inviting families. These will be in English and Spanish. 
3. Personal phone calls from our staff in First Cats. The UA can be an overwhelming and unfamiliar space to first-gen families, so we plan to call and personally invite them so we can answer questions, explain the event, and welcome them. We will have English and Spanish speakers. Spanish speaking parents often receive information in Spanish, but when they arrive the experience is not in Spanish. Calling will allow us to show them that we have Spanish speakers and are fully prepared to offer them an experience in Spanish.
4. The timeline of First Year Student Fee funding works well to allow us to begin advertising this event at the New Student Orientation sessions. First Cats is already represented during these days, and we are easily able to give students and families a save-the-date card at NSO.',
                'created_at' => '2018-04-06 17:28:42.000000',
                'question_id' => 877,
            ),
            21 => 
            array (
                'answer_text' => 'Hello and thank you for your message. The Design Thinking Challenges program is open to all years and all majors. This is intentional, as we want to foster multi-disciplinary and multi-year student teams to encourage innovative problem-solving and peer mentorship. That said, our messaging and marketing is specific and tailored to the diverse student populations. For Freshman, we intend to promote this program as an opportunity to expand and deepen their UA network. In addition to gaining valuable collaboration and public speaking skills. I hope this helps to clarify! I look forward to hearing from you all soon. Regards, Annie',
                'created_at' => '2018-04-06 21:44:12.000000',
                'question_id' => 878,
            ),
            22 => 
            array (
                'answer_text' => 'Hello, thank you for this great question! All students of all year levels participate in the challenge as equal partners. We encourage students to build their teams comprised of diverse years and majors in order to leverage unique insights, experiences, and skills. In the two pilots we have offered, the peer mentoring component occurred naturally based on the teams\' composition. However, if we receive funding through the FF, we would support this work more overtly by providing peer mentor resources and ensuring each team has diverse membership. Moreover, we are exploring graduate student mentorship as well through a partnership with the Graduate College. Based on the scope and nature of the challenge, we would identify graduate students in related fields to work with each team in a formal mentorship capacity. I hope this helps to answer your question! I look forward to hearing from you again soon. Regards, Annie',
                'created_at' => '2018-04-06 22:07:53.000000',
                'question_id' => 879,
            ),
            23 => 
            array (
                'answer_text' => 'If we don\'t get funding from First Year Student Fee, we need to explore other options for funding.  We would have to submit a proposal to New Student Services to request funding and funding is never guaranteed.',
                'created_at' => '2018-04-07 00:40:17.000000',
                'question_id' => 880,
            ),
        ));
        
        
    }
}