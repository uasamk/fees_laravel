<?php

use Illuminate\Database\Seeder;

class FfProgressReportsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        
        \DB::table('progress_reports')->insert(array (
            0 => 
            array (
                'netid' => 'crs',
                'scope' => 'Equiss is a 3-day social justice retreat for undergraduate students who want to understand systems of oppression, privilege, identity development, and take action to create a more fair and just world. Equiss is designed to educate students on how to positively and effectively address issues of injustice, inequity, and inequality on campus and in the broader context of American society. 

The National Collegiate Leadership Conference is a three-day conference for hundreds of college students across the country. NCLC offers over 80 workshops, service projects, and networking opportunities along with dinner with a nationally recognized keynote speaker. 

ATLAS is a leadership certificate program that allows students to select a leadership program most aligned with their needs. Through workshops and hands-on activities, students can earn a certificate in one or more of eight leadership topics.',
                'outcomes' => 'The goal was to provide the opportunity for 10 first year students to be able to attend Equiss without having to pay the $175 fee in an effort to encourage first year students to participate in a social justice experience. Most students who attend Equiss are paid for by a department in which they are involved. This is less likely for first year students. Two first year students who otherwise may not have been able to afford to attend by paying their own way were given a fee waiver to participate in Equiss. 

NCLC opened thirty spots for first-year students to attend the conference without paying the $60 registration fee. Three students attended the conference using FF funds. 

No students utilized the fee waivers for ATLAS, 100 of which were available.',
                'response' => '35 students attended Equiss. They participated in a variety of sessions around issues of sex/gender, race, class, religion, sexual orientation, and disability. Students who participate in Equiss leave the retreat with an increase in knowledge of social justice issues, value of being socially just, and the ability to confront injustice and advocate for social justice in addition to developing their leadership competencies. 100% of participants indicated developing the following leadership competencies as a result of Equiss: Understanding others’ circumstances, motivation to consider perspectives other than own, understanding how to promote inclusion, motivation to act in an ethical manner, motivation to respond appropriately to others’ circumstances, motivation to make connections of individual parts through a larger system, understanding how to articulate a point of view effectively, and likelihood to reflect on experiences to apply learning in the future. 

NCLC hosted 652 participants from over 60 institutions of higher education in over 20 states. Of these participants, over 300 earned a leadership certificate for completing a certain number of workshops and service projects and over 380 participated in the All Conference Service Project with the Lions Club.',
                'created_at' => '2012-07-26 15:30:31.000000',
                'project_id' => 774,
            ),
            1 => 
            array (
                'netid' => 'twhetzel',
            'scope' => 'Bear Down Camp is a three-day camp in which incoming freshmen travel up to Prescott, Arizona to learn about University history and traditions, how to get involved on campus, and how to succeed during their first year at the University of Arizona. BDC focuses on developing a sense of self within a larger community and truly aims at increasing retention rates among the freshmen class. BDC enables students to interact with other incoming freshmen, find their niche on campus, as well as obtain a mentor (counselor) to look up to throughout the year.',
                'outcomes' => 'Bear Down Camp estimated 100 campers to attend but obtained a final number of 56, four more students than last year’s program.  Bear Down Camp is a crucial tool for freshmen. Valuable ideas and instruments are taught that can be applied both inside and outside of the classroom. Campers are given perspectives from a wide range of people and backgrounds to truly allow them to flourish. One camper recalls, “I learned more in those three days at camp than I could have ever imagined.” Every freshman deserves the chance to get a head start for their college career, and BDC definitely supplies this opportunity. Students are given a nucleus within the greater UA community to use as a sounding board and a foundation for everlasting friendships and connections throughout their college experience. “The most memorable part of Bear Down Camp was seeing how everyone rapidly turned into a family and accepted one another for who they are.” Bear Down Camp is an experience every freshman should be entitled to enjoy in order to grow and thrive during their first year of college. One camper recalls, “I received a flyer with my acceptance letter [in the mail] and I was interested in it. So, I did a little research and decided to sign up. Best decision of my life.”',
                'response' => 'Bear Down Camp strives to be the best it can be and takes pride in exposing incoming freshmen to a diverse and inclusive group of students and staff. According to a post-camp survey, 90.5% of campers said they were able to meet and appreciate a diverse group of people while at camp. Another 90.5% said they are more excited to become involved on campus after going through BDC. “The Wildcats really have a lot of spirit [and] tradition, but most importantly, they are one family.” Bear Down Camp is all about teaching and opening the eyes of eager freshmen to all that the first-year college experience has to offer. Campers may come in not knowing the traditional background of the U of A, but they can walk off the first bus ride to camp knowing “Bear Down,” the alma mater, and even the stories behind John “Button” Salmon and a plethora of other University traditions and culture. The spirit expressed during the three days at camp works as an educational tool and a binding unit for the freshmen partaking in this opportunity. The survey showed 92.8% of campers learned about various campus involvement opportunities, many acted on these opportunities and can now be found in Freshman Class Council, Primus Freshman Honorary, Residence Hall Association, Greek Life, and many more. “We definitely had the capacity and ability to have more campers. If our numbers were doubled, I have full confidence we still would have been able to have a great BDC.” This is Bear Down Camp’s time to shine. The foundation has been set, and the benefits are known. Great amounts of spirit and pride—in the classroom, on the field, or simply in the Tucson community—have come from Bear Down Camp, which can now only continue to grow.',
                'created_at' => '2012-07-31 21:40:26.000000',
                'project_id' => 772,
            ),
            2 => 
            array (
                'netid' => 'arezu',
                'scope' => 'ARIZONA ASSURANCE MEET YOUR MENTOR EVENT:
The funding request for $8,600 directly supported the Mentor and Scholar relationship by contributing to the initial event where the Scholars were introduced to their Mentors for the first time.  In addition, at this initial meeting, the Arizona Assurance Scholars met other mentors and fellow scholars in their college. Each of the colleges hosted an event with their scholars and mentors and the funding subsidized this critical first person to person meeting. The Scholars had the opportunity to meet their Mentor in a comfortable yet official and formal event that also connected them to their college.   The outcomes of this event were designed to lay the foundation for a cohort experience within the college and developed a mentor/mentee network of support that is academically based.

GRADUATE ASSISTANT (0.25 FTE, 10 hours/week):
The funding request for $9,400 provided Arizona Assurance with a half-time Graduate Assistant that worked to strengthen both sides of the mentoring relationship for the benefit of the student.  The Graduate Assistant built a comprehensive year-round training program that included development of podcasts, interactive modules, sample email communications and conversation starters, frequently asked questions and scenario solutions.',
                'outcomes' => 'OUTCOMES FOR ARIZONA ASSURANCE MEET YOUR MENTOR EVENT:
The collaboration between the Arizona Assurance Scholars Program and all of the colleges on campus was a vastly improved and successful mechanism for the initial meetings between the Arizona Assurance Scholars and their Faculty/Staff Mentor.  Not only were the events more intimate but they also connected the student directly to their college community which created a “College” welcome within the very first weeks of the Fall semester.


OUTCOMES FOR GRADUATE ASSISTANT (0.25 FTE, 10 hours/week):
Higher Education Masters Candidate Thomas Beckwith was hired as the Graduate Assistant to assist in improving the web presence and information and training materials for the Arizona Assurance Mentoring Program. His contributions are as follows:
•Transcribed student focus group interviews regarding their experiences with their mentors and suggestions for future cohorts.  Specifically scholars wanted to meet more often with their mentors and in relaxed social settings.
•Quality checked the Mentor/Mentee Matches
•Wrote the speaker biographies for Focus on Success
•Updated training and informational documents for Mentors
•Helped prepare mentor information packets
•Researched activities for mentors and mentees to do together
•Created a monthly list of suggested events for mentors and mentees
•Prepared presentation materials for the International Mentoring Institute for the Pre-Conference workshop titled “Best Practices in Academic Settings” presented by Mary Irwin, PhD and Laura Lunsford, PhD
•Was the administrator for the Arizona Assurance Scholars website
•Designed the layout for the new Arizona Assurance website
•Reviewed articles, books, and websites to develop content for the upcoming Arizona Assurance website
•Prepared an online resource guide for mentors and mentees for the upcoming Arizona Assurance website
•Worked on the protocol to interview Arizona Assurance Mentors
•Scheduled interview times for Arizona Assurance Mentors
•Worked on marketing materials for Mentor recruitment for fall 2012
•Guest Speaker for the higher education student success course',
                'response' => 'Student quotes regarding their Arizona Assurance Mentors:
o The biggest help I’ve had was with my mentor.  He gave me the push to change my major. He had the connections to help me go into labs.  My mentor has been able to help me see that there are many different things to study.
o Having that professional mentoring hand whenever I needed it was awesome.
o I was having some problems with maybe a teacher and the TA in one of my classes, so he really helped to like sort that out. And, he kind of pushed me to go talk to them about what actually the problems were, so that was really nice to have that extra back up.
o I remember coming here at first and being really scared; my mentor helped relieve a lot of that.',
                'created_at' => '2012-08-01 16:24:44.000000',
                'project_id' => 776,
            ),
            3 => 
            array (
                'netid' => 'dabriggs',
            'scope' => 'The funds from the First Year Student Fee were awarded to support the Supplemental Instruction (SI) Program during Fall 2011 and Spring 2012. The goal of the funding was to support existing SI courses and enable expansion of SI into the Biological Sciences, by supporting Introductory Biology. Supplemental Instruction is a proven retention program that teaches students transferable study skills that can be applied to other courses.   Supplemental Instruction is one of the premier services offered by the THINK TANK.  Historically, freshmen are the largest users of SI. Past THINK TANK data shows that students that participated in Supplemental Instruction did better in the course than students that did not participate.  In addition, students that participated in SI were much less likely to receive a D, an E, or Withdraw from the course. The data from this project confirms that students did better, were less likely to receive a D, and E, or Withdraw from the course.  We are very excited about the outcomes and respectfully thank the First Year Student Fee Board for providing us with funding for this opportunity to support students.',
            'outcomes' => 'The funds were used to support the wages of fifteen student Supplemental Instruction Leaders and two Graduate Assistant supervisors (see attached budget).  Each week the SI Leaders offered three Supplemental Instruction group sessions open to all students enrolled in the SI sections of the course.   In Fall 2011 SI was offered in GER160D1,  and 6 sections (taught by three faculty members) of MCB181R. In Spring 2012 SI was offered in ASTR170B1, GER160C1, MCB170C1, MCB181R, MCB184, and 2 sections of SPAN102 (same instructor for both sections).  The table in Part One in the attached Supporting Document shows the number of visits by course.  Each time a student attended a session it counted as one visit.  The table also shows the number of hours students participated in the sessions, and the number of unique students that took advantage of the sessions, by course.  Overall we had 1,080 students make 7,911 visits to SI sessions.  The students spent 8,019 hours in SI sessions.

The good news is that without exception students that attended SI did better in the course than students that did not attend.  A table of the mean final grades can be seen in Part Two of the Supporting Document.

In addition, the data shown in Part Three of the Supporting Document illustrates the power of SI to reduce the grades of D, E, and Withdrawals.  SI assists students in making academic progress towards their ultimate goal of a degree.',
            'response' => 'Two focus groups were scheduled during Spring 2012 (3/22, 4/4). All students that came for Supplemental Instruction during Fall 2011 were invited to attend one of the sessions.  Invitations were sent via email.  Each group had 6 participants.  $10 Dining Cards were given for attendance and participation in the focus group.  The full details of the findings can be found in Part Five of the Supporting Document.  Highlights include:

Most helpful/useful part of SI
-Better understanding of reading and course materials; seeing the “whole picture”
-More one-on-one interaction; participatory learning
-Hearing other students’ questions was helpful 
-Having an SI leader who had taken the course and was familiar with what would be on the quizzes; help determining which material was most important  
-Learning new tips and strategies to help remember information
-Having a schedule for studying rather than doing everything right before the tests
-Being in an environment where everyone is there to learn
-Getting better grades on quizzes and exams

Some student quotes taken from the focus group transcripts:
-[Through SI I] “Gained a routine for studying for that class. Had a schedule for doing SI.”
-[It helped me gain] “Better understanding of material. Got all of my questions answered.”
-[Helped me create] "In-depth explanations. [The]Professor might not have time to go over everything. [The] SI leaders would explain all the concepts and make connections. [Sessions] built on what you did last time and gained more cohesive knowledge.”
-[I] “met other people interested in studying and we would have study sessions”
-“Studying on my own I didn’t realize where my weaknesses were.  SI helped me see what I didn’t know.”',
                'created_at' => '2012-08-01 16:27:12.000000',
                'project_id' => 775,
            ),
            4 => 
            array (
                'netid' => 'rabeech',
                'scope' => 'Freshmen Connections and Ongoing Student Support was funded to engage students during Wildcat Welcome, specifically at New Student Convocation, to build a community for freshmen living off campus and to provide ongoing support to students as they transition through their freshmen year.  In building these connections, the freshmen will feel more a part of their UA community, learning what it means to be a Wildcat.  The goal and scope of the programs was to impact the entirety of the freshman class through their experiences at Wildcat Welcome and creating a community for those living off campus.',
                'outcomes' => 'Wildcat Welcome Marketing and Promotion
Promotional items with the Wildcat Welcome logo and website were provided to all students at orientation and a second set of items were given away to those freshman living on campus during the 11-12 school year.  Our goal was to provide students with an item that was useful beyond the Wildcat Welcome period so students were provided with post-it note flag books which could be used for note taking our highlighting in text books.

New Student Convocation Celebration
Moving New Student Convocation to the McKale Center in 2011 allowed an increase in attendance, from 2000 to 5500 students, and created a more spirited environment.  Students enjoyed the new format, staying through the event, and on to the celebration that follows.  The celebration event was very well attended, with students taking advantage of the activities, including a photo booth, the free class t-shirts and the food that was provided.

Off-Campus Freshmen Welcome 
Our off campus freshman welcome events provided students the opportunity to engage with other commuter students, connect with Outreach Facilitators and learn about the resources available to them. Our first off campus welcome event brought over 80 participants and we had the opportunity to see students in the same major meet one another before classes began and we had the opportunity to connect with the parents of some of these students who were happy to know that there were resources available to their student whether they lived in a residence hall or not. Students were then walked over to the convocation celebration to celebrate the year with their peers. We continued “welcoming” our off campus freshman throughout the year with programming events that connected them to resources, personal support, and peer connections. Throughout the year we saw over 550 students at our events.

Outreach Facilitator Training 
Outreach facilitator training was an ongoing process and includes learning how to create meaningful connections with students through digital formats. Facilitator training topics include, Financial Aid, Academic Advising & Course Registration, Counseling and Crisis Support, Multiculturalism and Working with Diverse Students, Leadership and Involvement, Academic Success Skills, Interpersonal Communication and Social Media & Technology. Our 15 Outreach Facilitators engaged in over 50 hours of activities and discussions throughout the year that helped them to work with students and provide resources and programming to meet their needs. Outreach Facilitators had staff meetings/training every week of the semester where information and resources were timely, relevant, and intentional.

Technology/Social Media Tools
We spent the first semester trying to engage students in our text message service and while we had 40 students sign up for it in the first three weeks of classes that number went down to 4 students by the time that November rolled around. In a satisfaction survey students said that the text message service felt redundant (meaning that they were receiving the resource messages in other formats (Facebook, Twitter, Wildcat Connections emails) and that it worked just as well to send an email than to text. After an unsuccessful semester we opted to stop the text message service and focus our resources on our more successful endeavors (Facebook, Twitter, Wildcat Connections emails). Our Wildcat Connection Facebook page earned over 500 “likes” in our fall semester. During the year we had over 112,921 digital contacts with our off campus freshman as well as the 4,674 phone contacts. Our communication plan includes weekly updates on dates & deadlines, what is happening (social, service, and educational events) and the resource of the week. The Outreach Facilitators engaged students through blogs, promotional event videos, and our Facebook, Twitter, and Wildcats Connections weekly emails.',
                'response' => 'Wildcat Welcome activities were well attended in fall 2011 due in large part to the marketing materials used to promote the events.  The biggest success was the increased attendance at New Student Convocation from just over 2000 students in 2010 to over 5500 in 2011.  

Students appreciate messages from Outreach Facilitators. Social Media engagement spiked during peak academic stress times (midterms, finals, and registration) and great feedback was received throughout the year. Here are a few examples:

Hi Kyla!! Thank you so much for your email! I just finished registering for my classes yesterday, I\'m really excited about the classes I\'m taking for fall. I just can\'t wait till this semester gets over, it\'s so exhausting. Kyla I was wondering, is it really bad if a person’s grades go down one semester? If maybe I get a few B\'s and C\'s? I was really worried about it, the last few weeks of college have been so stressful and I think I didn\'t handle it too well and now I\'m afraid my grades are slipping. Do people have to leave college if we get bad grades one semester?  I should talk to you about psychology as a major sometime! ( : Hope you’re doing good. ...and thank you so much for your emails! It really helps me stay in touch with what’s happening on campus and the events you\'ll host looks so much fun, I haven\'t gotten a chance to come to one yet, but I really want to attend one sometime! 

- Smriti

Hi. Just wanted to say that you have been great in emailing me this info. Never got to meet you in person, but wanted you to know that I have been appreciating these emails a lot!

- JungMin Kim',
                    'created_at' => '2012-08-01 23:12:23.000000',
                    'project_id' => 767,
                ),
                5 => 
                array (
                    'netid' => 'kriester',
                    'scope' => 'Today’s college freshman at UA brings more than just their personal belongings to college with them – for many they are also bringing along their parents and family members.  Research and experience shows that today’s parents want to be savvy consumers, involved partners and can be very influential on their student’s adjustment and academic progress here at UA.  

To assist in the welcoming, orienting and adjustment process the UA Parents and Family Association requested funding for three different activities and resources.  These included our Summer Send Off Programs, Parent Orientation Sessions and support for the mailing of the Parents & Family Magazine.

For the Summer Send Offs PFA provided 1500 Wildcat for Life Lanyards for 22 Summer Send Offs around the country.  These events welcomed freshman in their hometowns from July – August 2011.  In addition, $5000 was used to supplement funding the Phoenix Summer Send Off which was a free event for Freshman and their parents.  

The Parents & Family Association also received $4462 in funding for Proud Parent Buttons, Recycled Shopping Bags and Success the Wildcat Way Brochures which were distributed to over 5000 parents during the 2011 Parent Orientation Sessions.  These items helped to enhance our ability to welcome and orient the parents of our newest Wildcats.

The Parents & Family Association also received $3600 to assist with the cost of mailing the Parents & Family Association to all Freshman in Fall 2011 and Spring 2012.  This magazine is an excellent resource for the parents of our Freshman class.',
                    'outcomes' => 'Summer Send Off Programs:
The Parents & Family Association feels that it has achieved the outcomes of both welcoming and orienting parents of Freshmen.  While there was no formal evaluation done on the Summer Send Off program the feedback at each event to both Alumni Association Staff members and hosts was very positive.  Both parents and students report that the events help them to feel both excited and more comfortable about starting their fall semester at UA.  

Parent Orientation Sessions:
Parents Satisfaction of their overall Orientation experience at UA is very high.  While the surveying doesn’t specifically ask about the Proud Parent Buttons, shopping bags or Success the Wildcat Way brochures, PFA receives comments and feedback from parents on these items.  Parents are always very happy to receive their bags which help to store all the items they pick up throughout the Orientation process.  The Proud Parent buttons are a much sought after item that parents wear with pride.  The Success the Wildcat Way brochure helps to guide a conversation between parents and their freshman to establish the new relationship they will have with each other here at UA.  

The Parent & Family Magazine is evaluated each year in PFA’s Parent Survey.  Eighty-two percent of our parents surveyed received the Magazine.  More than 70% of parents surveyed state that they share some or all of the information in the Magazine with their student.  The other 30% use the information for their own personal use which is still a great outcome. See page 2 of 8 in the attached Master Summary Report.',
                    'response' => 'The Summer Send Off Programs that the Parents & Family Association co-sponsors with the University of Arizona Alumni Association impact students in several ways. First both students and parents get to meet students, parents and UA Alumni from their local area.  This is especially helpful so that they know other students from their area that are attending UA.  The Alumni are able to welcome the students and parents to the Wildcat Family and Alumni Association, UA staff members and parent volunteers with the Parents & Family Association are able to help the students get excited and the parents get comfortable with the notion that the students will be leaving for campus in a short period of time.  Close to 1000 students and their family members attended 23 different Sends Offs last summer.  The largest of these was the Greater Phoenix Area Summer Send Off.  More than 1100 students and their families attended this event.  

Proud Parent Buttons and recycled logo bags were distributed to over 5000 parents that attended New Student Orientation in 2011.  Parents often asked to take a button home for a parent who wasn\'t able to attend and they wore the buttons with pride.  We also distributed the Success the Wildcat Way brochure which is a structured dialogue that parents and students have with each other prior to coming back to campus.  It helps to open up lines of communication in areas where we know parents and students traditionally have issues which need to be negotiated in their changing relationship.

The Fall and Spring Editions of the Parents & Family Magazine were mailed to parents of UA Freshman.  The Magazine is filled with interesting articles and resources that parents often share with their students as evidenced by our Parents Satisfaction Survey (see attachment).',
                    'created_at' => '2012-09-13 23:09:42.000000',
                    'project_id' => 768,
                ),
                6 => 
                array (
                    'netid' => 'kriester',
                    'scope' => 'The Dean of Students SafeCats project addressed specific safety priorities for freshmen. The first priority focused on getting safety resource information directly into the hands of the students through “Be a Friend. Do Something.” Which is an innovative campaign created through SafeCats in the Dean of Students Office. The other provided direct safety tools for freshman to have throughout their time at the UA.  These safety initiatives will increase knowledge and build skills within our freshman population who are most vulnerable for risky situations.  

“Be A Friend. Do Something.” is a one-stop shop for no-nonsense advice on topics like alcoholism, depression and suicidal behavior, unhealthy eating habits, hazing, sexual consent and threatening behavior. Most importantly, it was designed in a way that appeals to the first year student population. In the Spring 2009 Safety Survey conducted by the Dean of Students Office, 75% of freshmen indicated that they were “very concerned” or “somewhat concerned” about their personal safety. Additionally, 52% indicated that they either “did not feel very safe” or “not safe at all” when walking around campus at night.  Sixty-eight percent (68%) of students living in the residence halls and 60% of freshmen indicated that they did not feel safe within the area immediately surrounding the campus.

The data indicates that our freshmen students and students living on campus are in most need of safety information and resources.',
                    'outcomes' => 'Our goal was to get Safety Cards, Safety Whistles and various print aspect of the Be A Friend Do Something Campaign into the hands of freshmen.  We hoped that this would help the to achieve the following outcomes. 
•	Develop an understanding of university resources
•	Develop an awareness of their individual strengths and talents through the accountability and personal responsibility;
•	Develop the ability take initiative for fellow students or friends on and off campus;
•	Develop confidence, commitment, and dedication to the UA community;
•	Take an active role in the partnership of campus safety.

Based on the Surveying in the Residence Halls in Spring 2012 we feel that students have achieved the following outcomes: 
•	Having an understanding of safety resources 
•	They are able to take initiative and help a friend in need (17% said that they did when in a situation to do so, 70% said they were never in a position where they needed to assist a friend).
•	They are able to take an active role in campus safety

The other two outcomes were very hard to measure in a concrete way.',
                    'response' => 'Impact on Students:
The Dean of Students Marketing Graduate Assistants who are funded through the Student Services Fee worked with the development and distribution of the materials for this grant.  They had quite a significant impact based upon the large amount of materials which were distributed to Residence Halls and other places that freshmen frequent.  These Graduate Assistants distributed marketing materials for the Safe Cats and Be a Friend Do Something Campaigns.  Safety Cards, posters, door hangers and ½ sheets wee distributed to students living in the residence halls (6,268) each semester.  These materials were also distributed to fraternities and sororities (903) and to 1500 students living off campus in apartments.  Additionally 15 advertisements ran in the Wildcat, electronic slides were played before the large lectures classes in Centennial Hall and Gallagher Theater.  Media was displayed on plasma TV’s in the Union and Rec Center.  Four advertisements were run on Facebook which had 2.3 million impressions.  They also ran a Facebook fake ID campaign targeted at UA students under the age of 21 which had over 200 open the add.

In surveying (see attached survey) students felt that the content of the Be A Friend Campaign was clear, useful and most importantly, we had messaging that captured their attention.',
                    'created_at' => '2012-09-13 23:56:02.000000',
                    'project_id' => 771,
                ),
                7 => 
                array (
                    'netid' => 'sbasij',
                    'scope' => 'New Student Convocation/Wildcat Welcome 2012 was funded to engage students during Wildcat Welcome, the period of 10 days surrounding the start of the fall semester during which welcome events, including New Student Convocation, are held to build a community for freshman living off and on campus and to provide ongoing support to students as they transition through their freshman year.  In building these connections, the freshman will feel more a part of their UA community, learning what it means to be a Wildcat.',
                    'outcomes' => 'Freshman Connections and Student Impact
The First Year Student Fee funds were used to promote, develop and grow the programs for Wildcat Welcome, specifically New Student Convocation.  All freshmen are invited to participate in the 10 days of programming.  Specific funds were targeted to: Wildcat Welcome Marketing and Promotion and New Student Convocation.  Wildcat Welcome is designed to give all new freshman a chance to connect with the UA community.  We actively promote the activities to students via New Student Orientation, email and social media platforms.

Wildcat Welcome Marketing and Promotion
Promotional items with the Wildcat Welcome logo and website were provided to all students at orientation and a second set of items were given away to those freshman living on campus during the 12-13 school year.  Our goal was to provide students with an item that they could use to get into the Wildcat spirit.  We created Wilbur and Wilma masks that contained all of the Wildcat Welcome events as well as posters distributed via the Residence Hall and through various social media platforms like Twitter and Facebook.   

New Student Convocation Celebration
New Student Convocation serves as the formal welcome from President Hart and UA Leadership to the incoming class of freshmen students to UA.  Hosting New Student Convocation at the McKale Center in 2012 again allowed an increase in attendance, from 5,500 to a little over 6,000 students, and created a more spirited environment.  Hosting our event at McKale is deliberate, as it is the only space on campus where we can accommodate the entire freshman class in one location at one time.  Students enjoyed the format and the opportunity to meet President Hart.  Students stayed through the event, and on to the celebration that followed.  The celebration event was very well attended, with students taking advantage of the activities, including a photo booth and the food that was provided.  Students were also able to pick up their free ‘Bear Down’ red class t-shirts, an item that identifies them as Wildcats and officially welcomes them to the UA family.',
                    'response' => 'Wildcat Welcome activities were very well attended in the fall of 2012.  We can attribute much of that to the experience of New Student Convocation and the readiness of the marketing materials used to promote the events.  The biggest success we had was in the increase of attendance at New Student Convocation.  In 2010 2,000 students attended the New Student Convocation event and in 2011 attendance grew to over 5,500 and in 2012 we had a little over 6,000 in attendance.  McKale staff facilitating the event estimated attendance based on seating areas.',
                    'created_at' => '2012-12-13 17:57:50.000000',
                    'project_id' => 769,
                ),
                8 => 
                array (
                    'netid' => 'arezu',
                    'scope' => 'The Arizona Assurance Scholars Program is an institutional priority set forth by President Shelton that began in 2008.  The program is the University of Arizona’s commitment to access and completion of a baccalaureate degree for low-income students. Arizona Assurance Scholars are provided with a financial aid package that allows them to attend the University of Arizona with little or no debt. We invest significant faculty and staff resources to ensure that we do not rest on providing access to higher education. Arizona Assurance’s bold mark is that it aggressively pursues degree completion for each enrolled student, with the goal of changing the complexion of the educated population of Arizona and therefore, increasing the value of a degree earned by all students at the University of Arizona.  

The Arizona Assurance Scholars Program received funding for a Graduate Assistant to make further strategic improvements to its freshman year mentoring program that will benefit this year’s freshmen and freshmen for years to come. 

GRADUATE ASSISTANT (0.33 FTE, 13 hours/week):
The Graduate Assistant works to strengthen both sides of the mentoring relationship for the benefit of the student.  The Graduate Assistant is building a comprehensive year-round training program that includes development of podcasts, interactive modules, sample email communications and conversation starters, frequently asked questions and scenario solutions.  The graduate assistant supervises two undergraduates and the team is collecting stories and videos to create a promotional and training video for mentors and mentees.',
                'outcomes' => 'The goals are in process of being met.  The interviews and video production will take the entire academic year to complete.  At mid-point, two sets of mentor/mentees have been videotaped and interviewed along with seven students.  Four comprehensive newsletters have been produced and have been shared with the UA Foundation and with UA News, with one story so far featured on the UA website. The newsletters can be read on the assurance.arizona.edu homepage. The student stories can be read at assurance.arizona.edu (AZA Scholar Stories and at AZA Mentor Program).  The video will be finished by the end of the 2012-2013 academic year.

The structure of a training manual for the mentors is attached and all the content will be finalized by the end of the academic year.',
                    'response' => '85% of the 492 students registered for the AZA Mentor Program for this academic year.  This does not include the students that have maintained the mentoring relationships over the years.

1.	Karen Rivas took advantage of the Arizona Assurance Mentorship Program, pairing her with her future academic advisor, Dr. Paul Blowers. “I remember the first time I met Dr. Blowers, it was during the freshman orientation.  He basically went over the major for me, and it definitely helped solidify that I was going into the right major, in terms of what I’d like to do and where I’d like to be in life.” Rivas said.  “This (mentorship) program has been very beneficial in the sense that it’s always given me someone to be able to ask questions to—questions that I wouldn’t be able to have answered at home or answered by my friends.” 

2.	Three years ago, Wendy Wong received an Arizona Assurance Scholarship to attend the University of Arizona, where she accrued the necessary credits to gain acceptance to the Graduate College of Pharmacy.   According to her, one of the greatest benefits of Arizona Assurance—aside from the funding it provided—was its Mentorship Program, in which she was paired with Dr. Jeannie Lee, her future professor, academic advisor, and a research associate with the Arizona Center on Aging.  Wong credits Dr. Lee for both reaffirming her ambitions to pursue pharmaceutical studies and for furthering her interest in geriatric pharmacy.  Even though the Arizona Assurance Mentorship Program only stipulates participation during a student’s first year, Wong and Dr. Lee have stayed in touch, and regularly have lunch with other mentors and mentees within the College of Pharmacy.',
                    'created_at' => '2012-12-20 20:56:20.000000',
                    'project_id' => 782,
                ),
                9 => 
                array (
                    'netid' => 'dabriggs',
                'scope' => 'The purpose of the Math 100 Support Initiative has been to provide direct support for the 375 freshman enrolled in Math100AX.  The funding has been used to hire 4 GTA’s to teach students enrolled in SAS100AX.  SAS100AX is a companion class to Math100AX. SAS100AX has two main objectives. First, to help students succeed in the on-line math class they are currently taking, and second, to provide them with techniques and strategies that they will use to succeed in future, lecture based math classes like Math 112.  This fall semester, each GTA taught 5 sections of the SAS100AX class. Each section had no more than 19 students.  This provided an excellent opportunity for each GTA to form a connection to their students, becoming familiar with each student’s strength and abilities.  Class time was devoted to assisting students with content in their Math100AX class, as well as lectures on note taking, test preparation, test taking, and other mathematics study strategies.  Roughly half of the students enrolled in Math100AX completed four credits of math (Math100AX and Math100B) and are enrolled in a lecture based math class for the spring.  These students will face a challenging transition as they go from on-line delivery style class to a more traditional in-person class.  The GTA’s will continue to assist these students, and the  students enrolled in Math100B, with one-on-one tutoring, drop-in tutoring, and test preparation during the spring 2013 semester.',
                    'outcomes' => 'The success rates for Math100AX are:
Passing (Grade of A, B, or C) = 84%
Fail (Grade of D or E) = 13%
Withdraw (W or WP) = 3%

The success rates for Math100B are:
Passing (Grade of A, B, or C) = 74%
Fail (Grade of D or E) = 24%
Withdraw (W or WP) = 1%

One of the primary objectives was to provide in person assistance to students enrolled in the on-line Math100AX class.  Much of the credit for the success rates of the Math100AX and Math100B class belongs to the Math Instructor for the course, Michelle Woodward, and her staff of UTAs who provide excellent support to these students.  What the GTA’s and the in-class tutors, provided by the Think Tank, did was to provide additional in-person assistance, encouragement, and techniques that helped make these students more independent learners.

At this point in time we do not have retention data.',
                    'response' => 'During the course of the year we communicate frequently with the students enrolled in the course.  In addition to course evaluations the THINK TANK Learning Specialists assigned to work with the students hear what the students say about the course.  Below are some of the quotes shared by them:
“I like the SAS 100AX class, Larry makes everything make sense.”

“I reviewed with Larry in SAS class, he makes it easy to remember.”

“The review tab on ALEKS is helpful.”

“I like math online because I have to learn…ALEKS makes me do it.”

“I am getting pretty awesome at math.”

“I use the SAS class time to get help from Think Tank tutor.”

“I like the SAS class a lot.”

“I did better than I thought…math has gotten easier, ALEKS is helping…feel good about math.”

“I tested into Math 112 for the spring!”

“I feel really confident for my math final after going through the pie, I’m excited for college algebra.”

At the beginning of the spring semester we will hold a focus group of SAS100AX students to generate more feedback and get recommendations from the students.',
                    'created_at' => '2012-12-20 23:29:37.000000',
                    'project_id' => 780,
                ),
                10 => 
                array (
                    'netid' => 'kvd',
                    'scope' => 'Scholarship Universe is a student financial resource that is two-fold in purpose. Firstly, we seek to provide the UA student population with better, properly vetted and legitimate, scholarship opportunities. Secondly, we make the process of finding scholarships for which UA students are individually eligible faster, more efficient, less confusing, and ultimately easier. In our first two years of existence Scholarship Universe has done this by creating the only University-built and the nation’s largest strictly maintained and properly vetted database of non-university affiliated scholarships. Scholarship Universe Phase I created an internet application that made the process of finding state-wide and national scholarships less time-intensive and more user-friendly. Now, this same efficiency will be applied to UA-only scholarships. In the last year we began to add more local UA-only scholarships to our database, and moving forward into the next academic year the primary focus of Phase II is to connect students with UA-only scholarship opportunities in their colleges and campus-wide.',
                'outcomes' => 'In our proposal last year we asked for funding from the First Year Student Fee to enable us to hire 2 additional FWS student staff so that we could better tackle new initiatives moving forward into this academic year. After receiving funds from the First Year Student Fee we hired those 2 additional student staff (Both Freshmen) and set to work on our planned Phase II. Phase II entails the addition of UA college and Departmental scholarships and their individual application processes onto Scholarship Universe so that UA students will see every scholarship they are eligible for, and so that no student will miss an opportunity for university scholarship support. The initial testing stage of Phase II will begin this Spring semester (2013). We have the College of Engineering and the College of Medicine, as well as all the scholarships administered by OSFA and many of the scholarships administered by the UA Foundation as our first test group. After this preliminary testing stage we will be prepared to offer participation to every college and department on campus beginning next fall.
Another important indicator of our efforts to engage with and support the Freshmen population is our growth in users from last year’s incoming freshmen class to this year’s. Last year we saw a %45 participation rate among incoming freshmen. This year, due to our increased marketing and outreach, we’ve seen that percentage grow to %55 (Appendix, Fig. 7). Among our efforts to engage with the incoming Freshmen population we partnered with Admissions to make sure that admitted students see Scholarship Universe as part of the Next Steps process, and we participated in every campus orientation and numerous  high school outreach events this last year.  Our goal is to get students active early on in their search for scholarships and alternative financial resources so that they can build a skill set which will aid them as upperclassmen, and as they matriculate out both into the job market or into graduate school.',
                'response' => 'Our site usage statistics from Google Analytics provide a clear and encouraging picture of increasing student engagement and application use.  In the last year we’ve seen consistent positive increases in all the important metrics such as Total Visits (up by 36%), Unique Visitors (up by 12%), and Total Pageviews which showed an incredible 189% increase from this time last year. The pageviews metric is important to consider because it tells us that not only are more students logging in to Scholarship Universe, but those students are also more engaged; staying longer, viewing scholarships, and using the site’s new features. The average time on site increased by over 5 minutes per user-visit, and the average number of users spending more than 10 minutes per visit increased equally substantially from less than 30% of total users to more than 40% (Appendix, Fig 5).
Furthermore, we conducted several focus groups with student this semester in order to determine both how the student community perceives our program effectiveness, and also to get general feedback on what they’d like to see changed, and what they’d like from us moving forward. The feedback we received was largely positive, and while there were some criticisms we can use those points to better improve the service we offer. First and foremost the students in our focus group mirrored those in the Student Services Fee annual survey in desiring more institutional help finding scholarships and financial aid resources. 24 of the 30 participants in our focus groups were Scholarship Universe users. When asked if Scholarship Universe was a “worthwhile use of Student Services Fee funds?” they all replied positively. Following this, there was also unanimous demand for the inclusion of Departmental and College specific scholarships which is what we are working towards with Phase II in Spring 2013.',
                    'created_at' => '2012-12-20 23:52:07.000000',
                    'project_id' => 781,
                ),
                11 => 
                array (
                    'netid' => 'danthaix',
                'scope' => 'The Division of Student Affairs is committed to building a campus community in which students, faculty, and staff at all levels feel valued and have the opportunities to succeed. Connecting students to the resources available to them from the beginning of their academic career is an essential component to success, and the University’s retention efforts. The Finding Community Welcome (FCW) aimed to both increase the visibility of the programs and services provided by the four cultural centers, the Disability Resource Center (DRC), LGBTQ Affairs, and the Women’s Resource Center (WRC), and to help incoming students “find community” the very first week of school. 

The FCW has happened for the past two years as an outgrowth of Residence Life’s former “Student of Color Welcome.” Recognizing students’ intersecting, and multiple identities, our team sought to expand the Welcome, and highlight the major identity-based support centers at the University of Arizona. 

The 2012 FCW successfully occurred on Wednesday, August 22nd in the Student Union Memorial Center’s Grand Ballroom. Of the 407 participants that signed in, 61% were first year students. The event contained three main components which included a multicultural student club fair, a presentation of information about various support communities on campus that included videos of the areas, and student cultural performances. The FCW continued beyond the day of the event with FCW Passports, which had students visit individual areas’ open houses or events to qualify for a second set of raffle items that were given out on November 1st .',
                'outcomes' => 'Below are the 2012 goals stated in our Finding Community Welcome (FCW) proposal and how they were successfully addressed.

1)To allow students to have an opportunity to learn about diverse clubs and organizations on campus. 
a.The FCW provided a mini club fair with 15 multicultural organizations that provided information on how to connect with their communities. 
b.According to our FCW evaluation, 77 % agreed that this event increased his/her understanding of diverse clubs and opportunities to get involved on campus. 

2)To allow students to meet other new students in order to expand their peer networks.
a.The FCW provided a Human Bingo Icebreaker that allowed students to interact with other students that they did not know. In order for students to acquire a raffle ticket for the raffle items, students had complete the bingo form, ensuring that they met multiple students and expanded their peer networks at this event. 
b.According to the FCW evaluation, 53% agreed that this environment helped them make connections with other students.

3)To allow students to experience cultural student performances.
a.The FCW provided cultural student experiences from the NASA, DRC, AASA, APASA, and CHSA. From a presentation of Olympic athletes from DRC to a tutorial on stepping from the Greek organization of AASA, students were engaged throughout the event. 

4)To allow students to learn about AASA, APASA, CHSA, DRC, LGBTQ Affairs, NASA, & WRC. 
a.The event itself was lead by the ASUA Diversity Directors, in order to highlight the commitment ASUA has to student diversity and social justice initiatives on campus.
b.With the help of the Student Affairs Marketing team, a sustainable promotional video was created that helped define, promote, and experience each of the highlighted areas. 
c.According to the FCW evaluation, 75% of students agreed that they learned about at least one organization or resource that they plan to use during the school year.',
    'response' => 'The 2012 FCW was a great success in the engagement and retention of new students through multiple aspects of intentional programming for marginalized populations and effectively getting information to students through a more successful and sustainable mediums. After two years of a very successful programming of the FCW, the planning team reevaluated the event to find that we had strayed from the original purpose of welcoming the traditionally marginalize communities on campus and providing community resources to them. To help bring the original purpose back to the the forefront of the event, we restructured the marketing of the event to focus on first year marginalized populations, yet allowing any student to participate. The FCW has had large numbers in the past two years, but the goals for this year were to get this intentional information to the desired targeted population. This change produced a smaller number of participants at 407, but a more engaged audience that we expected. 

This year the FCW team was also able to create a professional video covering the areas that were focused on in the FCW. This reusable video gave an effective commercial for these students, allowing for positive messages to participate in these communities. Along with the last two FCW’s, the event was evaluated with Campus Labs. According to the evaluation, the student attendance was very diverse in gender, sexual orientation, and race. Below are some of the evaluation comments made from the students that aligned with assessing the successful outcome of the program.

•“The FCW brought together a diversity of people to help them feel like they belong in a school that\'s as big as ours.”
•“The bingo [icebreaker] helped me break the ice and not be so shy around other people.”
•“Now I know I can get more involved”',
    'created_at' => '2012-12-21 17:19:52.000000',
    'project_id' => 778,
),
12 => 
array (
    'netid' => 'claudia',
'scope' => 'Bear Down Camp is a four-day camp in which incoming freshmen travel up to Prescott, Arizona to learn about University history and traditions, how to get involved on campus, and how to succeed during their first year at the University of Arizona. BDC focuses on developing a sense of self within a larger community and truly aims at increasing retention rates among the freshmen class. BDC enables students to interact with other incoming freshmen, find their niche on campus, as well as obtain a mentor (Gato) to look up to throughout the year.',
    'outcomes' => 'Bear Down Camp estimated 200 campers to attend but obtained a final number of 188, 132 more students than last year’s program. We are in the process of measuring our impact on these students as well as their impact on others in the freshmen class through surveys. We hope to continue to expand the program and partner with other on-campus entities to further increase student retention while continuing to explore other programs in place at institutions nationwide.',
'response' => 'We are still in the process of compiling survey results, but everything to date has been largely helpful and supportive of the program. This year, we had 11 workshops that all Gatitos (campers) rotated through in addition to large group sessions. Overall, positive feedback has been overwhelming and students seemed to truly appreciate the program style. Students were able to immerse themselves in our rich UA culture and traditions and carry this on to our football games, classes, and other campus activities where they were able to teach their peers what they learned at camp.',
    'created_at' => '2013-01-02 17:38:54.000000',
    'project_id' => 773,
),
13 => 
array (
    'netid' => 'kriester',
'scope' => 'According to Wartman and Savage (2008) research shows that parent involvement and support, especially in the first year of college “provides important benefits in terms of adjustment to college, academic success and retention” (page 31).  Studies even demonstrate that there is a positive correlation between parental support and GPA (Cutrona and others, 1994).  The Survey of National Student Engagement in 2007 found that students who talk frequently to their parents and follow their advice are more likely to participate in extracurricular activities and to be more satisfied with their overall college experience (National Survey of Student Engagement, 2007). Research shows that most students are talking to their parents an average of 1.5 times per day (Junco and Mastrodicasa, 2007).  We also know that most students turn to their parents for support in times of stress or uncertainty and look for advice on how to solve problems as they arise.  For these reasons, it is important to welcome and orient parents to UA and to provide on-going, timely communication about resources and services that may help their student be successful.     

To assist in the welcoming, outreach and adjustment process the UA Parents and Family Programs requested and received funding for two different activities and resources.  These included the Phoenix Summer Send Off Program and support of the mailing of the Parents & Family Magazine each semester to freshmen.',
    'outcomes' => 'The Greater Phoenix Area Summer Send-Off was held on July 29th at the Scottsdale Plaza Resort.  We welcomed over 1200 incoming freshmen and their families from the greater Phoenix area and showed them the support they will have from not only the university; but from Phoenix, Tucson and the surrounding communities and businesses.  The attendees were able to meet UA President Dr. Ann Weaver Hart, Athletic Director Greg Bryne, Arizona Head Football Coach Rich Rodriguez, Alumni President Melinda Burke, and many other UA dignitaries.  Participants were able to interact with their fellow incoming freshmen, UA Alumni, as well as our amazing sponsors, all in a relaxed and fun-filled environment.

In November 2012 the Fall Edition of the UA Parents and Family Magazine was mailed to approximately 12,000 UA families including those of our freshman class thanks to First Year Student Fee support.  The Fall 2012 Magazine included articles on UA’s 21st President, Dr. Ann Weaver Hart, THINK TANK Success Stories, UA’s New Degree Tracker, student athlete accomplishments in the 2012 Olympics and Paralympics Games, PFA Grant Awards for 2012-2013 as well as an article on the Phoenix Summer Send Off.',
    'response' => 'Phoenix Summer Send Off –
A survey was sent to student attendees, unfortunately only 12 students responded.  So, this is not a representative sample.  However, the students who did respond indicated that they enjoyed the Send Off and that it made them feel welcome at UA. 

Parents & Family Magazine - 
Parents will be surveyed in spring 2013 on all Parent & Family Programs communication and those results will be included in the final report.',
    'created_at' => '2013-01-03 21:44:20.000000',
    'project_id' => 783,
),
14 => 
array (
    'netid' => 'kmbeyer',
    'scope' => '1 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
    'outcomes' => '2 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
    'response' => '3 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
    'created_at' => '2013-01-11 17:56:45.000000',
    'project_id' => 777,
),
15 => 
array (
    'netid' => 'dabriggs',
'scope' => 'The purpose of the Math 100 Support Initiative was to provide direct support for the 376 freshman enrolled in Math100AX.  The funding was used to hire 4 GTA’s to teach students enrolled in SAS100AX.  SAS100AX is a companion class to Math100AX. SAS100AX has two main objectives. First, to help students succeed in the on-line math class they are currently taking, and second, to provide them with techniques and strategies that they will use to succeed in future lecture based math classes.  During the fall semester, each GTA taught 5 sections of the SAS100AX class. Each section had no more than 19 students.  This provided an opportunity for each GTA to form a connection to their students and become familiar with each student’s strength and abilities.  Class time was devoted to assisting students with content in their Math100AX class, as well as lectures on note taking, test preparation, test taking, and other mathematics study strategies.  Roughly half of the students enrolled in Math100AX completed four credits of math (Math100AX and Math100B) and enrolled in a lecture based math class during the spring.  The GTA’s continued to assist these students, and the students enrolled in Math100B, with one-on-one tutoring, drop-in tutoring, and test preparation during the spring 2013 semester. Eighty-seven of the students completed a credit bearing mathematics course during the spring semester with an A, B, or C grade.',
'outcomes' => 'Of the students that completed Math100AX in fall 2012 (N=190), the following grades were received in the next level of math attempted during the spring (courses include 100, 100B, 105, 111, 112):
Passing (Grade of A, B, or C) = 44.23%
Fail (Grade of D or E) = 50.00%
Withdraw (W or WP) = 5.77%

Of the students that fast-tracked into Math100B during the fall (N=186), the following grades were received in the next level of math attempted during the spring (courses include 100B, 105, 109C, 111, 112, 113, 120R, 163, and 196L):
Passing (Grade of A, B, or C) = 40.59%
Fail (Grade of D or E) = 46.53%
Withdraw (W or WP) = 12.87%

One hundred and thirty students (35%) took no math course in the spring semester.  

Forty-one students (11%) did not return for the spring semester. 

At the end of the spring semester 67% (N=225) of the students that persisted completed the semester with a cum GPA of 2.0 or better.  Two students had a 4.0 GPA.',
    'response' => 'In the spring semester we hosted three focus groups with students that were participating in the Schedule for Success program.  Below are some of the summarized impressions about the SAS100AX class shared by them:
Overall impressions:
-Really liked it, instructor was great and motivated us. Felt comfortable because there were other students like me.  
-Created a community.
-Helped in other classes too. Instructor taught how to take notes. 
-Individualized instruction was helpful.  Good to be a small class. 
Most helpful:
-Individualized attention
-Getting used to getting help and advising that we need
-Note-taking and organizational skills
Suggestions :
-Keep it the same, no changes
-More focus on math, like a second math class.
-Option of additional SAS class for spring math courses
Advice to new students in S4S
-Stay caught up with your assignments, don’t fall behind
-Stay calm and go with the program. If you do everything you are more likely to succeed.
-Use all your resources
-Know what’s available to you. You can have a social life but academics should come first.
-Try to focus on it, don’t take it lightly. Really focus on the math so you don’t get behind.',
    'created_at' => '2013-08-21 00:09:04.000000',
    'project_id' => 780,
),
16 => 
array (
    'netid' => 'danthaix',
'scope' => 'The Division of Student Affairs is committed to building a campus community in which students, faculty, and staff at all levels feel valued and have the opportunities to succeed. Connecting students to the resources available to them from the beginning of their academic career is an essential component to success, and the University’s retention efforts. The Finding Community Welcome (FCW) aimed to both increase the visibility of the programs and services provided by the four cultural centers, the Disability Resource Center (DRC), LGBTQ Affairs, and the Women’s Resource Center (WRC), and to help incoming students “find community” the very first week of school. 

The FCW has happened for the past two years as an outgrowth of Residence Life’s former “Student of Color Welcome.” Recognizing students’ intersecting, and multiple identities, our team sought to expand the Welcome, and highlight the major identity-based support centers at the University of Arizona. 

The 2012 FCW successfully occurred on Wednesday, August 22nd in the Student Union Memorial Center’s Grand Ballroom. Of the 407 participants that signed in, 61% were first year students. The event contained three main components which included a multicultural student club fair, a presentation of information about various support communities on campus that included videos of the areas, and student cultural performances. The FCW continued beyond the day of the event with FCW Passports, which had students visit individual areas’ open houses or events to qualify for a second set of raffle items that were given out on November 1st .',
'outcomes' => 'Below are the 2012 goals stated in our Finding Community Welcome (FCW) proposal and how they were successfully addressed.
1)To allow students to have an opportunity to learn about diverse clubs and organizations on campus 
a.The FCW provided a mini club fair with 15 multicultural organizations that provided information on how to connect with their communities. 
b.According to our FCW evaluation, 77 % agreed that this event increased his/her understanding of diverse clubs and opportunities to get involved on campus. 
2)To allow students to meet other new students in order to expand their peer networks
a.The FCW provided a Human Bingo Icebreaker that allowed students to interact with other students that they did not know. In order for students to acquire a raffle ticket for the raffle items, students had complete the bingo form, ensuring that they met multiple students and expanded their peer networks at this event. 
b.According to the FCW evaluation, 53% agreed that this environment helped them make connections with other students.
3)To allow students to experience cultural student performances
a.The FCW provided cultural student experiences from the NASA, DRC, AASA, APASA, and CHSA. From a presentation of Olympic athletes from DRC to a tutorial on stepping from the greek organization of AASA, students were engaged throughout the event. 
4)	To allow students to learn about AASA, APASA, CHSA, DRC, LGBTQ Affairs, NASA, & WRC. 
a.The event itself was lead by the ASUA Diversity Directors, in order to highlight the commitment ASUA has to student diversity and social justice initiatives on campus.
b.With the help of the Student Affairs Marketing team, a sustainable promotional video was created that helped define, promote, and experience each of the highlighted areas. 
c.According to the FCW evaluation,  75% of students agreed that they learned about at least one organization or resource that they plan to use durning the school year.',
'response' => 'The 2012 FCW was a great success in the engagement and retention of new students through multiple apects of intentional programming for marginalized populations and effectively getting information to students through a more successful and sustainable mediums. After two years of a very successful programming of the FCW, the planning team reevaluated the event to find that we had strayed from the original purpose of welcoming the traditionally marginalize communities on campus and providing community resources to them. To help bring the original purpose back to the the forefront of the event, we restructured the marketing of the event to focus on first year marginalized populations, yet allowing any student to participate. The FCW has had large numbers in the past two years, but the goals for this year were to get this intential information to the desired targeted population. This change produced a smaller number of participants at 407, but a more engaged audience that we expected. 

This year the FCW team was also able to create a profesional video covering the areas that were focused on in the FCW. This reusable video gave an effective commercial for these students, allowing for positive messages to participate in these communities. Along with the last two FCW’s, the event was evaluated with Campus Labs. According to the evaluation, the student attendance was very diverse in gender, sexual orientation, and race. Below are some of the evalulation comments made from the students that aligned with assessing the successful outcome of the program.
•“The FCW brought together a diversity of people to help them feel like they belong in a school that\'s as big as ours.”
•“The bingo [icebreaker] helped me break the ice and not be so shy around other people.”
•“Now I know I can get more involved”',
'created_at' => '2013-08-23 18:09:29.000000',
'project_id' => 778,
),
17 => 
array (
'netid' => 'mairwin',
'scope' => 'Arizona Assurance (AZA) is an institutional priority set forth by President Shelton that began in 2008.  The program is the University of Arizona’s commitment to access and completion of a baccalaureate degree for low-income students. Arizona Assurance Scholars are provided with a financial aid package that allows them to attend the University of Arizona with little or no debt. We invest significant faculty and staff resources to ensure that we do not rest on providing access to higher education. Arizona Assurance’s bold mark is that it aggressively pursues degree completion for each enrolled student, with the goal of changing the complexion of the educated population of Arizona and therefore, increasing the value of a degree earned by all students at the University of Arizona.  

The Arizona Assurance Scholars Program received funding for a Graduate Assistant to make further strategic improvements to its freshman year mentoring program that benefited the 2012-2013 freshman cohort and will benefit other freshmen for years to come.',
'outcomes' => 'Creative Writing Masters Candidate Colin Hodgkins was hired as the 2012-2013 Graduate Assistant to assist in improving the web presence, information and training materials for the Arizona Assurance Mentoring Program including materials for both mentors and mentees. His contributions are as follows:

•Interviewed, filmed and wrote stories about mentors and mentees for the AZA website (all have been published to the U of A website and are posted at assurance.arizona.edu) 

•Wrote comprehensive training manuals for both mentors and mentees (paper copies provided upon request) 

•Administrator for Arizona Assurance Scholars website; made significant improvements and redesigned the layout for the Arizona Assurance website

•Reviewed articles, books, and websites to develop content for the Arizona Assurance website

•Prepared an online resource guide for mentors and mentees for the upcoming Academic Success & Achievement website

•Supervised two student workers who filmed, interviewed, and photographed scholars, mentors and then wrote stories for the monthly newsletter, to share with the UA Foundation and content for the website

•Wrote marketing materials for Mentor recruitment for fall 2013 

•Assisted at Arizona Assurance Events

•Edited communications and marketing materials for Arizona Assurance Scholars Program 

•Contacted and interviewed campus partners for content inclusion in Guides, publications and for the website 

•Organized an informational workshop for the Director of Campus Health to present a lecture regarding student stress to the Mentors',
'response' => 'During the fifth academic year of the AZA Mentor Program, 418 freshmen were paired with 275 volunteer faculty/staff mentors.
•1,943 meetings were recorded between mentors and scholars

Karen Rivas (Engineering major) and Dr. Paul Blowers: 
“I remember the first time I met Dr. Blowers, it was during the freshman orientation.” Rivas said. “This (mentorship) program has been very beneficial in the sense that it’s always given me someone to be able to ask questions to, questions that I wouldn’t be able to have answered at home or answered by my friends.  Also just having someone to tell me, ‘you know this is what you should do and this is how you should do it’, just to have that motivation.” Dr. Blowers, recipient of the UA’s 2012 Distinguished Professor Award was a first generation college graduate, he knows how challenging a college-level curriculum can be, and the importance of having a mentor. 

Wendy Wong (PharmD major) and Dr. Jeannie Lee: 
According to Wendy, one of the greatest benefits of AZA, aside from the funding it provided, was its Mentorship Program, in which she was paired with Dr. Jeannie Lee, her future professor, academic advisor, and a research associate with the Arizona Center on Aging. Wong credits Dr. Lee for both reaffirming her ambitions to pursue pharmaceutical studies and for furthering her interest in geriatric pharmacy. Regardless of where her profession takes her, Wong is grateful to those who have helped her to get there. Her advice to incoming Arizona Assurance Scholars is “Definitely sign up, because if you ever have something you’re worried about or not too sure, and if you just don’t know what to do, honestly, contact your mentor and they will talk to you about it, listen to you, and give you advice.”',
'created_at' => '2013-08-24 01:34:01.000000',
'project_id' => 782,
),
18 => 
array (
'netid' => 'kvd',
'scope' => 'Scholarship Universe is a scholarship matching system developed entirely within the Office of Scholarships and Financial Aid with significant contributions from both Student Affairs Marketing and Systems Group. UA students log in using their NetID and complete an individual profile that is generated from scholarship eligibility requirements. We then match each student to currently available scholarships they are eligible for. Our team of student employees search for, and vet, scholarship opportunities in order to make sure that our database is not only always up-to-date, but that we are also protecting UA students from scams as well as not wasting their time and energy on dead ends. 

Scholarship Universe seeks to provide the UA student population with better, properly vetted and legitimate, scholarship opportunities. However, we also make the process of finding scholarships for which UA students are individually eligible faster, more efficient, less confusing, and ultimately easier. In our first two years of existence Scholarship Universe has done this by creating the only University-built and the nation’s largest, strictly maintained and properly vetted database of non-university affiliated scholarships. A third, but no less important function Scholarship Universe serves is to advocate on the student’s behalf with scholarship donors both nationwide and at the UA, as well as with UA colleges and departments that offer scholarship opportunities. We strive to ensure that all UA students have an opportunity and that no scholarship funds go unused because eligible candidates could not be found.',
'outcomes' => 'Scholarship Universe Phase I created an internet application that made the process of finding state-wide and national scholarships less time-intensive and more user-friendly. In the last year this same efficiency was applied to UA college-specific and departmental scholarships. In spring 2013 we went live with an online application for two UA colleges and their departments. The proposal we submitted this year outlined two very specific measures of program effectiveness. 1) Meeting the developmental timeline for the implementation of Phase II (the Scholarship Universe Departmental App). 2) Increasing student use of Scholarship Universe and bringing in more non-UA scholarship dollars to UA students.  

1)Phase II Implementation
We met our intended goals this past spring to go live with the College of Engineering’s scholarship application on February 25th and the College of Medicine (Tucson Campus) on April 15th. Along with meeting the implementation timeline to provide scholarship applications online we also provided specific software functionality that allows departments the ability to review applications, grade applicant’s submitted documents and to award the selected winners via UAccess. Moving forward Scholarship Universe is forming partnerships to potentially go live with all of the following Colleges this fiscal year: Medicine (Phoenix Campus), Honors, Science, Social and Behavioral Sciences, Agriculture and Life Sciences, Education, Humanities, Fine Arts and other potentially interested divisions on campus that manage scholarships. Our goal is to have as many UA colleges and departments taking applications and awarding students via Scholarship Universe as possible. UA students will only have to complete one profile to be matched to every scholarship opportunity available to them.

2)Increases in Both Student Use and Scholarship Awards
2012 going into AY 2013 was really the first year that Scholarship Universe was fully operational. Scholarship application deadlines are largely 9-12 months before the academic year in which the award is received. So awards received by students in AY2013 were applied for in AY2012. Attached is a graph (Fig. 1) that helps us understand Scholarship Universe’s impact on the student population in AY 2013. It shows that both the number of students winning external scholarships, as well as the amount of scholarship dollars won have both increased a great deal. Furthermore, as we see by comparing 2012’s website statistics (Fig. 3) to those of 2011 (Fig. 2) we show a dramatic increase in both the number of UA student using Scholarship Universe as well as the time they spent using the application.',
'response' => 'Our site usage statistics from Google Analytics provide a clear and encouraging picture of increasing student engagement and application use.  Comparing the UA academic years of 2011-2012 [Fig. 2] to 2012-2013 [Fig. 3] we’ve seen consistent positive increases in all the important metrics such as Total Visits (up by 63%), Unique Visitors (up by 44%), and Total Pageviews which showed an incredible 109% increase. The pageviews metric is important to consider because it tells us that not only are more students logging in to Scholarship Universe, but those students are also more engaged; staying longer, viewing scholarships, and using the site’s new features. The average time on site increased by almost 4 minutes per user-visit. Furthermore, the initial pilot of the Phase II departmental scholarship applications for both College of Engineering and the College of Medicine Tucson Campus were both very successful with each college receiving more scholarship applications than in prior years.',
'created_at' => '2013-08-26 19:48:09.000000',
'project_id' => 781,
),
19 => 
array (
'netid' => 'kriester',
'scope' => 'According to Wartman and Savage (2008) research shows that parent involvement and support, especially in the first year of college “provides important benefits in terms of adjustment to college, academic success and retention” (page 31).  Studies even demonstrate that there is a positive correlation between parental support and GPA (Cutrona and others, 1994).  The Survey of National Student Engagement in 2007 found that students who talk frequently to their parents and follow their advice are more likely to participate in extracurricular activities and to be more satisfied with their overall college experience (National Survey of Student Engagement, 2007). Research shows that most students are talking to their parents an average of 1.5 times per day (Junco and Mastrodicasa, 2007).  We also know that most students turn to their parents for support in times of stress or uncertainty and look for advice on how to solve problems as they arise.  For these reasons, it is important to welcome and orient parents to UA and to provide on-going, timely communication about resources and services that may help their student be successful.     

To assist in the welcoming, outreach and adjustment process the UA Parents and Family Programs requested and received funding for two different activities and resources.  These included the Phoenix Summer Send Off Program and support of the mailing of the Parents & Family Magazine each semester to freshmen.',
'outcomes' => 'The Greater Phoenix Area Summer Send-Off was held on July 29th at the Scottsdale Plaza Resort.  We welcomed over 1200 incoming freshmen and their families from the greater Phoenix area and showed them the support they will have from not only the university; but from Phoenix, Tucson and the surrounding communities and businesses.  The attendees were able to meet UA President Dr. Ann Weaver Hart, Athletic Director Greg Bryne, Arizona Head Football Coach Rich Rodriguez, Alumni President Melinda Burke, and many other UA dignitaries.  Participants were able to interact with their fellow incoming freshmen, UA Alumni, as well as our amazing sponsors, all in a relaxed and fun-filled environment.

In November 2012 the Fall Edition of the UA Parents and Family Magazine was mailed to approximately 12,000 UA families including those of our freshman class thanks to First Year Student Fee support.  The Fall 2012 Magazine included articles on UA’s 21st President, Dr. Ann Weaver Hart, THINK TANK Success Stories, UA’s New Degree Tracker, student athlete accomplishments in the 2012 Olympics and Paralympics Games, PFA Grant Awards for 2012-2013 as well as an article on the Phoenix Summer Send Off.  

In April the spring edition of Parent & Family Magazine was mailed to approximately 18,000 students including all current freshmen (thanks to the First Year Student Fee) and parents of incoming freshman.  It included articles on: UA Baseball Coach, Andy Lopez, Signing up for UAlert, Scholarship Universe, the 24/7 Computing Assistance along with a new student section on Math Placement Testing, New Student/Parent Orientation, Parent Activities during Wildcat Welcome and the Linden Counseling Assistance Fund.',
'response' => 'Student/Parent Response
Phoenix Summer Send Off –
A survey was sent to student attendees, unfortunately only 12 students responded.  So, this is not a representative sample.  However, the students who did respond indicated that they enjoyed the Send Off and that it made them feel welcome at UA.  

Parent & Family Magazine - 
UA parent and family members feedback has indicated that they believe that the Parent & Family Magazine is a good resource and they often share information with their student.',
'created_at' => '2013-08-30 00:38:06.000000',
'project_id' => 783,
),
20 => 
array (
'netid' => 'sbasij',
'scope' => 'The First Year Student Fee funds were used to sponsor the 2012 New Student Convocation and Convocation Celebration.',
'outcomes' => 'The First Year Student Fee funds were used to promote, develop and grow the programs for Wildcat Welcome, specifically New Student Convocation.  All freshmen are invited to participate in the 10 days of programming.  Specific funds were targeted to: Wildcat Welcome Marketing and Promotion and New Student Convocation.  Wildcat Welcome is designed to give all new freshman a chance to connect with the UA community.  We actively promote the activities to students via New Student Orientation, email and social media platforms.',
'response' => 'Promotional items with the Wildcat Welcome logo and website were provided to all students at orientation and a second set of items were given away to those freshman living on campus during the 12-13 school year.  Our goal was to provide students with an item that they could use to get into the Wildcat spirit.  We created Wilbur and Wilma masks that contained all of the Wildcat Welcome events as well as posters distributed via the Residence Hall and through various social media platforms like Twitter and Facebook.   

New Student Convocation Celebration
New Student Convocation serves as the formal welcome from President Hart and UA Leadership to the incoming class of freshmen students to UA.  Hosting New Student Convocation at the McKale Center in 2012 again allowed an increase in attendance, from 5,500 to a little over 6,000 students, and created a more spirited environment.  Hosting our event at McKale is deliberate, as it is the only space on campus where we can accommodate the entire freshman class in one location at one time.  Students enjoyed the format and the opportunity to meet President Hart.  Students stayed through the event, and on to the celebration that followed.  The celebration event was very well attended, with students taking advantage of the activities, including a photo booth and the food that was provided.  Students were also able to pick up their free Bear Downn red class t-shirts, an item that identifies them as Wildcats and officially welcomes them to the UA family.
Attach a document with supporting data:',
'created_at' => '2013-08-30 21:14:21.000000',
'project_id' => 769,
),
21 => 
array (
'netid' => 'sbasij',
'scope' => 'New Student Convocation/Wildcat Welcome 2013 was funded to engage students during Wildcat Welcome, the period of 10 days surrounding the start of the fall semester during which welcome events, including New Student Convocation, are held to build a community for freshman living off and on campus and to provide ongoing support to students as they transition through their freshman year.  In building these connections, the freshman will feel more a part of their UA community, learning what it means to be a Wildcat.',
'outcomes' => 'Freshman Connections and Student Impact
The First Year Student Fee funds were used to promote, develop and grow the programs for Wildcat Welcome, specifically New Student Convocation.  All freshmen are invited to participate in the 10 days of programming.  Specific funds were targeted to: Wildcat Welcome Marketing and Promotion and New Student Convocation.  Wildcat Welcome is designed to give all new freshman a chance to connect with the UA community.  We actively promote the activities to students via New Student Orientation, email and social media platforms.
Wildcat Welcome Marketing and Promotion
Promotional items with the Wildcat Welcome logo and website were provided to all students at orientation and a second set of items were given away to those freshman living on campus during the 13-14 school year.  Our goal was to provide students with an item that they could use to get into the Wildcat spirit.  One way that we were able to inform students about Wildcat Welcome and more specifically New Student Orientation was through the use of Wilbur and Wilma masks that we distributed at Orientation sessions. On the front of each mask was the face of Wilbur or Wilma and on the back was information about the events that occurred during Wildcat Welcome, specifically New Student Convocation.  Another way that we were able to reach out to students was through Residence Life. We printed posters that were distributed to RAs in primarily Freshman Halls. These posters listed many of the big events that were going to occur during Wildcat Welcome. These posters were also distributed to students during Residence Hall move-in. 
New Student Convocation Celebration
New Student Convocation serves as the formal welcome from President Hart and UA Leadership to the incoming class of freshmen students to UA.  Hosting New Student Convocation at the McKale Center in 2013 again allowed us to continue to reach 6,000 freshmen.  Hosting our event at McKale is deliberate, as it is the only space on campus where we can accommodate the entire freshman class in one location at one time.  Students enjoyed the format and the opportunity to meet President Hart.  Students stayed through the event, and on to the celebration that followed.  The celebration event was very well attended, with students taking advantage of the activities, including an organization fair and the food that was provided.  Students were also able to pick up their free ‘Bear Down’ red class t-shirts, an item that identifies them as Wildcats and officially welcomes them to the UA family. The design of these shirts is unique so that students from this class are able to easily identify each other.',
'response' => 'Wildcat Welcome activities were very well attended in the fall of 2013.  We can attribute much of that to the experience of New Student Convocation and the readiness of the marketing materials used to promote the events.  One of our biggest successes was that for the second year in a row we had at least 6,000 students attend New Student Convocation. McKale staff facilitating the event estimated the attendance based on the seating.',
'created_at' => '2013-12-12 18:21:13.000000',
'project_id' => 786,
),
22 => 
array (
'netid' => 'kvd',
'scope' => 'Scholarship Universe (SU) is the University of Arizona’s (UA) web-based scholarship matching system developed entirely within the Office of Scholarships and Financial Aid with significant contributions from both Student Affairs Marketing and Systems Group. UA Students log in to SU with their NetID, are asked a series of questions to create their eligibility profile, and then are matched to scholarships for which they’re eligible. Our team of student employees continually search for, and verify, scholarship opportunities as well as develop and code new features and functionality.
Phase I of Scholarship Universe is focused on initial site development and adding non-UA scholarships to the database. It makes the process of finding scholarships faster, more efficient, less confusing, and ultimately easier for UA Students.  The UA is the only known university to offer such a robust service to its students. In 2012, Scholarship Universe was selected out of 354 global nominees to receive a Campus Technology Innovators Award.  
Phase II of Scholarship Universe is focused on the development of the Scholarship Universe Departmental Application (SUDA).  SUDA allows colleges and departments at the UA to post and administer the application, selection, and awarding of their scholarships in SU.  This further increases the value of SU for our students because it provides them a one-stop-shop for non-UA and UA scholarships, minimizing both their time and efforts. SUDA also increases the transparency of the scholarship process, making it easier for students to understand which departmental scholarships they’re actually applying for.',
'outcomes' => 'Scholarship Universe Phase I is an internet application that makes the process of finding state-wide and national scholarships less time-intensive and more user-friendly.  Earlier this year we started work on Phase II both in terms of developing SUDA and bringing on college and departmental partners.  In our funding proposal we outlined two goals: 1) meeting the developmental timeline for the implementation of Phase II and 2) increasing student use of Scholarship Universe and bringing in more non-UA scholarship dollars.
1)Phase II Implementation:
A significant amount of development work outlined in our scope of work document for Phase II has been completed.  As mentioned in our previous progress report, we successfully piloted Phase II with the College of Engineering in which over $600,000 in scholarships were offered to students via Scholarship Universe. Currently we are working with numerous other colleges and departments to add their scholarships to SU. Progress varies from college to college (see Fig. 1) and within certain colleges, progress varies between departments.  For example, the College of Social and Behavioral Science is decentralized. There are sixteen individual departments with scholarships all managed and administered independently of each other. Whereas the College of Engineering has a centralized model allowing for a faster implementation into SUDA. Partnering with Scholarship Universe has been entirely voluntary on behalf of colleges and departments. The number of current partners and potential interested partners shown in figure 1 demonstrates the recognized value and breadth of service SUDA will be providing moving forward.

2)Increases in Both Student Use and Scholarship Awards:
Student usage and engagement remain strong.  We saw solid increases in the number of students using SU, the amount of time they’re spending on the site, and the amount of content they’re viewing (see Student Response section below).  Being in the middle of the academic year, our data set of received non-UA scholarship awards is incomplete. However, our initial assessment of disbursed awards YTD, indicates that we’re on par with last year and may very well exceed last year’s totals.  More information will be provided on this subject in our next progress report when we have a complete picture of the 2013-2014 academic year.',
'response' => 'More students are using Scholarship Universe than ever before. As of 11/13/13, 30,739 enrolled students (about 75% of all UA Students) have logged into SU at least once; this includes 6,731 freshman (about 22% of SU users).  Looking at Google Analytics data, since Jan. 1st 2013, we’ve had 25,318 unique visitors. These unique visitors encompass currently enrolled and incoming students. In terms of site usage, we’ve seen a 29% increase in visits over the previous year, a 16% increase in pageviews, a 6% increase in avg. visit duration, and a slight increase in returning visitors as opposed to new visitors*.  These metrics indicate that SU is still increasing in terms of usage and popularity but also demonstrate the shift in usage patterns as the proportion of returning users continues to increase.  
Over the past semester, we conducted a survey of students who received a non-UA scholarship.  One of the questions asked was “Where did you find this scholarship opportunity?”  About 30% of respondents indicated that they’d found their scholarship via a high school counselor or newsletter.  However, this is a resource that freshmen will no longer have access too once they come to the UA.  SU provides scholarship resources which were formerly provided by high school counselors, thus enabling freshmen to continue to search for and win scholarships.
Finally, we’ve seen a 243% increase in the number of times a student has flagged a scholarship as ‘Interested’ in SU since Jan 1st, 2013 – rising from 18,449 to 63,269 during that time (see Fig. 3).  We suspect that this is a result of more scholarships being available in SU.  This further provides evidence of increasing student engagement as students are actively using this feature.

*Based on a comparison of google analytics statistics from 03/01/12-12/6/12 and 03/01/13-12/6/13 (see Fig. 2)',
'created_at' => '2013-12-17 23:08:19.000000',
'project_id' => 792,
),
23 => 
array (
'netid' => 'rjm264',
'scope' => 'For the first five years of the Arizona Assurance program’s existence, first-year Scholars were paired with faculty and staff volunteers that served as their mentors for the academic year. The goal of the mentoring component of the program is to support students’ transition into their first year at the University and to retain them into their second. As of Fall 2013, first-year Arizona Assurance Scholars were no longer required to sign up for the mentoring program – yet 274 enrolled voluntarily. Additionally, for the first time, students that are not Arizona Assurance Scholars were invited to participate in the program. 

The mentoring program has grown into a new standalone initiative, the Arizona Mentors program. An allocation from the First Year Student Fee made it possible to fund a graduate assistant that has provided significant support to the Arizona Mentors program and multiple initiatives in Academic Success & Achievement. Colin Hodgkins, a Masters Candidate in the Creative Writing Program, served as a graduate assistant supporting Arizona Assurance in the 2012-13 academic year. With First Year Student Fee funding, it was possible to rehire Colin to continue to support Arizona Assurance Scholars. Just as the Arizona Mentors program expanded, so did Colin’s role in Academic Success & Achievement. Colin’s professional writing and editing skills proved tremendously valuable, and his research experience helped with several department-wide projects that support our efforts to engage, retain, and educate first-year students.',
'outcomes' => 'Colin provided significant support for Academic Success & Achievement’s objectives this semester. His work contributed to ongoing changes the department is implementing to expand its offerings, create more opportunities for students to be engaged, and support student retention. Thousands of UA undergraduates are served by Academic Success & Achievement’s programs each semester, and an immeasurable number of current and prospective students are impacted by Colin’s work. Much of Colin’s research will help guide the department’s efforts for the long term. This semester, Colin:

*Interviewed, filmed, and wrote stories about mentors and mentees for the Arizona Mentors website. Additional profiles of mentees and mentors are planned for the Spring 2014 semester.
*Edited marketing and recruitment materials for the Cubs to Wildcats program and publications for other Academic Success & Achievement initiatives.
*Assisted with content for a new website for the Arizona Mentors program (website creation pending other departments’ approval and collaboration).
*Supervised an undergraduate employee who managed social media presence for Arizona Assurance program and profile Scholars for website and materials.
*Contributed to and edited the Arizona Assurance program profile in High Tech, High Touch: Campus-Based Strategies for Student Success, a book showcasing ten outstanding programs recognized by the Association of Public and Land-Grant Universities. 
*Served as a consultant for Fall 2013 structural changes to the Arizona Mentors program. Colin has regularly met with the director of the program to develop a strategy for expanding the program.
*Conducted research into online learning platforms that could potentially be used by programs on campus. This project involved reviewing materials, contacting the companies’ liaisons, and consulting with known customers of several products. Colin compiled his findings and made recommendations for how online learning could be built into existing Academic Success & Achievement programs to engage learners through a variety of media.
*Researched dozens of cohort-based programs from universities around the country designed to transition first-year students to and through their sophomore year.
*Helped prepare a three-session development series for faculty and staff in the Arizona Mentors program for Spring 2014 involving campus partners and their expertise.
*Researched service experience programs at peer institutions and conducted research into programs in departments similar to Academic Success & Achievement (pending).
*Developed a national directory of faculty/staff mentor and service experience programs for the programs’ web presence (pending).',
'response' => 'Kenneth Saufley and Ed Reid
As a homeschooled high school student from a family with limited financial resources, college did not seem like a realistic possibility for Kenneth Saufley. Saufley met a volunteer teacher, Edward Reid, through high school music programs, and Reid opened up a whole new world of possibilities. Reid has formally mentored Arizona Assurance Scholars every year since the program’s inception in 2008, so it was only logical for the trumpeters’ relationship to continue. “(Arizona Assurance) sent me an email asking if I’d like to be a part of (The Mentorship Program) and I said, ‘of course.’ And so I first thought, ‘Who could be my mentor,’ and then I immediately knew, ‘Professor Reid.’” Reid knows the value of mentorship, not only from his experience with Arizona Assurance, but from recalling his own experiences as an aspiring musician. “Ninety percent of my progress has been Mr. Reid showing me how to get from point A to point B, demonstrating and showing me that path,” Saufley said.',
'created_at' => '2013-12-20 00:16:02.000000',
'project_id' => 793,
),
24 => 
array (
'netid' => 'kriester',
'scope' => 'The goal of the Goal of the Dean of Students Parent & Family Programs Summer Send Off & Communications plan is to help develop a sense of community and involvement for our newest Wildcats and their parents.  We know that many parents are very involved in the life of their college student here at UA.  So, part of the communication includes the mailing of the Parent & Family Magazine once a semester to the parent of each Freshman.  We will also utilize this funding to help pay part of the expenses for our largest freshman/parent event: the Greater Phoenix Area Summer Send Off.  Below you will find a brief summary of both projects.

Greater Phoenix Summer Send Off – The Greater Phoenix Send Off is the largest Send Off event held each summer with all incoming freshmen from the greater Phoenix area invited to attend.  The Greater Phoenix Summer Send Off is free of cost to freshmen and their families.   A group of parent volunteers works with the Parents & Family Association Director to raise money in the form of sponsorships for this event each year.  

Parent & Family Magazine - Parents & Family Programs publishes and distributes one magazine each semester.  The Parents & Family Magazine provides helpful information for parents, stories about students, faculty and staff members and campus resources.  The Magazine provides a great glimpse of what is happening on campus each semester.',
'outcomes' => 'The Greater Phoenix Area Summer Send Off had a record year in attendance.  There were a total of 1231 attendees which was an increase of just over 100 guests from 2012.  In addition, this year a break out session was added for students and parents so that they could address any last minute questions prior to coming to campus.  There were also 48 volunteers most of whom were UA Alumni and current parents of UA students.  UA Band Members, Wilbur, Wilma and the Cheerleading Squad were also there to help welcome the new students and their families.  The attendance at this event has grown from 434 in 2009 to 1231 in just six year.  The Summer Send Off Committee of parent volunteers and UA Administrators are very thankful for the support of the First Year Student Fee which helped offset our $14,000 catering bill.

The Fall Edition of the UA Parent & Family Magazine was mailed in early December to freshman parents and donors to Parent & Family Programs.  We will be surveying parents in the spring on the Magazine and the Send Off events and will report our results in the final report.  We will also have one more mailing of the Magazine which will go out in early April.',
'response' => 'Both students and parents reported enjoying the Greater Phoenix Area Summer Send Off.   They had an opportunity to meet other students from Phoenix, to see some of the services offered and to get questions answered both by the UA staff in attendance and by each other prior to coming to campus in August.  They also had a tremendous feeling of being welcomed to the University of Arizona.  An added benefit is that the UA BookStore distributed IPads to student scholarship recipients.   This enabled students to have their IPad prior to arriving on campus.',
'created_at' => '2013-12-28 20:27:15.000000',
'project_id' => 787,
),
25 => 
array (
'netid' => 'danthaix',
'scope' => 'The Division of Student Affairs is committed to building a campus community in which students, faculty, and staff at all levels feel valued and have the opportunities to succeed. Connecting students to the resources available to them from the beginning of their academic career is an essential component to success, and the University’s retention efforts. The Finding Community Welcome (FCW) aimed to both increase the visibility of the programs and services provided by the four cultural centers, the Disability Resource Center (DRC), LGBTQ Affairs, and the Women’s Resource Center (WRC); and to help incoming students “find community” the very first week of school. 

This year the planning team reevaluated the program to meet the original purpose of the event, which was to welcome the traditionally marginalize communities on campus and recognize students’ intersecting and multiple identities. To help bring the original purpose back to the the forefront of the event, the committee restructured the programming and marketing efforts of the event to focus on first year traditionally marginalized populations while making the event open to any student that wanted to participate.

The FCW’s main components included a presentation of the multiple support centers, a welcome from Student Affairs Administration and ASUA student leadership, student cultural performances, and a multicultural club fair.',
'outcomes' => 'The 2013 FCW successfully occurred on Tuesday, September 3, 2013 in the Student Union Memorial Center’s Grand Ballroom. The physical setup of the event changed and was divided into two main areas. The main program occurred in the South Ballroom and the multicultural club fair with the food began after the program in the North Ballroom. Students were checked in using a CatCard swipe system, which was new this year and reflected 278 attendees. According to the FCW evaluation, 53% were first year students. Due to some issues with the swipe system and because many participants entered through a non-monitored door in the North Ballroom, the committee estimated the full room attendance closer to the 450 seats that were set up. 

Below are the 2013 goals stated in our Finding Community Welcome proposal and how they were successfully addressed.

1) To allow students to learn about AASA, APASA, CHSA, DRC, LGBTQ Affairs, NASA, & WRC.
a. According to the FCW evaluation,  75% of students agreed that they learned about at least one organization or resource that they plan to use during the school year.
b. The ASUA Diversity Directors spoke to highlight the commitment ASUA has to student diversity and social justice initiatives on campus. 
c. With the help of the Student Affairs Marketing team, a sustainable promotional video was created that helped define, promote, and highlight each of the centers. 

2) To allow students to have an opportunity to learn about diverse clubs and organizations on campus.
a. This year we had 38 different multicultural and social justice clubs at the fair, a 250% increase from the 2012 FCW.
b. According to our FCW evaluation, 81 % agreed that this event increased their understanding of diverse clubs and opportunities to get involved on campus. 

3) To allow students to meet other new students in order to expand their peer networks.
a. The FCW provided a Human Bingo Icebreaker that allowed students to interact with other students that they did not know. In order for students to acquire a raffle ticket for the raffle items, students had complete the bingo form, ensuring that they met multiple students and expanded their peer networks at this event.
b. According to the FCW evaluation, 56% agreed that this environment helped them make connections with other students.

4) To allow students to experience cultural student performances
a. The FCW provided cultural student experiences from the NASA, AASA, APASA, and CHSA centers. From a traditional Native American blessing to open the event to a tutorial on stepping from the greek organizations of AASA, students were engaged throughout the event.

In conclusion, the main purpose in hosting the FCW was to connect students to the resources here to support underrepresented students on campus. We feel the FCW showcases the diversity at UA from the very beginning of the year, and helps students know their support systems during their tenure as Wildcats.',
'response' => 'The 2013 FCW was a great success in the engagement and retention of new students through multiple apects of intentional programming for marginalized populations and effectively getting information to students through a more successful and sustainable mediums. To ensure we hit the target population for this event, we marketed by utilizing the listservs from the cultural centers, DRC, LGBTQ Affairs, and WRC, as well as directly emailing all students who indicated they are students of color upon application. In the past, we had emailed all first year students about the FCW, but we really wanted to make sure we honored the purpose of the event. The event was open to all UA students through specific venues, which included advertisement in the Daily Wildcat, a mention at new student Convocation, on the Class of 2017 Facebook Page, and other social media outlets. The sustainable FCW video continues to live on the centers’ websites, allowing for positive messages to participate in these communities throughout the year.

As in the past, the event was evaluated with Campus Labs. According to the evaluation, the student attendance was very diverse in gender, sexual orientation, and race. New this year was that we were able to attract a greater number of transfer students. The overall student response was very positive. Below are some of the evalulation comments made from the students that aligned with assessing the successful outcome of the program.

• “I loved seeing all the different ethnicities of the speakers and attendees, because it really highlights the diversity at UA.”
• “All the clubs helped me discover new things.”
• “Dividing the room and having seating space right up to the stage was nice because you could see and hear everything.”
• “My favorite part was seeing the performances, because I like seeing the diverse cultures on stage.”',
'created_at' => '2013-12-30 04:15:04.000000',
'project_id' => 785,
),
26 => 
array (
'netid' => 'johannej',
'scope' => 'Our freshman class brings together a diverse group of students from across the United States and throughout the world. Each student brings a different level of knowledge of existing policies, laws, and resources that can support his/her safety while attending the University of Arizona. Additionally, many freshmen experience new situations in which they are unsure of how to respond, which may be further compounded if they are unfamiliar with the campus and the Tucson community. 
UA SafeCats, housed within the Dean of Students Office, is designed to be a “one-stop” network that brings safety to the forefront of campus life. SafeCats is using this grant to fund a multi-faceted safety campaign that addresses specific safety priorities for freshmen. The first priority focuses on getting safety resource information directly into the hands of the students through the bystander intervention campaign “Be a Friend. Do Something. ” This innovative campaign created through SafeCats uses dramatic, eye-catching images, on educational posters, door hangers, and a website to challenge students to step up and do the right thing when they realize their friend is at risk. 
Another primary safety tool that is funded through the First Year Student Fee is the safety card. This provides a direct safety tool for freshman to have throughout their time at the UA by providing each student with a wallet-sized card containing safety resources, telephone numbers, and websites. These safety initiatives will increase knowledge, provide resources, and build skills within our freshman population who are most vulnerable for risky situations.',
'outcomes' => 'SafeCats developed a set of learning outcomes in our Freshmen Fee proposal that we hope to see our freshmen achieve and that we also believe meet the goals and mission of the University of Arizona, the Student Affairs Division, and advance the goals of the First Year Student Fee. Attached are the learning outcomes and the work we are doing to achieve them. Please see that attached supporting documentation for a full explanation of the impact and outcomes for the SafeCats First Year Student Fee grant.',
'response' => 'The estimated number of freshman students impacted is 10,000. This figure was calculated based on the number of students who live on campus in residence halls, who are enrolled in Centennial Hall classes, and the number of students typically reached at UA Clicks and New Student Orientation. 
• 7,000 safety business cards distributed to freshman during move-in weekend. These wallet-sized cards provide safety resources, telephone numbers, and websites. 
• 933 “Be A Friend. Do Something.” posters & 1,900 “Be A Friend. Do Something.” door hangers were distributed in the residence halls during the fall semester.  Using dramatic, eye-catching images, this campaign challenges students to step in and do the right thing when they realize their friend is at risk.

As mentioned previously, the number of visits to the Dean of Students SafeCats website and to Be a Friend has increased since the start of the school year.  Using Google Analytics to track traffic to the Dean of Students website, we have found that the “Be A Friend” site averages 9.7 visitors per week and the SafeCats site averages 25.6 visitors per week (Sep.-Nov. 2013). This traffic to the website has shown that students are utilizing the URL found on the posters, door hangers and plasmas to seek out more information and resources.',
'created_at' => '2013-12-30 13:08:25.000000',
'project_id' => 790,
),
27 => 
array (
'netid' => 'claudia',
'scope' => 'Bear Down Camp is a four-day experience for incoming freshmen of the University of Arizona. BDC is held annually in Prescott, Arizona for a continually growing number of students. The camp staff consists of an administrative team, counselors, and a crew team, all of which are volunteer students whom attending camp trainings throughout the academic year and at the end of the summer. While at camp, freshmen are divided into smaller groups lead by two counselors and participate in various activities that meet the camp’s mission and learning objectives. These include academic and on-campus resources, involvement opportunities, educational health resources, and providing general information that every student should know and utilize in a dynamic university setting.  Ultimately, Bear Down Camp’s primary goal is to engage incoming students in camp activities to prepare them for their first year of college and increase the University of Arizona’s overall retention rate. Additionally, we hope to create a peer network for students of all levels to rely on throughout their collegiate careers.',
'outcomes' => 'Bear Down Camp made a goal of taking 250 freshman students to camp and registered 241 students in the three-month registration period.  Ultimately, we were able to bring 230 students to Bear Down Camp. This number has increased by 42 students from the 188 students that enrolled in the 2012 camp. The program received the opportunity to test a new type of marketing that we believe to have been very successful as exhibited by the increase in campers. This year, we hope to perfect BDC’s marketing plan to continue the successful outreach that occurred for the 2013 camp. In addition to this, we hope to continue reaching out to local high schools, sending information to out-of-state orientations, and attending college fairs to outreach to potential campers.',
'response' => 'The camp learning objectives are completed through the implementation of a series of workshops. Each workshop has a main focus and counselors are responsible for delivering the information in a manner that gives a holistic view of the topic while also not overwhelming the campers. This year, the number of workshops was reduced from 11 to 7 to dedicate more time to team building activities and new additions to the program, including an Involvement Fair. A post camp survey of 80 students exhibited that 87% of the students agreed that they felt prepared for their freshman year while 90% agreed that Bear Down Camp has improved their overall University of Arizona experience thus far. Additionally, 93% of the students found the workshops helpful and 98% of students have gotten involved in at least one type of involvement, whether it be Greek Life, clubs, honoraries, or other campus-affiliated organizations. While these numbers will be helpful in improving this year’s camp, the survey was taken on a voluntary basis. Therefore, in the future, we hope to initiate a more reliable system of garnering feedback. By doing this, we hope to continue to expand the program and partner with other on-campus entities to further increase student retention while continuing to explore other programs in place at institutions nationwide.',
'created_at' => '2014-01-06 17:26:11.000000',
'project_id' => 794,
),
28 => 
array (
'netid' => 'hiatt',
'scope' => 'The Campus Night Security Project (CNSP) supports two UAPD security officers for night duty in and around student housing facilities on campus. The two officers are on duty Thursday, Friday, and Saturday nights from 10 pm - 2 am, the days and hours most police involved incidents occur, during the fall 2013 and spring 2014 semesters. The officers patrol the Park (West) and Highland (East) campus neighborhoods on foot, by golf-cart, and car. This physical presence of these officers helps bring an increased sense of safety and security to the campus.',
'outcomes' => 'The goal of the proposal is to increase the safety and security of our campus residents. While only the mid-point of the project, Residence Life can report that the CNSP has thus far fulfilled its goal. 

The CNSP patrol reported over 362 defined activities from September 6th – December 6th 2013 of which:

- 67% of all activities pertained to Area, Building and Welfare checks; 
- 14% were field interview/interaction related; 
- 3.9% were hall checks;
- 2.2% were minor in possession related; and
- 2.2% were traffic related; and 
the remaining activities were a combination of many different types of activities. The number and type of activities indicate that officers are making their presence known and are providing a sense of increased safety and security on campus.',
'response' => 'During fall 2013 over 4,500 students living in the Park and Highland area halls were directly impacted by the CNSP. The additional 1,300+ students living on campus have likely benefitted indirectly from either being in these areas during patrol hours or learning/hearing about the patrol from classmates.',
'created_at' => '2014-01-07 19:19:25.000000',
'project_id' => 791,
),
29 => 
array (
'netid' => 'claudia',
'scope' => 'Bear Down Camp is a four-day experience for incoming freshmen of the University of Arizona. BDC is held annually in Prescott, Arizona for a continually growing number of students. The camp staff consists of an administrative team, counselors, and a crew team, all of which are volunteer students whom attending camp trainings throughout the academic year and at the end of the summer. While at camp, freshmen are divided into smaller groups lead by two counselors and participate in various activities that meet the camp’s mission and learning objectives. These include academic and on-campus resources, involvement opportunities, educational health resources, and providing general information that every student should know and utilize in a dynamic university setting. Ultimately, Bear Down Camp’s primary goal is to engage incoming students in camp activities to prepare them for their first year of college and increase the University of Arizona’s overall retention rate. Additionally, we hope to create a peer network for students of all levels to rely on throughout their collegiate careers.',
'outcomes' => 'Bear Down Camp made a goal of taking 250 freshman students to camp and registered 241 students in the three-month registration period. Ultimately, we were able to bring 230 students to Bear Down Camp. This number has increased by 42 students from the 188 students that enrolled in the 2012 camp. The program received the opportunity to test a new type of marketing that we believe to have been very successful as exhibited by the increase in campers. This year, we hope to perfect BDC’s marketing plan to continue the successful outreach that occurred for the 2013 camp. In addition to this, we hope to continue reaching out to local high schools, sending information to out-of-state orientations, and attending college fairs to outreach to potential campers.',
'response' => 'The camp learning objectives are completed through the implementation of a series of workshops. Each workshop has a main focus and counselors are responsible for delivering the information in a manner that gives a holistic view of the topic while also not overwhelming the campers. This year, the number of workshops was reduced from 11 to 7 to dedicate more time to team building activities and new additions to the program, including an Involvement Fair. A post camp survey of 80 students exhibited that 87% of the students agreed that they felt prepared for their freshman year while 90% agreed that Bear Down Camp has improved their overall University of Arizona experience thus far. Additionally, 93% of the students found the workshops helpful and 98% of students have gotten involved in at least one type of involvement, whether it be Greek Life, clubs, honoraries, or other campus-affiliated organizations. While these numbers will be helpful in improving this year’s camp, the survey was taken on a voluntary basis. Therefore, in the future, we hope to initiate a more reliable system of garnering feedback. By doing this, we hope to continue to expand the program and partner with other on-campus entities to further increase student retention while continuing to explore other programs in place at institutions nationwide.',
'created_at' => '2014-08-06 01:30:58.000000',
'project_id' => 794,
),
30 => 
array (
'netid' => 'rjm264',
'scope' => 'This year, the funding ASA received for graduate assistant support helped significantly expand the Arizona Mentors program and launch a department-wide service initiative. Formerly a part of the Arizona Assurance Scholars program, Arizona Mentors pairs students (many of whom are at the highest risk of leaving the University without graduating) with faculty and staff members that support their development as students and citizens. As of Spring 2014, over 280 students were involved in the program. First Year Student Fee funding made it possible to provide a higher level of support to students and mentors, profile numerous students involved in ASA programs, research tools and services that may further ASA learning goals, and cultivate a culture of service in our programs.

Colin Hodgkins, a Masters Candidate in the Creative Writing Program, served as a graduate assistant supporting Arizona Assurance for several years. With First Year Student Fee funding, it was possible to rehire Colin to continue to support Academic Success & Achievement’s mentoring programs. Colin’s professional writing and editing skills proved tremendously valuable, and his research experience helped with several department-wide projects that support our efforts to engage, retain, and educate students.',
'outcomes' => 'Colin provided significant support for Academic Success & Achievement’s objectives this year. His work contributed to ongoing changes the department is implementing to expand its offerings, create more opportunities for students to be engaged, and support student retention. Thousands of UA undergraduates are served by Academic Success & Achievement’s programs each semester, and an immeasurable number of current and prospective students are impacted by Colin’s work. Much of Colin’s research will help guide the department’s efforts for the long term. This year, Colin:

*Interviewed, filmed, and wrote stories about mentors and mentees for the Arizona Mentors website. Additional profiles of mentees and mentors are planned for the Spring 2014 semester.
*Edited marketing and recruitment materials for the Cubs to Wildcats program and publications for other Academic Success & Achievement initiatives.
*Assisted with content for a new website for the Arizona Mentors program.
*Supervised an undergraduate employee who managed social media presence for Arizona Assurance program and profile Scholars for website and materials.
*Contributed to and edited the Arizona Assurance program profile in High Tech, High Touch: Campus-Based Strategies for Student Success, a book showcasing ten outstanding programs recognized by the Association of Public and Land-Grant Universities. 
*Served as a consultant for year-long structural changes to the Arizona Mentors program. Colin has regularly met with the director of the program to develop a strategy for expanding the program.
*Conducted research into online learning platforms that could potentially be used by programs on campus. This project involved reviewing materials, contacting the companies’ liaisons, and consulting with known customers of several products. Colin compiled his findings and made recommendations for how online learning could be built into existing Academic Success & Achievement programs to engage learners through a variety of media.
*Researched dozens of cohort-based programs from universities around the country designed to transition first-year students to and through their sophomore year.
*Helped prepare a three-session development series for faculty and staff in the Arizona Mentors program for Spring 2014 involving campus partners and their expertise.
* Supported the planning and coordination of four service experiences for students in ASA programs in Spring 2014, yielding 93 hours of community service performed.',
'response' => 'One of Colin’s projects this year was to profile students involved with ASA programs. The passage below is an excerpt of a profile Colin wrote about Sarsawati Chhetri, a student involved with multiple ASA programs. 

“…One of the most significant resources came through the Arizona Mentors Program.  Upon signing up, Chhetri was paired with Dr. Amy Fatzinger, a professor in the American Indian Studies Program, who helped Chhetri decide on an academic path.  With Dr. Fatzinger’s guidance, Chhetri came to realize she wants to major in Education, and to focus on teaching English as a Second Language. “I’ve been through it. That’s my background.  So I can relate, and I want to teach people, to help them get through it and succeed,” she said. Chhetri’s ambitions don’t stop there.  Already certain that she will graduate from UA, she is interested in pursuing a Master’s in Nursing, in order to simultaneously contribute to the health care needs around the globe.

“I’ve seen so many people suffering in my country, and I know that once I get a degree, I can go back and help,” she said.  “I want to travel to countries like Nepal and India, to simultaneously help teach poor people, and to help them access health care.” Chhetri credits programs like Arizona Assurance, New Start, and Arizona Mentors—all three programs part of the UA Office of Academic Success and Achievement—for providing her opportunities, which have empowered her to spread them as well.”',
'created_at' => '2014-08-25 22:14:41.000000',
'project_id' => 793,
),
31 => 
array (
'netid' => 'johannej',
'scope' => 'Our freshman class brings together a diverse group of students from across the United States and throughout the world. Each student brings a different level of knowledge of existing policies, laws, and resources that can support his/her safety while attending the University of Arizona. Additionally, many freshmen experience new situations in which they are unsure of how to respond, which may be further compounded if they are unfamiliar with the campus and the Tucson community. 
UA SafeCats, housed within the Dean of Students Office, is designed to be a “one-stop” network that brings safety to the forefront of campus life. SafeCats used this grant to fund a multi-faceted safety campaign that addresses specific safety priorities for freshmen. The first priority focuses on getting safety resource information directly into the hands of the students through the bystander intervention campaign “Be a Friend. Do Something. ” This innovative campaign created through SafeCats uses dramatic, eye-catching images, on educational posters, door hangers, and a website to challenge students to step up and do the right thing when they realize their friend is at risk. 
Another primary safety tool that is funded through the First Year Student Fee is the safety card. This provides a direct safety tool for freshman to have throughout their time at the UA by providing each student with a wallet-sized card containing safety resources, telephone numbers, and websites. These safety initiatives will increase knowledge, provide resources, and build skills within our freshman population who are most vulnerable for risky situations.',
'outcomes' => 'SafeCats developed a set of learning outcomes in our First Year Student Fee proposal that we hope to see our freshmen achieve and that we also believe meet the goals and mission of the University of Arizona, the Student Affairs Division, and advance the goals of the First Year Student Fee. Following are the learning outcomes and the attached file provides an overview of the work we are doing to achieve them.
1. Develop an understanding of university resources. 
2. Develop the ability to take initiatives for fellow students of friends on and off campus.
3. Develop confidence, commitment, and dedication to the UA community.
4. Take an active role in the partnership of campus safety.',
'response' => 'The estimated number of freshman students impacted is 10,000, calculated based on the number of students who live in residence halls, who are enrolled in Centennial Hall classes, and the number of students typically reached at UA Clicks and New Student Orientation. 
•	7,000 safety business cards distributed to freshman during move-in weekend. These wallet-sized cards provide safety resources, telephone numbers, and websites.  Additional safety cards were distributed while tabling throughout the year.
•	933 “Be A Friend. Do Something.” posters & 1,900 “Be A Friend. Do Something.” door hangers were distributed in the residence halls during the fall semester.  An additional 924 posters were distributed to the residence halls during spring semester.  Using dramatic, eye-catching images, this campaign challenges students to step in and do the right thing when they realize their friend is at risk.
•	The number of visits to the Dean of Students SafeCats website and to Be a Friend has increased since the start of the school year.  Using Google Analytics to track traffic to the Dean of Students website, we have measured that the “Be A Friend” site averages 8.8 visitors per week and the SafeCats site averages 39.1 visitors per week (Sep. ‘13- July ‘14). This traffic to the website has shown that students are utilizing the URL found on the posters, door hangers and plasmas to seek out more information and resources. 
In the spring of 2014, a survey was conducted of students living in the residence halls to gather information about what impact that the “Be A Friend. Do Something.” campaign had on them. Following is an impact statement from a student:  “I think it\'s a great campaign and I know if I\'m seeing the posters, people who really need to be seeing the posters are seeing them too.”',
'created_at' => '2014-08-27 00:12:25.000000',
'project_id' => 790,
),
32 => 
array (
'netid' => 'sbasij',
'scope' => 'The First Year Student Fee funds were used to sponsor the 2012 New Student Convocation and Convocation Celebration.',
'outcomes' => 'Freshman Connections and Student Impact
The First Year Student Fee funds were used to promote, develop and grow the programs for Wildcat Welcome, specifically New Student Convocation.  All freshmen are invited to participate in the 10 days of programming.  Specific funds were targeted to: Wildcat Welcome Marketing and Promotion and New Student Convocation.  Wildcat Welcome is designed to give all new freshman a chance to connect with the UA community.  We actively promote the activities to students via New Student Orientation, email and social media platforms.

Wildcat Welcome Marketing and Promotion
Promotional items with the Wildcat Welcome logo and website were provided to all students at orientation and a second set of items were given away to those freshman living on campus during the 12-13 school year.  Our goal was to provide students with an item that they could use to get into the Wildcat spirit.  We created Wilbur and Wilma masks that contained all of the Wildcat Welcome events as well as posters distributed via the Residence Hall and through various social media platforms like Twitter and Facebook.   

New Student Convocation Celebration
New Student Convocation serves as the formal welcome from President Hart and UA Leadership to the incoming class of freshmen students to UA.  Hosting New Student Convocation at the McKale Center in 2012 again allowed an increase in attendance, from 5,500 to a little over 6,000 students, and created a more spirited environment.  Hosting our event at McKale is deliberate, as it is the only space on campus where we can accommodate the entire freshman class in one location at one time.  Students enjoyed the format and the opportunity to meet President Hart.  Students stayed through the event, and on to the celebration that followed.  The celebration event was very well attended, with students taking advantage of the activities, including a photo booth and the food that was provided.  Students were also able to pick up their free Bear Down red class t-shirts, an item that identifies them as Wildcats and officially welcomes them to the UA family.',
'response' => 'Wildcat Welcome activities were very well attended in the fall of 2012.  We can attribute much of that to the experience of New Student Convocation and the readiness of the marketing materials used to promote the events.  The biggest success we had was in the increase of attendance at New Student Convocation.  In 2010 2,000 students attended the New Student Convocation event and in 2011 attendance grew to over 5,500 and in 2012 we had a little over 6,000 in attendance.  McKale staff facilitating the event estimated attendance based on seating areas.',
'created_at' => '2014-08-27 22:30:35.000000',
'project_id' => 786,
),
33 => 
array (
'netid' => 'hiatt',
'scope' => 'The UAPD Neighborhood Watch Project 2013-2014 was partially funded by the University of Arizona First Year Student Fee fund (FFF). Residence Life received matching funds to support this project after a submitting a successful competitive application to the FFF. 

This project added additional UAPD security officers for late night duty in and around student housing facilities on campus. Officers were on duty throughout the fall semester on Thursday, Friday, and Saturday nights from 10:00pm - 2:00am and on Thursday and Friday nights from 10:00pm - 2:00am during the spring semester. The removal of the Saturday watch during the spring semester was due to the unbudgeted UAPD salary increase which disproportionally drew down funds during the fall semester.

Officers patrolled the Park and Highland District residence hall neighborhoods on foot, by golf cart, and by car. This added physical presence helped bring an increased sense of safety and security to the campus.',
'outcomes' => 'The goal of the proposal was to increase the safety and security of our campus residents. Residence Life\'s analysis of the Neighborhood Watch data revealed that the Watch fulfilled its goal. 

The Neighborhood Watch patrol reported over 653 defined activities from September 6th 2013 – May 17th 2014 of which:

- 71% of all activities pertained to Area, Building and Welfare checks; 
- 14% were field interview/interaction related; 
- 2.6% were hall checks;
- 1.8% were traffic related; and 
- 1.6% were minor in possession related.

The remaining activities were a combination of many different types of activities. The number and type of activities indicate that officers made their presence known and provided a sense of increased safety and security on campus.',
'response' => 'During fall 2013 and spring 2014 over 4,500 students living in the Park and Highland area halls were directly impacted by the CNSP. The additional 1,300+ students living on campus have likely benefitted indirectly from the increased sense of safety and either being in these areas during patrol hours or learning/hearing about the patrol from classmates.',
'created_at' => '2014-08-28 16:16:36.000000',
'project_id' => 791,
),
34 => 
array (
'netid' => 'kvd',
'scope' => 'Scholarship Universe (SU) is the University of Arizona’s (UA) homegrown web-based scholarship matching system. UA Students log in to SU, are asked a series of questions to create their eligibility profile, and then are matched to scholarships for which they’re eligible. Our team of student employees continually search for and verify scholarship opportunities, as well as develop and code new site features and functionality.

Phase I of Scholarship Universe focused on initial site development and adding non-UA scholarships to the database; so far, over 2,400 such scholarships, with an estimated 10,000 potential awards, have been added. SU made the process of finding scholarships faster and more efficient for UA Students.  In 2012, Scholarship Universe was selected out of 354 global nominees to receive a Campus Technology Innovators Award.

Phase II of Scholarship Universe focused on the development of the Scholarship Universe Departmental Application (SUDA).  SUDA allows colleges and departments at the UA to offer their scholarships through SU. As a one-stop-shop for scholarships, SU enhances the visibility of UA scholarships and increases applicants.  SUDA also improves the transparency of the scholarship process, making it easier for students to understand which departmental scholarships they’re actually applying for.

Phase III (new) of Scholarship Universe will see the site undergo a comprehensive redesign to make it more efficient, mobile friendly, and align more closely with the UA brand.  Furthermore, the redesign will enhance our ability to assess SU’s effectiveness and help improve students’ experiences on the site.',
'outcomes' => 'Our funding proposal outlined two primary measures of success: 1) meeting the developmental timeline for the implementation of Phase II and 2) increasing student use of Scholarship Universe and bringing in more non-UA scholarship dollars.  With Phase II mostly complete, work has now begun on Phase III, which provides us the opportunity to share some new goals and anticipated outcomes with the SSF board.

1) Phase II Implementation
We’re pleased to report that satisfactory and timely progress has been made on Phase II in accordance with our developmental timeline and that we’ve successfully completed the goals outlined in our scope of work document for Phase II (see appendix item 1). Focus has now shifted to improving and optimizing SUDA’s functionality and bringing on more college and departmental scholarships.  

In the 2013-14 academic year, 600 UA Scholarships were available through SU; including, for the first time, scholarships from the College of Agriculture and Life Sciences (165), the College of Education (70), and the Office of Scholarships and Financial Aid (66).  2,784 students applied to those UA Scholarships and 1,056 awards have been made so far to those applicants worth $1,613,867.

For the 14-15 school year, we expect additional colleges to add their scholarships to SU, including: Social and Behavioral Sciences, Science, Fine Arts, Public Health, Law, and Humanities.  We anticipate that the addition of these colleges to push the number of UA Scholarships available in the system to over 1,000.  This will bring the total of colleges/divisions participating with Scholarship Universe in some capacity to 16.

2) Increases in Both Student Use and Scholarship Awards
Over the past few months, we’ve analyzed the 3,794 3rd party scholarship checks students received for the 2013-14 school year to determine the number of scholarships SU has helped bring in.  Our analysis revealed that SU users won nearly $2.2 million from 3rd party scholarships available in SU.  This represents an 11x return on investment from the $208,500 that SSF invested in SU during the 2013 fiscal year.


3) Redesign of Scholarship Universe landing page and student application (Phase III)
The Scholarship Universe website has served its purpose well over the past 4 years, but the original design imposes significant technical limitations, limits usability, and doesn’t reflect the UA Brand.  As a result, we’ve begun the process of a comprehensive site redesign which will not only change the look of SU but the feel as well.  The goals for the redesign are to: improve users’ experience on the site, make the site mobile friendly across many platforms, improve the tracking and analytic capabilities of the site, and to more closely align with the UA Brand.',
'response' => 'Our usage statistics remain strong: according to our Google Analytics reporting, from Jan. 1st to Jul. 31st of this year, Scholarship Universe has been visited 61,078 times by 27,084 unique users – this represents an almost 50% increase over the same period last year (see appendix item 2).

In February of this year, we conducted a survey of our student users to gather feedback on SU to help with the upcoming redesign process.  One of the most requested features to add to Scholarship Universe was some sort of notification system.  We’re pleased to report that this fall we’ll be launching our Automated Email System (AES) to address this desire.  This system will generate automated emails to let students know when new scholarships they’re eligible for become available, when scholarship deadlines are approaching, and if the student has any unanswered profile questions which could make them eligible for more scholarships.

Finally, we’d like to share a few quotes from students from the aforementioned survey:

“I really appreciate the concise website and opportunities SU provides. Thank You! :)” – Paul

“[SU] makes finding and applying to scholarships much easier.” – Fernanda

“I think the Scholarship Universe is a good and functional website that is very helpful and easy access for students wanting to apply for scholarships.” – Josie

“I really like how much easier it makes life when I’m looking for scholarships. I want to thank the team for taking the time to do all this! :)” - Callie

“I like the fact that you don’t have to spend hours looking for scholarships.  The website locates them for you.” – Valeria 

“The website offers me scholarship matches that best suit me and my needs and that is important and helpful to me.” – Almonese',
'created_at' => '2014-08-28 21:13:59.000000',
'project_id' => 792,
),
35 => 
array (
'netid' => 'danthaix',
'scope' => 'The Division of Student Affairs is committed to building a campus community in which students, faculty, and staff at all levels feel valued and have the opportunities to succeed. Connecting students to the resources available to them from the beginning of their academic career is an essential component to success, and the University’s retention efforts. The Finding Community Welcome (FCW) aimed to both increase the visibility of the programs and services provided by the four cultural centers, the Disability Resource Center (DRC), LGBTQ Affairs, and the Women’s Resource Center (WRC); and to help incoming students “find community” the very first week of school. 

This year the planning team reevaluated the program to meet the original purpose of the event, which was to welcome the traditionally marginalize communities on campus and recognize students’ intersecting and multiple identities. To help bring the original purpose back to the the forefront of the event, the committee restructured the programming and marketing efforts of the event to focus on first year traditionally marginalized populations while making the event open to any student that wanted to participate.

The FCW’s main components included a presentation of the multiple support centers, a welcome from Student Affairs Administration and ASUA student leadership, student cultural performances, and a multicultural club fair.',
'outcomes' => 'The 2013 FCW successfully occurred on Tuesday, September 3, 2013 in the Student Union Memorial Center’s Grand Ballroom. The physical setup of the event changed and was divided into two main areas. The main program occurred in the South Ballroom and the multicultural club fair with the food began after the program in the North Ballroom. Students were checked in using a CatCard swipe system, which was new this year and reflected 278 attendees. According to the FCW evaluation, 53% were first year students. Due to some issues with the swipe system and because many participants entered through a non-monitored door in the North Ballroom, the committee estimated the full room attendance closer to the 450 seats that were set up. 

Below are the 2013 goals stated in our Finding Community Welcome proposal and how they were successfully addressed.

1) To allow students to learn about AASA, APASA, CHSA, DRC, LGBTQ Affairs, NASA, & WRC.
a. According to the FCW evaluation, 75% of students agreed that they learned about at least one organization or resource that they plan to use during the school year.
b. The ASUA Diversity Directors spoke to highlight the commitment ASUA has to student diversity and social justice initiatives on campus. 
c. With the help of the Student Affairs Marketing team, a sustainable promotional video was created that helped define, promote, and highlight each of the centers. 

2) To allow students to have an opportunity to learn about diverse clubs and organizations on campus.
a. This year we had 38 different multicultural and social justice clubs at the fair, a 250% increase from the 2012 FCW.
b. According to our FCW evaluation, 81 % agreed that this event increased their understanding of diverse clubs and opportunities to get involved on campus. 

3) To allow students to meet other new students in order to expand their peer networks.
a. The FCW provided a Human Bingo Icebreaker that allowed students to interact with other students that they did not know. In order for students to acquire a raffle ticket for the raffle items, students had complete the bingo form, ensuring that they met multiple students and expanded their peer networks at this event.
b. According to the FCW evaluation, 56% agreed that this environment helped them make connections with other students.

4) To allow students to experience cultural student performances
a. The FCW provided cultural student experiences from the NASA, AASA, APASA, and CHSA centers. From a traditional Native American blessing to open the event to a tutorial on stepping from the greek organizations of AASA, students were engaged throughout the event.

In conclusion, the main purpose in hosting the FCW was to connect students to the resources here to support underrepresented students on campus. We feel the FCW showcases the diversity at UA from the very beginning of the year, and helps students know their support systems during their tenure as Wildcats.',
'response' => 'The 2013 FCW was a great success in the engagement and retention of new students through multiple apects of intentional programming for marginalized populations and effectively getting information to students through a more successful and sustainable mediums. To ensure we hit the target population for this event, we marketed by utilizing the listservs from the cultural centers, DRC, LGBTQ Affairs, and WRC, as well as directly emailing all students who indicated they are students of color upon application. In the past, we had emailed all first year students about the FCW, but we really wanted to make sure we honored the purpose of the event. The event was open to all UA students through specific venues, which included advertisement in the Daily Wildcat, a mention at new student Convocation, on the Class of 2017 Facebook Page, and other social media outlets. The sustainable FCW video continues to live on the centers’ websites, allowing for positive messages to participate in these communities throughout the year.

As in the past, the event was evaluated with Campus Labs. According to the evaluation, the student attendance was very diverse in gender, sexual orientation, and race. New this year was that we were able to attract a greater number of transfer students. The overall student response was very positive. Below are some of the evalulation comments made from the students that aligned with assessing the successful outcome of the program.

• “I loved seeing all the different ethnicities of the speakers and attendees, because it really highlights the diversity at UA.”
• “All the clubs helped me discover new things.”
• “Dividing the room and having seating space right up to the stage was nice because you could see and hear everything.”
• “My favorite part was seeing the performances, because I like seeing the diverse cultures on stage.”',
'created_at' => '2014-08-29 18:02:04.000000',
'project_id' => 785,
),
36 => 
array (
'netid' => 'kriester',
'scope' => 'The goal of the Goal of the Dean of Students Parent & Family Programs Summer Send Off & Communications plan is to help develop a sense of community and involvement for our newest Wildcats and their parents.  We know that many parents are very involved in the life of their college student here at UA.  So, part of the communication includes the mailing of the Parent & Family Magazine once a semester to the parent of each Freshman.  We will also utilize this funding to help pay part of the expenses for our largest freshman/parent event: the Greater Phoenix Area Summer Send Off.  Below you will find a brief summary of both projects.

Greater Phoenix Summer Send Off – The Greater Phoenix Send Off is the largest Send Off event held each summer with all incoming freshmen from the greater Phoenix area invited to attend.  The Greater Phoenix Summer Send Off is free of cost to freshmen and their families.   A group of parent volunteers works with the Parents & Family Association Director to raise money in the form of sponsorships for this event each year.  

Parent & Family Magazine - Parents & Family Programs publishes and distributes one magazine each semester.  The Parents & Family Magazine provides helpful information for parents, stories about students, faculty and staff members and campus resources.  The Magazine provides a great glimpse of what is happening on campus each semester.',
'outcomes' => 'The Greater Phoenix Area Summer Send Off had a record year in attendance.  There were a total of 1231 attendees which was an increase of just over 100 guests from 2012.  In addition, this year a break out session was added for students and parents so that they could address any last minute questions prior to coming to campus.  There were also 48 volunteers most of whom were UA Alumni and current parents of UA students.  UA Band Members, Wilbur, Wilma and the Cheerleading Squad were also there to help welcome the new students and their families.  The attendance at this event has grown from 434 in 2009 to 1231 in just six year.  The Summer Send Off Committee of parent volunteers and UA Administrators are very thankful for the support of the First Year Student Fee which helped offset our $14,000 catering bill.

The fall edition of the UA Parent & Family Magazine was mailed in early December to freshman parents and donors to Parent & Family Programs.  
The spring edition of the UA Parent & Family Magazine was mailed to parents of current freshman, donors and parents of fall 2014 freshman in April of 2014.  The total circulation of the spring Magazine is 20,000.',
'response' => 'Both students and parents reported enjoying the Greater Phoenix Area Summer Send Off.   They had an opportunity to meet other students from Phoenix, to see some of the services offered and to get questions answered both by the UA staff in attendance and by each other prior to coming to campus in August.  They also had a tremendous feeling of being welcomed to the University of Arizona.  An added benefit is that the UA BookStore distributed IPads to student scholarship recipients.   This enabled students to have their IPad prior to arriving on campus.  

Seventy percent of parents said that the Summer Send Off in general helped them and their students feel a part of the UA community.  In addition, 57% learned about the Parent & Family Association and 63% felt welcomed to campus.
Eighty-five percent of parents indicated that they were satisfied or very satisfied with the content of the Parent & Family Magazine.  73% of parents shared the majority or some of the information in the Magazine with their student and 27% used the resources for their personal knowledge.',
'created_at' => '2014-08-29 23:14:19.000000',
'project_id' => 787,
),
37 => 
array (
'netid' => 'danthaix',
'scope' => 'The Division of Student Affairs is committed to building a campus community in which students, faculty, and staff at all levels feel valued and have the opportunities to succeed. Connecting students to the resources available to them from the beginning of their academic career is an essential component to success, and the University’s retention efforts. The Finding Community Welcome (FCW) aimed to both increase the visibility of the programs and services provided by the four cultural centers, the Disability Resource Center (DRC), LGBTQ Affairs, and the Women’s Resource Center (WRC); and to help incoming students “find community” the very first week of school. 

The planning team continued to meet the original purpose of the event, which is to welcome the traditionally marginalize communities on campus and recognize students’ intersecting and multiple identities. The committee coordinated more concentrated efforts in the programming and marketing of the event to focus on first year traditionally marginalized populations while making the event open to any student that wanted to participate.

The FCW’s main components included a presentation of the multiple support centers, a welcome from Student Affairs Administration and ASUA student leadership, student cultural performances, and a multicultural club fair.',
'outcomes' => 'The 2014 FCW successfully occurred on Thursday, September 4, 2014 in the Student Union Memorial Center’s Grand Ballroom. The physical setup of the event was divided into two main areas. The main program occurred in the South Ballroom and the multicultural club fair with the food began after the program in the North Ballroom. Students were checked in using a CatCard swipe system and reflected 266 attendees.

Below are the 2014 goals stated in our Finding Community Welcome proposal and how they were successfully addressed.

1) To allow students to learn about AASA, APASA, CHSA, DRC, LGBTQ Affairs, NASA, & WRC.
a. According to the FCW evaluation, 85% of students agreed that they learned about at least one organization or resource that they plan to use during the school year.
b. The ASUA President spoke to highlight the commitment ASUA has to student diversity and social justice initiatives on campus. 
c. With the help of the Student Affairs Marketing team, a sustainable promotional video was created that helped define, promote, and highlight each of the centers. 

2) To allow students to have an opportunity to learn about diverse clubs and organizations on campus.
a. This year we had 47 different multicultural and social justice clubs at the fair, a 23% increase from the 2013 FCW.
b. According to our FCW evaluation, 83 % agreed that this event increased their understanding of diverse clubs and opportunities to get involved on campus. 

3) To allow students to meet other new students in order to expand their peer networks.
a. The FCW provided a Human Bingo Icebreaker that allowed students to interact with other students that they did not know. In order for students to acquire a raffle ticket for the raffle items, students had complete the bingo form, ensuring that they met multiple students and expanded their peer networks at this event.
b. According to the FCW evaluation, 82% agreed that this environment helped them make connections with other students.

4) To allow students to experience cultural student performances
a. The FCW provided cultural student experiences from the NASA, AASA, APASA, and CHSA centers. From a traditional Native American blessing to open the event to a tutorial on stepping from the Greek organizations of AASA, students were engaged throughout the event.

In conclusion, the main purpose in hosting the FCW was to connect students to the resources here to support underrepresented students on campus. We feel the FCW showcases the diversity at UA from the very beginning of the year, and helps students know their support systems during their tenure as Wildcats.',
'response' => 'The 2014 FCW was a great success in the engagement and retention of new students through multiple aspects of intentional programming for marginalized populations and effectively getting information to students through a more successful and sustainable mediums. To ensure we hit the target population for this event, we marketed by utilizing the listserves from the cultural centers, DRC, LGBTQ Affairs, and WRC, as well as directly emailing all students who indicated they are students of color upon application. The event was open to all UA students through specific venues, which included advertisement in the Daily Wildcat, a mention at new student Convocation, and other social media outlets. The sustainable FCW video continues to live on the centers’ websites, allowing for positive messages to participate in these communities throughout the year.

As in the past, the event was evaluated with Campus Labs. The overall student response was very positive. Below are some of the evaluation comments made from the students that aligned with assessing the successful outcome of the program.
•“I liked meeting diverse people b/c I hadn\'t realized this many ethnicities were on campus”
•“I enjoyed learning about the diversity of our campus because it\'s important”
•“What I appreciated the most was the amount of diverse cultures”
•“I appreciated learning & seeing the different cultural centers”',
'created_at' => '2014-12-22 21:45:03.000000',
'project_id' => 797,
),
38 => 
array (
'netid' => 'lscheu',
'scope' => 'The Campus Night Security Project (CNSP) supports two UAPD security officers for night duty in and around student housing facilities on campus. The two officers are on duty Thursday, Friday, and Saturday nights from 10 pm - 2 am, the days and hours most police involved incidents occur, during the fall 2014 and spring 2015 semesters. The officers patrol the Park (West) and Highland (East) campus neighborhoods on foot, by golf-cart, and car. This physical presence of these officers helps bring an increased sense of safety and security to the campus.',
'outcomes' => 'The goal of the proposal is to increase the safety and security of our campus residents. While only the mid-point of the project, Residence Life can report that the CNSP has thus far fulfilled its goal. 

The CNSP patrol reported over 310 defined activities from August 22 – December 1, 2014 of which:

•	62% of all activities pertained to Area, Building and Welfare checks; 
•	18% were field interview/interaction related; 
•	3.5% were minor in possession related;
•	2.5% were related to medical assistance;
•	2.5% were traffic related; and
•	2.5% were related to public assistance (providing directions, giving rides, etc.).

The remaining activities were a combination of many different types of activities. The number and variety of activities indicate that officers are making their presence known and are providing a sense of increased safety and security on campus.',
'response' => 'During fall 2014 over 5,500 students living in the Park and Highland area halls were directly impacted by the CNSP. The additional 1,300+ students living on campus have likely benefitted indirectly from either being in these areas during patrol hours or learning/hearing about the patrol from classmates.',
'created_at' => '2015-01-05 16:52:35.000000',
'project_id' => 788,
),
39 => 
array (
'netid' => 'claudia',
'scope' => 'Bear Down Camp is a four-day experience for incoming freshmen of the University of Arizona. BDC is held annually in Prescott, Arizona for a continually growing number of students. The camp staff consists of an administrative team, counselors, and a crew team, all of which are volunteer students whom attending camp trainings throughout the academic year and at the end of the summer. While at camp, freshmen are divided into smaller groups lead by two counselors and participate in various activities that meet the camp’s mission and learning objectives. These include academic and on-campus resources, involvement opportunities, educational health resources, and providing general information that every student should know and utilize in a dynamic university setting.  Ultimately, Bear Down Camp’s primary goal is to engage incoming students in camp activities to prepare them for their first year of college and increase the University of Arizona’s overall retention rate. Additionally, we hope to create a peer network for students of all levels to rely on throughout their collegiate careers.',
'outcomes' => 'Bear Down Camp made a goal of taking 250 freshman students to camp and registered 223 students in the three-month registration period.  Ultimately, we were able to bring 195 students to Bear Down Camp. This number has decreased by 35 students from the 230 students that enrolled in the 2013 camp. The program had a slight decrease in numbers due to participant’s change of plans or other options. We feel that with this year’s staff and improved marketing strategies, we will be able to not only reach last year’s goals, but also surmount it by a significant amount. In addition, we hope to continue to collaborate with our campus partners in Enrollment Management and Orientation to reach out local high schools, sending information to out-of-state orientations, and attending college fairs to outreach to potential campers.',
'response' => 'The camp learning objectives are completed through the implementation of a series of workshops. Each workshop has a main focus and counselors are responsible for delivering the information in a manner that gives a holistic view of the topic while also not overwhelming the campers. This year, the number of workshops was reduced from 11 to 7 to dedicate more time to team building activities and new additions to the program, including an Involvement Fair. A post camp survey of around 75 students exhibited that 96% of campers felt that they had a better understanding of the University’s resources such as Campus Health, Think Tank, and other essential areas needed to succeed.  Additionally, 91% of the respondents felt that they had a sense of belonging after camp which is something worth noting. One of the highest agreed with question, which was how likely the students were going to get involved with the University in some way after camp, 95% of students agreed with this. Whether it is Greek life, student government, or one of the many clubs on campus, students are more willing to apply themselves after attending camp. While these numbers will be helpful in improving this year’s camp, the survey was taken on a voluntary basis. Therefore, in the future, we hope to initiate a more reliable system of garnering feedback.',
'created_at' => '2015-01-05 18:14:59.000000',
'project_id' => 801,
),
40 => 
array (
'netid' => 'sbasij',
'scope' => 'The First Year Student Fee was used to provide a one of a kind Bear Down T-shirt to all freshmen who attended the mandatory 2014 UA Clicks program. These t-shirts served as an incentive for students to attend this very important program. The First Year Student Fee was also used to help fund the 2014 New Student Convocation. This year we were unable to host the New Student Convocation in McKale Center due to renovations so we needed to run the program on the UA Mall. The funds from were used to help fund this outdoor program that welcomes new freshman students to campus.',
'outcomes' => 'In 2013 we have attendance of 6000 students. This year we were able to increase attendance by over a thousand students. We had 7200 students in attendance at New Student Convocation.',
'response' => 'We saw an increase in attendance at the New Student Convocation of over 1000 students. In 2013 6000 students attended but in 2014 7200 students attended New Student Convocation. Since 2011 we have been able to increase the number of students who attend the New Student Convocation with the support of the First Year Student Fee. 
This year we had more students who attended UA Clicks than we had attend in prior years with the support of the First Year Student Fee.
4) Finances (Please upload the completed excel Budget Template that was e-mailed to you. Be sure to complete the section on the spreadsheet providing detail of other funding sources secured to maintain this project, if any beyond the funding of the First Year Student Fee.):',
'created_at' => '2015-01-07 00:54:48.000000',
'project_id' => 800,
),
41 => 
array (
'netid' => 'nathantack',
'scope' => 'The Residence Hall Association, Hall Involvement Team was created as a leadership training, retention/transition initiative for students moving into the residence halls. HI Team is dedicated to preparing incoming freshmen for their first year as a student living on campus at the University of Arizona. Hi Team is a week long experience for incoming freshmen of the University of Arizona. It is held annually on campus and has grown over the years. During Hi Team, freshmen are divided into smaller groups to participate in various activities that meet the mission and learning objectives. These include academic and on-campus resources, involvement opportunities, on campus living information, and providing general information that every student should. Hi Team is then rounded out with the RHA Block Party, which is the first major social event on the year for all on campus residents to come enjoy food and fun on the mall and get to know their fellow residents. Ultimately, Hi Team and Block Party’s primary goal is to engage incoming students to prepare them for their first year of college and increase the University‘s overall retention rate as well as engage students living in the residence halls. Overall, we strive to offer a top notch leadership experience for new students coupled with a smooth move in experience for all students and a great kick off event for the year. Combined, these programs have an amazing impact on students here at the UofA help us build communities and build leaders.',
'outcomes' => 'Our first goal was to have 200 students participate in HI Team and have 85% of the freshman class in attendance at Block Party. We managed to hit both of these marks. We had 217 students participate Hi Team and had our largest, most consistent crowd at block party in years. We worked with Residence Life Marketing and Student Affairs Marketing to get the word out for Hi Team and Block Party, as well as worked with our Community Directors and Resident Assistants to bring their wings out to Block Party. Our second goal was to include more entities and departments within Student Affairs to actively contribute to Block Party. We struggled initially to obtain more partners, but in the end we had the Bookstore and Campus recreation jump on for support as well as KAMP radio and the Student Union.  We decided to attach a award bid that we wrote that speaks to the outcomes of the program.',
'response' => 'This project had a large impact on students across our residence halls. We had 217 students participate in Hi Team directly who served roughly 6000 freshman who moved into the halls and assisted them. Block Party had about 5000 students in attendance over the duration of the program with the largest amount in program history staying through out the entire program. Some quotes form participants were:
“I did not expect to learn as much as I did! I thought we would strictly be moving people in the whole time, but the opportunity to learn about so-jo and sustainability was so informative. I also didn\'t expect to have the opportunity to connect with so many RAs and CDs around campus.”
“I really enjoyed joining hall council; it brought me closer to the RAs and students that I probably would\'ve never met if I didn\'t join HI team. It also made me familiarize myself with campus since we had to go to different places to help people move. Also, it made me want to get involved with Hall Council to further improve my leadership skills. Overall, it was a great experience.”
“I really liked this program overall, I met a lot of people, had the chance to provide service, and made some really great friendships within Hi-team. I think the program has a great start and will get better after each run. I loved the RHA team and it was a really great introduction the UA environment.”
“HI team is an extremely organized, helpful and a friendly team. For me, it was a very good introduction to the U of A, in fact, HI made me feel like I made the right decision choosing U of A! I\'ve met some amazing personalities and had a really fun time.”',
'created_at' => '2015-01-29 21:52:15.000000',
'project_id' => 789,
),
42 => 
array (
'netid' => 'lscheu',
'scope' => 'The Campus Night Security Project (CNSP) supports two UAPD security officers for night duty in and around student housing facilities on campus. The two officers are on duty Thursday, Friday, and Saturday nights from 10 pm - 2 am, the days and hours most police involved incidents occur. The officers patrol the Park (West) and Highland (East) campus neighborhoods on foot, by golf-cart, and car. This physical presence of these officers helps bring an increased sense of safety and security to the campus.',
'outcomes' => 'In the 2014-2015 academic year, the Neighborhood Watch was on patrol 49 nights. Most recorded Neighborhood Watch activities are “Area Checks” (377) and “Field Contacts,” (65) meaning they are mostly checking general areas – including recreation areas, parking lots, streets, and Residence Halls – and interacting with individuals, respectively. The Neighborhood Watch recorded a total of 600 activities in the past academic year. 
The Neighborhood Watch program has been successful in reaching its goals. The following are a small sample of activities recorded in the Neighborhood watch logs that support goal attainment:
“Spoke with student who is interested in Law Enforcement career.”
“Spoke to group of students at volleyball courts; all clear.”
“Gave a student a ride to Pima Hall.”
“Gave directions to Highland and spoke to students about safety.”
“Flagged down by female student with rash on body, was not transported.”
“Spoke with students and gave advice on how to be safe.”
“Educated student about proper bicycle equipment.
“Female student walking with a limp, said she was fine.”
“Contact with 3 RAs doing rounds.”',
'response' => 'During the 2015-2015 academic year well over 4,500 students living in the Park and Highland area halls were directly impacted by the Neighborhood Watch. The additional 1,300+ students living on campus have likely benefitted indirectly from either being in these areas during patrol hours or learning/hearing about the patrol from classmates. Additionally, other students and commnity members benefitted from the increased safety and security on campus',
'created_at' => '2015-08-31 15:54:30.000000',
'project_id' => 788,
),
43 => 
array (
'netid' => 'mwshulby',
'scope' => 'Hall Involvement Team (HI Team) is a group of Arizona Wildcats that get to move into their residence halls early in order to be the welcoming team for the rest of campus.  The week-long HI Team program includes three days of leadership training with U of A Residence Hall Association, and four days of campus move-in. During HI Team student leaders work with families to move students and their belongings into the residence halls on campus wide move in days. Following HI Team, the RHA Block Party event takes place hosting many attractions to bring all residents introducing them to one of the first UA programs.',
'outcomes' => 'Going into the program with additional funding, RHA was hoping to better advertise HI Team, and provide an item to the volunteer students that allows them to be clearly identified. Because of the funding, RHA was able to provide T-shirts to the all 200 HI Team volunteers. In the breakdown of costs, you can see how the majority of funds went to providing the students T-shirts to be identified.',
'response' => 'The HI Team students, were very happy to have served on the RHA HI Team. They were able to develop their leadership skills through trainings and workshops, meet knew people who arrive to campus early, and help other students move in to the halls across campus. Though it was hot during the move-in time, they enjoyed moving in students and talking to them. There was satisfaction knowing that the HI Team program helps relieve the stress of first-year students moving into the halls.',
'created_at' => '2015-08-31 18:27:12.000000',
'project_id' => 789,
),
44 => 
array (
'netid' => 'zacharykane',
'scope' => 'Bear Down Camp is a four-day experience for incoming freshmen of the University of Arizona. BDC is held 
annually in Prescott, Arizona for a continually growing number of students. The camp staff consists of an 
administrative team, counselors, and a crew team, all of which are volunteer students whom attending camp 
trainings throughout the academic year and at the end of the summer. While at camp, freshmen are divided into 
smaller groups lead by two counselors and participate in various activities that meet the camp’s mission and 
learning objectives. These include academic and on-campus resources, involvement opportunities, educational 
health resources, and providing general information that every student should know and utilize in a dynamic 
university setting.  Ultimately, Bear Down Camp’s primary goal is to engage incoming students in camp activities 
to prepare them for their first year of college and increase the University of Arizona’s overall retention rate. 
Additionally, we hope to create a peer network for students of all levels to rely on throughout their collegiate 
careers.',
'outcomes' => 'Bear Down Camp made a goal of taking 250 freshman students to camp and registered 223 students in the 
three-month registration period.  Ultimately, we were able to bring 195 students to Bear Down Camp. This 
number has decreased by 35 students from the 230 students that enrolled in the 2013 camp. The program had 
a slight decrease in numbers due to participant’s change of plans or other options. We feel that with this year’s 
staff and improved marketing strategies, we will be able to not only reach last year’s goals, but also surmount it 
by a significant amount. In addition, we hope to continue to collaborate with our campus partners in Enrollment 
Management and Orientation to reach out local high schools, sending information to out-of-state orientations, 
and attending college fairs to outreach to potential campers.',
'response' => 'The camp learning objectives are completed through the implementation of a series of workshops. Each
workshop has a main focus and counselors are responsible for delivering the information in a manner that gives 
a holistic view of the topic while also not overwhelming the campers. This year, the number of workshops was 
reduced from 11 to 7 to dedicate more time to team building activities and new additions to the program, 
including an Involvement Fair. A post camp survey of around 75 students exhibited that 96% of campers felt that 
they had a better understanding of the University’s resources such as Campus Health, Think Tank, and other 
essential areas needed to succeed.  Additionally, 91% of the respondents felt that they had a sense of 
belonging after camp which is something worth noting. One of the highest agreed with question, which was how 
likely the students were going to get involved with the University in some way after camp, 95% of students 
agreed with this. Whether it is Greek life, student government, or one of the many clubs on campus, students 
are more willing to apply themselves after attending camp. While these numbers will be helpful in improving this 
year’s camp, the survey was taken on a voluntary basis. Therefore, in the future, we hope to initiate a more 
reliable system of garnering feedback. 
By doing this, we hope to continue to expand the program and partner with other on-campus entities to further 
increase student retention while continuing to explore other programs in place at institutions nationwide.',
'created_at' => '2015-08-31 23:08:00.000000',
'project_id' => 801,
),
45 => 
array (
'netid' => 'venezia',
'scope' => 'The First Year Student Fee was used to provide a one of a kind Bear Down T-shirt to all freshmen who attended the mandatory 2014 UA Clicks program. These t-shirts served as an incentive for students to attend this very important program. The First Year Student Fee was also used to help fund the 2014 New Student Convocation. This year we were unable to host the New Student Convocation in McKale Center due to renovations so we needed to run the program on the UA Mall. The funds from were used to help fund this outdoor program that welcomes new freshman students to campus.',
'outcomes' => 'In 2013 we have attendance of 6000 students. This year we were able to increase attendance by over a thousand students. We had 7200 students in attendance at New Student Convocation.',
'response' => 'We saw an increase in attendance at the New Student Convocation of over 1000 students. In 2013 6000 students attended but in 2014 7200 students attended New Student Convocation. Since 2011 we have been able to increase the number of students who attend the New Student Convocation with the support of the First Year Student Fee. 
This year we had more students who attended UA Clicks than we had attend in prior years with the support of the First Year Student Fee.',
'created_at' => '2015-09-01 05:02:26.000000',
'project_id' => 800,
),
46 => 
array (
'netid' => 'kriester',
'scope' => 'Parent and Family Programs publishes and distributes one magazine each semester.  The Parents & Family Magazine provides helpful information and resources for parents and includes articles about students, faculty, and staff members.  The magazine provides a great glimpse of what is happening on campus each semester.  The funding for the printing of the magazine is covered by advertisement sales for the magazine which is handled by Student Media.  Parent and Family Programs received funding support for mailing the magazines to parents of our freshmen.   Each semester the magazine is mailed to approximately 7500 freshmen parents.  The spring edition will be mailed to the incoming freshmen and their parents.  The spring edition contains a special “New Student” section to help parents make sure that their student is ready for Orientation and for their first semester in college.  Parent and Family Programs will continue to cover the cost of mailing the magazine to our parent and family donors.',
'outcomes' => 'The fall edition of the Parents & Family Magazine was mailed on November 20, 2015 to 11,975 families.  First Year Student Fee funding assisted in the mailing cost to 7,429 families of UA freshmen.  UA Parent and Family Programs supported the remaining cost to mail to the other 4,546 families.  This edition was 33 pages long and contained eight articles/features including features on Financial Literacy, Applied Sustainability Education and a Donor Thank You article.  Please see the attached pdf for a sample of the table of contents for the Magazine. 

Parent and Family Programs has several learning outcomes for the Parent & Family Magazine:
-	Provide timely and important information to parents and families.  
-	Provide continual exposure to Parent & Family Programs and the UA Parent & Family Association as a resource for families.
-	Provide stewardship for Parents & Family Association Donors and Corporate Sponsors.

All three of these outcomes were met with the fall 2015 Edition of the Magazine.  Specific assessment will be conducted in spring 2015 as part of the bi-annual Parent Satisfaction Survey and results will be reported in the final First Year Student Fee Report for 2016.',
'response' => 'The fall 2015 Parent & Family Magazine was mailed to 11,975 families which included 7,429 families of freshman students.  The Parent Satisfaction Survey will be conducted in spring 2016 and will ask for specific feedback on the Parent & Family Magazine.  This feedback will be summarized and reported as part of the final First Year Student Fee Report.',
'created_at' => '2015-12-16 01:44:46.000000',
'project_id' => 810,
),
47 => 
array (
'netid' => 'sevapriya',
'scope' => 'The University of Arizona freshman class brings together a diverse group of students with varied backgrounds. Each student possesses a different level of knowledge of existing policies, laws, and resources that can support personal safety while attending the University of Arizona.  Our freshmen encounter new and unfamiliar situations and environments which require that they make safety assessments and decisions.  In order to best support freshman, UA SafeCats aims to equip them with information and support.

SafeCats, housed within the Dean of Students Office, is designed to be a “one-stop” network that brings safety to the forefront of campus life.  SafeCats uses this grant to fund a multi-faceted safety campaign that addresses specific safety priorities for freshmen. The first priority focuses on getting safety resource information directly into the hands of the students through the bystander intervention campaign “Be a Friend. Do Something.” This innovative campaign created through SafeCats uses dramatic, eye-catching images to challenge students to step up and do the right thing when they are making personal decisions or realize a fellow student is at risk.   Another primary safety tool is providing resource information and safety education through tabling, social media, and personal interactions.  

These safety initiatives increase knowledge, provide resources, and build skills within our freshman population who are vulnerable for risky situations.',
'outcomes' => 'The SafeCats Freshmen Fee proposal contains a set of learning outcomes for freshmen that: achieve specific safety priorities; meet the goals and mission of the University of Arizona and the division of Student Affairs & Enrollment Management, Academic Initiatives & Student Success;  and advance the goals of the First Year Student Fee.  Please see the attached supporting documentation for a full explanation of the impact and outcomes for the SafeCats First Year Student Fee grant.',
'response' => 'The estimated number of freshman students impacted is 7,000. This estimation is based on the number of freshman students who live on campus in residence halls, who utilize facilities where posters are displayed, and the number of students typically reached in person and via social media.',
'created_at' => '2015-12-17 01:31:26.000000',
'project_id' => 802,
),
48 => 
array (
'netid' => 'skomro',
'scope' => 'Bear Down Camp is a four-and-a-half-day experience for incoming first year students of the University of Arizona. Bear Down Camp is held annually the week before fall classes start in Prescott, Arizona for approximately 200 incoming students. The Bear Down Camp staff consists of an Executive and Associate Director, administrative team, counselors, and a crew team. Each member of staff is either a volunteer sophomore, junior, or senior student that is required to attend weekly trainings during the fall and spring semesters in addition to training sessions the week before camp. While at camp, groups consisting of 6-10 campers are led by a pair of counselors. Groups participate in various activities that meet the camp’s mission and learning objectives. These objectives include academic and on-campus resources, involvement opportunities, educational and health resources, and general information about the University of Arizona and university life. Bear Down Camp’s primary goal is to engage incoming students in camp activities that prepare them for their first year of college, leading to an increase in the University of Arizona’s overall retention rate. Additionally, we hope to create a peer network for students of all levels to rely on throughout their collegiate career.',
'outcomes' => 'Bear Down Camp made a goal of taking 275 freshman students to camp and registered 253 students in the three-month registration period. Ultimately, we were able to bring 201 students to Bear Down Camp, an increase of 3% from 2014.  
2015 registrants were our highest yet. We know there is demand for a low-cost extended orientation option for incoming students. We recognized this year that in order to meet our goal we need to improve our retention from registration to camp departure. In order to accomplish this, we plan to increase communication throughout the summer months prior to camp. This will include monthly emails until the last week in July in which we will send biweekly emails, with two emails sent each time – one specifically to the parent and one to the camper – in order to give the most information while also increasing camper excitement. We feel that with improvements in our marketing of camp and more contact with campers throughout the summer by staff members, we can improve upon our retention of campers to meet our goal. We also plan to set a hard deadline on when payment must be received in order to keep a camper’s place at camp, this will allow us to reduce the amount of miscommunication in terms of campers participating in camp. Additionally, we hope to continue to collaborate with Enrollment Management and Orientation to reach out to local high schools, sending information to out-of-state orientations, attending college fairs to outreach to potential campers, and increasing our social media presence.',
'response' => 'Each year, we assess our learning objectives in a survey. This year, we emailed the survey to 184 participants, and received 90 responses for a 48% response rate. Participants are asked to rate their level of agreement with 9 statements. Some of the most notable responses were: 97% of respondents felt that they had a better understanding of the University’s online resources, 92% of the respondents felt that they saw themselves as part of the Wildcat community after attending camp, and 99% of respondents were able to identify at least two ways to get involved at the University. Participants were also asked what they would tell a friend of what they learned about Bear Down Camp. The top four themed responses were of the new friendships made, Wildcat history and tradition, strategies to be successful at college, and services and resources on campus. Below are some student specific responses.

"At Bear Down Camp, I learned about the traditions at the University, places to eat, places to go to get help or become involved, and made many connections with individuals who I can turn to whenever I need help!” - 2015 BDC participant

“I learned all about what it means to be a true Wildcat and how to navigate through college, whether that be literally getting around campus or getting through school or personal or health struggles." - 2015 BDC participant

"I would tell them that after the camp I felt much more confident about college life at UA despite never visiting the school and coming from the East Coast." -2015 BDC participant

In general, Bear Down Camp makes a positive impact on the lives of the incoming freshmen that attend, which we hope to continue and expand upon.',
'created_at' => '2015-12-18 19:02:44.000000',
'project_id' => 803,
),
49 => 
array (
'netid' => 'kvd',
'scope' => 'Scholarship Universe (SU) is the University of Arizona’s (UA) homegrown web-based scholarship management system. UA Students answer questions to create their eligibility profile, and then are continually matched to scholarships for which they’re eligible for throughout the year. Our team searches for and verifies scholarship opportunities, as well as develops and codes new site features and functionality for both students and college/divisional partners.

Phase I of Scholarship Universe focused on initial site development and adding non-UA scholarships to the database; so far, 2,600 such legitimate scholarships (with an estimated 10,000 potential awards) have been added. SU made the process of finding scholarships faster and more efficient for UA Students.

Phase II of Scholarship Universe focused on the development of the Scholarship Universe Departmental Application (SUDA) which allows colleges and departments to manage numerous scholarship processes and offer their scholarships through SU. SUDA has increased the number of applicants and streamlined scholarship related processes for departments. SUDA also improves the transparency of the scholarship process by enhancing the visibility of UA scholarships. This makes it easier for students to see which departmental scholarships they’re actually applying for and when they’re available.

Phase III of Scholarship Universe is a comprehensive redesign to make it more efficient, more user and mobile friendly, and align more closely with the UA brand. Other than Arizona.edu, Scholarship Universe was the first UA site with newly branded home pages in 2015.',
'outcomes' => 'Because of Scholarship Universe’s innovation, mission, growth and success, it has been recognized nationally with two awards in the last three years. Most recently, Scholarship Universe was selected to be a Models of Excellence Honoree for the University Business magazine’s August 2015 online and print editions. The other national award was from Campus Technology in 2012, also a print and online magazine for higher education. Both awards are for innovation, serving students and inter-departmental partnerships. More information can be found at:
UA News:
•	http://uanews.org/story/scholarship-universe-named-a-national-model-program?utm_source=uanow&utm_medium=email&utm_campaign=biweekly-uanow

SU Awards Page:
•	https://scholarshipuniverse.arizona.edu/SUHA/AboutUS/Awards

1) UA Service
Scholarship Universe is the scholarship management system and service for most of the University of Arizona—18 colleges/divisions and the numerous departments within them. The newest partners of Scholarship Universe are Optical Sciences and the Graduate Interdisciplinary Programs (GIDP) within the Graduate College. UA Colleges and divisions all reached out for Scholarship Universe’s help because of the immense value it provides; with more potential partnerships on the horizon. UA Colleges and divisions depend on Scholarship Universe not only to promote scholarships, but to match students, import official student record data, create and collect applications, allow for online review committees, tracking of awards, thank you letter management, and more. Because of SU’s effort to integrate the student scholarship experience with the needs of colleges and divisions, more UA funds are being awarded than ever before—benefitting students and fulfilling UA’s promise to its scholarship donors. With a conservative estimate, Scholarship Universe will be helping departments across UA award Wildcats over five million dollars in scholarship awards for 2016-2017. 

2) Increases in Both Student Use and Scholarship Awards
According to our Google Analytics, pageviews of the student site, also known as the Scholarship Universe Student Application (SUSA), increased by over 100,000 (1,418,981 vs 1,318,326) January through November of 2015. This represents a 7.64% increase from the same period last year. Sessions (actively engaged) also saw a 14.95% increase (89,069 vs 77,487). Additionally, comparing those two same time periods, SUSA’s bounce rate dropped 38.49% (10.06 to 6.19%) as a result of the Phase III redesign efforts made thus far.

The Scholarship Universe Check App (SUCA) we developed also has provided us with one of the most accurate estimates yet of the financial impact that Scholarship Universe is having in regards to Non-UA scholarships.  Our analysis indicates that the 2014-15 school year, students won at least 807 non-UA awards which were available through Scholarship Universe worth over $2,440,000!  That is a $200,000 increase over the prior year’s estimate and represented an 11x annual return on the SSF’s investment in SU for the 2014-2015 budget which closed on June 30th, 2015.

3) Redesign of Scholarship Universe Landing Pages, SUSA and SUDA (Phase III)
The original Scholarship Universe website was successful in establishing itself as a unique resource for UA students, but the original design imposed significant technical limitations, limited usability, and didn’t present itself as a UA product.

As a result, we overhauled the look and feel of Scholarship Universe, starting first with the home pages.  The new homepages, launched on January 5th, 2015 and not only fixed those problems, but they enhanced the UA’s reputation as a leader for its design aesthetics, purpose, and web responsiveness accessible across various devices. Notably, Google started down grading web sites in their search results that were not responsively designed. The Scholarship Universe homepages were unaffected. SU also was one of the first responsive UA sites on campus.

Since our August progress report, we’ve launched many design improvements to both SUSA and SUDA, of which users can now see on their respective ‘What’s New’ pages. Because of the new design we are now able to incorporate newer and better functionality which has been overwhelmingly well received thus far. The speed of the sites have already improved and are becoming more and more intuitive.',
'response' => 'As noted previously, sessions increased (Jan. through Nov. 2015 vs 2014) by 14.95%. Based on site usage alone, students are speaking loud and clear that Scholarship Universe is a useful service.

The overwhelming response we get from students is a positive one: they like Scholarship Universe, recognize the value it provides, and are glad that it is available for them to use as a resource. 

This response is also reflected from our spring 2015 focus groups and open redesign survey. When asked – “what is one thing you’d like to improve in Scholarship Universe?” some responses have been: 

“I really love all of the website changes because they make everything easier to find.” – Rachel, Senior, Eller College of Management

“…I love the design and will probably use the site more now because of it.” – Kelly, Graduate Student, Government and Public Policy

“Can’t think of anything. So far its been a great resource!” – Noel, Freshman, Natural Resources

“Honestly, this site is just great in general.” – Tiffany, Sophomore, Pre-Business

“N/A, the layout is very easy to navigate” – Nissi, Junior, Pre-Family Studies & Human Development

“Nothing, this is so much better!” – Alexis, Graduate Student, American Indian Studies

“As of this moment there personally isn’t anything I would change about the website. It is easy to navigate through and I like the layout of it this way.” – Selena, Freshman, Law

“This semester has been my first experience with Scholarship Universe, and it has been very useful in applying for the grants and scholarships I need in order to pay for school.” – Barry, Sophomore, Astronomy

“Nothing” – Klaire, Sophomore, Nutritional Sciences

“It looks good.” – Andrew, Freshman, Biochemistry

“I honestly think that the website doesn’t need any improvements at the moment. The website is good as it is right now.” – Stephanie, Freshman, Veterinary Science',
'created_at' => '2015-12-22 18:05:59.000000',
'project_id' => 808,
),
50 => 
array (
'netid' => 'lscheu',
'scope' => 'The Campus Night Security Project (CNSP) supports two UAPD security officers for night duty in and around student housing facilities on campus. The two officers are on duty typically Thursday, Friday, and Saturday nights from 10 pm - 2 am, the days and hours most police involved incidents occur. However, to de-establish a pattern of patrolling, the Neighborhood watch also took place on other nights. The officers patrol the Park (West) and Highland (East) campus neighborhoods on foot, by golf-cart, and car. This physical presence of these officers helps bring an increased sense of safety and security to the campus.',
'outcomes' => 'The goal of the proposal is to increase the safety and security of our campus residents. While only the mid-point of the project year, Residence Life can report that the CNSP is fulfilling its goal. 

The CNSP patrol reported over 450 defined activities from August 15 – December 15, 2015 of which:

•	59% of all activities pertained to Area, Building and Welfare checks; 
•	15% were field interview/interaction or Field Contact related; 
•	4% were public assist related;
•	3% were traffic related; 
•	2% were related to suspicious activities; and
•	2% were flag downs (where a person asks for assistance from an officer passing by)

The remaining activities occurred less than 10 times over the course of the semester. The number and variety of activities indicate that officers are making their presence known and are providing a sense of increased safety and security on campus.',
'response' => 'During fall 2015 over 5,500 students living in the Park and Highland area halls were directly impacted by the CNSP. The additional 1,300+ students living on campus have likely benefitted indirectly from either being in these areas during patrol hours or learning/hearing about the patrol from classmates.',
'created_at' => '2015-12-22 19:04:48.000000',
'project_id' => 809,
),
51 => 
array (
'netid' => 'tbrett',
'scope' => 'The Division of Student Affairs is committed to building a campus community in which students, faculty, and staff at all levels feel valued and have the opportunities to succeed. Connecting students to the resources available to them from the beginning of their academic career is an essential component to success, and the University’s retention efforts. The Finding Community Welcome (FCW) aimed to both increase the visibility of the programs and services provided by the four cultural centers, the Disability Resource Center (DRC), LGBTQ Affairs, and the Women’s Resource Center (WRC); and to help incoming students “find community” the very first week of school. 

The planning team continued to meet the original purpose of the event, which is to welcome the traditionally marginalized communities on campus and recognize students’ intersecting and multiple identities. The committee coordinated more concentrated efforts in the programming and marketing of the event to focus on first year traditionally marginalized populations while making the event open to any student that wanted to participate.

The FCW’s main components included a presentation of the multiple support centers, a welcome from Student Affairs Administration and ASUA student leadership, student cultural performances, and a multicultural club fair. We are still implementing the second portion of the FCW with a professional speaker event in the spring 2016 semester.',
'outcomes' => 'The 2015 FCW successfully occurred on Wednesday, September 2, 2015 in the Student Union Memorial Center’s Grand Ballroom. The physical setup of the event was divided into two main areas. The main program occurred in the South Ballroom and the multicultural club fair with the food began after the program in the North Ballroom. Students were checked in using a CatCard swipe system and reflected 458 attendees.

Below are the 2015 goals stated in our Finding Community Welcome proposal and how they were successfully addressed.

1) To allow students to learn about AASA, APASA, CHSA, DRC, LGBTQ Affairs, NASA, & WRC.
a. According to the FCW evaluation, 86% of students agreed that they learned about at least one organization or resource that they plan to use during the school year.
b. The ASUA President spoke to highlight the commitment ASUA has to student diversity and social justice initiatives on campus. 

2) To allow students to have an opportunity to learn about diverse clubs and organizations on campus.
a. This year we had 47 different multicultural and social justice clubs at the fair.
b. According to our FCW evaluation, 91 % agreed that this event increased their understanding of diverse clubs and opportunities to get involved on campus. 

3) To allow students to meet other new students in order to expand their peer networks.
a. The FCW provided a Human Bingo Icebreaker that allowed students to interact with other students that they did not know. In order for students to acquire a raffle ticket for the raffle items, students had complete the bingo form, ensuring that they met multiple students and expanded their peer networks at this event.
b. According to the FCW evaluation, 75% agreed that this environment helped them make connections with other students.

4) To allow students to experience cultural student performances
a. The FCW provided cultural student experiences from the NASA, AASA, APASA, and Guerrero centers. From a traditional Native American blessing to open the event to a tutorial on stepping from the Greek organizations of AASA, students were engaged throughout the event.

In conclusion, the main purpose in hosting the FCW was to connect students to the resources here to support underrepresented students on campus. We feel the FCW showcases the diversity at UA from the very beginning of the year, and helps students know their support systems during their tenure as Wildcats. We are still planning to expand the event by adding a professional speaker to open the event. The speaker would address the issues of marginalized populations within higher education in parallel with the first year experience. We hope to help students find a sense of belonging in higher education beyond their time at the University of Arizona. We find it important to address these issues at a higher level, especially given the multiple issues marginalized communities are facing in our nation.',
'response' => 'The 2015 FCW was a great success in the engagement and retention of new students through multiple aspects of intentional programming for marginalized populations and effectively getting information to students through a more successful and sustainable mediums. To ensure we hit the target population for this event, we marketed by utilizing the listserves from the cultural centers, DRC, LGBTQ Affairs, and WRC, as well as directly emailing all students who indicated they are students of color upon application. The event was open to all UA students through specific venues, which included advertisement in the Daily Wildcat, a mention at new student Convocation, and other social media outlets. The sustainable FCW video continues to live on the centers’ websites, allowing for positive messages to participate in these communities throughout the year.

As in the past, the event was evaluated with Campus Labs. The overall student response was very positive. Below are some of the evaluation comments made from the students that aligned with assessing the successful outcome of the program.
•“I appreciated being able to recognize the diversity at the UofA. Even more so, find a community in a big place. I feel less alone.”
•“I enjoyed learning about other organizations on campus b/c i feel that only the frats/sororities
(Predominately white) are the main focus on campus.”
•“What I appreciated the most was learning about all the different clubs, I didn\'t know about them before”
•“I appreciated the community opportunity to make new friends”',
'created_at' => '2015-12-22 23:07:06.000000',
'project_id' => 807,
),
52 => 
array (
'netid' => 'venezia',
'scope' => 'New Student Convocation 2015, which was rebranded as "Bigger Better Bash!", is a celebratory event for new students held on the UA mall the Sunday evening before classes begin in the fall. 
The event teaches new students traditions, inspires community, and provides attendees with their official "Bear Down!" shirt.',
'outcomes' => 'Our goals included providing an inclusive, celebratory, safe, and tradition based ceremony that would build a sense of community for new students. Through First Year Student Fee funding, we were able to provide these services. We do not take an official assessment of attendees, however students anecdotally shared that the event was above their expectations (despite the additional entertainment, funded by the Student Unions, being cancelled due to weather and safety concerns).',
'response' => 'Approximately 7,500 new students attended the event.',
'created_at' => '2016-02-09 03:07:23.000000',
'project_id' => 799,
),
53 => 
array (
'netid' => 'tbrett',
'scope' => 'The Finding Community Welcome (FCW) was a series of events that welcomed new students who are part of marginalized populations on campus. The series of activities began in the Fall Semester with a large kick off event. With additional funding approved this year we extended this to Spring 2016 to include new students coming to the university in Spring as well as to continue to assist students in finding supportive communities on campus that contribute to their well-being and success.

The Division of Student Affairs is committed to building a campus community in which students, faculty, and staff at all levels feel valued and have the opportunities to succeed. Connecting students to the resources available to them from the beginning of their academic career is an essential component to success, and the University’s retention efforts. The Finding Community Welcome (FCW) aimed to both increase the visibility of the programs and services provided by the four cultural centers, the Disability Resource Center (DRC), LGBTQ Affairs, and the Women’s Resource Center (WRC); and to help incoming students “find community” during their first year at UA.',
'outcomes' => 'The Fall 2015 FCW successfully occurred on Wednesday, September 2, 2015 in the Student Union Memorial Center’s Grand Ballroom. The physical setup of the event was divided into two main areas. The main program occurred in the South Ballroom and the multicultural club fair with the food began after the program in the North Ballroom. Students were checked in using a CatCard swipe system and reflected 458 attendees.

Below are the 2015 goals stated in our Finding Community Welcome proposal and how they were successfully addressed.

1) To allow students to learn about AASA, APASA, CHSA, DRC, LGBTQ Affairs, NASA, & WRC.
a. According to the FCW evaluation, 86% of students agreed that they learned about at least one organization or resource that they plan to use during the school year.
b. The Vice Provost for Inclusive Excellence spoke to highlight the commitment ASUA has to student diversity and social justice initiatives on campus. 

2) To allow students to have an opportunity to learn about diverse clubs and organizations on campus.
a. This year we had 47 different multicultural and social justice clubs at the fair.
b. According to our FCW evaluation, 91 % agreed that this event increased their understanding of diverse clubs and opportunities to get involved on campus. 

3) To allow students to meet other new students in order to expand their peer networks.
a. The FCW provided a Human Bingo Icebreaker that allowed students to interact with other students that they did not know. In order for students to acquire a raffle ticket for the raffle items, students had complete the bingo form, ensuring that they met multiple students and expanded their peer networks at this event.
b. According to the FCW evaluation, 75% agreed that this environment helped them make connections with other students.

4) To allow students to experience cultural student performances
a. The FCW provided cultural student experiences from the NASA, AASA, APASA, and Guerrero centers. From a traditional Native American blessing to open the event to a tutorial on stepping from the Greek organizations of AASA, students were engaged throughout the event.

Spring Finding Community Welcome included 4 events that connect first year students to speakers and opportunities to meet students from across the cultural and resource centers. We sponsored 3 speakers and a social/recreational events in the Spring 2016 semester. 

Jessica Solomon: Director of Art in Praxis. Jess is an organization and community development practitioner. Jess hosted a lunch hour workshop with 30 students and an evening workshop with 20 students. 

Lourdes Ashley Hunter: Lourdes, Executive Director, focuses on research, curriculum development, global organizing, and activism. Lourdes worked with new students to assist them in identifying their goals and how they will achieve those goals during their time at UA.

Allyson Tintiangco-Cubales: Professor in Asian American Studies, Allyson focuses on Ethnic Studies pedagogy and community responsive pedagogy. Allyson presented to over 100 student and UA community members about creating an academic community that reflected their needs and diversity. 97.8% of respondents found Allyson’s talk to be valuable and 88.9% found that her talk increased their knowledge. 

Cultural and Resource Center Game Day at the Rec: This event welcomed students to the Recreation Center to connect with other students in a fun, relaxed environment. Centers hosted tables to give information to students and students participated in group organized games.',
'response' => 'The 2015 FCW was a great success in the engagement and retention of new students through multiple aspects of intentional programming for marginalized populations and effectively getting information to students through a more successful and sustainable mediums. To ensure we hit the target population for this event, we marketed by utilizing the listserves from the cultural centers, DRC, LGBTQ Affairs, and WRC, as well as directly emailing all students who indicated they are students of color upon application. The event was open to all UA students through specific venues, which included advertisement in the Daily Wildcat, a mention at new student Convocation, and other social media outlets. The sustainable FCW video continues to live on the centers’ websites, allowing for positive messages to participate in these communities throughout the year.

As in the past, the event was evaluated with Campus Labs. The overall student response was very positive. Below are some of the evaluation comments made from the students that aligned with assessing the successful outcome of the program.
•“I appreciated being able to recognize the diversity at the UofA. Even more so, find a community in a big place. I feel less alone.”
•“I enjoyed learning about other organizations on campus b/c i feel that only the frats/sororities
(Predominately white) are the main focus on campus.”
•“What I appreciated the most was learning about all the different clubs, I didn\'t know about them before”
•“I appreciated the community opportunity to make new friends”

As mentioned above, student found speakers to be valuable in increasing knowledge and making connections. 97.8% of respondents found Dr. Tintiangco-Cubales’ talk to be valuable and 88.9% found that her talk increased their knowledge.',
'created_at' => '2016-08-30 19:35:10.000000',
'project_id' => 807,
),
54 => 
array (
'netid' => 'kvd',
'scope' => 'Scholarship Universe (SU) is the University of Arizona’s (UA) homegrown web-based scholarship management system. UA Students answer questions to create their eligibility profile, and then are continually matched to scholarships for which they’re eligible for throughout the year. Our team searches for and verifies scholarship opportunities, as well as develops and codes new site features and functionality for both students and college/divisional partners.
Phase I of Scholarship Universe focused on initial site development and adding non-UA scholarships to the database; so far, 2,800 such legitimate scholarships (with an estimated 10,000 potential awards) have been added. SU made the process of finding scholarships faster and more efficient for UA Students.  
Phase II of Scholarship Universe focused on the development of the Scholarship Universe Departmental Application (SUDA) which allows colleges and departments to manage numerous scholarship processes and offer their scholarships through SU. SUDA has increased the number of applicants and streamlined scholarship related processes for departments. SUDA also improves the transparency of the scholarship process by enhancing the visibility of UA scholarships. This makes it easier for students to see which departmental scholarships they’re actually applying for and when they’re available.
Phase III of Scholarship Universe is a comprehensive redesign to make it more efficient, more user and mobile friendly, and align more closely with the UA brand. 
Phase IV is to work to commercialize the product with Phoenix-based company, CampusLogic, to be sold to other schools and a migrate UA to the new commercial product.',
'outcomes' => 'Because of Scholarship Universe’s innovation, mission, growth and success, it has been recognized nationally with two awards in the last three years. Most recently, Scholarship Universe was selected to be a Models of Excellence Honoree for the University Business magazine’s August 2015 online and print editions. The other national award was from Campus Technology in 2012, also a print and online magazine for higher education. Both awards are for innovation, serving students and inter-departmental partnerships. More information can be found at:
UA News:
•	http://uanews.org/story/scholarship-universe-named-a-national-model-program?utm_source=uanow&utm_medium=email&utm_campaign=biweekly-uanow

SU Awards Page:
•	https://scholarshipuniverse.arizona.edu/SUHA/AboutUS/Awards

1)	UA Service
Scholarship Universe is the scholarship management system and service for most of the University of Arizona—20 colleges/divisions and the numerous departments within them. The newest partners of Scholarship Universe are ASUA, Life & Work Connections, College of Nursing and the Alumni Association Clubs and Chapters. UA Colleges and divisions all reached out for Scholarship Universe’s help because of the immense value it provides; with more potential partnerships on the horizon. UA Colleges and divisions depend on Scholarship Universe not only to promote scholarships, but to match students, import official student record data, create and collect applications, allow for online review committees, tracking of awards, thank you letter management, and more. Because of SU’s effort to integrate the student scholarship experience with the needs of colleges and divisions, more UA funds are being awarded than ever before—benefitting students and fulfilling UA’s promise to its scholarship donors. With a conservative estimate, Scholarship Universe will be helping departments across UA award Wildcats over five million dollars in scholarship awards for 2016-2017. 

2)	User Growth
Distinct Scholarship Universe users has increased year-after-year. Unique users for 2016 has already surpassed 20,000. This is already greater than total distinct users (18,491) from the entire 2014 year. We are projecting to surpass 2015 total distinct users as well because the weekly average for distinct users in 2016 is 1,662 compared to 1,158 for 2015.

3)	Commercialization Licensing Deal
Scholarship Universe has garnered a lot of attention and recently was licensed to Phoenix-based company CampusLogic. CampusLogic is an education company focusing on transforming financial aid for students and schools. Scholarship Universe was a perfect complement to some of their other products and the two teams will be working together to take SU to other schools around the country. Here are some following press releases and articles covering the story. See Appendix for press releases and Articles.',
'response' => 'Based on site usage alone, students are speaking loud and clear that Scholarship Universe is a useful service.
The overwhelming response we get from students is a positive one: they like Scholarship Universe, recognize the value it provides, and are glad that it is available for them to use as a resource.  
This response is also reflected from our focus groups and open redesign survey. When asked – “what is one thing you’d like to improve in Scholarship Universe?” some responses have been: 
“I really love all of the website changes because they make everything easier to find.” – Rachel, Senior, Eller College of Management
“…I love the design and will probably use the site more now because of it.” – Kelly, Graduate Student, Government and Public Policy
“Can’t think of anything. So far its been a great resource!” – Noel, Freshman, Natural Resources
“Honestly, this site is just great in general.” – Tiffany, Sophomore, Pre-Business
“N/A, the layout is very easy to navigate” – Nissi, Junior, Pre-Family Studies & Human Development
“Nothing, this is so much better!” – Alexis, Graduate Student, American Indian Studies
“As of this moment there personally isn’t anything I would change about the website. It is easy to navigate through and I like the layout of it this way.” – Selena, Freshman, Law
“This semester has been my first experience with Scholarship Universe, and it has been very useful in applying for the grants and scholarships I need in order to pay for school.” – Barry, Sophomore, Astronomy
“Nothing” – Klaire, Sophomore, Nutritional Sciences
“It looks good.” – Andrew, Freshman, Biochemistry
“I honestly think that the website doesn’t need any improvements at the moment. The website is good as it is right now.” – Stephanie, Freshman, Veterinary Science',
'created_at' => '2016-08-31 00:12:24.000000',
'project_id' => 808,
),
55 => 
array (
'netid' => 'sevapriya',
'scope' => 'The University of Arizona freshman class brings together a diverse group of students with varied backgrounds. Each student possesses a different level of knowledge of existing policies, laws, and resources that can support personal safety while attending the University of Arizona.  Our freshmen encounter new and unfamiliar situations and environments which require that they make safety assessments and decisions.  In order to best support freshman, UA SafeCats aims to equip them with information and support.

SafeCats, housed within the Dean of Students Office, is designed to be a “one-stop” network that brings safety to the forefront of campus life.  SafeCats uses this grant to fund a multi-faceted safety campaign that addresses specific safety priorities for freshmen. The first priority focuses on getting safety resource information directly into the hands of the students through the bystander intervention campaign “Be a Friend. Do Something. ” This innovative campaign created through SafeCats uses dramatic, eye-catching images to challenge students to step up and do the right thing when they are making personal decisions or realize a fellow student is at risk.   Another primary safety tool is providing resource information and safety education through tabling, social media, and personal interactions.  

These safety initiatives increase knowledge, provide resources, and build skills within our freshman population who are vulnerable for risky situations.',
'outcomes' => 'The SafeCats Freshmen Fee proposal contains a set of learning outcomes for freshmen that: achieve specific safety priorities; meet the goals and mission of the University of Arizona and the division of Student Affairs & Enrollment Management, Academic Initiatives & Student Success;  and advance the goals of the First Year Student Fee.  Please see the attached supporting documentation for a full explanation of the impact and outcomes for the SafeCats First Year Student Fee grant.',
'response' => 'The estimated number of freshman students impacted is 7,000. This estimation is based on the number of freshman students who live on campus in residence halls, who utilize facilities where posters are displayed, and the number of students typically reached in person and via social media.',
'created_at' => '2016-08-31 21:58:21.000000',
'project_id' => 802,
),
56 => 
array (
'netid' => 'kriester',
'scope' => 'Parent and Family Programs publishes and distributes one magazine each semester.  The Parents & Family Magazine provides helpful information and resources for parents and includes articles about students, faculty, and staff members.  The magazine provides a great glimpse of what is happening on campus each semester.  The funding for the printing of the magazine is covered by add sales for the magazine which is handled by Student Media.  Parent and Family Programs received funding support for mailing the magazines to parents of our freshmen.   Each semester the magazine is mailed to approximately 7500 freshmen parents.  The spring edition will be mailed to the incoming freshmen and their parents.  The spring edition contains a special “New Student” section to help parents make sure that their student is ready for Orientation and for their first semester in college.  Parent and Family Programs will continue to cover the cost of mailing the magazine to our parent and family donors.',
'outcomes' => 'The fall edition of the Parents & Family Magazine was mailed on November 20, 2015 to 11,975 families.  The spring edition was mailed to the families of current freshmen as well as families of incoming freshmen for a total of 14,654 magazines mailed.  First Year Student Fee funding assisted in part of the mailing costs and the balance was supported by the Parent and Family Programs budget.   The fall edition was 33 pages long and contained eight articles/features including features on Financial Literacy, Applied Sustainability Education and a Donor Thank You article.   The spring edition was 36 pages and included articles on the I Will Campaign, the Student Engagement Initiative, New Student/Family Orientation, Summer Send Offs, donor recognition, and a feature on the new Dean of the Eller College, Dr. Palo Goes.  

Parent and Family Programs has several learning outcomes for the Parent & Family Magazine:
-Provide timely and important information to parents and families.  
-Provide continual exposure to Parent & Family Programs and the UA Parent & Family Association as a resource for families.
-Provide stewardship for Parents & Family Association Donors and Corporate Sponsors.

We believe these outcomes were met based on the content of the fall and spring magazines.',
'response' => 'The funding for this project helped to support the mailing of 26,629 Parent & Family Magazines to the families of our students.  

Here is feedback from families on the Magazine:
- 82% of those receiving the Parent & Family Magazine said they were either satisfied or very satisfied with the magazine content.  
- 74% of the respondents indicated that shared the majority or some of the information in the magazine with their student.
- 26% of the respondents used the magazine information for personal knowledge. 

See attached pdf for survey/data details.',
'created_at' => '2016-09-01 00:34:38.000000',
'project_id' => 810,
),
57 => 
array (
'netid' => 'lscheu',
'scope' => 'The Campus Night Security Project (CNSP) supports two UAPD security officers for night duty in and around student housing facilities on campus. The two officers are on duty typically Thursday, Friday, and Saturday nights from 10 pm - 2 am, the days and hours most police involved incidents occur. However, to de-establish a pattern of patrolling, the Neighborhood watch also took place on other nights. The officers patrol the Park (West) and Highland (East) campus neighborhoods on foot, by golf-cart, and car. This physical presence of these officers helps bring an increased sense of safety and security to the campus.',
'outcomes' => 'The CNSP patrol reported over 775 defined activities from August 15, 2015 – May 11, 2016 of which:

•	64% of all activities pertained to Area, Building and Welfare checks; 
•	13% were Field Interview/interaction or Field Contact related; 
•	3% were public assist related;
•	3% were related to suspicious activities; and
•	2% were traffic related; 

The remaining activities occurred less than 15 times over the course of the semester. The number and variety of activities indicate that officers are making their presence known and are providing a sense of increased safety and security on campus.',
'response' => 'As in fall 2015, the over 5,500 students living in the Park and Highland area halls continued to be directly impacted by the CNSP in spring 2016. The additional 1,300+ students living on campus have likely benefitted indirectly from either being in these areas during patrol hours or learning/hearing about the patrol from classmates.',
'created_at' => '2016-09-02 21:16:03.000000',
'project_id' => 809,
),
58 => 
array (
'netid' => 'shandispencer',
'scope' => 'Bear Down Camp is a five day experience for incoming first year students of the University of Arizona. Bear Down Camp is held annually the week before fall classes start in Prescott, Arizona for approximately 200 incoming students. The Bear Down Camp staff consists of Executive and Associate Directors, an administrative team, counselors, and a crew team. Each member of staff is either a volunteer sophomore, junior, or senior student that is required to attend bi weekly trainings during the fall and spring semesters in addition to training sessions the week before camp. While at camp, groups consisting of 6-10 campers lead by a pair of counselors. Groups participate in various activities that meet the camp’s mission and learning objectives. These objectives include academic and on-campus resources, involvement opportunities, educational and health resources, and general information about the University of Arizona and university life. Bear Down Camp’s primary goal is to engage incoming students in camp activities that prepare them for their first year of college, leading to an increase in the University of Arizona’s overall retention rate. Additionally, we hope to create a peer network for students of all levels to rely on throughout their collegiate career.',
'outcomes' => 'We were able to bring 123 students to Bear Down Camp in 2016.  We know there is demand for a low-cost extended orientation option for incoming students. We recognize that to impact the greatest number of students we must increase the number of campers who attend camp.  In order to accomplish this, we plan to increase marketing to incoming freshmen prior to the opening of our registration period. This will include working with Enrollment Management and Orientation to reach out to local high schools, sending information to out-of-state orientations, attending college fairs to outreach to potential campers, and increasing our social media presence. We feel that with improvements in our marketing of camp we can improve upon our camper attendance to meet our goal.',
'response' => 'Each year, we assess our learning objectives in a survey. This year, we emailed the survey to 123 participants, and received 79 responses, for a response rate of 64%. Participants are asked to rate their level of agreement with 12 statements. Some of the most notable responses were: 94% of respondents felt that they had a better understanding of the University’s online resources, 96% of respondents felt they were able to identify at least two strategies to lead a healthy lifestyle as a student, 97% of the respondents felt that they saw themselves as part of the Wildcat community after attending camp, and 95% of respondents were able to identify at least two ways to get involved at the University. Participants were also asked what they would tell a new student about what they learned at Bear Down Camp. The top four themed responses were of the new friendships made, Wildcat history and tradition, strategies to be successful at college, and services and resources on campus. Below are some student specific responses.

"I learned a lot about the school, chants, and various forms of resources for almost any problem that may arise during the school year. I also learned that there are kind people from everywhere in the country and that everyone at BDC truly just wants to help you succeed and make you feel comfortable here at UA." -2016 BDC participant

In general, Bear Down Camp makes a positive impact on the lives of the incoming freshmen that attend, which we hope to continue and expand upon.',
'created_at' => '2016-12-16 18:36:38.000000',
'project_id' => 811,
),
59 => 
array (
'netid' => 'tracecamacho',
'scope' => 'The SafeCats Freshmen Fee proposal contains a set of learning outcomes for freshmen that: achieve specific safety priorities; meet the goals and mission of the University of Arizona and the division of Student Affairs & Enrollment Management, Academic Initiatives & Student Success;  and advance the goals of the First Year Student Fee',
'outcomes' => 'Learning Outcome 1: Develop an understanding of university resources
SafeCats program initiatives aim to help our freshman population increase knowledge, build skills, and identify campus safety resources. Several tactics used to reach this goal are funded by the First Year Student Fee. 

* Students can “like” the SafeCats Facebook page or “follow” SafeCats on Twitter. 	Social media sites are updated almost daily with safety content.  
o SafeCats Facebook page: 1042 total likes (Dec. 2016)
o SafeCats Twitter: 672 total followers (Dec. 2016)
o The SafeCats Facebook page is updated almost daily with safety information.  Posts     
typically receive 150 – 800 views; “promoted” posts can receive up to 3,000 views 
* SafeCats Hosted a Safety Resource Fair in honor of National Campus Safety Awareness Month
o It was September 29th 10am-2pm on the mall outside of the union
o Key safety players on campus attended to hand out information to students
* UAPD
* Campus Health
* ASUA Campus Legal Services
* SPEAC (Women’s Resource Center organization committed to Sexual Assault Prevention) 
* Dean of Students Office /SafeCats
o Each table spoke with between 50-100 individuals as people walked to class, but the    
resources were visible to hundreds of students. 
Learning Outcome 2: Develop the ability to take initiative for fellow students or friends on and off campus. SafeCats promotes peer-to-peer bystander intervention and encourages students to take action if they see someone may be in danger.
* 237 posters were displayed in the residence halls during Fall 2015 covering the following topics:
o Alcohol Abuse
o Threatening Behavior
o Personal Safety
o Cyberbullying
o Financial Safety
o Hazing
o Personal Responsibility 

* The SafeCats graduate assistant tabled and distributed safety cards, pens, safety whistles, flash drives, color changing cups and general information to approximately 600 students at the following events:	
o National Campus Safety Awareness Month
*  “Be A Friend” electronic slides are displayed on plasma screens in the Dean of 
Students Office

Learning Outcome 3: Develop confidence, commitment, and dedication to the UA community
Hazing prevention benefits all students, but is particularly important for our freshman population. Hazing undermines student’s confidence and self-esteem and can negatively impact the first year experience.
Hazing Prevention 
* Multiple anti-hazing messages and graphics were posted to social media for National Hazing prevention week in September

Learning Outcome 4: Take an active role in the partnership of campus safety
SafeCats partners with various campus entities to spread important safety messages to campus.  

Campus Health:
* Submitted safety tips for Living Wild – a quarterly e-magazine published by Campus Health
o Over 1,000 views for both September and November 2016 issues: 

University of Arizona Police Department:
SafeCats partners with the University of Arizona Police Department (UAPD) to get out safety messages, promote safety tools, and help students identify and value UAPD as a campus resource.',
'response' => 'Number of students impacted is listed in the outcomes and in the progress report attached.',
'created_at' => '2016-12-19 15:55:41.000000',
'project_id' => 816,
),
60 => 
array (
'netid' => 'kriester',
'scope' => 'The division of Student Affairs has committed to expand Family Weekend Programming, making it a more robust and diverse event accessible to all family members and the campus community.  The program which has been student run for more than 30 years, and overseen by ASUA, is now partnering with the Parent and Family Programs making it a natural fit for family members and students.  The goals and expectations are to make Family Weekend an affordable and diverse event that helps promote the University’s retention and recruitment efforts while showcasing the University of Arizona’s academia, professors, UA spirit and traditions, campus resources, activities, and Tucson community experiences.  Family Weekend is a rich University of Arizona tradition which began in 1929. What started as a few families coming to visit their children has become an annual tradition for thousands of family members who come to the University of Arizona campus each fall.  Family Weekend provides family members the opportunity to understand the meaning of being a Wildcat.  This is achieved by families connecting with other Wildcat families, as well as, faculty, staff, and other students. Family Weekend celebrates the important role that family members have in supporting students and the program provides an experience that will help participants feel like a member of the Wildcat Family.',
'outcomes' => 'Outcomes for the event have been determined and have been assessed.
-	Families will have an opportunity to see student’s home away from home and experience everyday life on campus.
-	Families will gain a sense of the academic excellence of the UA faculty.  
-	Families will gain knowledge of their role and connect to campus resources to better support their student’s success.
-	Families will learn about what it means to be a Wildcat through experiencing UA traditions and spirit.
-	Families will have an opportunity to participate in activities on campus and in the Tucson community.
-	Families will have the opportunity to meet and get to know other Wildcat families, and UA faculty, and staff.
-	Families will have the opportunity to feel celebrated and appreciated for supporting their student’s success.

In addition to the outcomes, the staff and student directors of Family Weekend intend to follow the mission of Family Weekend, increasing participation of family members, and offering affordable and diverse opportunities for everyone in attendance.  Family Weekend has always been a self-funded program, relying on participation fees, and a $2000 grant from the Parent & Family Programs.',
'response' => 'A total of 466 2016 Family Weekend Assessments were distributed, and 134 responses were received, with 126 respondents fully completing the survey.
76% of the family members attending Family Weekend had a freshman student on campus.  The remaining were dispersed between sophomores through graduate students.  
76% of the respondents felt there were just enough events to cover family weekend, listing the football game as their most favorite event.
Outcomes met:
•	Families will have an opportunity to see student’s home away from home and experience everyday life on campus- PFP hosted a family conference where campus partners presented on a variety of topics ranging from nutrition on campus, to history and traditions.
•	Families will gain a sense of the academic excellence of the UA faculty- the final presenter for the conference was a last lecture presented by Dr. Bryan Carter, Associate Professor of Africana Studies
•	Families will learn about what it means to be a Wildcat through experiencing UA traditions and spirit- History and traditions session, campus ghost tours, UA football game.
•	Families will have an opportunity to participate in activities on campus and in the Tucson community- The Rec center hosted a variety of events throughout the Tucson community, along with a variety of events/programs on campus
•	Families will have the opportunity to meet and get to know other Wildcat families, and UA faculty, and staff- PFP ice cream social, Bear Down BBQ, PFP Family Conference, Wilbur & Wilma playground offered many opportunities to meet other family members, staff and students.
•	Families will have the opportunity to feel celebrated and appreciated for supporting their student’s success- PFP Ice Cream Social had an awards ceremony to celebrate “Parent/Family member of the year” “Parent/Family Volunteer of the year” and 2 student scholarship awards.
SEE ATTACHED DOCUMENT',
'created_at' => '2016-12-20 00:33:24.000000',
'project_id' => 819,
),
61 => 
array (
'netid' => 'lscheu',
'scope' => 'The Campus Night Security Project (CNSP) supports two UAPD security officers for night duty in and around student housing facilities on campus. The two officers are on duty typically Thursday, Friday, and Saturday nights from 10 pm - 2 am, the days and hours most police involved incidents occur. However, to de-establish a pattern of patrolling, the Neighborhood watch also took place on other nights. The officers patrol the Park (West) and Highland (East) campus neighborhoods on foot, by golf-cart, and car. This physical presence of these officers helps bring an increased sense of safety and security to the campus.',
'outcomes' => 'The goal of the proposal is to increase the safety and security of our campus residents. While only the mid-point of the project year, Residence Life can report that the CNSP is fulfilling its goal. 

The CNSP patrol reported over 475 defined activities from August 13 – December 7, 2016 of which:

•	64% of all activities pertained to Area, Building and Welfare checks; 
•	9% were field interview/interaction or Field Contact related; 
•	6% were assist related (public, medical, motorist or traffic);
•	3% were Minor in Possession related; 
•	2% were related to suspicious activities; and
•	2% were report writing 

The remaining activities occurred less than 10 times over the course of the semester. The number and variety of activities indicate that officers are making their presence known and are providing a sense of increased safety and security on campus.',
'response' => 'During fall 2016 over 5,500 students living in the Park and Highland area halls were directly impacted by the CNSP. The additional 1,300+ students living on campus have likely benefitted indirectly from either being in these areas during patrol hours or learning/hearing about the patrol from classmates.',
'created_at' => '2016-12-22 23:25:44.000000',
'project_id' => 814,
),
62 => 
array (
'netid' => 'tbrett',
'scope' => 'The Division of Student Affairs is committed to building a campus community in which students, faculty, and staff at all levels feel valued and have the opportunities to succeed. Connecting students to the resources available to them from the beginning of their academic career is an essential component to success, and the University’s retention efforts. The Finding Community Welcome (FCW) aimed to both increase the visibility of the programs and services provided by the four cultural centers, the Disability Resource Center (DRC), LGBTQ Affairs, and the Women’s Resource Center (WRC); and to help incoming students “find community” the very first week of school. 

The planning team continued to meet the original purpose of the event, which is to welcome the traditionally marginalize communities on campus and recognize students’ intersecting and multiple identities. The committee coordinated more concentrated efforts in the programming and marketing of the event to focus on first year traditionally marginalized populations while making the event open to any student that wanted to participate. The FCW’s main components included a presentation of the multiple support centers, a welcome from Student Affairs Administration and ASUA student leadership, student cultural performances, and a multicultural club fair.',
'outcomes' => 'The 2016 FCW successfully occurred on Monday, August 29, 2916 in the Student Union Memorial Center’s Grand Ballroom. The physical setup of the event was divided into two main areas. The main program occurred in the South Ballroom and the multicultural club fair with the food began after the program in the North Ballroom. Students were checked in using a CatCard swipe system and reflected 291attendees. Approximately 100 tablers and performers also participated in the event. This year, the organizers decided to implement changes to see if they would improve the overall event and engagement of students, while also highlighting the Cultural and Resource Centers week of welcome. For example, the day of the event was changed to Monday in order to kick off a week of open houses/welcome events at each of the Cultural and Resource Centers. We shifted the club and organization fair to make it the main component of the students engagement and interaction.

Below are the 2016 goals stated in our Finding Community Welcome proposal and how they were successfully addressed.

1) To allow students to learn about AASA, APASA, CHSA, DRC, LGBTQ Affairs, NASA, & WRC.
a. According to the FCW evaluation, 73% of students agreed that they learned about at least one organization or resource that they plan to use during the school year.
b. The Dean of Students spoke to highlight the commitment the Dean of Student’s Office has to student diversity and social justice initiatives on campus.
c. In order for students to acquire a raffle ticket for the raffle items, students were required to receive 3 signatures from 3 different organizations that had booths on display. 

2) To allow students to have an opportunity to learn about diverse clubs and organizations on campus.
a. This year we had 49 different multicultural and social justice clubs at the fair.
b. According to our FCW evaluation, 73% of the surveyed students agreed that this event increased their understanding of diverse clubs and opportunities to get involved on campus. 

3) To allow students to meet other new students in order to expand their peer networks.
a. FCW provided a Mix and Mingle type of atmosphere by serving a BBQ style of meal to allow students to interact with other students and to create new relationships. ASUA provided “life-size” types of games such as Jenga for students engage with one another and to focus on team building aspects. 
b. According to the FCW evaluation, half of the surveyed students agreed that this environment helped them make connections with other students.

4) To allow students to experience cultural student performances
a. The FCW provided cultural student experiences from the NASA, AASA, APASA, and Guerrero centers. The Cultural and Resource Center Directors performed a spoken word piece to demonstrate their support for all marginalized students. These performances kept students engaged throughout the event.

In conclusion, the main purpose in hosting the FCW was to connect students to the resources here to support underrepresented students on campus. We feel the FCW showcases the diversity at UA from the very beginning of the year, and helps students know their support systems during their tenure as Wildcats. We are still planning to expand the event by adding a professional speaker to open the event. The speaker would address the issues of marginalized populations within higher education in parallel with the first year experience. We hope to help students find a sense of belonging in higher education beyond their time at the University of Arizona. We find it important to address these issues at a higher level, especially given the multiple issues marginalized communities are facing in our nation.',
'response' => 'The 2016 FCW was a great success in the engagement and retention of new students through multiple aspects of intentional programming for marginalized populations and effectively getting information to students through a more successful and sustainable mediums. To ensure we hit the target population for this event, we marketed by utilizing the listserves from the cultural and resource centers, DRC, LGBTQ Affairs, and WRC, as well as directly emailing all students who indicated they are students of color upon application. The event was open to all UA students through specific venues, which included advertisement in the Daily Wildcat, a mention at new student Convocation, and other social media outlets. The sustainable FCW video continues to live on the centers’ websites, allowing for positive messages to participate in these communities throughout the year. 

The overall student response was very positive. Below are some of the evaluation comments made from the students that aligned with assessing the successful outcome of the program.

•“I appreciated seeing all of the clubs as it makes it easy to get connected and involved.”
•“I appreciated EVERYTHING.”
•“I appreciated the effort people made to make me feel welcomed.”
•“I appreciated the community VIBE.”
•“Diversity is incredible.”
•“I appreciated the culture and inclusiveness.”',
'created_at' => '2016-12-30 21:09:10.000000',
'project_id' => 815,
),
63 => 
array (
'netid' => 'siqueirj',
'scope' => 'The Bigger, Better Bash is an event to officially welcome all freshmen students to the Wildcat family.  At the event, the freshmen students were taught UA traditions, including cheers, sons, Wildcat Claw, etc.',
'outcomes' => 'We expected to have as many freshmen as possible at the Bigger, Better Bash and we were very glad to see so many students at the event, about 7,500 ended up showing up!',
'response' => 'Many of the students expressed how glad they were about having attended the Bigger, Better Bash as they had the opportunity to engage with speakers, pep band members, and cheerleaders.  They also indicated that they were able to meet and make new friends and to also interact via social media.',
'created_at' => '2017-01-06 15:47:52.000000',
'project_id' => 820,
),
64 => 
array (
'netid' => 'tracecamacho',
'scope' => 'SafeCats is a “one-stop-shop” network that brings safety to the forefront of campus life. Our various educational campaigns address some of the most prevalent issues facing college students and are designed in a way that appeals to the first year student population.

SafeCats receives all funding from external grants. Funding from the First Year Student Fee is crucial for the efforts listed below, and will help increase the successful impact that SafeCats can have on disseminating important safety information.',
'outcomes' => 'Learning Outcome 1: Develop an understanding of university resources
SafeCats program initiatives aim to help our freshman population increase knowledge, build skills, and identify campus safety resources. this was done through social media post and the campus safety fair

Learning Outcome 2: Develop the ability to take initiative for fellow students or friends on and off campus. THis was achieved through tabling at campus events and distributing posters in campus residence halls

Learning Outcome 3: Develop confidence, commitment, and dedication to the UA community
Hazing prevention benefits all students, but is particularly important for our freshman population.  Hazing undermines student’s confidence and self-esteem and can negatively impact the first year experience. This was achieved through posts on our social media outlets


Learning Outcome 4: Take an active role in the partnership of campus safety
SafeCats partners with various campus entities to spread important safety messages to campus.  This was done by submitting safety tips for Living Wild – a quarterly e-magazine published by Campus Health
o	Over 1,000 views for both September and November 2016 issues:',
'response' => 'Students can “like” the SafeCats Facebook page or “follow” SafeCats on Twitter.  Social media sites are updated almost daily with safety content.  
o	SafeCats Facebook page: 1042 total likes (Dec. 2016)
o	SafeCats Twitter: 672 total followers (Dec. 2016)
o	The SafeCats Facebook page is updated almost daily with safety information.  Posts typically receive 150 – 800 views; “promoted” posts can receive up to 3,000 views 

237 posters were displayed in the residence halls during Fall 2016 and 2017 covering the following topics:
o	Alcohol Abuse
o	Threatening Behavior
o	Personal Safety
o	Cyberbullying
o	Financial Safety
o	Hazing
o	Personal Responsibility 

The SafeCats graduate assistant tabled and distributed safety cards, pens, safety whistles, flash drives, color changing cups and general information to approximately 600 students at the following event:	
o	National Campus Safety Awareness Month Safety Fair on 9/29/16',
'created_at' => '2017-08-25 22:09:13.000000',
'project_id' => 816,
),
65 => 
array (
'netid' => 'kriester',
'scope' => 'The division of Student Affairs has committed to expand Family Weekend Programming, making it a more robust and diverse event accessible to all family members and the campus community.  The program which has been student run for more than 30 years, and overseen by ASUA, is now partnering with the Parent and Family Programs making it a natural fit for family members and students.  The goals and expectations are to make Family Weekend an affordable and diverse event that helps promote the University’s retention and recruitment efforts while showcasing the University of Arizona’s academia, professors, UA spirit and traditions, campus resources, activities, and Tucson community experiences.  Family Weekend is a rich University of Arizona tradition which began in 1929. What started as a few families coming to visit their children has become an annual tradition for thousands of family members who come to the University of Arizona campus each fall.  Family Weekend provides family members the opportunity to understand the meaning of being a Wildcat.  This is achieved by families connecting with other Wildcat families, as well as, faculty, staff, and other students. Family Weekend celebrates the important role that family members have in supporting students and the program provides an experience that will help participants feel like a member of the Wildcat Family.',
'outcomes' => 'Outcomes for the event have been determined and have been assessed.
-	Families will have an opportunity to see student’s home away from home and experience everyday life on campus.
-	Families will gain a sense of the academic excellence of the UA faculty.  
-	Families will gain knowledge of their role and connect to campus resources to better support their student’s success.
-	Families will learn about what it means to be a Wildcat through experiencing UA traditions and spirit.
-	Families will have an opportunity to participate in activities on campus and in the Tucson community.
-	Families will have the opportunity to meet and get to know other Wildcat families, and UA faculty, and staff.
-	Families will have the opportunity to feel celebrated and appreciated for supporting their student’s success.

In addition to the outcomes, the staff and student directors of Family Weekend intend to follow the mission of Family Weekend, increasing participation of family members, and offering affordable and diverse opportunities for everyone in attendance.  Family Weekend has always been a self-funded program, relying on participation fees, and a $2000 grant from the Parent & Family Programs.',
'response' => 'A total of 466 2016 Family Weekend Assessments were distributed, and 134 responses were received, with 126 respondents fully completing the survey.
76% of the family members attending Family Weekend had a freshman student on campus.  The remaining were dispersed between sophomores through graduate students.  
76% of the respondents felt there were just enough events to cover family weekend, listing the football game as their most favorite event.
Outcomes met:
•	Families will have an opportunity to see student’s home away from home and experience everyday life on campus- PFP hosted a family conference where campus partners presented on a variety of topics ranging from nutrition on campus, to history and traditions.
•	Families will gain a sense of the academic excellence of the UA faculty- the final presenter for the conference was a last lecture presented by Dr. Bryan Carter, Associate Professor of Africana Studies
•	Families will learn about what it means to be a Wildcat through experiencing UA traditions and spirit- History and traditions session, campus ghost tours, UA football game.
•	Families will have an opportunity to participate in activities on campus and in the Tucson community- The Rec center hosted a variety of events throughout the Tucson community, along with a variety of events/programs on campus
•	Families will have the opportunity to meet and get to know other Wildcat families, and UA faculty, and staff- PFP ice cream social, Bear Down BBQ, PFP Family Conference, Wilbur & Wilma playground offered many opportunities to meet other family members, staff and students.
•	Families will have the opportunity to feel celebrated and appreciated for supporting their student’s success- PFP Ice Cream Social had an awards ceremony to celebrate “Parent/Family member of the year” “Parent/Family Volunteer of the year” and 2 student scholarship awards.
SEE ATTACHED DOCUMENT',
'created_at' => '2017-08-30 00:21:34.000000',
'project_id' => 819,
),
66 => 
array (
'netid' => 'ccgieszl',
'scope' => 'Bigger. Better. Bash! is the official welcome event for all new students to the University of Arizona held the evening before classes begin.  While it varies each year, the 2016 program was held in the McKale Center simulating a ZonaZoo-esque experience for students.  All new students hae the opportunity to pick up their free Bear Down t-shirt on their way into the venue and were directed to all put their shirt on at one time as a class.
Better. Bash! aims to be fun, high-energy, and a great opportunity to help students feel at home and meet new friends.  Students heard welcoming remarks from leaders on campus such as Kendal Washington-White, Melissa Vito, Kasey Urquidez, the ASUA Student Body president, ZonaZoo Crew, and more.  On their way out, everyone was given free ice cream and KIND bars.',
'outcomes' => 'Our goal in 2016 was to reach at least 5000 new students.  We approximated that we had approximately 5000 students attend Bigger. Better. Bash!.',
'response' => 'On average, Bigger. Better. Bash! has reached about 5500 students each year.  This means thousands of students attended to connect with campus resources and hear motivating words before their first day as a college student.  This event allows students to feel connected to UA, leadership, the campus, and other students.  It provides the opportunity for those students who may be feeling nervous about classes starting the next day to relax, get excited about the next step in their journey, and ask questions of campus experts.',
'created_at' => '2017-08-31 16:34:34.000000',
'project_id' => 820,
),
67 => 
array (
'netid' => 'tbrett',
'scope' => 'The Division of Student Affairs is committed to building a campus community in which students, faculty, and staff at all levels feel valued and have the opportunities to succeed. Connecting students to the resources available to them from the beginning of their academic career is an essential component to success, and the University’s retention efforts. The Finding Community Welcome (FCW) aimed to both increase the visibility of the programs and services provided by the four cultural centers, the Disability Resource Center (DRC), LGBTQ Affairs, and the Women’s Resource Center (WRC); and to help incoming students “find community” the very first week of school. 

The planning team continued to meet the original purpose of the event, which is to welcome the traditionally marginalize communities on campus and recognize students’ intersecting and multiple identities. The committee coordinated more concentrated efforts in the programming and marketing of the event to focus on first year traditionally marginalized populations while making the event open to any student that wanted to participate. The FCW’s main components included a presentation of the multiple support centers, a welcome from Student Affairs Administration and ASUA student leadership, student cultural performances, and a multicultural club fair.

(Please note as this is a fall semester only event, the progress report has not changed from the mid-year report.)',
'outcomes' => 'The 2016 FCW successfully occurred on Monday, August 29, 2916 in the Student Union Memorial Center’s Grand Ballroom. The physical setup of the event was divided into two main areas. The main program occurred in the South Ballroom and the multicultural club fair with the food began after the program in the North Ballroom. Students were checked in using a CatCard swipe system and reflected 291attendees. Approximately 100 tablers and performers also participated in the event. This year, the organizers decided to implement changes to see if they would improve the overall event and engagement of students, while also highlighting the Cultural and Resource Centers week of welcome. For example, the day of the event was changed to Monday in order to kick off a week of open houses/welcome events at each of the Cultural and Resource Centers. We shifted the club and organization fair to make it the main component of the students engagement and interaction.

Below are the 2016 goals stated in our Finding Community Welcome proposal and how they were successfully addressed.

1) To allow students to learn about AASA, APASA, CHSA, DRC, LGBTQ Affairs, NASA, & WRC.
a. According to the FCW evaluation, 73% of students agreed that they learned about at least one organization or resource that they plan to use during the school year.
b. The Dean of Students spoke to highlight the commitment the Dean of Student’s Office has to student diversity and social justice initiatives on campus.
c. In order for students to acquire a raffle ticket for the raffle items, students were required to receive 3 signatures from 3 different organizations that had booths on display. 

2) To allow students to have an opportunity to learn about diverse clubs and organizations on campus.
a. This year we had 49 different multicultural and social justice clubs at the fair.
b. According to our FCW evaluation, 73% of the surveyed students agreed that this event increased their understanding of diverse clubs and opportunities to get involved on campus. 

3) To allow students to meet other new students in order to expand their peer networks.
a. FCW provided a Mix and Mingle type of atmosphere by serving a BBQ style of meal to allow students to interact with other students and to create new relationships. ASUA provided “life-size” types of games such as Jenga for students engage with one another and to focus on team building aspects. 
b. According to the FCW evaluation, half of the surveyed students agreed that this environment helped them make connections with other students.

4) To allow students to experience cultural student performances
a. The FCW provided cultural student experiences from the NASA, AASA, APASA, and Guerrero centers. The Cultural and Resource Center Directors performed a spoken word piece to demonstrate their support for all marginalized students. These performances kept students engaged throughout the event.

In conclusion, the main purpose in hosting the FCW was to connect students to the resources here to support underrepresented students on campus. We feel the FCW showcases the diversity at UA from the very beginning of the year, and helps students know their support systems during their tenure as Wildcats. We are still planning to expand the event by adding a professional speaker to open the event. The speaker would address the issues of marginalized populations within higher education in parallel with the first year experience. We hope to help students find a sense of belonging in higher education beyond their time at the University of Arizona. We find it important to address these issues at a higher level, especially given the multiple issues marginalized communities are facing in our nation.',
'response' => 'he 2016 FCW was a great success in the engagement and retention of new students through multiple aspects of intentional programming for marginalized populations and effectively getting information to students through a more successful and sustainable mediums. To ensure we hit the target population for this event, we marketed by utilizing the listserves from the cultural and resource centers, DRC, LGBTQ Affairs, and WRC, as well as directly emailing all students who indicated they are students of color upon application. The event was open to all UA students through specific venues, which included advertisement in the Daily Wildcat, a mention at new student Convocation, and other social media outlets. The sustainable FCW video continues to live on the centers’ websites, allowing for positive messages to participate in these communities throughout the year. 

The overall student response was very positive. Below are some of the evaluation comments made from the students that aligned with assessing the successful outcome of the program.

•“I appreciated seeing all of the clubs as it makes it easy to get connected and involved.”
•“I appreciated EVERYTHING.”
•“I appreciated the effort people made to make me feel welcomed.”
•“I appreciated the community VIBE.”
•“Diversity is incredible.”
•“I appreciated the culture and inclusiveness.”',
'created_at' => '2017-09-05 21:49:05.000000',
'project_id' => 815,
),
68 => 
array (
'netid' => 'lscheu',
'scope' => 'The project helped fund two dedicated campus security officers (who are currently University of Arizona Police Officers) for night duty in and around student housing facilities on campus during the Thursday, Friday and Saturday nights during the academic year.',
'outcomes' => 'The goal of the project is to provide a safer environment on campus where many freshmen live.',
'response' => 'The project impacted the 7,000 students that live on campus- more if those who live in fraternity and sorority houses are included. Well over 5,000 of those students are freshmen. Security presence provided the optics of a safer environment and helped students feel safer.',
'created_at' => '2017-09-06 18:08:18.000000',
'project_id' => 814,
),
69 => 
array (
'netid' => 'shandispencer',
'scope' => 'Bear Down Camp is a five-day experience for incoming first-year students of the University of Arizona. Bear Down Camp is held annually the week before fall classes start in Prescott, Arizona for approximately 200 incoming students. The Bear Down Camp staff consists of Executive and Associate Directors, an administrative team, counselors, and a crew team. Each member of staff is either a volunteer sophomore, junior, or senior student that is required to attend bi weekly trainings during the fall and spring semesters in addition to training sessions the week before camp. While at camp, groups consisting of 6-10 campers lead by a pair of counselors. Groups participate in various activities that meet the camp’s mission and learning objectives. These objectives include academic and on-campus resources, involvement opportunities, educational and health resources, and general information about the University of Arizona and university life. Bear Down Camp’s primary goal is to engage incoming students in camp activities that prepare them for their first year of college, leading to an increase in the University of Arizona’s overall retention rate. Additionally, we hope to create a peer network for students of all levels to rely on throughout their collegiate career.',
'outcomes' => 'We were able to bring 123 students to Bear Down Camp in 2016.  We know there is demand for a low-cost extended orientation option for incoming students. We recognize that to impact the greatest number of students we must increase the number of campers who attend camp.  In order to accomplish this, we plan to increase marketing to incoming freshmen prior to the opening of our registration period. This will include working with Enrollment Management and Orientation to reach out to local high schools, sending information to out-of-state orientations, attending college fairs to outreach to potential campers, and increasing our social media presence. We feel that with improvements in our marketing of camp we can improve upon our camper attendance to meet our goal.',
'response' => 'Each year, we assess our learning objectives in a survey. This year, we emailed the survey to 123 participants, and received 79 responses, for a response rate of 64%. Participants are asked to rate their level of agreement with 12 statements. Some of the most notable responses were: 94% of respondents felt that they had a better understanding of the University’s online resources, 96% of respondents felt they were able to identify at least two strategies to lead a healthy lifestyle as a student, 97% of the respondents felt that they saw themselves as part of the Wildcat community after attending camp, and 95% of respondents were able to identify at least two ways to get involved at the University. Participants were also asked what they would tell a new student about what they learned at Bear Down Camp. The top four themed responses were of the new friendships made, Wildcat history and tradition, strategies to be successful at college, and services and resources on campus. Below are some student specific responses.

“I learned about the traditions and history of the University of Arizona, which made me feel like part of the community here on campus. I learned about the resources available to me, which will help me succeed academically and personally. I made friends and formed relationships that will last much longer than the actual days at camp." - 2016 BDC participant

In general, Bear Down Camp makes a positive impact on the lives of the incoming freshmen that attend, which we hope to continue and expand upon.',
'created_at' => '2017-09-08 18:22:52.000000',
'project_id' => 811,
),
70 => 
array (
'netid' => 'tcola',
'scope' => 'The Wildcat Way is a six-week sexual violence prevention program, designed by the Women’s Resource Center (WRC) and facilitated by trained undergraduate peer educators. This program strives to educate first year students, as well as make our campus a safer place for first year students who are at highest risk of experiencing sexual violence. To garner buy in to the message of sexual assault prevention, the WRC developed a program that focuses on party culture on campus. Within this program, Transforming Wildcat Culture, the Peer Educators ask students to gain basic knowledge and skills about transforming rape culture into consent culture.  This is our buy in program that is given to large populations like Resident\'s Life and New members of Greek Life. Students are asked to imagine the perfect party. Then they learn about consent, the relationship between alcohol and sexual violence, rape culture, and consent culture. Finally, they are asked to apply what they’ve learned to the perfect party they imagined, encouraging them to consider specific tools for making college parties more likely to be characterized by consent culture. In this presentation we define rape culture and how our attitudes and actions perpetuate that culture. We then work to challenge rape culture and offer an alternative, Consent Culture. During this presentation we identify ways that we can change our attitudes and actions to reflect a consent culture at U of A. In spring 2018, we plan to work directly with these students again to build upon their knowledge.',
'outcomes' => 'Due to First Year Student Fee funding, the WRC was able to fund 13 Peer Educators during the Fall 2017 semester. The Peer Educators came back to campus early in order to complete an extensive orientation.  Additionally this funding was able to support the work on one Graduate Assistant (GA). This GA is responsible for providing educational opportunities and support to the Peer Educators. Over 4,200 student contacts were made during the course of the semester. Initial data suggests that the programs delivered are effective at creating change in attitudes and knowledge of participants.
Additionally, during the months of October and November, 49 students from fraternity and sororities completed the Wildcat Way series. The Wildcat Way series, led by the peer educators, is a 6-week 90 minute workshop series that dives deeper into Consent, Rape Culture, Understand systems of sexual violence response, and bystander intervention. Research has indicated a need for sustained, multiple-part sexual assault prevention programs targeting key peer influencers that covers topics of consent, rape culture, and bystander intervention (Casey & Lindhorst, 2009; DeGue, 2014; Flood, 2006; Gidycz et al., 2011; Langford, 2004). Our experience has also indicated a need for this education, when feasible, to be conducted in gender-segregated environments so that messaging may be targeted to the specific needs of men and women. A pre and post survey was administered during the course of this 6-week series. Data from this program is still being analyzed for results.',
'response' => 'During the months of August and September, 310 students within the Highland District and 1,596 new members within Greek Life participated in Transforming Wildcat Culture. Retrospective surveys were distributed during the presentations. The survey measured attitudes in 5 categories: Perceived peer rape supportive attitudes, personal responsibility with bystander intervention, self-efficacy in bystander intervention, knowledge of rape supportive attitudes and knowledge of sexual violence.  Results yielded statically significant change in attitudes from before the workshop to after the workshop in all five categories. More in depth analysis of the report able results can be seen in the supporting data document attached to this report.',
'created_at' => '2017-12-14 21:34:34.000000',
'project_id' => 823,
),
71 => 
array (
'netid' => 'lscheu',
'scope' => 'The Neighborhood Watch program supports two UAPD security officers for night duty in and around student housing facilities on campus. The two officers are on duty typically Thursday, Friday, and Saturday nights from 10 pm - 2 am, the days and hours most police involved incidents occur. However, to de-establish a pattern of patrolling, the Neighborhood Watch also took place on other nights. The officers patrol the Park (West) and Highland (East) campus neighborhoods on foot, by golf-cart, and car. This physical presence of these officers helps bring an increased sense of safety and security to campus.',
'outcomes' => 'The Neighborhood Watch patrol reported over 330 defined activities from August 12 – December 9, 2017 of which:

•	55% of all activities pertained to Area, Building and Welfare checks; 
•	5% were field interview/interaction or Field Contact related; 
•	3% were assist related (public, medical, motorist or traffic);
•	6% were Minor in Possession related; 
•	4% were related to suspicious activities; and
•	4% were related to traffic 

The remaining activities occurred less than 10 times over the course of the semester. The number and variety of activities indicate that officers are making their presence known and are providing a sense of increased safety and security on campus.',
'response' => 'During fall 2017 over 5,500 students living in the Park and Highland area halls were directly impacted by the Neighborhood Watch. The additional 1,300+ students living on campus have likely benefitted indirectly from either being in these areas during patrol hours or learning/hearing about the patrol from classmates.',
'created_at' => '2017-12-19 18:00:44.000000',
'project_id' => 826,
),
72 => 
array (
'netid' => 'tracecamacho',
'scope' => 'The SafeCats Freshmen Fee proposal contains a set of learning outcomes for freshmen that: achieve specific safety priorities; meet the goals and mission of the University of Arizona and the division of Student Affairs & Enrollment Management, Academic Initiatives & Student Success;  and advance the goals of the First Year Student Fee.  Below you will find the identified learning outcomes and the progress we have made.',
'outcomes' => 'Learning Outcome 1: Develop an understanding of university resources
SafeCats program initiatives aim to help our freshman population increase knowledge, build skills, and identify campus safety resources.  Several tactics used to reach this goal are funded by the First Year Student Fee. 
•	Students can “like” the SafeCats Facebook page or “follow” SafeCats on Twitter. Social media sites are updated almost weekly with safety content.  
o	SafeCats Facebook page: 1,118 Followers
o	SafeCats Twitter
o	The SafeCats Facebook page is updated almost weekly with safety information. 
o	
Learning Outcome 2: Develop the ability to take initiative for fellow students or friends on and off campus
SafeCats promotes peer-to-peer bystander intervention and encourages students to take action if they see someone may be in danger.

•	A new campaign and posters were created to educate student on UA’s “Good Samaritan Protocol”
o	100 posters were distrusted to residence halls, fraternity and sorority chapters and the Dean of Students Office.
o	Electric version of the poster were shared on social media and on electronic signage.
•	237 posters were displayed in the residence halls during Fall 2017 covering the following topics:
o	Alcohol Abuse
o	Threatening Behavior
o	Personal Safety
o	Cyberbullying
o	Financial Safety
o	Hazing
o	Personal Responsibility 
•	“Good Sam, I am” electronic slides are displayed on plasma screens in the Dean of Students Office
Learning Outcome 3: Develop confidence, commitment, and dedication to the UA community
A majority of time was spent this semester educating students on the “good Samaritan Protocol to show students how they individual can intervene and make a difference.

Learning Outcome 4: Take an active role in the partnership of campus safety
SafeCats partners with various campus entities to spread important safety messages to campus.  
Dean of Students Office:
•	Submitted alcohol safety tips for online article to be featured on the Dean of Students Website',
'response' => '•	237 posters were displayed in the residence halls during Fall 2017 covering the following topics:
o	Alcohol Abuse
o	Threatening Behavior
o	Personal Safety
o	Cyberbullying
o	Financial Safety
o	Hazing
o	Personal Responsibility 
•	A new campaign and posters were created to educate student on UA’s “Good Samaritan Protocol”
o	100 posters were distrusted to residence halls, fraternity and sorority chapters and the Dean of Students Office.
o	Electric version of the poster were shared on social media and on electronic signage.
•	Students can “like” the SafeCats Facebook page or “follow” SafeCats on Twitter. 	Social media sites are updated almost weekly with safety content.  
o	SafeCats Facebook page: 1,118 Followers
o	SafeCats Twitter
o	The SafeCats Facebook page is updated almost weekly with safety information.',
'created_at' => '2017-12-19 21:22:07.000000',
'project_id' => 824,
),
73 => 
array (
'netid' => 'stevenm1',
'scope' => 'The Division of Student Affairs is committed to building a campus community in which students, faculty, and staff at all levels feel valued and have the opportunities to succeed. Connecting students to the resources available to them from the beginning of their academic career is an essential component to success, and the University’s retention efforts. The Finding Community Welcome (FCW) aimed to both increase the visibility of the programs and services provided by African America Student Affairs, Asian Pacific American Student Affairs, Native American Student Affairs, the Guerrero Student Center, the Disability Resource Center (DRC), LGBTQ Affairs, and the Women’s Resource Center (WRC); and to help incoming students “find community” during their first weeks of school. 

The planning team continued to meet the original purpose of the event, which is to welcome the traditionally marginalize communities on campus and recognize students’ intersecting and multiple identities. The committee coordinated more concentrated efforts in the programming and marketing of the event to focus on first year traditionally marginalized populations while making the event open to any student that wanted to participate. As well as expanding to include open house and welcome events at each of the Cultural and Resource Centers.',
'outcomes' => 'The 2017 FCW successfully occurred on Thursday, August 24, 2017in the Student Union Memorial Center’s Grand Ballroom. The physical setup of the event was divided into two main areas. The main program occurred in the South Ballroom and the multicultural club fair and resource fair with the food began after the program in the North Ballroom. Students were checked in using a CatCard swipe system and sign in sheets and reflected 405 attendees. This year, the organizers decided to implement changes to see if they would improve the overall event and engagement of students, while also highlighting the Cultural and Resource Centers week of welcome. 

Below are the 2017 goals stated in our Finding Community Welcome proposal and how they were successfully addressed.

1) To allow students to learn about AASA, APASA, GSC, LGBTQ Affairs, NASA, & WRC.
a. Each center and its affiliated student organizations were responsible for a tabling in six areas of the ballroom.
b. The Dean of Students spoke to highlight the commitment the Dean of Student’s Office has to student diversity initiatives on campus.
c. In order for students to acquire a raffle ticket for the raffle items, students participated in carnival games in each area of the ballroom. . 

2) To allow students to have an opportunity to learn about diverse clubs, organizations, resources on campus.
a. This year we had 51 different multicultural clubs at the fair along with 11 campus resource partners..


3) To allow students to meet other new students in order to expand their peer networks.
a. FCW provided a Mix and Mingle type of atmosphere by serving a BBQ style of meal to allow students to interact with other students and to create new relationships. Along with carnival games, a DJ and interactive performances in the center of the ballroom. 


In conclusion, the main purpose in hosting the FCW was to connect students to the resources here to support underrepresented students on campus. We feel the FCW showcases the diversity at UA from the very beginning of the year, and helps students know their support systems during their tenure as Wildcats. We hope to help students find a sense of belonging in higher education beyond their time at the University of Arizona. We find it important to address these issues at a higher level, especially given the multiple issues marginalized communities are facing in our nation.',
'response' => 'The 2017 FCW was a great success in the engagement and retention of new students through multiple aspects of intentional programming for marginalized populations and effectively getting information to students through a more successful and sustainable mediums. To ensure we hit the target population for this event, we marketed by utilizing the listserves from the cultural and resource centers as well as directly emailing all students who indicated they are students of color upon application. The event was open to all UA students through specific venues, which included advertisement in the Daily Wildcat, a mention at new student Convocation, and other social media outlets. 

The overall student response was very positive. Below are some of the evaluation comments made from the students that aligned with assessing the successful outcome of the program.

What did you appreciate the most about this event? Why?

“To see the diversity of students and the different cultures.”
“Getting to see what other resources are available to me.”
“The invitation to students of all backgrounds to celebrate together.”
“The friendliness because it was so welcoming.”
“Getting to learn about the different clubs that I didn’t know existed.”
“Networking”
“Community”
“Meeting other students.”
“Making new friends.”',
'created_at' => '2017-12-21 17:06:42.000000',
'project_id' => 829,
),
74 => 
array (
'netid' => 'injado',
'scope' => 'Bear Down Camp is a five day experience for incoming first year students of the University of Arizona. Bear Down Camp is held annually the week before fall classes start in Prescott, Arizona for approximately 200 incoming students. The Bear Down Camp staff consists of Executive and Associate Directors, an administrative team, counselors, and a crew team. Each member of staff is either a volunteer sophomore, junior, or senior student that is required to attend bi weekly trainings during the fall and spring semesters in addition to training sessions the week before camp. While at camp, groups consisting of 5-7 campers are led by a pair of counselors. Groups participate in various activities that meet the camp’s mission and learning objectives. These objectives include academic and on-campus resources, involvement opportunities, educational and health resources, and general information about the University of Arizona and university life. Bear Down Camp’s primary goal is to engage incoming students in camp activities that prepare them for their first year of college, leading to an increase in the University of Arizona’s overall retention rate. Additionally, we hope to create a peer network for students of all levels to rely on throughout their collegiate career.',
'outcomes' => 'We were able to bring 86 students to Bear Down Camp in 2017.  We know there is demand for a low-cost extended orientation option for incoming students. We recognize that to impact the greatest number of students we must increase the number of campers who attend camp.  In order to accomplish this, we plan to increase marketing to incoming freshmen prior to the opening of our registration period during the Spring 2018 semester. This will include working with Enrollment Management and Orientation to reach out to local high schools, sending information to out-of-state orientations, attending college fairs to outreach to potential campers, and increasing our social media presence. We feel that with improvements in our marketing of camp we can improve upon our camper attendance to meet our goal. Although camp attendance for Bear Down Camp 2017 did not numerically represent our ideal goal, after reviewing our student responses via survey, we still found that BDC provided statistically significant affirmation of the success of the program at increasing connection, unity and inclusion, and pride as a supplemental introduction to the University of Arizona.',
'response' => 'Each year, we assess our learning objectives in a survey. This year, we emailed the survey to 86 participants, and received 56 responses, for a response rate of 65%. Participants are asked to rate their level of agreement with 12 statements. Some of the most notable responses were: 100% of respondents felt that they had a better understanding of the University’s online resources, 95% of respondents felt they were able to identify at least two strategies to lead a healthy lifestyle as a student, 98% of the respondents felt that they saw themselves as part of the Wildcat community after attending camp, and 96% of respondents were able to identify at least two ways to get involved at the University. Participants were also asked what they would tell a new student about what they learned at Bear Down Camp. The top four themed responses were of the new friendships made, Wildcat history and tradition, strategies to be successful at college, and services and resources on campus. Of the 56 students who completed the survey, 45 of the students participated in the Day of Service. The purpose of Day of Service is to assist students with self-identification as part of both the UA and Tucson communities, bring awareness to social issues, and to provide students with volunteer opportunities and resources. As a result of participating in the BDC Day of Service, 80% of respondents said they were very likely to do any type of volunteer service in Tucson after completing Day of Service and that they felt more connected to the local Tucson community. 69% of students stated that they were able to identify a social issue, and a potential way to improve the community.',
'created_at' => '2017-12-21 20:28:38.000000',
'project_id' => 828,
),
75 => 
array (
'netid' => 'kriester',
'scope' => 'The Dean of Students Office, ASUA and Parent and Family Programs have committed to expanding Family Weekend programming, making it a more robust and diverse event, accessible to all family members.  The goals and expectations are to make Family Weekend an affordable, diverse event that showcases the University’s academic excellence, professors, UA spirit and traditions, campus resources, activities, and Tucson community experiences for the families of our students.  The Family Weekend Committee hopes to give families a glimpse into their student’s life in their “home away from home” which is especially important for families of freshmen.    

Family Weekend was held October 13 – 15, 2017.  The planning committee included multiple campus partners such as, Fraternity & Sorority Programs, Residence Life, the University of Arizona Bookstores, the Arizona Student Unions, the Disability Resource Center, and Campus Recreation. Family Weekend supports the mission of Student Affairs and Enrollment Management by providing family members with the opportunity to understand the meaning of being a Wildcat.  This is achieved by families connecting with other Wildcat families as well as faculty, staff, and other students achieve this. Family Weekend celebrates the important role that family members have in supporting students and the program provides an experience that will help participants feel like a member of the Wildcat Family.  Family Weekend also provides an opportunity for the UA to recruit younger siblings of current Wildcats.',
'outcomes' => 'The Family Weekend evaluation includes assessment for the outcomes listed below which are reviewed and utilized to assist in improving the event.  Attached you will find an Executive Summary of the Family Weekend Survey from 2016 which includes the first assessment of these outcomes.  
-	Families will have an opportunity to see student’s home away from home and experience everyday life on campus.
-	Families will gain a sense of the academic excellence of the UA faculty.  
-	Families will gain knowledge of their role and connect to campus resources to better support their student’s success.
-	Families will learn about what it means to be a Wildcat through experiencing UA traditions and spirit.
-	Families will have an opportunity to participate in activities on campus and in the Tucson community.
-	Families will have the opportunity to meet and get to know other Wildcat families and UA faculty, and staff.
-	Families will have the opportunity to feel celebrated and appreciated for supporting their student’s success.
Student services fee funding allowed Family Weekend to increase physical access of the event by offering free shuttle that ran between Highland Garage and the Student Union from 9am – 9pm on Friday, October 13.  We were also able to increase accessibility to the Bear Down BBQ on the UA Mall by adding additional accessible ramps to the grass area and extra lighting.  Additionally, we were able to offer a Meet and Greet event with UA President, Dr. Robert Robbins at no cost.',
'response' => '•Out of 380 families who attended the weekend events, 36 responded to the survey. This is a 9.47% response rate, which is a much smaller response rate from 2016.  
Outcomes met:
•Families will have an opportunity to see student’s home away from home and experience everyday life on campus: Parent and Family Programs hosted a family conference where campus partners presented on a variety of topics ranging from nutrition on campus, to history and traditions. 
•Families will gain a sense of the academic excellence of the UA faculty: the final presenter for the conference was a lecture presented by Dr. Sudha Ram, Professor of MIS, Entrepreneurship and Innovation whose presentation was titled, “Extracting Value from Big Data.” 
•Families will learn about what it means to be a Wildcat through experiencing UA traditions and spirit: The Family Conference included a history and traditions session and the UA football game was the favorite activity.
•Families will have an opportunity to participate in activities on campus and in the Tucson community: The Rec center hosted a variety of events throughout the Tucson community, along with a variety of events/programs on campus. Family Weekend also coincided with Tucson Meet Yourself, which was promoted as an option on the Family Weekend website. 
•Families will have the opportunity to meet and get to know other Wildcat families and UA faculty, and staff: Several events offered this opportunity including Bear Down BBQ, PFP Family Conference, and the Family Fun Zone.  Additionally, 45 families attended a meet and greet with new UA President, Dr. Robbins. 
•Families will have the opportunity to feel celebrated and appreciated for supporting their student’s success- The Bear Down BBQ included an awards ceremony to celebrate “Parent/Family member of the year” “Parent/Family Volunteer of the year” and 2 student scholarship awards.
See attached document',
'created_at' => '2017-12-21 20:50:26.000000',
'project_id' => 827,
),
76 => 
array (
'netid' => 'mfig',
'scope' => 'Collectively, the programs and services of Academic Success & Achievement (ASA) connect with well over 8,000 students annually, freshmen through graduate, to enhance student engagement, access, retention and timely graduation. Research shows that peer-to-peer interactions play a pivotal role in student engagement, retention and success. ASA relies heavily on this model to expand and enhance programs and services. 

When selecting a peer-mentoring program, one of the challenges students face is feeling like the program does not speak to their identity or interests. For freshmen, this can also include a desire to connect to the University of Arizona (UA) as they transition to campus for the first time, and to identify specific support that makes the UA feel like a community that includes them. To meet this challenge, ASA has increased the opportunities for individualized services; one of these programs is the Cultural Learning Communities (CLC). These have operated in previous years in partnership with three of the four Cultural Centers on campus. The First Year Student Fee funding we received is being used to expand the number of students served and to add the Guerrero Center as a partner for a new CLC.',
'outcomes' => '1. We were able to hire two more undergraduate peer mentors and create a new learning community in partnership with the Guerrero Student Center.
2. Partnership was expanded with all Cultural Centers to provide more support to students of color on campus.',
'response' => 'The Cultural Learning Communities employ five peer mentors, and they provide freshmen an opportunity to connect to campus through identity based programming, and by exploring their transition through a cultural lens. Our partnership with the Cultural Centers ensures students’ access to a larger community that supports student development and provides a network of diverse support services in order to provide a comprehensive program for freshmen.

There are four Cultural Learning Communities facilitated in partnership with each of the Cultural Centers. The Guerrero Center (GSC), focusing on the Latinx/Hispanic culture, had such a high demand for participation, we hired a second peer mentor to create two communities in partnership with the GSC.
57 freshmen participated in workshops and one-on-one meetings regularly.

This group was also asked to participate in cultural events throughout the semester to help them find more cultural connections on campus beyond the CLCs:

30 students participated in at least one additional cultural event at the Cultural Centers or in the Tucson community.

27 students participated in two cultural events at the Cultural Centers or in the Tucson community.

48 additional freshmen participated in drop-in workshops, events, and one or two peer mentoring meetings.

A total of 105 freshmen have received services from the Cultural Learning Communities peer mentors this semester.

The peer mentors conducted a total of 26 workshops for a total of 481 contact hours with students in workshops.

The peer mentors held 334 one-on-one meetings.

This results in a total of 815 contacts with students.',
'created_at' => '2017-12-31 23:05:36.000000',
'project_id' => 825,
),
));
        
        
    }
}