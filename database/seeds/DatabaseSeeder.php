<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // Users
        /*
        DB::table('users')->insert([
          'role_id' => 4,
          'first_name' => 'Andrew',
          'last_name' => 'Waegli',
          'netid' => 'awaegli',
        ]);
        DB::table('users')->insert([
          'role_id' => 4,
          'first_name' => 'Theresa',
          'last_name' => 'Whetzel',
          'netid' => 'twhetzel',
        ]);
        DB::table('users')->insert([
          'role_id' => 4,
          'first_name' => 'Andrew',
          'last_name' => 'Armour',
          'netid' => 'aarmour',
        ]);
        DB::table('users')->insert([
          'role_id' => 4,
          'first_name' => 'Lauren',
          'last_name' => 'White',
          'netid' => 'laurenmwhite',
        ]);
        DB::table('users')->insert([
          'role_id' => 4,
          'first_name' => 'Ben',
          'last_name' => 'Champion',
          'netid' => 'bchampion',
        ]);
        */

        // Roles
        DB::table('roles')->insert([
          'name' => 'Standard',
        ]);
        DB::table('roles')->insert([
          'name' => 'Chair',
        ]);
        DB::table('roles')->insert([
          'name' => 'Administrative',
        ]);
        DB::table('roles')->insert([
          'name' => 'Power User',
        ]);

        // User Standings
        DB::table('user_standings')->insert([
          'name' => 'Freshman',
        ]);
        DB::table('user_standings')->insert([
          'name' => 'Sophomore',
        ]);
        DB::table('user_standings')->insert([
          'name' => 'Junior',
        ]);
        DB::table('user_standings')->insert([
          'name' => 'Senior',
        ]);
        DB::table('user_standings')->insert([
          'name' => 'Graduate',
        ]);
        DB::table('user_standings')->insert([
          'name' => 'Professional',
        ]);
        DB::table('user_standings')->insert([
          'name' => 'N/A',
        ]);

        // User Titles
        DB::table('user_titles')->insert([
          'name' => 'Chair',
        ]);
        DB::table('user_titles')->insert([
          'name' => 'Co-Chair',
        ]);
        DB::table('user_titles')->insert([
          'name' => 'Director',
        ]);
        DB::table('user_titles')->insert([
          'name' => 'Member',
        ]);
        DB::table('user_titles')->insert([
          'name' => 'Secretary',
        ]);
        DB::table('user_titles')->insert([
          'name' => 'Treasurer',
        ]);
        DB::table('user_titles')->insert([
          'name' => 'Vice Chair',
        ]);

        // User Types
        DB::table('user_types')->insert([
          'name' => 'Administrative Advisor',
        ]);
        DB::table('user_types')->insert([
          'name' => 'Advisor Ex-Officio',
        ]);
        DB::table('user_types')->insert([
          'name' => 'Voting Member',
        ]);
        DB::table('user_types')->insert([
          'name' => 'NON-MEMBER',
        ]);

        // User Statuses
        DB::table('user_statuses')->insert([
          'name' => 'Active',
        ]);
        DB::table('user_statuses')->insert([
          'name' => 'Retired',
        ]);
        DB::table('user_statuses')->insert([
          'name' => 'N/A',
        ]);

        // sites
        DB::table('sites')->insert([
          'name' => 'UA Green Fund', // 1
          'slug' => str_slug('UA Green Fund'),
          'abbreviation' => 'GF',
          'board_application_open' => '2017-12-24 00:00:00',
          'board_application_deadline' => '2017-12-25 00:00:00',
        ]);
        DB::table('sites')->insert([
          'name' => 'Student Services Fee', // 2
          'slug' => str_slug('Student Services Fee'),
          'abbreviation' => 'SF',
          'board_application_open' => '2017-12-24 00:00:00',
          'board_application_deadline' => '2017-12-25 00:00:00',
        ]);
        DB::table('sites')->insert([
          'name' => 'First Year Student Fee',
          'slug' => str_slug('First Year Student Fee'),
          'abbreviation' => 'FF',
          'board_application_open' => '2017-12-24 00:00:00',
          'board_application_deadline' => '2017-12-25 00:00:00',
        ]);
        DB::table('sites')->insert([
          'name' => 'Health & Recreation Fee',
          'slug' => str_slug('Health & Recreation Fee'),
        ]);

        // file types
        DB::table('file_types')->insert([
          'name' => 'Budget',
        ]);
        DB::table('file_types')->insert([
          'name' => 'Description',
        ]);
        DB::table('file_types')->insert([
          'name' => 'Extra',
        ]);
        DB::table('file_types')->insert([
          'name' => 'Award',
        ]);
        DB::table('file_types')->insert([
          'name' => 'Outcome',
        ]);
        DB::table('file_types')->insert([
          'name' => 'Advice',
        ]);
        DB::table('file_types')->insert([
          'name' => 'Governing Document',
        ]);
        DB::table('file_types')->insert([
          'name' => 'Upload',
        ]);

        // pages
        DB::table('pages')->insert([
          'site_id' => 1,
          'parent_id' => 0,
          'default' => 1,
          'title' => 'Home',
          'slug' => 'home',
          'body' => '
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus facilisis venenatis nulla quis gravida. Donec commodo et sem at accumsan. Pellentesque eros diam, iaculis sit amet risus sit amet, accumsan dignissim metus. Phasellus sollicitudin non enim quis mattis. Duis ornare, metus id malesuada congue, quam felis aliquet nunc, eget interdum urna eros at neque. Fusce erat ex, luctus eu eleifend tempor, aliquam sit amet mi. In dui nunc, aliquam id nunc vitae, pulvinar tincidunt augue. Cras vel augue hendrerit, lacinia neque non, consectetur massa. Nulla accumsan ac massa vehicula aliquam. Nunc sed dolor sit amet nulla cursus mollis vel eu eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras lacinia nibh quis ex laoreet blandit a sed eros. Nullam tempor massa lectus, at fringilla neque maximus nec. Proin fringilla elementum nisi efficitur finibus.</p>
            <p>Ut hendrerit molestie nibh et blandit. Sed aliquam imperdiet massa. Aliquam eleifend turpis sit amet quam ornare sagittis. Morbi pretium augue magna, id finibus elit venenatis sit amet. Proin vulputate fermentum turpis, a porttitor turpis imperdiet sed. Morbi lacinia lectus ut volutpat consectetur. Cras vulputate suscipit sem sed porta. Aenean rutrum sapien dolor, vitae sodales lectus maximus at.</p>
            <p>Aliquam erat volutpat. Sed erat ante, suscipit luctus rhoncus id, finibus ullamcorper risus. Duis quis ante ut mauris facilisis mattis. Praesent molestie est non accumsan imperdiet. Etiam eu nunc sed enim tincidunt vulputate. Duis egestas varius leo in semper. Etiam iaculis iaculis hendrerit.</p>
            <p>Cras a placerat ipsum. Maecenas ut porttitor nunc. Donec nec arcu arcu. Sed luctus lacinia eleifend. Phasellus vitae posuere lacus. Nunc non pharetra metus, ut molestie lacus. Vestibulum faucibus pharetra leo, venenatis porta nibh gravida non.</p>
            <p>Vivamus maximus blandit risus, vel gravida mi. Donec volutpat sed ex tempus egestas. Vivamus hendrerit odio ut purus gravida vulputate. Praesent accumsan quam nec magna semper tristique. Ut ut nulla et dui ornare malesuada. Donec vestibulum eget felis quis tristique. Phasellus quis quam leo. Aenean vitae lacus odio. </p>
          ',
          'list_order' => 0,
          'published' => 1,
        ]);

        DB::table('pages')->insert([
          'site_id' => 2,
          'parent_id' => 0,
          'default' => 1,
          'title' => 'Home',
          'slug' => 'home',
          'body' => '
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus facilisis venenatis nulla quis gravida. Donec commodo et sem at accumsan. Pellentesque eros diam, iaculis sit amet risus sit amet, accumsan dignissim metus. Phasellus sollicitudin non enim quis mattis. Duis ornare, metus id malesuada congue, quam felis aliquet nunc, eget interdum urna eros at neque. Fusce erat ex, luctus eu eleifend tempor, aliquam sit amet mi. In dui nunc, aliquam id nunc vitae, pulvinar tincidunt augue. Cras vel augue hendrerit, lacinia neque non, consectetur massa. Nulla accumsan ac massa vehicula aliquam. Nunc sed dolor sit amet nulla cursus mollis vel eu eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras lacinia nibh quis ex laoreet blandit a sed eros. Nullam tempor massa lectus, at fringilla neque maximus nec. Proin fringilla elementum nisi efficitur finibus.</p>
            <p>Ut hendrerit molestie nibh et blandit. Sed aliquam imperdiet massa. Aliquam eleifend turpis sit amet quam ornare sagittis. Morbi pretium augue magna, id finibus elit venenatis sit amet. Proin vulputate fermentum turpis, a porttitor turpis imperdiet sed. Morbi lacinia lectus ut volutpat consectetur. Cras vulputate suscipit sem sed porta. Aenean rutrum sapien dolor, vitae sodales lectus maximus at.</p>
            <p>Aliquam erat volutpat. Sed erat ante, suscipit luctus rhoncus id, finibus ullamcorper risus. Duis quis ante ut mauris facilisis mattis. Praesent molestie est non accumsan imperdiet. Etiam eu nunc sed enim tincidunt vulputate. Duis egestas varius leo in semper. Etiam iaculis iaculis hendrerit.</p>
            <p>Cras a placerat ipsum. Maecenas ut porttitor nunc. Donec nec arcu arcu. Sed luctus lacinia eleifend. Phasellus vitae posuere lacus. Nunc non pharetra metus, ut molestie lacus. Vestibulum faucibus pharetra leo, venenatis porta nibh gravida non.</p>
            <p>Vivamus maximus blandit risus, vel gravida mi. Donec volutpat sed ex tempus egestas. Vivamus hendrerit odio ut purus gravida vulputate. Praesent accumsan quam nec magna semper tristique. Ut ut nulla et dui ornare malesuada. Donec vestibulum eget felis quis tristique. Phasellus quis quam leo. Aenean vitae lacus odio. </p>
          ',
          'list_order' => 0,
          'published' => 1,
        ]);

        DB::table('pages')->insert([
          'site_id' => 2,
          'parent_id' => 0,
          'default' => 0,
          'title' => 'Board Structure',
          'slug' => str_slug('Board Structure'),
          'body' => '
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus facilisis venenatis nulla quis gravida. Donec commodo et sem at accumsan. Pellentesque eros diam, iaculis sit amet risus sit amet, accumsan dignissim metus. Phasellus sollicitudin non enim quis mattis. Duis ornare, metus id malesuada congue, quam felis aliquet nunc, eget interdum urna eros at neque. Fusce erat ex, luctus eu eleifend tempor, aliquam sit amet mi. In dui nunc, aliquam id nunc vitae, pulvinar tincidunt augue. Cras vel augue hendrerit, lacinia neque non, consectetur massa. Nulla accumsan ac massa vehicula aliquam. Nunc sed dolor sit amet nulla cursus mollis vel eu eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras lacinia nibh quis ex laoreet blandit a sed eros. Nullam tempor massa lectus, at fringilla neque maximus nec. Proin fringilla elementum nisi efficitur finibus.</p>
            <p>Ut hendrerit molestie nibh et blandit. Sed aliquam imperdiet massa. Aliquam eleifend turpis sit amet quam ornare sagittis. Morbi pretium augue magna, id finibus elit venenatis sit amet. Proin vulputate fermentum turpis, a porttitor turpis imperdiet sed. Morbi lacinia lectus ut volutpat consectetur. Cras vulputate suscipit sem sed porta. Aenean rutrum sapien dolor, vitae sodales lectus maximus at.</p>
            <p>Aliquam erat volutpat. Sed erat ante, suscipit luctus rhoncus id, finibus ullamcorper risus. Duis quis ante ut mauris facilisis mattis. Praesent molestie est non accumsan imperdiet. Etiam eu nunc sed enim tincidunt vulputate. Duis egestas varius leo in semper. Etiam iaculis iaculis hendrerit.</p>
            <p>Cras a placerat ipsum. Maecenas ut porttitor nunc. Donec nec arcu arcu. Sed luctus lacinia eleifend. Phasellus vitae posuere lacus. Nunc non pharetra metus, ut molestie lacus. Vestibulum faucibus pharetra leo, venenatis porta nibh gravida non.</p>
            <p>Vivamus maximus blandit risus, vel gravida mi. Donec volutpat sed ex tempus egestas. Vivamus hendrerit odio ut purus gravida vulputate. Praesent accumsan quam nec magna semper tristique. Ut ut nulla et dui ornare malesuada. Donec vestibulum eget felis quis tristique. Phasellus quis quam leo. Aenean vitae lacus odio. </p>
          ',
          'list_order' => 0,
          'published' => 1,
        ]);

        DB::table('pages')->insert([
          'site_id' => 2,
          'parent_id' => 3,
          'default' => 0,
          'title' => 'Test Subpage',
          'slug' => str_slug('Test Subpage'),
          'body' => '
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus facilisis venenatis nulla quis gravida. Donec commodo et sem at accumsan. Pellentesque eros diam, iaculis sit amet risus sit amet, accumsan dignissim metus. Phasellus sollicitudin non enim quis mattis. Duis ornare, metus id malesuada congue, quam felis aliquet nunc, eget interdum urna eros at neque. Fusce erat ex, luctus eu eleifend tempor, aliquam sit amet mi. In dui nunc, aliquam id nunc vitae, pulvinar tincidunt augue. Cras vel augue hendrerit, lacinia neque non, consectetur massa. Nulla accumsan ac massa vehicula aliquam. Nunc sed dolor sit amet nulla cursus mollis vel eu eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras lacinia nibh quis ex laoreet blandit a sed eros. Nullam tempor massa lectus, at fringilla neque maximus nec. Proin fringilla elementum nisi efficitur finibus.</p>
            <p>Ut hendrerit molestie nibh et blandit. Sed aliquam imperdiet massa. Aliquam eleifend turpis sit amet quam ornare sagittis. Morbi pretium augue magna, id finibus elit venenatis sit amet. Proin vulputate fermentum turpis, a porttitor turpis imperdiet sed. Morbi lacinia lectus ut volutpat consectetur. Cras vulputate suscipit sem sed porta. Aenean rutrum sapien dolor, vitae sodales lectus maximus at.</p>
            <p>Aliquam erat volutpat. Sed erat ante, suscipit luctus rhoncus id, finibus ullamcorper risus. Duis quis ante ut mauris facilisis mattis. Praesent molestie est non accumsan imperdiet. Etiam eu nunc sed enim tincidunt vulputate. Duis egestas varius leo in semper. Etiam iaculis iaculis hendrerit.</p>
            <p>Cras a placerat ipsum. Maecenas ut porttitor nunc. Donec nec arcu arcu. Sed luctus lacinia eleifend. Phasellus vitae posuere lacus. Nunc non pharetra metus, ut molestie lacus. Vestibulum faucibus pharetra leo, venenatis porta nibh gravida non.</p>
            <p>Vivamus maximus blandit risus, vel gravida mi. Donec volutpat sed ex tempus egestas. Vivamus hendrerit odio ut purus gravida vulputate. Praesent accumsan quam nec magna semper tristique. Ut ut nulla et dui ornare malesuada. Donec vestibulum eget felis quis tristique. Phasellus quis quam leo. Aenean vitae lacus odio. </p>
          ',
          'list_order' => 0,
          'published' => 1,
        ]);
        DB::table('pages')->insert([
          'site_id' => 3,
          'parent_id' => 0,
          'default' => 1,
          'title' => 'Home',
          'slug' => 'home',
          'body' => '
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus facilisis venenatis nulla quis gravida. Donec commodo et sem at accumsan. Pellentesque eros diam, iaculis sit amet risus sit amet, accumsan dignissim metus. Phasellus sollicitudin non enim quis mattis. Duis ornare, metus id malesuada congue, quam felis aliquet nunc, eget interdum urna eros at neque. Fusce erat ex, luctus eu eleifend tempor, aliquam sit amet mi. In dui nunc, aliquam id nunc vitae, pulvinar tincidunt augue. Cras vel augue hendrerit, lacinia neque non, consectetur massa. Nulla accumsan ac massa vehicula aliquam. Nunc sed dolor sit amet nulla cursus mollis vel eu eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras lacinia nibh quis ex laoreet blandit a sed eros. Nullam tempor massa lectus, at fringilla neque maximus nec. Proin fringilla elementum nisi efficitur finibus.</p>
            <p>Ut hendrerit molestie nibh et blandit. Sed aliquam imperdiet massa. Aliquam eleifend turpis sit amet quam ornare sagittis. Morbi pretium augue magna, id finibus elit venenatis sit amet. Proin vulputate fermentum turpis, a porttitor turpis imperdiet sed. Morbi lacinia lectus ut volutpat consectetur. Cras vulputate suscipit sem sed porta. Aenean rutrum sapien dolor, vitae sodales lectus maximus at.</p>
            <p>Aliquam erat volutpat. Sed erat ante, suscipit luctus rhoncus id, finibus ullamcorper risus. Duis quis ante ut mauris facilisis mattis. Praesent molestie est non accumsan imperdiet. Etiam eu nunc sed enim tincidunt vulputate. Duis egestas varius leo in semper. Etiam iaculis iaculis hendrerit.</p>
            <p>Cras a placerat ipsum. Maecenas ut porttitor nunc. Donec nec arcu arcu. Sed luctus lacinia eleifend. Phasellus vitae posuere lacus. Nunc non pharetra metus, ut molestie lacus. Vestibulum faucibus pharetra leo, venenatis porta nibh gravida non.</p>
            <p>Vivamus maximus blandit risus, vel gravida mi. Donec volutpat sed ex tempus egestas. Vivamus hendrerit odio ut purus gravida vulputate. Praesent accumsan quam nec magna semper tristique. Ut ut nulla et dui ornare malesuada. Donec vestibulum eget felis quis tristique. Phasellus quis quam leo. Aenean vitae lacus odio. </p>
          ',
          'list_order' => 0,
          'published' => 1,
        ]);
        DB::table('pages')->insert([
          'site_id' => 4,
          'parent_id' => 0,
          'default' => 1,
          'title' => 'Home',
          'slug' => 'home',
          'body' => '
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus facilisis venenatis nulla quis gravida. Donec commodo et sem at accumsan. Pellentesque eros diam, iaculis sit amet risus sit amet, accumsan dignissim metus. Phasellus sollicitudin non enim quis mattis. Duis ornare, metus id malesuada congue, quam felis aliquet nunc, eget interdum urna eros at neque. Fusce erat ex, luctus eu eleifend tempor, aliquam sit amet mi. In dui nunc, aliquam id nunc vitae, pulvinar tincidunt augue. Cras vel augue hendrerit, lacinia neque non, consectetur massa. Nulla accumsan ac massa vehicula aliquam. Nunc sed dolor sit amet nulla cursus mollis vel eu eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras lacinia nibh quis ex laoreet blandit a sed eros. Nullam tempor massa lectus, at fringilla neque maximus nec. Proin fringilla elementum nisi efficitur finibus.</p>
            <p>Ut hendrerit molestie nibh et blandit. Sed aliquam imperdiet massa. Aliquam eleifend turpis sit amet quam ornare sagittis. Morbi pretium augue magna, id finibus elit venenatis sit amet. Proin vulputate fermentum turpis, a porttitor turpis imperdiet sed. Morbi lacinia lectus ut volutpat consectetur. Cras vulputate suscipit sem sed porta. Aenean rutrum sapien dolor, vitae sodales lectus maximus at.</p>
            <p>Aliquam erat volutpat. Sed erat ante, suscipit luctus rhoncus id, finibus ullamcorper risus. Duis quis ante ut mauris facilisis mattis. Praesent molestie est non accumsan imperdiet. Etiam eu nunc sed enim tincidunt vulputate. Duis egestas varius leo in semper. Etiam iaculis iaculis hendrerit.</p>
            <p>Cras a placerat ipsum. Maecenas ut porttitor nunc. Donec nec arcu arcu. Sed luctus lacinia eleifend. Phasellus vitae posuere lacus. Nunc non pharetra metus, ut molestie lacus. Vestibulum faucibus pharetra leo, venenatis porta nibh gravida non.</p>
            <p>Vivamus maximus blandit risus, vel gravida mi. Donec volutpat sed ex tempus egestas. Vivamus hendrerit odio ut purus gravida vulputate. Praesent accumsan quam nec magna semper tristique. Ut ut nulla et dui ornare malesuada. Donec vestibulum eget felis quis tristique. Phasellus quis quam leo. Aenean vitae lacus odio. </p>
          ',
          'list_order' => 0,
          'published' => 1,
        ]);

        // Project Sets
        DB::table('project_sets')->insert([
          'site_id' => 2, //1
          'name' => 'Funding',
          'slug' => str_slug('Funding'),
          'fiscal_cutoff_month' => 11,
          'fiscal_cutoff_add' => true,
          'application_open' => '2017-12-24 00:00:00',
          'application_deadline' => '2017-12-25 00:00:00',
          'progress_deadline' => '2017-12-25 00:00:00',
          'question_deadline' => 6,
        ]);

        DB::table('project_sets')->insert([
          'site_id' => 1, // 2
          'name' => 'Annual Grants',
          'slug' => str_slug('Annual Grants'),
          'fiscal_cutoff_month' => 3,
          'fiscal_cutoff_add' => true,
          'application_open' => '2017-12-24 00:00:00',
          'application_deadline' => '2017-12-25 00:00:00',
          'progress_deadline' => '2017-12-25 00:00:00',
          'question_deadline' => 6,
        ]);

        DB::table('project_sets')->insert([
          'site_id' => 1, // 3
          'name' => 'Mini Grants',
          'slug' => str_slug('Mini Grants'),
          'fiscal_cutoff_month' => 7,
          'fiscal_cutoff_add' => false,
          'application_open' => '2017-12-24 00:00:00',
          'application_deadline' => '2017-12-25 00:00:00',
          'progress_deadline' => '2018-1-25 00:00:00',
          'question_deadline' => 6,
        ]);

        DB::table('project_sets')->insert([
          'site_id' => 3, //4
          'name' => 'Funding',
          'slug' => str_slug('Funding'),
          'fiscal_cutoff_month' => 11,
          'fiscal_cutoff_add' => true,
          'application_open' => '2017-12-24 00:00:00',
          'application_deadline' => '2017-12-25 00:00:00',
          'progress_deadline' => '2017-12-25 00:00:00',
          'question_deadline' => 6,
        ]);

        // Funding Statuses
        DB::table('funding_statuses')->insert([
          'alias_id' => 0,
          'name' => 'Pending Review', //1
          'public' => 1,
        ]);

        DB::table('funding_statuses')->insert([
          'alias_id' => 0,
          'name' => 'Funded', //2
          'public' => 1,
        ]);

        DB::table('funding_statuses')->insert([
          'alias_id' => 2,
          'name' => 'Fully Funded', //3
          'public' => 0,
        ]);

        DB::table('funding_statuses')->insert([
          'alias_id' => 2,
          'name' => 'Partially Funded', //4
          'public' => 0,
        ]);

        DB::table('funding_statuses')->insert([
          'alias_id' => 0,
          'name' => 'Rejected', //5
          'public' => 1,
        ]);

        DB::table('funding_statuses')->insert([
          'alias_id' => 0,
          'name' => 'Request Withdrawn', //6
          'public' => 1,
        ]);

        // Initiatives
        DB::table('initiatives')->insert([
          'site_id' => 2,
          'name' => 'Access to scholarships and financial aid information', //1
          'undergraduate_order' => 1,
          'graduate_order' => 2,
        ]);

        DB::table('initiatives')->insert([
          'site_id' => 2,
          'name' => 'Expanded career-related opportunities', //2
          'undergraduate_order' => 2,
          'graduate_order' => 4,
        ]);

        DB::table('initiatives')->insert([
          'site_id' => 2,
          'name' => 'Academic support services (including tutoring, supplemental instruction, and educational planning) that teach the skills needed to be a successful student', //3
          'undergraduate_order' => 3,
          'graduate_order' => 3,
        ]);

        DB::table('initiatives')->insert([
          'site_id' => 2,
          'name' => 'Career counseling within specific academic areas', //4
          'undergraduate_order' => 4,
          'graduate_order' => 0,
        ]);

        DB::table('initiatives')->insert([
          'site_id' => 2,
          'name' => 'Career-based experiential learning and internship opportunities for juniors and seniors', //5
          'undergraduate_order' => 5,
          'graduate_order' => 0,
        ]);

        DB::table('initiatives')->insert([
          'site_id' => 2,
          'name' => 'Funding for graduate student academic travel, presentations, and professional development', //6
          'undergraduate_order' => 0,
          'graduate_order' => 1,
        ]);

        DB::table('initiatives')->insert([
          'site_id' => 2,
          'name' => 'Campus safety/security enhancements (e.g., safety program enhancements; increased security on campus; additional security at campus events)', //7
          'undergraduate_order' => 0,
          'graduate_order' => 5,
        ]);

        // Departments
        DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Academic Success and Achievement', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Access and Inclusion', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Admissions', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Arizona Student Media', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Arizona Student Unions', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'ASUA (Associated Students of the University of Arizona)',
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'BookStores', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Campus Health', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Campus Recreation', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Career Services', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Dean of Students Office', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Disability Resource Center', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Financial Aid', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Fund Development', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'GPSC (Graduate and Professional Student Council)',
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Leadership Programs', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Military Science', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'New Student Services', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Parents & Family Association', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Registrar', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Residence Life', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'SALT Center', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'SA Central Office', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'SA Marketing', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'SA Systems Group', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Testing Office', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Think Tank', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'Transfer Student Services', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'UA Facilitators', //1
        ]);
      	DB::table('departments')->insert([
          'site_id' => 2,
          'name' => 'VETS (Veterans Education & Transition Services)',
        ]);

        // student services fee data
        $this->call(SsfProjectsTableSeeder::class);
        $this->call(SsfInitiativeProjectTableSeeder::class);
        $this->call(SsfParsTableSeeder::class);
        $this->call(SsfQuestionsTableSeeder::class);
        $this->call(SsfAnswersTableSeeder::class);
        $this->call(SsfProgressReportsTableSeeder::class);
        $this->call(SsfUsersTableSeeder::class);
        $this->call(SsfSiteUserTableSeeder::class);
        
        // greenfund data
        $this->call(GfProjectsTableSeeder::class);
        $this->call(GfParsTableSeeder::class);
        $this->call(GfQuestionsTableSeeder::class);
        $this->call(GfAnswersTableSeeder::class);
        $this->call(GfProgressReportsTableSeeder::class);
        $this->call(GfUsersTableSeeder::class);
        $this->call(GfSiteUserTableSeeder::class);

        // greenfund minigrant data
        $this->call(MgProjectsTableSeeder::class);
        $this->call(MgParsTableSeeder::class);
        $this->call(MgQuestionsTableSeeder::class);
        $this->call(MgAnswersTableSeeder::class);
        $this->call(MgProgressReportsTableSeeder::class);

        // freshman fee data
        $this->call(FfProjectsTableSeeder::class);
        $this->call(FfQuestionsTableSeeder::class);
        $this->call(FfAnswersTableSeeder::class);
        $this->call(FfProgressReportsTableSeeder::class);
        $this->call(FfUsersTableSeeder::class);
        $this->call(FfSiteUserTableSeeder::class);
    }
}
