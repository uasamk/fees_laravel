<?php

use Illuminate\Database\Seeder;

class SsfAnswersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('answers')->delete();
        
        \DB::table('answers')->insert(array (
            0 => 
            array (
                'answer_text' => 'No it will not include restoring the usual operating hours to Campus Rec. We didn\'t put that in as part of our request. I would be willing to add those costs if you would consider that as part of this request. Let me know and I can get you the figures quickly.',
                'created_at' => '2010-01-25 21:43:04.000000',
                'question_id' => 14,
            ),
            1 => 
            array (
                'answer_text' => 'The support we are requesting is only for the student staff.  There are additional staff for the service in addition to operational expenses, cart upkeep etc.  The overall budget is approximately $120,000 per year.',
                'created_at' => '2010-01-25 21:45:00.000000',
                'question_id' => 8,
            ),
            2 => 
            array (
                'answer_text' => 'Yes, they have been maintaining the program and its amazing growth with limited support from ASUA for over a decade.  Given budget constraints, we are hoping to utilize the fee to allow the program to meet campus need without reflecting a cost onto PTS users.',
                'created_at' => '2010-01-25 21:48:19.000000',
                'question_id' => 9,
            ),
            3 => 
            array (
                'answer_text' => 'The Career Ambassadors will be at tables on the mall and at their colleges to: answer questions students may have about the services provided by Career Services, talk about an upcoming program or event, refer students to specific services at Career Services such as counseling, resume checks, presentations, campus interviewing etc.
They will serve in focus groups. We have one in the planning stages about what types of marketing and in what formats students pay attention to.
They will make announcements in classes and club meetings about special events or services through Career Services, such as career fairs, mock interviews, campus interviewing.
They will assist Career Services staff with special events such as career fairs.',
                'created_at' => '2010-01-25 22:04:59.000000',
                'question_id' => 20,
            ),
            4 => 
            array (
            'answer_text' => 'Yes, student staff from the Rec center working closely and collaboratively wit the SA marketing web team will jointly manage and maintain the new site, absorbing staff costs as well as the investment necessary for new web infrastructure (web server, database software, etc.). No additional SSF monies are required for maintenance and upkeep of the new site.',
                'created_at' => '2010-01-25 22:11:47.000000',
                'question_id' => 16,
            ),
            5 => 
            array (
                'answer_text' => 'No, they are all distinctly different. Both Career Serach and Vault have employer information, but what they have is different. Career Search is a searchable database of organizations with information about what the organizations does and key employees or board members. Vault has broad information about industires and "insider" information about what it is like to work at an organization. Vault has some in-depth information on a few companies. Career Search allows users to search for the type of orgnaization that interests them in the geographic location that interests them, for example all the magazine publishers in Dalla, TX.',
                'created_at' => '2010-01-25 22:13:30.000000',
                'question_id' => 24,
            ),
            6 => 
            array (
            'answer_text' => 'Thank you for considering the proposal.  Please find the curriculum specifics for the educational (ADAPT) component of the proposal.  

-----------------------------

Alcohol Decisions and Prevention Training (ADAPT) 

Introduce program goals 

Decisional Balance:  ( put up slide)
•	Good Things/Not So Good Things of Alcohol Use – initiate moderation tips (ask students to list ideas)

Potential Discussion Topics related to Decisional Balance: 
1.	Reasons NOT to change (pro), Reasons TO change (con)
2.	Weigh the costs and benefits of actions
3.	Readiness ruler:  How ready are you to change?  (1=not ready, 5=unsure, 10=ready)   How confident are you that you can change?
4.	Where you are now and where would you like to be? 

Administer AUDIT- Alcohol Use Disorders Identification Test (put up slide for scoring while students are completing questionnaire)
•	Give scoring interpretation
•	UA norms/perceptions  (use normative clicker slides)
•	How do you compare to other UA students?

Identify Risks  (have students compile list)- How does this fit with your personal values?

Do Continuum of Use…Abuse…Dependence (invite students to provide definitions)


Step-Up section (brief format, 20-30 minutes max, put up slides as you go)


Encourage creation of personal goal statement or change plan

•	Invite students to monitor drinking for 2 week period',
                'created_at' => '2010-01-25 22:19:14.000000',
                'question_id' => 13,
            ),
            7 => 
            array (
                'answer_text' => 'In the past years, ASUA has maintained a 100,000 dollar funding account which has always been exhausted by the Spring semester or before. Due to the additional funding contribution from the student services fee, ASUA maintained a 150,000 funding account for club funding this year. It has been able to market club funding and has seen a surplus in the amount of clubs funded as a result of the additional funds.',
                'created_at' => '2010-01-25 23:02:12.000000',
                'question_id' => 3,
            ),
            8 => 
            array (
                'answer_text' => 'We hope to make this Alcohol Risk Reduction Program mandatory down the road. Although this program will have a significant impact in reducing risk associated with student alcohol use at the UA, the staffing level requested in this proposal is not sufficient to mandate the program for all high-risk students.

Campus Health is currently working with departments throughout Student Affairs to recruit participants for both ADAPT and BASICS.  Residence Life has agreed to expand ADAPT sessions to Residence Halls this spring as part of their ongoing health and safety programming.  Last semester, over 500 students participated in an ADAPT pilot project, among them freshmen, fraternity/sorority members, and on-campus residents.  During the Fall 2010, we propose that mandated, first wing meetings in the halls include ADAPT.  We are also working with CSIL, Fraternity and Sorority Programs and Greek Chapter Presidents to recruit fraternity and sorority members for BASICS.  The Hunter White Health Advocates are another group who also plans to support this program by recruiting student participants.  Finally, other programming with less supporting evidence of effectiveness (e.g. speakers, seminars, etc.) will be replaced with ADAPT and BASICS, resulting in better student health outcomes without imposing  additional time demands on students.

Thank you for your consideration of this proposal.',
                'created_at' => '2010-01-25 23:06:11.000000',
                'question_id' => 12,
            ),
            9 => 
            array (
                'answer_text' => 'This varies from day to day, depending on the time of year.  Since the Next Steps Center focuses on the enrollment side of Admissions, we begin seeing students in early October - August.  

In the fall, 20 students/families on average per day.  The number increases each month.  In the spring, we will see more students, averaging 25-35 per day.  Again, the numbers increase as we get closer to summer.  In the summer, the Next Steps Center will be the Information Center during New Student Orientation so we will see hundreds per day all through June.  In July, we will continue to see an average of about 50+ per day, mainly transfer students.  Then in August, we will see about 50+ per day on non-Orientation days and hundreds on Orientation days. 

Thank you so much for your consideration!
Kasey Urquidez',
                'created_at' => '2010-01-26 15:51:16.000000',
                'question_id' => 2,
            ),
            10 => 
            array (
            'answer_text' => 'If monies were not available, we would continue to employ the students we have now in the Call Center, approximately 18.  At this time, the Call Center students are not able to cover all hours necessary to operate the full functions required for the Next Steps Center and many prospective students/families are put on hold, have to wait an extra day for a return email or cannot get an operator at the moment they wish for instant messaging (taking away the value of the services we are trying to offer). Currently, we can only pay minimum wage which disadvantages us as other departments often pay more - meaning we risk losing student staff easily as well as our invested training time.  This leads to less support for prospective students and more strain on our current student workers. 

Quick response:  Keep the same staff we have now, approximately 18 students, at $7.25 per hour (minimum wage). 

Thank you for your consideration, 
Kasey Urquidez',
                'created_at' => '2010-01-26 15:56:42.000000',
                'question_id' => 1,
            ),
            11 => 
            array (
                'answer_text' => 'The outreach and counseling are intended to serve all students. Career Services is well utilized by international and graduate students. For example, over the last fiscal year 34% of the students who came for counseling were international students and 13% were graduate students, with 52% of the graduate students being international. 27% of the students who came to presentations were international students and 9% were graduate students with 65% of the graduate students being international. 30% of students using online and in-person career resources were international and 20% were graduate students, with 49% of the graduate students being international.

The technology that the Student Technology Assistant would be involved in developing will be designed with all student audiences in mind.',
                'created_at' => '2010-01-27 20:14:17.000000',
                'question_id' => 21,
            ),
            12 => 
            array (
                'answer_text' => 'These packages are immensely useful to the job seeker in finding out information about potential employers, industries and job openings, however our "placement" data is self-reported and cannot accurately reflect this information.',
                'created_at' => '2010-01-27 20:20:20.000000',
                'question_id' => 23,
            ),
            13 => 
            array (
                'answer_text' => '1. NACELink Wildcat eFolio
2. Career Search
3. Vault
4. Going Global
5. DISCOVER
6. Internships.com',
                'created_at' => '2010-01-27 20:22:23.000000',
                'question_id' => 22,
            ),
            14 => 
            array (
                'answer_text' => 'The following grading rubric is listed in order of importance:

1) Financial need which will be assessed through the interview process. One particular question that we will be asking in the interview is how much outside funding the applicant is receiving to support their educational needs. 
2) Grades
3) Community Service
4) Extracurricular Activities

Contact one of us if you have any further questions

Stephen Wallace & Leo Yamaguchi',
'created_at' => '2010-01-28 01:07:08.000000',
'question_id' => 11,
),
15 => 
array (
'answer_text' => 'Wait times for clients of the Disabled Cart Service will vary depending on the type of trip request, time of day and for registered and non-registered clients.



·      Ninety Nine Percent (99%) of the trips provided by the Disabled Cart Service (15,161 Annual Trips) are scheduled trips for pre-qualified students and staff. During “Peak Service,” (class change times) wait times will usually run from 0 to 15 minutes depending on the number of pickups that a driver has been assigned. During “Off Peak,” wait times may vary from zero to 10 minutes for scheduled trips and 0 to 15 minutes for client trips that are inserted into the schedule (real time scheduling) for registered clients.     

·      Approximately 1% of Disabled Cart Service trips are provided to non-registered clients on request of the Disability Resource Center and Student Health Service for persons requesting assistance or persons in the process of registering for the Disabled cart service. Student & staff drivers of the Disabled Cart Service also provide courtesy rides to disabled visitors encountered on campus, disabled students navigating the Campus unaware of the Disabled Cart Service and elderly visitors to Campus (all events) as capacity and schedules permit. Wait times will vary but will usually be minimal and occur at any time of day or at any Campus event.',
'created_at' => '2010-01-28 05:46:59.000000',
'question_id' => 7,
),
16 => 
array (
'answer_text' => 'At the current time, ASUA is ever expanding its programs to meet the need of our student body.  Each operational budget within our department is strained and while we remain committed to the advocacy, programs, and service we offer,as new programs such as Sustainability become a priority we continue to stretch the same dollar amount to go further.  

Parking & Transportation Services is an auxiliary unit at the University.  Decisions regarding the creation, continuance or expansion of programs is dependant primarily upon revenue generated from parking permits.  Continued support of the Disabled Cart Service could require some serious evaluation by PTS of other PTS services that do not generate revenue.  Alternatively, current services may need to be reduced to support the Disabled Cart Service.  Some specific programs/services that could be adversely impacted are; the subsidy allotment for the Sun Tran U-Pass program, alternative transportation programs including the Cat Tran service and bicycle enhancement/safety programs.  It is possible revenue enhancements could need to be implemented, such as increased parking permit rates, to support programs.',
'created_at' => '2010-01-28 05:55:32.000000',
'question_id' => 7,
),
17 => 
array (
'answer_text' => 'This request is to cover only the student employee costs associated with the Disabled Cart Service.  The program itself has an overall operational cost of approximately $120,000 annually which includes student staff, full time staff, and cart maintenance.',
'created_at' => '2010-01-28 06:02:02.000000',
'question_id' => 7,
),
18 => 
array (
'answer_text' => 'Yes. This request is being made on behalf of both ASUA and PTS to secure the maintenance and growth of the program to meet the demand that is increasing annually.   Both departments are facing budget constraints which threaten the quality of our programs and services.  The cart services is essential in making our campus accessible and we are committed to continuity.',
'created_at' => '2010-01-28 06:11:20.000000',
'question_id' => 7,
),
19 => 
array (
'answer_text' => 'From technical side, we can use NetID to verify a registrant\'s UA status and
identity before allowing them to reserve equipment or courts. We would also
be able to keep a running total of reservations for specific resources to
avoid overbooking.

On the Rec Center side, validation could be handled in a number of ways. One
option, if necessary, would be to allow for printing a \'ticket\' or \'pass\' to
physically present to Rec Center staff. Or, Rec Center staff could have
access to a password-protected area of the website to easily view/validate
registrations.

It is important to note that beyond the technical tools to allow
registration and the subsequent validation, the responsibility of on-site
enforcement of scheduled equipment use would ultimately fall to Rec Center
staff.',
'created_at' => '2010-01-28 20:56:11.000000',
'question_id' => 17,
),
20 => 
array (
'answer_text' => 'Based on anecdotal input from students, it is common to experience wait times during the high traffic periods of the day that are long enough for students to avoid using the Rec Center at these time. Court times especially, may require long waits--sometimes over an hour. We do not yet know the impact the Rec Center\'s expansion will have on wait times and attendance.',
'created_at' => '2010-01-28 20:56:56.000000',
'question_id' => 18,
),
21 => 
array (
'answer_text' => 'We do not readily have access to specific data. However, in reviewing just a few Rec Center sites at other universities, it\'s clear that our Rec Center site is not competitive by today\'s standards. Although the sites below or not perfect, they are pushing ahead in terms of online registrations, reservations, live webcam views and events calendars.

Stanford
http://www.stanford.edu/dept/pe/cgi-bin/
- Online registration for Rec classes, personal training
- Online events calendar

https://www.stanford.edu/dept/pe/cgi-bin/facilities/facility-reservations/
- Online squash and tennis court registrations

UCLA
http://www.recreation.ucla.edu/
- Online Rec class registration

UMD
http://www.crs.umd.edu/
- Web cams
- Court reservations

UK
http://www.earthcam.com/clients/uky/
- Webcam',
'created_at' => '2010-01-28 20:57:33.000000',
'question_id' => 19,
),
22 => 
array (
'answer_text' => 'If the Student Services Fee monies were not available the size and scope of the student employee group in Campus Recreation would be reduced by 21%.',
'created_at' => '2010-02-02 20:58:37.000000',
'question_id' => 30,
),
23 => 
array (
'answer_text' => '$27500.00 is the correct amount requested.',
'created_at' => '2010-02-02 21:58:54.000000',
'question_id' => 29,
),
24 => 
array (
'answer_text' => 'Scholarships would be administered through an application process that focuses on financial need of the applicant. Availability of scholarships would be advertised through listserv messages, on websites, and as announcements in meetings, classes, etc. to reach the greatest amount of people. Applications will be reviewed by the programming staff who oversee each program to determine which applicants demonstrate the greatest financial need and award the scholarships accordingly.',
'created_at' => '2010-02-02 23:12:31.000000',
'question_id' => 35,
),
25 => 
array (
'answer_text' => 'The $825.00 from the Hunter White Foundation will only be used to cover TIPS training if the Student Fee money is not awarded. Our staff feels strongly enough about the TIPS training that we will pull the money from the Hunter White Foundation if needed, otherwise the $825.00 will be allocated towards other wellness trainings such as CPR certification.',
'created_at' => '2010-02-02 23:38:47.000000',
'question_id' => 44,
),
26 => 
array (
'answer_text' => 'TIPS training is the only training that students have access to currently without having to pay for the training or without sanction. TIPS training does cover basic information such as, standard drink size, BAC and other common alcohol education topics. However, TIPS training uses this as the foundation for the more practical portion of the training which is related to intervention techniques. 

Participants who are TIPS trained complete a survey at the end of the training which asks them "How does this program (TIPS) compare to other alcohol education you\'ve received?" The responses from our most recent training of 36 students were as follows: 24 (Better) 8 (Same) 1 (worse) 3 (Have not had alcohol education program before).',
'created_at' => '2010-02-02 23:49:29.000000',
'question_id' => 45,
),
27 => 
array (
'answer_text' => 'For FY 09-10, ASUA allocated a $2,000 budget for Sustainability programming. This was raised from $1,000 which was our budget for FY08-09. To my knowledge, ASUA has no current plans to raise the Sustainability budget for upcoming years.',
'created_at' => '2010-02-03 01:14:26.000000',
'question_id' => 27,
),
28 => 
array (
'answer_text' => 'Several programs are currently being developed, to which program do you refer (Compost Go-Live, Solar Dorm Initiative etc.)?

The Education and Outreach components benefit the interns assigned to developing these aspects of the Sustainability program, as well as the entire student body. For example, education components include events outside Earth Day, such as the eco-friendly fashion show we will be hosting in April, lecture series and film series. This benefits interns because it provides them with hands-on learning opportunities associated with large scale education and event planning. This benefits the student body by providing additional and alternative means of education in the realm of environmental sustainability. The primary component of the Outreach program is working with and forming partnerships with the broader Tucson community. Current partnerships include working with many junior high and high schools in the community and assuming the role of guest speaker in classrooms, introducing younger generations to environmental sustainability and the various opportunities at the University of Arizona. Interns work to develop a unique sustainability curriculum and learn important communication skills while working with younger students. While this Outreach component may not directly impact the entire student body per se, it is important to continue to develop our relationship as the students of the University of Arizona with the broader community.',
'created_at' => '2010-02-03 01:33:42.000000',
'question_id' => 26,
),
29 => 
array (
'answer_text' => 'GIS is Geographic Information Systems and in the most basic sense, is a type of mapping and modeling technology used across several disciplines. Further, in depth information can be found at www.gis.com

IT equipment would consist of low server grade computers and related software (such as database management systems). This equipment will provide the means by which to store data, create multi-layered maps and manage websites, all of which are necessary components vital to initiative success.',
'created_at' => '2010-02-03 01:40:28.000000',
'question_id' => 25,
),
30 => 
array (
'answer_text' => 'We are looking to secure funding from outside sources as well as from the Student Services Fee. We have already raised $1000 for our trip in March. We were required to submit our entire budget, including that which we were not intending to fund through the Student Services Fee.

Thank you,
Benjamin Kochman
bkochman@email.arizona.edu',
'created_at' => '2010-02-03 05:06:48.000000',
'question_id' => 51,
),
31 => 
array (
'answer_text' => 'Parking & Transportation Services is an auxiliary unit at the University.  Decisions regarding the creation, continuance or expansion of programs is dependant primarily upon revenue generated from permits.  Continued support of the Service could require some serious evaluation by PTS of other PTS services that do not generate revenue.  Alternatively, current services may need to be reduced to support the Service.  Some specific programs/services that could be adversely impacted are; the subsidy allotment for the Sun Tran U-Pass program, alternative transportation programs including the Cat Tran service and bicycle enhancement/safety programs.  It is possible revenue enhancements could need to be implemented, such as increased parking permit rates, to support programs.  
There are many programs that continue to grow within ASUA. While our programs will not be cut as they are secured in our bylaws, the overall stretching of our budget constricts the growth of our programs to meet the demand evident on our campus.  With each new generation of students, new initiatives and needs will arise and it is the responsibility of ASUA to be able to address and commit to these developing needs. With student job opportunities being a high priority, we want to take advantage of  supplementing the student staffing of the cart service with fee dollars to allow ASUA to ready itself for growth of current programs and those on the horizon.',
'created_at' => '2010-02-03 18:27:04.000000',
'question_id' => 28,
),
32 => 
array (
'answer_text' => 'No. TIPS is a training that has been used for some organizational sanctioning by the Dean of Student\'s Office, but any organizations that are sanctioned must pay for TIPS. The student fee money would only cover TIPS for students who are not sanctioned.',
'created_at' => '2010-02-03 20:28:15.000000',
'question_id' => 46,
),
33 => 
array (
'answer_text' => 'No. However, Student Services Fee monies are being requested to re-certify the currently certified trainers. Trainer certification is $225 and recertification is only $75 so it much more cost effective to spend the money each year for current trainers to recertify, rather than to let it expire and have to be trained again for $225.',
'created_at' => '2010-02-03 20:30:36.000000',
'question_id' => 47,
),
34 => 
array (
'answer_text' => 'Student Services Fee Committee:

Thank you for the opportunity to answer this question and clarify the information. The total annual expenses for this proposal is $62,982, as you noted. The total provided in the proposal ($53,442) was incorrect. My apologies for the incorrect information.

Sincerely,
Carol Thompson',
'created_at' => '2010-02-05 16:24:51.000000',
'question_id' => 52,
),
35 => 
array (
'answer_text' => 'Dear Student Services Fee Committee:

Thank you for the opportunity to answer this question. We have been working on a number of different levels to identify additional resources that could be sources of new dollars. Our priority is to identify reliable and adequate funds while simultaneously improving how we serve students.

Private Support:  
We have worked with the Student Affairs Director of Development Lonelle Rathje to be included in the development initiatives for the division. As a part of this planning process, we have been involved in identifying prospective donors and developing plans for engaging those prospective donors to determine interest and ability to give. 

Grants: 
We have been researching grant opportunities in the following areas and have identified one to date that we may pursue pending additional information on our eligibility: 
o	Substance Abuse and Mental Health Services Administration
o	Department of Education
o	Office of Victims of Crime 
o	Office on Violence Against Women 

Parents & Family Association Grants: PFA typically does not provide grant monies for positions, but will support operational and programmatic needs. We will be submitting a request once the Call for Proposals is announced.

First Year Student Fee: Once the funding criteria are known for the Freshmen Fee monies, we will review the criteria to determine if a proposal to support the Student Assistance and Advocacy area needs would be appropriate.',
'created_at' => '2010-02-05 16:59:58.000000',
'question_id' => 53,
),
36 => 
array (
'answer_text' => 'Thank you for the opportunity to address any questions you have about the proposal. All students found responsible for violating the Student Code of Conduct are assessed a monetary sanction of $80. Those students assigned to the personal responsibility workshop, as an educational sanction, will be assessed an additional $80 which is less than what they would pay if referred off campus (on average $100).',
'created_at' => '2010-02-05 19:31:14.000000',
'question_id' => 55,
),
37 => 
array (
'answer_text' => 'Both workshops are intended for two very distinct populations and address two  distinct skill sets. The academic integrity process and referral to the ethics workshop is the responsibility of faculty following sanctioning guidelines defined in the academic integrity process. Students sanctioned to the personal responsibility workshop have violated the Student Code of Conduct and are imposed this educational sanction for behavioral issues. The ethics workshop focuses on building academic skills to reduce plagiarism and cheating. The personal responsibility workshop focuses on helping students recognize errors in thinking to reduce poor choices that lead to at risk behaviors.

Specific obstacles related to combining the workshops consist of:
The personal responsibility workshops are structured into two 2-hour sessions
The ethics workshops are structured into 5 individual sessions
The topics are not interchangeable 
Reducing staffing will reduce the number of workshops and variety of workshop  times available to students
Reducing staffing will reduce paraprofessional opportunities for graduate 
students to participate in teaching activities
Reducing staffing increases the workload of graduate students who are 
responsible for preparing, marketing, teaching, reviewing student journals, and 
assessing the workshops

For the 2010 – 2011 academic year we plan to hold 22 workshops (6 on weekends) making outreach to 550 students.',
'created_at' => '2010-02-06 00:22:01.000000',
'question_id' => 56,
),
38 => 
array (
'answer_text' => 'Historical data from the 2005-2006 to the 2008-2009 academic years, reveals the number of academic integrity violations committed by international students increased from 28 to 56 cases, with plagiarism comprising the majority of the violations.  What is contributing to this increase?  According to research conducted by university teaching centers, the educational systems and cultures of other countries differ from the USA in key ways, including 1) plagiarism – the Western concept of “ownership” of ideas may not exist, 2) interdependent culture is one where students collaborate or help each other with academic success vs. the individualistic Western culture, and 3) simply not understanding academic integrity policies.  Our continued partnership with the Office of International Affairs is important to address this issue.
One-third of the Fall 2009 semester Academic Integrity Workshop participants were graduate and/or professional students.  Those students shared, anecdotally, that they learned more about proper citation, paraphrasing, etc., in our workshop than in their entire academic career.
From an employment and professional development perspective, we wish to provide two graduate students with an opportunity to provide students with critical academic support, proactive educational outreach, and the enhancement of their coordination, teaching and curriculum development skills via a graduate assistantship in the Dean of Students Office.',
'created_at' => '2010-02-06 01:26:34.000000',
'question_id' => 54,
),
39 => 
array (
'answer_text' => 'Our plans to identify and secure on-going funding go hand in hand with our
commitment to embrace relationship building and cultivate communication that will advance the Initiatives\' goals.  We are committed to building a
network of allies and partners, and to identifying activities
and events that will bring together a broader coalition of University
departments, student organizations, and local and national groups.  Our
strategies to secure funding to sustain the programs include:

-Pursuing outside sources such as corporate sponsorships and
private support.  Also, opportunities to apply for grants through private
foundations, and federal, state and local organizations will be sought.

-Encouraging units that are currently putting on one-time events for a small
number of people, to collaborate and pool resources to expand the reach of
programs.  This increased collaboration will allow units to leverage
existing resources and to work together to find additional funding.

-Offering seed money to departments and organizations that might not have
previously considered social justice education and cultural awareness
programming.  This initial investment would allow units to pilot ideas which
can lead to more permanent self-sustained efforts.

-Utilizing a "train the trainers" model to ensure efforts are sustained.  An
initial investment will allow an intergroup dialogue infrastructure to be
developed to ensure programming occurs beyond one academic year.',
'created_at' => '2010-02-06 18:06:02.000000',
'question_id' => 57,
),
40 => 
array (
'answer_text' => 'The staff that did participate on the committee was optimistic about where the committee would go in the future.  Staff effected by YOUnion Aid initiatives and projects (such as Renew the U) gave extremely positive feedback.  Staff has felt that events such as Renew the U (the clean up initiative) help raise questions about what sustainability is (beyond going green).  The Union as a whole is promoting sustainability, in all facets (social, environmental and economic) and as a result the feedback is positive that  a group exists, committed to promoting these efforts as well as making people aware of sustainability on basic and more abstract levels.  Most importantly, positive feedback exists having seen the beginnings of a committee form and the potential this has for future growth.',
'created_at' => '2010-02-09 20:05:47.000000',
'question_id' => 79,
),
41 => 
array (
'answer_text' => 'The Professional Internship Program does not currently have a director, and this creates a sense of disunity within the program.  Students are recommended by their supervisors, who guide them in their management positions after the class, and Union senior managers teach the academic portion of the course but have no further involvement beyond this.  We have formed a committee of undergraduate students to serve as mentors throughout the PIP experience, but there is currently no overall guiding force providing direction to the program and its participants.  Hiring a graduate assistant (preferably a student with experience in programming) would provide the program with dedicated direction, a quality it currently lacks and which prevents opportunities for expansion and improvement.',
'created_at' => '2010-02-10 17:59:53.000000',
'question_id' => 80,
),
42 => 
array (
'answer_text' => 'Students who have successfully completed the PIP program tend to remain in their management positions throughout their college careers, and former PIP students have moved on to a variety of postgraduate opportunities, including Teach for America and graduate school.  PIP students tend to maintain a high degree of commitment to their work units, increasing their involvement with the University on a professional and social level, as well as encouraging academic progress (full time enrollment in good standing is a condition of the PIP program and employment at the Unions).  Focus group sessions with current and former PIP students have revealed positive feedback about class material and professional experience, but also negative feedback about the lack of direction within the program itself, which is an issue we are attempting to address through seeking a graduate assistant director position.  Establishing a position in charge of the program would create more opportunities to do solid research on PIP students and their progress during college and after graduation.  In terms of full time and administrative stakeholders in the program, PIP saved the Student Unions $58,000 in employee related expenses during FY08/09.',
'created_at' => '2010-02-10 18:01:05.000000',
'question_id' => 81,
),
43 => 
array (
'answer_text' => 'The repemption estimates for 2010/11 were determined by evaluated the Fall 2009 actual participation numbers and increasing the equivalent days in year 2010 slightly to allow for the increased cappacity of the soon to be remodeled locations.

The Spring 2011 estimate was done the same way but was compared to the Spring 2009 actuals.

To date our record day is 3,830 particpants on December 2, 2009. This number represents a maximum logistical number of customers we can process at this time.

Regards
DCG',
'created_at' => '2010-02-10 22:34:15.000000',
'question_id' => 78,
),
44 => 
array (
'answer_text' => 'Highest priority is student wages at $362,000 with associated ERE of $7,200 for a total of $369,200.
The student wages impact the most students directly.  The wages pay for approximately 125 student employees who have direct contact with students using the Think Tank services. 
Of the student services provided, the priority ranking is:
1. Nugent services
2. Recreation Center services
3. Park Student Union services

The fourth priority is graduate student assistantships at $95,000 with associated ERE of $34,800 for a total of $129,800.  The graduate assistants supervise the sites and provide direct student services.',
'created_at' => '2010-02-10 22:54:55.000000',
'question_id' => 82,
),
45 => 
array (
'answer_text' => 'The Student Services Fee does not currently support any of the listed graduate assistant positions.  Some of these positions are already existent and some are new positions.  Please reference the list below:

Currently exist:
1 Arizona Blue Chip Program GA
Olympian Wellness Program GA
Gallagher Theater GA
Union Galleries GA

New Positions:
1 Arizona Blue Chip Program GA
Campus Activities GA
National Collegiate Leadership Conference GA
Aristotle Academic Program GA
ATLAS Program GA',
'created_at' => '2010-02-11 16:52:40.000000',
'question_id' => 32,
),
46 => 
array (
'answer_text' => 'Not having student services funding for these graduate assistant positions could have numerous effects on students and programs:
- Decrease the number of graduate assistant positions available to incoming and current graduate students.  
- Through these positions we would be able to provide even more direct, hands-on advising to individual students and groups.  So while these programs would not be removed if funding were not awarded, having these positions would improve the advisement and support students receive.
- Programs such as Campus Activities, NCLC, ATLAS, Union Galleries, and Gallagher Theater serve a campus-wide population.  Having these graduate assistant positions would continue to increase the quality of campus-wide programming for all students.
- While the Blue Chip, Aristotle, and Olympian positions would serve a specific population, the number of students they would work with comprises 14% of the entire campus population.  The effects of their advisement and education on leadership, academic skills, and healthy lifestyle choices can have a ripple effect on the students\' peers.',
'created_at' => '2010-02-11 16:59:31.000000',
'question_id' => 31,
),
47 => 
array (
'answer_text' => 'Some of the programs included in this proposal are offered in various areas on campus.  The purpose of linking these programs with ATLAS are intended to create a holistic wellness series and engage students in education around wellness.  If awarded this funding, we would work with campus departments that may have these services (ex. campus recreation for CPR instruction, etc.).  However, as we know from our students, it is often difficult to navigate the university system to know what resources are available to you.  This program would be a packaged wellness series for students that can link them with not only leadership education, but wellness resources throughout campus.',
'created_at' => '2010-02-11 18:01:58.000000',
'question_id' => 61,
),
48 => 
array (
'answer_text' => 'There are both incentives and subsidies included in this proposal.  Below are examples of each and background on why this would be beneficial to the program:

Subsidies:
The funding from the Student Fee would allow us to offer the programs listed (CPR Certification, Nutritionist, Self-Defense) at a reduced cost to the student.  For example, the average cost of the First Aid/ CPR Program with a Red Cross instructor is $45-$55 per person.  If funding were awarded, we would like to offer the training to students for only $10, if not free of charge.  This type of subsidy can make this type of training and wellness education to students who could not otherwise afford these types of programs.

Incentives:
While the subsidies listed above are for any individual student interested in this type of education or certification, the incentives are targeted toward club and organization leaders.  With clubs and organizations impacting over 20,000 students on campus, educating club leaders in wellness, healthy lifestyle choices, and emergency response can have a large affect on our campus population.  These students are in positions of responsibility with their organizations and they can also set an example for their club participants.  The incentives outlined in this proposal are intended to be used as encouragement and motivation for club leaders to complete this program.',
'created_at' => '2010-02-11 18:18:52.000000',
'question_id' => 62,
),
49 => 
array (
'answer_text' => 'The education offered in this series allows student leaders an opportunity to be better stewards of their own and their peers\' wellness.  As students, these leaders face academic pressure, time challenges, alcohol and drug use, eating disorders, violence, and more.  These types of programs can support the students college experience and allow them to be proactive in creating a successful environment for themselves and others they impact.  Club leaders have taken advantage of the workshops we already offer and we believe that this program would offer valuable resources that students may not know of or seek outside of this structure.',
'created_at' => '2010-02-11 18:29:51.000000',
'question_id' => 63,
),
50 => 
array (
'answer_text' => 'We have not determined the specific individual to be placed in this position.  In our discussions with Campus Health this may be someone from their staff or may need to be a contracted nutritionist off-campus.  Campus Health has said they already see a large number of students and would work with us to make appropriate staffing decisions for having someone located in the Union throughout the week if we are awarded this funding.',
'created_at' => '2010-02-11 18:32:36.000000',
'question_id' => 64,
),
51 => 
array (
'answer_text' => 'This was an error in accounting the total number - the incentives should listed as $3750.00.',
'created_at' => '2010-02-11 18:33:53.000000',
'question_id' => 65,
),
52 => 
array (
'answer_text' => 'As seen in the SSSP’s December 2009 Progress report, a considerable amount of start up expenses (office space, computer, and miscellaneous operational expenses) were covered by the Office of Scholarships and Financial Aid. As a result, none of the items from the budget are one-time expenses. 

While no items from the budget are one-time expenses needing funding for only the first year, less funds should be needed for database development and interface design after the first year as much of the overall design and development should be completed. However the SSSP would need continuing funding to pay for support in maintaining and refreshing the existing SSSP interface. 

Since the exact amount needed in year 2 is not yet known, we propose that the line items for Year 2 of Database Development and Interface Design be based upon estimates that will be provided with our December 2010 progress report. At that time we will have a much better idea of how much ongoing support is necessary to maintain the success of the project. As always, any unused funds will be returned to the Board.',
'created_at' => '2010-02-11 22:06:24.000000',
'question_id' => 73,
),
53 => 
array (
'answer_text' => 'The “Database Development” line item is funding to pay a web developer/programmer for approximately 200 hours of work throughout the course of the year. 

The web developer/programmer is the architect of the SSSP interface system. This person develops all of the logic and coding behind pulling student data, matching student information to scholarships, creating user access, and reporting. This person is an essential player in developing a streamlined scholarship selection system.',
'created_at' => '2010-02-11 22:08:14.000000',
'question_id' => 73,
),
54 => 
array (
'answer_text' => 'The budget is incorrect because of bad formula in the spreadsheet. The correct budget amounts should be:

$340,000 Printing Expense
$240,000 Income from Ad Sales
$100,000 Fee Subscription Subsidy

We apologize for taking up more of the committee\'s time because of this mistake.
Thank you for following up.',
'created_at' => '2010-02-11 23:46:42.000000',
'question_id' => 85,
),
55 => 
array (
'answer_text' => 'Program Coordinators, Sr.: 

Advise student/families in person and on phone about financial aid eligibility, application procedures, and aid program, cost indebtedness, money management and financial planning and individualizes information to the particular needs and situations of the students and families. 

Review awards to ensure compliance with government rules and regulations, looking for over awards, under awards, disbursements, ISIR’s errors, non-degree seeking students, satisfactory academic appeals, changes to the student’s resources and budgets.

Collect and /or analyzes financial data on students to determine aid eligibility and makes revised awards.

Make recommendation/referrals to SALT and Think Tank for students not meeting satisfactory academic progress as applicable. 

Assist in developing communications and system tools to finds and rescue students in financial needs who may or may not contact our office. 


Participate in outreach activities 


Operation Rescue Student Workers: Team leads 

Duties of student workers include but not limited to:

Responsible for answering inquires from direct phone, counter and email contact.

Responsible for direct initial phone contact to students and families we believe are at risk and need our assistance. 

Responsible for receiving, coding, tracking and quality control of all documents submitted for Operation Rescue. 


Participate in outreach activities',
'created_at' => '2010-02-12 01:00:22.000000',
'question_id' => 69,
),
56 => 
array (
'answer_text' => 'The current salary of the Assistant Director who would be responsible for Operation Rescue is $54,596 with a projected $64,596 depending on Operation Rescue funding.

The current salaries of Associate Directors in OSFA range from $65,000 to $70,000.
Like all salary request in Operation Rescue, the $14,000 includes both salary and Employee Related Expenses (ERE) expenses.',
'created_at' => '2010-02-12 01:06:36.000000',
'question_id' => 70,
),
57 => 
array (
'answer_text' => 'We must hire graduate or undergraduate students with high level skills.

Operation Rescue by its nature requires staff with the highest level of advanced Financial Aid knowledge and experience.    
Operation Rescue Student Team Leads:
Must have advanced experience in answering phones, emails or in person inquiries
Must have advance financial aid troubleshooting skills--- be able to take information or ask students question to determine a student’s real issue. 
Must be able to evaluate financial aid documents 
Must be experience in electronic document management and coding
Must be detailed and accurate
Must be confident in a one on one communication settings
Must have excellent listening skills 
Must have good communication skills
Must be have advance knowledge in the following systems: ELM, NSLDS, UA information systems (Matrix, SIS, Mosaic) Email, Word, Excel, Power Point and EdTest.',
'created_at' => '2010-02-12 01:13:14.000000',
'question_id' => 71,
),
58 => 
array (
'answer_text' => 'These projected expenses are based on current cost and are likely to increase by July.  It’s vital we have the flexibility to adjust these expenditures within the budget as expert advice dictates. If we determine that a different communication option would be more effective at reaching the students, we need the flexibility to quickly move to that new option. 
Facebook advertising expenditures:
We would enter a bid price of 0.51 per clicks with a $95 per day Max Budget. The Face Book ad will run 184 days   
Design & Ad cost $17,500
Face Book was determined to be more cost effective than the Daily Wild Cat

Daily Wild Cat advertising expenditures:  per day cost $233, ad size 5by8
$ 1,800 August 
$ 2750 November 
$ 2750 March

$ 325 UA Mall Events',
'created_at' => '2010-02-12 01:18:12.000000',
'question_id' => 72,
),
59 => 
array (
'answer_text' => 'As a new initiative last year, Campus MovieFest (CMF) was funded solely through the Campus Activities budget of CSIL. This budget encompasses a variety of programming areas including the University Activities Board and PSU programs. CMF took quite a bit of our funds this year, but the number of participants (70+ teams) and the quality of the event definitely made the program one we want to continue each year.',
'created_at' => '2010-02-12 18:51:24.000000',
'question_id' => 68,
),
60 => 
array (
'answer_text' => 'At this time there has been no financial support through sponsors. As the first year doing this program, we were unable to secure outside sponsorships. With the success of last year, we hope to create sponsorship packages that will entice outside business to help financially with the program. There is no guarantee, however, that we will ensure the funding to cover the cost of the program. We hope to build these relationships as we continue to build the program over the years to make it a UA tradition.',
'created_at' => '2010-02-12 18:55:37.000000',
'question_id' => 67,
),
61 => 
array (
'answer_text' => 'At this time there has been no financial support through sponsors. As the first year doing this program, we were not able to secure outside sponsorships. With the success of last year, we hope to create sponsorship packages that will entice outside business to help financially with the program. We hope to build these relationships as we continue to build the program over the years to make it a UA tradition.',
'created_at' => '2010-02-12 18:56:31.000000',
'question_id' => 66,
),
62 => 
array (
'answer_text' => 'We plan to host 10 Friday Night Live! events throughout the academic year. We will host 8 FNLs on Friday nights (Aug.-Nov. and Jan.-Apr.) and 2 on Wednesday nights (Day before reading day each semester). We would eventually love to expand the program beyond 1 Friday a month and make them multiple Fridays per month as the program grows.',
'created_at' => '2010-02-12 18:57:52.000000',
'question_id' => 60,
),
63 => 
array (
'answer_text' => 'I apologize for the confusion. The budget I submitted was for that of one high end FNL and not the entire year. The $50,000 was to encompass the entire academic school year for 10 FNLs as the cost of each varies. We usually spend more in August and January as we want to make the welcome back FNLs larger to get students excited about the new school year. Cost of each is also dependent on the type of featured entertainment for each and routing for entertainers. I do have an updated budget that I will send that shows how we will spend the funds over a 10 month period.',
'created_at' => '2010-02-12 19:05:08.000000',
'question_id' => 58,
),
64 => 
array (
'answer_text' => 'Last semester, we averaged approximately 600 individuals attending events at each FNL.  We hope to increase that number next year to be approximately 800 and eventually have over 1000 individuals coming to our FNLs. We also hope to impact a number of student organizations and departments through cosponsorships with FNL each month.',
'created_at' => '2010-02-12 19:11:43.000000',
'question_id' => 59,
),
65 => 
array (
'answer_text' => 'The last year we operated without Savvy Student was Fall 2007.

I have compared the units that currently particpate (Fall 2009) with their counterparts of Fall 2007 during the equivalent dates and can offer the following answers to your questions.

Wednesday sales have increased by 19% comparing 2009 to 2007 (no adjustment for inflation has been included). A 19% increase results in $138,118 of additional net sales and $28,661 net revenue (4%).

(In 2008/2009 the allowance was $250,000. Actual particpation was $293,756. Yielding a net loss of $43,756).

At the current rate of redemption we are anticipating falling short by $25,000, essentially bringing the program to a near break even proposition.

Wednesday customer counts have increased by 12%

Thank you for your consideration',
'created_at' => '2010-02-12 21:18:52.000000',
'question_id' => 77,
),
66 => 
array (
'answer_text' => 'Thank you for your question. The answer is that we are looking for $12,000 to support, in conjunction with UA South, the Student Services Fee Advisor position for two full years. 

Example of distribution if awarded at $12,000: UA South pays $6,000 and the SSF Monies pay $6,000 for a total of $12,000 for one year period. Second year, the same occurs and by the third year,it is believed that the monies paid by UA South students should be sufficient to cover its Advisor position without further assistance from Main Campus SSF monies.',
'created_at' => '2010-02-12 23:27:29.000000',
'question_id' => 84,
),
67 => 
array (
'answer_text' => 'The post assessment done for TRAD 104: Love and Eroticism evaluated student response to services available to them (cohorts, passive instruction slides, supplemental instruction, etc.). Students were not required to answer all questions in the post survey; percentages are based on responses only.

-Over 70% of students said that passive instruction slides or cohort leaders were how they learned about resources. 
- Over 81% of students found the social networking with cohort leaders to be helpful. 
- Over 62% of students learned about programs (like ASUA, Blue Chip, Clubs and Organizations, etc.) through either the cohorts or passive instruction slides. 

As you can see both cohort leaders and passive instruction were both useful in relaying resource and program opportunities; both will be integrated in the new expanded student support cohort. The cohorts will not be attached to a class in this model; we want to provide this opportunity to freshman regardless of their class registration. The goal of creating smaller, more intimate communities among new Wildcats is the reason that Student Affairs created the cohort program. Many things influence a student’s persistence to graduation; the cohorts will use passive instruction, mentoring, and resource events to integrate and engage students successfully with the University of Arizona.',
'created_at' => '2010-02-12 23:50:09.000000',
'question_id' => 83,
),
68 => 
array (
'answer_text' => 'The SA Marketing dept. provides support for every area of Student Affairs-from the time a prospective student reads a Viewbook, or gets an email asking them "What kind of a Wildcat Will YOU be?"-to the time they put on their cap and gown for graduation. There are over 100 projects being worked on at any given time, some examples:

Enrollment/admissions/recruiting
* Printed materials: postcards, magazines, brochures, fliers for recruiters
* Web: landing pages, emails, interactive surveys, Next Steps Center
* MacBook Scholarship: website, event planning, laptop customization

Orientation
* Printed materials: schedules, resource guides, convocation programs
* Presentations: video, technical support, design
* Web: online academic tour, math placement, design/imagery

Student involvement
* Student Unions: website, signage, dining services, meal plans, jobs
* CSIL: posters, websites, online conference registration
* Gallagher Theater: web banners, posters, plasmas
* Union Gallery: web banners, posters
* Greek Life: recruitment brochure, magazine, Tshirts
* Spring Fling & Wildcat Welcome: website, design, community/campus publicity

Student advocacy and support
* Dean of Students: academic integrity booklet, safety materials
* Veterans: conference materials, website, signage
* Faculty Fellows: website, promotional materials
* AZ Assurance: website, event materials, postcards, Tshirts
* SALT center & Family Weekend: website, advertising, promotional materials
* Think Tank: mktg. research  branding, website support, promo materials, signage
* Student Transitions: New Start summer program, MERITS
* Off Campus Housing: website, promotional materials, Housing Guide
* Financial Aid: email and design support (FAFSA reminders, scholarship matching)

UA/general campus community support
* Campus Emergency Response Team: web site, promotional materials
* VP for Student Affairs: office support, website, printed materials for ABOR, President Shelton, Provost Hay
* Commencement: printed materials
* New UA (arizona.edu) site: content for "Future Students" & "Students" websites
* Student Services Fee: web site design, functionality, content management, logo design, signage & materials, photography

Our impact is far-reaching and deep, affecting virtually all UA students, prospective students and their parents. We strive for consistency of message, efficiency of scale, and creative, energetic design that captures the Wildcat spirit!',
'created_at' => '2010-02-13 01:08:02.000000',
'question_id' => 76,
),
69 => 
array (
'answer_text' => 'The SA Marketing dept. creates messaging for, and communicates to, virtually the entire UA student body-even before they choose to become Wildcats.

If you can, take yourself back to your high school student self, making what is arguably a life-altering decision: where to attend college. Our ability to communicate to that high school student is severely hindered by resources, as is our ability to provide consistent messaging to current Wildcats.

A Student Marketing Team will provide content for an endless variety of projects:
* Design and provide web content where users see a student-guided tour of each residence hall
* Post written or video reviews of:
o Campus concerts (The Fray, Katy Perry, Plain White T\'s)
o Performances by the College of Fine Arts (theatre, music, etc.)
o Campus restaurants/union eateries/off-campus dining
o Campus life
* Profile students, professors, or staff of interest
* Design artwork to promote events for campus groups
* Determine content for soon-to-be launched new UA website
* Implement and manage social media messaging

As an institution, the University of Arizona is arguably decentralized, consisting as a collection of colleges and schools, units and departments. While each regards itself as experts for its own unique audience and stakeholders, message consistency and effectiveness is often lost as a result. Information becomes difficult to obtain for prospective, and current, students.

Students know better than faculty and staff how to communicate to each other. They can readily identify their interests-and also where the informational pitfalls lie. Including them in the process has already served the University well: for the Unions alone, students have left their mark through their work with student-managed projects including Core, IQ Fresh, Catwalk, Spring Fling-and the upcoming Bagel Talk.

SA Marketing has a proven track record in mentoring students and employing them in a professional environment. With their marketing, design, and writing skills, our students have gone on to become eminently more employable, no small feat in today\'s economic climate.',
'created_at' => '2010-02-13 01:08:56.000000',
'question_id' => 75,
),
70 => 
array (
'answer_text' => 'Yes, the current application will support non-Arizona Assurance students for 2011-2012 school year.  We plan to begin the expansion of the program into other colleges in the 2011-2012 to include non-Arizona Assurance students so that we can begin to offer programming for the broader student community.',
'created_at' => '2011-01-24 22:31:48.000000',
'question_id' => 91,
),
71 => 
array (
'answer_text' => 'The faculty student interaction funding will be for events that the faculty member would like to coordinate for students outside of the classroom. They can be for activities related to the class or not.  The purpose of the student-faculty interactions is to provide students an opportunity to engage in conversations and experiences with faculty outside of the classroom.  These interactions could include field trips, cultural events, artistic events, direct work with the research that the faculty is conducting themselves etc. Research shows that faculty/student interaction outside of the classroom improves academic performance and persistence towards graduation (Chickering and Gamson, 1987; Astin, 1993; Kuh et. al., 2005).  In their article, Kim and Sax (2007) state that “college impact research has continually demonstrated a positive relationship between student-faculty interaction and a broad range of student educational outcomes, including academic achievement, educational aspirations, intellectual growth, and academic satisfaction”. A study conducted at UA in 2002-2003 regarding the Student/Faculty Interaction programs demonstrated that students feel more connected to the class curriculum, are less likely to demonstrate disruptive behavior, and participate more in the classroom. In addition, the study found the following to be true from the faculty perspective, 81% believed this program’s results for students’ learning was worth the effort, 77% believed students would benefit from more educational experiences like those provided by this program, and 75% believed this program embodies important values of our campus. Providing these additional opportunities for interaction with faculty will help to improve student engagement in the classroom as well as overall success of students.  In addition, these activities could lead to development of mentoring relationships between students and their faculty.',
'created_at' => '2011-01-24 22:37:46.000000',
'question_id' => 92,
),
72 => 
array (
'answer_text' => 'Within the Center for Student Involvement & Leadership there is a vast array of graduate student opportunities that allow for real transferable work skill development.  Each assistantship works in an operational area such as Gallagher, Gallery, Off Campus Housing, etc.  There is one graduate assistantship that is currently offered, The Sustainability GA, which oversees the campus wide composting project.  This particular assistantship may in the long term be able to sustain itself if we master compost production and sale in a few years time.  At this time, however, compost runs at a cost to the University.  This position has potential of being funded via the Green Fund but was included in this request in an effort to package our assistantships as a holistic career development opportunity.  At the committee\'s request, I am happy to reduce my request to 4 GAs and make a request for the Compost GA to the Green Fund but in doing so we must be cognizant that this position is critical to a significant ongoing UA effort.
The other graduate positions, such as Gallagher Theater and the Galleries play critical roles in providing and maintaining a healthy commitment to the arts in our campus wide programming. In our efforts to provide low cost and free opportunities for our UA student community, it is challenging to find a balance of staffing these areas and having operational budgets that allow for healthy programming.  We continue to seek sponsors and grants that can supplement our programs in each unit which in turn reduces operational costs but in small units such as these, supplementing to the extent that payroll and ERE are covered is challenging and places these areas in constant risk of cuts and reduction to quality.',
'created_at' => '2011-02-01 17:35:21.000000',
'question_id' => 97,
),
73 => 
array (
'answer_text' => 'The Dean of Students Office seeks qualified graduate students from all academic disciplines.  Student Affairs departments tend to have a higher level of interest from students admitted to the Center for the Study of Higher Education, as they are seeking career opportunities typically found in our Division’s departments.  

We plan to participate in the Higher Education – Student Affairs Graduate Assistant Interview Weekend (February 25-27, 2011) in support of the important partnership developed by the leadership of the two areas; however, we will also advertise our positions to specific academic programs within colleges such as Fine Arts, Eller, Social & Behavioral Sciences, and Humanities.  The diversity of experience, academic discipline, and application of theory and practice provides the Dean of Students Office and our student constituents enhanced and more informed service.',
'created_at' => '2011-02-02 23:15:46.000000',
'question_id' => 95,
),
74 => 
array (
'answer_text' => 'Student Transitions is a fairly new unit comprised of many different departments that include New Student Services, Academic Success and Achievement, National Student Exchange, Leadership Programs and Career Services.  Departmental directors have evaluated the growth in each of their programs and have come to the conclusion that a stronger infrastructure is needed to expand these opportunities on a larger scale to meet campus demand.  

Departmental directors identified specific program needs and how student jobs can be created to meet these needs.  The current proposal requests funding to cover 200 weekly hours during academic semesters and 60 weekly hours for the summer which roughly translates to 20 Student Assistant positions.  Jobs will be provided to the most qualified students, the actual number of positions will depend on funding and availability of students.  Based on the recommendations of departmental directors if 20 positions are funded students would be primarily based in the following areas:

Career Services needs 6 Student Assistants to promote Career Services resources and assist in reaching a broader cross section of colleges and majors.  Students will serve as liaisons to their college and as representatives of Career Services at events across campus.  

Academic Success & Achievement needs 5 Student Assistants to receive and direct callers and visitors as a primary function, but also perform a variety of routine clerical tasks pertinent to Student Transitions.

Leadership Programs needs 4 Student Assistants to assist with the administrative, assessment, and coordinating components of leadership programs. 

National Student Exchange Program needs 4 Student Assistants to assist with the administrative and outreach aspects of NSE program.

New Student Services needs 1 Student Assistant to assist with Early Alert implementation.

The Student Assistant position will serve Student Transitions by providing a group of student employees who have the skills and training to flex across the division to provide support as needed.  Each Student Assistant will have specialized training in one of these areas, but will also be crossed trained so that in times of greater need staff resources can be combined.  Student Transitions wants to make sure this process works in an efficient manner so all departments will collaborate in hiring, training and scheduling Student Assistants.',
'created_at' => '2011-02-02 23:44:18.000000',
'question_id' => 102,
),
75 => 
array (
'answer_text' => 'LGBTQ Affairs. Currently the LGBTQ Affairs office is staffed with a Program Director who relies upon undergraduate and short-term graduate student interns to assist with the development and implementation of programs and services for students, faculty and staff.  The short-term nature of interns presents challenges to the Program Director in providing consistent, effective, and long-term programming.  

Central Office and Marketing. Research and assessment is the responsibility of all members of the Dean of Students Office; however, that duty too often takes a back seat for staff when the majority of their time is spent investigating cases, meeting with victims, witnesses, assisting students in crisis, consulting with faculty and staff, etc.  The office has a rich bank of data regarding our student contacts, and we need a dedicated individual to help us develop a research and assessment strategic plan as well as analyze the data to inform our practices.  The work of the proposed Student Assistance and Advocacy graduate assistant has been conducted, in part, by the current Marketing Graduate Assistant; however, the entire department does not benefit from that individual’s knowledge and experience. Additionally, the Marketing Graduate Assistant was funded via “rollover” funds from short-term vacancies in professional staff positions in the previous fiscal year, and those funds will not exist in the 2011-12 fiscal year.  

Fraternity and Sorority Programs (FSP). The current FSP graduate assistant for academic support and leadership development is funded through the Center for Student Involvement and Leadership (CSIL) Graduate Assistant Student Services Fee Proposal for one year.  Since FSP is no longer part of CSIL, we will not have this funding next year.   

Parent & Family Association. This area is currently staffed by two full-time staff members (Assistant Dean and Office Specialist), with the Assistant Dean also responsible for the Campus Mall Use Policy and First Amendment issues for the central Dean of Students Office.  

Judicial Affairs. The Judicial Affairs graduate assistants are currently funded via revenue generated from the Dean of Students Office’s Personal Responsibility Course; however, we may not be able to continue to fund those positions with the pending state budget cuts.  We have a number of professional staff funded by state dollars, and will need to utilize our revenue to continue those contracts.',
'created_at' => '2011-02-03 00:03:55.000000',
'question_id' => 94,
),
76 => 
array (
'answer_text' => 'How accessible is this program going to be to students outside of Eller?
The addition of this program will provide greater access to students outside of Eller by design.  
The Career Services office will move from a generalist model to a specialist model using this program as an important step to better address our UA student body’s desire to receive career counseling related to academic major (74% of student Fee Survey 2010 Respondents).  In addition, this program will address the demand for increased employment opportunities (82% of respondents) through the Employer Relations Ambassador Team assigning team members to specific colleges/disciplines.
The full time position that is part of this proposal allows the Career Services staff to advance a new liaison model where each career counselor/coordinator is assigned up to two liaison colleges.  With the Student Services Fees funding a career counselor/coordinator assigned to Eller, the other full time career counseling staff can focus on the needs of the remaining colleges.  This will allow our existing four career counselor/coordinators, Assistant and Associate Director to strategically outreach and build greater expertise in the career fields represented in their assigned colleges as well as strengthen communication with department advisors, key faculty and staff. We estimate enhanced service delivery to reach 12,000 students directly through this new model above and beyond Eller.
The full time career counselor/coordinator assigned to Eller would still reside in the Career Services office 50% of the time to insure effective and efficient staffing and service delivery.  While each of our counseling staff will more fully develop expertise for students in their assigned colleges, knowledge will be shared and staff will continue to work with students across disciplines to best meet demand.',
'created_at' => '2011-02-07 19:37:00.000000',
'question_id' => 103,
),
77 => 
array (
'answer_text' => 'At a minimum it would be maintained. Our goal certainly is to sustain readership and reach new ones. We have no plans on cutting back. However, we are working this semester on a plan to diversify readership by extending our distribution offcampus, particularly to student housing complexes. That plan will hopefully initiate before the end of the semester but will certainly be in place next year. There will be additional delivery costs but we hope to increase circ by 2,000.',
'created_at' => '2011-02-08 18:25:49.000000',
'question_id' => 106,
),
78 => 
array (
'answer_text' => 'This has not been determined. In any event, our vision is more wholistic than separating the two products. Marketing will promote the brand and readership across all platforms, print, online and social media. Right now, print advertising accounts for the vast majority of revenue so our marketing efforts cannot ignore providing print sales support. However, one reason for creating this initiative is to help us develop strategic approaches to deal with new media realities. But, no, the exact scope of how time and resources will be used has not been decided.',
'created_at' => '2011-02-08 18:30:39.000000',
'question_id' => 105,
),
79 => 
array (
'answer_text' => 'We have not done an audit yet this semester, but over the years returns have ranged fairly steadily in the 10-15% range (of course we have also trimmed our press run).  Industry-wide, free distribution papers would aim for a 10% return.  But Wildcat returns vary according to location and often just the day of the week. Four or five stands can account for almost all returns, while elsewhere on campus you can\'t find a paper. Because we distribute exclusively outdoors, a bad (but rare) weather day has an immediate impact. But the Wildcat\'s biggest problem with distribution is newsstand location. Many of our stands probably no longer are in high traffic or visible areas as new construction and campus design has emerged. Almost all the stands are permanent so moving is difficult and expensive. That\'s why, in an answer to another question, we are initiating an expanded offcampus distribution plan to put the papers in readers\' hands (or at their housing units) rather than depend on them picking one up on campus.',
'created_at' => '2011-02-08 18:50:50.000000',
'question_id' => 104,
),
80 => 
array (
'answer_text' => 'Our requests are all inter related because they are all within the same payroll.  How they are funded will ultimately impact the overall operational budget of CSIL.  More specifically, the funding of the front desk staff will have an impact of CSIL\'s hours of operation.  We are currently able to have our spaces open during the prime time our students are meeting and in need of assistance while also not altering the "regular" business hours of operation, 8am-5pm.  We will need to assess these hours and how best to serve our students without compromising our ability to work on a UA schedule if we are not funded.
In terms of the graduate assistants, our ability to have both an operational programming budget, to create programs for UA students- at little to no cost, as well as a staff GA to oversee the area will need to be re-evaluated.  Not receiving this funding will  result in a reduction of hours of the operational units, such as the gallery and games room, as well as a reduction in graduate assistants. The fee dollars are vital to the continued success of the student employment we offer, both graduate and undergraduate.  We will do whatever is reasonable within our operational budgets to maintain the quality of our employment and programs and services we offer but a reduction in staff will ultimately result in some direct impact.',
'created_at' => '2011-02-11 21:15:32.000000',
'question_id' => 107,
),
81 => 
array (
'answer_text' => 'On average, 50 students utilize CODE Gaming Lounge throughout the week. Although the number is not as high as we would like, this has been an increase from a year ago when the space first opened. We also have a dedicated group of students who utilize the space and come back regularly. 

This number does not include weeks we have tournaments. During tournament days, the number of people who participate at a given time range from 6-15 depending on the type tournament.  

We have also had 2 student groups rent out the space to hold functions this year.

We hope to be able to maintain the hours we are currently open to be able to grow the space usage exponentially for the 11-12 academic year and beyond. We are striving to make this a destination for individuals at PSU.',
'created_at' => '2011-02-12 01:35:57.000000',
'question_id' => 109,
),
82 => 
array (
'answer_text' => 'The Games Room averages 1800 individuals weekly utilizing the space from 10am-12am. These numbers include those who participate in our weekly tournaments for billiards and poker.

From 12am-2am, we average 60-70 individuals a week. The individuals who utilize the Games Room at this time either play billiards or Dance Dance Revolution. Some of these individuals have also utilized the late night menu at the Cellar Bistro in the past.',
'created_at' => '2011-02-12 01:40:25.000000',
'question_id' => 108,
),
83 => 
array (
'answer_text' => 'Campus Security Officers are not full Police Officers.  They do not carry weapons, nor can they arrest or cite individuals.  Their role in the context of this proposal is to:
1.  Provide Deterrance.  Provide a visual presence in and around those parts of campus that are most active during late nights (e.g.,residence halls and University Village). We anticipate this presence will deter some behaviors.
2.  Provide Education.  Encourage individuals to consider the consequences of problematic behaviors when it is apparent that they are close to putting themselves at risk.  (For example, asking, "Are you sure that you want to do that?"
3.  Provide Assistance.  Provide assistance to students, as needed (e.g. helping injured or incapacitated students).  Summon emergency assistance, as needed.
4.  Enhance Enforcement.  Assist police by summoning an Officer to a location when a situation is encountered that requires full Police response.',
'created_at' => '2011-02-14 16:14:09.000000',
'question_id' => 117,
),
84 => 
array (
'answer_text' => 'If the program is successful, we intend to create or find permanent funding.  Including the program\'s costs specifically as part of UAPD or Residence Life\'s budgets would occur only if there are no other funding sources.  Inclusion in Residence Life\'s budget would also require support from the Residence Hall Association (RHA), which participates in the rate setting process each year.

While there might be reason in the future to request some amount of continued funding for specific (currently unknown) reasons, it is not our intent to create a program that requires long-term continued support of the Student Services Fee.',
'created_at' => '2011-02-14 16:39:32.000000',
'question_id' => 118,
),
85 => 
array (
'answer_text' => 'The Disabled Cart Service of Parking & Transportation Services receives 100% of its operating funds from revenues generated from parking operations. Last year, we were fortunate to receive partial funding from ASUA to pay for student salaries(only). 

On February 25, 2011 PTS will also submit an application to the Arizona Department  of Transportation for Federal Capital funding of $ 43,846 to purchase 4 relpacement Golf Carts (3 Six Passenger and 1 Wheelchair cart) for the Disabled Cart Service on Campus.',
'created_at' => '2011-02-14 22:21:54.000000',
'question_id' => 113,
),
86 => 
array (
'answer_text' => 'If the MERITS Program were to receive full funding, each Peer Advisor would be initially assigned 20-25 students. This group size allows Peer Advisors to facilitate fun and engaging workshops that allow for full group participation while still having a manageable number of students to meet with individually every other week. With 20-25 students, Peer Advisors meet with approximately 10-12 students each week for a total of about 5-6 hours of one-on-one meetings weekly. While smaller group sizes are optimal, this size allows us to maximize our funding while still keeping group sizes manageable and within the space available to our office.

We are continually trying to improve and expand the program’s accessibility to students. During the current academic year, the MERITS Program has been serving 650 students. We hope to continue to increase our impact on the student body by offering the program to more students next year. As mentioned above, the optimal number is 20-25 students per Peer Advisor. If additional Peer Advisors are needed, our office will provide in-kind support to hire and fund additional student employees.',
'created_at' => '2011-02-15 17:06:19.000000',
'question_id' => 123,
),
87 => 
array (
'answer_text' => 'The PIP GA Coordinator provides guidance and direction, as well as logistical support, to both the PIP program itself and the annual student cohorts that move through it. The PIP students themselves integrate into influential student leadership positions within their Student Affairs departments, and are able to propose and often implement student-focused projects under the guidance of the GA and their department supervisors. The program is focused on moving talented and enthusiastic undergraduate students into positions and projects in which they can create lasting change within Student Affairs for the benefit of the entire UA student population.',
'created_at' => '2011-02-15 17:11:28.000000',
'question_id' => 127,
),
88 => 
array (
'answer_text' => 'The Arizona Student Unions continues to fund most of the program’s costs, including room rental, marketing, food, orientation, supplies, and other auxiliary expenses. Student Services Fee funding is committed exclusively to the personnel costs associated with hiring a Graduate Assistant to perform as the PIP program coordinator.  The Student Unions continues to invest in the program’s development – however, the Unions have been and continue to be unable to sustain the costs of a graduate position dedicated exclusively to program coordination.  The expansion of the program to other units in Student Affairs calls for the existence of a Coordinator position to handle interdepartmental logistics and student issues.',
'created_at' => '2011-02-15 17:11:55.000000',
'question_id' => 128,
),
89 => 
array (
'answer_text' => 'After requests for clarification, the following is an itemized budget of expenses related to staff training:

1. Staffing

Training Wages for Peer Advisors
Year One: $7,750 (includes ERE)
Year Two: $7,750 (includes ERE)
Notes: Wages for all hourly employees for 45 combined hours each of training and continuing professional development during Fall and Spring semesters.

2. Operations ($5,750 for each of two years-itemized below)

Training Retreat & Materials
Year One: $2,000
Year Two: $2,000
Notes: MERITS Peer Advisors participate in departmental staff training retreat and receive all necessary programming supplies for the year.

Office Supplies
Year One: $3,750
Year Two: $3,750
Notes: Includes program expenses such as copies for workshops, service events, etc.',
'created_at' => '2011-02-15 17:26:35.000000',
'question_id' => 123,
),
90 => 
array (
'answer_text' => 'The answer to this question requires further explanation. ATLAS serves students in several ways. One way that ATLAS serves students is by the individual workshops. Any student has the option to attend any of our workshops free of charge as stand-alone events. ATLAS also does “Workshops on Demand,” which are one-time workshops for groups. So, not every student that ATLAS serves is seeking a certificate. Of the 558 students that ATLAS has served so far this year (as reported in Appendix B of the proposal), 257 were attendees at Workshops on Demand, and 130 were attendees at single workshops. The remaining 171 students were actually seeking a certificate through completion of an entire track and 126 of those (73.6%) have completed a certificate so far. This number will likely rise throughout the rest of the year as participants have no deadlines for completion.',
'created_at' => '2011-02-15 17:46:48.000000',
'question_id' => 125,
),
91 => 
array (
'answer_text' => 'There actually is no current graduate assistant for this program. ATLAS is allotted .25 FTE of a professional coordinator and has one student worker. These two requested graduate assistant positions are intended to expand the program. One graduate assistant will focus on expanding the program as it currently exists (focused on undergraduate students) while the second graduate assistant will focus on developing ATLAS programs to meet the leadership and professional development needs of graduate and professional students.',
'created_at' => '2011-02-15 17:47:57.000000',
'question_id' => 126,
),
92 => 
array (
'answer_text' => 'Project RUSH will restore OSFA’s ability to create financial aid opportunities of a more personalized nature for every student who needs our assistance and allows our financial aid services to be more efficient. Over the past two years, our ability to create financial aid opportunities has diminished due to a 30% increase in financial aid demand. In an effort to meet the needs of our students, our office has an Opening of School schedule which pulls a minimum of seven professional staff members  from: the Counseling, Scholarships and Central Processing (CPS) areas for each shift to help cover this increase in demand. This results in the inability of these professional staff members to perform their necessary tasks such as one-on-one counseling, form processing and completing student requests, which in turn generates even more student inquiries. This cascading situation leads to further student questions and a delay in students’ receiving their financial aid. Project RUSH will generate faster processing time, faster financial aid disbursements, shorter waiting time in the lobby and on phones, as well as a quicker response via email. Project RUSH will meet the demand by supplementing our professional staff with seven people to free up approximately 6,000 hours for these Counseling, Scholarships and CPS professionals. Project Rush will enable these individuals to provide the necessary personalized opportunities, thus improving the overall financial aid service to UA students by disbursing their financial aid sooner and hassle free.',
'created_at' => '2011-02-18 17:39:01.000000',
'question_id' => 111,
),
93 => 
array (
'answer_text' => 'The discrepancy between 10 and 11 vehicles is because Safe Ride purchases vehicle insurance for all 10 cars and the golf cart, however, only the 10 cars need fuel since the golf cart is electric.',
'created_at' => '2011-02-18 17:46:53.000000',
'question_id' => 114,
),
94 => 
array (
'answer_text' => 'Many of Safe Ride\'s vehicles are pushing well over the 150,000 mile mark. It is critical to plan for these to be replaced over the next 3 years because these vehicles are costing more and more in fuel and maintenance costs. This is due to the fact that they will be more than 10 years old and efficiency degrades very quickly after 100,000 miles. It costs the program a lot in repairs of the older vehicles need to be replaced to maintain maintenance costs.',
'created_at' => '2011-02-18 17:55:06.000000',
'question_id' => 115,
),
95 => 
array (
'answer_text' => 'The majority of our money comes from the Student Services Fee, another portion comes from ASUA, and a small fraction of Safe Ride total budget comes from the Parents & Family Association Grant.',
'created_at' => '2011-02-18 18:01:34.000000',
'question_id' => 116,
),
96 => 
array (
'answer_text' => 'Client Services area will pre-fund Project RUSH by borrowing from other OSFA accounts. This will ensure that these individuals are hired, trained and well equipped to assist with the Opening of School rush. With your approval, OSFA will use the SSF funding to repay the funds borrowed for the May and June expenses.  By agreeing to allow OSFA to reimburse the pre-funding of Project RUSH there will be no adverse effects to the budget or the program.  The success of Project RUSH is dependent on this repayment component. This flexibility is critical, as we must have these individuals hired and in training by May 15, 2011.',
'created_at' => '2011-02-18 22:14:03.000000',
'question_id' => 110,
),
97 => 
array (
'answer_text' => 'No, ASUA is not guarenteeing $138,575.76 for the Fall 2011-Spring 2012 academic year. This is the amount that is currently in club funding for this academic year (2010-2011), $75,000 of which was given by the Student Services Fee and the remainder of which was provided by ASUA. The Student Services Fee granted club funding $75,000 for two years, this academic year and the next. There is a higher demand for club funding that we are experiencing from students. In addition, the budget cuts that UA will be facing will most likely impact the ASUA budget, giving them less of an opportunity to provide additional club funding that is needed to assist clubs on campus. Recognizing these things, I requested an additional $25,000 from the Student Services Fee so that ASUA would receive $100,000 for club funding next year instead of $75,000 so that we can best meet the needs of students. Please let me know if any additional information is needed.',
'created_at' => '2011-02-20 03:07:15.000000',
'question_id' => 134,
),
98 => 
array (
'answer_text' => 'The following are monthly/yearly totals 2009-2010.  It’s hard to adjust the volume between peak and non-peak hours per day, but I can provided monthly and yearly numbers.  

Calls-
Jul        3416
Aug        3624
Sept       2705
Oct        3916
Nov        4728
Dec        1395
Jan        1478
Feb        4125
Mar        6836
Apr        5308
May        5167
Jun        5899
Total      48597

Instant Messaging (estimate)

Jul        200
Aug        450
Sept       600
Oct        620
Nov        700
Dec        200
Jan        250
Feb        450
Mar        650
Apr        600
May        450
Jun        200
Total      5370

Visitors to Old Main

Aug        59
Sept       374
Oct        1455
Nov        900
Dec        553
Jan        722
Feb        1355
March      2317
April      3474
May        297
Total      11506',
'created_at' => '2011-02-21 02:49:56.000000',
'question_id' => 119,
),
99 => 
array (
'answer_text' => 'We have 13 phone lines in the student call center.  The average call is about 5 minutes.  Many days all the lines are busy and students are multitasking between answering the phones and on the instant message.',
'created_at' => '2011-02-21 02:53:06.000000',
'question_id' => 120,
),
100 => 
array (
'answer_text' => 'If we were able to hire 10 more students to work 25 hours each. 10 students x 25 hrs = 250 staff hours.
or
If we were able to hire 8 more students to work 31.25 hours each. 8 students x 31.25 hrs = 250 staff hours.',
'created_at' => '2011-02-21 02:54:30.000000',
'question_id' => 121,
),
101 => 
array (
'answer_text' => 'The purpose of the Million Dollar Scholar event is to encourage and equip students to successfully apply for outside donor scholarship funding. Our objective is for UA student usage of Scholarship Universe to increase following the event—however, your question gave us the idea of hosting a post-event “Million Dollar Scholarship Campaign” in which students report the dollar amount for each scholarship application they have submitted—and we would track the results to $1 million. 

i.e. Wilbur Wildcat applies for a $3000 scholarship on October 1st. He reports this to Scholarship Universe and we post his efforts on a campaign goal sheet. 

The campaign goal sheet would be something we could promote on our webpage, at promotional events on the UA Mall. This would provide both an incentive to work towards a collective goal as well as inspiration to follow the lead of other UA students. 

Thanks for prompting this idea!',
'created_at' => '2011-02-21 16:12:44.000000',
'question_id' => 136,
),
102 => 
array (
'answer_text' => 'I have sent the complete descriptions to the committee chair as they will not fit within the character limits.',
'created_at' => '2011-02-21 23:04:11.000000',
'question_id' => 129,
),
103 => 
array (
'answer_text' => 'Within the Center for Student Involvement & Leadership there is a vast array of graduate student opportunities that allow for real transferable work skill development. Each assistantship works in an operational area such as Gallagher, Gallery, Off Campus Housing, etc. There is one graduate assistantship that is currently offered, the sustainability grad, which oversees our campus wide compost project that may in the long term be able to sustain itself if we master compost   production and sale in a few years time. At this time, however, compost runs at a cost to the University. This position has potential of being funded via the Green Fund but I included in this request in an effort to package our assistantship as a holistic career development opportunity.   
The other graduate positions, such as Gallagher Theater and the Galleries play a critical role in providing and maintaining a healthy commitment to the arts in our campus wide programming.  In our efforts to provide low cost and free opportunities for our UA student community,  it is challenging to find a balance of staffing of staffing these areas and having operational budgets that allow for healthy programming.  We continue to seek sponsors and grants that can supplement our programs in each unit which in turn reduces operational costs but in small units such as these, supplementing to the extent that payroll and ERE are covered is challenging and places these areas  in constant risk of cuts and reduction to quality.',
'created_at' => '2011-02-21 23:40:39.000000',
'question_id' => 129,
),
104 => 
array (
'answer_text' => 'Yes. In order to do so, a component of our point-of-sale system, Micros, needs to be updated. Doing so will require that the system be offline for an extended period of time. The next window of opportunity to do so will be Memorial Day weekend, and we are targeting that time period for the required update.',
'created_at' => '2011-02-24 18:50:54.000000',
'question_id' => 132,
),
105 => 
array (
'answer_text' => 'When the original Student Services Fee request was made, this item was included as the starting point for expanding the Savvy Student program to include more days, times or options for students. While data about headcount and trends is very consistent for the weekday lunch hour, it is not very consistent for late afternoon and dinner hours. This makes it difficult to project usage of an expanded program and, therefore, provide a concrete alternate budget for such an option.

The process of making the request, though, got us thinking about how to accomplish a "savvy-student style" dinner. Our Dining Services and Production teams went to work and realized that, at a price point of $4, they could provide a very affordable meal option to everyone (not just students) at both Café Sonora and 3 Cheeses and a Noodle not just one night a week, but every night of the week. As such, $4 Dinners were born and have already been implemented as follows:

Café Sonora: 1 Beef Taco, 1 Bean Tostada, Rice, Fountain Drink

3 Cheeses and a Noodle: Baked Ziti, Caesar Side Salad, Garlic Bread, Fountain Drink

Additional locations are being worked on right now, and $4 Dinners will be expanded to include more options within the next 3 to 4 weeks – all without the need for subsidization by the Student Services Fee.

When added to the $5 Friday options already in place, we have created an expanding portfolio of very affordable meal options taking place Wednesday and Friday during the lunch hour, and at Dinner each day. This portfolio is going to be rebranded as "Cheap Eats", and will encompass all of the options available to students, faculty and staff. For the current list of $3 Savvy Student options and $5 Friday options (with the $4 dinner options added soon) go to:

http://www.union.arizona.edu/cheapeats

Therefore, the Unions withdraw the request to receive funding for an "expanded program to include one weekly dinner hour."',
'created_at' => '2011-02-24 18:52:03.000000',
'question_id' => 133,
),
106 => 
array (
'answer_text' => 'Currently, the $3 Savvy Student is available at the following locations north of Speedway:

Nucleus @ Bio 5: Deli Sandwich & Smoothie
Eller Deli:  Deli Sandwich, Salad & Bottled Water

There are no plans to expand Savvy Student (outside of the current locations) to any new location north of Speedway. The only other food stop north of Speedway is AME, which is more of a traditional convenience store that does not lend itself to participation in the Savvy Student program.

Additionally, we will expand our $5 Fridays at Nucleus and the Eller Deli (menu TBD) beginning Fall 2011.',
'created_at' => '2011-02-24 18:49:43.000000',
'question_id' => 131,
),
107 => 
array (
'answer_text' => 'Answer to the second question...',
'created_at' => '2011-11-28 18:21:51.000000',
'question_id' => 139,
),
108 => 
array (
'answer_text' => 'regular answer ...',
'created_at' => '2011-11-28 19:05:10.000000',
'question_id' => 140,
),
109 => 
array (
'answer_text' => 'Here\'s the PAR answer for SF-1',
'created_at' => '2011-11-28 21:13:41.000000',
'question_id' => 138,
),
110 => 
array (
'answer_text' => 'The classified regular employees’ salary line of $104,500 covers:
•$91,800 for3 Full Time Equivalent (FTE) Project RUSH positions (split among four part-time year-round individuals)
•$12,700 for one position upgrade from Program Coordinator (35,500 + ERE) to Program Coordinator Senior (48,000 + ERE) for the manager of the Project RUSH employees

The rationale for 4 year round Project RUSH employees:
•Flexibility to have employees work full time during RUSH and part-time throughout the rest of the academic year 
•Retention of best Project RUSH employees—without the potential of ongoing employment there is high attrition of temporary staff   
•Need additional support year round especially during spring opening of school
•Additional support throughout spring term including: award letters, incoming student call volume, orientation days, and opening of school
•Additional support during training of new Project RUSH employees

The rationale for a Program Coordinator Senior position upgrade:
•This project requires management of full time classified staff which falls under the supervisory qualifications of a Program Coordinator Sr. 
•This position has greatly increased workload as in addition to Project RUSH, this person must also complete counselor level student contact and document processing
•Responsible for hiring and initial month long immersion training of all Project RUSH staff
•Responsible for project reports and data collection—a high level management position that approaches Assistant Director level.   
 ',
'created_at' => '2012-01-24 22:35:14.000000',
'question_id' => 154,
),
111 => 
array (
'answer_text' => 'Year 1 Operations Budget $15,000 
•Phone Bank, need to build partitions: $10,000
•Phone Bank, build additional phone bay: $3,000
•Additional phone line (plus equipment): $1,000
•Computer for additional phone bay: $1,000
•5 Additional computers $5,000 
•SASG desktop computer maintenance and refresh ($302 per computer x 16 computers): $4,832
•iPad for lobby line control (OSFA purchased 1 new iPad for 2012 RUSH) another is needed for future years $700
•Training Materials: $500

Total: $26,032

Over by $11,032 – OSFA will cover this first year overage as an investment in future benefits to students 

Years 2 and 3 Budget $10,000 per year 
•SASG desktop computer maintenance and refresh ($302 per computer x 16 computers): $4,832
•Training Materials: $500
•Phone equipment maintenance (including phones and headsets): $500
•iPad for lobby line control:  $700
•CatQ customer service kiosk upgrades and development: $5,000
oCatQ is still in testing and we are unable to afford an upgrade for year 1. For years 2 and 3 we will create upgrades including a mobile app, off-site appointment scheduling, and live chat assistance. 
oIn its current beta version CatQ is already providing enhanced service to students by allowing OSFA staff to better move client demand smoothly throughout all 80+ OSFA employees. This helps reduce peak wait times and client frustration. This investment will pay big dividends in improved student service. 

Total: $11,532

Over by 1,532 per year– OSFA will cover this overage as an investment in future benefits to students',
'created_at' => '2012-01-24 22:36:59.000000',
'question_id' => 144,
),
112 => 
array (
'answer_text' => 'Project RUSH 2011 hired a total of 7 temporary employees with RUSH funding. OSFA was also able to hire 4 new student employees at OSFA expense. Without RUSH employees handling student demand, OSFA would not have been able to manage these 4 new student employees.  

•Hired 6 Project RUSH employees, start date May 16th
-One of the 6 left for a permanent position 3 weeks into training

•Hired a replacement, start date June 13th 
-Employee left for a permanent position 4 weeks into training
-Unable to hire a replacement for this person as a replacement would not possibly be trained in time for Fall 2012 opening of school 
-The second replacement would have begun training the beginning of August, in the midst of opening of school
-Training is a minimum of 4 weeks 

•Out of 7 total, 2 left for permanent positions prior to August, 3 others left for permanent positions after Fall 2012 opening of school 

•Project RUSH functioned during July-September 2012 with 5 employees instead of the ideal 7 employees (one with less experience than other RUSH staff) 

•Of the 5 Project RUSH employees, after the fall opening of school
-1 found a full time permanent position on campus
-2 moved into OSFA permanent positions
-2 stayed on as part time Project RUSH employees throughout the academic year to help with regular work load and spring 2012 opening of school

•Based on this knowledge we need to hire 3 temporary and 4 part-time year-round Project RUSH employees
-We expect to better recruit and retain quality staff with this new ability to offer the potential of a year-round position 
-High attrition of temporary staff is a great loss of staff asset and time invested in training and staff development 
-Retaining RUSH staff is essential to the health and retention of both the RUSH Project Manager and other OSFA managers',
'created_at' => '2012-01-24 22:39:28.000000',
'question_id' => 143,
),
113 => 
array (
'answer_text' => 'We are partnering with colleges (SBS, Education, Pre-Pharmacy and Pre-Physiology) that do not currently offer a probation program or course and are not participating in the probation partnership that Think Tank has in place with the Center for Exploratory Students-CLAS.  

If the PASS Probation Program is funded the Think Tank will play a key role in the training of all respective student staff. 

The Arizona Assurance Scholars are naturally a portion of students in the colleges outlined above and will be served accordingly.',
'created_at' => '2012-01-26 01:11:20.000000',
'question_id' => 145,
),
114 => 
array (
'answer_text' => 'We expect the student employees to work 10-15 hours a week.  They will co-facilitate a weekly one hour workshop, meet for a half-hour bi-weekly with each of the 25 students assigned to them, they will attend a one hour weekly staff meeting, and the remainder of their work hours will be filled by prep time for the workshops, follow-up with students via email, and review of mid-term progress reports.',
'created_at' => '2012-01-26 01:12:08.000000',
'question_id' => 146,
),
115 => 
array (
'answer_text' => 'The Graduate Students will share office space with other graduate students located in the basement of the Robert L. Nugent Building.

The Peer Advisors will meet with their students in the central programming area in the basement of the Nugent Building.

The workshops will be held in the Nugent Building basement workshop room and possibly other workshop or conference rooms on campus.',
'created_at' => '2012-01-26 01:13:10.000000',
'question_id' => 147,
),
116 => 
array (
'answer_text' => 'Our primary goal is to assist students on probation in gaining resource awareness and helpful strategies that they may continue to use and build from as they navigate through their academic careers at the university.  Student success is measured differently by each unique individual based on their circumstances and personal goals. 

We will also measure the success of the program by percentage of student retention from one semester to another for each semester cohort.  For example:

Fall 2012: Sophomores, juniors and seniors on academic probation referred by Academic Advisors—It is sometimes very difficult for upper division students to drastically change their cumulative GPA in one semester because they have accumulated a larger amount of units.  Therefore getting them off probation is a bigger challenge.  Our goal is to retain 60% of these students from fall 2012 into the spring 2013 semester.

Spring 2013: First-time freshmen on academic probation in the following colleges: Social and Behavioral Sciences, Education, and Pre-Majors, i.e. Pre-Pharmacy, Pre-Physiology—Our goal is to retain 70% of these students from spring 2013 into the fall 2013 semester.

Fall 2013: Freshman continuing on academic probation—Our goal is to retain 60% of these students from fall 2013 into the spring 2014 semester.',
'created_at' => '2012-01-26 01:14:54.000000',
'question_id' => 149,
),
117 => 
array (
'answer_text' => 'For fall 2011 there were a total 3162 students (freshmen-senior) on probation, 2167 (68.5%) of them received financial aid.',
'created_at' => '2012-01-26 01:16:19.000000',
'question_id' => 148,
),
118 => 
array (
'answer_text' => 'Scholarship Universe (SU) allows students to self-report if they applied for a scholarship and if they won a scholarship that they were matched to in the system. Since SU\'s launch on November 1, 2010 - 3,767 students applied for 11,167 scholarships and 922 of those students won 2,165 scholarships. Therefore, 24.5% of the students who reported having applied for a scholarship also reported that they had won a scholarship. Or, 19.4% of the scholarships applied for were reported as won.',
'created_at' => '2012-01-26 23:18:15.000000',
'question_id' => 150,
),
119 => 
array (
'answer_text' => 'Scholarship Universe (SU) gives UA students a competitive advantage for a limited number of available scholarships. However, it is difficult to speculate how UA student success would be affected if SU is licensed to other colleges. The actual impact may depend on several factors. Such as the geographic locations of other colleges, the number of colleges and the number of students in those colleges using the system. Also, the system might be able to be licensed as an entire package, as a system without the database of scholarships included, or as a system with the departmental functionality only. Some colleges may want all or nothing. Some may find one aspect most attractive because of their needs and budget. Advertising on the website may be another viable option in addition to or in lieu of licensing. Any revenue from licensing or advertising could help run the program and return to Student Affairs and UA students. The plans to license or to allow advertising are very young as the departmental phase needs to be developed and the copyright needs to be completed. All factors will need to be explored to determine what is best for UA students and the program. No decisions will be made without weighing all factors and getting proper authorization.',
'created_at' => '2012-01-26 23:32:00.000000',
'question_id' => 151,
),
120 => 
array (
'answer_text' => 'Based on student reported data through Scholarship Universe, we know the aggregate dollar amount is greater than $2,369,700 because it is only accounting for 72.9% of scholarships reported as won. The remaining 27.1% is comprised of 591 scholarships with unknown dollar amounts. "Varies" was listed on 483 scholarships and 108 scholarships had dollar ranges listed starting as low as $50 and rising as high as $20,000. Therefore the true aggregate is the $2,369,700 plus the 591 scholarships with indeterminable amounts.',
'created_at' => '2012-01-26 23:37:25.000000',
'question_id' => 152,
),
121 => 
array (
'answer_text' => 'Aside from winning a small grant from the Parents and Family Association, the first step to secure outside investment is the copyright process. A copyright should be completed before pursuing licensing and advertising. The copyright is nearly ready to be submitted to the UA\'s Office of Technology Transfer for review. The second step to find outside investment is promotion at the National Scholarship Provider\'s Association (NSPA)conferences that included hundreds of school and foundation scholarship professionals from around the country including, Canada. However, even though the Scholarship Universe (SU) team is well organized and efficient, it is a small team comprised of one full-time employee and eight part-time students. Therefore, the SU team must focus most of its time on the maintenance and development of the system. Additional resources are needed to allow more time for exploring funding possibilities and for all the other goals mentioned in Appendix (B) of the proposal.',
'created_at' => '2012-01-26 23:46:40.000000',
'question_id' => 153,
),
122 => 
array (
'answer_text' => 'The application process for selecting Executive Board members happens every year after our trips have returned from Spring Break.  We market the applications over fliers distributed around campus, listservs, and Facebook.  The selection process is done by the advisor for Alternative Breaks, along with members of the Executive Board who are not returning next year (usually this just means graduating seniors).  All students who apply are given an interview.  Based on their interview answers and application, each applicant is ranked, and then positions are offered.',
'created_at' => '2012-01-28 01:40:02.000000',
'question_id' => 155,
),
123 => 
array (
'answer_text' => 'Currently, we are looking to secure outside grant monies on a long-term basis that would ensure the continued funding for Alternative Breaks.  In the past, we have been fortunate enough to receive several one-year grants through not only on-campus departments such as RHA and the Student Services Fee Committee, but also through Liberty Mutual Financial.  

Specific step currently being taken:
We are in the process of researching grant opportunities along with reaching out to potential donors and grantors to begin the communication process with them about potential opportunities.  Over the next three years, we hope to continue this and to secure permanent funding.',
'created_at' => '2012-01-28 01:50:10.000000',
'question_id' => 156,
),
124 => 
array (
'answer_text' => 'We are actually not allowed to do that, given how funding and budgets work for departments at the University of Arizona.  Please note that this is different than how funding works for student clubs and organizations.

Here is a brief rundown of how departmental funding works here at the University of Arizona.  At the beginning of each fiscal year (July 1), departments and programs are given a pre-determined budget.  For example, a fictional department on campus has $80,000 to work with.  Whatever has not been spent of that $80,000 as of June 30th of the following year gets returned to the University, and they begin again with $80,000 on July 1.  

This is the same way even with extra money we bring in.  If our fictional department has a bake sale and brings in $1,000 in extra money to supplement our $80,000 budget, that money must still be used by June 30th or it will go away and the department starts over again on July 1 with its pre-determined budget.',
'created_at' => '2012-01-28 02:07:33.000000',
'question_id' => 157,
),
125 => 
array (
'answer_text' => 'In evaluating the Advocacy and Assistance program offered by the Dean of Students, it is clear that the original concept and the actualization of the program have subtle differences.  Originally, cell phone cost and marketing were the bulk of operational costs requested in prior years.  In this request our operational costs are focused again on marketing for $2000 to allow for printed materials for outreach.  Additionally, the staff has experienced great success in targeted outreach at appropriate programs on campus. In order to engage students at these events, the coordinators will provide snacks that will draw attention to the table and allow them to have one on one interaction.  The anticipated cost per year for these snacks is $1000. Lastly, the coordinators work with students experiencing a variety of challenges and in order to best serve their needs, professional training will continue to be critical so we have included $1000 for such trainings.',
'created_at' => '2012-01-28 05:57:52.000000',
'question_id' => 167,
),
126 => 
array (
'answer_text' => 'Student Advocacy & Assistance requires engaging with many departments and colleagues on campus in order to successfully help UA students. These collaborative efforts allow us to condense many complex bureaucratic processes into one thoughtful process.  There are many opportunities for this service to be more collaborative in its delivery and in visioning the future of the service our intention is to outreach and create a wider web of support.  With three years of financial support from the Student Services Fee, we are confident that these connections can move from vision to reality allowing the Dean of Students to provide outstanding service to UA students independent of the SSF funding.',
'created_at' => '2012-01-28 07:04:10.000000',
'question_id' => 168,
),
127 => 
array (
'answer_text' => 'The amount under "Other Projected Sources of Funding" that is indicated as being provided by the Student Services Fee is the total projected funding over 3 years. The other projected sources of funding are indicative of per year funding (i.e. we have or plan to receive them for FY 2011-2012). These sources are expected to continue for the next three years, however, the exact amounts may differ, so they were not estimated. Some sources, such as\' ASUA Funding\', may not increase proportionally to membership.',
'created_at' => '2012-01-28 18:53:42.000000',
'question_id' => 163,
),
128 => 
array (
'answer_text' => 'In 2010-2011, we had 67 club members total in the fall. In 2011-2012, we had 121 club members total in the fall.',
'created_at' => '2012-01-28 18:58:09.000000',
'question_id' => 164,
),
129 => 
array (
'answer_text' => 'The UA is insured for liability arising from its programs and activities by the State of Arizona through a statutory self-insurance program.  This insurance program covers the acts and omissions of university officials, employees, and volunteers while acting in the course and scope of employment and/or authorized duty.  The UA Student Emergency Medical Services (SEMS) program will function as a volunteer service on behalf of the UA, and as such the program and individual members will be insured for liability incurred while acting in an official capacity.  There will be potential UA exposure to liability arising from the SEMS program.  However, the SEMS program has been evaluated by Risk Management Services, the UA Police Department, and other university administrators, and it has been determined that the SEMS program is appropriately designed to manage the risks associated with providing these services .  Further, it has been determined that the overall benefit in terms of enhanced response to campus medical emergencies justifies the increase in institutional liability risk.  Questions about how UA insurance will apply to SEMS may be directed to Steve Holland, UA Risk Management Services Dept. at 621-1556 or sholland@email.arizona.edu.',
'created_at' => '2012-02-02 02:58:46.000000',
'question_id' => 160,
),
130 => 
array (
'answer_text' => 'The justification for requesting more funding for equipment is that this organization requires materials that are disposable and need replacing every year. For instance, you cannot reuse gloves, epi-pens, BVMs, etc. for different patients. Also for the equipment that is supposed to last for many years such as radios, bikes, backboards, etc. we have only purchased the bare minimum we need to get this organization off of its feet, however as UA SEMS begins to grow, the demand for more radios, bikes, etc. will increase.  We intend on using this additional start up funding to expand our coverage to multiple response crews per shift.  This increase in personnel per shift will allow each crew to be assigned to sectors of campus which will allow for a greater reduction in the time it takes for treatment initiation after a medical emergency call to 911 is made.',
'created_at' => '2012-02-02 03:10:45.000000',
'question_id' => 161,
),
131 => 
array (
'answer_text' => 'This organization is constantly looking to make ties with other organizations for future funding. For instance, UAPD has offered multiple 20 hour bike training courses at their own expense to all the participants in this organization. Our relationship with Campus Health has led to them offering our organization disposable medical supplies at a discounted rate. ASUA has given our organization space in there office to store and maintain our equipment/records. Also, we have been working closely with Tucson Fire Department, Risk Management and Southwest Ambulance, which hopefully will allow this organization to expand in the near future. As UA SEMS gains more exposure and becomes recognized as a professional organization by other entities the opportunities to create partnerships that can generate funding will increase.',
'created_at' => '2012-02-02 03:36:24.000000',
'question_id' => 162,
),
132 => 
array (
'answer_text' => 'Site Leader Position Description:
1.  Site Leader is responsible for leading a group of up to 15 students to a local Tucson service site each week of the academic semester.
2.  Site Leader will be responsible for facilitating post-service reflection each week for the group of student volunteers.
3.  Site Leader will serve as the primary point of contact for their service agency.
4.  Site Leader will attend monthly staff meetings with the Coordinator for Student Programs, Graduate Assistant for Community Engagement, and other Site Leaders.
5.  Site Leader will attend weekly 1:1 with the Graduate Assistant for Community Engagement.',
'created_at' => '2012-02-02 19:21:56.000000',
'question_id' => 159,
),
133 => 
array (
'answer_text' => 'I have just e-mailed the budget breakdown to Joel Hauff in Excel format.  Joel will provide a copy to the Committee.',
'created_at' => '2012-02-02 23:42:26.000000',
'question_id' => 158,
),
134 => 
array (
'answer_text' => 'The Next Steps Center was launched by the Office of Admissions to aid students through the matriculation process.  The physical center is housed in the Office of Admissions call center, and through the Student Services Fee, we are able to hire 8 more students this year.  We will continue the excellent service to our future Wildcats if the fee is not approved, however the number of student hours would diminish (would not be able to hire as many students) which could cause longer hold/wait times for these students and parents.',
'created_at' => '2012-02-06 22:55:12.000000',
'question_id' => 171,
),
135 => 
array (
'answer_text' => 'The Dean of Students Office is under the leadership of a new Dean – Dr. Keith Humphrey.  Dean Humphrey will be re-evaluating the funding models for our office and anticipates requiring fewer Graduate Assistants for the 2013-2014 academic year.  Dean Humphrey is hopeful at that time that the Dean of Students Office might be able to secure additional funding which would help the Office to pay for more of the needed Graduate Assistant positions.',
'created_at' => '2012-02-07 17:28:53.000000',
'question_id' => 169,
),
136 => 
array (
'answer_text' => 'Seven of the eight Graduate Assistants are being retained for next year.  The GA who is leaving is doing so because she is graduating.',
'created_at' => '2012-02-07 17:31:21.000000',
'question_id' => 170,
),
137 => 
array (
'answer_text' => 'This year\'s marketing team has successfully expanded distribution, created productive street teams, and sponsored revenue-generating business partnerships on the mall. We can do more in all these efforts with the addition of 2 marketing associates AND launch anticipated new initiatives in social networking services and app development (working with local businesses on building facebook pages and apps).

But most of the new funds ($13,000) we are requesting are going to the creative end. Our goal is to diversify product and service offerings in building mobile ads, newsstand billboard campaigns, and multimedia and web ads -- beyond the time or capacity of existing production staff. That\'s why our marketing application includes funds for 3 student graphic designers. Their efforts are, in part, also an answer to the question about using marketing resources to promote readership. Thank you for the opportunity to expand on this and I hope this response helps.',
'created_at' => '2012-02-08 21:15:14.000000',
'question_id' => 174,
),
138 => 
array (
'answer_text' => 'There is a role for our marketing staff to create collaborative materials (with the Wildcat web staff) promoting online readership. The nature of specific campaigns will really have to be determined by the staff but the kinds of things that I would hope for include more giveaways, online contests, and cross-media (print-online) sponsorships. But the core responsibility for driving traffic will be on the content end. That\'s why in our application we are seeking funds just for staff on the content development side -- making an improved product. The web staff will also have (as they do now) social media responsibilities for managing our tangled web of twitter and facebook accounts.

More significantly, our marketing team *does have* a role in supporting digital ad sales. It\'s not just traffic; we need to monetize our digital products, which now account for only about 5% of revenue. And not just banner ads, which are somewhat passe. Almost 15% of the Wildcat\'s web traffic comes from mobile devices -- a number bound to grow -- and we have not effectively monetized that traffic. Our marketing staff will be involved in those efforts. About 2,000 readers (and growing) get the Daily Wildcat email edition, which of late has carried a Jim Click ad, but we need to open up more inventory and provide stronger marketing support to the sales team to push our ad efforts deeper into the digital ecosystem. This is new territory. We know that. But it is also a good opportunity for students to gain experience with digital-age marketing tools.  Thank you for the opportunity to elaborate on this question.',
'created_at' => '2012-02-08 21:28:03.000000',
'question_id' => 173,
),
139 => 
array (
'answer_text' => 'We use google analytics. 

For the fall 2011 semester, per month, we averaged:

All visits:  92,116
Unique:      72,642
Page views:  197,423

Because we had some hangups in switching CMS early in the semester, we believe the numbers were impacted on the low side in August and September. The stats for October and November show, on average per month:

All visits:  103,943
Unique:       82,945
Page views:  215,178

14.4% of web traffic was accessed via a mobile device during the fall. A year ago it was 1.76%.

Thank you for this opportunity to provide this information. In our application we noted that the website "averages about 200,000 visitors a month". That number clearly refers to pageviews.',
'created_at' => '2012-02-08 21:39:58.000000',
'question_id' => 172,
),
140 => 
array (
'answer_text' => 'Thank you for your question.

Historic information on funds raised each year is not available.  We are reliant on systems managed by the UA Foundation and other University operations, which have merged old historic information from a variety of sources into a newer system. For a number of reasons, it is not easy to capture your requested information in the timeframe given.

However, please note that there have been many successful efforts. A few of these are highlighted below. ALL monies raised within the Division of Student Affairs directly benefit students.

•	Campus Health:	
The Edward & Jonathan Linden endowment was created to establish the Friend 2 Friend Program in CAPS to helps students know how to help their friends in trouble. Verizon has recently provided funds to support OASIS in its efforts to educate the campus community around sexual assault and relationship violence.

•	Career Services:
Various corporate partners have provided funds to expand services to students, including Liberty Mutual who has supported the Edge Series  - a professional development speaker’s series open to all students and Apple who has funded the Student Resume Guide.

•	ASUA:
The $250,000 Ray Cammack Shows endowment has provided over 45 students with the opportunity to participate in student government who have otherwise might not been able to participate due to financial constraints.

•	Fraternity & Sorority Programs:
The Hunter White Advocacy Program provides opportunities for students to become peer health advocates and provide advocacy, information and resources around health issues. 

•	Parents & Family Association (PFA):
Over a million dollars has been raised to date to support the PFA campus grants program. This program returns monies raised back to campus departments to directly benefit the broadest cross-section of the student body. 

•	Student Transitions:
The New Start Endowment was established to provide financial assistance to students who were recent participants of the New Start Summer Bridge Program.

•	To Be Announced Soon:
A seven-figure gift will be announced in the coming months that will directly benefit one of the student affairs units. 

This year priorities for fund raising include – student learning support, health and wellness initiatives, career services and leadership development opportunities.',
'created_at' => '2012-02-09 21:01:09.000000',
'question_id' => 176,
),
141 => 
array (
'answer_text' => 'Thank you for your question.

To ensure our fund development efforts are successful we must be good stewards of the gifts wehave already received and continue to engage donors, friends and alumni with the programs and services that make a difference in the student experience. As a result, cultivation activities can take many forms.

The Division of Student Affairs is one of the largest segments of the University and the fund development needs and related support activities are great. The graduate assistants will be critical in enhancing our ability to communicate and provide quality donor and alumni relations’ experiences. It is critical that we are keeping individuals who care about the student experience informed about the excellent programs, the experience student’s are having, the points of pride, and the needs – so that we can continue to ensure quality and excellence throughout the division.

Currently the infrastructure that exists for this is limited and is of upmost priority if the division is going to maximize its ability to obtain more resources to ensure an extraordinary student experience.

Some of the specific ways that they will assist Student Affairs Outreach & Development are:

·      Communications:
o   Develop e-newsletter to send to alumni, friends, donors
o   Develop social network presence for Student Affairs
o   Assist with specific event related communication pieces
o   Content management for specific web pages
o   Calling individuals  
o   Assist with donor and alumni relations activities
o   Outreach to Student Affairs departments to assist fund development and outreach team with specific tasks and needs

·      Coordinating donor and alumni relations activities including:
o   Recognition programs (e.g. honoring individuals, recognizing gifts received, thanking those who have given)
o   Campus visits for donors and prospective donors
o   Special events in certain priority cities (dinners, alumni gathering’s)
o   Special events – Homecoming, Family Weekend, other anniversaries

Other:
o   Manage specific projects (e.g. management of alumni and donor database’s)

I hope that this provides you with the information you desired. Please let me know if you have any other questions.',
'created_at' => '2012-02-09 22:37:40.000000',
'question_id' => 179,
),
142 => 
array (
'answer_text' => 'Thank you for your question.

To ensure our fund development efforts are successful we must be good stewards of the gifts we have already received and continue to engage donors, friends and alumni with the programs and services that make a difference in the student experience. As a result, cultivation activities can take many forms. 

The Division of Student Affairs is one of the largest segments of the University and the fund development needs and related support activities are great. The graduate assistants will be critical in enhancing our ability to communicate and provide quality donor and alumni relations’ experiences. It is critical that we are keeping individuals who care about the student experience informed about the excellent programs, the experience student’s are having, the points of pride, and the needs – so that we can continue to ensure quality and excellence throughout the division.

Currently the infrastructure that exists for this is limited and is of upmost priority if the division is going to maximize its ability to obtain more resources to ensure an extraordinary student experience. 

Some of the specific ways that they will assist Student Affairs Outreach & Development are:

•	Communications:
o	Develop e-newsletter to send to alumni, friends, donors 
o	Develop social network presence for Student Affairs
o	Assist with specific event related communication pieces 
o	Content management for specific web pages
o	Calling individuals  
o	Assist with donor and alumni relations activities
o	Outreach to Student Affairs departments to assist fund development  and outreach team with specific tasks and needs

•	Coordinating donor and alumni relations activities including:
o	Recognition programs (e.g. honoring individuals, recognizing gifts received, thanking those who have given)
o	Campus visits for donors and prospective donors
o	Special events in certain priority cities (dinners, alumni gathering’s)
o	Special events – Homecoming, Family Weekend, other anniversaries

•	Other:
o	Manage specific projects (e.g. management of alumni and donor database’s)

I hope that this provides you with the information you desired. Please let me know if you have any other questions.',
'created_at' => '2012-02-09 22:46:33.000000',
'question_id' => 175,
),
143 => 
array (
'answer_text' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur',
'created_at' => '2012-02-09 23:10:46.000000',
'question_id' => 179,
),
144 => 
array (
'answer_text' => 'The most important aspect of our current and previous collaborations that will lead to future funding is the professional relationships that we have established and will establish with various groups and individuals. For example, we have worked with the multi-cultural centers for several years, including this year to help bring Tim Wise. The multi-cultural centers, in turn, find sponsors and help to contribute even more funding for our events. This is just one example of the collaborators that the speaker series searches for. Through our collaborations with organizations such as the Women’s Resource Center, Residence Hall Association, LGBTQ Affairs, and more, we are capable of reaching even more off campus sponsors. As these sponsors see the success of our events and our willingness to work with them, they are more likely to support future events with even greater sponsorships. Thus, through the years we gain the trust of both on campus and off campus sponsors, allowing us to funding for years to come.',
'created_at' => '2012-02-10 01:52:05.000000',
'question_id' => 178,
),
145 => 
array (
'answer_text' => 'This program would support a wide number of initiatives in the Annual Survey Results. Many of them would be dependent on the speakers that are chosen to speak at the University. 

•	Expanded Career Related Opportunities – Speakers dealing with business and entrepreneurial pursuits or those closely related to an individual’s major could open up career related opportunities. Internship opportunities or helping to expand a student’s desire to pursue a career in a certain field or even getting students to think about a specific area in which they would like to devote their lives are what we can offer through the speaker series. 

•	Personal Leadership Development Opportunities and Programs – Bringing in speakers to run workshops as well as talking about leadership and personal growth help to meet this criterion. Our hope is that we can bring in speakers who educate students to inspire lasting change in their communities and lead lives of service, purpose, and leadership though individuals we bring to campus.

•	Outside Classroom Faculty-Student Interaction – The more we can engage faculty in our speaking engagements, the better. We always look to work with academic departments especially if one of our speakers parallels their work. We currently run a Last Lecture Series, which is all about student/faculty interactions. We would love to bring more of these interactions into the speakers we bring. For example, we would work with Political Science faculty if we bring in a political speaker.

•	Opportunities to engage in Fine Arts Activities Outside of the Classroom – We hold workshops associated with some of our speakers. We did a workshop with the Gorilla Girls and have looked at possibilities with other speakers. We are also looking at working with existing annual events such as our Campus MovieFest where we have more than 50 student groups make 5 minute movies for a competition. We have brought prominent director Spike Lee and actor/director Adrien Grenier and are very interested in bringing other directors or speakers to discuss the movie making process and other fine arts. 

•	Social Justice Programs—The speaker series looks for opportunities to bring social justice speakers every semester. Last semester, we brought Better World Shopper speaker Dr. Ellis Jones and held a screening for Invisible Children with discussion from a Ugandan speaker. This semester, we are bringing Tim Wise and holding a screening for Invisible Children’s newest film with discussion from another Ugandan speaker. In the past, we have brought Jamie Tworkowski from To Write Love in Her Arms, Mark Zuppan from Murderball, the Guerrilla Girls, and more. Social justice is an area of great emphasis for the speakers series, and we will continue to provide outstanding social justice speakers.

We can go in many directions with this program. Through our research, we have found that some speaker series at other universities have a stipulation with receiving funding that one speaker must be at least a fine arts, political speaker, or some other genre that the student body wants to make sure is represented every academic year.  We would not be opposed to this if the Student Services Fee Board feels it appropriate.',
'created_at' => '2012-02-10 02:02:12.000000',
'question_id' => 177,
),
146 => 
array (
'answer_text' => 'The Graduate and Professional Student Council (GPSC) is currently requesting $20,000, instead of the $15,000 that SSFAB has awarded in previous years, for multiple reasons. First, in the Fall 2011 semester, the GPSC (using non-SSFAB funds) hired a Club and POD Funding Administrator to coordinate outreach and advertising of this funding opportunity to graduate and professional students across the UA community. This person will continue to work with specific colleges and student groups to improve awareness of the POD program and improve the application process.  We expect the expanded outreach and advertising to increase the number of POD funding applications the GPSC receives over the course of the next two years.  Even now, the GPSC has witnessed a spike in the number of POD applications.  Just within the last three grant rounds (4 months), after hiring the new Club and POD Funding Administrator, the number of applications has increased from about 0-2 per month to 4-5 per month. 

Second, in Fall 2011 alone, the GPSC pledged nearly $9,000 among 8 POD events across the graduate and professional community.  With the number of applications increasing each grant round, this semester is likely to surpass that value based on the number of quality applications.  Therefore, the GPSC will almost certainly award at least $18,000 in FY2012.  Similarly, with outreach and demand increasing, the GPSC believes it will almost undoubtedly receive enough quality POD applications to fund $20,000 in each of the next two years.

The underlying reason for requesting an increase in funding is based on the received quantity of quality applications, as well as the anticipation of the continuing increase in number of future applications. Our hopes of continuing to provide much needed financial support to graduate and professional student development at the UA relies on our ability to allocate sufficient resources through our POD funding.',
'created_at' => '2012-02-16 21:25:36.000000',
'question_id' => 180,
),
147 => 
array (
'answer_text' => 'We will not know the longevity of this program until the next fiscal year.  Next year, ASUA will begin talking with the Bookstore to renegotiate our contract with them, and we hope to find a way to funnel more money into the Club Funding initiative. Currently, we do not have the monetary resources to help clubs and organizations without the help of the Student Services Fee.  We hope to one day be able to fund all Clubs and Organizations funding requests, but until then the contribution from the Student Services Fee is vital.',
'created_at' => '2012-02-16 22:06:51.000000',
'question_id' => 184,
),
148 => 
array (
'answer_text' => 'We received 203 requests last year.  The amount requested from UA Clubs and Organizations for 2010-2011 was $304,579.68.  In Fall 2010, UA Clubs and Organizations requested $111,433.38, and received $54,707.59.  In Spring 2011, Clubs and Organizations requested $193,146.30 and received $79,572.24.  The 2010-2011 Appropriations Board appropriated a total of $134,279.83.  I will email the spreadsheet with the funding break down to Leo Yamaguchi.',
'created_at' => '2012-02-16 22:07:14.000000',
'question_id' => 183,
),
149 => 
array (
'answer_text' => 'As of Monday February 13th, we have received 101 applications for Club Funding.  We are about to enter our busiest time of the year, when we see, on average, 15 clubs and organizations per meeting. On Monday Feb. 13th, we saw a total request of $20,098.84 for the meeting, and funded $6,979.73.',
'created_at' => '2012-02-16 22:07:30.000000',
'question_id' => 182,
),
150 => 
array (
'answer_text' => 'There are many reasons why we are requesting $125,000 from the Student Services Fee.  Last year, UA Clubs and Organizations requested a total amount of $304,579.68, and we were only able to fund $79,572.24.  We are still requesting lower than what our clubs and organizations need to fully fund every request that came through our office last year.  According to our bylaws, to be a recognized organization within ASUA, each club has to have at least 5 members.  With 600 organizations recognized through ASUA, we have at least 3,000+ students involved with Clubs.  The extra $25,000 requested this year will allow the ASUA Appropriations Board to fund more of what UA Clubs and Organizations want and need to be a successful organization on a campus level, and on a national level.',
'created_at' => '2012-02-16 22:07:51.000000',
'question_id' => 181,
),
151 => 
array (
'answer_text' => 'Yes, as currently negotiated with LWC, the annual $25,000 award would exclusively fund graduate and professional students. 

However, the ability for undergraduate students to receive money from LWC funds would not be affected in any way.  The conditions as currently negotiated with LWC would ensure that $25,000 of SSFAB money would not be used to supplant existing LWC funds.  This means that LWC would continue to award its funds according to their established rules on a need-only basis, regardless of the person’s status (i.e. undergraduate, graduate, or professional student).  The additional funds would come from SSFAB to support graduate students.  These funds would not be provided for from existing LWC funds.  In case SSFAB monies exceed the additional need of graduate students and there were leftover LWC funds, the LWC could use their remaining funds to support more undergraduate students than without the additional SSFAB funds.

The GPSC is aware of the need for childcare subsidies among undergraduate students.  This is detailed in LWC statistics—which was provided in our original application.  The GPSC concedes that the current conditions could potentially exclude undergraduate students in need of childcare support subsidies from SSFAB monies.  Hence, we are willing to drop this provision, as the principal desire of our application is to support all students who need child care support.  We hope that SSFAB and undergraduates consider our willingness to drop the “graduate-student only” provision as a statement of our desire to work with the undergraduates to provide inspirational and financial support for the greater student body of the University of Arizona.',
'created_at' => '2012-02-16 23:38:26.000000',
'question_id' => 185,
),
152 => 
array (
'answer_text' => 'All students are eligible to apply for the JEDI internship program; therefore, marketing is very important and we will share this information broadly across the campus.  To market and recruit students for the credit bearing and paid internship opportunities, we will use numerous strategies of announcing this opportunity to students.  These strategies include:

-Posting flyers in high traffic student areas
-Providing academic departments on campus with email marketing announcing the opportunity
-Posting announcements on FaceBook and Twitter
-Utilizing the listservs of all six partnering offices, plus distributing email marketing to all offices throughout Student Affairs.',
'created_at' => '2012-02-23 22:06:08.000000',
'question_id' => 190,
),
153 => 
array (
'answer_text' => 'The one credit elective course offers students pass/fail credit and is facilitated by students who’ve been trained in the Intergroup Dialogue method. Part of the $5,000 will be used to provide student facilitators with additional training in facilitating sensitive subjects through opportunities offered by the Social Justice Training Institute and the White Privilege Conference. The rest of the funding will be used to incentivize participation in the internship with refreshments and t-shirts, to develop marketing materials, and to support the purchase of supplies and any additional training.',
'created_at' => '2012-02-23 22:07:12.000000',
'question_id' => 191,
),
154 => 
array (
'answer_text' => 'The $18,000 will be divided equitably between the six offices of AASA, APASA, CHSA, LGBTQ Affairs, NASA, and the WRC as $3,000 distributions. Each intern team working with their professional mentors, will then use the funding to support the implementation of new events and initiatives developed by the JEDI team. Examples of operations expenses are facility fees, food costs, speaker fees and travel expenses, film rights, event marketing, parking, etc.',
'created_at' => '2012-02-23 22:08:04.000000',
'question_id' => 192,
),
155 => 
array (
'answer_text' => 'The addition of the Student Affairs Outreach Facilitators and Transfer Student Center Outreach Facilitators and Graduate Student Assistant will both enhance the functions in those areas as well as expand the pool of student assistants to support special events and activities.  The addition of these student positions will also enhance the numbers and types of students who have access to support services. 

While many programs target specific student populations, Student Affairs Outreach works not only with a targeted student population (off-campus freshmen) but with students across demographic affiliations (year in school, campus involvement, faculty referral, etc.). 

The expansion also addresses the needs of prospective and new transfer students. Transfer Student Assistants will serve as Outreach Facilitators in the Transfer Student Center corresponding with Pima Community College students who have indicated their intent to transfer to the UA; and responding to prospective and newly admitted transfer student questions and concerns. 
The Graduate Assistant and Outreach Facilitators will engage them in workshops and activities designed to facilitate their transition to UA campus.',
'created_at' => '2012-02-23 22:35:28.000000',
'question_id' => 186,
),
156 => 
array (
'answer_text' => 'I have included a brief job description for each role:

The Student Assistants in Academic Success & Achievement (ASA) are responsible for answering telephone calls-directs caller, based on nature of call, to appropriate program or person or takes message from caller; answering inquiries and provides factual information pertaining to work unit verbally or by sending relevant literature through mail; greeting visitors, determining nature of business, directing them to the appropriate program or person and notify appropriate person of visitor\'s arrival; answering inquiries and provide factual information verbally or by giving visitor relevant literature; receiving, date stamping, separating and distributing incoming mail or other materials to work unit members; determining appropriate recipient based on subject of correspondence or mail; helping visitors complete departmental forms correctly by answering questions or perusing forms for completeness; performing a variety of clerical duties pertinent to type of work unit or organization such as filing and retrieving correspondence and typing various department forms, data entry or documents; stuffing, addressing and labeling envelopes; photocopying; representing the unit as a whole at recruitment events when necessary, i.e. New Student Orientation.

The additional Student Assistants (15 Outreach Facilitators) in ASA while supporting the larger efforts of Student Transitions will each be assigned a group of off-campus freshmen for the academic year.  Each week they will send an email/newsletter to their students that includes important university resource information.  They will maintain the Wildcat Connections Facebook page that includes blogs written by the students about first-year experience issues: places to study, registration, money management, etc.; they will hold office hours each week; and will perform targeted email and phone call outreach, i.e. centered around registration; immunization holds; advisor holds; bursar holds; they will also track all student interactions and referrals to better understand how we can continue to support our students’ efforts to access resources and become engaged in the campus community.

Student Assistants for the Career Services Ambassador Program will continue to provide expanded outreach to the student body about Career Services and opportunities across disciplines.  The ambassadors have presented to student groups and organizations, provided outreach through tabling on the mall and/or in strategically located high student traffic areas, assisted with career fair and related employer events.  Their reach to students has expanded Career Services outreach by as much as 5,000 students throughout the year.  These student leaders have further developed their capacity to serve other departments needing peer to peer outreach and program assistance throughout the transitions areas.

The Student Assistants assigned to Leadership Programs would be responsible for tracking assessment data for all leadership programs (approximately 400 events/programs per year); marketing/outreach for the two newest programs, the Minor in Leadership Studies and Practice and the National Collegiate Leadership Association; monitoring hours and progress of all interns (approximately 100 per semester) from Leadership Programs, Fraternity and Sorority Programs, ASUA, Women’s Resource Center, and LGBTQ Affairs; overseeing curriculum and resources for course instructors.

The Student Assistants (National Student Exchange Ambassadors) in Transfer Student Services work to increase campus awareness and student participation in the NSE program through presentations in residence halls, classrooms, and outreach activities on campus.  NSE Ambassador’s outreach and marketing efforts have a direct impact on the growth of the program, their duties include:  updating and maintaining the UA NSE website; presenting to general campus groups; tabling on the UA mall and at other campus events; holding walk-in appointments with prospective NSE students; NSE Ambassadors also coordinate orientations and special programs and events for incoming and outgoing NSE students.

The Transfer Student Center is requesting additional staff (1 GA, 5 Transfer Student Assistants) to support other programmatic functions:

The Graduate Assistant will supervise the Transfer Student Assistants Outreach Facilitators and work with the following programs and activities: Transfer Tuesdays, Faculty Fellow Program, Transfer Student Welcome Programs, and outreach & communication with transfer students.  The GA will also advise transfer student groups (UA chapter of Tau Sigma and Transfer Student Association) and support the National Student Exchange Program (NSE).

The additional Student Assistants will serve as Outreach Facilitators in the Transfer Student Center. They will greet and welcome prospective and new transfer students to the Transfer Student Center; speak to visiting groups of prospective transfer students; maintain phone and email contact with prospective and newly admitted transfers; support walk-ins and appointments with new and prospective transfer students; provide administrative and front desk support and assist with center programs and workshops.',
'created_at' => '2012-02-23 22:36:51.000000',
'question_id' => 187,
),
157 => 
array (
'answer_text' => 'Our employee cross training occurs in the fall semester.  Training includes a 5 hour joint training to train on each department and roles during collaborative events as well as provide leadership development for success in these roles.  We will use a common performance evaluation and self-reflection tool across all student assistants. 

Many or all Student Assistants are pulled throughout the year to work large events within Student Transitions.  Some of these duties include NSE Outreach/Program Assistance, Career Fairs, Commencement ceremonies, New Student Orientation/Expo, Focus on Success, Meet Your Mentor Event and Field Day for Blue Chip.',
'created_at' => '2012-02-23 22:37:15.000000',
'question_id' => 188,
),
158 => 
array (
'answer_text' => 'If the STSA-Expansion Proposal is funded for two years, Student Transitions will cover the training costs of $3,000 for the second year.  It will be shared and paid for out of the operations budgets of the participating departments, i.e. Career Services, Leadership Programs, Transfer Student Center and Academic Success & Achievement.',
'created_at' => '2012-02-23 22:38:00.000000',
'question_id' => 189,
),
159 => 
array (
'answer_text' => 'Please accept my sincere apology for any confusion.  This position I am requesting in the adjustment is in fact an appointed position.  I apologize for stating that we would hire the coordinator as an “Extended Temporary Employee”, in my double-checking have realized this only qualifies for classified status.  I clearly made a mistake.  We will be hiring for an appointed position and would be clear before offering the job the most qualified candidate that this is a one-year contract for this particular program.  Please let me know if I can clarify any further.  Thank you!',
'created_at' => '2012-03-31 00:22:14.000000',
'question_id' => 194,
),
160 => 
array (
'answer_text' => 'Here\'s an answer to your question.',
'created_at' => '2012-10-12 23:00:22.000000',
'question_id' => 195,
),
161 => 
array (
'answer_text' => 'Here\'s another answer!',
'created_at' => '2012-10-16 00:43:29.000000',
'question_id' => 198,
),
162 => 
array (
'answer_text' => 'The Speakers Series will benefit directly from attending the NACA West conference in several ways. The conference presents an opportunity to network with a multitude of agents and representatives from various speakers committees, as well as a large number of students in similar event planning capacities from other universities. We will have the opportunity to form valuable relationships with these individuals that can serve to benefit us in the future. NACA also books a good number of performances to take place during the conference, giving us a chance to witness these performers first-hand, and better draw conclusions. Finally NACA also runs education sessions that cover a wide range of event planning skills and techniques, leadership methods, and other beneficial material. All of these things will benefit my committee immensely and help us to put on more successful events in the future.',
'created_at' => '2012-10-16 23:22:33.000000',
'question_id' => 197,
),
163 => 
array (
'answer_text' => 'The Wildcat Events Board has benefited directly from attending the NACA WEST conference for the past 4 years in a variety of ways. One of which is that the conference gives us the opportunity to secure a reduced "conference price", as well as to work with other colleges to block book talent to save on travel and sometimes receive other discounts. Acts WEB has brought in the past directly from NACA include comedian Johnnhy Walker, the cast of  Reno 911, various Bands, Acoustic artists, and Gabriela Mendoza Slam poet. We have also booked many events resulting from our networking with agents and have also been able to receive those events at reduced prices. In addition to that many of our student leaders have directly benefited from educational sessions with improved leadership and event planning skills.',
'created_at' => '2012-10-16 23:51:08.000000',
'question_id' => 197,
),
164 => 
array (
'answer_text' => 'Employer and Career Development Program – Acceleration
Response to Fee Committee Question 1/18/13

How did you arrive at 10 for the number of Ambassadors?

Student Ambassadors (10) – 2 cross disciplinary teams of 5 to provide student and employer outreach (10@ 10 hours per week or equivalent of 100 hours/week)

•Student Ambassadors: Peer to Peer Career Outreach Team (5) – This team of 5 would provide peer to peer outreach to inform students in their colleges of career related opportunities, engage them in programs, services and employer events.  
•Student Ambassadors: Employer Outreach Team (5) - This team of 5 will provide research, employer outreach and VIP services to targeted employers to engage them in posting positions and internships, participate in recruiting events, and on-campus interviewing.  
Having 5 on each team allows us to cover a variety of disciplines to meet the greatest spectrum of student needs and job market sectors. Teams will also be able to work collaboratively.
For each team of 5 we will hire students in the following disciplines to represent their colleges and/or assist with employer Development in desired market sectors:
•	1-Agriculture and Life Science
•	1-Engineering
•	1-Eller College of Management
•	2- College of Liberal Arts and Science – SBS, Humanities, Education, Science, Fine Arts, etc.
Thank You
Eileen McGarry, Director',
'created_at' => '2013-01-22 15:35:05.000000',
'question_id' => 206,
),
165 => 
array (
'answer_text' => 'Sits at ASUA front desk during specified hours. Answers telephone and greets and helps people who come into the office. Makes appointments for Student Legal Services and does intake when students arrive in the office for their appointments. These tasks entail explaining eligibility for legal services, the parameters of the service offered and answering basic questions about the service. The student worker also makes copies of pamphlets and other informational material produced by Student Legal Services, obtains necessary office materials and acts as an aid and assistant to the Student Legal Services advisor.',
'created_at' => '2013-01-22 16:09:00.000000',
'question_id' => 200,
),
166 => 
array (
'answer_text' => 'The program currently shares student workers who are hired to staff the ASUA front desk. ASUA strives to have the front desk staffed during all hours the office is open, but this is not always possible with the funding available. The PAR will allow us to make sure we are fully staffed, so that there is someone at the desk during all hours ASUA is open. This is particularly important for Student Legal Services, as many of the clients are unfamiliar with ASUA and are anxious to talk to someone right away. Having them leave phone messages to be returned later, or having them come in and find the front desk unstaffed is discouraging to potential clients.',
'created_at' => '2013-01-22 16:13:47.000000',
'question_id' => 199,
),
167 => 
array (
'answer_text' => 'Every year there are two large conferences for Student Legal Services providers--the western regional conference in January and a national conference in July. I usually attend the western regional conference. The travel funds are used for air travel and the conference fee, which includes hotel and some meals. I find this conference very valuable, as I always learn about issues that pertain to students and have a chance to talk to other attorneys who work with students.',
'created_at' => '2013-01-22 16:22:47.000000',
'question_id' => 207,
),
168 => 
array (
'answer_text' => 'Legal subscriptions (to the Arizona Revised Statutes Annotated, primarily) and publications, office supplies, production of educational materials, marketing and advertising, continuing legal education.',
'created_at' => '2013-01-22 16:26:53.000000',
'question_id' => 208,
),
169 => 
array (
'answer_text' => 'Please see my answer to the previous question about the PAR regarding the job description of the student worker. 

We are asking for a student worker dedicated to Student Legal Services for approximately 15 hours per week. The way the office is organized, whoever sits at the front desk must deal with all of the programs and services. So, this request for funds transfer is a way of acknowledging that all of the student workers spend a significant part of their time dealing with legal services clients and issues and paying our fair share.',
'created_at' => '2013-01-22 16:31:22.000000',
'question_id' => 209,
),
170 => 
array (
'answer_text' => 'The free movie program is available only to UA students who must present their CatCard for entry into the Gallagher Theater. The entry process is monitored and controlled by Gallagher staff. 

We recognize that one or two faculty or staff who present a CatCard may also be admitted. The likelihood of this is low given the early and late evening timing of the showings. The Unions could implement a card-swipe system (approximately $1,500) to electronically check CatCard status (student, staff or faculty) should the SSF Board deem this important.

Thank you for your consideration.',
'created_at' => '2013-01-23 16:44:41.000000',
'question_id' => 205,
),
171 => 
array (
'answer_text' => 'Each of our departments need a variety of hours worked during the year, sometimes depending on the staffing needs of larger events.  The numbers of hours required add up to 330 weekly hours for 32 weeks (academic year) of undergraduate level work and 20 hours a week for 32 weeks of graduate level work.  If we hire undergraduate students to work 10 hours a week for 32 weeks it would be about 33 students.  The same for 20 hours of graduate level work, if we hire graduate students to work 10 hours a week for 32 weeks it would be about 2 graduate students.  If we hire undergraduate students needing more than 10 hours a week then our overall number of hires may be less than 35.  We also need 60 weekly hours for 10 weeks (summer) of undergraduate level work, students already working during the academic year would most likely cover summer hours.  Our goal is to hire the most qualified student pool and fulfill our commitment of hours agreed upon hire.  Breakdown of hours/staffing needed:

330 weekly hours for undergraduate Student Assistants during academic semesters; 10 hours/week for 32 total weeks = 33 students 

20 weekly hours for graduate Student Assistants during academic semesters; 10 hours/week for 32 total weeks = 2 students 

60 weekly hours for undergraduate Student Assistants for summer employment; 10 hours/week for 10 total weeks = 6 students 

One 0.5 FTE Graduate Assistant during academic year for 32 total weeks, 20 hours/week',
'created_at' => '2013-01-24 17:26:13.000000',
'question_id' => 210,
),
172 => 
array (
'answer_text' => 'The student employees who work under the Student Transitions Student Assistants funding support many programs within Academic Success & Achievement, the Transfer Center, and Leadership Programs. The diversity of programs, populations, and levels of support make it challenging to give an exact number but the estimated impact on the undergraduate student population of the UA is around 11,000 students when looking at the total number of students who are connected, contacted, and supported by these programs. While there is some overlap among programs, the Student Transitions Student Assistants impact almost a third of the total undergraduate students enrolled at the university.',
'created_at' => '2013-01-24 17:26:56.000000',
'question_id' => 211,
),
173 => 
array (
'answer_text' => 'We plan to do two trainings, a Fall strengths development training and a Spring professional development training for all student employees. For the strengths development training, we will administer the Strengths Finder instrument and facilitate a strengths overview and interpretation session. We believe it is important to understand everyone’s strengths as they will be engaging not only in their role in their home department, but will be taking on cross-departmental roles. It is insightful for a student to know that they may have the strengths to be a stronger asset in a role like assisting students at Focus on Success than calling first year students to check in on their academics or vice versa. When placing students in roles that fit their strengths, they perform better and enjoy the experience more. For the Spring training, we plan to bring everyone back together to reconnect as a work team, review their work experiences & learning from the fall semester and engage with them in higher level professional development. Building upon their knowledge of their strengths from the fall strength builder training, coupled with what they have learned from their semester work experience, students will identify specific skills and knowledge they have gained.  We believe that these students should be provided opportunities to develop their leadership skills and career readiness through these positions and through training.  They will learn how to network and market themselves utilizing this information. 

Fall Training (9am-2pm):
StrengthsFinder ($15 per person)	$525
Room Rental	                        $90
AV (we will bring our own laptop)	$75
Beverages and light fare for morning	$400
Lunch (for 35 plus 6 pro staff)	        $900
Copies of handouts	                $50
Total	                                $2040

Spring Training (1pm-4pm):
Room Rental	                        $90
Handouts	                        $20
Beverages and light fare 	        $350
Total	                                $460',
'created_at' => '2013-01-24 17:29:36.000000',
'question_id' => 212,
),
174 => 
array (
'answer_text' => 'If all aspects of the cyberbullying campaign are funded, I believe that the following student groups will be most impacted by the campaign: Undergraduate and graduate students who live on-campus in a residence hall, fraternity and sorority students who live in their chapter houses, students who use facebook, students who subscribe to the LGBTQ affairs listserv, and students who are enrolled in a class that meets at Centennial Hall. The campaign will also reach students who participate in the FORCE intern presentations, use the LGBTQ Affairs Office, and who have access to view the plasma screens on campus, but it is a bit tricky at this time to identify them as specific student population. The campaign will hopefully reach many students due to the proposed use of facebook advertisements and the video but I believe that the student populations I first outlined in this response will likely be most impacted by the campaign. However until the campaign is fully implemented it is hard to measure which students are going to be most impacted.',
'created_at' => '2013-01-24 21:16:32.000000',
'question_id' => 203,
),
175 => 
array (
'answer_text' => 'From the student populations I identified in the previous question, I believe that it would be hard to ask some of them to contribute to the campaign. I think that the most likely group that could contribute would come from the fraternity and sorority population. This would likely include asking the governing councils of IFC and Panhellenic to support this campaign, rather than individual chapters, as some chapters would not have the resources to contribute. 
Residence Life would likely not be able to contribute to campaign but potentially could be approached to assist if no student fee money was made available. SafeCats would not approach the Women’s Resource Center or LGBTQ Affairs to contribute, as these departments are majority student fee funded and do not have the ability to assist.',
'created_at' => '2013-01-24 21:22:32.000000',
'question_id' => 203,
),
176 => 
array (
'answer_text' => 'Members of the Behavioral Intervention Team (BIT) will comprise the committee making decisions about awarding emergency funds.  BIT members include representatives from Student Assistance, Counseling and Psych Services, Student Accountability, Residence Life, Fraternity and Sorority Programs, the Disability Resource Center, and UAPD.  BIT meets weekly, and its members are the staff on-campus most engaged with students in crisis on a regular basis.  Through their departments and their everyday student interactions, BIT members and the Coordinators of Student Assistance will be actively marketing the program directly to those students that may benefit.
Student Assistance also frequently presents to students, faculty, and staff on-campus.  Once we have the emergency fund established, information about it will be included in all presentations.  The presentations have reached hundreds of faculty, staff, and students, and the Student Assistance area has plans to increase the number of presentations that occur each semester.
Emergency fund information will also be available on the Student Assistance page of the Dean of Students Office website.  Many students locate the Student Assistance area through web searches, and an internet presence will actively market the fund to students and other campus constituents.',
'created_at' => '2013-01-24 23:54:06.000000',
'question_id' => 201,
),
177 => 
array (
'answer_text' => 'The actual application process to receive funding will largely depend on the specific situation.  Students will be required to submit a 1-2 page application as well as any supporting documentation about their situation.  Once they have submitted the necessary documents, BIT will review their packet and determine if their request is approved or denied.  BIT meets weekly, which will ensure a timely review and response to requests.
If students need funds immediately due to their situation, requests will be circulated electronically to BIT members for a faster response.  The Coordinators will then make arrangements for the student to receive their funds.
It is possible for the process to take less than 24 hours to a few days, depending on the urgency of the situation and how fast the student submits their request.',
'created_at' => '2013-01-24 23:55:03.000000',
'question_id' => 202,
),
178 => 
array (
'answer_text' => 'Thank you for your question. The travel in would be for invited guests to come to UA to share knowledge. We are exploring hosting a forum on the overprescription of medications, and lack of counseling, to veterans. For example we have one student veteran I met with just this week who has been on the same medication since 2008. He finally stopped taking one of the medications due to complications. He has yet to obtain a visit with the original Doctor who prescribed the meds in 2008. This is a national issue and a number of veterans are voicing their concerns, the media is barely beginning to discuss it. However, there is yet to be a serious conversation in an effort to force congress and the Department of Veterans Affairs to make change. 

The travel out would include traveling to conferences, two in particular. The UA is a partner university with the Pat Tillman Foundation (PTF). We are only one of 16 current (14 as of a week ago - and when I submitted my application) universities across the country. PTF pays for our Tillman Military Scholars to attend the annual summit previously held in Washington DC. Part of our responsibility as a university partner is to send the University Point Person (UPP) to the Summit. At the summit UPP"s discuss best practices and in tandem with our Tillman Military Scholars have the opportunity to meet with our representatives. Last summer we met with Senator McCain and staff from Congressman Grijalva\'s office. This upcoming year PTF is considering holding the Summit on the campus at Stanford University. The second would the the national conference of the Student Veterans of America. Our chapter won chapter of the year 2 years ago and we have a strong presence of student veterans at the annual conference. I was invited to sit on two forums and speak this year, however, due to budget constraints was unable to attend.',
'created_at' => '2013-01-26 16:53:26.000000',
'question_id' => 216,
),
179 => 
array (
'answer_text' => 'Student support would directly fund student programatic activity that should fall outside the operations budget, including supporting student travel to national conferences, such as the Student Veterans of America (SVA) national conference. Our SVA chapter at UA won the chapter of the year two years ago and is as strong if not stronger today. We have a strong representation at the national conference and these funds would provide solid support and assistance for our leadership of the SVA to attend. Of critical importance is the knowledge our leadership will obtain and bring back to the UA campus that we can apply or modify here to better support and ultimately integrate student veterans into the greater UA community thus providing a richer community overall.',
'created_at' => '2013-01-26 17:03:42.000000',
'question_id' => 217,
),
180 => 
array (
'answer_text' => 'We have already begun to develop relationships with organizations outside of the university to provide longterm funding both from donors and from organizations who provide grant funding. Additionally we have a strong relationship with the Office of Federal Relations at UA who have already expressed a strong interest in working with VETS to reach out to the federal government and Department of Veterans Affairs to propose a partnership as it relates to our (working) VETS 2020 Initiative. We are also developing relationship within the universities colleges to support the sustainability of the program.',
'created_at' => '2013-01-26 17:11:14.000000',
'question_id' => 218,
),
181 => 
array (
'answer_text' => 'These figures are approximate, as they change from year-to-year, depending on the circumstances.

Office supplies:               $1000
Operating supplies:         $700
Conference registration:  $600
Dues:                               $600
Subscriptions:                 $2500
Printing & Misc.               $1500',
'created_at' => '2013-01-28 16:00:03.000000',
'question_id' => 213,
),
182 => 
array (
'answer_text' => 'The day to day operational expenses that the Dean of Students office or area will cover with our expenses include a computer, computer support, software, phone line with voice mail, phone costs including long distance.  It also includes our orientation and training materails and supplies.',
'created_at' => '2013-01-31 01:04:03.000000',
'question_id' => 221,
),
183 => 
array (
'answer_text' => 'The $500 can be used for professional development opportunities such as attending a conference, taking a non-academic class or course, attending a training etc.  The GA must discuss how they would like to use this money with thier supervisor who will then give approval for it being spent.  Our hope is that the professional development opportunity relates to thier job and/or professional aspirations.',
'created_at' => '2013-01-31 01:06:54.000000',
'question_id' => 222,
),
184 => 
array (
'answer_text' => 'In the year and a half since the Program Director took her position, the efforts to gain institutional funding by the Women\'s Resource Center have made ground! 
The WRC was selected as an issue of concern this year by the Commission on the Status of Women, which is a Presidential Advisory Board. The CSW will present the issue of WRC funding on Thursday, February 26, when the Diversity Coalition leaders are invited to meet with President Hart. This opportunity is encouraging, as our new President is very busy and many are requesting her time and attention.
The WRC has also garnered the personal interest and support of its NEW Leadership Arizona advisory board members--influential women of Tucson who are well connected to the University and serve on various boards and in various capacities. Many of these board members, and certainly many more who participate in the NEW Leadership Arizona conference, are also leaders among community women\'s organizations (Women\'s Foundation of Southern Arizona, Arizona Women\'s Political Caucus, the YWCA, Avon Program for Women, O\'Connor House, Arizona Foundation for Women, and the Community Foundation for Southern Arizona), which results in a vested community interest in the WRC and its continued place on the UA campus. It is anticipated that these connections will serve to further the awareness and concern about WRC funding. 
When it is time for financial decisions to be made, the WRC should be at the top of our decision makers\' minds. However, any financial decision of this magnitude--a new line of permanent funding--takes time, not only to be made but to be implemented. Therefore, even though the campaign to gain institutional funding is advancing, we have not yet reached our goal. It is for this reason that we need and request the support of the Student Services Fee. Continued SSF funding would allow the Women’s Resource Center the time it needs to secure institutional support, while also continuing its contribution to UA’s tradition of excellence.',
'created_at' => '2013-01-31 20:01:52.000000',
'question_id' => 223,
),
185 => 
array (
'answer_text' => 'Business Meeting Expenses: (~30%)
We have requests and host Federal, State and Local politicians, staffers, and policy makers who meet with student veterans to discuss issues as it pertains to higher education, health care, and veteran issues in general.
We have local and community partners including the Southern Arizona VA Health Care System, Carondelet Heatlh Network, The Arizona Department of Veteran Services, Raytheon, and others whom meet with student veterans to discuss collaborative efforts that can be undertaken to advance student veteran issues both in Arizona and nationally.
We have campus collaborators who request to meet with student veterans to discuss issues and concerns of student veterans on campus and to proactively address and reach out to the campus community.
We host an admissions orientation for veterans in the fall and the spring and are scheduled to take part in admissions orientations moving forward, including Transfer Tuesdays, and various admissions activities. Additionally we host a Welcome  Back at the start of each semester.

General Operating Expenses to include: (~30)
Office supplies and printing
Marketing materials

Computer, phone, printer/fax support: (~25)

Office upkeep and maintenance: (~15)',
'created_at' => '2013-01-31 20:38:30.000000',
'question_id' => 219,
),
186 => 
array (
'answer_text' => 'How do you plan to market this program and recruit students to have them participate in the volunteer opportunities?

The Dean of Students Office plans to market and recruit students to participate in a variety of ways. We will focus on printed materials, a web presence, social media, and partnerships to educate students on the area and opportunities available.

Our goal is to create a strong branding campaign that we can market to the campus. We need to be able to send a clear message to students about the purpose and benefits of the area to motivate them to utilize the services available.

Partnerships: We want to market the programs and services available beginning with our incoming freshman at orientation. We will add information into the involvement presentations as well as hand out information during the business fair. We also plan to market through any on campus and community partners that we may be working with such as Residence Life, Volunteer Southern Arizona, academic classes, service clubs and organizations, and outside apartment complexes. 

Printed Media: We will create posters that will go into the residence halls and around campus. Brochures will be created and the area will also have a presence in other printed materials through ASUA and the Dean of Students. We also plan to outreach to the Daily Wildcat to do a story over the area and potentially place ads.

Web Presence and Social Media: We will create Facebook and Twitter accounts and also utilize established social media avenues such as ASUA and the Arizona Student Unions. We will also create a user friendly website and have links on the Dean of Students, ASUA, and Student Union sites.

Whenever we coordinate campus wide events such as alternative breaks trips and volunteer days, we will develop marketing campaigns that utilize all the strategies listed. 

We plan to handle printed marketing expenses, which is why it was not added in the proposal. We really want to utilize social media and target marketing as much as possible to reach a wide range of students, reduce costs, and be more sustainable.',
'created_at' => '2013-01-31 23:41:50.000000',
'question_id' => 220,
),
187 => 
array (
'answer_text' => 'Each department needs a certain amount of hours covered for various reasons including projects, front desk support, outreaching to students, etc.  Combined we actually need 370 weekly hours.  The breakdown by department is as follows:

Academic Success and Achievement, Student Affairs Outreach needs Undergraduate Student Assistants to provide assistance with administrative, program and events coordination, as well as customer service and outreach to peers.   
*Front Desk Student Assistants:  Help students learn about programs and resources, sign them up for peer advising and academic success workshops, help coordinate large events and efficiently run the front desk. (60 hours/week)
*Outreach Facilitators:  Outreach Facilitators send weekly "Wildcat Connections" emails to a group of approximately 200-300 students each which highlights what’s happening on campus that they need to know and includes important dates/deadlines, and highlights campus resources, call students periodically throughout semester to talk about classes, clubs and organizations, course registration, and final exams, answer questions and connect students to different campus resources and information, and connect with new students through social media outlets like Facebook, Twitter, and Wordpress. (150 hours/week)

Leadership Programs needs Undergraduate and Graduate Student Assistants to assist with administrative, assessment, and coordination components of leadership programs.
*NCLA Student Assistant: Coordinate all marketing, recruitment, administrative, and programmatic functions for online leadership association. This includes setting up online live chats with leaders from around the country, webinars on leadership topics, and discussion groups on leadership issues.  (10 hours/week)
*Avenues Student Assistant: Coordinate all marketing, recruitment, and administrative functions for online program, Avenues, to provide resources and tools to students to help them identify their leadership skills and interests, get matched with leadership opportunities, and develop their Leadership & Involvement Transcript and ePortfolio. (10 hours/week)
*Assessment Student Assistant: Oversee tracking and assessment for all leadership programs. In 2011-2012, assessment included data entry and analysis for 719 events. (10 hours/week)
*Leadership On Demand Student Assistant: Develop workshop curriculum for student organizations, campus departments, and campus groups around a variety of leadership topics; facilitate specialty leadership workshops and trainings for campus groups. (10 hours/week)
*Graduate Student Leadership Programming Student Assistant: Develop workshop curriculum for graduate students around a variety of leadership topics; facilitate specialty leadership workshops and trainings for graduate students; partner with departments and organizations serving graduate students to offer tailored leadership workshops for their needs. (20 hours/week)

Transfer Student Center, National Student Exchange needs Undergraduate Student Assistants and a Graduate Assistant to provide assistance with administrative, program and events coordination, as well as customer service and outreach to peers.  
*Graduate Assistant:   Assists Director with duties that include creation and distribution of information materials, database and website management, and communication and advising students, making referrals to resources, planning and scheduling of activities and programs, troubleshooting student issues. The graduate assistant works with the following programs and activities: Transfer Tuesdays, Faculty Fellow Program, Transfer Student Orientation, outreach & communication with transfer students, supervision of Transfer Assistants Program, advising UA chapter of Tau Sigma –(academic national honor society), advising Transfer Student Association, National Student Exchange Program (NSE) and other events associated with Transfer Student Center. (20 hours/week)
*Transfer Student Assistants/Outreach Facilitators:  Greet and welcome prospective and new transfer students to Transfer Student Center. Provide administrative and front desk support. Assist with center programs and workshops and presentations to visiting groups of prospective transfer students.  Maintain and update Transfer Student Center Resource information bulletin board. Maintain current information on assigned college cluster. Timely email information to designated college cluster transfer students on resources, deadlines and activities.  Phone and email contact with prospective and newly admitted transfers. Provide walk-in appointments with new and prospective transfer students. (50 hours/week)
*NSE Ambassadors:  Provide classroom presentations, tabling on the mall, monthly presentations to UA Up close groups, weekly public info sessions, maintain NSE Facebook page, monthly NSE postings around campus one on one meetings for information and application assistance for prospective applicants, Weekly emails to prospective applicants, and monthly emails and programming for current NSE participants. (30 hours/week)',
'created_at' => '2013-01-31 23:57:00.000000',
'question_id' => 214,
),
188 => 
array (
'answer_text' => 'The impact on these 11,000 students varies due to the nature of the work that we do and their level of engagement with each of our programs. We are an information and triage hub for the 11,000 students connected to our programs. When students have questions we are there to answer them in a supportive and efficient manner. A student can be directly impacted by receiving emails or calls that are targeted to their unique circumstance which reminds them about to do items that can impact their academic success (account holds, FAFSA deadlines, registration information, and academic support services). A student can call or walk in to any of our offices to get their questions answered, set up appointments or have someone walk them through a process (using UAccess, filling out National Student Exchange paperwork, etc.) They can also come for coaching (how to talk to faculty, how to ask for letters of recommendation, planning for the future, making a decision about staying at UA, etc.). This campus can be overwhelming and confusing to students; through our programs students have a first point of contact for questions and help navigating the university. Some students will engage with our offices only when they need us and others will interact with us on a regular basis; the goal is to provide help when students need it not just when we think that they need it.',
'created_at' => '2013-01-31 23:58:14.000000',
'question_id' => 215,
),
189 => 
array (
'answer_text' => 'Although ASUA Club Funding is a great financial resource for clubs to put on events, coordinate speakers, and travel to conferences, there are some restrictions that do not allow clubs to receive funds from ASUA. Such as the restriction on funding clubs who receive more than 30% of their total event budget from another campus resource. Our funding can be a supplement to clubs who are getting the majority of their funding from other campus groups and just need additional funding to put on their event, travel or acquire other needed items. Many clubs that are a majority Graduate or Professional students do work closely with departments or colleges to help support their programs, but they do need additional assistance for larger scale events or travel. Additionally we have two types of funding for clubs, Initial Club Funding and Special Club Funding. Initial club funding is intended for annual start up expenses and a club can only be awarded up to $500 for this funding. The Special Club Funding is intended for large events or travel and there is no limit on the amount awarded however for requests higher than $1,500 we do require the club to provide a letter of recommendation from a UA faculty member stating the importance of the event to the UA community. We feel that both ASUA funding and our club funding are great financial resources and that together we are able to support a larger number of clubs based on our different requirements. By providing funding for the graduate population clubs we are able to let ASUA free up some of their funds to go to other groups. We know that each year ASUA distributes most, if not all, of their club funds and our program allows their money to go further and for a larger number of Graduate and Professional Student Clubs to get the funding they need. This funding greatly provides these organizations and a large number of graduate and professional students the opportunity to continue to develop and advance their education through the participation in clubs and organizations. Because the SSF funding would directly benefit both graduate and professional students as well as undergraduate student members, it would be used in a manner consistent with the mission of Student Affairs.',
'created_at' => '2013-02-07 15:48:38.000000',
'question_id' => 224,
),
190 => 
array (
'answer_text' => 'The medical director of UA SEMS is a board certified Emergency Medicine physician recognized by the Arizona Medical Board. The medical director: maintains a SEMS board of directors which includes student, faculty, administrative, and UAPD leaders who meet biannually to provide service direction; delegates responsibilities to UA SEMS EMTs enabling them to see and care for patients on campus; ensures Arizona Revised Statutes for emergency medical services are followed and integrated into the service’s treatment, triage, communication and record keeping protocols. The base hospital involvement will compliment the medical director’s role as UA SEMS expands providing SEMS a systematic quality assurance review process for EMT performance and certifications. The base hospital will also facilitate ongoing education, training or remediation necessary to promote professional competency.',
'created_at' => '2013-02-08 00:31:17.000000',
'question_id' => 225,
),
191 => 
array (
'answer_text' => 'The services provided by UA SEMS are Basic Life Support (BLS) services. TFD provides Advanced Life Support (ALS) and BLS services. The difference between the UA SEMS and TFD BLS programs is that the UA SEMS program uses UA students who hold certification through the Arizona Department of Health Services to provide the BLS services. Not only are these students more familiar with the university campus, but have better access to student housing, and a more intimate knowledge of university operations which may allow UA SEMS to better respond to and manage student calls for assistance. Additionally this program offers the involved students a unique opportunity to gain experience as medical personnel that they would otherwise not be able to obtain.',
'created_at' => '2013-02-08 00:34:30.000000',
'question_id' => 226,
),
192 => 
array (
'answer_text' => 'UA SEMS expansion will remain insured by the UA through the State of Arizona’s self-insurance program. As UA SEMS expands 9-1-1 response hours and provides standby coverage for larger events such as Tucson Festival of Books, additional institutional liability exposure is possible. However, Risk Management Services and other administrative departments continue to work closely with UA SEMS to manage and minimize liability exposure associated with UA SEMS operations. Additionally, Risk Management has determined that some aspects of the proposed UA SEMS expansion have the potential to reduce the liability exposure for the university. Questions about how UA insurance applies to SEMS may be directed to Steve Holland, UA Risk Management Services Dept. at 621-1556 or sholland@email.arizona.edu',
'created_at' => '2013-02-08 00:37:26.000000',
'question_id' => 227,
),
193 => 
array (
'answer_text' => 'In reviewing the data from our 2011-2012 student satisfaction survey, the majority of student comments reflected the helpfulness of their Peer Advisor in providing advice and highlighting resources they may not have otherwise known were available. These opportunities to get timely recommendations from experienced students appear to be the most engaging and helpful piece of the program. Additionally, their 1:1 appointments every other week provide students with the recurring chance to talk about their ongoing transition personally and privately. As a result of this relationship building, it is often during these meetings that specific challenges are revealed. This allows us to be proactive in supporting our students’ academic and personal well-being, resulting in retention rates that are 12% higher than the university average.

For this reason and as explained in our proposal, we are making changes to the program with the idea that it should be a one semester orientation program with the option for students to continue their relationship with their Peer Advisors in the Spring. For some students who have a smoother transition to college or are more academically prepared, they may not need the skills-based workshops we currently offer as much as others. Therefore, our workshops will focus more on the resources and skills all freshmen may need to be aware of while still allowing students to meet with their Peer Advisor to receive more personalized attention and referrals.',
'created_at' => '2013-02-08 00:39:24.000000',
'question_id' => 231,
),
194 => 
array (
'answer_text' => 'Over the past years UA SEMS has been continuously discussing its response capabilities with TFD. These response capabilities are limited to BLS response during a limited time frame. This expansion of services will expand the number of hours that SEMS is available to respond to incidents and will continue to supplement the services currently offered by TFD. If this expansion and additional funding are approved further discussion with TFD could be conducted regarding the scope of UA SEMS responsibilities during medical emergencies.',
'created_at' => '2013-02-08 00:39:56.000000',
'question_id' => 228,
),
195 => 
array (
'answer_text' => 'Our increased funding request reflects the dramatic increases in employee related expenses (ERE) incurred with employing our graduate and undergraduate staff over the past few years. We wrote in what was the anticipated ERE of 12.0% for student employees.  We were just informed the actual ERE after some negotiation will be at 6.0% instead so our costs should come down.  However we are requesting your assistance to cover a .5FTE Graduate Assistant position which the ERE is anticipated to increase to 65%, meaning that for every dollar we pay our graduate assistant, we must also budget $.65 for accompanying benefits. As a result, our request has increased significantly even as we continue to ask for the financial support to employ 20 Peer Advisors and one Graduate Assistant. Any staffing increases beyond this proposal will be funded through our department similar to the staff member and student coordinators that are already part of our budget.',
'created_at' => '2013-02-08 00:41:19.000000',
'question_id' => 232,
),
196 => 
array (
'answer_text' => 'Though Prodigy is its own self-contained retention program, we coordinate with a number of other programs to ensure that there is no duplication of services among our target populations. What makes Prodigy unique on campus is that it is free and open to the entire freshman class. There are no qualifications to be a part of Prodigy which allows us to serve students who may not otherwise qualify to participate in other retention-based programs. For example, the University of Arizona’s federally funded TRiO Student Support Services grant requires that students identify as being first-generation, low income or have a disability in order to participate and has a cap on the number of students it can serve through the grant. If students do not meet these federally mandated criteria, we collaborate with TRiO to ensure a smooth transition into our program. In collaborating with other departments and retention programs, we do our best to make certain that students are not enrolled in multiple retention programs as such services are redundant and we wish to maximize the number of students we can serve with our university’s limited resources. 

Similarly, we also coordinate with other programs to help students determine the program that will best fit their individual needs. Students in Arizona Assurance have a scholarship requirement that mandates that students engage in a first-year program. Students whose colleges do not offer a freshman retention program are referred to Prodigy thus bridging a critical gap in services when individual colleges do not have the resources necessary to provide specialized retention programming to their students. 

Finally, we also coordinate with other offices whose missions may not exclusively be retention to provide supplemental programming that may further augment the student experience. Our culturally-based Prodigy sections (360, EDGE and First Year Scholars) pair our effective skills-based curriculum with the added benefit of specific cultural connections through the Cultural Centers in the Dean of Students office. Our Peer Advisors in these dedicated sections partner with the centers to highlight their resources and events while also providing an outlet for students to discuss the impact that their cultural identity has on their university experience. As mentioned in our proposal, we hope to further strengthen our partnerships with other campus departments through the Real Campus Tour which would offer opportunities for students to investigate major and career options by exploring the operations of key units on-campus. Potential partners for this project could include Career Services, the Athletics Department, and high-profile research labs on campus.',
'created_at' => '2013-02-08 00:42:38.000000',
'question_id' => 233,
),
197 => 
array (
'answer_text' => 'UA SEMS responded to its first emergency dispatch on April 7th, 2012 and continued responding until May 6th, 2012. During that time UA SEMS was responding Thursday through Saturday from 8PM until 2AM and responded to 11 medical emergencies. UA SEMS began responding again on August 16th, 2012. Since the beginning of the current academic year UA SEMS has maintained operations every week except during major holidays such as Thanksgiving break and Winter break. Additionally UA SEMS has continued to expand operational hours as current resources allow. At this point UA SEMS is in service from 6PM to 6AM everyday. During the current operational hours UA SEMS crews frequently respond to between one and three medical emergencies per shift.',
'created_at' => '2013-02-08 00:43:10.000000',
'question_id' => 229,
),
198 => 
array (
'answer_text' => 'The average GPA increases for students who participated in PASS are as follows:

Average Semester GPA increase of .61
Average Cumulative GPA increase is .17

Due to the fact that a student\'s cumulative GPA is an average of all units attempted, it can potentially require multiple semesters for a student on academic probation to bring their cumulative GPA up above a 2.0.  This helps to explain why the increase in the average cumulative GPA increase is much smaller than that of the average semester GPA increase for our student population.',
'created_at' => '2013-02-08 00:43:33.000000',
'question_id' => 234,
),
199 => 
array (
'answer_text' => 'The PASS Probation Program is a unique retention program added to the UA campus.  The academic colleges we currently partner with are specifically selected because they do not provide any other support services to students on academic probation and do not have the resources to support students in this population.  Therefore, our strong partnerships with academic colleges and the Arizona Assurance Scholars Program enable students in need to receive individualized services who would not have been able to receive additional help otherwise.',
'created_at' => '2013-02-08 00:44:07.000000',
'question_id' => 235,
),
200 => 
array (
'answer_text' => 'To date ~63% of UA SEMS’ responses have involved patients whose primary medical complaint was altered mental status. With 20% of patients treated in that category being unconscious on arrival and requiring SEMS responders to pay special attention to the patient’s airway management, blood oxygen saturation, and blood glucose levels. 7% of patients in the altered mental status category had suffered from varying degrees of seizures, and 2% being related to traumatic injury to the head. 

The remaining ~37% have involved:
Difficulty Breathing-4%
Chest Pain-2%
Abdominal pain-8%
Trauma (broken bones, lacerations, falls, car accidents, etc.)-16%
Other medical/non-traumatic (psychiatric, diabetic, uncontrolled bleeding, etc.)-7%',
'created_at' => '2013-02-08 00:46:53.000000',
'question_id' => 230,
),
201 => 
array (
'answer_text' => 'Thank you for the question. Yes, the GA position requested is a half time position. If you have any other questions, please let us know.',
'created_at' => '2013-02-11 15:37:47.000000',
'question_id' => 236,
),
202 => 
array (
'answer_text' => 'Our smaller events thus far this year have all been extremely successful in terms of attendance. All of these events have met or, in many cases, exceeded our attendance goals. I will list our attendance numbers we have on file for all of our past events:
Fall 2012
The Last Lecture Series (Fall): 248
Ten Million-A-Minute Bus Tour: 173
PostSecret: 1079
Spring 2013
Diversity Workshop with Dr. Maura Cullen: 58
Keynote Lecture with Dr. Maura Cullen: 186
Eliot Chang\'s "Easily Excited" Comedy Tour: 279

Our goals for upcoming events this semester are: 
Hunter White Safe Spring Break Symposium with Mark Sterner: 750
The Last Lecture Series (Spring): 250
HitRecord with Joseph Gordon Levitt: 1900

I am extremely proud of the attendance we have achieved at our events thus far this year. We have bee able to achieve these goals because of the time and dedication my committee members have shown in seeking out alternative forms of marketing, and truly taking a hands-on approach to event promotion.',
'created_at' => '2013-02-14 19:32:56.000000',
'question_id' => 237,
),
203 => 
array (
'answer_text' => 'The THINK TANK employs 11 Graduate Assistants.  Nine of the Graduate Assistants are .5FTE and two of the Graduate Assistants are .25FTE.  

The number of student employees varies from semester to semester based on student schedule availabilities and staffing needs.  We employ anywhere from 150-175 student employees.  We currently have 162 on student employees on staff.',
'created_at' => '2013-02-18 23:29:02.000000',
'question_id' => 244,
),
204 => 
array (
'answer_text' => 'The $500 operations offsets some of the expense of marketing materials for the THINK TANK.  It specifically helps to cover the brochures used during Orientation.  Annually the THINK TANK creates a brochure that highlights all the services for parents and students.  It is handed out during the Conference Sessions, during the University Business Services Fair, and during Wildcat Welcome Week.  We also share them with Academic Advisors and interested faculty.  The brochures include the Student Services Fee funding logo.  Last year the THINK TANK ordered 10,000 brochures and paid $1,312.95 for the order.',
'created_at' => '2013-02-18 23:32:27.000000',
'question_id' => 244,
),
205 => 
array (
'answer_text' => 'Yes.  This will be for UA students only during finals/end of semester.  Our staff will be very diligent in checking CatCards to ensure only UA students are benefitting from the program.',
'created_at' => '2013-02-20 17:55:57.000000',
'question_id' => 238,
),
206 => 
array (
'answer_text' => 'Every effort will be made to reach as many students as possible.  Specials will vary from day to day and be offered at different times.  Our commitment is to offer as much variety and be as flexible as possible to adjust to the needs of our student base.  We will be able to set up specials across campus to ensure lines can be maintained and everyone has a chance to benefit. For specials such as free coffee we will set no limit so during the times they are offered all who are interested can participate. The mission of this program is relieve stress and anxiety during finals.  We in no way intend to create a first come first serve situation that would force students to rush somewhere before the special ran out.  We want this to be a fun and relaxing way to end the semester during finals.',
'created_at' => '2013-02-20 18:07:33.000000',
'question_id' => 239,
),
207 => 
array (
'answer_text' => 'The Common Ground Alliance Project expands the JEDI program a few key ways:

1.	It creates more opportunities for large scale events that will promote social justice education by distributing funding across the 6 partnering offices. Through creating an event series, the potential for collaboration across identities and areas of interest is much greater than it is with the current JEDI structure. With each office being responsible for a portion of the funding, we also feel that there will be more equitable exploration of multiple identities than through funding one central group to do fewer large scale events, which is the current structure of JEDI.

2.	CGA more clearly connects Intergroup Dialogue to the work of the JEDI interns. The Justice Education and Diversity Internship will continue to be a resource for students who want to facilitate social justice workshops and coalition building activities for student leaders. Students who complete the Intergroup Dialogue program will strongly be encouraged to become a JEDI. While there are only a few opportunities for students to continue into an IGD facilitator role, almost anyone interested can become a JEDI and have meaningful impact on their peers who engage in the CGA Workshop Series and CGA Coalition Building. Right now, IGD and JEDI operate almost completely separate of one another, though they are under the same JEDI umbrella. The CGA structure utilizes IGD as a training ground for future workshop leaders, trainers, facilitators, who we hope will move on from CGA to take other leadership roles on campus.


3.	It provides specific direction and funding for coalition building and diversity training for student leaders. There currently is no resource on campus that provides dynamic, sustained opportunities for student leaders to engage with each other with regard to how the work within their organization impacts campus climate, or a general overview of how to run an inclusive multicultural organization. CGA would prepare other students to lead those workshops and opportunities with other student leaders and provide a valuable resource not currently facilitated within the JEDI structure. CGA coalition building would also set the stage for student leaders to connect across organizations to find opportunities for collaboration and support of their common goals and interests.',
'created_at' => '2013-02-22 00:57:26.000000',
'question_id' => 240,
),
208 => 
array (
'answer_text' => 'The Common Ground Alliance will intentionally engage in a number of different ways with other social justice programs on campus. The Social Justice office within Residence Life is one of the main supporters of this initiative, and as such, we plan to work with ACT, RHA, and other student groups within Residence Life who take on social justice related activities. Additionally, we hope to work with the ASUA Executive Directors of Diversity as soon as those positions are established for the Fall 2013-2014 school year in order to help ASUA meet some of their diversity and outreach goals. Also, by targeting certain national days of recognition, such as Indigenous People’s Day, the Martin Luther King Jr. Holiday, the anniversaries of the Trayvon Martin and Matthew Shepard murders, we plan to partner with local student groups who’d be interested in raising awareness around issues related to these events or national days of recognition, but typically fall outside of the purview of student life offices or student organizations. Lastly, the workshop and coalition building components of CGA will look to create useful opportunities for student leaders from across campus to come together around similar topics.

Alliance, collaboration, coalition building are central to the purpose and mission of CGA, and as such, we\'ll partner with other programs as much and whenever possible.',
'created_at' => '2013-02-22 01:00:05.000000',
'question_id' => 240,
),
209 => 
array (
'answer_text' => 'We have a few different reasons why we feel it’s important to provide stipends for the facilitators. I’ll number them below:

1.	Intergroup Dialogue Facilitation needs to be one of the student’s top priorities. Between actually facilitating the IGD course for 2 hours, reading, commenting and grading up to 15 journals every week, planning with the co-facilitator, and receiving coaching from professional staff, IGD facilitators put in a minimum of 8 to 10 hours a week of work. There are higher expectations and responsibilities placed on these students than we would expect from volunteers and we need the best students for the job. Combining a students’ passion with a small stipend helps us secure strong students.

2.	We feel that providing some compensation makes IGD facilitation available to students who either come from lower socio-economic households or otherwise must work to stay at the UA. Compensating students makes it possible for them to take less hours at their regular job and helps facilitation be available to a more diverse pool of possible facilitators.',
'created_at' => '2013-02-22 01:01:41.000000',
'question_id' => 240,
),
210 => 
array (
'answer_text' => 'The graduate assistants are responsible for a number of different roles as it relates to an alumni (or donor/friend) event being coordinated.  Some of our events are more involved than others but with each of them many of the tasks that have to be accomplished are the same. Coordination of a major event involves the entire unit and typically one of the full-time staff assumes the lead role for the event coordination and the rest of the team provides support. It is difficult to extract a percentage as there are on-going responsibilities that occur daily or weekly that are not directly related to a specific event, but ultimately supports the event when it does occur.

For example, record and data management is an on-going challenge. If we don’t have good data in the system, it makes it difficult to locate individuals, which impacts our ability to include them in activities or communications or invite them to a special event, get them involved with our students and departments and so on. There are a number of Student Affairs Development & Alumni Relations record and data management projects underway that are addressing our need for better student affairs alumni records and one of the graduate assistant’s spends a regular part of her job assisting with these projects. Without a graduate assistant position dedicated to help us with these projects, our records would remain in poor shape resulting in real challenges in developing a strong alumni (donor) outreach program.

Other examples of this support include but are not limited to researching alumni; writing content and assisting with the development of multi-media pieces for everything from invitations to new releases and marketing pieces for websites, external audiences related to the respective alumn/i or student affairs department; and helping on-site with the implementation of a specific event.',
'created_at' => '2013-02-24 15:56:50.000000',
'question_id' => 246,
),
211 => 
array (
'answer_text' => 'The Justice Education Diversity Internship (JEDI) program continues to exceed the expectations of its grant for the spring 2013 semester.  As stated in the goals of the grant, JEDI staff and interns have organized events and training sessions that increase the understanding of cultures and identities; strengthen skills in the areas of time management, budgeting and assessment; and increase the cultural competency and understanding of individuals who attend JEDI events.  For the spring 2013 semester the JEDI staff and interns have organized the following events:

1) Run weekly training sessions for JEDI interns.
JEDI staff, guest speakers, or interns facilitate weekly meeting to discuss issues of social justice and develop skills needed to run campus-wide events including program management and budgeting.  The interns will continue to meet weekly and respond to reflections about the lessons taught at training sessions through the beginning of May 2013.

2) Attend a professional development retreat.
JEDI staff and interns visited the Museum of Tolerance in Los Angeles, CA; met with student leaders in social justice organizations at UCLA; and discussed social justice topics with community leaders in LA who teach about community development, the criminal justice system, and using music for justice. 

3) Organize and implement campus-wide events that meet the goals of the JEDI grant. JEDI interns implemented two events that discuss social justice topics and showcase the intersection of identities for individuals leading social justice movements. On March 26, 2013, JEDI interns brought Jeff Chang, Executive Director of the Institute for Diversity in the Arts at Stanford University and hip-hop historian, to speak at the University of Arizona. Mr. Chang is the author of “Can’t Stop Won’t Stop: A History of the Hip-Hop Generation,” and he spoke about multiculturalism, race relations, student activism, political engagement, and the state of the arts. His presentation was preceded by a slam poetry workshop led by local educators. On March 27, 2013, JEDI interns brought OuterSpaces, a group of emcees, hip-hop poets, and slam poets to present and perform at the University.  This international, bilingual, pansexual, polyracial, multi-media, cross-genre collaboration of artists discussed various social justice topics and identified ways to work for justice as students. Their performance was preceded by a performance by high school, college, and professional members of the Tucson Youth Poetry Slam.',
'created_at' => '2013-04-04 20:04:51.000000',
'question_id' => 248,
),
212 => 
array (
'answer_text' => 'Students will submit a 16-page application form that details their interest in careers in Student Affairs.  From those complete applications, 30 students will be drawn at random.',
'created_at' => '2013-10-04 22:38:52.000000',
'question_id' => 249,
),
213 => 
array (
'answer_text' => 'In the Fall we put on two Live at Five concert events (October & November) and Wildcat Winter -- a holiday celebration event in November. In Spring 2014, we will host Live at Fives in partnership with the Wildcat Events Board in January, February, March, and April. We will also host a weekly lunchtime series every Wednesday starting in February. And we are looking to host a larger scale event every month beginning in February. 

We market these events through myriad forms of print and digital media including posters around the union and through campus, Table Topper ads, all plasma screens in the Unions, in the Daily Wildcat, 6 foot signs in our canyon, and advertisements at movie screenings in Gallagher, just to name a few. We also have a social media and web presence.',
'created_at' => '2014-01-25 01:08:41.000000',
'question_id' => 252,
),
214 => 
array (
'answer_text' => 'The ultimate goal for the Leadership Online program is to offer incentives to participants as they complete the online eLearning modules. We are going to offer a Leadership Online certificate after participants have gone through a select number of sessions. The certificates will be available to students after the completion of the module set. 
In addition to certificates, we are looking to create a virtual badge program where, after each completed module, the participants will get a virtual badge that they can collect to track and share with others.',
'created_at' => '2014-01-27 17:23:13.000000',
'question_id' => 255,
),
215 => 
array (
'answer_text' => 'We intend to employ two University Emergency Medical Services Responders per each shift, 24 hours a day, 7 days a week. The only time we may have more is during large-scale events (Tucson Festival of Books, Football Tailgates, Spring Fling). We plan to hold a general membership of around 35-45 responders (University of Arizona students).',
'created_at' => '2014-01-29 01:55:02.000000',
'question_id' => 256,
),
216 => 
array (
'answer_text' => 'We currently offer continuing education training through our base hospital (University of Arizona Medical Center). They hold monthly sessions with our organization that involve anything from current medical protocol updates, to lectures, and hands-on training. Our Captains of Training within University EMS also hold weekly training sessions involving mock drill scenarios ranging from traumas to altered level of consciousness. These training scenarios are in order to keep our general membership up to date in response guidelines, how to work with surrounding agencies, and life-saving medical training. We also have several certified instructors that are able to certify our membership in CPR.',
'created_at' => '2014-01-29 01:55:42.000000',
'question_id' => 257,
),
217 => 
array (
'answer_text' => 'University EMS has (one) Chief, (one) Assistant Chief, and (one) Deputy Chief of EMS Operations.  The three Chiefs are the recognized ASUA director\'s of the organization with the Chief being the Executive Director. They are ultimately responsible for organization operations and sustainment as a whole, as well as with working with University officials on a daily basis, and outside public safety agencies.  University EMS has (five) Captain\'s who are responsible for Training, Personnel, Auxiliary Support, and Fleet Services. They are responsible for everything from public education and awareness, training, scheduling, and ensuring equipment is maintained at appropriate levels and quality. Shift Supervisors and CQI/Development Officers are general members who have been approved by the Captain\'s of Training, and Deputy Chief of Operations to be the lead on any two-man crew. They are ultimately responsible for anything that happens during a shift, and other clerical duties, respectively.',
'created_at' => '2014-01-29 01:56:23.000000',
'question_id' => 258,
),
218 => 
array (
'answer_text' => 'We do think it would be possible to have a "hybrid" type of membership, where Chiefs, Captains, Shift Supervisors, and CQI/Development Officers all receive wages, with a stipend reflecting their rank. However, we do think it would be most beneficial to have an all paid organization, which would ensure our response during breaks and holidays such as Spring Break, Thanksgiving, and other lesser holidays.  
Another major benefit of being an entirely paid organization is the establishment of student employment for around another 35-45 University of Arizona students.',
'created_at' => '2014-01-29 01:56:50.000000',
'question_id' => 259,
),
219 => 
array (
'answer_text' => 'Student Data Entry Position:
The Student Data Entry Position is a key member of the Career Services Website Team who will work closely with the career professionals on staff to classify the content on the Career Services website.  They will be the point person responsible for updating the website database with changes to page classifications, and will assist in adding and/or revising content within the website at the direction of our career professionals.  While experience with HTML is a definite plus, any detail-oriented, highly organized student who works well on cross-discipline teams would be welcome to apply.  This is not a technical position and would be open to any student, undergraduate or graduate.

Student Quality Assurance Position:
The Student Quality Assurance Position will assist the Technology staff at Career Services with completing and end-to-end test of the SmartWeb system.  They will be responsible for creating and then running various scenarios in order to fully test the SmartWeb suggestion engine.  This position has been intentionally designed to be filled by a non-technical student so we may better simulate the significant cross section of possible users.  The testing process will involve locating any issues or concerns about the operation of the SmartWeb suggestion engine and providing constructive feedback to the Website Team.  The QA position will also put together two focus groups of student volunteers who will conduct usability tests at the start and end of the project, creating recommendations to be shared with the Website Team.  The ideal candidate will be web savvy and have excellent communication skills, be a problem solver and an active team participant.  The position would be open to any student, undergraduate or graduate.',
'created_at' => '2014-01-30 15:15:22.000000',
'question_id' => 250,
),
220 => 
array (
'answer_text' => 'Ongoing maintenance of the Career Services Website is already part of our annual budget.  This includes the content update and classification by our professional staff as well as any of the programmatic updates performed by our technical staff to keep the system up-to-date.  The initial version of the SmartWeb suggestion engine has already been released and we are currently in the early stages of planning the next version.  Our work on rounding out the capabilities of the suggestion engine will receive a significant boost should our fee proposal be accepted; without the extra help, we will still be able to complete the final adjustments to the suggestion engine, but it will take us significantly longer to work through that process.',
'created_at' => '2014-01-30 15:36:34.000000',
'question_id' => 251,
),
221 => 
array (
'answer_text' => 'Yes. All Office of Scholarships and Financial Aid (OSFA) staff are utilized in some capacity to ensure a successful opening of school. Financial aid counselors, managers, compliance officers, and student employees are utilized on the front line to serve UA students alongside Project Rush staff. 

The Fall and Spring opening of school periods are a whole office effort to ensure that UA students and families are being helped efficiently and effectively.  Project RUSH enables OSFA to better handle the huge increase in student requests. The front line support provided by Project RUSH staff allows counselors and other full time OSFA staff to dedicate more time for document processing and aid coordination tasks. Disbursing aid and quick resolution to student requests are essential to meeting heightened student demand.',
'created_at' => '2014-01-30 21:36:24.000000',
'question_id' => 262,
),
222 => 
array (
'answer_text' => 'Our ongoing goal has been to have 6-7 Project RUSH staff fully trained for the June-Sept rush period and to maintain up to 4 RUSH staff year-round as classified Office Specialists. 

We learned from the pilot year of Project RUSH (FY12) that we need to plan for attrition as well as expect some RUSH Office Specialists to fill permanent positions in the Office of Scholarships and Financial Aid (OSFA).

In FY13, Project RUSH employed a total of 11 Office Specialists, 17 UA students (at various times throughout the year), and 1 Senior Program Coordinator, whose responsibility is to manage the Project. 

The breakdown of the 11 RUSH Office Specialists funded in FY13 is below: 

2 classified Office Specialists continued on the project from FY12. 
5 temporary office specialists were hired for Fall 2012 opening of school. 
1 of new temporary RUSH staff was hired into a permanent position with OSFA, so a total of 6 RUSH Office Specialists were fully trained and worked the 2012 rush period.

4 temporary Office Specialists were retained as classified staff at reduced hours throughout the 12-13 academic year. In Feb 2013, 1 of those RUSH office specialists was hired into a permanent position with OSFA.

3 classified Office Specialists remained for the April 2013 RUSH hiring cycle. In order to again meet our goal of having 6-7 Project RUSH staff fully trained for the June-Sept rush period,  
5 new temporary RUSH Office Specialists were hired. 
1 did not make it past the second week so we continued RUSH training and had 
7 fully trained RUSH employees to work the 2013 rush period.',
'created_at' => '2014-01-30 21:37:30.000000',
'question_id' => 261,
),
223 => 
array (
'answer_text' => 'We had 7 fully trained RUSH employees who worked through the 2013 rush period (June-Sept). 

1 of the temporary Office Specialists was not retained past September, 1 of the classified RUSH Office Specialists was hired into a permanent position with the Office of Scholarships and Financial Aid (OSFA), and another RUSH employee resigned for personal reasons. 

Currently, 4 classified RUSH Office Specialists are employed to provide continuing customer service for UA students as well as help train the 5-6 new temporary RUSH Office Specialists we are looking to hire in April 2014. 

The main role of Project RUSH staff is to counsel UA students and parents in all aspects of the financial aid process. Project RUSH has been trained to identify problems, perform research and provide solutions to ensure complete resolution of students’ financial aid concerns. They provide excellent customer service to students, faculty and staff via phones, counter, and emails.

In addition to these responsibilities, classified RUSH staff, who are retained throughout the academic year, assist with training new RUSH staff and student employees. Due to their greater financial aid knowledge and experience, they are able to recommend and assist in implementing new or improved office processes. 

During the 2013-2014 academic year, Project RUSH staff have the opportunity to apply their knowledge and experience by acting as the supervisor on call for up to 3 hours throughout the week. This not only enhances their skill set, but also allows full-time financial aid counselors to spend additional hours meeting with UA students, processing aid requests, and participating in financial aid outreach events. 

Classified Project RUSH staff are trained to perform higher level systems tasks, which enable them to participate in large-scale counselor level projects that benefit all UA students, as well as resolve individual students’ financial aid issues in a more expedient manner. This additional training and access allows for reduced document processing time throughout the academic year while maintaining individualized service for UA students. Classified Project RUSH staff participates in all departmental meetings to ensure they are well versed in new financial aid policies and procedures. 

Project RUSH staff also promote interoffice collaboration and cross-training. This year we piloted a formal cross-training experience and one Project RUSH Office Specialist served as a scholarship liaison throughout the fall. This partnership has proven especially beneficial as she is now able to assist with scholarship questions and help cross-train others. Other classified RUSH Office Specialists have applied their knowledge and experience to improving the OSFA website, assessing Project RUSH performance metrics, and exploring a financial literacy pilot.',
'created_at' => '2014-01-30 21:37:56.000000',
'question_id' => 260,
),
224 => 
array (
'answer_text' => 'We envision using $18,000 for the Live @ 5 concert series ($3,000 per event for 3 concerts per semester), $8,000 for larger events such as a tailgate or Wildcat Winter to attract more people to the space, and $3,000 to support the weekly lunchtime series.',
'created_at' => '2014-02-03 22:35:48.000000',
'question_id' => 272,
),
225 => 
array (
'answer_text' => 'Commercial grade furniture included in the budget are: flip top tables, nesting chairs, rolling white boards, a television screen, a divider and vendor installation costs.
Flip-top tables:  60 tables at $700/table = $43,500
Nesting Chairs: 150 chairs at $290/ chair = $42,000
Rolling White Boards: 3 whiteboards at $1,500/whiteboard = $4,500
Television Screen: 1 television at $1,700/television = $1,700
Divider: 1 Divider at $5,000/divider = $5,000
Installation Costs = $4,500 
Total: $101,200',
'created_at' => '2014-02-04 18:46:52.000000',
'question_id' => 265,
),
226 => 
array (
'answer_text' => 'Four spaces will be furnished with the Student Services Fee requested funding. Residence Life and Think Tank staff toured and discussed the following sites:

Arizona Sonora- The space is currently used - to over capacity - by both on and off-campus students for Supplemental Instruction. The overflowing use is likely due to the number of students living in the halls, the number of students living in the nearby neighborhoods, and proximity to the Tyndall garage and south of 6th street parking. To maximize this resource and make learning easier nesting chairs and foldable tables on casters are envisioned for mobility and therefore a means to better use the space and provide the possibility for additional teaching methodologies (e.g. break out groups who can move their table for smaller discussions, instructors can change the room configurations etc.).

Coronado- The space would be newly dedicated to Think Tank Center where both tutoring and Supplemental Instruction would be possible. The space in Coronado is ideal as it can help with the SI overflow from Arizona-Sonora and the space configuration is one such that tutoring session are also possible. Foldable tables on casters and nesting chairs are envisioned to maximize the use of space and to provide the possibility for different teaching methodologies. 

La Aldea- The space was identified by Think Tank and Residence Life as an ideal place to provide English tutoring for graduate students (La Aldea is the graduate apartment complex). Foldable tables on casters and nesting chairs are envisioned to maximize the use of space and to provide the possibility for different teaching methodologies.  

La Paz- The identified space would be newly dedicated Think Tank Center where both tutoring and SI could take place. The location is ideal due to its proximity to south of 6th street parking and external entrance and its ability to help mediate the overflowing use of SI occurring in Likins hall. Foldable tables on casters and nesting chairs are envisioned to maximize the use of space and to provide the possibility for different teaching methodologies.',
'created_at' => '2014-02-04 19:04:32.000000',
'question_id' => 264,
),
227 => 
array (
'answer_text' => 'We will promote ASA\'s service programs widely via campus listservs, The Daily Wildcat, and through academic partners around campus. As a major function of the Service Experience Program is developing a culture of service among existing ASA programs, we will actively encourage students involved in other support programs to participate.',
'created_at' => '2014-02-06 18:35:53.000000',
'question_id' => 266,
),
228 => 
array (
'answer_text' => 'The service programs we envision are a means of deepening students\' sense of belonging at the University of Arizona through service. This sense of belonging will contribute significantly to retention and the likelihood that, through long-term and short-term service experiences, a student will persist to a timely graduation. Staff will take part in the projects with students and lead reflection discussions to help students understand the social issues related to their projects. In consultation with staff around campus, ASA has developed a service series that aims to increase students\' exposure to peers, staff, and community opportunities regardless of financial need or commitments that may limit participation in other programs. The Student Service Assistant role will provide an employment opportunity that will help students develop as professionals, refine their public speaking skills, and gain experience inspiring a team of volunteers to give back to their community.',
'created_at' => '2014-02-06 18:39:15.000000',
'question_id' => 267,
),
229 => 
array (
'answer_text' => 'SafeRide has to compartmentalize a huge amount of our budget for vehicle related items. We have about $20,000 allotted each year for vehicle purchases. Vans (our preference because of their size) run at about $8,000 from the UA MotorPool. Sedans are around $5,000. Vehicle maintenance alone, per semester, can run at about $10,000, especially if a vehicle gets into an accident or needs major repairs. Fuel for the vehicles (10) cost about $70,000  a year. Thus, we allocate $95,000 - $100,000 per academic year for vehicle expenses. SafeRide vehicles are used very rigorously, therefore our operational costs are relatively high in comparison to our overall budget. Vehicle maintenance is about 65% to 70% of our budget every year.',
'created_at' => '2014-02-06 22:56:26.000000',
'question_id' => 271,
),
230 => 
array (
'answer_text' => 'SafeRide currently owns ten vehicles (3 Chevy Malibus, 1 Toyota Corola, 2 Chevy Impalas, 2 Dodge Grand Caravans, 1 Dodge Caravan, 1 Chevy Uplander) and a golf cart.  Due to regular vehicle maintenance, we have at least 8 cars on the road per night.  On average we calculate that a single vehicle will transport about 8 passengers per hour or 48 passengers over our 6 hours of operation and drive about 50 miles. This is about 400 miles per shift for our fleet which translates into almost 2500 miles per week. This is a huge burden, especially when considering that our cars are turned on for over six hours at time.  With an average of at least 8 vehicles on the road, that translates to 384 passengers per night.  For comparison, during the Fall 2013 semester in which were understaffed and had multiple vehicle problems, we had on average 6 cars on the road per night which translated to less than 300 people per night.  The only thing that limits the amount of people we are capable of transporting is the amount of cars we have on the road per night. Therefore, vehicle maintenance and acquisition has and will always be a priority of SafeRide.',
'created_at' => '2014-02-06 22:56:44.000000',
'question_id' => 271,
),
231 => 
array (
'answer_text' => 'The upcoming light rail system should not have much effect on SafeRide’s nightly operations. The light rail system has a primarily southwest to northeast trajectory, starting downtown and slithering northeast towards the medical center, and vice versa. This is not a common pathway for SafeRide passengers. We take less than 5% of our passengers downtown and to the medical school. And even if our passengers are at these locations, most do not live on the path of the light rail. For instance, we get a lot of calls from downtown (4ave/9st) going to dorms on highland or grocery stores, none of which the light rail services. However, the light rail will most notably affect traffic conditions. This will be something SafeRide has to deal with, especially if our waiting locations are impeding the light rail’s movement. We do not foresee SafeRide losing passengers to the light rail.',
'created_at' => '2014-02-06 22:57:02.000000',
'question_id' => 271,
),
232 => 
array (
'answer_text' => 'This is the last year that WEB plans to request funds from the Student Services Fees Advisory Board. This next year of funding should allow us enough time to secure a separate student activities fee, in which case we will no longer require aid. We really appreciate the funding we have received from you the last two years, and hope that you can continue to help us for this third and final year.',
'created_at' => '2014-02-11 00:43:39.000000',
'question_id' => 278,
),
233 => 
array (
'answer_text' => 'In our application last year, we based our request for FY15 funding on an anticipated growth rate of 10% in travel grant applications. We anticipated that a 10% increase would match the number of applications to be received during fall 2013 and spring 2014. In fact, the increase in the number of applications and amount requested has doubled from our original projection. 
Looking at the data we have collected through four grant rounds this year, GPSC’s Travel Grants Program is growing at 18% in terms of total application submissions and 20% in terms of total amount requested by students. We based the current SSFAB request for FY15 on the growth data from this year. We are now projecting at least a 15% growth rate in applications for FY15 and feel that the increase in the amount requested would help more students in need. 
Last year SSFAB recommended that GPSC assess the Travel Grants Program funding needs on an annual basis, and request money based on annual trends and needs. SSFAB provided funding for FY14 based on this recommendation because they felt there might be more growth for FY15 than we had projected. We put significant weight in the board’s advice and have adjusted our request for FY15 based on current growth trends. We have requested funding for one year so that we may assess the need correctly each year.
We are looking to not only meet the needs of our growth projections, but also to expand our current program. The current maximum amount that a student may request is $500 per year. When GPSC founded the program in 2003, $500 generally covered the cost of most trips. In 2013, however, a typical trip costs $700 based on a sample of travel award recipients. Trips for international conferences costs much more. In past years students were able to offset the cost with support from their department, but that funding has been drastically cut in many departments. As such, we are looking to increase the maximum amount awarded per year to $750 per applicant, while still meeting the volume demands of the program.
With the drastic increase in applications we have seen this year we are experiencing a great increase in the workload for our Graduate Assistant who runs the Travel Grants Program. The work load is well exceeding the job expectations of a .50 FTE Graduate Assistance and we need more administrative assistance to keep up with the work load associated with the sheer volume of applications we are receiving at this point in time. Part of the funding request is to be able to support such a position. This was not something that was factored into last year’s numbers.',
'created_at' => '2014-02-13 19:40:55.000000',
'question_id' => 273,
),
234 => 
array (
'answer_text' => 'We hope to work with colleges, cultural centers, the Transfer Student Center, Residence Life, and other departments to reach students through their communication mechanisms. By partnering with colleges throughout the University, we will increase students\' exposure to the Arizona Mentors program and to the significance of mentoring itself. When possible, we hope to coordinate events that help students build their relationships with their mentors in campus spaces created to foster identity development and a particular community.',
'created_at' => '2014-02-13 22:32:53.000000',
'question_id' => 277,
),
235 => 
array (
'answer_text' => 'We have found that students need different kinds of mentoring at various points in their UA experience. Many students struggling with their initial transition to college thrive with the support of a peer mentor and have basic questions about getting involved and acclimated. Connecting with a student leader is beneficial, as they are able to relate over a common experience. For other students - especially older students - interacting with someone with more life experience and knowledge of an academic discipline or profession is most productive. Though many of the peer mentoring relationships do also support students\' personal development, we hope to create opportunities for students to gain a higher level of support from a seasoned scholar or professional. Some mentees have noted that their mentors have included them in research projects or have helped them find positions in campus labs. Others have even found internship opportunities through their mentors. Although we do not ask mentors to serve as tutors or advisors, some even support their mentees\' academics directly. At a December focus group ASA conducted, one student mentioned that her mentor "puts [her] in touch with upperclassmen," and another said her mentor "shared her story and struggles." Mentees have noted that this perspective has helped clarify professional goals and that their mentors regularly connect them with their own professional networks.',
'created_at' => '2014-02-13 22:39:13.000000',
'question_id' => 276,
),
236 => 
array (
'answer_text' => 'Relationships with faculty and staff across campus have been carefully cultivated by ASA staff for several years, and many of these faculty and staff have served as mentors since the Arizona Assurance program was first created. We hope to draw faculty and staff members that are new to the program through a recruitment campaign on listservs, by giving presentations for departments and colleges around campus, and through direct outreach. We will also ask current mentors for nominations and request that they send information through their own networks.',
'created_at' => '2014-02-13 22:41:07.000000',
'question_id' => 275,
),
237 => 
array (
'answer_text' => 'The graduate assistant will work with ASA staff to create new manuals and web resources for mentors and mentees. This person will coordinate the program\'s online presence and implement our assessment plan for the program. The graduate assistant will also profile mentee/mentor pairs throughout the year for our outreach and recruitment efforts.

The student employee working with Arizona Mentors will focus primarily on supporting the mentees involved with the program. Specifically, the student in this role will create a bi-weekly newsletter with tips and strategies to help participants maximize their relationships, call program participants to gauge engagement and offer suggestions, and assist in the development of online resources for mentees. We hope that regular communication from this student and reminders about resources will support the building of solid relationships with mentors.',
'created_at' => '2014-02-13 22:44:24.000000',
'question_id' => 274,
),
238 => 
array (
'answer_text' => 'I have submitted a graph to Teresa Whetzel with the data to reflect the number of applications that have been submitted and awarded since FY11 for Professional Opportunity Development (POD) Funding. You will see in the graph that in FY12 we had a spike in applications and many of the events that we funded in FY12 were put on by recognized clubs. In the following year the GPSC Appropriations Committee did a better job of shifting those applications being submitted by clubs to the Club Funding Program so that the POD funding was preserved for groups who were not eligible for Club Funding. This shift explains the slight dip in applications in the following year. In FY14 we are on pace to receive more applications than in FY13 as we still have 4 grant rounds remaining and have already awarded 11 projects. We estimate that we will receive 15-20 applications by the end of this year.',
'created_at' => '2014-02-13 23:00:48.000000',
'question_id' => 279,
),
239 => 
array (
'answer_text' => 'We currently have about 15 students from various units within Arizona Student Unions planning the event. We hope to expand to even more students outside of the department. We will also seek to work with other groups on campus (RHA, WEB, etc) to get other departments engage in the events as well.',
'created_at' => '2014-02-15 00:00:08.000000',
'question_id' => 282,
),
240 => 
array (
'answer_text' => 'Our department does not have access to that data.  However the Office of Institutional Research and Planning Support (OIRPS) provides data (not college specific) online related to overall persistence and graduation of students on academic probation.  For more information please visit the following links:

http://oirps.arizona.edu/files/Student_Demo/Compendium/FR_FTFT_Probation_Table.pdf

http://oirps.arizona.edu/files/Student_Demo/Compendium/FR_FTFT_Probation_Persistence_Ch.pdf

The first chart indicates that the overall first year persistence rate for freshmen on probation in 2012 was 51.4%.  If you reference our supporting documents you will see that our spring 2013 cohort which consists primarily of first time freshmen has a first year retention rate of 60%.',
'created_at' => '2014-02-20 22:40:05.000000',
'question_id' => 281,
),
241 => 
array (
'answer_text' => 'Our programming will be made available to students in colleges not currently partnering with PASS thereby increasing our numbers.  We anticipate a 40% increase for the 2014-2015 academic year.  This is an additional 170-190 students.',
'created_at' => '2014-02-20 22:40:38.000000',
'question_id' => 283,
),
242 => 
array (
'answer_text' => 'There are a number of benefits to using paid employees rather than volunteers both to our student leaders as well as for student participants in our program. There are also a number of reasons why, based on the nature of their work, it is actually required. While a few mentoring programs on-campus do use volunteers rather than paid employees, the nature and extensive scope of our Peer Mentors’ work requires that they be employees of the University of Arizona for reasons related to security, accountability and stability. 

Our Peer Mentors not only work one-on-one with students, but they also provide critical support to our office in other areas such as content development and delivery, digital outreach, data management and customer service. They are also crossed trained on a variety of areas within the office to help out with larger scale events when needed. While we may be able to identify volunteers that excel in some of these areas, it is unlikely that we could find as many volunteers as necessary that would have the combination of skills and availability we are seeking. 
In their work with students, our Peer Mentors have access to confidential information including student academic records and meeting notes that are protected by the federal Family Educational Rights and Privacy Act (FERPA).  They are also fully aware of how, as paid representatives of the University of Arizona, they must address student situations where the health and safety of a student is at risk. As employees, our Peer Mentors are held to strict standards that protect our students’ information and ensure that record-keeping is in compliance with federal law. Additionally, university data systems such as UAccess and Student Affairs’ SMART retention software are only accessible to employees for similar security reasons. 

Using paid employees has a number of benefits in terms of accountability and stability that are critical in ensuring a positive experience for our program participants. Our student employees receive over 30 hours of paid training up front in addition to continuing professional development throughout the year.  They are also held to the higher levels of accountability inherent in a paid position, including expectations around professionalism, time management, and communication. Student employees are observed and evaluated a number of times each year and provided with feedback. They are also asked to make a commitment to work the entire year, a commitment which can sometimes be difficult to get from volunteers for the amount of time we are requesting. 

In addition to benefits to the office, our student employees also see an obvious benefit as well in terms of both financial compensation as well as employment experience. Volunteer opportunities are an important part of career preparation for students; however, paid work experience is still one of the most critical pieces of finding a job upon graduation. Their experiences within our office including the professional training and support they receive as paid employees increase their chances and opportunities for future employment.',
'created_at' => '2014-02-20 22:42:37.000000',
'question_id' => 284,
),
243 => 
array (
'answer_text' => 'We are requesting funding for a full time staff person as opposed to a graduate student due to a variety of factors that are outlined below.

First, the position requires full-­‐time staff hours to accomplish the current and future goals. I currently work more than 40 hours/week meeting with students, advising 4 students weekly and 10 site leaders bi-­‐weekly, finding collaborations across  campus, attending student events, creating a strategic plan, making learning outcomes for our programs, developing assessment, meeting with non-­‐profits, researching new trends in civic engagement, and much more. Lowering these hours to graduate assistant hours will halt much of the progress we have made this semester. In addition, graduate assistants will lack the expertise needed to move  this initiative forward at an appropriate rate.

Second, a goal of our program is to be seen as a necessary entity on campus by higher-­‐level administrators so that we do not rely on Student Services Fee to support this project. Due to limited hours of a graduate assistant, key relationships with on-­‐campus partners and community agencies cannot be fully established, and will not have the possibility to continue beyond the student’s time at the institution or within the position. I currently have relationships and have met personally with 10+ on-­‐campus areas, and work with approximately 50 agencies around Tucson. This initiative has the capacity to continue to grow rapidly. A full time staff person maximizes the time, resources, and contacts that he/she already has to make the most efficient trajectory for the program as possible.

Third, our community partners truly value having one person/area on campus whom they can rely on to share volunteer needs. A graduate assistant often changes every year. If they did continue to work with the program, they would, most likely, be in the position for 2 years, at most. Having frequent change over in this position would hinder our partner relationships. These partners are potential employers and internship supervisors that are under-­‐utilized. As we aim to support the 100% engagement initiative of Dr. Hart, it is critical that we show these constituents that they are valued, and we, as a university, recognize the potential impact of the opportunities they offer. We can accomplish this goal by stable, constant staffing  and contacts.

University of Arizona students deserve a full-­‐time staff person who is  knowledgeable in connecting them to opportunities to enhance their own community, and exercise their civic responsibility. From our benchmarking research conducted last spring, we found that the average number of professional, full-­‐time staff in a volunteer/civic engagement-­‐like area was 4.5 people. In addition to these full time members, each area had an average of 1 graduate student staff member. This information means that the average number of full-­‐time staff, and graduate assistants at 19 other institutions (including our peer institutions) have a total of 5  to 6 people who work directly with their civic efforts. We want to offer our students opportunities that their peers are receiving at other PAC 12 institutions. The success of our peer institutions’ programs is largely due to dedicated staff who are truly

committed to the students, their community, and the possibility to create a well-­‐ educated, and active citizenship. I believe in the growing possibilities that exist for the Volunteer and Civic Engagement Initiative, and hope that you see that a full-­‐time staff person is necessary to accomplish the goals and vision of the Volunteer and Civic Engagement initiative.',
'created_at' => '2014-02-20 22:49:04.000000',
'question_id' => 286,
),
244 => 
array (
'answer_text' => 'That is correct. We received reassurance that the activities fee will probably be passed. But we thank you for your consideration of our application!',
'created_at' => '2014-02-22 03:16:00.000000',
'question_id' => 285,
),
245 => 
array (
'answer_text' => 'Yes, that is correct we are requesting only one year of funding.  Specifically for the 2014-2015 fiscal year. Thank you for your consideration.',
'created_at' => '2014-02-22 18:52:07.000000',
'question_id' => 287,
),
246 => 
array (
'answer_text' => 'These funds are for two staff member to visit four of UA\'s top metro areas for graduates seeking employment upon graduation (usually where there is a base of employers and alumni working for those employers) in order to strengthen existing relationships and more important develop new relationships as well as seek donations/sponsors.  These trips are highly productive usually visiting 4-8 employers over two days plus alumni and gaining various levels of commitments to investing in students at UA and their future careers. These trips are often to areas such as Chicago, Los Angeles, Seattle, San Francisco, etc. and involve air fare, hotel and ground transportation as well as any meals that the employer partners do not host us for. In the past similar types of visits have deepened relationships with organizations such as Black Rock, Factset, Bloomberg, Gap, San Francisco Giants, Vanguard, etc. to name a few. Each employer makes it a point for us to meet alums working for them. These meetings strengthen our relationship with the employers and those alums so that the employer maintains or even increases their focus on recruiting UA graduates.',
'created_at' => '2015-01-29 22:43:57.000000',
'question_id' => 292,
),
247 => 
array (
'answer_text' => 'These funds would be based on financial need and applicability of the internships to their career objectives, therefore could always be a limited number.  However, they would probably need to grow as the initiative takes hold and grows as well.  $10,000 a feasible starting point to pilot the idea with 5-10 students ranging in scholarships from $200-$1000 not to exceed $1000. Scholarship would be based on need, objectives, and travel costs to internship site.',
'created_at' => '2015-01-29 22:44:47.000000',
'question_id' => 292,
),
248 => 
array (
'answer_text' => 'Wages have been determined due to the professional nature and level of experience required of the roles.  These roles interact with employers, students and faculty in order to ensure employers connect with our students and to follow up with employers and increase their engagement on campus. This role requires a higher level of professionalism and training than other student employee roles on campus because of the client relationship management and data base use.  For example, student ambassadors will follow up with employers who post a job and advise them on the many other opportunities to connect with candidates through UA suggestive selling other services and increasing their involvement and visibility on campus.  The student employee receives extensive training and a stellar experience working in a very professional environment that will behoove them as they continue their career journey.',
'created_at' => '2015-01-29 22:45:05.000000',
'question_id' => 292,
),
249 => 
array (
'answer_text' => 'Absolutely! All of career services programs and resources are available to graduate and undergraduate students.  The grant calls specific attention to early outreach to new students because of the importance to raise awareness to students early in their time at UA and the role that having a career plan earlier plays into student retention and persistence.  Career Services is collaborating with GSPC on programming such as the Andrew Green’s Job Talk in March.  Career Services has begun discussions internally and with other departments to explore ways to reach more graduate students, attract more graduate students to events and resources, and create resources and programs relevant to graduate students while also maintaining the necessary services to the 80% majority of our audience that are not graduate students.  At present as much as nearly 18% of attendees at certain events are graduate students.  Due to the advanced and specialized career progression of graduate students, they often require individualized career planning assistance through tailored services and programs which Career Services does offer. In time, Career Services does see the need for a full time career services professional to assist with graduate students and programming.',
'created_at' => '2015-01-30 00:03:12.000000',
'question_id' => 292,
),
250 => 
array (
'answer_text' => 'SafeRide is looking at purchasing an additional 2 vehicles for the FY16 year. This would be to purchase a golf cart for on campus driving, and a van. If there are no vans available for sale from Motorpool, SafeRide will purchase additional cars. As indicated by Table 2 and 3, there is more demand than vehicle availability by an average rate of 10-15%. This means 85-90% of demand has been met by an average of 8 vehicles on the road. 10-11% of the total demand is met by each vehicle. By adding 2 additional vehicles, SafeRide can meet all the demand.
The population of people willing to wait 5 minutes for a ride is much larger than the population of people willing to wait 12 minutes for a ride. The people who currently utilize SafeRide are willing to wait that additional time. If capacity is increased and wait times are driven down, then the overall demand for the service will significantly increase. By adding an additional 2 cars, SafeRide can increase capacity by a total of 40%, and thus, increase total capacity by 40%. Increased capacity means that more students, faculty, and staff have equal opportunity to safe transportation on and off campus.',
'created_at' => '2015-01-30 21:36:44.000000',
'question_id' => 299,
),
251 => 
array (
'answer_text' => 'UITS was unable to provide comprehensive information for January and February. There were some technical difficulties and inability to send data for every day. As a result they were omitted and a sample was created from March-December of 2014.
March-Dec ‘14 Total Incoming Calls: 67,404
March-Dec ’14 Total Calls Taken: 53182',
'created_at' => '2015-01-30 20:51:16.000000',
'question_id' => 289,
),
252 => 
array (
'answer_text' => 'UITS was unable to provide comprehensive information for January and February. There were some technical difficulties and inability to send data for every day. As a result they were omitted and a sample was created from March-December of 2014.
March-Dec ‘14 Total Incoming Calls: 67,404
March-Dec ’14 Total Calls Taken: 53182',
'created_at' => '2015-01-30 20:53:00.000000',
'question_id' => 288,
),
253 => 
array (
'answer_text' => 'The current boundaries are Country Club to the East, Stone to the West, Grant to the North, and Broadway to the south. SafeRide will drive past those boundaries in certain circumstances. Since the spring of 2014, SafeRide started driving to El Con Mall. The most common is the drive to El Con Mall. Extending boundaries is a large topic of discussion. SafeRide would ideally increase the boundaries, but prior to, SafeRide must increase capacity to meet demand within current geographic limits. Specifically, SafeRide is looking into reaching apartment complexes outside the current boundaries if capacity were increased.',
'created_at' => '2015-01-30 20:49:53.000000',
'question_id' => 290,
),
254 => 
array (
'answer_text' => 'Although companies like Uber operate under a very efficient taxi model, because all passengers are U of A affiliates, SafeRide can comfortably route different calls together. SafeRide is able to meet more demand than if one car was sent to one call (which is how companies like Uber operate). SafeRide may route as many as four calls together, given a similar geographic destination.
SafeRide has been investigating a software company called RideCell. It is a company that is utilized by a significant number of other collegiate SafeRide programs nationwide. It allows for the text message reminders as well as notifications for average wait times. It also allows for members to request rides without waiting on the phone. They can enter a request via telephone/Smartphone or online. The overall cost of the product can be significant, but they are attempting to develop a quote with product specifications to meet the budget requirements.',
'created_at' => '2015-01-30 20:50:59.000000',
'question_id' => 291,
),
255 => 
array (
'answer_text' => 'Thank you for the opportunity to provide you with more information about the Graduate & Professional Students Program.

Every semester the areas served by GAs who are part of the Dean of Students Office (DOS) Graduate & Professional Students Program submit progress reports to DOS.  Part of the information included in the progress reports is the number of students impacted by the work of the GA.  Below you will find a compilation which reflects the minimum number of contact/impact opportunities created by the work of the GAs.

Fall 2011 – Spring 2012
Student contact/impact opportunities: 40,944 
Fall 2012 – Spring 2013
Student contact/impact opportunities: 19,316
Fall 2013 – Spring 2014
Student contact/impact opportunities: 39,776
Fall 2014
Student contact/impact opportunities: 41,389

It is a bit harder to provide the number of student contact/impact opportunities by classification (classification as used here refers to the type of degree program in which the student is enrolled - undergraduate, graduate, or professional).  The Graduate & Professional Students Program currently has GAs with the following specializations: Academic Integrity, Ally Development for LGBTQ Affairs, Fraternity & Sorority Programs – Health and Wellness, Marketing, Education & Outreach, Parent & Family Programs, Student Accountability, and Student Assistance.  

Some areas’ programs/services lend themselves to tracking student classifications; good examples are the workshops conducted by Academic Integrity and Student Accountability GAs.  Some areas mainly serve a particular student classification making it simple to assume the population served; a good example is Fraternity & Sorority Programs which mainly serves undergraduate students. However, certain areas have neither a target audience based upon classification nor programs/services which lend themselves to tracking of classification; good examples are panel discussions organized by the GA for Ally Development and safety materials distributed by the GA for Marketing, Education & Outreach.

The numbers below reflect the best estimation of students served by classification based upon records which include classification or records in an area/program with a known population served.  Where we neither have records nor a known population I report numbers as unknown.

Fall 2011 – Spring 2012
Undergraduate student contact/impact opportunities: 16,875
Graduate student contact/impact opportunities: 125
Unknown classification student contact/impact opportunities: 23,944

Fall 2012 – Spring 2013
Undergraduate student contact/impact opportunities: 7,638
Graduate student contact/impact opportunities: 289
Unknown classification student contact/impact opportunities: 11,389

Fall 2013 – Spring 2014
Undergraduate student contact/impact opportunities: 13,625
Graduate student contact/impact opportunities: 229
Unknown classification student contact/impact opportunities: 25,922

Fall 2014
Undergraduate student contact/impact opportunities: 7,133
Graduate student contact/impact opportunities: 291
Unknown classification student contact/impact opportunities: 33,965

The current proposal requests funding for an increased number of GAs from past years.  An expansion of the program will facilitate a greater number of student contacts/impacts and place GAs in areas not currently served by the program - namely African American Student Affairs (AASA), Asian Pacific American Student Affairs (APASA), and Native American Student Affairs (NASA).

While the focus of this answer has been on the broad student population served I would be remiss if I did not mention the tremendous impact the program has on our wonderful GAs!',
'created_at' => '2015-01-30 16:52:49.000000',
'question_id' => 296,
),
256 => 
array (
'answer_text' => 'The need for alternative funding stems from several factors. First, ASUA already has their docket full with requests from countless undergraduate student clubs. It is GPSC’s duty as the representatives for graduate and professional students to ensure there is a distinct funding source for graduate and professional student clubs. Because we serve the graduate and professional student body every day we are distinctly positioned to administer these funds. Second, graduate and professional student clubs organize many events centered on bringing guest speakers to campus for academic conferences. These events incur traditional event expenses such as printing marketing materials, renting facilities, and buying t-shirts. However, most graduate and professional student club applications incur additional expenses such as honorariums, travel, lodging, and per diems for distinguished speakers and lecturers. These expenses and events are unique to graduate and professional student clubs. It would be unfair to place them in direct competition with undergraduate groups that do not have such costs associated with their events. Applications to GPSC club funding often refer specific aspects on graduate and professional student’s academic field of study. Since the GPSC is made up of graduate and professional students GPSC is well-positioned to assess when club proposals deserve funding and when they do not deserve funding. Finally, if a student club receives 40% or more of their funding from another source they do not qualify for ASUA club funding. Graduate and professional student clubs often receive outside funding so GPSC has removed this requirement. Many times a graduate student club might get funding from their department or from an advisor and they are then still eligible to apply for GPSC funding because the bill may still be too large for the students to pay. Clubs require at least 50% graduate and/or professional student membership to be eligible to apply for these funds.

General Club Funding Guidelines

1. Applicants should compare the current budget and event to past years’ events if applicable, including how and why the event and budget changed, but this does not guarantee that the event will be funded again.

2. A follow-up report will be required of all recipients of funding. Information that should be included are amount of funds allocated and used, number of attendees, affiliation of attendees, program schedule, etc.

3. Activities that could otherwise be funded through our travel grants program will not be considered for Club funding.

4. The activity or potential activities must be in keeping with the mission of the University of Arizona and the GPSC.

5. Acknowledgement of the GPSC’s support is expected on all announcements and public materials. The GPSC logo will be provided for use if funding is awarded.

6. Allocated funds may only be used for the purposes for which they were approved by the GPSC.

7. It is the responsibility of the organization to ensure that allocated funds are spent in accordance with University of Arizona and Arizona Board of Regents (ABOR) policy.

8. If the final cost of an approved item exceeds the budgeted amount, the GPSC will not pay the difference.

9. As per the ABOR policy, UA Printing and Publishing has the first right of refusal for all large print jobs done by any club or organization at UA, excluding jobs done at Fast Copy. The GPSC will not be able to pay for items printed off-campus without prior approval from UA Printing and Publishing.

10. The GPSC may fund politically oriented or religious organizations solely for the administration, execution, or maintenance of non-partisan/non-political/non-religious programs, services or special events deemed beneficial to the student body by the GPSC Appropriations Committee. For purposes of organization funding, "political" is defined as "having to do with the organization of individuals who seek to control or influence the action of those who manage the affairs of the state." "Religious" is defined as "having to do with an organization of individuals combining a particular system of faith and worship recognized and practiced by a particular church, sect, or denomination." The Committee funds only secular, non-sectarian activities.

11. The GPSC Appropriations Committee reserves the right to transfer a Club Funding application to a POD Funding application.

Additional Initial Club Funding Guidelines

1. ICF is intended to support basic operations of student organizations and may not be used for travel or events. Examples of articles that may be purchased include: postage, stationary, office supplies, advertising and marketing, copies, and banners.

2. Each graduate and/or professional student organization may receive up to $500 per fiscal year.

3. Even though these funds are not requested for specific events, clubs and organizations are still expected to address the purposes for which the monies will be used.

Additional Special Club Funding Guidelines

1. Requests above $1,500 require a letter of recommendation from a UA faculty member.

2. SCF can involve large sums of money and is highly competitive. Thus, the club must demonstrate why they are deserving of being funded.

3. The GPSC funding of past club events is no guarantee of funding for future or current events.

4. Events may be advertised on the GPSC calendar upon request, since all events should be open to, at a minimum, all graduate and professional students.

5. Group Travel: The GPSC supports club travel.  These Special Club Funding - Group Travel Guidelines apply:

- Travel AS A GROUP is necessary to apply for Club Funding-Group Travel.  Therefore, it is very unlikely a club can apply for Club Funding-Group Travel if individuals (as opposed to groups) are allowed to attend/register for the conference, event, etc. for which the club seeks funding.  In this instance, individuals should seek financial help from the GPSC by applying to the Travel Grants Program.  In the application, a club must defend and demonstrate why travel as a club is necessary.

- Expenses covered by the GPSC are limited to hotel, travel/transportation, and conference registration costs.  The GPSC will fund organizations for approved travel outside Arizona, through rental fees for a vehicle or airline tickets.  The GPSC will fund organizations up to the current fiscal year travel expense rate or the rental fees for a vehicle (whichever is less) for approved travel that is outside Pima County but within Arizona.

- Expenses covered by the GPSC may not be covered by another funding source; this includes GPSC Travel Grants.

- Clubs may request up to $500 per student traveling.  There is no cap on the amount a club can request, but requests over $1,500 require a letter of recommendation from a UA faculty member.

- The GPSC operates Club Funding-Group Travel on a reimbursement basis; no travel advances will be issued.  Before funds are transferred from the GPSC, the club must submit receipts for all expenses to be covered by the GPSC (i.e. plane ticket stubs, conference papers, photocopies of credit cards, and canceled checks used to pay travel costs).

- A club who receives funding is required to sign a sworn statement affirming costs covered by Club Funding are not also covered by a GPSC Travel Grant(s) or other funding source.

- A club who receives funding is required to provide post-travel feedback to all members of the club—especially members who do not travel.  This could be accomplished in an email, oral report at a club meeting, etc.  The club must provide a copy of the distributed material to the GPSC or, sign a sworn statement affirming that such a report was given.

- The GPSC Appropriations Committee will give Club Funding-Group Travel low funding priority.',
'created_at' => '2015-01-30 18:45:06.000000',
'question_id' => 297,
),
257 => 
array (
'answer_text' => 'Workshop Instructor Salary: $7,488
Workshop Instructor Bonus: $1,200
Food for Workshops: $1,016
Materials for Workshops: $296
Overall Budget: $10,000

The Graduate Enrichment Program is GPSC’s workshops series. These are workshops by and for graduate and professional students.  For five years, the GPSC Workshop Program has been designed to help graduate and professional students develop their professional skills through either taking or leading GPSC workshops. Workshops are free to attend for all graduate and professional students. For those students who lead the workshops, the opportunity to teach a novel idea to a new audience is an invaluable opportunity to develop skills of sharing research findings with new audiences. The process of sharing what is learned in the lab to a wider audience is an important skill for every graduate and professional student to develop as the University of Arizona and its graduate and professional students ultimately serve the Tucson community, the state of Arizona, the United States, and the world. Students are “hired” by the GPSC to lead workshops for $12/hour with a maximum of 52 hours. Workshop leaders can also earn a potential $100 bonus by designing original workshops and completing multiple workshops. GPSC puts out a call for workshops and students submit their ideas. The line item budget above is an estimate of how the $10,000 will be used. In past years students have lead workshops on topics ranging from molecular dynamics, recognizing sexual diversity, learning Spanish or Mandarin, or fixing computer hardware problems. If a workshop is really popular, GPSC occasionally may spend more on materials costs or food.  Because some workshops are very-well attended (~over people in some cases), GPSC may offer 8-10 workshops per academic year versus 12. GPSC does not pay rental fees for rooms at the Student Union Memorial center per our MOU with the SUMC.',
'created_at' => '2015-01-30 19:08:53.000000',
'question_id' => 298,
),
258 => 
array (
'answer_text' => 'Teaching Evolution: Case Studies in Two Arizona Communities - $1,631.00

Graphene-Fullerene supercapacitors for high efficiency energy storage - $2,000.00 storage

Sonoran Desert Science Writing: Transcribing Interviews With Experts - $167.00

Risk & Resilience in Low-Income Families: Linking Contextual Risks, Parenting Style, Child Socioemotional Development and Maltre - $350.00

Genomic Mechanism of the return from dioecy to gynodioecy of Waterman Mountain Bursera microphylla - $2,000.00

Development of Non-Opioid Dyn A Analogs for the Modulation of Pain - $1,768.00

Advanced EEG Assessment of Concussive and Subconcussive Injury in College Athletes - $2,000.00

Reconstructing the Canal Network at La Playa, Mexico -  $975.00

The semiotics and social practices of constructing a “proper” Singaporean identity -$1,000.00

Digital Light Processor Implementation to Manipulate Quantum Turbulence in a Bose Einstein Condensate - $1,000.00

PumaPlex100: An Improved Genetic Method for the Conservation of Pumas -$2,000.00

Qualitative Evaluation of Various Algorithms for Cartograms Using Extensive User Study -$920.00

The role of sleep in memory updating - $1,000.00 

On the Nature of Aerosol Physicochemical Properties and Implications for Health Effects in the Southwestern United States - $1,781.00

DNA Damaging Properties of Garlic-Derived Natural Products in Breast Cancer and Mammary Epithelial Cells - $1,000.00 

Voices of Marginalized Youth:  An Exploration of Limited School Choice, High-Mobility, and Mathematical Learning - $890.22 

Exploring the Chemical Space of a Novel Indole Multicomponent Reaction to Generate Pharmacologically Relevant Chemotypes - $2,000.00 

The role of dopamine in retinal inhibition with light adaptation - $1,000.00 

Freeform Lens for Ultra-Efficient LED Lighting - $2,000.00

Curation as a participatory research tool for illuminating patterns of scientific discovery in astronomy - $1,065.00 

Microbial monitoring of a new skilled nursing facility – $2,000.00 

Schistosoma spp: Characterizing parasite-host attraction and potential for application to an environmental surveillance device - $1,398.00 

Predicting Occupational Heat Strain with Alternative Measures - $1,907.00 

Evolution of insect virulence factors in the bacterium Xenorhabdus bovienii (Enterobacteriaceae) - $969.00 

Remembering Nishu: Community, Identity, and Transformation on the Missouri River, North Dakota  - $1,000.00 

Paleoecological Perspective on the Human Colonization of Eastern Beringia - $1,000.00 

Taking the ocean’s temperature: corals and the story of El Niño’s past, present, and future - $1,346.00 

Aedes aegypti Oviposition Flight Response to Temperature and Humidity - $800.91

Smart Meter Design and Evaluation for Smart Grid Applications - $1,601.00 

Modulation And Imaging Of pH Of Tumors In A Mouse Window Chamber Model  - $2,000.00

Stories and Performances: Preservice Teachers\' Identities as Teachers of Science - $1,810.00

Examining the use of universal screening to decrease rates of racial/ethnic disproportionality - $1,989.00

Analysis of Neuroprotective Glycopeptides and Further Modifications to Increase Drug-Like Properties - $1,436.00 

Do complementary currencies reduce inequality? A case study of the Palmas Bank in Conjunto Palmeiras, Brazil - $2,000.00

Tertiary potassium metasomatism as related to manganese and copper deposits in upper-plate sedimentary units of central-western - $2,000.00

Quality Performances and Recordings of the Composers Concert Series - $2,000.00

The Welfare Effects of Preferred Pharmacy Networks in Medicare Part D - $2,000.00

The Organizational Culture of Community Engagement:  Mexican American Studies at the University of Arizona - $885.00

The Study of Role and Practices of Real Estate Agents - $1,126.00

Adaptive Protocols for Cognitive Radio Networks with Full-duplex Capabilities - $2,000.00 

Loving Through the War Years: Affective Politics and Emotional Landscapes in Israel-Palestine - $1,129.00

The role of personal versus social information in pollinator foraging decision-making - $1,155.00

Information disclosure in fraudulent companies: A study of language similarity between earnings calls and financial statements - $743.00',
'created_at' => '2015-02-05 23:04:24.000000',
'question_id' => 300,
),
259 => 
array (
'answer_text' => 'A marketing campaign will be developed to include:
- Throughout the Student Unions and campus restaurants, plasma screen slides for display on television screens, totem "tower" signs to display in canyon area and breeze ways, and table toppers for dining area tables will be created.
- Posters will be distributed to Campus Health, Campus Rec, Residence Life, Disability Resources, Meal Plan Office and Athletics.
- Twitter and Facebook accounts will market the resource.
- Each restaurant full time supervisors and student leads will be educated on the resource so they can make customers aware.  
- Registered Dietitian, Nutrition Science student employee, and UA graduate dietetic interns will market this resource during orientations for new incoming students.',
'created_at' => '2015-02-08 16:06:24.000000',
'question_id' => 301,
),
260 => 
array (
'answer_text' => 'The Student Unions hired their first Registered Dietitian (RD) in history last summer to focus on implementing nutrition programming and making foods more nutrient dense.

The RD works with the Senior Executive Chef and Culinary Team to improve nutrient density of foods.  Here are some of the improvements that have been completed thus far:

1.  Replaced fruit juice offered at restaurants - new juices as a whole:
- Contain 5% less calories and 5% less sugar
- Contain 28% more vitamin C and 16% more potassium
- Contain 63% more fruit juice
- Contain no high fructose corn syrup or artificial colors

2.  Reduced sodium in bases and sauces:
- Teriyaki sauce reduced sodium by 71%
- Chicken base and beef consommé base reduced sodium by at least 25%

3.  Offering chicken sausage as option over pork sausage:
- Decreased calories 42% 
- Decreased fat 50% and decreased saturated fat 53% 

4.  Developed Grain & Nut Pancakes and Waffles for a more nutrient dense option:
- 79% more fiber compared to buttermilk variety
- 46% more protein compared to buttermilk variety

5.  Offering 2 varieties of Jamba Juice Smoothies at Cactus Grill & Highland Market:
- Both varieties are vegan
- Mango Manifico offers 50% of vitamin A and 45% of vitamin C daily needs
- Strawberry Symphony offers 15% of vitamin A and 120% of vitamin C daily needs

6.  Ensured 50% of cereals offered contain no added sugar.

7.  Replaced protein powder offered as an add on for smoothies:
- NSF Certified for Sport
- contains no artificial flavors, colors, or sweeteners

Up and Coming for Spring Semester…
- New nutritional campaign being developed
- Greek yogurt bar with options for nutrient dense, functional food toppings
- Smoothie bar at Bear Down Kitchen
- All natural chicken with no additives',
'created_at' => '2015-02-08 16:07:53.000000',
'question_id' => 302,
),
261 => 
array (
'answer_text' => 'From the SSF survey, 69% of students are interested in health and wellness programs and initiatives.  The nutrition information website is a project that addresses this interest in addition to providing support for a successful student assisting with making more informed decisions.  Through this effort we also will offer job opportunities for undergraduate nutrition students and career experience for UA graduate dietetic interns.

Students will definitely benefit from an easier to use, simpler to access website complete with a mobile application to coincide with their busy schedules.

The Student Unions has a many initiatives requiring funds.  In-house resources are limited and require excessive wait times.  

The funds requested would simply allow us to launch this effort in an expedited manner.  After the initial 2 years of funding, Student Unions would be able to budget requiring no additional funding from SSF.',
'created_at' => '2015-02-08 16:09:21.000000',
'question_id' => 303,
),
262 => 
array (
'answer_text' => 'Yes, the four temporary employees that are retained throughout the year as part of Project RUSH are funded by the SSF fee. At the end of each Fall RUSH season (typically the last week of September or first week of October), four of the highest performing and knowledgeable Office Specialists are retained as regular classified (benefits eligible) employees. There are several reasons for this model. First, while Project RUSH training is very rigorous and extensive, it does take an entire year to fully experience the financial aid cycle. Financial aid knowledge is a vital asset to Project RUSH and necessary in order to have a successful Spring RUSH. The Project RUSH specialists who are retained provide valuable experience that exceeds the skill level of newer staff. The four retained RUSH staff help train and support new student staff and new RUSH Office Specialists in the next RUSH cycle. Second, by promoting the Project RUSH Office Specialist temporary position as one that has the potential to become long-term employment, Project RUSH is able to recruit a more talented applicant pool year after year than if the position was guaranteed to end 6 months after hire. 

The breakdown of each personnel budget line item is described below. 

Classified regular: In addition to the four retained RUSH Office Specialists (described above), this budget line includes the salary differential (OSFA pays the base salary & ERE) for Angie Valdez, the Program Coordinator, Sr. responsible for overseeing Project RUSH training and supervision of project staff year-round. 

Classified temporary: Four to five new Office Specialists are hired and rigorously trained in May and remain in a temporary position through the September rush (19 weeks). 

Student employees: 15 student employees staff Project RUSH year-round. Their responsibilities include assisting parents and students in person, over the phone and via email; identifying and prioritizing problems, performing research, offering solutions, and representing UA at outreach and financial aid events throughout the year. 

Student work study: This covers the 10% overhead fee for the students who are eligible for Federal Work Study (FWS). This is a major cost savings for Project RUSH as FWS covers 75% of eligible student employees\' wages.',
'created_at' => '2015-02-12 18:20:24.000000',
'question_id' => 306,
),
263 => 
array (
'answer_text' => 'What is the reason for the increase in employees from last year to this year? 

Preliminary findings from our assessments, shows positive impacts on the students that are more thoroughly involved in Project Pave the Way.  Again, we believe that students can be better rooted to The University of Arizona if one foot is rooted in the campus and a second is rooted in the community that they are serving through our work in Paving the Way for future students to come to The University of Arizona.  

Since our application last year, we have inherited one more initiative that could benefit from undergraduate student employees – Cubs to Wildcats.  This group of 5 students would be compensated for their leadership in engaging other college students in going out to local middle and high schools to provide college going presentations and messages to youth.  The 5 students would each earn $640 per semester for their work and Cubs to Wildcats added $6,400 to our grant request, as compared to last year’s request. 


Do you have any plans for developing the program into a self-sustaining model?

Yes.  Early Academic Outreach strives to develop college access initiatives that are sustainable beyond initial funding sources.  Most initiatives are piloted for 2-3 years.  

A couple of examples.  In 2007, we received grant funding from the Lumina Foundation that allowed us to develop an Algebra Academy for three years.  Our office through a variety of ways has been able to sustain that program, five years beyond the initial funding through a combination of funding from students, the high schools, the UA Math department, and our office.  In 2009, we received two years of NSF funding to establish a Native American Science & Engineering Program.  That program has been sustained four years beyond the initial funding source with funding coming from the UA College of Optical Science, UA Geosciences, the Green Fund, and our office.

In the first eight months of our first Pave the Way grant, we have already made some attempts to seek sustainability.  

The UA Center for the Study of Higher Education helped us to identify a potential funder and we have had an initial conversation about our efforts and engaging them into our work.  

We also have met with two Foundations to discuss our work.  College Success Arizona is very interested in finding innovative solutions to increasing efforts around engaging male high school students in planning for college.  They showed great interest as we shared with them our Man Up and Go to College conference that brought 300 high school students from 10 local high schools to the UA campus, where they had an opportunity to meet current college students that had been organized because of our SSF funding.  

We are also working with the Be A Leader Foundation in Phoenix on a possible partnership that would bring Be A Leader to Tucson and in 3 years would allow our office some additional operational dollars that would allow us to at the very least sustain all of the undergraduate positions and some of the graduate positions or campus masculinity initiatives.  If we are granted three years of SSF funding, we feel that we can achieve sustainability in a manner that we have begun to lay the groundwork for.  

We also have been invited to become a part of The Men’s Collective.  This group is coming together to ensure that all of our masculinity initiatives build upon one another, and don’t replicate programs/services.  These initiatives have the potential to place The University of Arizona at the forefront of schools looking at masculinity.

Sustainability is critical to our success.  We seek three additional years of funding to put us in a great position to sustain the impact of these programs on our current UA students, and secondarily on the future students that our work seeks to Pave the Way for.',
'created_at' => '2015-02-14 01:41:31.000000',
'question_id' => 305,
),
264 => 
array (
'answer_text' => 'Simply, no, the individual groups will not hinder access. Think Tank sessions will be the first scheduled then followed by other potential groups. Time allotments have not yet been determined, although we will work to assure a coordinated and efficient use of the space.',
'created_at' => '2015-02-16 15:59:22.000000',
'question_id' => 311,
),
265 => 
array (
'answer_text' => 'Scholarship Universe received one year of funding from the IT fee which runs out in August. The funding was used to hire a desperately needed full-time system architect/developer. Funding after August from the IT fee is unknown which is why additional funding is being requested from SSF because the system architect must be retained for the overall stability and ongoing development of SU.
As noted in our budget, Scholarship Universe receives in-kind support from the Office of Scholarships and Financial Aid, Student Affairs Systems Group and Student Affairs Marketing. Not noted in the budget, is federal work-study which supplements some of our student wages, however it depends on eligible employees, hours worked, rate of pay and amount of work-study the employee has. It is difficult to quantify that support as those factors always change. 
Scholarship Universe currently does not have any other sources of funding, but with our new landing pages we were able to setup an account for potential donors to contribute. It is too early to know if any donors will contribute to a strategic project when most donors prefer to give directly to students via actual scholarships. However, any gifts made by donors would not be permitted for wages of students or staff, only operations.',
'created_at' => '2015-02-19 16:57:41.000000',
'question_id' => 307,
),
266 => 
array (
'answer_text' => 'Scholarship Universe is not requesting funding to hire a new staff member. We are requesting additional funding compared to prior years in order to retain our current team that is already in place. The system architect was hired last year with the help of the IT fee. The system architect has over 23 years of development experience and initially developed Scholarship Universe years ago, but it was only one of his many large projects when he was working for Student Affairs Systems Group. Since then, Scholarship Universe has grown into a complex web-software that has thousands of users and 16 college/divisional partners depending on it and using it daily.  It is far beyond an informational website and part-time student developers cannot be responsible for it. Only the University of Arizona has Scholarship Universe and this is because it is extremely hard to do.  Given funding for our current team, SU will be able to continue to meet the project goals which will not be  possible without our full-time architect.',
'created_at' => '2015-02-19 16:58:42.000000',
'question_id' => 308,
),
267 => 
array (
'answer_text' => 'Our goal is to sustain all programming designed to support undergraduates that fall on probation for the first time through the Academic Recovery (AR) Fee.  We must build the staffing to support all students who enter probation status and pay the fee however, not all of these students return to the UA and pay their account balances which includes the AR fee.  As we continue to refine the program staffing based on the student participation baseline data we collect this year, we will in a better position to sustain our services without the need for further Student Services Fee funding.  Our team is also actively exploring additional ways to make our services more efficient and less costly in the coming year so that we are able to sustain ourselves in future years.  Thank you for your consideration and support.',
'created_at' => '2015-02-19 20:43:07.000000',
'question_id' => 312,
),
268 => 
array (
'answer_text' => 'Both Student Assistance and the Behavioral Intervention Team are often unfamiliar to the campus at large until there is a need. In order for our campus community to realize the resource that Student Assistance offers, the team has spent the past 3 years doing significant face to face education about the program and practice of Assistance and Behavioral Intervention and a variety of marketing and outreach efforts. To date, our campus presentations have connected to 1163 faculty and staff.  The demand for live presentations and the positive response to Student Assistance shaped the creation of an Emergency Folder.  The folder addresses the common questions and concerns raised by faculty and staff and offers resources, frequently asked questions, and contact information in an easy and accessible formate. The Emergency Folders, created in summer 2014, have been distributed to 1800 faculty and staff.
In addition to faculty and staff outreach, Student Assistance also has a presence in both the student and parent segments of New Student Orientation\'s Campus Safety Presentation. Parents, in particular, are very responsive to the Student Assistance team at orientation and often refer to the presentation when calling about concern for their students.
Student Assistance also has a prominent presence on the recently redesigned Dean of Students website. The Dean of Students Office website is designed for distinct populations: Students, Faculty, Parents, and Need help now. The  resources most frequently browsed by population on our website vary, but Student Assistance is consistently woven throughout the website so that whether a parent, student, or faculty member are on the website, each group is quickly and easily directed to the services we offer.
Another important marketing tool Student Assistance has employed is our Wind Down Wednesday program.  The program started in Fall 2012 and with one program a month, we have had over 4470 students enjoy the program.  the intent is to offer an opportunity for students to get to know Student Assistance and the Dean of Students Office in a casual, relaxing and fun environment.  The program offers a range of activities including dog therapy, chair massage, cookie decorating, Eegees,  pizza, etc.  The dog therapy, by far, is the most popular program and a great example of the simple support we can offer students for stress relief and balance.
Moving forward, we will continue all of the marketing we have established these past three years.  It has had a significant impact on the number of referrals we see each semester.  Additionally, we track patterns in the referrals and assistance we offer.  Once a faculty or staff member establishes a relationship with Student Assistance, we work directly with the referral source to connect with the department or college they are in to broaden the overall knowledge of our resources. This is very effective because the referral source is able to testify to the level of support we were able to offer the student but also they share the sense of confidence we offered them in partnering in the student support plan.
We have also recently created and started to present to departments on our Behavioral Intervention Team.  This presentation has been well received because of the extent of expertise articulated by this impressive and extraordinarily collaborative interdisciplinary team.',
'created_at' => '2015-02-21 05:43:32.000000',
'question_id' => 309,
),
269 => 
array (
'answer_text' => 'Both Student Assistance and the Behavioral Intervention Team are often unfamiliar to the campus at large until there is a need. In order for our campus community to realize the resource that Student Assistance offers, the team has spent the past 3 years doing significant face to face education about the program and practice of Assistance and Behavioral Intervention and a variety of marketing and outreach efforts. To date, our campus presentations have connected to 1163 faculty and staff.  The demand for live presentations and the positive response to Student Assistance shaped the creation of an Emergency Folder.  The folder addresses the common questions and concerns raised by faculty and staff and offers resources, frequently asked questions, and contact information in an easy and accessible formate. The Emergency Folders, created in summer 2014, have been distributed to 1800 faculty and staff.
In addition to faculty and staff outreach, Student Assistance also has a presence in both the student and parent segments of New Student Orientation\'s Campus Safety Presentation. Parents, in particular, are very responsive to the Student Assistance team at orientation and often refer to the presentation when calling about concern for their students.
Student Assistance also has a prominent presence on the recently redesigned Dean of Students website. The Dean of Students Office website is designed for distinct populations: Students, Faculty, Parents, and Need help now. The  resources most frequently browsed by population on our website vary, but Student Assistance is consistently woven throughout the website so that whether a parent, student, or faculty member are on the website, each group is quickly and easily directed to the services we offer.
Another important marketing tool Student Assistance has employed is our Wind Down Wednesday program.  The program started in Fall 2012 and with one program a month, we have had over 4470 students enjoy the program.  the intent is to offer an opportunity for students to get to know Student Assistance and the Dean of Students Office in a casual, relaxing and fun environment.  The program offers a range of activities including dog therapy, chair massage, cookie decorating, Eegees,  pizza, etc.  The dog therapy, by far, is the most popular program and a great example of the simple support we can offer students for stress relief and balance.
Moving forward, we will continue all of the marketing we have established these past three years.  It has had a significant impact on the number of referrals we see each semester.  Additionally, we track patterns in the referrals and assistance we offer.  Once a faculty or staff member establishes a relationship with Student Assistance, we work directly with the referral source to connect with the department or college they are in to broaden the overall knowledge of our resources. This is very effective because the referral source is able to testify to the level of support we were able to offer the student but also they share the sense of confidence we offered them in partnering in the student support plan.
We have also recently created and started to present to departments on our Behavioral Intervention Team.  This presentation has been well received because of the extent of expertise articulated by this impressive and extraordinarily collaborative interdisciplinary team.',
'created_at' => '2015-02-21 05:43:54.000000',
'question_id' => 309,
),
270 => 
array (
'answer_text' => 'Both Student Assistance and the Behavioral Intervention Team are often unfamiliar to the campus at large until there is a need. In order for our campus community to realize the resource that Student Assistance offers, the team has spent the past 3 years doing significant face to face education about the program and practice of Assistance and Behavioral Intervention and a variety of marketing and outreach efforts. To date, our campus presentations have connected to 1163 faculty and staff.  The demand for live presentations and the positive response to Student Assistance shaped the creation of an Emergency Folder.  The folder addresses the common questions and concerns raised by faculty and staff and offers resources, frequently asked questions, and contact information in an easy and accessible formate. The Emergency Folders, created in summer 2014, have been distributed to 1800 faculty and staff.
In addition to faculty and staff outreach, Student Assistance also has a presence in both the student and parent segments of New Student Orientation\'s Campus Safety Presentation. Parents, in particular, are very responsive to the Student Assistance team at orientation and often refer to the presentation when calling about concern for their students.
Student Assistance also has a prominent presence on the recently redesigned Dean of Students website. The Dean of Students Office website is designed for distinct populations: Students, Faculty, Parents, and Need help now. The  resources most frequently browsed by population on our website vary, but Student Assistance is consistently woven throughout the website so that whether a parent, student, or faculty member are on the website, each group is quickly and easily directed to the services we offer.
Another important marketing tool Student Assistance has employed is our Wind Down Wednesday program.  The program started in Fall 2012 and with one program a month, we have had over 4470 students enjoy the program.  the intent is to offer an opportunity for students to get to know Student Assistance and the Dean of Students Office in a casual, relaxing and fun environment.  The program offers a range of activities including dog therapy, chair massage, cookie decorating, Eegees,  pizza, etc.  The dog therapy, by far, is the most popular program and a great example of the simple support we can offer students for stress relief and balance.
Moving forward, we will continue all of the marketing we have established these past three years.  It has had a significant impact on the number of referrals we see each semester.  Additionally, we track patterns in the referrals and assistance we offer.  Once a faculty or staff member establishes a relationship with Student Assistance, we work directly with the referral source to connect with the department or college they are in to broaden the overall knowledge of our resources. This is very effective because the referral source is able to testify to the level of support we were able to offer the student but also they share the sense of confidence we offered them in partnering in the student support plan.
We have also recently created and started to present to departments on our Behavioral Intervention Team.  This presentation has been well received because of the extent of expertise articulated by this impressive and extraordinarily collaborative interdisciplinary team.',
'created_at' => '2015-02-21 05:44:17.000000',
'question_id' => 309,
),
271 => 
array (
'answer_text' => 'BECAUSE, NICK. BECAUSE.',
'created_at' => '2015-09-20 17:18:45.000000',
'question_id' => 313,
),
272 => 
array (
'answer_text' => 'The desired impact of a Faculty Fellow will is to increase Academic support services that teach the skills needed to be a successful student.
While the Faculty Fellow might not be directly considered tutoring, supplemental instruction, or educational planning. I think there is a case for how we hope to engage.
For example holding small group discussion sessions about current events and of various relevant topics- with an intent of increasing both understanding of current issues but in also encouraging academic discourse. For example we had a small group discussion in the VETS Center the day following the Lecture on "The Iran Agreement: Containment or Catastrophe?" that was lead by two of the panelists from the night before, Professor Philip Pinto from the College of Science and Professor Faten Ghosn from the School of Government and Public Policy. The discussion was in part follow up but also in part to engage student veterans in this type of academic discourse. In this particular case the group started small and as individuals overheard the conversation more joined. There was certainly some diverse opinions but as the conversation continued (long past the stop time) you could see a change in discourse for some of the individuals - simply in terms of how they approached the conversation. The idea was not to convince anyone of one particular opinion or another but to engage in critical academic discourse.',
'created_at' => '2015-12-11 17:45:10.000000',
'question_id' => 315,
),
273 => 
array (
'answer_text' => 'The estimates provided for the application development, installation, and yearly fees come from TransLoc. TransLoc was originally recommended to SafeRide due to their collaboration with Parking and Transportation services, in particular the CatTran which utilizes TransLoc to track the location of the vehicles every second, allowing easy access to students to see the status of the vehicle. Because of their relationship with the University of Arizona we already trust in their ability to work with our campus, the reliability of their service, as well as their proven track record with the yearly maintenance. Some examples of schools that already utilize this service include, Emory University, NY University, University of Tennessee, Knoxville, Louisiana State University, and University of Alabama, Birmingham will be going live next week. Some examples of statements from some of these schools that helped direct this decision include this from Emory “It offers a convenient, easy-to-use interface for both the passenger and the admin staff” and from NYU ““the real time reporting is very helpful in making decisions quickly.” Emory and NYU show a great example of how much this can affect the service, showing a 32% increase in passengers, 53% increase in rides given, 15% decrease in wait time, and 49% reduction in dispatcher calls all without the addition of any vehicles. Thus, the TransLoc solution truly will be an ideal way to increase the safety of our program as well as our efficiency.',
'created_at' => '2016-01-29 18:18:23.000000',
'question_id' => 318,
),
274 => 
array (
'answer_text' => 'The key difference between the first and the second year line items for the application development is that the funds allocated for the first year towards the installation of the program have been added to the yearly maintenance line. Though the actual estimated funds for the yearly fees remain the same at $46,200, the additional $7600 was brought over to help cover any additional overhead with the program. This would include installation of the app in an additional vehicle, increased annual charges for the service, as well as any maintenance fees to help cover any repairs to the program, or any other transition materials to help the change in our operations, such as possibly requiring the removal of our current radios from our vehicles.',
'created_at' => '2016-01-29 18:19:36.000000',
'question_id' => 319,
),
275 => 
array (
'answer_text' => 'This is the application link that can be found on the GPSC website as well as on the Life and Work Connections page. https://lifework.arizona.edu/apps/childcare-forms/student-subsidy.php. 
This application is connected to the UA ID network. Upon login, students have to answer a series of questions for verification of enrollment, including graduate or undergraduate status, child(ren) information, and childcare provider information. It is a first come, first serve enrollment upon completion of the application. The second part is a form that is completed by the Childcare provider. 
The Childcare provider is responsible for completing the enrollment verification form. The provider provides specific information including: Childcare address, Tax ID or Social Security number associated with the Childcare provider, child(ren) name(s), Date of Birth, Child Care Fee (indicating either an hourly, weekly, or monthly rate) including the full or part time enrollment status of the child(ren). The provider is the person who signs this form and agrees with the content that includes that the individual is an award recipient with the student Child Care & Housing Subsidy Program (“Subsidy”) for this current academic and July 1-June 30 fiscal year period. That the student is solely responsible for contracting with the provider’s child care program. That the Subsidy Program is designed specifically to help eligible UA students pay for qualifying, child care occurring in Arizona regarding their classroom commitments. The provider will be required to provide child care program information about the child care charges, the parent’s payments and eligibility as a qualifying child care provider, as part of the Subsidy Program. I understand UA Life & Work Connections’ Child Care and Family Resources will provide the forms, and that it is the parent’s responsibility to bring them to the provider for completion.
Once completed, the form needs to be submitted by the awarded student via email or physical delivery to the UA Life and Work Connections address.',
'created_at' => '2016-01-29 20:56:03.000000',
'question_id' => 315,
),
276 => 
array (
'answer_text' => 'GPSC is making a huge stride to increase our marketing, specifically around Child Care. GPSC will soon be launching our new website which will feature the Child Care Program under the resource tab, as well as periodically feature it on the revolving carousel during peak enrollment times (beginning of Fall and Spring semesters). GPSC has recently increased our Facebook advertisement campaigns by adding additional boosts to reach our growing number of followers (2,390 likes). We have coordinated with the Grad Center and Cultural centers to promote our newsletter through their listserves to promote the program. We are working more closely with ASUA in promoting this program to the undergraduate and graduate students who utilize their resources and space on a regular basis.',
'created_at' => '2016-01-29 20:56:21.000000',
'question_id' => 315,
),
277 => 
array (
'answer_text' => 'We feel strongly that we will be successful in increasing the pool of students interested in and seeking mentorship. In the fall of 2015, we had 46 students request a “mentor in my professional field.” This was 37.7% of those who completed the survey requesting a mentor. This group of students applied without any additional outreach. Students were letting us know that they wanted a professional mentor. We know that advertising a formal professional mentoring relationship will only increase the number of students opting into professional mentorship.

Additionally, we will continue to work with our contacts in each of the academic colleges. Advisors and coordinators who oversee large outreach efforts within each college are already huge supporters of Arizona Mentors, and play a large role in recruiting students for faculty and staff mentoring. We will meet with them to inform them of the addition of professional mentoring and how these mentoring relationships are intended to help students. Some colleges and majors have more students requesting professional mentors than others (i.e., Social and Behavioral Sciences and Family and Consumer Sciences). We will work with colleges and majors in these areas to not only recruit more students but also partner to recruit professional mentors in their fields. 

Building an even stronger partnership with Career Services is also a natural way to recruit more student participants into Arizona Mentors. Career Services works with hundreds of students preparing to enter the workforce directly after graduation. Providing Career Services with information on the opportunity available to students in professional mentoring will be pivotal to increasing student awareness of this program.

Finally, we have requested funding to create rack cards for students. These will serve as a physical handout that can be placed in reception areas around campus, and that partners can keep in their offices to give to students when mentoring may seem like a good opportunity for them. The rack cards will be coupled with the electronic outreach that is currently in place. Our partners in advising, programs within Academic Success & Achievement, Residence Life and Wildcat Connections are great advocates that assist in the recruitment of students. Their promotion of the program helps us to increase the number of students seeking mentorship.',
'created_at' => '2016-01-30 05:05:36.000000',
'question_id' => 317,
),
278 => 
array (
'answer_text' => 'Our plan to create a larger pool of mentors in the greater Tucson community is trifold: (1) Utilize the assistance of a Graduate Assistant; (2) Leverage already existing campus partnerships; (3) Develop strategic partnerships with community businesses.

Seeking to increase the pool of mentors within the Tucson Community is the main reason behind our request for a graduate assistant. This individual would be tasked with reaching out to professionals not represented on a college campus. Employing a Graduate Assistant who is dedicated to the Arizona Mentors program will have a tangible impact on the number of mentors we recruit from the greater Tucson community.

Building upon our existing partnerships will serve as a large contribution in how we begin to grow our pool of professional mentors. Arizona Mentors currently partners with the UA Alumni Association, the UA Foundation, Career Services, and several academic colleges. All of these areas work with UA alumni and Tucson community professionals regularly. The Arizona Mentors staff will meet with these partners at the beginning of the summer and outline our plan to expand Arizona Mentors. We will ask them to widely distribute our recruitment rack cards, as well as assist us with developing relationships with their alumni in the community. We have no doubt that our current campus partners will serve as strong advocates for the program and will gladly promote the program to the Tucson community. 

Additionally, we will present to various UA Alumni Clubs. These include, but are not limited to, culturally based alumni clubs, band, Eller, and Honors. Establishing a strong rapport with our current campus partners has taken place over the span of several years and utilizing them as spokespeople for mentoring is sure to be one of our strongest recruitment tools. 

Much of the groundwork in building a pool of mentors is in connecting with our existing network of Arizona Mentors supporters. As UA employees we have the opportunity to connect with alumni and professionals during key events a few times each year. During these events we are often approached by alumni about ways they can give back to current students, and mentoring would be a natural fit. This component has not been in place, nor have we had the staff hours available to follow-up with people or businesses that have shown interest. However, the requested graduate assistant will be able to engage those who have shown interest and offer them a structured mentoring opportunity.

We are aware of several businesses in the Tucson area that are dedicated to education and giving back to their community. One such example is Raytheon – they are committed to improving STEM education in the K-12 system, and also have very strong ties to the UofA. Already having this knowledge of their strong commitment to education, we will strategically approach them about recruiting Raytheon employees to mentor students who want to enter a STEM profession. A second example is Cox Communications; a business our office already has close ties with. We also plan to approach Cox about partnering with Arizona Mentors. 

This past semester we received several requests for professional mentors that were not readily available in our pool of faculty and staff volunteers. Some of these requests included: wedding planner, interior designer, executive director of a non-profit, and a lawyer specializing in family-law. By expanding our pool to the larger Tucson community, requests such as these can be met. 

It is also important to explain that some students will benefit more from a professional mentor than a faculty mentor depending on their projected career path. For example, a student who wants to enter industrial engineering is best paired with an engineer from the community who is working daily in the field as opposed to an engineering faculty member who is largely focused on academic research, and not engineering in everyday practice. The same is true of a UA staff mentor. We have some accounting students paired with UA staff accountants, but one of these students is specifically interested in accounting with a large corporate firm (the “Big 4”), so a professional mentor in the corporate world would make a stronger match for this student. Again, the expansion of Arizona Mentors will allow for higher quality matches. 

Starting our outreach efforts with the groups listed above will begin to grow our pool of mentors in Tucson. Building our network of faculty and UA staff mentors took many years and we know that growing the professional mentor piece will also take time. This is our reason for asking for three years of funding. It is our goal to increase slightly each year and to make a positive name of Arizona Mentors in the Tucson community.',
'created_at' => '2016-01-30 05:40:21.000000',
'question_id' => 316,
),
279 => 
array (
'answer_text' => 'The Immigrant Student Resource Center (ISRC) will ensure that a professional staff member is supported in the near future and training is provided both to center staff and the campus at large. With the help of SSF funding, we will ensure longevity of the ISRC for the following reasons:

First, ISRC is grounded in research and experience from similar immigrant resource centers across different universities in the Pac-12.  The priorities of the ISRC (training, outreach, support of students) are grounded in research articles about centers based in institutions including UCLA and UC Berkeley. ISRC is also being developed with the guidance of immigrant students and allies who work with community organizations that help immigrant students attend and graduate from higher education institutions.  This includes ScholarshipsA-Z, which has committed financial support to this project’s development. 

Second, ISRC will be housed in the Office of Early Academic Outreach (EAO), which has a proven record for supporting the growth of graduate assistants.  EAO is purposeful in our work with graduate students and believe that our assistantships allow a unique opportunity to see how practical experience ties to the theoretical emphasis of college access in the UA’s Center for the Study of Higher Education.  We have been fortunate to have graduate students in different EAO assistantships that have felt a connection to the EAO office and a responsibility to their assistantships that go beyond the need for a job that helps pay for tuition expenses. Therefore, many graduate assistants have remained in their EAO positions for multiple years, including those working in programs supported by SSF funds. EAO provides ongoing professional development with funds budgeted in separate SSF grants, institutional money, or grants available through different UA colleges and the Graduate and Professional Student Council (GPSC).  EAO is dedicated to supporting the graduate student selected for the ISRC graduate assistantship in the same way.

Third, the ISRC will develop training for the campus community and staff members working in the center. In addition to providing financial support to the development of the ISRC, ScholarshipsA-Z, researchers, and community leaders will provide the initial training to the center staff. This training has been created by educators and immigrant students across U.S. colleges and universities, including one UA graduate student and two UA undergraduate students.  This training curriculum will be used as a starting point to develop the skills of the ISRC professional team as they establish a curriculum specific to the needs of the UA community. EAO staff and directors who will oversee the ISRC have also received extensive training from ScholarshipsA-Z and consulted with researchers for their current student engagement programs. 

Fourth, ISRC is being developed in a similar way that some of the UA Cultural & Resource Centers were created.  While the journeys of the Asian Pacific American Student Affairs and LGBTQ (Lesbian, Gay, Bisexual, Transgender, Queer and Questioning) Student Affairs offices to becoming official UA Centers are different, their stories have some similarities to how the ISRC is being developed.  For each center, students, faculty and staff identified a need for resources that met a student population’s needs.  Each office began with one staff member dedicated to serving the needs of thousands of students.  APASA was founded in 1993 and first received a part-time employee who worked 20 hours per week in the office.  LGBTQ Affairs was founded in 2007 and first received one full-time staff member to run a center that had small, dedicated physical space and few resources to organize programs and training. Today, each office has its own programming budget, one director, two graduate assistants, and student interns who organize campus events.  At its core, the story of ISRC’s development will be similar.  

We intend to utilize the SSF funding to create opportunities for a graduate student and five undergraduate students that not only provides employment, but established an environment that brings employees who want to create the change on campus that is outlined in the ISRC’s purpose.  If given the opportunity to be funded for three years through SSF funding, we will utilize the connections with community, build strong relationships with campus partners who already commit to our work, and work with institutional leaders to advocate for the creation of a permanent staff and space. 

We are confident that the ISRC proposal has been developed in a unique way that it will continue to grow and become an exemplary program of support for immigrant students and professionals who work with them on our university campus.',
'created_at' => '2016-01-30 21:11:49.000000',
'question_id' => 322,
),
280 => 
array (
'answer_text' => 'The Immigrant Student Resource Center (ISRC) will ensure that a professional staff member is supported in the near future and training is provided both to center staff and the campus at large. With the help of SSF funding, we will ensure longevity of the ISRC for the following reasons.

First, ISRC is grounded in research and experience from similar immigrant resource centers across different universities in the Pac-12.  The priorities of the ISRC (training, outreach, support of students) are grounded in research articles about centers based in institutions including UCLA and UC Berkeley. ISRC is also being developed with the guidance of immigrant students and allies who work with community organizations that help immigrant students attend and graduate from higher education institutions.  This includes ScholarshipsA-Z, which has committed financial support to this project’s development. 

Second, ISRC will be housed in the Office of Early Academic Outreach (EAO), which has a proven record for supporting the growth of graduate assistants.  EAO is purposeful in our work with graduate students and believe that our assistantships allow a unique opportunity to see how practical experience ties to the theoretical emphasis of college access in the UA’s Center for the Study of Higher Education.  We have been fortunate to have graduate students in different EAO assistantships that have felt a connection to the EAO office and a responsibility to their assistantships that go beyond the need for a job that helps pay for tuition expenses. Therefore, many graduate assistants have remained in their EAO positions for multiple years, including those working in programs supported by SSF funds. EAO provides ongoing professional development with funds budgeted in separate SSF grants, institutional money, or grants available through different UA colleges and the Graduate and Professional Student Council (GPSC).  EAO is dedicated to supporting the graduate student selected for the ISRC graduate assistantship in the same way.

Third, the ISRC will develop training for the campus community and staff members working in the center. In addition to providing financial support to the development of the ISRC, ScholarshipsA-Z, researchers, and community leaders will provide the initial training to the center staff. This training has been created by educators and immigrant students across U.S. colleges and universities, including one UA graduate student and two UA undergraduate students.  This training curriculum will be used as a starting point to develop the skills of the ISRC professional team as they establish a curriculum specific to the needs of the UA community. EAO staff and directors who will oversee the ISRC have also received extensive training from ScholarshipsA-Z and consulted with researchers for their current student engagement programs. 

Fourth, ISRC is being developed in a similar way that some of the UA Cultural & Resource Centers were created.  While the journeys of the Asian Pacific American Student Affairs and LGBTQ (Lesbian, Gay, Bisexual, Transgender, Queer and Questioning) Student Affairs offices to becoming official UA Centers are different, their stories have some similarities to how the ISRC is being developed.  For each center, students, faculty and staff identified a need for resources that met a student population’s needs.  Each office began with one staff member dedicated to serving the needs of thousands of students.  APASA was founded in 1993 and first received a part-time employee who worked 20 hours per week in the office.  LGBTQ Affairs was founded in 2007 and first received one full-time staff member to run a center that had small, dedicated physical space and few resources to organize programs and training. Today, each office has its own programming budget, one director, two graduate assistants, and student interns who organize campus events.  At its core, the story of ISRC’s development will be similar.  

We intend to utilize the SSF funding to create opportunities for a graduate student and five undergraduate students that not only provides employment, but established an environment that brings employees who want to create the change on campus that is outlined in the ISRC’s purpose.  If given the opportunity to be funded for three years through SSF funding, we will utilize the connections with community, build strong relationships with campus partners who already commit to our work, and work with institutional leaders to advocate for the creation of a permanent staff and space. 

We are confident that the ISRC proposal has been developed in a unique way that it will continue to grow and become an exemplary program of support for immigrant students and professionals who work with them on our university campus.',
'created_at' => '2016-01-30 21:13:57.000000',
'question_id' => 322,
),
281 => 
array (
'answer_text' => 'Thank you for your question. 

Digital credentials are relatively new to most people, and organizations currently employing badging are considered "early adopters." At the UA we have an opportunity to serve as national leaders by being one of the first universities to document the effectiveness of digital skills badging on student career outcomes and strengthened employer relations.

As examples, I would like to share with you some recent research related to employers’ perception of badges, as well as current trends in employer and university collaboration related to digital badges.

A recent study in the current issue of College & Research Libraries surveyed a limited sample of employers across eight key industries regarding their perceptions of digital credentials related to information literacy. After a brief tutorial of digital badging, including a representation of a badge and its content, the employers were asked about their level of interest. Only 5% indicated no interest, while 33% indicated definite interest and 62% indicating they would like to learn more.

Digital badging is part of the conversation between the higher education community and employers on many fronts. Employers increasingly seek evidence of skills that are not conveyed through a typical transcript or resume, as there is often little variation between the transcripts and resumes of good candidates. They can employ case studies or competitions as a means to discern what candidates can actually do and how well they do it before making an offer, but these methods can be costly and inefficient for employers, which may lead them to focus their recruitment on a fewer, elite schools in order to maximize their resources. 

As an alternative, the 21st Century Badging Skills Challenge, which is a high profile design lab collaboration between employers, colleges and other thought leaders currently in its third and final phase of development, has found that employers are excited and willing to work with universities and colleges to design electronic credentials that contribute to and validate student learning beyond the classroom. A blog post titled "Are Badges College Ready?" provides 10 insights from their process so far, including that "Employers Get It" and are excited about badges that will help discern the skills that are not readily apparent from a resume. The post also notes that high profile employers such as JP Morgan appreciate the efficiency of being able to recruit beyond Ivy league without having to travel across the country for initial candidate screening, and that badges could help in identifying qualified candidates beyond the typical Type A students who have the longest resumes. A survey of recent articles in both the popular business and higher education press on the topic paints a picture of an emerging technology about which there is excitement but also many questions as stakeholders together seek the best path forward toward implementation of informal curriculum and credentials that document learning beyond the classroom in a way that will be meaningful to future employers.

We are working with Career Services to discuss with UA’s top employers their interest in digital badging and any opportunities to learn and develop content together. We hope that this can be a topic of the upcoming employer summit hosted at UA on March 7.

The most important aspect about our request for student services fee funding for this priority is that we are seeking to provide engaging, on-demand curriculum to support development of key professional skills. We plan to develop curriculum for competencies such as Teamwork/Collaboration, Verbal Communication, Critical Thinking, and Problem Solving, that employers cite as highly valued and that college students frequently overestimate their own skill in relative to employers’ perceptions, according to a 2015 study by the American Association of Colleges and Universities. These are skills that can be explicitly taught and improved upon with appropriate feedback. Digital badging is a platform that will make the curriculum accessible outside of D2L or any other learning management system, so that students can access it without enrolling in a course. This will allow students to personalize their learning and track their own progress toward their goal on their own time. Students’ intrinsic motivation to perform well in their class projects, leadership roles, internships and future full-time employment can serve as motive enough to engage in the curriculum. However, digital badges also provide a way for students to gain important feedback that can help them close the gap between their desired and current skill level, while adding some extrinsic motivation.

When students earn badges, they will always have a choice as to whether or not to display them on their LinkedIn profile or link to them in electronic versions of their resumes. Regardless of whether a student who has earned a badge decides to publish it, that student will have demonstrated understanding of the discrete elements of the skill they’ve worked on as well as a developed a collection of their own examples of these skills to draw upon in crafting cover letters or resumes and responding to interview questions. This can only benefit students as they develop their fullest potential and prepare to launch their careers.

Sources:
Education Design Lab. (June 24, 2015). Are Badges College Ready? [Weblog post]. Retrieved from http://eddesignlab.org/2015/06/are-badges-college-ready/

Jaschik, S. (January 20, 2015) Well-Prepared in Their Own Eyes. Retrieved from https://www.insidehighered.com/news/2015/01/20/study-finds-big-gaps-between-student-and-employer-perceptions

Raish, V., & Rimland, E. (2016). Employer Perceptions of Critical Information Literacy Skills and Digital Badges. College & Research Libraries, 77(1), 87-113. doi:10.5860/crl.77.1.87',
'created_at' => '2016-01-30 21:51:25.000000',
'question_id' => 323,
),
282 => 
array (
'answer_text' => 'Career Services is the central hub of all career services for the UA community. Much of our focus is on all of the colleges besides Eller yet we work collaboratively with Eller as well. We have recently changed our college liaisons model to be aligned with industry clusters to address industry openings that span across majors and colleges and renamed our Career Coaches as Career and Industry Coaches. We also hired one specifically for Business, Communications and the Arts to emphasize that business and all other careers are available to all majors, not only business. We have a close partnership with College of Social and Behavioral Science, College of Engineering and College of Science. Our Career Fairs and variety of mixers are designed to serve all majors. We also offer as we always have, the types of programming to train students to be career ready that students probably hear of their Eller counterparts having to take. Career Services has always offered this to students through clubs and orgs, their respective colleges and our department. We have added webinars recently in addition to our face to face modality to deliver content. We are also redesigning our website and online resources to meet the needs of all students in all UA locations.',
'created_at' => '2016-02-01 16:01:44.000000',
'question_id' => 327,
),
283 => 
array (
'answer_text' => 'This is the application link that can be found on the GPSC website as well as on the Life and Work Connections page. https://lifework.arizona.edu/apps/childcare-forms/student-subsidy.php. 
This application is connected to the UA ID network. Upon login, students have to answer a series of questions for verification of enrollment, including graduate or undergraduate status, child(ren) information, and childcare provider information. It is a first come, first serve enrollment upon completion of the application. The second part is a form that is completed by the Childcare provider. 
The Childcare provider is responsible for completing the enrollment verification form. The provider provides specific information including: Childcare address, Tax ID or Social Security number associated with the Childcare provider, child(ren) name(s), Date of Birth, Child Care Fee (indicating either an hourly, weekly, or monthly rate) including the full or part time enrollment status of the child(ren). The provider is the person who signs this form and agrees with the content that includes that the individual is an award recipient with the student Child Care & Housing Subsidy Program (“Subsidy”) for this current academic and July 1-June 30 fiscal year period. That the student is solely responsible for contracting with the provider’s child care program. That the Subsidy Program is designed specifically to help eligible UA students pay for qualifying, child care occurring in Arizona regarding their classroom commitments. The provider will be required to provide child care program information about the child care charges, the parent’s payments and eligibility as a qualifying child care provider, as part of the Subsidy Program. I understand UA Life & Work Connections’ Child Care and Family Resources will provide the forms, and that it is the parent’s responsibility to bring them to the provider for completion.
Once completed, the form needs to be submitted by the awarded student via email or physical delivery to the UA Life and Work Connections address.',
'created_at' => '2016-02-03 19:11:34.000000',
'question_id' => 332,
),
284 => 
array (
'answer_text' => 'GPSC is making a huge stride to increase our marketing, specifically around Child Care. GPSC will soon be launching our new website which will feature the Child Care Program under the resource tab, as well as periodically feature it on the revolving carousel during peak enrollment times (beginning of Fall and Spring semesters). GPSC has recently increased our Facebook advertisement campaigns by adding additional boosts to reach our growing number of followers (2,390 likes). We have coordinated with the Grad Center and Cultural centers to promote our newsletter through their listserves to promote the program. We are working more closely with ASUA in promoting this program to the undergraduate and graduate students who utilize their resources and space on a regular basis.',
'created_at' => '2016-02-03 19:14:09.000000',
'question_id' => 332,
),
285 => 
array (
'answer_text' => 'Thank you for the opportunity to explain our proposal on January 29th and now respond to your follow-up question.  When we launch the Campus Events and Engagement internships, we would like to be able to select students from a wide range of majors on campus who are involved in diverse student activities.  We believe offering stipends instead of credit through a particular academic department is the more favorable option and will afford us this flexibility.  As time goes on, we may determine that students from particular majors, such as Architecture, Planning, or Public Management and Policy, are best suited for the type of work involved in the internship.  Once we identify the most appropriate academic department or departments, we can explore the possibility of partnering to offer credit for the internships in lieu of stipends.  Our initial period of working with student interns funded by stipends will allow us to fine-tune the internship program before approaching academic departments.',
'created_at' => '2016-02-03 23:21:48.000000',
'question_id' => 324,
),
286 => 
array (
'answer_text' => 'Career Services consistently has new programming and new employers coming to campus. Furthermore, given that new students enter as freshmen and transfer students, new faculty arrive each year and there are always more employers to go after. Career services work is never done and must remain proactive to maintain faculty, student, alumni and employer relations. Therefore, these roles are critical for outreach to students, presentations requested by faculty, and follow up to employers to engage them with campus. Last year career services worked with approximately 100 new companies!

Ambassadors can help us reach thousands more students with professional development and outreach messages to promote services, programs and events that benefit them such as networking mixers, career fairs, etc. In addition, ambassadors help us reach hundreds of employers to engage them beyond simply checking out our website or posting one job. Our goal is to engage the employers further with additional, regular job postings as well as internship opportunities and invite them to events. It all requires a lot of follow up, follow through and leg work.',
'created_at' => '2016-02-04 21:22:52.000000',
'question_id' => 325,
),
287 => 
array (
'answer_text' => 'Career Services is available to every student of UA whether distance, online or on campus and regardless of race, ethnicity, backgroud, religion, etc. Career Services abides by the National Association of Colleges and Employers Standards for Professional Conduct and adheres to laws and regulations related to the Equal Employment Opportunity act.

Career Services has a high tech and high touch approach to serving students, alumni and employers. Following are some numbers from last year.

>Combined in-person and web visits 
Total web visits: 			471,950
Total social media hits/likes/follows:	4,586
Total counseling appointments:		2,913
Total walkins:				2,638
Total event attendance:			8,350
Total workshop attendance:		2,198___    
Total web + in-person “touches”:	492,635

That is over 470,000 or nearly half a million hits to our website! These are true hits as in visitors who visited the page and stayed on the page, they did not bounce off to another page right away (those were edited out). In addition, the unique number of students having an appointment with career services increased 16.5% and resumes referred to employers increased 85%. Students resumes are referred multiple times to different employers, therefore, an individual student\'s resume may be referred to many different employers.

We employer outreach which has led to increased employer engagement on campus.
>Posted 2,517 new internships for students (35% increase over FY14)
>Posted 11,208 new local positions for students (16% increase over FY14)
>Posted 9,426 new full-time positions for students (53% increase over FY14)

In addition, Career Services staff and ambassadors will conduct in classroom presentations to 250-300 classrooms in about 65 plus different course sections throughout the colleges across UA.

Due to the sensitive nature of the work of career services to make career opportunities available to all students and to mitigate risk of claims of discrimination it is best for career services according to legal council to not store ethnicity information in the career services management software as it is in the student information system. However, we do track class level attending events, using Wildcat Joblink, etc.

Freshman	14.36%		5804 unique
Sophomore	14.70%		5947 unique
Junior		18.53%		7494 unique
Senior		34.65%		14012 unique
Grad		9.30%		3763 unique
Alumni		8.46%		3423 unique
Total:	40,443 unique users

Hopefully this gives an adequate enough snapshot of what our department offers.',
'created_at' => '2016-02-04 21:37:37.000000',
'question_id' => 331,
),
288 => 
array (
'answer_text' => 'At this time, the Travel Grant Program receives funding from the Graduate College, Confluence Center and UA Alumni Association. The Graduate College is the largest contributor giving us $50,000 in funding per annum, with the Confluence Center contributing $5,000/year and the UA Alumni Association contributing $1,000/year respectfully at this time. The GPSC also contributes anywhere from $20,000 to $40,000 in our own departmentally granted funds to the program as our budget allows. This year the GPSC is also entering into a working relationship with Women in Science and Engineering to help support a recipient who 1) self-identifies as a woman; and 2) is pursuing a degree in a STEM field (broadly defined, including the social sciences). The amount that was contributed to the TGP was $750 (which is the maximum amount that can be requested per applicant). This amount could essentially assist 1 or more applicants who fulfill the requirements depending funding request. This is one of the GPSC’s first steps toward becoming a central hub for the travel funds that The UA’s Graduate and Professional students are requesting. To this end, we have been working on a website initiative with the goal of not only being able to serve as a funding source, but also to be the aforementioned central hub in providing information and other resources regarding travel funding access to students. We hope that this initiative will lead to expanded relationships and additional funding avenues in the future.',
'created_at' => '2016-02-05 17:08:36.000000',
'question_id' => 330,
),
289 => 
array (
'answer_text' => 'This is the application link that can be found on the GPSC website as well as on the Life and Work Connections page. https://lifework.arizona.edu/apps/childcare-forms/student-subsidy.php. 
This application is connected to the UA ID network. Upon login, students have to answer a series of questions for verification of enrollment, including graduate or undergraduate status, child(ren) information, and childcare provider information. It is a first come, first serve enrollment upon completion of the application. The second part is a form that is completed by the Childcare provider. 
The Childcare provider is responsible for completing the enrollment verification form. The provider provides specific information including: Childcare address, Tax ID or Social Security number associated with the Childcare provider, child(ren) name(s), Date of Birth, Child Care Fee (indicating either an hourly, weekly, or monthly rate) including the full or part time enrollment status of the child(ren). The provider is the person who signs this form and agrees with the content that includes that the individual is an award recipient with the student Child Care & Housing Subsidy Program (“Subsidy”) for this current academic and July 1-June 30 fiscal year period. That the student is solely responsible for contracting with the provider’s child care program. That the Subsidy Program is designed specifically to help eligible UA students pay for qualifying, child care occurring in Arizona regarding their classroom commitments. The provider will be required to provide child care program information about the child care charges, the parent’s payments and eligibility as a qualifying child care provider, as part of the Subsidy Program. I understand UA Life & Work Connections’ Child Care and Family Resources will provide the forms, and that it is the parent’s responsibility to bring them to the provider for completion.
Once completed, the form needs to be submitted by the awarded student via email or physical delivery to the UA Life and Work Connections address.',
'created_at' => '2016-02-05 18:30:19.000000',
'question_id' => 326,
),
290 => 
array (
'answer_text' => 'At this time the GPSC has not secured an additional funds for the POD program. We have communicated with outside sources like the Graduate College for additional support and will continue to do so. We have also designated a position within the GPSC (titled Development Administrator) to increase our communication with GPSC alumni in pursuit of future donations to support programs like POD.',
'created_at' => '2016-02-05 20:38:56.000000',
'question_id' => 329,
),
291 => 
array (
'answer_text' => 'Students/Workshop leaders are “hired” by the GPSC to lead workshops for $12/hour with a maximum of 52 hours. For our final stipends for FY15 presenters, we only had one presenter almost reach the anticipated maximum and she was paid $544 for a total of 6 workshops conducted.',
'created_at' => '2016-02-05 20:39:59.000000',
'question_id' => 335,
),
292 => 
array (
'answer_text' => 'The Student Unions has access to a variety of marketing channels. To increase student awareness of and engagement with the Technology to Work initiatives we would utilize these channels as follows:

1. Buzz Report 
• CATPRINT Kiosks: create a simple ad featuring Student Union specific hashtags (i.e. #EatatSUMC, #EatatPSU, #HighlandBurrito, etc.) that students can use when they post on social media. These hashtags would contain keywords that we include in the Buzz Report’s search filters to help us gather student feedback. 
• Food Court Plasmas: create an eye catching ad that runs every Friday showing the number of responses we’ve made that week to improve the student experience at the Student Unions based on their feedback. This ad would be updated weekly, and would begin to show a running total of responses as time progresses. 
• Gallagher Theater Pre-Show Slides: combine the CATPRINT Kiosk and Food Court Plasma ad concepts into one larger ad to be shown to all Gallagher Theater patrons before the two movie screenings each week. These pre-show slides rotate through multiple times before a movie screening, which provides a great opportunity to maximize the power of repetition to help students remember the Student Unions hashtags and see how many times we have been able to respond to their online feedback.
• Showcase Posters: similar to the Gallagher Pre-Show Slide, we can create a Showcase Poster featuring our hashtags to continue reminding/encouraging students to use them when they post about the Student Unions on social media.

2.Nutrition Information Website
• Student Unions Website: we would create a “Did you know” style ad to fill one of the blocks on the Student Unions website homepage with an interesting nutrition fact that links directly to the Nutrition Information Website when clicked on.  
• Student Unions Social Media Accounts: we would post the Nutrition Information Website link on our various social media pages/accounts with fun nutrition facts about dining at the Student Unions to generate interest and curiosity leading students to the website. 
• Student Union Listservs: word of mouth is a quick and easy way to spread the word about the Nutrition Information Website, so we’ll send out the link with more information about this new tool and encourage student employees and full-time staff to tell their friends!
• Table-toppers: displayed on tables in five restaurant locations on campus and throughout the food courts at SUMC and PSU, content in these ad spaces have a large reach. We will create an ad to be included on table-toppers directing students to the website when they want nutrition information about the food available to them when dining on campus. 
• Showcase Posters: similar to the ad we create for the Student Unions website homepage, we can create a “Did you know” style Showcase Poster to introduce the Nutrition Information Website to students and the University community, highlighting the website’s main features and tools.
• Tabling at Events like Student Orientations: The Nutrition Information Website will be mentioned during Student Orientations so incoming students and their parents are aware of this resource. The Student Unions Registered Dietitian will offer tabling events in various on-campus restaurants to showcase the website, engage students, and answer questions.

3. Charging Stations
• Table Toppers (for tables with a wireless charging pad): create a full crown table-topper ad to highlight which tables have the wireless charging pads on them (i.e. “Charge Your Device Here with a downward pointing arrow”), and combine that with a column ad on the table-topper (beneath the crown ad) to provide instructions on how to set your phone/device to charge on the pad. 
• Table Toppers (for tables without a wireless charging pad): to create awareness of the  secure charging stations on the Student Unions walls we would play off the wireless charging pad table topper ad to say something like “Are you sad that this table doesn’t have a wireless charging pad? Check out the secure charging station next to [location] and turn that frown upside down!” 
• CATPRINT Kiosks: create a simple ad that informs students of the new device charging options available to them at the Student Unions. 
• Food Court Plasmas: create two separate ads to inform students about the wireless charging pads on some Student Unions tables and the secure charging stations at specific locations throughout the Student Unions. Both ads would be bold and eye catching to encourage students to take advantage of this service every time they visit the Student Unions.',
'created_at' => '2016-02-08 21:43:56.000000',
'question_id' => 344,
),
293 => 
array (
'answer_text' => 'Colleges that do not participate in PASS have a variety of options.  Some colleges use Wildcat Track through Think Tank, some have their own programming (many offer a success course).  Our partner colleges (Eller College of Management, College of Social and Behavioral Sciences, College of Medicine/Pre-Physiology, and the College of Public Health) have chosen to use PASS as their Academic Recovery program because of our Peer-to-Peer model and our track record of success.

Our partner colleges cannot currently offer their own Academic Recovery programs as their staff are already overburdened (e.g. there are about 2,000 students who are pre-physiology and only 4 pre-physiology advisors) and they do not currently have college level funding for Academic Recovery.  Our program model is also very cost-efficient.  Utilizing peer advising, in addition to being very effective programmatically, allows us to provide services for a large number of students with fewer resources.  To provide enough professional staffing to serve all of these students would more than double the cost of personnel (approximately $360,000 compared to the current request for $173,800).',
'created_at' => '2016-02-10 21:47:27.000000',
'question_id' => 339,
),
294 => 
array (
'answer_text' => 'In the additional documents submitted, we have comparison numbers for change in GPA.  Below are comparison numbers for retention.  As cohorts have been here a while, “retention” goes down and one of the reasons aside from students stopping out is that some from that cohort have been awarded degree, but degree attainment is also a marker of success.  Below is a comparison of the success of PASS participants versus the comparison group, defining “success” as still enrolled during Spring 2016 or having attained their degree.

Fall 2013 cohort
PASS Participants who are still enrolled or have graduated: 44%
Comparison group who are still enrolled or have graduated: 25%

Spring 2014 cohort
PASS Participants who are still enrolled or have graduated: 45%
Comparison group who are still enrolled or have graduated: 27%

Fall 2014 cohort
PASS Participants who are still enrolled or have graduated: 47%
Comparison group who are still enrolled or have graduated: 24%

Spring 2015 cohort
PASS Participants who are still enrolled or have graduated: 51%
Comparison group who are still enrolled or have graduated: 22%',
'created_at' => '2016-02-10 21:48:31.000000',
'question_id' => 340,
),
295 => 
array (
'answer_text' => 'UITS provides funding to cover staffing costs for Executive Director and related overhead.  The UA Libraries provide funding for renovation, maintenance, equipment and partial staff time to support iSpace. They also provide funding to staff two students to support further innovation-related programming within iSpace.  Additional matching support is provided by Student Affairs including funding to for partial staff time as well as partial student time in support of finance and marketing administration.',
'created_at' => '2016-02-11 18:12:58.000000',
'question_id' => 336,
),
296 => 
array (
'answer_text' => 'During 2015-2016, across all InnovateUA programs, we have or will directly serve approximately 2500 students with an estimated 2000 of those being unique students.  The expanded programs we intend to run for next year, will grow those numbers those numbers to grow to between 3500-5000 students with 80% of that total estimated to be unique students as well.',
'created_at' => '2016-02-11 18:24:17.000000',
'question_id' => 337,
),
297 => 
array (
'answer_text' => 'The Office of Scholarships and Financial Aid (OSFA) makes every effort to mitigate the number of students requesting service and reducing wait times.  Virtually every decision within our office in evaluated with student impact in mind.  The reduction in wait times can be attributed to several intentional actions within our office, some big and some small.  

While it may appear that the number of student’s requesting service is going down, the number of unique students served is actually increasing.  From August 2014 to August 2015 the number of student inquiries that our office was able to respond to was up nearly 12%.  Students who do not want to wait will repeatedly call, email, or visit our office. With continued Rush funding we have been able to hire and retain top student and office specialist talent.  As the tenure in our front line staff increases the level and speed of service increases as well.  The volume of overall attempted contacts is down because with Project Rush we are able to answer student’s initial inquiries and resulting in fewer repeat callers.

Since the conversion to UAccess a few years ago, OSFA has made many improvements and modifications to the program improving the delivery of financial aid and communication regarding outstanding financial aid requirements. Financial aid requirements are now incorporated into the Next Steps Center for new students.  OSFA utilizes To Do items in UAccess Student and email communications to prompt for action from students. On a daily basis we look for opportunities to improve the service to students from creating and uploading videos to our website for online students to adding functionality to the UAccess Guest Center allowing parents to now request parent plus loans faster.

This fall OSFA also implemented a new integrated messaging system for students to hear when the phone queue was too full to allow them to wait.  This messaging system provides valuable information allowing students to resolve their problems without needing to ever speak to a representative.  Prior to the new integrated messaging system if the phone queue was at capacity students would hear a busy signal and just have to call back.  This one change alone seems to have reduced call attempts during the rush season by as much as 30-40%.  For the upcoming year we are piloting a new email communication strategy to incoming freshman and transfer students to prompt them to take action on their financial aid sooner.  For the first time OSFA will send emails to non-UA emails listed on the FAFSA to students and parents informing them of financial aid requirements that still need resolved.  

Again, while the wait time for students has decreased as a direct result of Project Rush funding, the number of unique students requesting customer service at the Financial Aid office has not decreased and we anticipate the number of students requesting service to continue to rise. In the near future OSFA will face many challenges with major changes coming from the Department of Education.  Beginning this year the FAFSA is going to be available in October, 3 months earlier than prior years.  Next year the Department of Education is making all schools follow their logic for selection students for verification.  We have done a preliminary analysis and anticipate a 300% increase in the number of students required to complete verification.  Every effort is being made to implement these changes while reducing the impact to our current and incoming students, Project Rush is a vital piece of delivering financial services to University of Arizona students.',
'created_at' => '2016-02-13 22:53:33.000000',
'question_id' => 343,
),
298 => 
array (
'answer_text' => 'There is definitely a correlation between employee count and reduced wait times.  Without Project Rush’s student staff and office specialists, there are 6 full-time client service counselors and approximately 8-10 student workers that work approximately 15 hours a week to handle student contact.  Any reduction in Project rush staffing to handle the front line contact would have an exponential impact raising service levels and delaying the delivery of financial aid and information to many students.  With Project Rush staff we are better able to answer students’ questions the first time they visit, call or email.  Analysis of phone statistics during the month of August shows a 5 minute increase in wait time causes an increase in calls abandoned (people who hung up while waiting) by nearly 70%.  Additionally we have found that emails not answered within 48 hours either prompt another email, lobby visit or phone call.  Both of these statistics show the importance of Project Rush support during the  opening of school because the extended wait/response times has a exponential snowball effect causing more phone calls, more emails and more lobby visits instead of answering their questions the first time.  This heightened volume due to multiple contacts leads to much longer wait and response times that has a direct impact on student academics and retention.',
'created_at' => '2016-02-13 22:54:03.000000',
'question_id' => 342,
),
299 => 
array (
'answer_text' => 'The Office of Scholarships and Financial Aid (OSFA) is always considering options to mitigate or to best handle the rush of students that visit, email or call our office.  Two years ago, students from Eller conducted a study of our customer service as part of a class project. Their recommendations included requesting additional funds to develop a scheduling system to implement during the non-rush/less busy periods of the semester. After a thorough joint review of their study, they supported our rationale listed below for not implementing this system during our busiest times of year. While we would love additional funding for an appointment based scheduling system, we chose not to request more than the minimum funding necessary from the SSF Board. 

During the weeks leading up to and after the first day of class, the number of student contacts is constant from open to close leaving no staff availability for appointments until a few a weeks into the semester. Since we are already at peak capacity, additional staffing for appointment based scheduling during this period would cause other students to have to wait longer, cause delays in aid processing, and create other inefficiencies because students frequently don’t show up on time or at all when appointments are scheduled.  Additionally, appointment scheduling would only address those students that come to the lobby in person and not take into account the thousands of phone calls and emails we handle during that period.  Our overall strategy as an office is to mitigate the number of students that need service from our office and reduce wait times for those that do visit.',
'created_at' => '2016-02-13 22:55:09.000000',
'question_id' => 341,
),
300 => 
array (
'answer_text' => 'We do not know the likelihood of receiving the other funding listed in our budget because it is decided by a student board, similar to the Student Services Fee. We have submitted our proposal for renewal funding and we will be contacted to give a presentation like we did for SSF. The board has posted that decisions will be made by May 6th. We have made outstanding progress and produced excellent results due to their prior funding and we explained in our proposal the critical need for the funding to be renewed. The request also is less per year than in prior years because ERE for full-time staff has come down some. We have done all that we can with the resources we have and we are hopeful the board will continue to support us, but we have no way of knowing what the actual decision will be until the board informs us.',
'created_at' => '2016-02-19 01:31:39.000000',
'question_id' => 345,
),
301 => 
array (
'answer_text' => 'We are carrying the minimum number of student positions we need at this time (see proposal appendix). The project suffered a cut to a full-time staff position this past year, and we have not asked to recoup funding for that the position, or for additional funds for student positions or Graduate Assistantships to make up for the entire loss of that position. We could use additional funding and we have some deserving graduate students on the team we can’t offer GA’s to, but we are trying to be mindful of the funds SSF has available. Given the progress and growth of the project and the loss of a full-time position, we hope it is understandable how important our student team members are to the project.',
'created_at' => '2016-02-19 01:32:38.000000',
'question_id' => 346,
),
302 => 
array (
'answer_text' => 'We absolutely believe $1,000 is a reasonable budget amount for each Faculty Fellow (not only in the halls, but also in the cultural centers and resource centers). In residence halls like Coronado and Arbol de la Vida, Fellows are tasked with engaging more than 700 students a year on an annual budget of $500 – which calculates to , at best, $0.70 per student each year. Even a mid-sized hall, like Villa del Puente, houses 300 students which gives a Fellow assigned to that hall a budget of $1.67 per student each year. Outside of the residence halls, in resource centers like CATS Academics which support 500 student athletes, Fellows have a budget of $1 per student each year. Doubling the “per Fellow” budget from $500 to $1,000 or, conversely, $1 per student to $2 per student, is certainly reasonable. While Faculty Fellows are modest and creative in their spending, our request for a $1,000 budget per Fellow is in acknowledgement of the age old adage – “you can do more with more.” Faculty are so excited to do more, but simply need the resources to do so. 

If a Fellow advises us that their plans would not require the $1,000 allotment, we will re-assign their funds toward our “cross-collaborative, cross-disciplinary, multi-Fellow” programs initiative (we are working on a catchier name, but we in Student Affairs and Enrollment Management, Academic Initiatives and Student Success, do tend to have an affinity for long names). This initiative gets faculty collaborating from different colleges, departments, and sites to host large-scale programs that reach vast numbers of students, many of which may not have an assigned Faculty Fellow. This initiative has been inspiring for the faculty and beneficial for the students as it provides them the opportunity to connect with people outside of their typical realm. Additionally, it provides an introduction to our program for students who may not engage at sites where Fellows are assigned. These types of programs can often be more costly than traditional Faculty Fellows programs, so unclaimed monies would be used to fund this initiative.

Currently, our Faculty Fellow events budget is $15,000 per year, but to date in Fiscal Year 16, we have spent $12,000. In Fiscal Years 12, 13, 14, and 15 we consistently spent $20,000 - $25,000 on Faculty Fellows programs, illustrating our desperate need for sufficient funding. We used reserve funds in past years to compensate these overages, but can no longer afford to do so. Furthermore, our program continues to expand into areas that have large need for Faculty Fellows. For example, next year we will expand into SALT Center to provide support for students with learning disabilities. Since 2013, we have grown our program 50% from 30 Faculty Fellows to 45 to service students outside of the halls who also need support, such as students in the VETS Center, LBGTQ Resource Center, and Women’s Resource Center. Amongst all of this growth, our program budget has remained the same ($15,000), so we have broadened our outreach, but do not currently have the means to make the impact in which we are capable. In addition to expanding our sites, it is our hope to engage higher percentages of students at our current sites, of which there are 36. This, again, will require additional funding.

Lastly, we cannot stress enough that this program is directly aligned with the purpose of the student services fee – to enhance the student experience at the University of Arizona. Should we be awarded this grant, your dollars will go toward programs that directly impact students, and ultimately, make this large school feel much more personable.

Thank you so much for your consideration,
Jessica',
'created_at' => '2016-02-19 02:58:00.000000',
'question_id' => 347,
),
303 => 
array (
'answer_text' => 'Transforming Our Campus Community will be widely marketed to students, faculty and departments through various outreach methods including, but not limited to, directly emailing all enrolled students about support opportunities and distributing brochures and handouts to students during orientation, direct email to leaders of registered student organizations, outreach to ASUA, Welcome Week, etc. Faculty will be engaged through direct email and through outreach to Deans and department heads. Passive education about the program in the form of posters and digital announcements will be distributed throughout the student union, residence halls, and select campus buildings.

The Inclusion and Multicultural Engagement unit of the Dean of Students Office does not have authority to mandate training for all faculty and staff. However, we have already created strategic partnerships with academic units to offer training. These departments include Geosciences, Molecular and Cellular Biology, College of Education, Eller School of Business Marketing Department, and staff organizations such as the University Professional Advisors Council to offer training. All staff in the Dean of Students Office have been participating in a year-long mandated 12-hour training implemented by the Associate Dean of Students during this current academic year. This training has been a model for the Associate Dean and Directors of the Cultural and Resource Centers in developing this proposal. Should faculty and staff training become mandatory university-wide we would be ready to collaborate with other units such as Human Resources, the Office of Institutional Equity, and Faculty Affairs to deliver training. 

Given the feedback received during the faculty senate listening tours to the six cultural and resource centers this fall, students described very clearly the need to offer more training to faculty and staff. We developed this proposal out of that feedback as a way to provide more comprehensive training which would enhance the current efforts of the centers and allow the centers to focus on directly serving students.  ',
'created_at' => '2016-02-21 02:41:30.000000',
'question_id' => 348,
),
304 => 
array (
'answer_text' => 'The Transforming Our Campus Community proposal was developed at the end of the fall semester with direct involvement and input from the Directors of African American Student Affairs, Asian Pacific American Student Affairs, the Guerrero Student Center, LGBTQ Affairs, Native American Student Affairs, and the Women’s Resource Center. The Directors of these centers report to the Associate Dean of Students for Inclusion and Multicultural Engagement. 

After the Faculty Senate Listening tours we held a staff retreat to discuss priorities and challenges for the six centers. The Directors all expressed a desire to do more campus training and they recognized they were at capacity with the current demands and level of staffing in their centers. The proposal addresses 2 specific areas of need identified by the Directors. 

1. There is limited staffing in each of the centers to provide the level of direct outreach and advocacy to individual students needed in each of the centers.

2. With the exceptions of the Women’s Resource Center, with the full-time coordinator (OASIS) position and LGBTQ Affairs, with a 1/2 time graduate assistant position for SafeZone training, there are no other staff to conduct training without taking away time from serving students directly in the centers. And even within the capacity of the full-time position and GA position, there are more training requests than can be accommodated.',
'created_at' => '2016-02-21 03:16:36.000000',
'question_id' => 349,
),
305 => 
array (
'answer_text' => 'The Timeline for Implementation

To be completed by August/beginning of Fall 2016
-Hire Director of Training and Education
-Recruit and hire GAs for cultural resource centers
-Recruit and hire GAs and student facilitator/assistant positions for training and education
-Complete GA training for cultural resource centers

September 2016
-Conduct training for GAs and student facilitators for training and education
-Refine curricula from current training being offered through Inclusion and Multicultural Engagement and cultural resource centers
-Begin in-service training for cultural resource center GAs, including training from CAPS, Student Assistance, community resources, bias incident support
-Include GAs and student facilitators on any on-going training to observe
-Develop training publicity materials

October 2016 - May 2017
-Publicize training resources
-Begin implementing training and consultation with departments and students organizations
-Conduct on-going assessment of training
-Continue to develop curricula based on requests from student organizations and departments
-On-going in-service and case management with cultural resource center GAs
-Review assessment and outcome data from training programs

Summer 2017
-Conduct comprehensive overview of assessment data
-Refine curricula
-Revise publicity materials
-Revise training as needed
-Begin recruitment for 2017-2018

Fall 2017 and Spring 2018 would look similar but with the assumption that the training and education director will stay in place and that some GAs and student facilitators will continue in their positions. Training will be modified based on the GAs and students in place.',
'created_at' => '2016-02-21 06:02:21.000000',
'question_id' => 350,
),
306 => 
array (
'answer_text' => 'This year, UEMS received $20,000 to purchase and outfit the used suburban from ASUA. ASUA also provides the accounting and office services required to operate the program in accordance with University policies and state and federal laws. Typically, UEMS will request a funding amount from ASUA for special projects that were not foreseen or that require additional funding in order to fulfill. For this upcoming budget cycle, we will be requesting funds from the new executive group of ASUA as they begin to do the budget for FY17 as a supplement to the SSF budget that has been requested, with the exact amount to be determined as we continue developing our goals and operational structure for next year. The additional funding amount usually bridges the gap with operational costs, and enables us to continue expanding our education outreach and response capabilities. In addition to funding directly from ASUA operations, we also make funding requests to the ASUA Senate when we are seeking funds for projects. ASUA was also instrumental in helping to secure our base of operations with Residence Life.',
'created_at' => '2016-02-21 06:53:56.000000',
'question_id' => 351,
),
307 => 
array (
'answer_text' => 'Over the past two years, UEMS has been working diligently to create and enact intergovernmental agreements with UAPD, Tucson Fire Department and the Pima County Wireless Integrated Network (PCWIN) group in order to become a full operating member on the PCWIN radio network. The PCWIN network is the new advanced digital trunking radio system that the county has built for all public service agencies to operate on, including UAPD and other city resources. This year, UEMS successfully completed the process to become a member of this network, and we are now able to use the two handheld radios that we have to communicate with other agencies during our 911 operations. As we continue to grow rapidly, we have the need to purchase more radios so that we can operate more units during standby events, and also to enhance our communications capabilities with the city communications center and other public safety units. By purchasing more radios under the communications budget request, we will be able to operate more efficiently and safely while using the network.

In addition to the radios and voice communications capabilities with TFD, we would like to purchase mobile dispatch tablets (MDT) that will allow us to connect directly with the TFD computer aided dispatch (CAD) system that generates and monitors all 911 calls in the city. By utilizing the MDT’s, we will be able to receive significantly more information about calls while still responding. This information will enhance the safety of our crews, preventing them from entering dangerous situations, and will also enhance our service to the community by being able to more appropriately address our patients needs by having information ahead of time. Overall, the communications equipment we are seeking will greatly enhance our ability to respond safely, efficiently, and more accurately to 911 and standby medical calls. The enhancement from this communication equipment will significantly benefit the public due to our increased capabilities, and will also enable us to provide enhanced training for our members that will help them in their future careers in public safety or otherwise.',
'created_at' => '2016-02-21 06:54:27.000000',
'question_id' => 353,
),
308 => 
array (
'answer_text' => 'Currently UEMS is requesting a significant amount of volunteer time and effort to be put in by our members in order to operate the highly efficient and professional EMS service on campus. Many of our members must work other jobs in order to make ends meet, and the requirements of a nearly full-time volunteer program and other paid jobs make it difficult for our students to also focus on their schoolwork and career growth, making our very successful growth unsustainable as a purely volunteer group. While we have increased our membership numbers in an attempt to meet demand and alleviate some of the hours requirements, it becomes impractical to increase our membership numbers much further at this time due to the large amount of resources and training required to keep our EMTs up to state and national certification requirements. Other volunteer and hybrid departments across the country have faced similar issues, and one of the best solutions that has been created is to form a partially paid and partially volunteer agency that utilizes either a stipend system or one that requires an EMT to work one volunteer shift in exchange for every paid shift. The proposal we would like to make would include a stipend payment each semester for our EMTs to help alleviate the pressure of having to work as many hours at other jobs, and which would allow us to maintain our 24/7 operations while keeping costs to a minimum. In our model we would pay each EMT a stipend at the beginning and end of the semester, and would require a commitment to work a minimum of 48 hours on shift per month (12hours/week) in return. Utilizing a stipend payment with a promise of active service in return allows us to keep logistics and operational costs low, while also providing more on campus jobs, increased professional and career training, and enhance campus safety by having our professional EMS service in place. We have checked and we are able to do the stipend process.',
'created_at' => '2016-02-21 06:54:38.000000',
'question_id' => 353,
),
309 => 
array (
'answer_text' => 'We have not requested funding from ASUA Club funding for travel to the conference because we are not a recognized club, rather we are a program. After reading your question we reached out to ASUA to see the possibilities of funding and they responded "Hi Ben, Thanks for reaching out. Unfortunately the ASUA Appropriations Board is only able to fund student organizations that are officially recognized as a club by ASUA. There is a whole recognition process that clubs have to go through to become an ASUA recognized club. We would not be able to fund student travel for a residence life program." Feel free to ask any more questions that will help Student Services Fee get a better understanding of the Off-Campus Housing Sustainability program.',
'created_at' => '2017-01-24 16:57:47.000000',
'question_id' => 361,
),
310 => 
array (
'answer_text' => 'The Games Room is a student engagement space within the Student Union that does not receive external funding.  The only funding for the Games Room comes from the sales generated in the Games Room throughout the year.  Because this is a student engagement space, prices are set considerably lower than they would be in a typical retail environment to accommodate student’s spending habits.  These reduced prices do not generate enough revenue to complete projects such as the eSports viewing area, which is why we are requesting funding.  In fact, the Games Room budget runs in the red in order to provide a safe and accessible gaming space for students, so funding updates is often a challenge.  The computers being utilized for this are supplied and serviced by OSCR and are not property of the Games Room.',
'created_at' => '2017-01-26 18:13:42.000000',
'question_id' => 360,
),
311 => 
array (
'answer_text' => 'Approximately 20 classes are offered in Gallagher Theater every year.  

Gallagher Theater is reserved for classroom use in the Fall from 7:30 AM – 4:20 PM M, W, F and 7:30 AM – 3:45 AM T, TH.  

Gallagher Theater is reserved for classroom use in the Spring from 7:30 AM – 4:20 PM M, W and 7:30 AM – 3:45 AM T, TH.  

Gallagher Theater is reserved for classroom use from 7:30 AM – 7:30 PM for Finals Week in the Fall and Spring.',
'created_at' => '2017-01-26 18:15:05.000000',
'question_id' => 358,
),
312 => 
array (
'answer_text' => 'In addition to a suggestion box available at the Gallagher Theater Box Office we also created a survey that was completed by attendees at the RHA Block party at the start of the school year.  This survey helped us identify the types of movies that students are interested in.  Blockbuster films, for example, scored quite favorably and this result is reflected in the attendance numbers for the Blockbuster films shown last semester.  Moving forward we plan to conduct additional surveys throughout the year to identify the films that students would like to see.  Our marketing plan for the next school year includes a survey available to every attendee, which when completed will enter them into a raffle to win prizes such a free movie tickets, popcorn, or candy.',
'created_at' => '2017-01-26 18:16:03.000000',
'question_id' => 357,
),
313 => 
array (
'answer_text' => 'Last semester Gallagher Theater saw 802 attendees.  On average Gallagher sees 120 – 145 attendees a week.  Average movie attendance varies greatly depending on the type of film being shown.  We have been adjusting throughout the semester to better match student desires.  When we show a blockbuster film (last semester this included selections such as “Suicide Squad” and “Magnificent Seven”) we see larger turnout than when we show an independent or foreign film.  Because of this trend we are showing a far greater number of blockbuster films (“Arrival”, “Moana”, “La La Land”, and “Fantastic Beasts and Where to Find Them” to name a few) this semester.  Attendance is tracked through our ticket sales by the student workers that operate Gallagher Theater.',
'created_at' => '2017-01-26 18:17:32.000000',
'question_id' => 356,
),
314 => 
array (
'answer_text' => 'In a typical year, 50-60% of our peer mentors are returners, with most employees working for the program for an average of two years. Having students return for another year (or two) to the position, allows for leadership opportunities (Project Lead or Student Coordinator), as well as consistency of voice and message. Having multi-year funding allows us to ensure student employment and financial support into a new year, as well as allows for multi-year professional development plans.',
'created_at' => '2017-01-26 20:31:38.000000',
'question_id' => 354,
),
315 => 
array (
'answer_text' => 'The increase in requested funds is for both student employees and student participants. 

While we have 2.5 FTE devoted to ASA Peer Mentoring, these SSF funds will support the hiring and development of student employees. We have been working to expand our identity-based peer mentoring tracks for students with the introduction of a first generation college student track, and growth of the cultural learning communities. This funding would allow us to add a learning community in partnership with the Guerrero Center, the only Cultural Center that is not currently represented in our identity/culturally based communities. We currently have partnerships with African American Student Affairs (360), Asian Pacific American Student Affairs (EDGE), & Native American Student Affairs (FYS). 

The goal of hiring more staff is ultimately to increase the number of students we’re able to serve throughout all of ASA Peer Mentoring. Anticipating the growth in participation numbers, we must also prepare for more resources to be utilized by the students. These include office supplies such as pens, paper, poster paper, dry-erase markers, copies of worksheets, etc. These are all materials used during workshops, discussion groups, and one-on-one sessions. Food is also sometimes provided at large program events a couple of times throughout the semester. Funding for operations is critical in providing fun and interactive ways of engaging with student participants.',
'created_at' => '2017-01-26 20:33:07.000000',
'question_id' => 355,
),
316 => 
array (
'answer_text' => 'When the initial agreement was established between the Student Unions and Room & Course Scheduling, the thought was that classes taking place within Gallagher Theater would bring additional students into the Union.  These additional students would then potentially bring in additional food service revenue.  There is unfortunately not a way to specifically track if students from Gallagher are producing extra food service revenue.  Because of that there is not a way to allocate money from said revenue to Gallagher Theater for classroom maintenance.',
'created_at' => '2017-01-26 22:15:56.000000',
'question_id' => 359,
),
317 => 
array (
'answer_text' => 'Global Experiential Learning (GEL) Programs in Inclusion and Multicultural Engagement offers an accessible global learning opportunities for historically marginalized and low socioeconomic scholars. Students will engage in transformative education through self-reflection, intercultural engagement, dialogue, and service learning that will give them deeper insight and understanding of their own cultural perspectives while building a cultural competent community that can be returned to the University of Arizona campus. These programs are a combination of partnerships with new and existing departments as well as new separate initiatives. Though some of these departments and initiatives may exist on the University of Arizona (UA) campus, we found that historically marginalized and low socioeconomic students were not taking advantage of global experiential learning opportunities not only on our campus, but nationwide. The issues of cost, time frame, and intentional cultural competency were important factors for students participating in these experiences, so we created innovative programs that address these issues. The Cultural Centers’ relationship with students provides an advantage in that students are more accessible to our outreach and interventions, which are structured to build community and enhance academic, professional and leadership development.

Project Goals and Objectives
The Global Experiential Learning Project seeks to accomplish the following:
•Cultivate student exploration of parallel identity issues within the host community including, but not limited to the Asian, Indigenous, African, Latinx, and immigrant experiences.
•The program will engage historically marginalized students that do not traditionally experience these opportunities. 
•Develop sustaining relationships and create communities for retention on campus.
•Students will engage in cultural competency activities. 
•Students will engage in transformative education through self-reflection, intercultural engagement, dialogue and service, that will give them deeper insight and understanding of their own cultural-environmental perspectives informed by US-colonial relations. 
•Students will have intercultural engagement through service learning projects that address environmental justice, equitable education, language revitalization efforts, and gender equity in social movements within local communities.
•Challenge students to consider their own ethnocentric perspectives as they learn from and experience global perspectives on issues that can inform the southwest borderlands.

Program Components include:

•	Intergroup Dialogue
•	Cultural Exploration/Exchange
•	Community Building
•	Community Service/Service Learning
•	UA Online
•	Self-Authorship
•	Closing the loop: Coming Back to Campus

Vivir Mexico is a study abroad program includes interactive classroom, experiential, and online instruction, of which four weeks are in-country cultural immersion and service. The course concentrates on the history, culture, politics, and economic systems of Mexico with special emphasis on diverse sociocultural and linguistic experiences. The Vivir Mexico Study Abroad Program will be going into the fifth year with increasing numbers from 8 students the first year to 22 students. The existing partnerships that we have include the UA College of Education, UA Office of Study Abroad, UA Mexican American Studies, US Embassy, Mexican Senate, El Centro de Investigaciones y Estudios Superiores en Antropología Social (CIESAS), Resplandor International, and COJUFE. Our hopes are to continue a program like this in Southeast Asia through the connection with the UA micro campuses.  

The UA Cultural Centers’ Alternative Spring Break Program provides students from the University of Arizona (UA) and other institutional partners with an intimate cross-cultural immersion experience with Indigenous communities in the United States. Participants explore Native history, culture, and the struggle for sovereignty through service-learning, community/cultural engagement, dialogue, and reflection. The first two years were in Tuba City, Arizona where students explored the Navajo and Hopi cultures and the third year will be on the island of Oahu, Hawaii. We will be going into the fourth year with an increasing number of students from 4 students the first year to 12 students participating this year. Existing and new partnerships include ASUA, Navajo Nation, Hopi Nation, University of Hawaii, University of Southern California, Leeward Community College, and Pacific Island Institute.

The Gap Experience is the newest component of the GEL Program. The Gap Experience is a condensed experiential learning for both incoming and currently enrolled students in order to deepen practical, professional, and personal awareness. We would be working with the UA Enrollment Management, UA Online, SALT Center, and UA Downtown to create this new initiative that will span local, domestic, and international experiences.',
'created_at' => '2017-02-02 01:11:57.000000',
'question_id' => 364,
),
318 => 
array (
'answer_text' => 'The goal first and foremost would be donating produce grown to the Campus Pantry.  Any yields above and beyond what is needed by the Pantry would be used for cooking workshops, sampling, R&D work on recipes, and then into restaurants.  Any produce used by the Union in restaurants would be highlighted as crops from the garden grown by students for students.  It is still to be determined what crops will be grown yet as students working with mentors regarding best crops to grow based on the infrastructure.  As Registered Dietitian for the Union, I will work as a mentor to ensure a variety of nutrient dense crops would be grown that could work well for the Campus Pantry and for educational purposes.  If there are crops that do well with the infrastructure and climate but cannot be used by the Pantry for some reason (such as difficulty preparing or short life of freshness for example) we would use that in the other ways listed above. The hope, however, is that mentors working with students could offer ideas for produce that could be used for the Pantry in addition to the other ways discussed.',
'created_at' => '2017-02-02 14:57:12.000000',
'question_id' => 366,
),
319 => 
array (
'answer_text' => 'I hear repeatedly from students dining on campus and students working at on campus restaurants who are using the nutrition calculator.  Last April, I traveled to three of the 4 out-of-state orientations for incoming students and their parents.  Of those three orientations, I had 92 incoming students and their parents inquire about nutrition information and/or dietary requests who were then made aware of the nutrition calculator.  These students were very excited about this option and parents were relieved.  During the fall semester, I had 187 students, parents and staff inquire again with the same result of their excitement with this resource.  Fall semester, my nutrition students and I were able to set up "tablings" to get the word out about the nutrition calculator at over 15 events including Bookstore events, Meet me at the Rec, Rec on the Mall, Taste of the Union, Party at Park, Dine with the Dietitian, and graduate students welcome reception with over 500 nutrition calculator website bookmarks and 250 nutrition calculator step-by-step instruction flyers.  I have contacted the vendor we use to see if he has metrics on the number of page views.  He has just turned this on so we will have metrics of page views moving forward.  I also plan to set up several tabling events throughout the Student Union and other dining locations in March to highlight the nutrition calculator as March is National Nutrition Month.  We also will be able to highlight the calculator in the nutrition workshop curriculum as we build it out.  Finally, I will continue to have visibility at out-of-state and on-campus orientations.  I believe as the nutrition calculator continues to roll out and we gain grass roots word of mouth with incoming students, the number of users will grow quickly and now that we have the ability to track page views, we will be able to show this as well.',
'created_at' => '2017-02-02 17:27:39.000000',
'question_id' => 365,
),
320 => 
array (
'answer_text' => 'There is an initial “Licensing, Delivery, and Training" and a "Single Sign On installation” fee which together totals $1500. This cost is the base cost to get the software up and running. It is a one-time, star- up fee, and is not included in future costs. Do note, however, that the "training" portion is for UA students and staff to be trained on how to use the site. Galaxy Digital offers this training at any time during our contract to anyone who will be using the site for free as it is included in the contract costs. Typically, we would need to pay additional funds to receive training on how to use software, so this is a valuable added benefit.

The annual cost for the software, including fully functioning Single Sign On, is $4350, compared to $7500 with VolunteerMatch, and did not include Single Sign On. The Single Sign On feature allows students to easily link their university account as opposed to having to create a separate account with Galaxy Digital. It also will assist in our reporting as the Single Sign On logs user activity through the site. 

In total, the price breakdown is below. 
Year One: 
$1000 – one time start up fees (Licensing, Delivery, Training)
$500 – one time install fee (Single Sign On)
$4000 – Annual fee for service
$350 – annual fee for Single Sign On service
YEAR ONE TOTAL: $5850
AFTER YEAR ONE: $4350 (Reduced because of the one-time set up charges)

The Galaxy Digital platforms serves a critical need for students, faculty, and staff to be able to find volunteer opportunities in Tucson through a UA branded, centralized system.  The site will also serve our UA community needs since students, colleges, and departments will be able to post event opportunities open only to UA students. Hosting a volunteer matching system is a valuable piece of the volunteer program at the UA. We are excited to be able to offer students a more fiscally sound alternative to the VolunteerMarch site we have used in past years.',
'created_at' => '2017-02-02 20:59:43.000000',
'question_id' => 367,
),
321 => 
array (
'answer_text' => 'The PASS punctuated program is going to be designed to serve the 10-15% of students who are required to participate in PASS but never register.  The intention is to continue to do outreach to those students, along with their academic advisors throughout the first 5-6 weeks of the semester.  We hope that continued outreach would encourage students to come to the conclusion that they should participate in PASS.  We will grant them access to the online modules and they will have several follow up meetings with full-time staff to discuss the information. Though they will not have the peer-to-peer component, we hope they will still receive enough support to persist into the next semester, with an increase in their GPA and academic skills.',
'created_at' => '2017-02-09 17:43:19.000000',
'question_id' => 374,
),
322 => 
array (
'answer_text' => 'Students who actively participate in the PASS program are retained at a significantly higher rate than other students on their first semester of academic probation.  The most significant differences in retention are for the students who should have signed up for PASS but didn’t. In Spring 2016, there were 140 students who should have completed PASS, and only 39% of them returned to the UA for Fall 2016. During the same time period, 70% of the active PASS participants returned to the University of Arizona.  In Fall 2016, there were 55 students who should have participated in PASS but didn’t, and only 38 of those students returned for Spring 2017, a retention rate of 69%. Comparatively, 83% of active PASS participants are enrolled for Spring 2017.  Students who actively engage and use the PASS program show increases in their GPA and are more likely to persist at the University of Arizona.  Programs like PASS, which provide necessary academic support to students, are vital to the institutional challenge of increasing retention and graduation rates.',
'created_at' => '2017-02-09 17:44:43.000000',
'question_id' => 373,
),
323 => 
array (
'answer_text' => 'Peer advisors complete a four-day training on a variety of topics related to university resources, policies and working with students.  During the 25-hour training at the beginning of each semester, they cover: expectations and professional standards, UA policies related to GRO, add/drop, university withdrawal, and mandatory reporting on sexual assault and hazing.  Peer advisors also receive training on resources including: managing student needs, the advising tool and grade calculation, identity development, diversity and inclusiveness, QPR training which is in regard to helping identify when to refer a student for mental health issues, university resources, working with difficult students, and the content of all of the PASS workshops.  In addition, there are a number of trainings designed to be more developmental for the peer advisors regarding building rapport, and conducting speeches and workshops.

Many of the trainings and experiences peer advisors have are transferrable to other career paths.  Peer advisors learn how to engage participants, when they may not be enthusiastic about attending the program, they are required to behave in a professional manner at all times, and they learn time and people management skills by working with 15-20 students at a time. 

From Fall 2015 to Fall 2016 (3 semesters), PASS employed 30 students as peer advisors.  Of the 30 students, 21 worked at PASS for more than one semester.  Additionally, 12 of the 30 still work at PASS, 14 have graduated and 4 left for other opportunities at the UA.  The lead peer advisor has been with PASS for more than 1.5 years and will be continuing through the 2017-2018 year.  The PASS program has excellent retention of student employees, and conducts surveys to gain feedback about their work environment to continue to improve.',
'created_at' => '2017-02-09 17:46:36.000000',
'question_id' => 372,
),
324 => 
array (
'answer_text' => 'Peer advising is a way to help students synthesize the information that they are learning through the workshop.  In 2015-2016, 55% of students indicated that meeting with a peer advisor was the most helpful portion of the PASS program, only 20% of indicated online 
workshops were the most helpful and the remaining 25% of students indicated in-person workshops, handouts and other items as having the most impact on them.  The peer advising component and support system that is created through that relationship is very impactful on students.',
'created_at' => '2017-02-09 17:47:27.000000',
'question_id' => 371,
),
325 => 
array (
'answer_text' => 'Thank you for your question.

The Wildcat Career Network runs on an alumni mentoring platform from software company Firsthand. It does not integrate with Wildcat JobLink, UA Career Services\' job board. There are several reasons for this, but it’s not because the Alumni Association is supporting the platform.

First, UA Career Services will be sunsetting Wildcat JobLink at the end of FY1617 and moving to a new job board software that is more user-friendly for students and provides better management and reporting tools for staff. Thus, there are no plans to integrate the tools, but we will explore options for using the new job board software to promote the Wildcat Career Network.

Second, Career Services attempted to use Wildcat JobLink to connect students to alumni mentors in the past, but the service was not well adopted by either students or alumni. A primary issue was a highly cumbersome process for alumni to sign up and fill out a lengthy profile through an antiquated user interface, and a limited amount of control over their contact information.

The student experience with an alumni career network is highly dependent on the amount and diversity of alumni involved, so user-friendliness was a critical factor in choosing a new alumni career advice platform.

The following are some key user experience features offered by the Wildcat Career Network:

- Student and alumni contact information is kept completely private unless parties choose to share that information with each other. When students reach out to alumni for career consultations, their messages are sent through the platform; alumni are contacted through their preferred email or phone number, but the platform sends the message. When students and alumni meet, they do so over a private conference call line, so no one has to exchange contact information with someone they haven’t met. If students and alumni choose to exchange contact information at that point, they can do so.

- Alumni can sign up as career advisers using LinkedIn, and the platform will import their career information to create their profile. This feature is crucial; it means alumni can sign up almost instantly, making promoting the platform to busy, successful, working professionals much easier. They can also register via Facebook, or simply and email address and password. Alumni career advisers who sign up are verified and sent a personal thank you message from the director of marketing and alumni relations at Career Services on at least weekly basis.

- Alumni can also specify how frequently they are willing to be contacted per month, and they can also block out entire busy time periods. This means that busy alumni professionals in career fields or companies that are popular with students don’t have to worry about being contacted more frequently than they can handle. 

- Students and alumni can choose to receive reminders for their confirmation via text message. They can also download calendar event files for their scheduled consultations.

- When students reach out to alumni through the Wildcat Career Network, the platform will automatically identify other alumni with similar profiles allow students to quickly and easily customize a message to schedule a consultation with those alumni as well.

These types of user experience features are not available through Wildcat JobLink or other software whose primary function is a job board. 

Career Services and the Alumni Association have a very strong working relationship, including sharing two staff lines, partnering to provide career tools and events to students and alumni, and coordinating consistent messaging to students, new graduates, and alumni around career information, resources, and services. These two units working together allows the UA to effectively leverage the UA alumni population to increase access to specific career advice and opportunities students while creating a powerful alumni network on which all UA students and alumni can rely.',
'created_at' => '2017-02-15 20:53:22.000000',
'question_id' => 376,
),
326 => 
array (
'answer_text' => 'The current breakdown of the requested budget would have the student Director and Assistant Director paid hourly wages. We budgeted for the Director to work 20 hours per week and the Assistant Director to work 15 hours a week at minimum wage. The budget also includes stipends of $500 for the 4 committee chairs each semester.

We believe that hourly wages for the two leadership positions will help compensate these students appropriately for the work that these positions will demand. Compensation will also help us recruit qualified and competitive candidates for these vital positions. 

Without Student Services Fee support, ASUA would at most only be able to afford annual stipends for the Director and Assistant Director. The Director would receive $1500 and the Assistant Director would receive $1200. Committee chairs would have to work on a volunteer basis without compensation.',
'created_at' => '2017-02-17 00:20:29.000000',
'question_id' => 375,
),
327 => 
array (
'answer_text' => 'The app maintenance area of our budget includes every component that keeps the app up and running. I\'ll give a breakdown below:

Agency Subscription Fees: 12 units(each vehicle) at $275/month - $3300 a month, totaling $39,600 pre-tax for the year.

Internet Data for the iPads which run the app: $40/month for 12 units - $480 a month, totaling $5760 for the year.

After those two primary line items we are left with about $8400, which would be used to account for various expenses including costs that arrise in the expansion of our program. For instance, if we acquire a new vehicle and plan to fully staff our cars, that would mean purchasing a new iPad($400), adding that iPad to our agency subscription($3300), and ensuring that it has access to the internet($480). 

Another area that I know this budget will work into is the purchase and installation of proper iPad mounts. This was something we looked into this year, however we found it an expense the budget would not permit. Having permanent holders installed into our vehicles will lead to safe, more reliable service from our drivers. Our early estimates put this somewhere in the $2000-$3000 range for good quality mounts with installation. 

Lastly, there are a multitude of small costs that pile up quickly. As an example, our iPad cases, wall chargers, and car chargers all receive quite a bit of mileage and there is a good chance that they will break in time. Replacement for a single item is insignificant but all of them in conjunction add up quickly. 

I hope this helps clarify our justification behind this area of our budget. If you have further questions regarding the app budget I am happy to help answer them.',
'created_at' => '2017-02-20 19:48:51.000000',
'question_id' => 378,
),
328 => 
array (
'answer_text' => 'To give you some background of our current system, our app provider(TransLoc) tracks all of our ride statistics for us. They give us detailed information on the amount of rides we give per night, how many passengers did not show up, how many cancelled, the number of passengers we transported, our average wait time, mileage on our vehicles, and many more categories. However, they do not explicitly tell us how many unique passengers have used our service. That being said, using the data given to us, I took all of the phone numbers that were used for each ride, and made a separate list of only unique phone numbers.

Of the over 19,000 ride requests we had in the Fall 2016 semester, about 2500 of them had a unique phone number attached. This does not necessarily tell us how many unique passengers took SafeRide, as a ride request can be up to six passengers, but we only require one phone number. With that in mind, it still seems like it would be in the ballpark, and shows us that an average user requests 7.6 rides per semester. This shows a high retention rate among the users that do use our service. Another interesting statistic to glean from this is that the average number of riders per request is 1.43, which could contribute to the amount of unique riders we transport through additional riders on a single call.

One idea to note along this line of thought is that SafeRide is working with Risk Management Services to acquire card readers. These readers would plug directly into our iPads and we could use them to scan our riders\' Cat Cards, and store that information in a spreadsheet where we could perform more detailed analytics easily.',
'created_at' => '2017-02-20 20:51:50.000000',
'question_id' => 377,
),
329 => 
array (
'answer_text' => 'The funds ($9,000) in line 47 will be utilized for coupons to the Scoop and other venues in the Union for the 2018-2019 Academic Year. Each fall the program purchases coupons to the Scoop for Faculty Fellows to take students for a free ice cream or coffee where the Faculty can connect with students from their site as they transition to the University of Arizona. I would like to extend this to the Spring semester as well and to more venues than the Scoop. Often times it is helpful to have a meal to connect over where Faculty can take a small group of students. Food is a tool to bring faculty and students together for meaningful connection while also enforcing that students feel as though a faculty member cares about them and that they belong at the University of Arizona.',
'created_at' => '2018-01-22 15:48:18.000000',
'question_id' => 381,
),
330 => 
array (
'answer_text' => 'The Student Faculty Interaction program is excited to build its marketing campaign to reach more faculty and instructors. One of our priorities is to spread the word to faculty and instructors who teach in the online setting. Our plan is to work with the UA Online team and Vin Del Casino, Vice President, Academic Initiatives and Student Success to create a Request for Proposals to the entire list of instructors who teach online courses to let them know about the SFI grant program at the beginning of each semester moving forward. We will set aside a significant amount of funds to online courses so that our online community can also benefit from the program. Additionally, we are working with current SFI recipients to spread the word to their colleagues from across campus to let them know about this resource and how they can utilize these funds to bolster student-faculty interaction in the classroom both online and face to face.',
'created_at' => '2018-01-22 19:32:25.000000',
'question_id' => 388,
),
331 => 
array (
'answer_text' => 'The leftovers refrigerator, though located in a public area (lower level of SUMC, in or outside of the Games Room), will be secured with a keypad to ensure that the food inside is saved for students in need. The students who use the Campus Pantry will be given the opportunity to “opt-in” to the Boxing Up Hunger initiative, and we also plan to launch a broader awareness campaign in an effort to raise awareness of this service among students in need who are not using the Campus Pantry. Once students “opt-in” and provide their cell phone number and/or email address, they will be contacted via text or email message alert when meals are placed in the refrigerator, and the message will contain the keypad access code for students to use to access the meals in the refrigerator. Keypad access codes will be reset after a certain amount of time (likely 48 hours) to ensure we follow health codes for refrigerated foods and maintain security of the refrigerator’s contents.',
'created_at' => '2018-01-24 17:56:18.000000',
'question_id' => 379,
),
332 => 
array (
'answer_text' => 'The $10,000 for staffing covers two student employees working 12-15 hours per week, 16 weeks per semester, to complete the following tasks in support of the Boxing Up Hunger initiative: 1) Assist in safely packaging leftover food under the supervision of Student Unions culinary and catering teams (5-7 hours/week); 2) Labeling boxed food with package/placement date and time and reheating instructions, placing boxes in the refrigerator, and rotating existing boxes of food. Maintaining food safety logs tracking food rotation through refrigerator (4-6 hours/week); and 3) Sending out text message/email alerts to notify students of food availability and provide access codes (3 hours/week). In addition, each student will complete a one-time 2-day ServSafe class for certification in proper safe food handling requirements. As mentioned in our proposal, this first year pilot phase will be one of learning and adjusting, so the labor hours and amount of time allocated to accomplish the tasks above are our estimates based on the initiative’s currently planned process and needs.',
'created_at' => '2018-01-24 20:49:08.000000',
'question_id' => 380,
),
333 => 
array (
'answer_text' => 'The rooftop garden (greenhouse) is planned to grow eggplant (single row), cherry tomato (double row), vine tomato (double row), specialty tomato (double row), and peppers (single row). The Nugent garden is planned to grow broccoli, carrots, cucumbers, onions, peppers, cabbage, potatoes, cauliflower, radishes, spinach, squash, beans, tomatoes, and pomegranates and will also include citrus trees.',
'created_at' => '2018-01-24 22:14:13.000000',
'question_id' => 382,
),
334 => 
array (
'answer_text' => 'The Nugent garden will consist of 9 exposed aggregate concrete raised garden beds with paths between them. Separate raised beds will reduce the possibility of disease spreading throughout the entire garden in the event of any contamination. This layout will also provide plenty of walking space, enabling visitors to wander freely throughout the garden as they pass through, creating a welcoming and educational space as signs and placards with garden information will be located throughout the area. The awning currently covering the site will be removed to provide enough direct sunlight for the crops, but the garden design incorporates posts with hooks on the corners of the raised beds to provide convenient tie-off points for removable shade/frost cloths. Construction blueprints and design visuals were sent directly to Teresa Whetzel and Andrew Armour. These plans will be modified to accommodate the presence of citrus trees as requested by the partner RTG student team.',
'created_at' => '2018-01-24 22:25:51.000000',
'question_id' => 384,
),
335 => 
array (
'answer_text' => '90% of produce grown from the rooftop and Nugent gardens will go directly to the Campus Pantry for students in need. The remaining 10% of the produce from these gardens will be used by the Student Unions as storytelling showcase pieces for the community. For example, a basket of tomatoes harvested from the rooftop garden could be used for a display during ABOR lunches with a sign identifying them as “Grown by students for students on the Rooftop Garden”. The intent of the produce from these gardens is not to generate profits, but to support the Campus Pantry and help address food insecurity on campus. Produce from the other greenhouses at CEAC may be sold to the Student Unions for use in on campus restaurants, and the profits from those sales would go back into the HydroCats program to help ensure its long-term sustainability as mentioned in the HydroCats SSF Proposal.',
'created_at' => '2018-01-24 23:19:20.000000',
'question_id' => 383,
),
336 => 
array (
'answer_text' => 'A second GA for VETS will work closely with the Assistant Dean and current GA to help build a robust and vibrant resource center.  The major responsibility of this new position is the coordination of new programs to capture our population before they have decided on UA or arrived on campus.  This GA will focus on programs and services dedicated to outreach which involves networking with the local community at Davis-Monthan Air Force Base, Pima Community College, Cochise College, Southern Arizona VA Health Care System, and more, to connect with students before they step foot on campus.  This GA will coordinate outreach efforts to connect students with our academic support services such as tutoring, SERV (Supportive Education for Returning Veterans) courses, on-campus job opportunities to include work-study, and veteran scholarship and financial aid opportunities through the VETS Military Connected Student Center, including outreach to new students at Orientation, Wildcat Welcome, Graduate Student Orientation, and other UA-wide student events. Another outreach and orientation related task will be to reach out to students that are newly admitted, connecting them to resources available to them through VETS, but also UA as a whole.  This also involves working closely with the Transfer Student Center and assisting with evaluating of JSTs (Joint Service Transcripts or transcripts awarded for military service and job).  Evaluating JSTs will ensure service members and veterans are receiving proper credit for all military education, training, and occupation experiences received during time serving.  This GA will help support an inclusive environment that fosters a sense of belonging for military-connected students from diverse backgrounds, and provides an opportunity for VETS to impact the larger University community’s educational experiences.',
'created_at' => '2018-01-25 18:15:30.000000',
'question_id' => 387,
),
337 => 
array (
'answer_text' => 'The $75,000 per year is based on past budget years ($75,000 – 2016, $76,500 – 2017, and $78,000 – 2018).  Nearly 40% of this budget is used for business meeting expenses involved in career preparation opportunities such as bringing employers and special events to our population.  For example, in the last year we brought Raytheon, Amazon, Intuit, TEKsystems, USAA, and many more to UA VETS.  Moving forward, our goal is to have a career fair expansion upwards of 20 employers trying to reach our student veteran population.  Student Engagement & Career Development is supporting VETS by connecting employers directly with us in order to help facilitate our goal.  Another large business expense is hosting the Warrior Scholar Project during the summer – now on our 3rd year, this is an academic support service and “boot camp” in humanities and STEM for student veterans just beginning their journey into higher education.  Other large expenses that allow us function daily are in communications, phone lines, and computers.  Our 31 PCs and 2 laptops are maintained by SASG and UITS, to include hardware maintenance/upgrades and software licensing for special programs (Photoshop, AutoCAD, etc). Since the VETS Center opened its doors in 2009, we’ve provided free printing and thus require large amount office supplies, Xerox rental, and maintenance costs.  Other large expenditures include catering, reserving room space, parking, advertising, and marketing for VETS.  It is our hope SSFAB will once again agree to fund this grant proposal for three years in our joint effort to maintain our two VETS Centers so we can continue to provide the proactive, cutting edge, and excellent services and resources for our veteran and military-connected student population at UA.',
'created_at' => '2018-01-25 18:15:39.000000',
'question_id' => 387,
),
338 => 
array (
'answer_text' => 'Internal funding from the Student Unions to support HydroCats is not possible since the Unions is already funding the remaining capital expenses for the rooftop garden greenhouse building and infrastructure. We are (and have been) actively seeking grant funding and sponsorships to offset the costs already incurred, but there is still a deficit based on what has been spent, even with the $27,700 of funding from last year’s SSF and Green Fund. As a result, the Unions needs additional SSF funding for HydroCats in order to cover the cost to operate and manage the rooftop garden greenhouse. We have no other way to accomplish this without your continued support and funding.',
'created_at' => '2018-01-30 16:27:12.000000',
'question_id' => 392,
),
339 => 
array (
'answer_text' => 'The Campus Pantry would receive approximately 400lbs of produce (cherry tomatoes, vine tomatoes, bell peppers, eggplants, mini cucumbers, long cucumbers) and 270 heads of lettuce per month. No revenue would be earned on this produce as the primary intent of the rooftop garden project was always to grow produce for the Campus Pantry to support students and staff who experience food insecurity on campus. However, UA Dining Services would be able to purchase approximately 1000lbs of produce and 1500 heads of lettuce each month from the other two greenhouses at CEAC, which has the potential to generate $3,300 in revenue per month. This amount assumes that the price per pound of produce is $2.50/lb or less, depending on the item. All revenue generated from produce sales to the UA Dining Services would go back to HydroCats to support and fund the program long-term.',
'created_at' => '2018-01-30 16:29:52.000000',
'question_id' => 393,
),
340 => 
array (
'answer_text' => 'No other sources of funding have been secured for HydroCats, but we are submitting a Green Fund Proposal for this initiative as well. In addition, we just received a commitment letter for donations of the growing media needed in the greenhouses, which values at $2,500 and will help offset some of the greenhouse operating costs, and we will be speaking with different companies to try to secure donated or reduced price lettuce NFT systems.',
'created_at' => '2018-01-30 16:30:58.000000',
'question_id' => 394,
),
341 => 
array (
'answer_text' => 'Students who have internships or independent studies would earn course credit(s) for their work in the greenhouses, which is why they cannot also be paid (per University policy). This is why the HydroCats proposal outlines the 14 student internships or independent studies as separate from the 1 paid greenhouse manager student position.',
'created_at' => '2018-01-30 16:32:00.000000',
'question_id' => 395,
),
342 => 
array (
'answer_text' => 'The PlantEd Culinary Workshops are currently limited to 50 attendees (25 in each session at 12noon and 5pm). However, if granted SSF funds to make this class available to 125 students who experience food insecurity, we would accommodate the additional attendees in the following ways: 1) offer more sessions each month; 2) open the back (west) wall of the studio with the installation of an air wall to accommodate more people in part of the Sonora Room for overflow seating while also activating the TV screens around the Sonora Room for better visibility for those seated further away from the kitchen area; or 3) expand the size of the culinary studio. Option 1 is an easy short-term solution to accommodate more attendees, but the Unions is committed to pursuing option 2 or 3 in the future if necessary.',
'created_at' => '2018-01-30 16:47:59.000000',
'question_id' => 396,
),
343 => 
array (
'answer_text' => 'In partnership with the Campus Pantry, students who experience food insecurity could receive up to eight vouchers to attend each of the PlantEd Culinary Workshops for free. Students would be given a voucher for the PlantEd Culinary Workshop one week prior to the scheduled workshop date during a Campus Pantry food distribution. The Campus Pantry team would track the students receiving workshop vouchers, and the Student Unions team would track the students attending the workshops with the vouchers to ensure students are not only receiving but also using the vouchers. Both teams would work together to verify that students are only receiving/using a maximum of eight vouchers each.',
'created_at' => '2018-01-30 16:52:27.000000',
'question_id' => 397,
),
344 => 
array (
'answer_text' => 'SPEAKOut was created to fill a void in the University of Arizona’s marketing that left the Cultural and Resource Centers out of its purview and resulted in a huge gap in representation in University marketing materials of our student populations across the Centers: African American Student Affairs, Asian Pacific American Student Affairs, Guerrero Student Center, LGBTQ Student Affairs, Native American Student Affairs and the Women’s Resource Center. Research tells us that if students cannot see themselves represented in marketing, they do not believe the University is for them. From recruitment to retention, marketing is a highly valued tool in achieving student success. Since the Cultural and Resource Centers do not have the power of the University’s marketing budget, we had to be creative in creating SPEAKOut. In the past three years, we have had to test out different structures and methods of working with undergraduate and graduate students who are with us for a limited time and come with varying degrees of knowledge and skills. These are all factors that impact the thought behind the marketing strategy of SPEAKOut. The strength of this structure is that the SPEAKOut student interns are part of the Centers and know best what current trends they and their peers are relating to. 

Strategy one is that SPEAKOut interns are working across multiple platforms and mediums such as social media as well as print. When we think of marketing and design, we often think only of print flyers for events but SPEAKOut is focusing their energy on looking at social media analytics and researching how students both in and outside of the Centers hear about our events. From facebook to instagram and snapchat, SPEAKOut is working towards learning more about students’ use of social media to engage in passive programming and event attendance. Recently, SPEAKOut developed a sign in sheet for Center events with social media options as answers to “How did you hear about this event?” The responses showed that nearly 100% of students found out about the event via facebook. This is important information to seek and mold a next step of strategy around. 

In regards to print design, building the skill set around multiple design tools is still a necessary and needed skill that translates to multiple platforms. SPEAKOut students have generated over 120 design pieces just in the Fall 17 semester compared to 80 design pieces in the first year of the project. SPEAKOut students work collaboratively to teach and support each other in using Indesign, Illustrator, Photoshop and other tools. 

Strategy two is building a brand and identity for the Cultural and Resource Centers. SPEAKOut students have created and edited logos for almost every Cultural and Resource Center that reflects the true essence of the Centers. Additionally, SPEAKOut students are working on creating logos for many of the student run clubs and organizations operating within the Cultural and Resource Centers. Building a brand and identity is crucial so Centers stand out in the slew of information that is given to students coming to the University and to students who are already at the University. We found that many students were coming to the Centers in their junior or senior year because they didn’t know we existed. Working on building out the brand and deepening the Centers identity results in carving out a more visible space on campus that allows students to engage in our work to support student success. SPEAKOut students are thinking about brand work as visual communication for our student audiences, this work is not just about making great looking design. This helps students who are in need of resources from financial aid to counseling, to find the Centers and to know the Centers provide this support to students in a welcoming space. SPEAKOut students are designing logos, signage and artwork for the Centers, and putting great intention into design for events that are advertised to the greater UA campus that encourage all to participate in the Cultural and Resource Centers. 


Strategy three is representation. Diverse students deserve to be represented in marketing and design materials because the story of the UA includes them. It is important to tell our stories and to be seen around campus, in online media and everywhere that UA students are making a difference. SPEAKOut students have been building on their photography skills by learning from guest artists and supporting each other. Our Graduate Assistant is pursuing his MFA in photography and is a great resource to the student interns in practicing shooting and editing photography. SPEAKOut has been able to photograph and document important Cultural and Resource Center events like Convocation, Welcome Week activities and Heritage Month Events. This photographic representation is critical for institutional memory, it provides information on what has been done, who was part of it and also celebration and inspiration of all those who have come before and persevered. Secondly, it creates a photo library of diverse students from which all the Centers can pull from in creating marketing and design. Prior to SPEAKOut, Centers did not have the capacity to photograph our events in a professional way and the photo library of the University only included small numbers of students of color, limiting Centers access to photographs for marketing and design work. 

The marketing strategy which includes these three components has allowed the Centers to already see an increase in event attendance and an increase in cross collaborative work with campus partners. SPEAKOut students are part of the Centers and find it important and fun to learn about what their peers like to see and then implementing within the marketing and design work. SPEAKOut’s work is relatable, concise, eye catching. We look forward to continuing SPEAKOut’s contributions to success in the retention and recruitment of our student populations.',
'created_at' => '2018-01-30 20:00:13.000000',
'question_id' => 391,
),
345 => 
array (
'answer_text' => 'In the first year of the SPEAKOut project, 80 pieces of design were created by student interns. In just the Fall 2017 semester, over 120 pieces of design were created by the student interns. One reason this is important to note is that pure volume of work is contributing to more students attending events at the Cultural and Resource Centers. Center Directors have seen an increase in student attendance at events across Centers. SPEAKOut designed sign in sheets show that students who attend found out about the events from social media marketing. As stated in our marketing strategy, we are working towards analyzing social media statistics to inform our marketing and design. When there is an increase in student attendance to Center events and programs, then we see the benefits of the Centers’ work to the students. The capacity of doing marketing and design work that SPEAKOut has offered the Centers helps bring in students to access the resources and support the Centers offer. Research from the Centers show retention of marginalized students is rooted in finding a sense of belonging. Marketing and design is one piece of that work connecting and engaging students to come to the Centers and find their home. Additionally, once students first participate in Center events, they are exposed to Center related activities and opportunities and get connected via social media to stay in touch. Marketing and design in the Centers opens students to more opportunities that might get lost outside of the Centers with the rest of marketing that exists everywhere. Centers experience students who attend a free food event for example, and then go on to stay connected and get involved as student leaders in deep and meaningful ways. At the Women’s Resource Center for example, students who attended events in their sophomore year went on to continue and grow their leadership in different areas of WRC programming for 2 more years and counting. Marketing and design in the Centers play an important role in disseminating that information, in creating lines of communication that engage students regularly and because SPEAKOut students always work directly with their peers on creating marketing and design--students feel a part of Center work and continue to participate.  

Another way students are impacted by SPEAKOut’s work is through student led clubs and organizations. Our student leaders are working hard to carve out space for inclusion, equity and speaking up about issues impacting marginalized students at the UA. A majority of the students are not graphic design students but have had to spend a lot of time working on creating marketing and design for their events and activities. SPEAKOut is now able to support these students in creating professionally designed marketing which hits their goals better and frees student leaders to focus on the content of their important work. Lastly, because SPEAKOut is a collaboratively functioning structure with student interns from each Center meeting weekly to support and co-design marketing, SPEAKOut students have had a critical role in bringing the Centers together for more collaborative programming and created communication that has been supportive in avoiding conflicting events across the Centers and promoting intersectionality as a value across campus. 

All the research on marketing and design and its impact on consumers/clients is true for SPEAKOut’s work as well. Marketing and design is critical in telling the story of the Cultural and Resource Centers, in getting students who need support connected with resources that result in graduation and a healthy well-being, in engaging students to leadership development and opportunities, in creating a sense of belonging at a home away from home.',
'created_at' => '2018-01-30 20:02:03.000000',
'question_id' => 390,
),
346 => 
array (
'answer_text' => 'The art and culture portion of SPEAKOut was designed to support continued learning about how graphic design, marketing and art can be used for addressing inequity and empowering students to develop the artistry needed for design work. 

In the first year, SPEAKOut students hosted local photographers who led a session on how to create and run a photobooth for Cultural and Resource Center events. These events and sessions are open to the greater campus for all to participate. While students learned technical skills of photography and printing photos for photobooth events, SPEAKOut was also able to dialogue through the ways props and sets are used in photo booths. Students experienced many photo booth operators who bring culturally offensive props for students to use like Native headdresses or Mexican sombreros. SPEAKOut is now equipped to offer photobooths for Center events and understand how even photo booths can perpetuate racism if not intentionally operated. 

In the 2nd year, SPEAKOut brought guest speaker Ryan Redcorn to campus. The event was open to all students and staff and was well attended. Ryan is a young indigenous graphic designer who started his own marketing business after repeatedly not being hired and after seeing no other indigenous graphic designers doing design work for Native American agencies. He was able to speak on the inequity in the graphic design field to designers of color and gave students a foundational understanding of how to think about incorporating culture into design respectfully.  Ryan is also a member of the all Native sketch comedy group, the 1491’s, and was able to show how multiple forms of artistry can influence social issues related to genocide, racism, historical trauma, resilience, and much more. 

This year we are using the funds to support SPEAKOut’s work in designing placards for each Center that reflects knowledge that the UA sits on Tohono O’odham native lands. Our stories are important, including our histories and how they impact us in present day. We will be working with multiple Tohono O’odham cultural experts and TO visual artists to co-design the placards. This long overdue project is important for our students who have limited knowledge about local tribes and also give them the opportunity to engage in collaborative design work with community members that will support their future endeavors. There will be a public event to showcase the work which will be open to all of campus. Funds are used for honorariums, printing, and costs associated with the public event. 

These events have supported SPEAKOut students and all students who have attended to think consciously about how design impacts communities with great care to marginalized populations. Students have learned career building tips, survival skills as a functioning artist, and how to navigate one’s own identity in marketing and design. Our students experience inequity in the graphic design field on multiple levels and this work in Art and Culture is an affirming space for them to learn how to use their best tools to address it.',
'created_at' => '2018-01-30 20:04:59.000000',
'question_id' => 389,
),
347 => 
array (
'answer_text' => 'Yes. We have full control of the app from the number of points awarded per sticker scan and the number of points needed to earn a reward (free lunch, gift card, etc.) to the frequency by which users can scan a sticker in a given timeframe, and we can adjust all of these settings as needed. Control of these settings allows us to reduce the likelihood for a user to abuse the system, even though we can’t guarantee that no abuse will occur. For example, the app is currently set to award 10 points per scan and only allows one scan per user every thirty minutes. In addition, a reward is only earned after reaching 1,000 points, so a user could certainly scan his/her sticker every thirty minutes regardless of a qualifying drink purchase in their reusable cup/bottle, but it would take 50 hours to reach 1,000 points.',
'created_at' => '2018-01-31 16:06:43.000000',
'question_id' => 398,
),
348 => 
array (
'answer_text' => 'Yes, each sticker is custom made for our school and costs $0.95 per sticker as set by the company (Cupanion). We rounded the cost per sticker up to $1 to account for tax, shipping fees, and the extra material designed by the Student Unions to accompany the stickers in the students’ Orientation Packets so that they are not just loosely inserted and lost among the other contents of the folder.',
'created_at' => '2018-01-31 16:11:30.000000',
'question_id' => 399,
),
349 => 
array (
'answer_text' => 'Unfortunately, no. OSFA does not have any funding available to support this position.

Thank you.',
'created_at' => '2018-01-31 19:22:16.000000',
'question_id' => 400,
),
350 => 
array (
'answer_text' => 'Yes, at this time, our estimate is that it could take up to three hours a week to send out text messages or emails about food availability because this will all be done manually until a listserv and/or group text messaging system can be set up. Once those systems are set up, they will still have to be managed, though the use of the systems will make the notification process easier and more efficient. In addition, each message about food availability has to be individually tailored with new access codes for participants, and the three hours per week on this task assumes a seven-day week, not just a five-day week, since catering events happen seven days a week. We believe this time/labor allocation will help achieve one of the goals of this initiative, having food available for students as often as possible, via notification for code-access to the leftovers refrigerator.',
'created_at' => '2018-02-06 17:18:54.000000',
'question_id' => 401,
),
351 => 
array (
'answer_text' => 'Hello, thank you for your question.
In 2015-2016 academic year, attendance numbers for programs across the Cultural and Resource Centers was 2364 students. 

SPEAKOut began in 2016-2017 academic year, attendance across the Centers was recorded at 3932 students that year. 

This past year, in Fall 2017 attendance was recorded at 2579 students which puts us on track to more than double attendance numbers post SPEAKOut in comparison to pre-SPEAKOut.

Thank you!',
'created_at' => '2018-02-06 22:14:43.000000',
'question_id' => 405,
),
352 => 
array (
'answer_text' => 'To make HydroCats self-sustaining in the long-term, the plan is to get all three greenhouses (1 on the SUMC rooftop and 2 at CEAC) up and running (by the HydroCats) and to sell 85-90% of the produce grown in the 2 greenhouses at CEAC to the Student Unions. The produce grown in the SUMC rooftop greenhouse and the remaining 10-15% of the produce grown in the 2 greenhouses at CEAC will be donated to the Campus Pantry for free. The revenue generated by the produce sales to the Student Unions (approximately $39,600 annually) would be used to fund HydroCats personnel (faculty lead and student greenhouse manager). Capital costs are a one-time investment, and Dr. Tollefson and team are working to establish partnerships with companies like Grodan to secure donations of consumable greenhouse supplies long term. In addition, and if necessary, we would also apply for Extension Funds or ask CALS to help fund Dr. Tollefson\'s salary going forward, but the goal is for the project to be self-sustaining as a result of produce sales.',
'created_at' => '2018-02-06 22:36:59.000000',
'question_id' => 404,
),
353 => 
array (
'answer_text' => 'Hopefully our process of purchasing vouchers for all fellows at the beginning of the year (approximately 20 for each Fellow) and switching to a case-by-case request based on need   will help negate unused vouchers. In the event that there are unused vouchers, the Coordinator of Faculty Programs would make them part of the Faculty Fellow booth at the Finals Survival Week Kick-Off event to have Fellows hand them out to students as they prepare for finals. It is important for students to have positive interactions with faculty members, knowing that they have support on campus going into such a stressful time.',
'created_at' => '2018-02-08 16:13:12.000000',
'question_id' => 403,
),
354 => 
array (
'answer_text' => 'The Coordinator of Faculty Programs purchases a set number of vouchers at the beginning of the year (approximately 20 per Faculty Fellow). The last several years these vouchers have been a success where Faculty Fellows hand out the vouchers at their sites and offer to walk to the Scoop (or other vendors) to connect with students over coffee or ice cream. The Fellows continue to request these in the semester. After the initial push of vouchers at the beginning of the year, Faculty Fellows are welcome to request more through the Coordinator so funds are spent on a need by case-by-case basis instead of ordering vouchers that are not needed.',
'created_at' => '2018-02-08 16:13:14.000000',
'question_id' => 403,
),
355 => 
array (
'answer_text' => 'As a result of not receiving full funding for FY\'2018, GPSC lowered the maximum award from $1500/event to $1000/event. Accordingly, speaker honoraria were lowered from $1000/event to $500/event. Speaker honoraria are one of the items that applicants frequently request, as almost all POD events invite keynote speakers from outside the University of Arizona. 

GPSC also made changes to the funding process. Before FY\'2018, GPSC funded as many events as possible each year as long as 1) the event was qualified as a POD event, 2) the application was complete, and 3) there was available funding. This was to maximize the impact on students\' professional development. In FY\'2018, the GPSC has adopted a robust rubric for reviewing POD applications and fund applications that receive a total of 65 out of 70 points (93% cut-off). Many events that are worthy of support do not receive funding because the Program is highly competitive. For example, we expect to fund about 12 events in FY\'2018, which is six events less than FY\'2017 (each event can potentially impact at least 50 students.)',
'created_at' => '2018-02-13 20:52:53.000000',
'question_id' => 413,
),
356 => 
array (
'answer_text' => 'In FY\'2017, GPSC received 26 POD applications and approved 18 of the requests. In Fall 2017, the total number of applications was 13, and we approved five initiatives  with a total of $4,345.96 (average ask: $869.19). There are three rounds in Spring 2018: February 15, March 15, and April 15. With the remaining funds ($5,554.04), GPSC expects to fund six to seven initiatives in Spring. In other words, we expect to support about 12 events in the current fiscal year, and that is six events/300 students less than last year as each event can potentially impact at least 50 students.',
'created_at' => '2018-02-13 20:53:16.000000',
'question_id' => 414,
),
357 => 
array (
'answer_text' => 'As a result of not receiving full funding for FY\'2018, GPSC lowered the maximum award from $1500/event to $1000/event. Accordingly, speaker honoraria were lowered from $1000/event to $500/event. Speaker honoraria are one of the items that applicants frequently request, as almost all POD events invite keynote speakers from outside the University of Arizona. 

GPSC also made changes to the funding process. Before FY\'2018, GPSC funded as many events as possible each year as long as 1) the event was qualified as a POD event, 2) the application was complete, and 3) there was available funding. This was to maximize the impact on students\' professional development. In FY\'2018, the GPSC has adopted a robust rubric for reviewing POD applications and fund applications that receive a total of 65 out of 70 points (93% cut-off). Many events that are worthy of support do not receive funding because the Program is highly competitive. For example, we expect to fund about 12 events in FY\'2018, which is six events less than FY\'2017 (each event can potentially impact at least 50 students.)',
'created_at' => '2018-02-13 20:53:50.000000',
'question_id' => 413,
),
358 => 
array (
'answer_text' => 'Hello Panel, Thank you for your question and continued consideration. Here is the job description for the proposed student workers. Funding for this program would also allow us to expand to offering job shadow opportunities in the summer as well as the current winter and spring break.
•	Conduct employer and alumni outreach to potential job shadow hosts by following up with previous hosts and warm leads provided by the Employer & Alumni Engagement team
•	Assist Employer & Alumni Engagement team with developing a host outreach strategy that is responsive to student needs and interests
•	Answer questions from job shadow hosts, potential hosts, and students
•	Contact prospective job shadow hosts to confirm receipt of their application, inform them of next steps, and gather any additional information required to market the opportunity to students
•	Post job shadow opportunities in Handshake
•	Schedule job shadow information sessions and orientations in Handshake
•	Assist with presenting job shadow information sessions and orientations
•	Update and edit all host and student marketing and orientation materials each time the program is offered
•	Assist with creating tailored marketing of specific job shadow opportunities to student groups, specific majors, and other target student populations
•	Send job shadow evaluation survey to students and to hosts
•	Write blog posts and assist with social media marketing related to student job shadow stories
•	Track and report on job shadow program participation and satisfaction data',
'created_at' => '2018-02-15 05:35:28.000000',
'question_id' => 415,
),
359 => 
array (
'answer_text' => 'Total for student retreat: $8000
Venue rental $2500
Transportation vans: $700
Meals: $2000
Supplies: needed to support the activities/Planning committee supplies: $1000
Support for post-retreat student meetings and student mini retreats throughout the year: $2000
*Note: The total amount of expenses is $8200. The additional $200 was paid out of other departmental funds.',
'created_at' => '2018-02-15 19:56:33.000000',
'question_id' => 412,
),
360 => 
array (
'answer_text' => 'We anticipate the projected impact of additional employees will allow us to serve 200-300 more undergraduate students one-on-one per academic year. 

Additionally, there are over 10,000 first-generation college students on campus that we will regularly communicate with electronically throughout the semester.

We also anticipate impacting 75-100 graduate students the first year, and to foster community among 150+ graduate students that are first-gen, low-income, or ethnic minorities over the next three years.

The objective of the ASA PM funding request is to impact graduation rates. With added focus on structured services for students through their senior year we anticipate long-term support to students will manifest itself through increased graduation rates.',
'created_at' => '2018-02-15 19:58:58.000000',
'question_id' => 406,
),
361 => 
array (
'answer_text' => 'The GA\'s were chosen because we believed that we should prioritize funding for student positions over the hiring of full-time staff. These GA positions provide opportunities for the development of professional skill-sets that will increase their competitiveness for professional positions. These positions also afforded graduate students tuition funding.',
'created_at' => '2018-02-15 20:04:53.000000',
'question_id' => 411,
),
362 => 
array (
'answer_text' => 'The ASA PM proposal is combining SSF requests that have been separate in past years. The recommendation of the 2016-2017 SSF Board was that our department combine the ASA Peer Mentoring and the PASS requests. This is what is being done through this proposal. 

Our funding request also includes a proposed expansion of some existing programs in FY2019. Detailed below is what is newly requested in this proposal:

Cultural Learning Communities
- The Cultural Learning Communities (CLCs) are not new to ASA, but they are currently funded through the First Year Student Fee grant, so this would be a new funding request through SSF.
- The CLCs currently consist of five undergraduate communities. With this funding we would increase to seven learning communities – an additional CLC in partnership with the Guerrero Center and a new CLC for students who identify as being multiracial/multicultural. This includes two additional peer mentors and operational expenses.
- The CLCs have received requests to support transfer students, so this would allow us to open up to all first-year UA students.
- We are also proposing the creation of a Cultural Learning Community for graduate students that would only include an operations budget as professional staff and faculty would help facilitate this community.
- Total current funding from First Year Student Fee is $15,400; the total for the expansion of the CLCs is estimated at $14,560. This totals a new request to SSF of $29,960.

First Cats
- First Cats is another program that we propose expanding. This year we were unable to accommodate all of the requests for a peer mentor made by second year students. Additionally, the two peer mentors we do have had very high caseloads. We would like to grow the second year peer mentoring from two peer mentors to five peer mentors – an increase of three peer mentors.
- We are also requesting a graduate assistant to work with third year students and above. This includes providing assistance to the First Cats program coordinator in creating and establishing support for first-generation college students at the graduate/professional level. We currently have a graduate assistant supporting the program, but the funding for this position was only one year and will end at the end of the fiscal year.
- The anticipated additional costs for three peer mentors and a graduate assistant is $49,620.

General Peer Mentoring
- Although ASA will continue to provide services and support to first-year students, we anticipate increasing our scope to include heavy outreach and support for sophomores and juniors in the “murky middle.” This includes students who are between a 2.0 GPA and a 3.0 GPA.
- Research shows this group of students is highly likely to leave the institution prior to graduation, and those close to a 2.0 GPA are in danger of going onto academic probation. 
- This request is for four additional peer mentors to conduct outreach and support for this group for a total estimated request of $21,120.

PASS/Academic Recovery
- In prior years, PASS was submitted under its own proposal. This is not a new program, but it is new to being combined with the overall ASA Peer Mentors request.
- This request includes two graduate assistants. The current graduate assistant for PASS is funded through carry forward funds that run out at the end of this fiscal year. We are also requesting a second graduate assistant. This is a position that was funded in past years, but we were not able to secure funding for this GA position this fiscal year.
- Two graduate assistants is estimated at $57,000.',
'created_at' => '2018-02-15 20:22:27.000000',
'question_id' => 407,
),
363 => 
array (
'answer_text' => 'We currently employ 65 undergraduates as peer mentors. These peer mentors are funded through a combination of two SSF grants (ASA PM and PASS), First Year Student Fee funding, and one-time carry forward funds. The 2017-2018 ASA PM SSF funding provides a budget that funds 28 positions and the 2017-2018 PASS SSF funding provides for 27 positions. A total of 55 out of the 65 positions are currently funded through SSF. We also anticipate hiring nine new/additional peer mentors (undergraduates). This is a total request of 74 peer mentors for the 2018-2019 academic year. 

We currently employ two graduate assistants, but their funding runs out at the end of this academic year and is not part of our currently funded SSF grant. This request would allow us to retain our two graduate assistants and also add a third to support academic recovery.',
'created_at' => '2018-02-15 20:23:58.000000',
'question_id' => 408,
),
364 => 
array (
'answer_text' => 'Distribution of employees FY2019:

Cultural Learning Communities peer mentors – 7 
First Cats peer mentors – 17
General peer mentoring – 9 
Wildcat Academy (FT/PT cohort support) – 7 
PASS/Academic Recovery – 27
Wildcat Connections (electronic outreach) – 7 

First Cats graduate assistant – 1
PASS graduate assistant – 1
Academic recovery graduate assistant – 1',
'created_at' => '2018-02-15 20:25:38.000000',
'question_id' => 409,
),
365 => 
array (
'answer_text' => 'Global Experiential Learning (GEL) GA Job Duties and Responsibilities:

•Coordinate the implementation of the GEL experiences which include, but are not limited to budget, logistics, curriculum, and partnerships. 
•Management of the GEL leads and interns.
•Assist with the coordination, management, and grading of the GEL College of Education class.
•Actively pursue opportunities to partner with other departmental, campus, community, or national organizations to develop collaborations that support professional growth and/or meet GEL goals.
•Assists in program assessment, including data collection and analysis (including progress reports, annual reports, and statistical summaries).
•Assist with grant writing and funding for GEL programming.
•Assist in the marketing, recruitment, and outreach efforts through class visits, presentations, informational meetings, and social media. 
•Assist with the organization and facilitation of return/re-entry programming
•Assist with the research of GEL experiences
•Maintain regular, preferably daily, office hours to meet with students individually and in small groups. Act as a mentor to students and engage interactions that support student growth and development.
•Attend regular staff meetings, events, and trainings
•Work with the Director to complete the 6 month and 12 month intern evaluation
•Support all grant related reporting activities.
•Other duties as assigned.

Common Ground Alliance (CGA) GA Duties and Responsibilities: 

I. Coordinating the implementation and further development of student led social justice programs.
•Coordinate and support student-led events and activities, including budget maintenance, purchasing, room reservations, event documentation, and any other logistics related to programming and events.
•Support and engage students in art and healing, Queer and Transgender People of Color (QTPOC) programming and all logistics that correspond with these programs. 

II. Support the coordination and supervision of the CGA Social Justice Retreat.
•Coordinate logistics for the CGA retreat in collaboration with Common Ground Alliance Coordinator.
•Serve as the support staff to undergraduate/graduate students who are part of the planning process. 
•Manage the assessment of the CGA retreat. 

Additional Responsibilities
•Maintain regular, preferably daily, office hours to meet with students individually and in small groups. Act as a mentor to students and engage interactions that support student growth and development.
•Actively pursue opportunities to partner with other departmental, campus, community, or national organizations to develop collaborations that support professional growth and/or meet CGA goals.
•Attend all regular staff meetings & grant related meetings, events, trainings
•Work with the Program Coordinator to complete the 6 month and 12 month employee evaluation
•Support all grant related assessment and reporting activities.
•Other duties as assigned.',
'created_at' => '2018-02-15 21:36:51.000000',
'question_id' => 410,
),
366 => 
array (
'answer_text' => 'The night security officers have in the past concentrated their efforts from the Main Library and mall area in the north and east to 6th street to the south and Euclid to the west. There has been more recorded activities near Tyndall Ave. as well as near Highland Ave. due to the residence halls and higher concentrations of students. 
This written, the Peace Officers have never been restricted to a specific area and their intentional coverage areas can be defined with student input.',
'created_at' => '2018-02-19 19:02:54.000000',
'question_id' => 422,
),
367 => 
array (
'answer_text' => 'Innovate UA\'s Graduate Assistant Duties and Responsibilities:
-- Work with the Executive Director to create a plan of work that enlists and develops student leadership to ensure quality program delivery and increases student participation and diversity
-- Identify, recruit, and develop volunteer leadership necessary to carry out the position’s plan of work.
-- Supervise, monitor, and coordinate activities including the work of student Directors and program volunteers.
-- Develop strategies and execute campus outreach efforts including delivering presentations, liaising with other academic and non-academic units on campus.
-- Work as a member of the Student Engagement & Career Development team to integrate the department\'s resources appropriately into Innovate UA activities, and to integrate Innovate UA activities into the larger scope of campus activities designed to prepare of UA students for careers aligned with their purpose and values
-- Regularly prepare educational and promotional materials using appropriate technology.
-- Communicate with Executive Director on a regular basis; identify problems and recommend changes
-- Be responsive to evolving position, program, and organizational needs; perform adjusted or additional duties as requested or assigned.',
'created_at' => '2018-02-19 21:57:29.000000',
'question_id' => 421,
),
368 => 
array (
'answer_text' => 'The current GA is funded from an FY18 Green Fund grant in support of the local government internship program.  Given changes to Green Fund governance starting this year, the Office of Sustainability is no longer eligible to submit Green Fund proposals, since we run the process and it is considered a form of bias for us to also be submitting proposals for consideration.  So, we need to seek support from other mechanisms to offer programmatic funding to students. In many cases, we can partner with others on programs who are submitting for Green Fund grants.  However, we still need some forms of direct financial support to offer programs of our own, and we would be very grateful if Student Services Fee could provide one piece of this puzzle for FY19.  

With the development of the UUASC sustainability advisory committee through partnership with student leadership and President Robbins, there is potential for this to be a mechanism to lobby UA administration for program budget allocations for the Office of Sustainability.  As UUASC is not yet established, this is not an opportunity currently available for FY19 planning.',
'created_at' => '2018-02-19 23:59:33.000000',
'question_id' => 423,
),
369 => 
array (
'answer_text' => 'The Common Ground Alliance (CGA) GA position is a 12 month contract due to the CGA Student Leadership Retreat which takes place early in the Fall semester. The GA supports and coordinates the retreat planning in handling the logistics which sets up the retreat to be well planned and incorporate student collaboration once the students are back in session in the Fall. The fiscal year spending is not allowed until July when the GA works on booking venues, ordering food, organizing the curriculum and ordering supplies. A project of this magnitude is the main portion of the work for the GA over the summer months, additionally they work on assessment of our programs and implement changes for the upcoming year. 

Global Experiential Learning (GEL) will have programming that will include our Vivir Mexico: Culture, History, and People Study Abroad program that will be in its fifth year. The program is implemented in the summer from June 18 to July 15. The graduate assistant (GA) will have responsibilities for this program leading up to the program with logistics and the pre departure classes being taught before we leave. The GA will be the point person in Tucson during the program to help with any program issues, and will be helping with the assignments and assessment after the program has been completed. The time in the summer will also be used for the future planning of other GEL programs in the upcoming year.',
'created_at' => '2018-02-22 18:48:09.000000',
'question_id' => 417,
),
370 => 
array (
'answer_text' => 'What is the role of the new care coordinator? 
This proposal seeks to mitigate the impact on connecting students to mental health professionals by working in partnership with Counseling and Psych Services (CAPS) to hire a full time licensed care coordinator with a masters in social work to be housed in the Dean of Students Office (DOS) for direct and timely intervention and support. 

What will that coordinator\'s work load look like? 
This position would support Dean of Students (DOS) referrals for mandatory administrative referrals (130 in FY 2016-17, 107 in fall 2017); helping assess the level of mental health risk, and also help bridge students with clear need for counseling or psychiatric treatment to CAPS , or off campus providers when appropriate. They would also be available for the lower level non mandatory referrals that will likely parallel approximately a quarter of our overall Student Assistance case load (~300). They would serve on the Student Assistance team collaboratively working on intervention plans, serving on BIT, and consulting on general mental health. This collectively would account for at least thirty hours a week (.75 FTE). The remaining hours (.25 FTE)  would be used to develop outreach and education for students including group sessions related to life skills, self-care, resilience, strategies for managing stress, and mindfulness. This position would also do general education and outreach to faculty, staff and advisors similar to the general outreach offered by Student Assistance but with a mental health focus. 
In evaluating students that get referred to DOS, experiencing any level of psychological concern, including substance abuse, it would be useful to have a licensed and trained mental health professional right on hand to talk to these students and be able to recognize what they may be looking at diagnostically and to then be able to assess severity, and follow up needs.

What is the expected impact of this position with the student population?
The expected impact, even  for students who are not  at a suicidal level, having relatively immediate access to talk to  a trained professional right where they are is important, because we know that many people  get lost in the shuffle between the suggestion to go to CAPS  and actually going. Right now, the assistance staff are serving in this role, but they are not specifically trained or credentialed to deal with some of these concerns.  As a result, the immediate access is a really important service to students, some of whom may not need ongoing counseling, but might be able to benefit from another type of consultation with a mental health provider.',
'created_at' => '2018-02-22 19:19:20.000000',
'question_id' => 420,
),
371 => 
array (
'answer_text' => 'A theme for all of the Cultural and Resource Centers this year has been to address issues of mental wellness. Beyond universal design considerations in our programs, offices, and services, we view disability as an intersectional issue that impacts the populations served within and across the centers. Disability-related issues intersecting with other identities impacts LGBTQ Students, Women, and Students of Color. In particular, we have focused on providing culturally relevant and supportive mental wellness programs and staff to support students with anxiety, depression, and other disabilities to be successful at the UA.

For instance, the Common Ground Alliance hosts a number of programs that aim to address mental health issues of our students. Specifically, the Queer, Trans People of Color (QTPoC) weekly sessions are rooted in art and healing to provide space for students to dialogue about mental health issues. This past year, Outspoken—a spoken word poetry program brought a guest poet who addressed topics of race, culture and mental health issues through poetry. 

As another example, Global Experiential Learning includes programs that have existed in the past, but this is the first year as an official area under the Dean of Students Office. In creating the foundation of GEL, we want all of the programs to be accessible to all students.  We have met and had consultation with the staff of the Disabilities Resource Center (DRC) to discuss how to create an accessible experience for all students in our past programs and moving toward the future. We have been changing components in our current programing to accommodate students with disabilities as well as placed a priority in accessibility when creating new programs.

In these discussions, together with the DRC we have proposed creating a program that is accessible to all students regardless of disability. Currently, there is a Geoscience program called Accessible Earth that is fully accessible to all students in Orvieto, Italy. GEL would like to collaborate with this program to create an accessible program like this with a focus on the foundational pillars of GEL. These foundation pillars include social justice, environmental justice, cultural competency, and service learning. During our discussions with DRC, it was brought up that students with disabilities are hesitant to travel or take these global experiences to other countries because of the challenges of international locations not being accessible or the difficult planning it takes to travel. GEL would like to research these countries to eliminate these barriers and give all students a chance to experience these programs.

A third example is at the Women’s Resource Center’s upcoming feminist conference where we have invited a speaker Alisha Vasquez, who will conduct a workshop titled “(Dis)Ability and Intersectional Living." Alisha will examine the Disability Rights Movement, common micro-aggressions that disabled people experience, living at the intersection of multiple marginalized identities, and the ways disabled people today are fighting for their rights.',
'created_at' => '2018-02-22 22:21:41.000000',
'question_id' => 416,
),
372 => 
array (
'answer_text' => 'This proposal was funded with 15K from Student Services Fee for FY18. In the ASUA budget, the Treasurer separated out the projects funded by Student Services Fee. You can see on page 3 of the ASUA budget that Student Services Fee funding is included as Revenue for the budget, and then it is also included as an expense of 15K on page 13. This was included as an expense to ensure that the budget would balance. ASUA provided funding in the way of $1,000 for the Campus Pantry director. ASUA budget is available at https://docs.wixstatic.com/ugd/5b0d66_3c2655eb3fb4465db3689785a7ea1e46.pdf.',
'created_at' => '2018-02-22 23:54:55.000000',
'question_id' => 424,
),
));
        
        
    }
}