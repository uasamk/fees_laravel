<?php

use Illuminate\Database\Seeder;

class SsfInitiativeProjectTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('initiative_project')->delete();
        
        \DB::table('initiative_project')->insert(array (
            0 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 1,
            ),
            1 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 4,
            ),
            2 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 6,
            ),
            3 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 10,
            ),
            4 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 11,
            ),
            5 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 15,
            ),
            6 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 18,
            ),
            7 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 22,
            ),
            8 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 25,
            ),
            9 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 29,
            ),
            10 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 41,
            ),
            11 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 42,
            ),
            12 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 43,
            ),
            13 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 44,
            ),
            14 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 45,
            ),
            15 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 50,
            ),
            16 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 53,
            ),
            17 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 57,
            ),
            18 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 58,
            ),
            19 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 66,
            ),
            20 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 75,
            ),
            21 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 76,
            ),
            22 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 77,
            ),
            23 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 81,
            ),
            24 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 84,
            ),
            25 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 85,
            ),
            26 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 89,
            ),
            27 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 91,
            ),
            28 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 101,
            ),
            29 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 106,
            ),
            30 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 129,
            ),
            31 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 143,
            ),
            32 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 154,
            ),
            33 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 156,
            ),
            34 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 159,
            ),
            35 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 161,
            ),
            36 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 162,
            ),
            37 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 172,
            ),
            38 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 174,
            ),
            39 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 182,
            ),
            40 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 185,
            ),
            41 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 186,
            ),
            42 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 188,
            ),
            43 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 191,
            ),
            44 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 197,
            ),
            45 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 205,
            ),
            46 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 214,
            ),
            47 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 221,
            ),
            48 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 224,
            ),
            49 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 225,
            ),
            50 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 226,
            ),
            51 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 232,
            ),
            52 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 237,
            ),
            53 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 250,
            ),
            54 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 260,
            ),
            55 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 267,
            ),
            56 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 268,
            ),
            57 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 269,
            ),
            58 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 275,
            ),
            59 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 0,
                'project_id' => 277,
            ),
            60 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 23,
            ),
            61 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 48,
            ),
            62 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 53,
            ),
            63 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 55,
            ),
            64 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 60,
            ),
            65 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 63,
            ),
            66 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 65,
            ),
            67 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 67,
            ),
            68 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 72,
            ),
            69 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 73,
            ),
            70 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 74,
            ),
            71 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 75,
            ),
            72 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 77,
            ),
            73 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 78,
            ),
            74 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 79,
            ),
            75 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 81,
            ),
            76 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 82,
            ),
            77 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 83,
            ),
            78 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 85,
            ),
            79 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 86,
            ),
            80 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 87,
            ),
            81 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 88,
            ),
            82 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 90,
            ),
            83 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 91,
            ),
            84 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 92,
            ),
            85 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 93,
            ),
            86 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 96,
            ),
            87 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 97,
            ),
            88 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 99,
            ),
            89 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 100,
            ),
            90 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 101,
            ),
            91 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 105,
            ),
            92 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 110,
            ),
            93 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 111,
            ),
            94 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 113,
            ),
            95 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 116,
            ),
            96 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 117,
            ),
            97 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 118,
            ),
            98 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 119,
            ),
            99 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 120,
            ),
            100 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 122,
            ),
            101 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 123,
            ),
            102 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 124,
            ),
            103 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 125,
            ),
            104 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 127,
            ),
            105 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 129,
            ),
            106 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 134,
            ),
            107 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 135,
            ),
            108 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 136,
            ),
            109 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 137,
            ),
            110 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 150,
            ),
            111 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 151,
            ),
            112 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 152,
            ),
            113 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 153,
            ),
            114 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 156,
            ),
            115 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 159,
            ),
            116 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 160,
            ),
            117 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 161,
            ),
            118 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 162,
            ),
            119 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 163,
            ),
            120 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 170,
            ),
            121 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 171,
            ),
            122 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 172,
            ),
            123 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 174,
            ),
            124 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 175,
            ),
            125 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 177,
            ),
            126 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 179,
            ),
            127 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 180,
            ),
            128 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 182,
            ),
            129 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 185,
            ),
            130 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 186,
            ),
            131 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 187,
            ),
            132 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 188,
            ),
            133 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 189,
            ),
            134 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 190,
            ),
            135 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 191,
            ),
            136 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 193,
            ),
            137 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 194,
            ),
            138 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 195,
            ),
            139 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 197,
            ),
            140 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 198,
            ),
            141 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 206,
            ),
            142 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 211,
            ),
            143 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 212,
            ),
            144 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 214,
            ),
            145 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 217,
            ),
            146 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 218,
            ),
            147 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 221,
            ),
            148 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 222,
            ),
            149 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 223,
            ),
            150 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 224,
            ),
            151 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 225,
            ),
            152 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 228,
            ),
            153 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 229,
            ),
            154 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 231,
            ),
            155 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 232,
            ),
            156 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 233,
            ),
            157 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 234,
            ),
            158 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 235,
            ),
            159 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 236,
            ),
            160 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 240,
            ),
            161 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 244,
            ),
            162 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 245,
            ),
            163 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 246,
            ),
            164 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 247,
            ),
            165 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 248,
            ),
            166 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 250,
            ),
            167 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 260,
            ),
            168 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 262,
            ),
            169 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 265,
            ),
            170 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 266,
            ),
            171 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 267,
            ),
            172 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 269,
            ),
            173 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 271,
            ),
            174 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 272,
            ),
            175 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 276,
            ),
            176 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 279,
            ),
            177 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 281,
            ),
            178 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 282,
            ),
            179 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 284,
            ),
            180 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 0,
                'project_id' => 292,
            ),
            181 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 4,
            ),
            182 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 6,
            ),
            183 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 8,
            ),
            184 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 10,
            ),
            185 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 11,
            ),
            186 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 15,
            ),
            187 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 16,
            ),
            188 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 18,
            ),
            189 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 25,
            ),
            190 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 30,
            ),
            191 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 33,
            ),
            192 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 34,
            ),
            193 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 35,
            ),
            194 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 37,
            ),
            195 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 41,
            ),
            196 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 43,
            ),
            197 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 44,
            ),
            198 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 45,
            ),
            199 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 46,
            ),
            200 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 47,
            ),
            201 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 49,
            ),
            202 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 50,
            ),
            203 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 52,
            ),
            204 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 53,
            ),
            205 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 54,
            ),
            206 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 55,
            ),
            207 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 56,
            ),
            208 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 61,
            ),
            209 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 66,
            ),
            210 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 67,
            ),
            211 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 71,
            ),
            212 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 77,
            ),
            213 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 81,
            ),
            214 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 85,
            ),
            215 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 88,
            ),
            216 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 90,
            ),
            217 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 94,
            ),
            218 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 95,
            ),
            219 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 97,
            ),
            220 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 101,
            ),
            221 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 104,
            ),
            222 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 110,
            ),
            223 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 111,
            ),
            224 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 117,
            ),
            225 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 118,
            ),
            226 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 134,
            ),
            227 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 135,
            ),
            228 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 138,
            ),
            229 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 143,
            ),
            230 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 144,
            ),
            231 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 150,
            ),
            232 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 154,
            ),
            233 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 155,
            ),
            234 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 157,
            ),
            235 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 162,
            ),
            236 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 172,
            ),
            237 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 173,
            ),
            238 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 174,
            ),
            239 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 177,
            ),
            240 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 187,
            ),
            241 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 188,
            ),
            242 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 189,
            ),
            243 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 194,
            ),
            244 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 196,
            ),
            245 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 198,
            ),
            246 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 203,
            ),
            247 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 205,
            ),
            248 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 206,
            ),
            249 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 218,
            ),
            250 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 219,
            ),
            251 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 224,
            ),
            252 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 226,
            ),
            253 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 227,
            ),
            254 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 230,
            ),
            255 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 233,
            ),
            256 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 234,
            ),
            257 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 235,
            ),
            258 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 237,
            ),
            259 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 238,
            ),
            260 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 246,
            ),
            261 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 260,
            ),
            262 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 262,
            ),
            263 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 265,
            ),
            264 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 269,
            ),
            265 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 277,
            ),
            266 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 280,
            ),
            267 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 281,
            ),
            268 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 289,
            ),
            269 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 0,
                'project_id' => 291,
            ),
            270 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 9,
            ),
            271 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 10,
            ),
            272 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 17,
            ),
            273 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 20,
            ),
            274 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 24,
            ),
            275 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 28,
            ),
            276 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 30,
            ),
            277 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 31,
            ),
            278 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 32,
            ),
            279 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 35,
            ),
            280 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 37,
            ),
            281 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 38,
            ),
            282 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 42,
            ),
            283 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 43,
            ),
            284 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 47,
            ),
            285 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 52,
            ),
            286 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 56,
            ),
            287 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 57,
            ),
            288 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 61,
            ),
            289 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 63,
            ),
            290 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 67,
            ),
            291 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 72,
            ),
            292 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 73,
            ),
            293 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 74,
            ),
            294 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 75,
            ),
            295 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 77,
            ),
            296 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 79,
            ),
            297 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 81,
            ),
            298 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 83,
            ),
            299 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 88,
            ),
            300 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 96,
            ),
            301 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 101,
            ),
            302 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 115,
            ),
            303 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 120,
            ),
            304 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 125,
            ),
            305 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 135,
            ),
            306 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 163,
            ),
            307 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 182,
            ),
            308 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 189,
            ),
            309 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 193,
            ),
            310 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 206,
            ),
            311 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 214,
            ),
            312 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 223,
            ),
            313 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 224,
            ),
            314 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 225,
            ),
            315 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 246,
            ),
            316 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 248,
            ),
            317 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 250,
            ),
            318 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 267,
            ),
            319 => 
            array (
                'initiative_id' => 4,
                'is_grad' => 0,
                'project_id' => 276,
            ),
            320 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 1,
            ),
            321 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 2,
            ),
            322 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 3,
            ),
            323 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 4,
            ),
            324 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 6,
            ),
            325 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 7,
            ),
            326 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 8,
            ),
            327 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 9,
            ),
            328 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 10,
            ),
            329 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 12,
            ),
            330 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 14,
            ),
            331 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 15,
            ),
            332 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 16,
            ),
            333 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 17,
            ),
            334 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 18,
            ),
            335 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 22,
            ),
            336 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 24,
            ),
            337 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 29,
            ),
            338 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 30,
            ),
            339 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 31,
            ),
            340 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 34,
            ),
            341 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 35,
            ),
            342 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 59,
            ),
            343 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 63,
            ),
            344 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 67,
            ),
            345 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 72,
            ),
            346 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 77,
            ),
            347 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 79,
            ),
            348 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 84,
            ),
            349 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 86,
            ),
            350 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 87,
            ),
            351 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 88,
            ),
            352 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 92,
            ),
            353 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 93,
            ),
            354 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 96,
            ),
            355 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 101,
            ),
            356 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 105,
            ),
            357 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 113,
            ),
            358 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 117,
            ),
            359 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 119,
            ),
            360 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 120,
            ),
            361 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 122,
            ),
            362 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 123,
            ),
            363 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 129,
            ),
            364 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 137,
            ),
            365 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 152,
            ),
            366 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 153,
            ),
            367 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 156,
            ),
            368 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 159,
            ),
            369 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 160,
            ),
            370 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 161,
            ),
            371 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 163,
            ),
            372 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 164,
            ),
            373 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 172,
            ),
            374 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 174,
            ),
            375 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 175,
            ),
            376 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 179,
            ),
            377 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 180,
            ),
            378 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 182,
            ),
            379 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 185,
            ),
            380 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 186,
            ),
            381 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 188,
            ),
            382 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 189,
            ),
            383 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 190,
            ),
            384 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 191,
            ),
            385 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 193,
            ),
            386 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 197,
            ),
            387 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 205,
            ),
            388 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 206,
            ),
            389 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 211,
            ),
            390 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 212,
            ),
            391 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 214,
            ),
            392 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 218,
            ),
            393 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 220,
            ),
            394 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 221,
            ),
            395 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 222,
            ),
            396 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 223,
            ),
            397 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 224,
            ),
            398 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 225,
            ),
            399 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 230,
            ),
            400 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 231,
            ),
            401 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 236,
            ),
            402 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 240,
            ),
            403 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 246,
            ),
            404 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 247,
            ),
            405 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 248,
            ),
            406 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 250,
            ),
            407 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 260,
            ),
            408 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 266,
            ),
            409 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 267,
            ),
            410 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 269,
            ),
            411 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 271,
            ),
            412 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 272,
            ),
            413 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 276,
            ),
            414 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 277,
            ),
            415 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 280,
            ),
            416 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 281,
            ),
            417 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 282,
            ),
            418 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 284,
            ),
            419 => 
            array (
                'initiative_id' => 5,
                'is_grad' => 0,
                'project_id' => 292,
            ),
            420 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 4,
            ),
            421 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 5,
            ),
            422 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 12,
            ),
            423 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 17,
            ),
            424 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 18,
            ),
            425 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 21,
            ),
            426 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 25,
            ),
            427 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 26,
            ),
            428 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 27,
            ),
            429 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 29,
            ),
            430 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 33,
            ),
            431 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 34,
            ),
            432 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 35,
            ),
            433 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 43,
            ),
            434 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 50,
            ),
            435 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 53,
            ),
            436 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 67,
            ),
            437 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 69,
            ),
            438 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 70,
            ),
            439 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 72,
            ),
            440 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 77,
            ),
            441 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 78,
            ),
            442 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 84,
            ),
            443 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 92,
            ),
            444 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 99,
            ),
            445 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 100,
            ),
            446 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 101,
            ),
            447 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 103,
            ),
            448 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 107,
            ),
            449 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 108,
            ),
            450 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 109,
            ),
            451 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 117,
            ),
            452 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 121,
            ),
            453 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 122,
            ),
            454 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 123,
            ),
            455 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 125,
            ),
            456 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 129,
            ),
            457 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 130,
            ),
            458 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 131,
            ),
            459 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 132,
            ),
            460 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 133,
            ),
            461 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 142,
            ),
            462 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 155,
            ),
            463 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 158,
            ),
            464 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 160,
            ),
            465 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 161,
            ),
            466 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 165,
            ),
            467 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 166,
            ),
            468 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 167,
            ),
            469 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 168,
            ),
            470 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 172,
            ),
            471 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 174,
            ),
            472 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 175,
            ),
            473 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 177,
            ),
            474 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 185,
            ),
            475 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 186,
            ),
            476 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 188,
            ),
            477 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 189,
            ),
            478 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 194,
            ),
            479 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 199,
            ),
            480 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 200,
            ),
            481 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 201,
            ),
            482 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 202,
            ),
            483 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 204,
            ),
            484 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 205,
            ),
            485 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 211,
            ),
            486 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 213,
            ),
            487 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 215,
            ),
            488 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 216,
            ),
            489 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 223,
            ),
            490 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 224,
            ),
            491 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 226,
            ),
            492 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 227,
            ),
            493 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 231,
            ),
            494 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 240,
            ),
            495 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 243,
            ),
            496 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 246,
            ),
            497 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 250,
            ),
            498 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 259,
            ),
            499 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 260,
            ),
        ));
        \DB::table('initiative_project')->insert(array (
            0 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 262,
            ),
            1 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 264,
            ),
            2 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 271,
            ),
            3 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 276,
            ),
            4 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 281,
            ),
            5 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 284,
            ),
            6 => 
            array (
                'initiative_id' => 6,
                'is_grad' => 1,
                'project_id' => 292,
            ),
            7 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 1,
            ),
            8 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 10,
            ),
            9 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 11,
            ),
            10 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 17,
            ),
            11 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 18,
            ),
            12 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 21,
            ),
            13 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 25,
            ),
            14 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 29,
            ),
            15 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 31,
            ),
            16 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 48,
            ),
            17 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 53,
            ),
            18 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 60,
            ),
            19 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 66,
            ),
            20 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 68,
            ),
            21 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 70,
            ),
            22 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 75,
            ),
            23 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 76,
            ),
            24 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 77,
            ),
            25 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 81,
            ),
            26 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 85,
            ),
            27 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 89,
            ),
            28 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 91,
            ),
            29 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 101,
            ),
            30 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 103,
            ),
            31 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 106,
            ),
            32 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 108,
            ),
            33 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 109,
            ),
            34 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 121,
            ),
            35 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 129,
            ),
            36 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 133,
            ),
            37 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 154,
            ),
            38 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 156,
            ),
            39 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 159,
            ),
            40 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 161,
            ),
            41 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 162,
            ),
            42 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 167,
            ),
            43 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 168,
            ),
            44 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 172,
            ),
            45 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 182,
            ),
            46 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 185,
            ),
            47 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 188,
            ),
            48 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 191,
            ),
            49 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 197,
            ),
            50 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 199,
            ),
            51 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 200,
            ),
            52 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 201,
            ),
            53 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 202,
            ),
            54 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 204,
            ),
            55 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 213,
            ),
            56 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 216,
            ),
            57 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 221,
            ),
            58 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 224,
            ),
            59 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 225,
            ),
            60 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 226,
            ),
            61 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 232,
            ),
            62 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 237,
            ),
            63 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 243,
            ),
            64 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 264,
            ),
            65 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 267,
            ),
            66 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 268,
            ),
            67 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 269,
            ),
            68 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 275,
            ),
            69 => 
            array (
                'initiative_id' => 1,
                'is_grad' => 1,
                'project_id' => 277,
            ),
            70 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 9,
            ),
            71 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 10,
            ),
            72 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 17,
            ),
            73 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 20,
            ),
            74 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 24,
            ),
            75 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 28,
            ),
            76 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 30,
            ),
            77 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 31,
            ),
            78 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 32,
            ),
            79 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 35,
            ),
            80 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 37,
            ),
            81 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 38,
            ),
            82 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 42,
            ),
            83 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 43,
            ),
            84 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 47,
            ),
            85 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 52,
            ),
            86 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 56,
            ),
            87 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 57,
            ),
            88 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 61,
            ),
            89 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 67,
            ),
            90 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 69,
            ),
            91 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 70,
            ),
            92 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 72,
            ),
            93 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 75,
            ),
            94 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 77,
            ),
            95 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 78,
            ),
            96 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 79,
            ),
            97 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 80,
            ),
            98 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 81,
            ),
            99 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 82,
            ),
            100 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 85,
            ),
            101 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 86,
            ),
            102 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 87,
            ),
            103 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 88,
            ),
            104 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 90,
            ),
            105 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 91,
            ),
            106 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 92,
            ),
            107 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 95,
            ),
            108 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 96,
            ),
            109 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 99,
            ),
            110 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 100,
            ),
            111 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 101,
            ),
            112 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 103,
            ),
            113 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 107,
            ),
            114 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 108,
            ),
            115 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 109,
            ),
            116 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 110,
            ),
            117 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 111,
            ),
            118 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 113,
            ),
            119 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 117,
            ),
            120 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 121,
            ),
            121 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 130,
            ),
            122 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 131,
            ),
            123 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 132,
            ),
            124 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 133,
            ),
            125 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 138,
            ),
            126 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 150,
            ),
            127 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 154,
            ),
            128 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 155,
            ),
            129 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 158,
            ),
            130 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 162,
            ),
            131 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 165,
            ),
            132 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 166,
            ),
            133 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 167,
            ),
            134 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 168,
            ),
            135 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 172,
            ),
            136 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 173,
            ),
            137 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 177,
            ),
            138 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 187,
            ),
            139 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 188,
            ),
            140 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 189,
            ),
            141 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 194,
            ),
            142 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 198,
            ),
            143 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 200,
            ),
            144 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 201,
            ),
            145 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 202,
            ),
            146 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 203,
            ),
            147 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 204,
            ),
            148 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 206,
            ),
            149 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 215,
            ),
            150 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 216,
            ),
            151 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 224,
            ),
            152 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 226,
            ),
            153 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 227,
            ),
            154 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 230,
            ),
            155 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 234,
            ),
            156 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 235,
            ),
            157 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 237,
            ),
            158 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 238,
            ),
            159 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 243,
            ),
            160 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 246,
            ),
            161 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 259,
            ),
            162 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 262,
            ),
            163 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 264,
            ),
            164 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 265,
            ),
            165 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 269,
            ),
            166 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 277,
            ),
            167 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 281,
            ),
            168 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 289,
            ),
            169 => 
            array (
                'initiative_id' => 3,
                'is_grad' => 1,
                'project_id' => 291,
            ),
            170 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 2,
            ),
            171 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 4,
            ),
            172 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 18,
            ),
            173 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 30,
            ),
            174 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 33,
            ),
            175 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 43,
            ),
            176 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 46,
            ),
            177 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 47,
            ),
            178 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 49,
            ),
            179 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 50,
            ),
            180 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 51,
            ),
            181 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 53,
            ),
            182 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 54,
            ),
            183 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 61,
            ),
            184 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 66,
            ),
            185 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 67,
            ),
            186 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 69,
            ),
            187 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 70,
            ),
            188 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 77,
            ),
            189 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 81,
            ),
            190 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 85,
            ),
            191 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 88,
            ),
            192 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 90,
            ),
            193 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 94,
            ),
            194 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 95,
            ),
            195 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 97,
            ),
            196 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 101,
            ),
            197 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 103,
            ),
            198 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 104,
            ),
            199 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 108,
            ),
            200 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 109,
            ),
            201 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 110,
            ),
            202 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 111,
            ),
            203 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 116,
            ),
            204 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 117,
            ),
            205 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 118,
            ),
            206 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 119,
            ),
            207 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 120,
            ),
            208 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 121,
            ),
            209 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 122,
            ),
            210 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 123,
            ),
            211 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 124,
            ),
            212 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 125,
            ),
            213 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 127,
            ),
            214 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 130,
            ),
            215 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 131,
            ),
            216 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 132,
            ),
            217 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 133,
            ),
            218 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 134,
            ),
            219 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 135,
            ),
            220 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 136,
            ),
            221 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 137,
            ),
            222 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 142,
            ),
            223 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 150,
            ),
            224 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 152,
            ),
            225 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 153,
            ),
            226 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 155,
            ),
            227 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 156,
            ),
            228 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 158,
            ),
            229 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 159,
            ),
            230 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 160,
            ),
            231 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 161,
            ),
            232 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 162,
            ),
            233 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 163,
            ),
            234 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 164,
            ),
            235 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 165,
            ),
            236 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 166,
            ),
            237 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 167,
            ),
            238 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 168,
            ),
            239 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 171,
            ),
            240 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 172,
            ),
            241 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 174,
            ),
            242 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 175,
            ),
            243 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 177,
            ),
            244 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 180,
            ),
            245 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 182,
            ),
            246 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 185,
            ),
            247 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 186,
            ),
            248 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 187,
            ),
            249 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 188,
            ),
            250 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 189,
            ),
            251 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 190,
            ),
            252 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 191,
            ),
            253 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 193,
            ),
            254 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 194,
            ),
            255 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 195,
            ),
            256 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 197,
            ),
            257 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 198,
            ),
            258 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 199,
            ),
            259 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 200,
            ),
            260 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 201,
            ),
            261 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 202,
            ),
            262 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 204,
            ),
            263 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 205,
            ),
            264 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 206,
            ),
            265 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 211,
            ),
            266 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 212,
            ),
            267 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 213,
            ),
            268 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 215,
            ),
            269 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 216,
            ),
            270 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 217,
            ),
            271 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 218,
            ),
            272 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 221,
            ),
            273 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 222,
            ),
            274 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 223,
            ),
            275 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 224,
            ),
            276 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 225,
            ),
            277 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 227,
            ),
            278 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 228,
            ),
            279 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 229,
            ),
            280 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 230,
            ),
            281 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 231,
            ),
            282 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 232,
            ),
            283 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 234,
            ),
            284 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 235,
            ),
            285 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 236,
            ),
            286 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 240,
            ),
            287 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 243,
            ),
            288 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 244,
            ),
            289 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 245,
            ),
            290 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 246,
            ),
            291 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 247,
            ),
            292 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 248,
            ),
            293 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 250,
            ),
            294 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 259,
            ),
            295 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 260,
            ),
            296 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 262,
            ),
            297 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 264,
            ),
            298 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 265,
            ),
            299 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 266,
            ),
            300 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 267,
            ),
            301 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 269,
            ),
            302 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 271,
            ),
            303 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 276,
            ),
            304 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 277,
            ),
            305 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 279,
            ),
            306 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 280,
            ),
            307 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 281,
            ),
            308 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 282,
            ),
            309 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 284,
            ),
            310 => 
            array (
                'initiative_id' => 2,
                'is_grad' => 1,
                'project_id' => 292,
            ),
            311 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 24,
            ),
            312 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 56,
            ),
            313 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 59,
            ),
            314 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 61,
            ),
            315 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 66,
            ),
            316 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 73,
            ),
            317 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 77,
            ),
            318 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 84,
            ),
            319 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 85,
            ),
            320 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 86,
            ),
            321 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 89,
            ),
            322 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 95,
            ),
            323 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 98,
            ),
            324 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 99,
            ),
            325 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 101,
            ),
            326 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 104,
            ),
            327 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 115,
            ),
            328 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 124,
            ),
            329 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 136,
            ),
            330 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 154,
            ),
            331 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 155,
            ),
            332 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 170,
            ),
            333 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 182,
            ),
            334 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 187,
            ),
            335 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 192,
            ),
            336 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 199,
            ),
            337 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 217,
            ),
            338 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 224,
            ),
            339 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 225,
            ),
            340 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 227,
            ),
            341 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 234,
            ),
            342 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 235,
            ),
            343 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 237,
            ),
            344 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 245,
            ),
            345 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 267,
            ),
            346 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 269,
            ),
            347 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 270,
            ),
            348 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 276,
            ),
            349 => 
            array (
                'initiative_id' => 7,
                'is_grad' => 1,
                'project_id' => 279,
            ),
        ));
        
        
    }
}