<?php

use Illuminate\Database\Seeder;

class SsfQuestionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('questions')->delete();
        
        \DB::table('questions')->insert(array (
            0 => 
            array (
                'id_old' => 19993,
                'question_text' => 'If Student Services Fee monies weren\'t available how many students would the Next Steps Center hire and at what rate of pay?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:24:27.000000',
                'created_at' => '2010-01-25 21:24:27.000000',
                'questionable_id' => 22,
                'questionable_type' => 'App\\Project',
            ),
            1 => 
            array (
                'id_old' => 19994,
                'question_text' => 'How many IN PERSON contacts does the Next Step Center have during any given day?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:25:30.000000',
                'created_at' => '2010-01-25 21:25:30.000000',
                'questionable_id' => 22,
                'questionable_type' => 'App\\Project',
            ),
            2 => 
            array (
                'id_old' => 19995,
                'question_text' => 'What has been the traditional total dollar amount demanded from Club Funding in the past several years?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:27:00.000000',
                'created_at' => '2010-01-25 21:27:00.000000',
                'questionable_id' => 26,
                'questionable_type' => 'App\\Project',
            ),
            3 => 
            array (
                'id_old' => 19996,
                'question_text' => 'What is GIS and what does the IT equipment consists of?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:30:24.000000',
                'created_at' => '2010-01-25 21:30:24.000000',
                'questionable_id' => 2,
                'questionable_type' => 'App\\Project',
            ),
            4 => 
            array (
                'id_old' => 19997,
                'question_text' => 'What programming is being developed and how does the \'Education\' and \'Outreach\' components benefit students as a whole?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:31:18.000000',
                'created_at' => '2010-01-25 21:31:18.000000',
                'questionable_id' => 2,
                'questionable_type' => 'App\\Project',
            ),
            5 => 
            array (
                'id_old' => 19998,
                'question_text' => 'How much is ASUA willing to support the administrative positions within ASUA sustainability?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:31:51.000000',
                'created_at' => '2010-01-25 21:31:51.000000',
                'questionable_id' => 2,
                'questionable_type' => 'App\\Project',
            ),
            6 => 
            array (
                'id_old' => 19999,
                'question_text' => 'For the Disabled Cart Services are there any waiting times between pickups and if so, approximately how long are they?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:32:39.000000',
                'created_at' => '2010-01-25 21:32:39.000000',
                'questionable_id' => 9,
                'questionable_type' => 'App\\Project',
            ),
            7 => 
            array (
                'id_old' => 199910,
                'question_text' => 'For Disabled Cart Services, is the request to the Student Services Fee Advisory Board for support of the entire program or simply an addition to the program?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:33:33.000000',
                'created_at' => '2010-01-25 21:33:33.000000',
                'questionable_id' => 9,
                'questionable_type' => 'App\\Project',
            ),
            8 => 
            array (
                'id_old' => 199911,
                'question_text' => 'Is PTS still willing to share in support for the program?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:34:10.000000',
                'created_at' => '2010-01-25 21:34:10.000000',
                'questionable_id' => 9,
                'questionable_type' => 'App\\Project',
            ),
            9 => 
            array (
                'id_old' => 199912,
                'question_text' => 'The proposal makes clear that with the Disabled Cart Services prioritized so high some other ASUA or PTS programs may suffer. Please explain what these programs may be.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:35:28.000000',
                'created_at' => '2010-01-25 21:35:28.000000',
                'questionable_id' => 9,
                'questionable_type' => 'App\\Project',
            ),
            10 => 
            array (
                'id_old' => 199913,
            'question_text' => 'What are the specific guidelines for determining which of the applicants are awarded a bookstore scholarship? (If a \'grading rubric\' is available please feel free to incorporate its contents.)',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:36:57.000000',
                'created_at' => '2010-01-25 21:36:57.000000',
                'questionable_id' => 11,
                'questionable_type' => 'App\\Project',
            ),
            11 => 
            array (
                'id_old' => 199914,
                'question_text' => 'Is the Alcohol Risk Reduction Program volunteer or mandatory for participants. If it is volunteer please explain how the participants would be recruited.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:38:08.000000',
                'created_at' => '2010-01-25 21:38:08.000000',
                'questionable_id' => 17,
                'questionable_type' => 'App\\Project',
            ),
            12 => 
            array (
                'id_old' => 199915,
                'question_text' => 'Please provide curriculum specifics for the educational component of the Alcohol Risk Reduction Program.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:39:04.000000',
                'created_at' => '2010-01-25 21:39:04.000000',
                'questionable_id' => 17,
                'questionable_type' => 'App\\Project',
            ),
            13 => 
            array (
                'id_old' => 199916,
                'question_text' => 'Does the submitted budget for Federal Minimum Wage increases include restoring the usual operating hours to campus rec?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:40:40.000000',
                'created_at' => '2010-01-25 21:40:40.000000',
                'questionable_id' => 31,
                'questionable_type' => 'App\\Project',
            ),
            14 => 
            array (
                'id_old' => 199917,
                'question_text' => 'If Student Services Fee monies were not available what would happen to the size and scope of the student employee group in the Recreation Center?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:42:14.000000',
                'created_at' => '2010-01-25 21:42:14.000000',
                'questionable_id' => 31,
                'questionable_type' => 'App\\Project',
            ),
            15 => 
            array (
                'id_old' => 199918,
                'question_text' => 'Will the Campus Recreation Center pay for upkeep of the proposed Campus Rec Website or will further Student Services Fee support be solicited for its maintenance?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:43:30.000000',
                'created_at' => '2010-01-25 21:43:30.000000',
                'questionable_id' => 38,
                'questionable_type' => 'App\\Project',
            ),
            16 => 
            array (
                'id_old' => 199919,
                'question_text' => 'What is the plan for regulating and enforcing online registration, especially for equipment?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:44:07.000000',
                'created_at' => '2010-01-25 21:44:07.000000',
                'questionable_id' => 38,
                'questionable_type' => 'App\\Project',
            ),
            17 => 
            array (
                'id_old' => 199920,
                'question_text' => 'Is there currently a wait time for student using Campus Recreation services? If so, what is it?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:44:39.000000',
                'created_at' => '2010-01-25 21:44:39.000000',
                'questionable_id' => 38,
                'questionable_type' => 'App\\Project',
            ),
            18 => 
            array (
                'id_old' => 199921,
                'question_text' => 'Please submit data which supports the need for this product.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:45:40.000000',
                'created_at' => '2010-01-25 21:45:40.000000',
                'questionable_id' => 38,
                'questionable_type' => 'App\\Project',
            ),
            19 => 
            array (
                'id_old' => 199922,
            'question_text' => 'What is the nature of the activities the Career Ambassadors perform (please detail the bullets in the submitted proposal.)?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:46:49.000000',
                'created_at' => '2010-01-25 21:46:49.000000',
                'questionable_id' => 7,
                'questionable_type' => 'App\\Project',
            ),
            20 => 
            array (
                'id_old' => 199923,
                'question_text' => 'To what extent are graduate and international student taken into account?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:47:14.000000',
                'created_at' => '2010-01-25 21:47:14.000000',
                'questionable_id' => 7,
                'questionable_type' => 'App\\Project',
            ),
            21 => 
            array (
                'id_old' => 199924,
                'question_text' => 'Please prioritize the subscriptions from most valuable to least valuable.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:47:42.000000',
                'created_at' => '2010-01-25 21:47:42.000000',
                'questionable_id' => 14,
                'questionable_type' => 'App\\Project',
            ),
            22 => 
            array (
                'id_old' => 199925,
                'question_text' => 'If possible, how many student have been employed as a result of these subscriptions?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:48:15.000000',
                'created_at' => '2010-01-25 21:48:15.000000',
                'questionable_id' => 14,
                'questionable_type' => 'App\\Project',
            ),
            23 => 
            array (
                'id_old' => 199926,
                'question_text' => 'Do any of the subscriptions overlap in terms of the services provided?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-01-29 21:48:44.000000',
                'created_at' => '2010-01-25 21:48:44.000000',
                'questionable_id' => 14,
                'questionable_type' => 'App\\Project',
            ),
            24 => 
            array (
                'id_old' => 199927,
                'question_text' => 'What is GIS and what does the IT equipment consists of?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:06:17.000000',
                'created_at' => '2010-02-02 20:06:17.000000',
                'questionable_id' => 2,
                'questionable_type' => 'App\\Project',
            ),
            25 => 
            array (
                'id_old' => 199928,
                'question_text' => 'What programming is being developed and how does the \'Education\' and \'Outreach\' components benefit students as a whole?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:06:30.000000',
                'created_at' => '2010-02-02 20:06:30.000000',
                'questionable_id' => 2,
                'questionable_type' => 'App\\Project',
            ),
            26 => 
            array (
                'id_old' => 199929,
                'question_text' => 'How much is ASUA willing to support the administrative positions within ASUA sustainability?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:06:44.000000',
                'created_at' => '2010-02-02 20:06:44.000000',
                'questionable_id' => 2,
                'questionable_type' => 'App\\Project',
            ),
            27 => 
            array (
                'id_old' => 199930,
                'question_text' => 'The proposal makes clear that with the Disabled Cart Services prioritized so high some other ASUA or PTS programs may suffer. Please explain what these programs may be.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:07:48.000000',
                'created_at' => '2010-02-02 20:07:48.000000',
                'questionable_id' => 9,
                'questionable_type' => 'App\\Project',
            ),
            28 => 
            array (
                'id_old' => 199931,
                'question_text' => 'In the application $7,500.00 is requested of the Student Services Fee however the attached budget has a total figure of $27,500.00. Please indicate which figure is correct in your response.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:11:22.000000',
                'created_at' => '2010-02-02 20:11:22.000000',
                'questionable_id' => 11,
                'questionable_type' => 'App\\Project',
            ),
            29 => 
            array (
                'id_old' => 199932,
                'question_text' => 'If Student Services Fee monies were not available what would happen to the size and scope of the student employee group in the Recreation Center?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:12:02.000000',
                'created_at' => '2010-02-02 20:12:02.000000',
                'questionable_id' => 31,
                'questionable_type' => 'App\\Project',
            ),
            30 => 
            array (
                'id_old' => 199961,
                'question_text' => 'If Student Services Fee monies were not available what would the affect of NOT having these positions filled be on the student body at large?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:01:41.000000',
                'created_at' => '2010-02-09 19:01:41.000000',
                'questionable_id' => 21,
                'questionable_type' => 'App\\Project',
            ),
            31 => 
            array (
                'id_old' => 199960,
                'question_text' => 'Are these Graduate Assistant positions already existent within CSIL or will they be new positions? If these positions already exist does the Student Services Fee currently support them?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:01:18.000000',
                'created_at' => '2010-02-09 19:01:18.000000',
                'questionable_id' => 21,
                'questionable_type' => 'App\\Project',
            ),
            32 => 
            array (
                'id_old' => 199935,
                'question_text' => 'Are these Graduate Assistant positions already existent within CSIL or will they be new positions? If these positions already exist does the Student Services Fee currently support them?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:21:57.000000',
                'created_at' => '2010-02-02 20:21:57.000000',
                'questionable_id' => 21,
                'questionable_type' => 'App\\Project',
            ),
            33 => 
            array (
                'id_old' => 199936,
                'question_text' => 'If Student Services Fee monies were not available what would the affect of NOT having these positions filled be on the student body at large?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:22:20.000000',
                'created_at' => '2010-02-02 20:22:20.000000',
                'questionable_id' => 21,
                'questionable_type' => 'App\\Project',
            ),
            34 => 
            array (
                'id_old' => 199937,
                'question_text' => 'Who administers these applications and how are they administered?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:25:53.000000',
                'created_at' => '2010-02-02 20:25:53.000000',
                'questionable_id' => 25,
                'questionable_type' => 'App\\Project',
            ),
            35 => 
            array (
                'id_old' => 199938,
                'question_text' => 'The application indicates a request of $50,000.00 from the Student Services Fee however the attached budget only accounts for $7,600.00. Please explain the difference in budgeted numbers.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:27:31.000000',
                'created_at' => '2010-02-02 20:27:31.000000',
                'questionable_id' => 28,
                'questionable_type' => 'App\\Project',
            ),
            36 => 
            array (
                'id_old' => 199939,
                'question_text' => 'Approximately, how many students will be impacted by the Friday Night Live events?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:28:00.000000',
                'created_at' => '2010-02-02 20:28:00.000000',
                'questionable_id' => 28,
                'questionable_type' => 'App\\Project',
            ),
            37 => 
            array (
                'id_old' => 199940,
                'question_text' => 'If possible, please indicate how many Friday Night Live! events will be hosted throughout the 2010-2011 academic year.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:28:50.000000',
                'created_at' => '2010-02-02 20:28:50.000000',
                'questionable_id' => 28,
                'questionable_type' => 'App\\Project',
            ),
            38 => 
            array (
                'id_old' => 199941,
                'question_text' => 'Are these services offered elsewhere on campus?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:30:50.000000',
                'created_at' => '2010-02-02 20:30:50.000000',
                'questionable_id' => 32,
                'questionable_type' => 'App\\Project',
            ),
            39 => 
            array (
                'id_old' => 199942,
                'question_text' => 'Please give concrete examples of \'incentives\' and \'subsidies\'. Why are both necessary for the success of the program?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:31:52.000000',
                'created_at' => '2010-02-02 20:31:52.000000',
                'questionable_id' => 32,
                'questionable_type' => 'App\\Project',
            ),
            40 => 
            array (
                'id_old' => 199943,
                'question_text' => 'What are the direct benefits to the campus leaders, how will these programs enable them to be better leaders?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:32:47.000000',
                'created_at' => '2010-02-02 20:32:47.000000',
                'questionable_id' => 32,
                'questionable_type' => 'App\\Project',
            ),
            41 => 
            array (
                'id_old' => 199944,
                'question_text' => 'In the budge a nutritionist was incorporated, will this be the same nutritionist hired at Campus Recreation?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:33:23.000000',
                'created_at' => '2010-02-02 20:33:23.000000',
                'questionable_id' => 32,
                'questionable_type' => 'App\\Project',
            ),
            42 => 
            array (
                'id_old' => 199945,
                'question_text' => 'The itemized budget has a line which reads "Incentives for Student Org Leaders" which accounts for 75 students at $50.00 per student. When totaled this number should be $3,750.00 and not the $3,375.00 amount entered into the budget. Was the request of this lower figure intentional?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:38:24.000000',
                'created_at' => '2010-02-02 20:38:24.000000',
                'questionable_id' => 32,
                'questionable_type' => 'App\\Project',
            ),
            43 => 
            array (
                'id_old' => 199946,
                'question_text' => 'If the Hunter White Foundation is supporting TIPS Training with $825.00 why is it being incorporated into the total budget request?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:40:56.000000',
                'created_at' => '2010-02-02 20:40:56.000000',
                'questionable_id' => 20,
                'questionable_type' => 'App\\Project',
            ),
            44 => 
            array (
                'id_old' => 199947,
                'question_text' => 'Are there any identifiable redundancies in TIPS Training when compared to other campus Alcohol Awareness Programs?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:41:54.000000',
                'created_at' => '2010-02-02 20:41:54.000000',
                'questionable_id' => 20,
                'questionable_type' => 'App\\Project',
            ),
            45 => 
            array (
                'id_old' => 199948,
                'question_text' => 'Is TIPS Training a mandatory service?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:42:15.000000',
                'created_at' => '2010-02-02 20:42:15.000000',
                'questionable_id' => 20,
                'questionable_type' => 'App\\Project',
            ),
            46 => 
            array (
                'id_old' => 199949,
                'question_text' => 'As a clarification, are Student Services Fee monies being requested for student to retake the training?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:43:00.000000',
                'created_at' => '2010-02-02 20:43:00.000000',
                'questionable_id' => 20,
                'questionable_type' => 'App\\Project',
            ),
            47 => 
            array (
                'id_old' => 199950,
                'question_text' => 'If known, please delineate the outside financial support for Campus MovieFest.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:44:40.000000',
                'created_at' => '2010-02-02 20:44:40.000000',
                'questionable_id' => 36,
                'questionable_type' => 'App\\Project',
            ),
            48 => 
            array (
                'id_old' => 199951,
                'question_text' => 'About how much financial support has been realized through sponsors?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:45:25.000000',
                'created_at' => '2010-02-02 20:45:25.000000',
                'questionable_id' => 36,
                'questionable_type' => 'App\\Project',
            ),
            49 => 
            array (
                'id_old' => 199952,
                'question_text' => 'How has this program been funded in the past?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:45:41.000000',
                'created_at' => '2010-02-02 20:45:41.000000',
                'questionable_id' => 36,
                'questionable_type' => 'App\\Project',
            ),
            50 => 
            array (
                'id_old' => 199953,
                'question_text' => 'The application indicates a request of $3,000.00 while the itemized budget shows a total figure of $5,025.00 please clarify the difference in amounts.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:47:22.000000',
                'created_at' => '2010-02-02 20:47:22.000000',
                'questionable_id' => 33,
                'questionable_type' => 'App\\Project',
            ),
            51 => 
            array (
                'id_old' => 199954,
            'question_text' => 'For the Student Assistance & Advocacy proposal $53,442.00 was requested from the Student Services Fee - the itemized budget matches this. However, the total of the items in the budget ($48,374.00 in wages  + $10,608.00 in ERE + $4,000.00 in costs) yields $62,982.00. Please clarify this difference.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:54:27.000000',
                'created_at' => '2010-02-02 20:54:27.000000',
                'questionable_id' => 24,
                'questionable_type' => 'App\\Project',
            ),
            52 => 
            array (
                'id_old' => 199955,
                'question_text' => 'Please delineate any plans to make this a long-term, sustainable project.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:55:43.000000',
                'created_at' => '2010-02-02 20:55:43.000000',
                'questionable_id' => 24,
                'questionable_type' => 'App\\Project',
            ),
            53 => 
            array (
                'id_old' => 199956,
                'question_text' => 'Please highlight the benefits this program has/will have on Graduate and International students.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:57:33.000000',
                'created_at' => '2010-02-02 20:57:33.000000',
                'questionable_id' => 34,
                'questionable_type' => 'App\\Project',
            ),
            54 => 
            array (
                'id_old' => 199957,
                'question_text' => 'If these workshops are being used as a sanction will students be charged a fee for participating in them?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:58:36.000000',
                'created_at' => '2010-02-02 20:58:36.000000',
                'questionable_id' => 35,
                'questionable_type' => 'App\\Project',
            ),
            55 => 
            array (
                'id_old' => 199958,
                'question_text' => 'Are there any obstacles in combining the 2 proposals "Personal Responsibility Workshops" with "Student Ethics and Integrity"? If so please describe them.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 20:59:55.000000',
                'created_at' => '2010-02-02 20:59:55.000000',
                'questionable_id' => 35,
                'questionable_type' => 'App\\Project',
            ),
            56 => 
            array (
                'id_old' => 199959,
                'question_text' => 'Are there plans for securing on-going funding for this program outside of the Student Services Fee? If so, please explain.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-06 21:01:22.000000',
                'created_at' => '2010-02-02 21:01:22.000000',
                'questionable_id' => 37,
                'questionable_type' => 'App\\Project',
            ),
            57 => 
            array (
                'id_old' => 199962,
                'question_text' => 'The application indicates a request of $50,000.00 from the Student Services Fee however the attached budget only accounts for $7,600.00. Please explain the difference in budgeted numbers.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:02:04.000000',
                'created_at' => '2010-02-09 19:02:04.000000',
                'questionable_id' => 28,
                'questionable_type' => 'App\\Project',
            ),
            58 => 
            array (
                'id_old' => 199963,
                'question_text' => 'Approximately, how many students will be impacted by the Friday Night Live events?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:02:17.000000',
                'created_at' => '2010-02-09 19:02:17.000000',
                'questionable_id' => 28,
                'questionable_type' => 'App\\Project',
            ),
            59 => 
            array (
                'id_old' => 199964,
                'question_text' => 'If possible, please indicate how many Friday Night Live! events will be hosted throughout the 2010-2011 academic year.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:02:33.000000',
                'created_at' => '2010-02-09 19:02:33.000000',
                'questionable_id' => 28,
                'questionable_type' => 'App\\Project',
            ),
            60 => 
            array (
                'id_old' => 199965,
                'question_text' => 'Are these services offered elsewhere on campus?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:02:54.000000',
                'created_at' => '2010-02-09 19:02:54.000000',
                'questionable_id' => 32,
                'questionable_type' => 'App\\Project',
            ),
            61 => 
            array (
                'id_old' => 199966,
                'question_text' => 'Please give concrete examples of \'incentives\' and \'subsidies\'. Why are both necessary for the success of the program?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:03:08.000000',
                'created_at' => '2010-02-09 19:03:08.000000',
                'questionable_id' => 32,
                'questionable_type' => 'App\\Project',
            ),
            62 => 
            array (
                'id_old' => 199967,
                'question_text' => 'What are the direct benefits to the campus leaders, how will these programs enable them to be better leaders?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:03:21.000000',
                'created_at' => '2010-02-09 19:03:21.000000',
                'questionable_id' => 32,
                'questionable_type' => 'App\\Project',
            ),
            63 => 
            array (
                'id_old' => 199968,
                'question_text' => 'In the budge a nutritionist was incorporated, will this be the same nutritionist hired at Campus Recreation?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:03:38.000000',
                'created_at' => '2010-02-09 19:03:38.000000',
                'questionable_id' => 32,
                'questionable_type' => 'App\\Project',
            ),
            64 => 
            array (
                'id_old' => 199969,
                'question_text' => 'The itemized budget has a line which reads "Incentives for Student Org Leaders" which accounts for 75 students at $50.00 per student. When totaled this number should be $3,750.00 and not the $3,375.00 amount entered into the budget. Was the request of this lower figure intentional?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:04:01.000000',
                'created_at' => '2010-02-09 19:04:01.000000',
                'questionable_id' => 32,
                'questionable_type' => 'App\\Project',
            ),
            65 => 
            array (
                'id_old' => 199970,
                'question_text' => 'If known, please delineate the outside financial support for Campus MovieFest.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:04:18.000000',
                'created_at' => '2010-02-09 19:04:18.000000',
                'questionable_id' => 36,
                'questionable_type' => 'App\\Project',
            ),
            66 => 
            array (
                'id_old' => 199971,
                'question_text' => 'About how much financial support has been realized through sponsors?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:04:29.000000',
                'created_at' => '2010-02-09 19:04:29.000000',
                'questionable_id' => 36,
                'questionable_type' => 'App\\Project',
            ),
            67 => 
            array (
                'id_old' => 199972,
                'question_text' => 'How has this program been funded in the past?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:04:41.000000',
                'created_at' => '2010-02-09 19:04:41.000000',
                'questionable_id' => 36,
                'questionable_type' => 'App\\Project',
            ),
            68 => 
            array (
                'id_old' => 199973,
                'question_text' => 'The Operation Rescue application looks to create a few new positions. Please list the critical job responsibilities for each position.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:06:51.000000',
                'created_at' => '2010-02-09 19:06:51.000000',
                'questionable_id' => 18,
                'questionable_type' => 'App\\Project',
            ),
            69 => 
            array (
                'id_old' => 199974,
                'question_text' => 'What is the current Associate Director Salary?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:07:29.000000',
                'created_at' => '2010-02-09 19:07:29.000000',
                'questionable_id' => 18,
                'questionable_type' => 'App\\Project',
            ),
            70 => 
            array (
                'id_old' => 199975,
            'question_text' => 'What kind of student employees is Operation Rescue looking to hire? (ie graduate students, undergraduate students, etc...)',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:08:21.000000',
                'created_at' => '2010-02-09 19:08:21.000000',
                'questionable_id' => 18,
                'questionable_type' => 'App\\Project',
            ),
            71 => 
            array (
                'id_old' => 199976,
                'question_text' => 'Please describe the potential expenses which form the \'marketing\' line-item of the budget.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:08:58.000000',
                'created_at' => '2010-02-09 19:08:58.000000',
                'questionable_id' => 18,
                'questionable_type' => 'App\\Project',
            ),
            72 => 
            array (
                'id_old' => 199977,
                'question_text' => 'Please list the items from the budget which are one-time expenses, needing funding for only the first year.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:10:34.000000',
                'created_at' => '2010-02-09 19:10:34.000000',
                'questionable_id' => 1,
                'questionable_type' => 'App\\Project',
            ),
            73 => 
            array (
                'id_old' => 199978,
                'question_text' => 'Please explain the specifics of the \'database development\' line-item in the budget.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:11:55.000000',
                'created_at' => '2010-02-09 19:11:55.000000',
                'questionable_id' => 1,
                'questionable_type' => 'App\\Project',
            ),
            74 => 
            array (
                'id_old' => 199979,
                'question_text' => 'Please explain how the proposed marketing team will have a direct impact on the entire student body.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:12:58.000000',
                'created_at' => '2010-02-09 19:12:58.000000',
                'questionable_id' => 12,
                'questionable_type' => 'App\\Project',
            ),
            75 => 
            array (
                'id_old' => 199980,
                'question_text' => 'What is the current level of demand for Student Affairs Marketing Services?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:13:35.000000',
                'created_at' => '2010-02-09 19:13:35.000000',
                'questionable_id' => 12,
                'questionable_type' => 'App\\Project',
            ),
            76 => 
            array (
                'id_old' => 199981,
                'question_text' => 'What is the marginal increase in revenue, profit, and customer count from the Savy Student Discount when compared to historical Wednesday data?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:16:13.000000',
                'created_at' => '2010-02-09 19:16:13.000000',
                'questionable_id' => 23,
                'questionable_type' => 'App\\Project',
            ),
            77 => 
            array (
                'id_old' => 199982,
                'question_text' => 'How were the redemption estimates calculated for the 2010-2011 academic year?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:17:05.000000',
                'created_at' => '2010-02-09 19:17:05.000000',
                'questionable_id' => 23,
                'questionable_type' => 'App\\Project',
            ),
            78 => 
            array (
                'id_old' => 199983,
                'question_text' => 'What was the feedback from staff that underwent the YOUnion Aid program in previous semesters?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:17:56.000000',
                'created_at' => '2010-02-09 19:17:56.000000',
                'questionable_id' => 30,
                'questionable_type' => 'App\\Project',
            ),
            79 => 
            array (
                'id_old' => 199984,
                'question_text' => 'Currently, does the Professional Internship Program have a director? If so,what will the benefit of hiring a graduate assistant to assist in directing the program be?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:21:06.000000',
                'created_at' => '2010-02-09 19:21:06.000000',
                'questionable_id' => 29,
                'questionable_type' => 'App\\Project',
            ),
            80 => 
            array (
                'id_old' => 199985,
                'question_text' => 'From data in recent years, please describe the effectiveness of the program as reported by the participants.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:22:35.000000',
                'created_at' => '2010-02-09 19:22:35.000000',
                'questionable_id' => 29,
                'questionable_type' => 'App\\Project',
            ),
            81 => 
            array (
                'id_old' => 199986,
                'question_text' => 'The application asks for the Student Services Fee to support wages at the THINK TANK, please prioritize the sections of the submitted budget from \'highest priority\' to \'least priority\'.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:26:39.000000',
                'created_at' => '2010-02-09 19:26:39.000000',
                'questionable_id' => 8,
                'questionable_type' => 'App\\Project',
            ),
            82 => 
            array (
                'id_old' => 199987,
                'question_text' => 'If available, please briefly describe the student response to the pilot program of Student Support Cohorts.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:28:26.000000',
                'created_at' => '2010-02-09 19:28:26.000000',
                'questionable_id' => 10,
                'questionable_type' => 'App\\Project',
            ),
            83 => 
            array (
                'id_old' => 199988,
                'question_text' => 'Does the application ask for the full $12,000.00 from the Student Services Fee or for $6,000.00 in order to cover the difference between what UA South currently pays and the needed level of funding?',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-13 19:31:41.000000',
                'created_at' => '2010-02-09 19:31:41.000000',
                'questionable_id' => 4,
                'questionable_type' => 'App\\Project',
            ),
            84 => 
            array (
                'id_old' => 199989,
                'question_text' => 'The itemized budget shows projected advertising revenues as $285,000.00 while estimated printing costs as $340,000.00. Also, $100,000.00 is being requested of the Student Services Fee. When added together $285,000.00 and $100,000.00 equal $385,000.00. This will leave $45,000.00 unaccounted by the budget. Is the budget incorrect? If so, please explain the error. If not, please explain the intended uses of the surplus $45,000.00.',
                'netid' => 'mrtotlis',
                'deadline' => '2010-02-15 22:15:12.000000',
                'created_at' => '2010-02-11 22:15:12.000000',
                'questionable_id' => 3,
                'questionable_type' => 'App\\Project',
            ),
            85 => 
            array (
                'id_old' => 199992,
            'question_text' => 'How many people have completed the tracks (specifically compared to how many people entered the program)?',
                'netid' => 'altomare',
                'deadline' => '2011-01-26 03:54:09.000000',
                'created_at' => '2011-01-22 03:54:09.000000',
                'questionable_id' => 43,
                'questionable_type' => 'App\\Project',
            ),
            86 => 
            array (
                'id_old' => 199993,
                'question_text' => 'Will the requested Graduate Assistants work to expand the program to reach more students or will they distribute the same work load among them?',
                'netid' => 'altomare',
                'deadline' => '2011-01-26 04:00:20.000000',
                'created_at' => '2011-01-22 04:00:20.000000',
                'questionable_id' => 43,
                'questionable_type' => 'App\\Project',
            ),
            87 => 
            array (
                'id_old' => 199994,
                'question_text' => 'What is your average incoming and outgoing call volume during peak and non-peak hours? What is your average instant messaging volume during peak and non-peak hours? What is your average walk-in volume during peak and non-peak hours?',
                'netid' => 'altomare',
                'deadline' => '2011-01-26 04:08:22.000000',
                'created_at' => '2011-01-22 04:08:22.000000',
                'questionable_id' => 55,
                'questionable_type' => 'App\\Project',
            ),
            88 => 
            array (
                'id_old' => 199995,
                'question_text' => 'What is your average hold time for students on the phone?',
                'netid' => 'altomare',
                'deadline' => '2011-01-26 04:09:52.000000',
                'created_at' => '2011-01-22 04:09:52.000000',
                'questionable_id' => 55,
                'questionable_type' => 'App\\Project',
            ),
            89 => 
            array (
                'id_old' => 199996,
                'question_text' => 'Can you please clarify how you calculated 250 staff hours per week in your budget?',
                'netid' => 'altomare',
                'deadline' => '2011-01-26 04:11:48.000000',
                'created_at' => '2011-01-22 04:11:48.000000',
                'questionable_id' => 55,
                'questionable_type' => 'App\\Project',
            ),
            90 => 
            array (
                'id_old' => 199997,
                'question_text' => 'Can you please clarify whether or not the current application will support non-Arizona Assurance students for the 2011-2012 school year?',
                'netid' => 'altomare',
                'deadline' => '2011-01-26 04:20:05.000000',
                'created_at' => '2011-01-22 04:20:05.000000',
                'questionable_id' => 45,
                'questionable_type' => 'App\\Project',
            ),
            91 => 
            array (
                'id_old' => 199998,
            'question_text' => 'What is the nature of the student-faculty interactions according to the current application(will they serve as mentors, advisors, etc.)?',
                'netid' => 'altomare',
                'deadline' => '2011-01-26 04:24:32.000000',
                'created_at' => '2011-01-22 04:24:32.000000',
                'questionable_id' => 45,
                'questionable_type' => 'App\\Project',
            ),
            92 => 
            array (
                'id_old' => 199999,
                'question_text' => 'If you receive full funding, on average how many students will each peer mentor have?  How many total students will you expect to participate for the 2011-2012 school year?',
                'netid' => 'altomare',
                'deadline' => '2011-01-26 04:38:38.000000',
                'created_at' => '2011-01-22 04:38:38.000000',
                'questionable_id' => 58,
                'questionable_type' => 'App\\Project',
            ),
            93 => 
            array (
                'id_old' => 1999100,
            'question_text' => 'Who is currently providing the services outlined in the application (GAs, FTEs, etc.)?',
                'netid' => 'altomare',
                'deadline' => '2011-02-03 03:56:37.000000',
                'created_at' => '2011-01-29 03:56:37.000000',
                'questionable_id' => 50,
                'questionable_type' => 'App\\Project',
            ),
            94 => 
            array (
                'id_old' => 1999101,
                'question_text' => 'Why are you only looking at graduate students from the College of Higher Education?',
                'netid' => 'altomare',
                'deadline' => '2011-02-03 04:03:41.000000',
                'created_at' => '2011-01-29 04:03:41.000000',
                'questionable_id' => 50,
                'questionable_type' => 'App\\Project',
            ),
            95 => 
            array (
                'id_old' => 1999102,
            'question_text' => 'Can you please give a brief description of the work that the graduate students will be doing as described in the application (e.g. at the Games Room, Gallagher Theater, etc.)?',
                'netid' => 'altomare',
                'deadline' => '2011-02-03 04:10:37.000000',
                'created_at' => '2011-01-29 04:10:37.000000',
                'questionable_id' => 51,
                'questionable_type' => 'App\\Project',
            ),
            96 => 
            array (
                'id_old' => 1999103,
                'question_text' => 'Have you sought out additional sources of funding for this program?  If yes, please elaborate.',
                'netid' => 'altomare',
                'deadline' => '2011-02-03 04:17:27.000000',
                'created_at' => '2011-01-29 04:17:27.000000',
                'questionable_id' => 51,
                'questionable_type' => 'App\\Project',
            ),
            97 => 
            array (
                'id_old' => 1999104,
                'question_text' => 'Have you sought out additional sources of funding for this program? If yes, please elaborate.',
                'netid' => 'altomare',
                'deadline' => '2011-02-03 04:19:17.000000',
                'created_at' => '2011-01-29 04:19:17.000000',
                'questionable_id' => 51,
                'questionable_type' => 'App\\Project',
            ),
            98 => 
            array (
                'id_old' => 1999105,
                'question_text' => 'What is the potential impact the graduate assistant will have on the general student body outside of the forty-three professional intern student employees?',
                'netid' => 'altomare',
                'deadline' => '2011-02-03 04:28:02.000000',
                'created_at' => '2011-01-29 04:28:02.000000',
                'questionable_id' => 53,
                'questionable_type' => 'App\\Project',
            ),
            99 => 
            array (
                'id_old' => 1999106,
                'question_text' => 'Have you sought out additional sources of funding for this program? If yes, please elaborate.',
                'netid' => 'altomare',
                'deadline' => '2011-02-03 04:28:58.000000',
                'created_at' => '2011-01-29 04:28:58.000000',
                'questionable_id' => 53,
                'questionable_type' => 'App\\Project',
            ),
            100 => 
            array (
                'id_old' => 1999107,
                'question_text' => 'Can you please include a detailed budget of the proposed programs in the current application?',
                'netid' => 'altomare',
                'deadline' => '2011-02-03 04:34:20.000000',
                'created_at' => '2011-01-29 04:34:20.000000',
                'questionable_id' => 58,
                'questionable_type' => 'App\\Project',
            ),
            101 => 
            array (
                'id_old' => 1999108,
                'question_text' => 'How and why did you determine that 20 additional students was the optimal number of Student Transitions student assistants?',
                'netid' => 'altomare',
                'deadline' => '2011-02-03 05:11:34.000000',
                'created_at' => '2011-01-29 05:11:34.000000',
                'questionable_id' => 44,
                'questionable_type' => 'App\\Project',
            ),
            102 => 
            array (
                'id_old' => 1999109,
                'question_text' => 'How accessible is this program going to be to students outside of Eller?',
                'netid' => 'altomare',
                'deadline' => '2011-02-12 03:17:15.000000',
                'created_at' => '2011-02-05 03:17:15.000000',
                'questionable_id' => 46,
                'questionable_type' => 'App\\Project',
            ),
            103 => 
            array (
                'id_old' => 1999110,
                'question_text' => 'On average, how many papers are collected at the end of the day?',
                'netid' => 'altomare',
                'deadline' => '2011-02-12 03:22:50.000000',
                'created_at' => '2011-02-05 03:22:50.000000',
                'questionable_id' => 54,
                'questionable_type' => 'App\\Project',
            ),
            104 => 
            array (
                'id_old' => 1999111,
                'question_text' => 'How will the Marketing Team\'s time and resources be allocated in regards to the print version of the Daily Wildcat and the online version of the Daily Wildcat?',
                'netid' => 'altomare',
                'deadline' => '2011-02-12 03:27:57.000000',
                'created_at' => '2011-02-05 03:27:57.000000',
                'questionable_id' => 54,
                'questionable_type' => 'App\\Project',
            ),
            105 => 
            array (
                'id_old' => 1999112,
                'question_text' => 'If we fund the Wildcat Printing Expense as requested, will the Daily Wildcat circulation be maintained, decreased or increased from the current academic year?',
                'netid' => 'altomare',
                'deadline' => '2011-02-12 03:36:57.000000',
                'created_at' => '2011-02-05 03:36:57.000000',
                'questionable_id' => 54,
                'questionable_type' => 'App\\Project',
            ),
            106 => 
            array (
                'id_old' => 1999113,
                'question_text' => 'What is the relationship between this application and the Graduate Assistantship Opportunities application? For example, if we fund one application but not the other, how will this affect the workload, duties, etc. of CSIL employees?',
                'netid' => 'altomare',
                'deadline' => '2011-02-12 03:50:44.000000',
                'created_at' => '2011-02-05 03:50:44.000000',
                'questionable_id' => 49,
                'questionable_type' => 'App\\Project',
            ),
            107 => 
            array (
                'id_old' => 1999114,
            'question_text' => 'On average, how many students use the SUMC Game Room per week during normal hours(10am to 12am)and during late night hours (12am to 2am)?',
                'netid' => 'altomare',
                'deadline' => '2011-02-12 03:55:30.000000',
                'created_at' => '2011-02-05 03:55:30.000000',
                'questionable_id' => 52,
                'questionable_type' => 'App\\Project',
            ),
            108 => 
            array (
                'id_old' => 1999115,
                'question_text' => 'On average, how many students use the CODE gaming lounge per week?',
                'netid' => 'altomare',
                'deadline' => '2011-02-12 03:56:38.000000',
                'created_at' => '2011-02-05 03:56:38.000000',
                'questionable_id' => 52,
                'questionable_type' => 'App\\Project',
            ),
            109 => 
            array (
                'id_old' => 1999116,
                'question_text' => 'We cannot award funding until July 1st, 2011, how will this impact your program and your budget?',
                'netid' => 'altomare',
                'deadline' => '2011-02-19 02:57:39.000000',
                'created_at' => '2011-02-12 02:57:39.000000',
                'questionable_id' => 60,
                'questionable_type' => 'App\\Project',
            ),
            110 => 
            array (
                'id_old' => 1999117,
                'question_text' => 'How did you come to the conclusion that seven employees would meet the demand?',
                'netid' => 'altomare',
                'deadline' => '2011-02-19 02:59:17.000000',
                'created_at' => '2011-02-12 02:59:17.000000',
                'questionable_id' => 60,
                'questionable_type' => 'App\\Project',
            ),
            111 => 
            array (
                'id_old' => 1999118,
                'question_text' => 'What is the total cost of maintaining Disabled Cart services, and how much of this funding does PTS provide?',
                'netid' => 'altomare',
                'deadline' => '2011-02-19 03:06:06.000000',
                'created_at' => '2011-02-12 03:06:06.000000',
                'questionable_id' => 56,
                'questionable_type' => 'App\\Project',
            ),
            112 => 
            array (
                'id_old' => 1999119,
                'question_text' => 'Are you seeking outside sources of funding? If so, from where?',
                'netid' => 'altomare',
                'deadline' => '2011-02-19 03:07:36.000000',
                'created_at' => '2011-02-12 03:07:36.000000',
                'questionable_id' => 56,
                'questionable_type' => 'App\\Project',
            ),
            113 => 
            array (
                'id_old' => 1999120,
                'question_text' => 'Can you please clarify the discrepancy between the 10 vehicles and 11 vehicle insurance policies requested in your budget?',
                'netid' => 'altomare',
                'deadline' => '2011-02-19 03:10:09.000000',
                'created_at' => '2011-02-12 03:10:09.000000',
                'questionable_id' => 57,
                'questionable_type' => 'App\\Project',
            ),
            114 => 
            array (
                'id_old' => 1999121,
            'question_text' => 'Can you please clarify the new vehicle line item?  Are you requesting 2 new vehicles every year for 3 years (a total of 6 new vehicles), or are you requesting a one time purchase of 2 new vehicles over the course of 3 years?',
                'netid' => 'altomare',
                'deadline' => '2011-02-19 03:13:44.000000',
                'created_at' => '2011-02-12 03:13:44.000000',
                'questionable_id' => 57,
                'questionable_type' => 'App\\Project',
            ),
            115 => 
            array (
                'id_old' => 1999122,
                'question_text' => 'Are you seeking outside sources of funding? If so, from where?',
                'netid' => 'altomare',
                'deadline' => '2011-02-19 03:14:12.000000',
                'created_at' => '2011-02-12 03:14:12.000000',
                'questionable_id' => 57,
                'questionable_type' => 'App\\Project',
            ),
            116 => 
            array (
                'id_old' => 1999123,
                'question_text' => 'Can you please define the nature of the campus security officers? Will they carry weapons, are they certified UAPD officers or peace officers, and will they have the authority to issue tickets or make arrests?',
                'netid' => 'altomare',
                'deadline' => '2011-02-19 03:25:40.000000',
                'created_at' => '2011-02-12 03:25:40.000000',
                'questionable_id' => 40,
                'questionable_type' => 'App\\Project',
            ),
            117 => 
            array (
                'id_old' => 1999124,
                'question_text' => 'If this program is successful, will it become a permanent part of UAPD or Residence Life\'s budget?',
                'netid' => 'altomare',
                'deadline' => '2011-02-19 03:28:32.000000',
                'created_at' => '2011-02-12 03:28:32.000000',
                'questionable_id' => 40,
                'questionable_type' => 'App\\Project',
            ),
            118 => 
            array (
                'id_old' => 1999125,
                'question_text' => 'Due to technical errors, these applicants have encountered difficulties resulting in late submissions. The Board recognizes these issues and granted them an extension to accommodate for these circumstances.  

What is your average incoming and outgoing call volume during peak and non-peak hours? What is your average instant messaging volume during peak and non-peak hours? What is your average walk-in volume during peak and non-peak hours?',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 16:55:02.000000',
                'created_at' => '2011-02-15 16:55:02.000000',
                'questionable_id' => 55,
                'questionable_type' => 'App\\Project',
            ),
            119 => 
            array (
                'id_old' => 1999126,
                'question_text' => 'Due to technical errors, these applicants have encountered difficulties resulting in late submissions. The Board recognizes these issues and granted them an extension to accommodate for these circumstances.  

What is your average hold time for students on the phone?',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 16:55:41.000000',
                'created_at' => '2011-02-15 16:55:41.000000',
                'questionable_id' => 55,
                'questionable_type' => 'App\\Project',
            ),
            120 => 
            array (
                'id_old' => 1999127,
                'question_text' => 'Due to technical errors, these applicants have encountered difficulties resulting in late submissions. The Board recognizes these issues and granted them an extension to accommodate for these circumstances.  

Can you please clarify how you calculated 250 staff hours per week in your budget?',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 16:56:20.000000',
                'created_at' => '2011-02-15 16:56:20.000000',
                'questionable_id' => 55,
                'questionable_type' => 'App\\Project',
            ),
            121 => 
            array (
                'id_old' => 1999128,
                'question_text' => 'Due to technical errors, these applicants have encountered difficulties resulting in late submissions. The Board recognizes these issues and granted them an extension to accommodate for these circumstances.  

If you receive full funding, on average how many students will each peer mentor have? How many total students will you expect to participate for the 2011-2012 school year?',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 16:57:23.000000',
                'created_at' => '2011-02-15 16:57:23.000000',
                'questionable_id' => 58,
                'questionable_type' => 'App\\Project',
            ),
            122 => 
            array (
                'id_old' => 1999129,
                'question_text' => 'Due to technical errors, these applicants have encountered difficulties resulting in late submissions. The Board recognizes these issues and granted them an extension to accommodate for these circumstances.  

Can you please include a detailed budget of the proposed programs in the current application?',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 16:58:04.000000',
                'created_at' => '2011-02-15 16:58:04.000000',
                'questionable_id' => 58,
                'questionable_type' => 'App\\Project',
            ),
            123 => 
            array (
                'id_old' => 1999130,
                'question_text' => 'Can you please include a detailed budget of the proposed programs in the current application?',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 16:58:33.000000',
                'created_at' => '2011-02-15 16:58:33.000000',
                'questionable_id' => 47,
                'questionable_type' => 'App\\Project',
            ),
            124 => 
            array (
                'id_old' => 1999131,
                'question_text' => 'Due to technical errors, these applicants have encountered difficulties resulting in late submissions. The Board recognizes these issues and granted them an extension to accommodate for these circumstances.  

How many people have completed the tracks (specifically compared to how many people entered the program)?',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 16:59:34.000000',
                'created_at' => '2011-02-15 16:59:34.000000',
                'questionable_id' => 43,
                'questionable_type' => 'App\\Project',
            ),
            125 => 
            array (
                'id_old' => 1999132,
                'question_text' => 'Due to technical errors, these applicants have encountered difficulties resulting in late submissions. The Board recognizes these issues and granted them an extension to accommodate for these circumstances.  

Will the requested Graduate Assistants work to expand the program to reach more students or will they distribute the same work load among them?',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 17:00:01.000000',
                'created_at' => '2011-02-15 17:00:01.000000',
                'questionable_id' => 43,
                'questionable_type' => 'App\\Project',
            ),
            126 => 
            array (
                'id_old' => 1999133,
                'question_text' => 'Due to technical errors, these applicants have encountered difficulties resulting in late submissions. The Board recognizes these issues and granted them an extension to accommodate for these circumstances.  

What is the potential impact the graduate assistant will have on the general student body outside of the forty-three professional intern student employees?',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 17:01:00.000000',
                'created_at' => '2011-02-15 17:01:00.000000',
                'questionable_id' => 53,
                'questionable_type' => 'App\\Project',
            ),
            127 => 
            array (
                'id_old' => 1999134,
                'question_text' => 'Due to technical errors, these applicants have encountered difficulties resulting in late submissions. The Board recognizes these issues and granted them an extension to accommodate for these circumstances.  

Have you sought out additional sources of funding for this program? If yes, please elaborate.',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 17:01:30.000000',
                'created_at' => '2011-02-15 17:01:30.000000',
                'questionable_id' => 53,
                'questionable_type' => 'App\\Project',
            ),
            128 => 
            array (
                'id_old' => 1999135,
                'question_text' => 'Due to technical errors, these applicants have encountered difficulties resulting in late submissions. The Board recognizes these issues and granted them an extension to accommodate for these circumstances.  

Can you please give a brief description of the work that the graduate students will be doing as described in the application (e.g. at the Games Room, Gallagher Theater, etc.)?',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 17:02:39.000000',
                'created_at' => '2011-02-15 17:02:39.000000',
                'questionable_id' => 51,
                'questionable_type' => 'App\\Project',
            ),
            129 => 
            array (
                'id_old' => 1999136,
                'question_text' => 'Due to technical errors, these applicants have encountered difficulties resulting in late submissions. The Board recognizes these issues and granted them an extension to accommodate for these circumstances.  

Have you sought out additional sources of funding for this program? If yes, please elaborate.',
                'netid' => 'altomare',
                'deadline' => '2011-02-22 17:03:14.000000',
                'created_at' => '2011-02-15 17:03:14.000000',
                'questionable_id' => 51,
                'questionable_type' => 'App\\Project',
            ),
            130 => 
            array (
                'id_old' => 1999137,
                'question_text' => 'Are there plans to expand the Savvy Student Discount to any new locations north of Speedway?',
                'netid' => 'altomare',
                'deadline' => '2011-02-26 03:02:51.000000',
                'created_at' => '2011-02-19 03:02:51.000000',
                'questionable_id' => 59,
                'questionable_type' => 'App\\Project',
            ),
            131 => 
            array (
                'id_old' => 1999138,
                'question_text' => 'Are you taking steps to ensure that only University of Arizona students are using the Savvy Student Discount?',
                'netid' => 'altomare',
                'deadline' => '2011-02-26 03:06:25.000000',
                'created_at' => '2011-02-19 03:06:25.000000',
                'questionable_id' => 59,
                'questionable_type' => 'App\\Project',
            ),
            132 => 
            array (
                'id_old' => 1999139,
                'question_text' => 'In your application you mentioned "expanding the program to include at least one weekly dinner hour."  Can you please expand upon this and provide an alternate budget for this funding option?',
                'netid' => 'altomare',
                'deadline' => '2011-02-26 03:18:45.000000',
                'created_at' => '2011-02-19 03:18:45.000000',
                'questionable_id' => 59,
                'questionable_type' => 'App\\Project',
            ),
            133 => 
            array (
                'id_old' => 1999140,
                'question_text' => 'Can you please clarify the budget you provided? Is ASUA guaranteeing $138,575.76 to club funding for the Fall 2011-Spring 2012 academic year?',
                'netid' => 'altomare',
                'deadline' => '2011-02-26 03:24:39.000000',
                'created_at' => '2011-02-19 03:24:39.000000',
                'questionable_id' => 61,
                'questionable_type' => 'App\\Project',
            ),
            134 => 
            array (
                'id_old' => 1999141,
                'question_text' => 'Can you please provide a job description for the student assistant? Is this the Legal Adviser\'s personal assistant?',
                'netid' => 'altomare',
                'deadline' => '2011-02-26 03:41:29.000000',
                'created_at' => '2011-02-19 03:41:29.000000',
                'questionable_id' => 39,
                'questionable_type' => 'App\\Project',
            ),
            135 => 
            array (
                'id_old' => 1999142,
                'question_text' => 'Will there be a workshop or follow up event where students can apply their new knowledge obtained from the Million Dollar Scholar event?',
                'netid' => 'altomare',
                'deadline' => '2011-02-26 03:44:41.000000',
                'created_at' => '2011-02-19 03:44:41.000000',
                'questionable_id' => 48,
                'questionable_type' => 'App\\Project',
            ),
            136 => 
            array (
                'id_old' => 1999143,
                'question_text' => 'Are there any plans to extend the Student Legal Services Program?',
                'netid' => 'altomare',
                'deadline' => '2011-02-26 03:51:28.000000',
                'created_at' => '2011-02-19 03:51:28.000000',
                'questionable_id' => 39,
                'questionable_type' => 'App\\Project',
            ),
            137 => 
            array (
                'id_old' => 1999144,
                'question_text' => 'Here\'s a question for PAR SF-1 for the TEST initiative.',
                'netid' => 'kmbeyer',
                'deadline' => '2011-12-05 18:16:17.000000',
                'created_at' => '2011-11-28 18:16:17.000000',
                'questionable_id' => 1,
                'questionable_type' => 'App\\Par',
            ),
            138 => 
            array (
                'id_old' => 1999145,
                'question_text' => 'Here\'s a second question...',
                'netid' => 'kmbeyer',
                'deadline' => '2011-12-05 18:19:36.000000',
                'created_at' => '2011-11-28 18:19:36.000000',
                'questionable_id' => 1,
                'questionable_type' => 'App\\Par',
            ),
            139 => 
            array (
                'id_old' => 1999146,
                'question_text' => 'This is a regular question...',
                'netid' => 'kmbeyer',
                'deadline' => '2011-12-05 19:04:36.000000',
                'created_at' => '2011-11-28 19:04:36.000000',
                'questionable_id' => 62,
                'questionable_type' => 'App\\Project',
            ),
            140 => 
            array (
                'id_old' => 1999147,
                'question_text' => 'Can you please provide a brief breakdown of the operations budget?',
                'netid' => 'altomare',
                'deadline' => '2012-01-28 01:20:21.000000',
                'created_at' => '2012-01-21 01:20:21.000000',
                'questionable_id' => 66,
                'questionable_type' => 'App\\Project',
            ),
            141 => 
            array (
                'id_old' => 1999148,
                'question_text' => 'Can you please justify why three years of funding is crucial to the success of the program as opposed to two years of funding?',
                'netid' => 'altomare',
                'deadline' => '2012-01-28 01:24:36.000000',
                'created_at' => '2012-01-21 01:24:36.000000',
                'questionable_id' => 66,
                'questionable_type' => 'App\\Project',
            ),
            142 => 
            array (
                'id_old' => 1999149,
            'question_text' => 'How many total employees were hired last year through Project Rush (ie part time employees, full time, etc.)?',
                'netid' => 'altomare',
                'deadline' => '2012-01-28 01:28:14.000000',
                'created_at' => '2012-01-21 01:28:14.000000',
                'questionable_id' => 76,
                'questionable_type' => 'App\\Project',
            ),
            143 => 
            array (
                'id_old' => 1999150,
                'question_text' => 'Can you please provide a brief breakdown of the operations budget?',
                'netid' => 'altomare',
                'deadline' => '2012-01-27 01:40:44.000000',
                'created_at' => '2012-01-21 01:40:44.000000',
                'questionable_id' => 76,
                'questionable_type' => 'App\\Project',
            ),
            144 => 
            array (
                'id_old' => 1999151,
            'question_text' => 'How will you interact with other programs (ie Think Tank-Wildcat Track, Arizona Assurance Scholars) that target students on probation?',
                'netid' => 'altomare',
                'deadline' => '2012-01-27 01:45:07.000000',
                'created_at' => '2012-01-21 01:45:07.000000',
                'questionable_id' => 71,
                'questionable_type' => 'App\\Project',
            ),
            145 => 
            array (
                'id_old' => 1999152,
                'question_text' => 'How many hours do you expect your student employees to work per week?',
                'netid' => 'altomare',
                'deadline' => '2012-01-27 01:46:07.000000',
                'created_at' => '2012-01-21 01:46:07.000000',
                'questionable_id' => 71,
                'questionable_type' => 'App\\Project',
            ),
            146 => 
            array (
                'id_old' => 1999153,
            'question_text' => 'Where would this program be housed (physical location on campus)?',
                'netid' => 'altomare',
                'deadline' => '2012-01-27 01:46:48.000000',
                'created_at' => '2012-01-21 01:46:48.000000',
                'questionable_id' => 71,
                'questionable_type' => 'App\\Project',
            ),
            147 => 
            array (
                'id_old' => 1999154,
                'question_text' => 'What is the percentage of students on probation that receive financial aid?',
                'netid' => 'altomare',
                'deadline' => '2012-01-27 01:47:38.000000',
                'created_at' => '2012-01-21 01:47:38.000000',
                'questionable_id' => 71,
                'questionable_type' => 'App\\Project',
            ),
            148 => 
            array (
                'id_old' => 1999155,
            'question_text' => 'How will you specifically measure the success of this program (ie increase average GPA by x amount, retain x% of students in the program, etc.)?',
                'netid' => 'altomare',
                'deadline' => '2012-01-27 01:51:05.000000',
                'created_at' => '2012-01-21 01:51:05.000000',
                'questionable_id' => 71,
                'questionable_type' => 'App\\Project',
            ),
            149 => 
            array (
                'id_old' => 1999156,
                'question_text' => 'How many UA students have applied for scholarships through Scholarship Universe? Of those that applied, how many were successful recipients of scholarships?',
                'netid' => 'altomare',
                'deadline' => '2012-01-27 01:53:12.000000',
                'created_at' => '2012-01-21 01:53:12.000000',
                'questionable_id' => 75,
                'questionable_type' => 'App\\Project',
            ),
            150 => 
            array (
                'id_old' => 1999157,
                'question_text' => 'How would selling this program to other colleges effect the success rate of UA students at receiving scholarships through Scholarship Universe?',
                'netid' => 'altomare',
                'deadline' => '2012-01-27 01:56:23.000000',
                'created_at' => '2012-01-21 01:56:23.000000',
                'questionable_id' => 75,
                'questionable_type' => 'App\\Project',
            ),
            151 => 
            array (
                'id_old' => 1999158,
                'question_text' => 'What is the total amount of money that students have been awarded in aggregate by using this program?',
                'netid' => 'altomare',
                'deadline' => '2012-01-27 01:57:21.000000',
                'created_at' => '2012-01-21 01:57:21.000000',
                'questionable_id' => 75,
                'questionable_type' => 'App\\Project',
            ),
            152 => 
            array (
                'id_old' => 1999159,
                'question_text' => 'What specific steps are you taking to find outside investors for this program?',
                'netid' => 'altomare',
                'deadline' => '2012-01-27 01:57:58.000000',
                'created_at' => '2012-01-21 01:57:58.000000',
                'questionable_id' => 75,
                'questionable_type' => 'App\\Project',
            ),
            153 => 
            array (
                'id_old' => 1999160,
                'question_text' => 'Can you please clarify the classified regular employee\'s salary?',
                'netid' => 'altomare',
                'deadline' => '2012-01-27 02:01:41.000000',
                'created_at' => '2012-01-21 02:01:41.000000',
                'questionable_id' => 76,
                'questionable_type' => 'App\\Project',
            ),
            154 => 
            array (
                'id_old' => 1999161,
                'question_text' => 'What is the application process for selecting executive board members?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:19:11.000000',
                'created_at' => '2012-01-28 01:19:11.000000',
                'questionable_id' => 83,
                'questionable_type' => 'App\\Project',
            ),
            155 => 
            array (
                'id_old' => 1999162,
                'question_text' => 'What steps will be taken in the future to receive outside funding?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:21:13.000000',
                'created_at' => '2012-01-28 01:21:13.000000',
                'questionable_id' => 83,
                'questionable_type' => 'App\\Project',
            ),
            156 => 
            array (
                'id_old' => 1999163,
            'question_text' => 'Will you keep any money that has been raised to help fund the program in future years (ie. if you raise $10,000 from B+ Dance Marathon will you keep $1,000 of it)?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:23:52.000000',
                'created_at' => '2012-01-28 01:23:52.000000',
                'questionable_id' => 65,
                'questionable_type' => 'App\\Project',
            ),
            157 => 
            array (
                'id_old' => 1999164,
                'question_text' => 'Can you please supply a budget breakdown for each specific area of operations?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:25:34.000000',
                'created_at' => '2012-01-28 01:25:34.000000',
                'questionable_id' => 65,
                'questionable_type' => 'App\\Project',
            ),
            158 => 
            array (
                'id_old' => 1999165,
                'question_text' => 'Can you please provide job descriptions for the site leaders?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:26:08.000000',
                'created_at' => '2012-01-28 01:26:08.000000',
                'questionable_id' => 65,
                'questionable_type' => 'App\\Project',
            ),
            159 => 
            array (
                'id_old' => 1999166,
            'question_text' => 'What is the liability to the university in funding this program (ie. does your insurance cover personal student liability, liability to the university, etc.)?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:28:55.000000',
                'created_at' => '2012-01-28 01:28:55.000000',
                'questionable_id' => 73,
                'questionable_type' => 'App\\Project',
            ),
            160 => 
            array (
                'id_old' => 1999167,
            'question_text' => 'Since the SSFAB has already funded this program through a PAR (and it included money for start-up costs), how do you justify requesting more money for equipment and other start-up needs?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:31:24.000000',
                'created_at' => '2012-01-28 01:31:24.000000',
                'questionable_id' => 73,
                'questionable_type' => 'App\\Project',
            ),
            161 => 
            array (
                'id_old' => 1999168,
                'question_text' => 'How would collaboration with supporting organizations lead to future funding?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:31:51.000000',
                'created_at' => '2012-01-28 01:31:51.000000',
                'questionable_id' => 73,
                'questionable_type' => 'App\\Project',
            ),
            162 => 
            array (
                'id_old' => 1999169,
            'question_text' => 'Can you please clarify the amount of money indicated in "Other Projected Sources of Funding" on the SSF Project Financials Page (ie are the amounts indicative of per year funding or total funding for three years)?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:35:27.000000',
                'created_at' => '2012-01-28 01:35:27.000000',
                'questionable_id' => 67,
                'questionable_type' => 'App\\Project',
            ),
            163 => 
            array (
                'id_old' => 1999170,
                'question_text' => 'How many club members do you have this year compared to last year?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:37:12.000000',
                'created_at' => '2012-01-28 01:37:12.000000',
                'questionable_id' => 67,
                'questionable_type' => 'App\\Project',
            ),
            164 => 
            array (
                'id_old' => 1999171,
                'question_text' => 'Which undergraduate and graduate initiatives in the annual Survey Results would this program support?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:43:05.000000',
                'created_at' => '2012-01-28 01:43:05.000000',
                'questionable_id' => 64,
                'questionable_type' => 'App\\Project',
            ),
            165 => 
            array (
                'id_old' => 1999172,
                'question_text' => 'How would collaboration with supporting organizations lead to future funding?',
                'netid' => 'altomare',
                'deadline' => '2012-02-03 01:43:45.000000',
                'created_at' => '2012-01-28 01:43:45.000000',
                'questionable_id' => 64,
                'questionable_type' => 'App\\Project',
            ),
            166 => 
            array (
                'id_old' => 1999173,
                'question_text' => 'Can you please provide a brief breakdown of the operations budget?',
                'netid' => 'lyamaguc',
                'deadline' => '2012-02-03 01:46:31.000000',
                'created_at' => '2012-01-28 01:46:31.000000',
                'questionable_id' => 66,
                'questionable_type' => 'App\\Project',
            ),
            167 => 
            array (
                'id_old' => 1999174,
                'question_text' => 'Can you please justify why three years of funding is crucial to the success of the program as opposed to two years of funding?',
                'netid' => 'lyamaguc',
                'deadline' => '2012-02-03 01:47:10.000000',
                'created_at' => '2012-01-28 01:47:10.000000',
                'questionable_id' => 66,
                'questionable_type' => 'App\\Project',
            ),
            168 => 
            array (
                'id_old' => 1999175,
                'question_text' => 'What are your long-term goals for this program specifically concerning employment and securing outside sources of funding?',
                'netid' => 'altomare',
                'deadline' => '2012-02-10 01:29:40.000000',
                'created_at' => '2012-02-04 01:29:40.000000',
                'questionable_id' => 80,
                'questionable_type' => 'App\\Project',
            ),
            169 => 
            array (
                'id_old' => 1999176,
                'question_text' => 'How many of the 8 graduate assistants already in the program will be retained next year?',
                'netid' => 'altomare',
                'deadline' => '2012-02-10 01:29:58.000000',
                'created_at' => '2012-02-04 01:29:58.000000',
                'questionable_id' => 80,
                'questionable_type' => 'App\\Project',
            ),
            170 => 
            array (
                'id_old' => 1999177,
                'question_text' => 'Can you please clarify if we are the only source of funding for this program?
If no, please specify the other sources of funding and where it is spent?',
                'netid' => 'altomare',
                'deadline' => '2012-02-10 01:36:00.000000',
                'created_at' => '2012-02-04 01:36:00.000000',
                'questionable_id' => 79,
                'questionable_type' => 'App\\Project',
            ),
            171 => 
            array (
                'id_old' => 1999178,
                'question_text' => 'What is the unique number of visits per month to the website?',
                'netid' => 'altomare',
                'deadline' => '2012-02-10 01:38:26.000000',
                'created_at' => '2012-02-04 01:38:26.000000',
                'questionable_id' => 74,
                'questionable_type' => 'App\\Project',
            ),
            172 => 
            array (
                'id_old' => 1999179,
                'question_text' => 'How do you plan to use marketing resources to promote online readership?',
                'netid' => 'altomare',
                'deadline' => '2012-02-10 01:40:53.000000',
                'created_at' => '2012-02-04 01:40:53.000000',
                'questionable_id' => 74,
                'questionable_type' => 'App\\Project',
            ),
            173 => 
            array (
                'id_old' => 1999180,
                'question_text' => 'How do you plan to use the additional marketing staff for next year?',
                'netid' => 'altomare',
                'deadline' => '2012-02-10 01:42:22.000000',
                'created_at' => '2012-02-04 01:42:22.000000',
                'questionable_id' => 74,
                'questionable_type' => 'App\\Project',
            ),
            174 => 
            array (
                'id_old' => 1999181,
            'question_text' => 'Can you please provide a more detailed job description for the two graduate assistants (ie. how will they be cultivating prospective donors)?',
                'netid' => 'altomare',
                'deadline' => '2012-02-10 01:45:55.000000',
                'created_at' => '2012-02-04 01:45:55.000000',
                'questionable_id' => 77,
                'questionable_type' => 'App\\Project',
            ),
            175 => 
            array (
                'id_old' => 1999182,
                'question_text' => 'Since this program started, how much money has been raised each year that has directly benefited students?',
                'netid' => 'altomare',
                'deadline' => '2012-02-10 01:50:33.000000',
                'created_at' => '2012-02-04 01:50:33.000000',
                'questionable_id' => 77,
                'questionable_type' => 'App\\Project',
            ),
            176 => 
            array (
                'id_old' => 1999183,
                'question_text' => 'Which undergraduate and graduate initiatives in the annual Survey Results would this program support?',
                'netid' => 'altomare',
                'deadline' => '2012-02-10 02:53:25.000000',
                'created_at' => '2012-02-04 02:53:25.000000',
                'questionable_id' => 64,
                'questionable_type' => 'App\\Project',
            ),
            177 => 
            array (
                'id_old' => 1999184,
                'question_text' => 'How would collaboration with supporting organizations lead to future funding?',
                'netid' => 'altomare',
                'deadline' => '2012-02-10 02:54:05.000000',
                'created_at' => '2012-02-04 02:54:05.000000',
                'questionable_id' => 64,
                'questionable_type' => 'App\\Project',
            ),
            178 => 
            array (
                'id_old' => 1999185,
                'question_text' => 'test\'s for the db',
                'netid' => 'kmbeyer',
                'deadline' => '2012-02-15 22:37:19.000000',
                'created_at' => '2012-02-09 22:37:19.000000',
                'questionable_id' => 62,
                'questionable_type' => 'App\\Project',
            ),
            179 => 
            array (
                'id_old' => 1999186,
                'question_text' => 'What is the justification for requesting $20,000 instead of the $15,000 awarded in previous years?',
                'netid' => 'altomare',
                'deadline' => '2012-02-17 00:42:49.000000',
                'created_at' => '2012-02-11 00:42:49.000000',
                'questionable_id' => 69,
                'questionable_type' => 'App\\Project',
            ),
            180 => 
            array (
                'id_old' => 1999187,
            'question_text' => 'What is your justification for requesting $125,000 (as opposed to the $100,000 awarded the year before)?',
                'netid' => 'altomare',
                'deadline' => '2012-02-17 00:45:08.000000',
                'created_at' => '2012-02-11 00:45:08.000000',
                'questionable_id' => 78,
                'questionable_type' => 'App\\Project',
            ),
            181 => 
            array (
                'id_old' => 1999188,
                'question_text' => 'What was the total amount of funding requests received this year?',
                'netid' => 'altomare',
                'deadline' => '2012-02-17 00:46:51.000000',
                'created_at' => '2012-02-11 00:46:51.000000',
                'questionable_id' => 78,
                'questionable_type' => 'App\\Project',
            ),
            182 => 
            array (
                'id_old' => 1999189,
                'question_text' => 'What was the total amount of funding appropriated last year?',
                'netid' => 'altomare',
                'deadline' => '2012-02-17 00:47:50.000000',
                'created_at' => '2012-02-11 00:47:50.000000',
                'questionable_id' => 78,
                'questionable_type' => 'App\\Project',
            ),
            183 => 
            array (
                'id_old' => 1999190,
                'question_text' => 'How are you ensuring the longevity of this program?',
                'netid' => 'altomare',
                'deadline' => '2012-02-17 00:49:37.000000',
                'created_at' => '2012-02-11 00:49:37.000000',
                'questionable_id' => 78,
                'questionable_type' => 'App\\Project',
            ),
            184 => 
            array (
                'id_old' => 1999191,
            'question_text' => 'Will the three conditions that LWC agreed to (paragraph four in the program description) specifically exclude undergraduates from receiving the $25,000?  
If yes, how will these three conditions effect the ability of undergraduates to receive money from the remaining budget of LWC (i.e. not the $25,000 SSFBA money)?',
                'netid' => 'altomare',
                'deadline' => '2012-02-17 01:08:53.000000',
                'created_at' => '2012-02-11 01:08:53.000000',
                'questionable_id' => 68,
                'questionable_type' => 'App\\Project',
            ),
            185 => 
            array (
                'id_old' => 1999192,
                'question_text' => 'Please justify how this program combines and expands last year\'s pilot
programs (Transfer Student Services Center and Student Transitions Student Assistants)?',
                'netid' => 'altomare',
                'deadline' => '2012-02-24 01:40:12.000000',
                'created_at' => '2012-02-18 01:40:12.000000',
                'questionable_id' => 44,
                'questionable_type' => 'App\\Project',
            ),
            186 => 
            array (
                'id_old' => 1999193,
                'question_text' => 'Please provide a brief job description for the student employees and the grad assistant.',
                'netid' => 'altomare',
                'deadline' => '2012-02-24 01:51:27.000000',
                'created_at' => '2012-02-18 01:51:27.000000',
                'questionable_id' => 82,
                'questionable_type' => 'App\\Project',
            ),
            187 => 
            array (
                'id_old' => 1999194,
                'question_text' => 'Please explain your plan for employee cross training as well as how the employees and programs are being integrated.',
                'netid' => 'altomare',
                'deadline' => '2012-02-24 01:54:44.000000',
                'created_at' => '2012-02-18 01:54:44.000000',
                'questionable_id' => 82,
                'questionable_type' => 'App\\Project',
            ),
            188 => 
            array (
                'id_old' => 1999195,
            'question_text' => 'Please clarify the matched funding being provided (on the front of the application it says that the SSF will be the sole funding and in the longevity plan it says that Student Transitions will match funds).',
                'netid' => 'altomare',
                'deadline' => '2012-02-24 01:58:36.000000',
                'created_at' => '2012-02-18 01:58:36.000000',
                'questionable_id' => 82,
                'questionable_type' => 'App\\Project',
            ),
            189 => 
            array (
                'id_old' => 1999196,
                'question_text' => 'What is your marketing plan to recruit students for these internship
opportunities?',
                'netid' => 'altomare',
                'deadline' => '2012-02-24 01:59:36.000000',
                'created_at' => '2012-02-18 01:59:36.000000',
                'questionable_id' => 63,
                'questionable_type' => 'App\\Project',
            ),
            190 => 
            array (
                'id_old' => 1999197,
                'question_text' => 'Please explain how the $5,000 for credit bearing internship will be used?',
                'netid' => 'altomare',
                'deadline' => '2012-02-24 02:01:40.000000',
                'created_at' => '2012-02-18 02:01:40.000000',
                'questionable_id' => 63,
                'questionable_type' => 'App\\Project',
            ),
            191 => 
            array (
                'id_old' => 1999198,
                'question_text' => 'Please describe in greater detail how the $18,000 for campus wide events will be utilized across each center mentioned in the application, as well as if the money will be spent on new or old programs.',
                'netid' => 'altomare',
                'deadline' => '2012-02-24 02:05:46.000000',
                'created_at' => '2012-02-18 02:05:46.000000',
                'questionable_id' => 63,
                'questionable_type' => 'App\\Project',
            ),
            192 => 
            array (
                'id_old' => 1999199,
                'question_text' => 'Please provide a job description for the Graduate Assistant.',
                'netid' => 'altomare',
                'deadline' => '2012-02-24 02:06:06.000000',
                'created_at' => '2012-02-18 02:06:06.000000',
                'questionable_id' => 81,
                'questionable_type' => 'App\\Project',
            ),
            193 => 
            array (
                'id_old' => 1999200,
            'question_text' => 'Can you respectfully clarify what type of professional staff position you are hiring (appointed or classified regular)?',
                'netid' => 'altomare',
                'deadline' => '2012-04-05 22:30:50.000000',
                'created_at' => '2012-03-30 22:30:50.000000',
                'questionable_id' => 6,
                'questionable_type' => 'App\\Par',
            ),
            194 => 
            array (
                'id_old' => 1999201,
                'question_text' => 'This is a question about the SSFAB demo project.',
                'netid' => 'kmbeyer',
                'deadline' => '2012-10-18 22:59:15.000000',
                'created_at' => '2012-10-12 22:59:15.000000',
                'questionable_id' => 84,
                'questionable_type' => 'App\\Project',
            ),
            195 => 
            array (
                'id_old' => 1999202,
                'question_text' => 'Specifically, what are the benefits that the Speakers Series will receive with you and your committee member representing them there?',
                'netid' => 'jevans91',
                'deadline' => '2012-10-19 00:26:31.000000',
                'created_at' => '2012-10-13 00:26:31.000000',
                'questionable_id' => 9,
                'questionable_type' => 'App\\Par',
            ),
            196 => 
            array (
                'id_old' => 1999203,
                'question_text' => 'How has the Wildcat Events Board benefited from attending this conference in the past? Have you been able to secure any speakers?',
                'netid' => 'jevans91',
                'deadline' => '2012-10-19 00:28:53.000000',
                'created_at' => '2012-10-13 00:28:53.000000',
                'questionable_id' => 9,
                'questionable_type' => 'App\\Par',
            ),
            197 => 
            array (
                'id_old' => 1999204,
                'question_text' => 'Testing question deadline for PARs...',
                'netid' => 'kmbeyer',
                'deadline' => '2012-10-22 00:26:13.000000',
                'created_at' => '2012-10-16 00:26:13.000000',
                'questionable_id' => 1,
                'questionable_type' => 'App\\Par',
            ),
            198 => 
            array (
                'id_old' => 1999205,
                'question_text' => 'What is program\'s current student staffing situation?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:12:12.000000',
                'created_at' => '2013-01-19 01:12:12.000000',
                'questionable_id' => 14,
                'questionable_type' => 'App\\Par',
            ),
            199 => 
            array (
                'id_old' => 1999206,
                'question_text' => 'Please provide a job description for the student worker.',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:14:17.000000',
                'created_at' => '2013-01-19 01:14:17.000000',
                'questionable_id' => 14,
                'questionable_type' => 'App\\Par',
            ),
            200 => 
            array (
                'id_old' => 1999207,
                'question_text' => 'How do you plan to market this program?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:15:09.000000',
                'created_at' => '2013-01-19 01:15:09.000000',
                'questionable_id' => 89,
                'questionable_type' => 'App\\Project',
            ),
            201 => 
            array (
                'id_old' => 1999208,
                'question_text' => 'How long would the application process for a student to receive funding take?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:15:45.000000',
                'created_at' => '2013-01-19 01:15:45.000000',
                'questionable_id' => 89,
                'questionable_type' => 'App\\Project',
            ),
            202 => 
            array (
                'id_old' => 1999209,
                'question_text' => 'Which student populations will be most impacted by the Cyberbullying Campaign?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:17:27.000000',
                'created_at' => '2013-01-19 01:17:27.000000',
                'questionable_id' => 98,
                'questionable_type' => 'App\\Project',
            ),
            203 => 
            array (
                'id_old' => 1999210,
                'question_text' => 'Would it be possible for those constituencies to contribute?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:17:47.000000',
                'created_at' => '2013-01-19 01:17:47.000000',
                'questionable_id' => 98,
                'questionable_type' => 'App\\Project',
            ),
            204 => 
            array (
                'id_old' => 1999211,
                'question_text' => 'Will this program only be available to students? And if so, how will you monitor that?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:19:51.000000',
                'created_at' => '2013-01-19 01:19:51.000000',
                'questionable_id' => 97,
                'questionable_type' => 'App\\Project',
            ),
            205 => 
            array (
                'id_old' => 1999212,
                'question_text' => 'How did you arrive at 10 students? What makes this number unique?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:23:06.000000',
                'created_at' => '2013-01-19 01:23:06.000000',
                'questionable_id' => 96,
                'questionable_type' => 'App\\Project',
            ),
            206 => 
            array (
                'id_old' => 1999213,
                'question_text' => 'What is included in the Travel Out part of your budget?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:26:23.000000',
                'created_at' => '2013-01-19 01:26:23.000000',
                'questionable_id' => 95,
                'questionable_type' => 'App\\Project',
            ),
            207 => 
            array (
                'id_old' => 1999214,
                'question_text' => 'What is included in your operations budget?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:26:39.000000',
                'created_at' => '2013-01-19 01:26:39.000000',
                'questionable_id' => 95,
                'questionable_type' => 'App\\Project',
            ),
            208 => 
            array (
                'id_old' => 1999215,
                'question_text' => 'Please provided a job description of the student employees as well as how many students and hours they will be working.',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:27:30.000000',
                'created_at' => '2013-01-19 01:27:30.000000',
                'questionable_id' => 95,
                'questionable_type' => 'App\\Project',
            ),
            209 => 
            array (
                'id_old' => 1999216,
                'question_text' => 'How did you arrive at 35 students? What makes that number unique?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:27:56.000000',
                'created_at' => '2013-01-19 01:27:56.000000',
                'questionable_id' => 111,
                'questionable_type' => 'App\\Project',
            ),
            210 => 
            array (
                'id_old' => 1999217,
                'question_text' => 'How many individual students are affected by this program opposed to repeat meetings?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:28:38.000000',
                'created_at' => '2013-01-19 01:28:38.000000',
                'questionable_id' => 111,
                'questionable_type' => 'App\\Project',
            ),
            211 => 
            array (
                'id_old' => 1999218,
                'question_text' => 'What is included in the Training Materials aspect of your budget?',
                'netid' => 'jevans91',
                'deadline' => '2013-01-25 01:29:13.000000',
                'created_at' => '2013-01-19 01:29:13.000000',
                'questionable_id' => 111,
                'questionable_type' => 'App\\Project',
            ),
            212 => 
            array (
                'id_old' => 1999219,
                'question_text' => 'Please give a more specific breakdown of your requested operations budget.',
                'netid' => 'jevans91',
                'deadline' => '2013-02-01 00:57:58.000000',
                'created_at' => '2013-01-26 00:57:58.000000',
                'questionable_id' => 95,
                'questionable_type' => 'App\\Project',
            ),
            213 => 
            array (
                'id_old' => 1999220,
                'question_text' => 'Why does the program require 3,300 weekly hours to sustain itself?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-01 00:59:08.000000',
                'created_at' => '2013-01-26 00:59:08.000000',
                'questionable_id' => 111,
                'questionable_type' => 'App\\Project',
            ),
            214 => 
            array (
                'id_old' => 1999221,
                'question_text' => 'How are those 11,000 students directly impacted?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-01 01:01:14.000000',
                'created_at' => '2013-01-26 01:01:14.000000',
                'questionable_id' => 111,
                'questionable_type' => 'App\\Project',
            ),
            215 => 
            array (
                'id_old' => 1999222,
                'question_text' => 'Please explain what is included in the Travel In and Out section of your budget.',
                'netid' => 'jevans91',
                'deadline' => '2013-02-01 01:03:55.000000',
                'created_at' => '2013-01-26 01:03:55.000000',
                'questionable_id' => 85,
                'questionable_type' => 'App\\Project',
            ),
            216 => 
            array (
                'id_old' => 1999223,
                'question_text' => 'Please explain the student support section of your budget.',
                'netid' => 'jevans91',
                'deadline' => '2013-02-01 01:05:43.000000',
                'created_at' => '2013-01-26 01:05:43.000000',
                'questionable_id' => 85,
                'questionable_type' => 'App\\Project',
            ),
            217 => 
            array (
                'id_old' => 1999224,
                'question_text' => 'How does this program plan to become more sustainable?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-01 01:06:24.000000',
                'created_at' => '2013-01-26 01:06:24.000000',
                'questionable_id' => 85,
                'questionable_type' => 'App\\Project',
            ),
            218 => 
            array (
                'id_old' => 1999225,
                'question_text' => 'Please provide a breakdown of what is included in the $75,000 for your operations budget.',
                'netid' => 'jevans91',
                'deadline' => '2013-02-01 01:07:21.000000',
                'created_at' => '2013-01-26 01:07:21.000000',
                'questionable_id' => 85,
                'questionable_type' => 'App\\Project',
            ),
            219 => 
            array (
                'id_old' => 1999226,
                'question_text' => 'How do you plan to market this program and recruit students to have them participate in the volunteer opportunities?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-01 01:10:33.000000',
                'created_at' => '2013-01-26 01:10:33.000000',
                'questionable_id' => 93,
                'questionable_type' => 'App\\Project',
            ),
            220 => 
            array (
                'id_old' => 1999227,
                'question_text' => 'What is included in the day to day operations of the GA\'s',
                'netid' => 'jevans91',
                'deadline' => '2013-02-01 01:11:17.000000',
                'created_at' => '2013-01-26 01:11:17.000000',
                'questionable_id' => 107,
                'questionable_type' => 'App\\Project',
            ),
            221 => 
            array (
                'id_old' => 1999228,
                'question_text' => 'What is the $500 each GA is awarded used for?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-01 01:11:48.000000',
                'created_at' => '2013-01-26 01:11:48.000000',
                'questionable_id' => 107,
                'questionable_type' => 'App\\Project',
            ),
            222 => 
            array (
                'id_old' => 1999229,
                'question_text' => 'How are your efforts to get institutional funding going?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-01 01:12:20.000000',
                'created_at' => '2013-01-26 01:12:20.000000',
                'questionable_id' => 92,
                'questionable_type' => 'App\\Project',
            ),
            223 => 
            array (
                'id_old' => 1999230,
                'question_text' => 'Please explain the difference in graduate clubs receiving funding from GPSC club funding opposed to ASUA  club funding.',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 00:54:29.000000',
                'created_at' => '2013-02-02 00:54:29.000000',
                'questionable_id' => 108,
                'questionable_type' => 'App\\Project',
            ),
            224 => 
            array (
                'id_old' => 1999231,
                'question_text' => 'Please explain what the medical direction/base hospital entails.',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 00:56:55.000000',
                'created_at' => '2013-02-02 00:56:55.000000',
                'questionable_id' => 86,
                'questionable_type' => 'App\\Project',
            ),
            225 => 
            array (
                'id_old' => 1999232,
                'question_text' => 'Do you provide any services different from tucson fire department?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 00:57:41.000000',
                'created_at' => '2013-02-02 00:57:41.000000',
                'questionable_id' => 86,
                'questionable_type' => 'App\\Project',
            ),
            226 => 
            array (
                'id_old' => 1999233,
                'question_text' => 'Have you spoken with risk management about your expansion and if so are their any problems or liability concerns from the university\'s perspective?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 00:59:16.000000',
                'created_at' => '2013-02-02 00:59:16.000000',
                'questionable_id' => 86,
                'questionable_type' => 'App\\Project',
            ),
            227 => 
            array (
                'id_old' => 1999234,
                'question_text' => 'Have you spoken to Tucson Fire Department and if so, do they think expansion of this program is valuable?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 00:59:42.000000',
                'created_at' => '2013-02-02 00:59:42.000000',
                'questionable_id' => 86,
                'questionable_type' => 'App\\Project',
            ),
            228 => 
            array (
                'id_old' => 1999235,
                'question_text' => 'When did you begin counting the 100 students that you have helped?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 01:00:51.000000',
                'created_at' => '2013-02-02 01:00:51.000000',
                'questionable_id' => 86,
                'questionable_type' => 'App\\Project',
            ),
            229 => 
            array (
                'id_old' => 1999236,
                'question_text' => 'Do you have any statistics on the types of incidents that you have responded to?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 01:01:48.000000',
                'created_at' => '2013-02-02 01:01:48.000000',
                'questionable_id' => 86,
                'questionable_type' => 'App\\Project',
            ),
            230 => 
            array (
                'id_old' => 1999237,
                'question_text' => 'Which aspect of this program most helps student retention?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 01:02:24.000000',
                'created_at' => '2013-02-02 01:02:24.000000',
                'questionable_id' => 110,
                'questionable_type' => 'App\\Project',
            ),
            231 => 
            array (
                'id_old' => 1999238,
                'question_text' => 'Seeing your increase in wages from last year to this year, does this mean you are increasing your staff?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 01:03:08.000000',
                'created_at' => '2013-02-02 01:03:08.000000',
                'questionable_id' => 110,
                'questionable_type' => 'App\\Project',
            ),
            232 => 
            array (
                'id_old' => 1999239,
                'question_text' => 'Do you coordinate your services with any retention based programs?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 01:03:38.000000',
                'created_at' => '2013-02-02 01:03:38.000000',
                'questionable_id' => 110,
                'questionable_type' => 'App\\Project',
            ),
            233 => 
            array (
                'id_old' => 1999240,
                'question_text' => 'What is the average GPA increase for those who participate?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 01:04:58.000000',
                'created_at' => '2013-02-02 01:04:58.000000',
                'questionable_id' => 90,
                'questionable_type' => 'App\\Project',
            ),
            234 => 
            array (
                'id_old' => 1999241,
                'question_text' => 'Do you coordinate your services with any other retention based programs ?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-08 01:06:25.000000',
                'created_at' => '2013-02-02 01:06:25.000000',
                'questionable_id' => 90,
                'questionable_type' => 'App\\Project',
            ),
            235 => 
            array (
                'id_old' => 1999242,
                'question_text' => 'Is the GA you have budget for a half time position?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-15 00:51:36.000000',
                'created_at' => '2013-02-09 00:51:36.000000',
                'questionable_id' => 91,
                'questionable_type' => 'App\\Project',
            ),
            236 => 
            array (
                'id_old' => 1999243,
                'question_text' => 'In terms of attendance, how successful have your smaller events been?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-15 00:52:09.000000',
                'created_at' => '2013-02-09 00:52:09.000000',
                'questionable_id' => 102,
                'questionable_type' => 'App\\Project',
            ),
            237 => 
            array (
                'id_old' => 1999244,
                'question_text' => 'Is this program only for UA students and if so will you be checking catcards?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-22 01:20:29.000000',
                'created_at' => '2013-02-16 01:20:29.000000',
                'questionable_id' => 114,
                'questionable_type' => 'App\\Project',
            ),
            238 => 
            array (
                'id_old' => 1999245,
                'question_text' => 'Is the program 1st come 1st serve?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-22 01:21:17.000000',
                'created_at' => '2013-02-16 01:21:17.000000',
                'questionable_id' => 114,
                'questionable_type' => 'App\\Project',
            ),
            239 => 
            array (
                'id_old' => 1999246,
                'question_text' => 'In what ways does this replace or expand on the existing JEDI program?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-22 01:23:28.000000',
                'created_at' => '2013-02-16 01:23:28.000000',
                'questionable_id' => 99,
                'questionable_type' => 'App\\Project',
            ),
            240 => 
            array (
                'id_old' => 1999247,
                'question_text' => 'How will this program collaborate with other social justice program on campus?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-22 01:24:10.000000',
                'created_at' => '2013-02-16 01:24:10.000000',
                'questionable_id' => 99,
                'questionable_type' => 'App\\Project',
            ),
            241 => 
            array (
                'id_old' => 1999248,
                'question_text' => 'What is the rational for providing the facilitators with stipends.',
                'netid' => 'jevans91',
                'deadline' => '2013-02-22 01:25:22.000000',
                'created_at' => '2013-02-16 01:25:22.000000',
                'questionable_id' => 99,
                'questionable_type' => 'App\\Project',
            ),
            242 => 
            array (
                'id_old' => 1999249,
                'question_text' => 'What is included in the $500 operations section of your budget?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-22 01:26:13.000000',
                'created_at' => '2013-02-16 01:26:13.000000',
                'questionable_id' => 94,
                'questionable_type' => 'App\\Project',
            ),
            243 => 
            array (
                'id_old' => 1999250,
                'question_text' => 'How many GA\'s and how many student employees does your proposed budget allow for?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-22 01:26:51.000000',
                'created_at' => '2013-02-16 01:26:51.000000',
                'questionable_id' => 94,
                'questionable_type' => 'App\\Project',
            ),
            244 => 
            array (
                'id_old' => 1999251,
                'question_text' => 'What percentage of the GA\'s time is spent on coordinating alumni events?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-22 01:28:33.000000',
                'created_at' => '2013-02-16 01:28:33.000000',
                'questionable_id' => 110,
                'questionable_type' => 'App\\Project',
            ),
            245 => 
            array (
                'id_old' => 1999252,
                'question_text' => 'What percentage of the GA\'s time is spent on coordinating alumni events?',
                'netid' => 'jevans91',
                'deadline' => '2013-02-24 18:10:13.000000',
                'created_at' => '2013-02-18 18:10:13.000000',
                'questionable_id' => 101,
                'questionable_type' => 'App\\Project',
            ),
            246 => 
            array (
                'id_old' => 1999253,
                'question_text' => 'Please disregard the previous question in regards to GA\'s. Thank you',
                'netid' => 'jevans91',
                'deadline' => '2013-02-24 18:10:49.000000',
                'created_at' => '2013-02-18 18:10:49.000000',
                'questionable_id' => 110,
                'questionable_type' => 'App\\Project',
            ),
            247 => 
            array (
                'id_old' => 1999254,
                'question_text' => 'This looks like a reasonable request, the board just has a question regarding the Spring events. What are the spring events that you have planned?',
                'netid' => 'kyleoman',
                'deadline' => '2013-04-04 22:24:12.000000',
                'created_at' => '2013-03-29 22:24:12.000000',
                'questionable_id' => 17,
                'questionable_type' => 'App\\Par',
            ),
            248 => 
            array (
                'id_old' => 1999255,
                'question_text' => 'How will students be selected?',
                'netid' => 'ledward1',
                'deadline' => '2013-10-10 22:35:06.000000',
                'created_at' => '2013-10-04 22:35:06.000000',
                'questionable_id' => 115,
                'questionable_type' => 'App\\Project',
            ),
            249 => 
            array (
                'id_old' => 1999256,
                'question_text' => 'What are the job descriptions of the two student employees?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:13:37.000000',
                'created_at' => '2014-01-25 00:13:37.000000',
                'questionable_id' => 120,
                'questionable_type' => 'App\\Project',
            ),
            250 => 
            array (
                'id_old' => 1999257,
                'question_text' => 'How to do you intend to maintain and update the website without ongoing funding?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:14:04.000000',
                'created_at' => '2014-01-25 00:14:04.000000',
                'questionable_id' => 120,
                'questionable_type' => 'App\\Project',
            ),
            251 => 
            array (
                'id_old' => 1999258,
                'question_text' => 'Could you please break down your entertainment budget?
What events have you put on to date, and what are you planning to do in the Spring 2014 semester?
How are you marketing events to students?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:14:49.000000',
                'created_at' => '2014-01-25 00:14:49.000000',
                'questionable_id' => 128,
                'questionable_type' => 'App\\Project',
            ),
            252 => 
            array (
                'id_old' => 1999259,
                'question_text' => 'What events have you put on to date, and what are you planning to do in the Spring 2014 semester?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:17:19.000000',
                'created_at' => '2014-01-25 00:17:19.000000',
                'questionable_id' => 128,
                'questionable_type' => 'App\\Project',
            ),
            253 => 
            array (
                'id_old' => 1999260,
                'question_text' => 'How are you marketing events to students?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:17:38.000000',
                'created_at' => '2014-01-25 00:17:38.000000',
                'questionable_id' => 128,
                'questionable_type' => 'App\\Project',
            ),
            254 => 
            array (
                'id_old' => 1999261,
                'question_text' => 'Can certificates, class credits, or any other tangible benefits be obtained through this program, and if so, when would those be available to students?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:19:49.000000',
                'created_at' => '2014-01-25 00:19:49.000000',
                'questionable_id' => 117,
                'questionable_type' => 'App\\Project',
            ),
            255 => 
            array (
                'id_old' => 1999262,
                'question_text' => 'How many student employees do you intend to fund?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:21:17.000000',
                'created_at' => '2014-01-25 00:21:17.000000',
                'questionable_id' => 136,
                'questionable_type' => 'App\\Project',
            ),
            256 => 
            array (
                'id_old' => 1999263,
                'question_text' => 'What types of training services do you provide your volunteers?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:21:51.000000',
                'created_at' => '2014-01-25 00:21:51.000000',
                'questionable_id' => 136,
                'questionable_type' => 'App\\Project',
            ),
            257 => 
            array (
                'id_old' => 1999264,
                'question_text' => 'What is the supervisory structure of the organization?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:24:03.000000',
                'created_at' => '2014-01-25 00:24:03.000000',
                'questionable_id' => 136,
                'questionable_type' => 'App\\Project',
            ),
            258 => 
            array (
                'id_old' => 1999265,
                'question_text' => 'Do you think it is possible for your organization to be partially volunteer, partially paid employees?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:25:00.000000',
                'created_at' => '2014-01-25 00:25:00.000000',
                'questionable_id' => 136,
                'questionable_type' => 'App\\Project',
            ),
            259 => 
            array (
                'id_old' => 1999266,
                'question_text' => 'How many RUSH staff were employed in the 2014 fiscal year and what were their roles?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:26:27.000000',
                'created_at' => '2014-01-25 00:26:27.000000',
                'questionable_id' => 129,
                'questionable_type' => 'App\\Project',
            ),
            260 => 
            array (
                'id_old' => 1999267,
                'question_text' => 'How many RUSH staff were employed in the 2013 fiscal year?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:28:48.000000',
                'created_at' => '2014-01-25 00:28:48.000000',
                'questionable_id' => 129,
                'questionable_type' => 'App\\Project',
            ),
            261 => 
            array (
                'id_old' => 1999268,
                'question_text' => 'Are other paid staff utilized during opening of school?',
                'netid' => 'ledward1',
                'deadline' => '2014-01-31 00:29:57.000000',
                'created_at' => '2014-01-25 00:29:57.000000',
                'questionable_id' => 129,
                'questionable_type' => 'App\\Project',
            ),
            262 => 
            array (
                'id_old' => 1999269,
            'question_text' => 'Could you please breakdown your entertainment budget (for FY15)?',
                'netid' => 'twhetzel',
                'deadline' => '2014-02-02 15:40:50.000000',
                'created_at' => '2014-01-27 15:40:50.000000',
                'questionable_id' => 128,
                'questionable_type' => 'App\\Project',
            ),
            263 => 
            array (
                'id_old' => 1999270,
                'question_text' => 'How many spaces will be furnished with this funding?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-06 23:50:12.000000',
                'created_at' => '2014-01-31 23:50:12.000000',
                'questionable_id' => 138,
                'questionable_type' => 'App\\Project',
            ),
            264 => 
            array (
                'id_old' => 1999271,
                'question_text' => 'Could you give us a breakdown of your budget for furniture?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-06 23:50:42.000000',
                'created_at' => '2014-01-31 23:50:42.000000',
                'questionable_id' => 138,
                'questionable_type' => 'App\\Project',
            ),
            265 => 
            array (
                'id_old' => 1999272,
                'question_text' => 'How are you going to educate students about the service opportunities?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-06 23:51:33.000000',
                'created_at' => '2014-01-31 23:51:33.000000',
                'questionable_id' => 116,
                'questionable_type' => 'App\\Project',
            ),
            266 => 
            array (
                'id_old' => 1999273,
                'question_text' => 'How does this differ from Volunteer UA and other existing programs?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-06 23:52:28.000000',
                'created_at' => '2014-01-31 23:52:28.000000',
                'questionable_id' => 116,
                'questionable_type' => 'App\\Project',
            ),
            267 => 
            array (
                'id_old' => 1999274,
                'question_text' => 'If you do not receive funding, how would it impact your business analysis?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-06 23:53:05.000000',
                'created_at' => '2014-01-31 23:53:05.000000',
                'questionable_id' => 137,
                'questionable_type' => 'App\\Project',
            ),
            268 => 
            array (
                'id_old' => 1999275,
                'question_text' => 'How do you believe the light rail system will affect SafeRide?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-06 23:54:14.000000',
                'created_at' => '2014-01-31 23:54:14.000000',
                'questionable_id' => 124,
                'questionable_type' => 'App\\Project',
            ),
            269 => 
            array (
                'id_old' => 1999276,
                'question_text' => 'Could you give us data regarding the current use of vehicles and the need for new vehicles?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-06 23:54:38.000000',
                'created_at' => '2014-01-31 23:54:38.000000',
                'questionable_id' => 124,
                'questionable_type' => 'App\\Project',
            ),
            270 => 
            array (
                'id_old' => 1999277,
                'question_text' => 'Could we get a breakdown of your budget for vehicles, supplies, and maintenance?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-06 23:55:14.000000',
                'created_at' => '2014-01-31 23:55:14.000000',
                'questionable_id' => 124,
                'questionable_type' => 'App\\Project',
            ),
            271 => 
            array (
                'id_old' => 1999278,
                'question_text' => 'Could you please provide a breakdown of your entertainment budget for FY15?  Please respond by 5:00 PM on Wednesday Feb 5th.',
                'netid' => 'twhetzel',
                'deadline' => '2014-02-09 20:24:01.000000',
                'created_at' => '2014-02-03 20:24:01.000000',
                'questionable_id' => 128,
                'questionable_type' => 'App\\Project',
            ),
            272 => 
            array (
                'id_old' => 1999279,
                'question_text' => 'In last year\'s application, you requested $181,500 for Travel Funds for 
FY2015. Why did your request increase to $280,000 in this application for FY2015?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-14 00:05:01.000000',
                'created_at' => '2014-02-08 00:05:01.000000',
                'questionable_id' => 121,
                'questionable_type' => 'App\\Project',
            ),
            273 => 
            array (
                'id_old' => 1999280,
                'question_text' => 'Could you give us more information about the job descriptions for the graduate assistant and the student employees?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-14 00:07:02.000000',
                'created_at' => '2014-02-08 00:07:02.000000',
                'questionable_id' => 135,
                'questionable_type' => 'App\\Project',
            ),
            274 => 
            array (
                'id_old' => 1999281,
                'question_text' => 'How do you recruit mentors?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-14 00:07:39.000000',
                'created_at' => '2014-02-08 00:07:39.000000',
                'questionable_id' => 135,
                'questionable_type' => 'App\\Project',
            ),
            275 => 
            array (
                'id_old' => 1999282,
                'question_text' => 'Could you provide us with any data regarding the efficacy of staff mentoring versus peer-to-peer mentoring?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-14 00:09:48.000000',
                'created_at' => '2014-02-08 00:09:48.000000',
                'questionable_id' => 135,
                'questionable_type' => 'App\\Project',
            ),
            276 => 
            array (
                'id_old' => 1999283,
                'question_text' => 'Could you give us more information about how you hope to reach targeted groups?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-14 00:10:46.000000',
                'created_at' => '2014-02-08 00:10:46.000000',
                'questionable_id' => 135,
                'questionable_type' => 'App\\Project',
            ),
            277 => 
            array (
                'id_old' => 1999284,
                'question_text' => 'If the Activities Fee is passed, what are WEB\'s future plans for requests to the Student Services Fees Advisory Board?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-14 00:13:45.000000',
                'created_at' => '2014-02-08 00:13:45.000000',
                'questionable_id' => 127,
                'questionable_type' => 'App\\Project',
            ),
            278 => 
            array (
                'id_old' => 1999285,
                'question_text' => 'Can you please give us information regarding the number of requests over the past few years?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-14 00:14:35.000000',
                'created_at' => '2014-02-08 00:14:35.000000',
                'questionable_id' => 131,
                'questionable_type' => 'App\\Project',
            ),
            279 => 
            array (
                'id_old' => 1999286,
                'question_text' => 'We understand that the Activities Fees money will begin being collected this Fall. What would you need the requested Student Services Fees money for when this fee is collected?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-20 23:55:49.000000',
                'created_at' => '2014-02-14 23:55:49.000000',
                'questionable_id' => 127,
                'questionable_type' => 'App\\Project',
            ),
            280 => 
            array (
                'id_old' => 1999287,
                'question_text' => 'Could you give us data regarding the retention rates of colleges that are not participating in the PASS program?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-20 23:57:30.000000',
                'created_at' => '2014-02-14 23:57:30.000000',
                'questionable_id' => 118,
                'questionable_type' => 'App\\Project',
            ),
            281 => 
            array (
                'id_old' => 1999288,
                'question_text' => 'How do you intend to engage students in planning on these events?',
                'netid' => 'osoo',
                'deadline' => '2014-02-20 23:57:45.000000',
                'created_at' => '2014-02-14 23:57:45.000000',
                'questionable_id' => 126,
                'questionable_type' => 'App\\Project',
            ),
            282 => 
            array (
                'id_old' => 1999289,
                'question_text' => 'How many more students do you intend to serve after the fee is in place?',
                'netid' => 'ledward1',
                'deadline' => '2014-02-20 23:58:07.000000',
                'created_at' => '2014-02-14 23:58:07.000000',
                'questionable_id' => 118,
                'questionable_type' => 'App\\Project',
            ),
            283 => 
            array (
                'id_old' => 1999290,
                'question_text' => 'What benefit do you see in paying the peer mentors instead of having them be volunteers?',
                'netid' => 'osoo',
                'deadline' => '2014-02-20 23:59:01.000000',
                'created_at' => '2014-02-14 23:59:01.000000',
                'questionable_id' => 134,
                'questionable_type' => 'App\\Project',
            ),
            284 => 
            array (
                'id_old' => 1999293,
                'question_text' => 'We understand that you are no longer in need of funding. Please confirm that this is correct.',
                'netid' => 'ledward1',
                'deadline' => '2014-02-28 02:18:25.000000',
                'created_at' => '2014-02-22 02:18:25.000000',
                'questionable_id' => 127,
                'questionable_type' => 'App\\Project',
            ),
            285 => 
            array (
                'id_old' => 1999292,
                'question_text' => 'Why are you requesting funding for a staff coordinator rather than a GA coordinator?',
                'netid' => 'twhetzel',
                'deadline' => '2014-02-26 22:44:09.000000',
                'created_at' => '2014-02-20 22:44:09.000000',
                'questionable_id' => 119,
                'questionable_type' => 'App\\Project',
            ),
            286 => 
            array (
                'id_old' => 1999294,
                'question_text' => 'We understand that you are withdrawing your application for a second year of funding. Please confirm that this is correct.',
                'netid' => 'ledward1',
                'deadline' => '2014-02-28 02:18:57.000000',
                'created_at' => '2014-02-22 02:18:57.000000',
                'questionable_id' => 118,
                'questionable_type' => 'App\\Project',
            ),
            287 => 
            array (
                'id_old' => 1999309,
            'question_text' => 'How many students have you served this past year? (Total call volume v. accepted call)',
                'netid' => 'twhetzel',
                'deadline' => '2015-02-05 20:44:40.000000',
                'created_at' => '2015-01-30 20:44:40.000000',
                'questionable_id' => 170,
                'questionable_type' => 'App\\Project',
            ),
            288 => 
            array (
                'id_old' => 1999308,
            'question_text' => 'How many students have you served this past year? (Total call volume v. accepted call)',
                'netid' => 'twhetzel',
                'deadline' => '2015-02-05 20:44:24.000000',
                'created_at' => '2015-01-30 20:44:24.000000',
                'questionable_id' => 170,
                'questionable_type' => 'App\\Project',
            ),
            289 => 
            array (
                'id_old' => 1999306,
                'question_text' => 'What are the boundaries of Safe Ride? Has it expanded and do you anticipate extending the boundaries?',
                'netid' => 'twhetzel',
                'deadline' => '2015-02-05 20:43:22.000000',
                'created_at' => '2015-01-30 20:43:22.000000',
                'questionable_id' => 170,
                'questionable_type' => 'App\\Project',
            ),
            290 => 
            array (
                'id_old' => 1999307,
                'question_text' => 'In regards to technological updates to better process incoming and outgoing calls, along with text messages with updates to students, would you consider an application similar to Uber for SafeRide? If not, what are your anticipated ideas for updating SafeRide’s tech in the upcoming years?',
                'netid' => 'twhetzel',
                'deadline' => '2015-02-05 20:44:04.000000',
                'created_at' => '2015-01-30 20:44:04.000000',
                'questionable_id' => 170,
                'questionable_type' => 'App\\Project',
            ),
            291 => 
            array (
                'id_old' => 1999299,
                'question_text' => 'Please provide detail of the planned expenditures for the $10,000 requested for the employer outreach and visits budget line item.',
                'netid' => 'nhavey',
                'deadline' => '2015-01-30 20:13:42.000000',
                'created_at' => '2015-01-24 20:13:42.000000',
                'questionable_id' => 163,
                'questionable_type' => 'App\\Project',
            ),
            292 => 
            array (
                'id_old' => 1999300,
            'question_text' => 'Do you have any particular plans to include the 20% population of Grad Students in career outreach? Our concern is that the career fairs have (in the past) seemed to only include undergraduate employment recruiters and that graduate opportunities for networking and outreach seem few and far between.',
                'netid' => 'nhavey',
                'deadline' => '2015-01-30 20:13:53.000000',
                'created_at' => '2015-01-24 20:13:53.000000',
                'questionable_id' => 163,
                'questionable_type' => 'App\\Project',
            ),
            293 => 
            array (
                'id_old' => 1999301,
            'question_text' => 'The budget line for student employees (and the addendum page) states that the students will be paid $10/hr. There are obvious benefits to offering student employees higher (than minimum) wages but we would like to know what those benefits/what the ideology behind it is.',
                'netid' => 'nhavey',
                'deadline' => '2015-01-30 20:14:04.000000',
                'created_at' => '2015-01-24 20:14:04.000000',
                'questionable_id' => 163,
                'questionable_type' => 'App\\Project',
            ),
            294 => 
            array (
                'id_old' => 1999302,
                'question_text' => 'How did you arrive at the $10,000 amount for the Scholarship fund?  Do you see the need for these types of funds growing as 100% Engagement proliferates?',
                'netid' => 'nhavey',
                'deadline' => '2015-01-30 20:14:17.000000',
                'created_at' => '2015-01-24 20:14:17.000000',
                'questionable_id' => 163,
                'questionable_type' => 'App\\Project',
            ),
            295 => 
            array (
                'id_old' => 1999303,
            'question_text' => 'The GA positions reach a broad audience of students. The board would like to know if you have any updated metrics on the amount of students annually impacted by the GA positions and this funding. Additionally, are other graduate and professional students impacted in the same capacity? (If so, these metrics would also be helpful.)',
                'netid' => 'nhavey',
                'deadline' => '2015-01-30 20:14:29.000000',
                'created_at' => '2015-01-24 20:14:29.000000',
                'questionable_id' => 155,
                'questionable_type' => 'App\\Project',
            ),
            296 => 
            array (
                'id_old' => 1999304,
            'question_text' => 'Since graduate clubs can apply for regular ASUA Club funding, is there a reason to have alternative funding? Can graduate clubs double dip? What are the requirements to apply for this funding? (allocation requirements)',
                'netid' => 'nhavey',
                'deadline' => '2015-01-30 20:14:40.000000',
                'created_at' => '2015-01-24 20:14:40.000000',
                'questionable_id' => 158,
                'questionable_type' => 'App\\Project',
            ),
            297 => 
            array (
                'id_old' => 1999305,
                'question_text' => 'Please provide detail of the planned expenditures for the $10,000 GPSC GEP Program budget line item.',
                'netid' => 'nhavey',
                'deadline' => '2015-01-30 20:14:52.000000',
                'created_at' => '2015-01-24 20:14:52.000000',
                'questionable_id' => 165,
                'questionable_type' => 'App\\Project',
            ),
            298 => 
            array (
                'id_old' => 1999310,
                'question_text' => 'How many cars are you looking into buying for FY16 and how do you anticipate the increased volume of cars will help cover the increased rate of demand from students?',
                'netid' => 'twhetzel',
                'deadline' => '2015-02-05 20:57:20.000000',
                'created_at' => '2015-01-30 20:57:20.000000',
                'questionable_id' => 170,
                'questionable_type' => 'App\\Project',
            ),
            299 => 
            array (
                'id_old' => 1999311,
            'question_text' => '1.	What recent projects were funded through this grant program? (This is potentially a ton of information, but we are curious for any examples! Thank you Deya!!)',
                'netid' => 'nhavey',
                'deadline' => '2015-02-10 05:40:00.000000',
                'created_at' => '2015-02-04 05:40:00.000000',
                'questionable_id' => 167,
                'questionable_type' => 'App\\Project',
            ),
            300 => 
            array (
                'id_old' => 1999312,
                'question_text' => '1.	What specific marketing initiatives will be utilized to inform the student body?',
                'netid' => 'nhavey',
                'deadline' => '2015-02-10 05:40:23.000000',
                'created_at' => '2015-02-04 05:40:23.000000',
                'questionable_id' => 150,
                'questionable_type' => 'App\\Project',
            ),
            301 => 
            array (
                'id_old' => 1999313,
                'question_text' => '2.	What additional initiatives are being taken by the student union to provide more nutrient-dense food options?',
                'netid' => 'nhavey',
                'deadline' => '2015-02-10 05:40:38.000000',
                'created_at' => '2015-02-04 05:40:38.000000',
                'questionable_id' => 150,
                'questionable_type' => 'App\\Project',
            ),
            302 => 
            array (
                'id_old' => 1999314,
                'question_text' => '3.	Why specifically ask SSF for funding as opposed to the Unions or other potential sources?',
                'netid' => 'nhavey',
                'deadline' => '2015-02-10 05:41:02.000000',
                'created_at' => '2015-02-04 05:41:02.000000',
                'questionable_id' => 150,
                'questionable_type' => 'App\\Project',
            ),
            303 => 
            array (
                'id_old' => 1999315,
            'question_text' => 'What are the projected time allotments for individual groups outside of Think Tank (Blue Chip, CESL)? Will they hinder other students’ access to the Think Tank location?',
                'netid' => 'nhavey',
                'deadline' => '2015-02-14 01:44:37.000000',
                'created_at' => '2015-02-08 01:44:37.000000',
                'questionable_id' => 173,
                'questionable_type' => 'App\\Project',
            ),
            304 => 
            array (
                'id_old' => 1999316,
                'question_text' => 'What is the reason for the increase in employees from last year to this year? Do you have any plans for developing the program into a self-sustaining model?',
                'netid' => 'nhavey',
                'deadline' => '2015-02-14 01:44:59.000000',
                'created_at' => '2015-02-08 01:44:59.000000',
                'questionable_id' => 174,
                'questionable_type' => 'App\\Project',
            ),
            305 => 
            array (
                'id_old' => 1999317,
                'question_text' => 'Are the 4 retained temporary employees paid throughout the year from this funding? Please provide us a breakdown of the budget line items of classified regular, temporary, student employee and student work student.',
                'netid' => 'nhavey',
                'deadline' => '2015-02-14 01:45:31.000000',
                'created_at' => '2015-02-08 01:45:31.000000',
                'questionable_id' => 159,
                'questionable_type' => 'App\\Project',
            ),
            306 => 
            array (
                'id_old' => 1999318,
                'question_text' => '1.	Is it true that Scholarship Universe receives funding from an IT fee?  Are there other funding sources for this specific project?',
                'netid' => 'nhavey',
                'deadline' => '2015-02-22 03:12:19.000000',
                'created_at' => '2015-02-16 03:12:19.000000',
                'questionable_id' => 156,
                'questionable_type' => 'App\\Project',
            ),
            307 => 
            array (
                'id_old' => 1999319,
                'question_text' => '2.	Is it possible to have the current staff work more efficiently towards achieving the project goals instead of hiring/employing a new staff member?',
                'netid' => 'nhavey',
                'deadline' => '2015-02-22 03:12:51.000000',
                'created_at' => '2015-02-16 03:12:51.000000',
                'questionable_id' => 156,
                'questionable_type' => 'App\\Project',
            ),
            308 => 
            array (
                'id_old' => 1999320,
                'question_text' => '1.	What sort of marketing will be done for this program?',
                'netid' => 'nhavey',
                'deadline' => '2015-02-22 03:14:01.000000',
                'created_at' => '2015-02-16 03:14:01.000000',
                'questionable_id' => 154,
                'questionable_type' => 'App\\Project',
            ),
            309 => 
            array (
                'id_old' => 1999321,
                'question_text' => '1.	How would the money from the academic recovery fee be sufficient for funding next year? Ie: are you confident in the sustainability of this program, please elaborate.',
                'netid' => 'nhavey',
                'deadline' => '2015-02-22 03:14:45.000000',
                'created_at' => '2015-02-16 03:14:45.000000',
                'questionable_id' => 159,
                'questionable_type' => 'App\\Project',
            ),
            310 => 
            array (
                'id_old' => 1999322,
            'question_text' => 'What are the projected time allotments for individual groups outside of Think Tank (Blue Chip, CESL)? Will they hinder other students’ access to the Think Tank location?',
                'netid' => 'nhavey',
                'deadline' => '2015-02-22 03:18:57.000000',
                'created_at' => '2015-02-16 03:18:57.000000',
                'questionable_id' => 173,
                'questionable_type' => 'App\\Project',
            ),
            311 => 
            array (
                'id_old' => 1999323,
                'question_text' => 'How would the money from the academic recovery fee be sufficient for funding next year? Ie: are you confident in the sustainability of this program, please elaborate.',
                'netid' => 'nhavey',
                'deadline' => '2015-02-22 21:21:22.000000',
                'created_at' => '2015-02-16 21:21:22.000000',
                'questionable_id' => 157,
                'questionable_type' => 'App\\Project',
            ),
            312 => 
            array (
                'id_old' => 1999324,
                'question_text' => 'WHY JOEL WHY?

How will this program be assessed in years 1-3?',
                'netid' => 'nhavey',
                'deadline' => '2015-09-26 17:17:26.000000',
                'created_at' => '2015-09-20 17:17:26.000000',
                'questionable_id' => 164,
                'questionable_type' => 'App\\Project',
            ),
            313 => 
            array (
                'id_old' => 1999325,
                'question_text' => 'How would Stealth Cats initiatives uphold values of students as represented by the survey?',
                'netid' => 'tzaman',
                'deadline' => '2015-09-26 17:18:16.000000',
                'created_at' => '2015-09-20 17:18:16.000000',
                'questionable_id' => 164,
                'questionable_type' => 'App\\Project',
            ),
            314 => 
            array (
                'id_old' => 1999326,
                'question_text' => 'What is the potential or desired impact of a Faculty Fellow on your program goals?',
                'netid' => 'tzaman',
                'deadline' => '2015-11-16 18:10:00.000000',
                'created_at' => '2015-11-10 18:10:00.000000',
                'questionable_id' => 32,
                'questionable_type' => 'App\\Par',
            ),
            315 => 
            array (
                'id_old' => 1999327,
            'question_text' => 'How do plan to create a larger pool of mentors in the greater Tucson community (outside of the University of Arizona)? Please answer specifically in regards to matching the students who requested mentors in their chosen career field outside the university setting.',
                'netid' => 'nhavey',
                'deadline' => '2016-01-30 22:03:29.000000',
                'created_at' => '2016-01-24 22:03:29.000000',
                'questionable_id' => 195,
                'questionable_type' => 'App\\Project',
            ),
            316 => 
            array (
                'id_old' => 1999328,
                'question_text' => 'In terms of marketing and outreach, how do you intend to increase the pool of students interested in seeking mentorship through your program?',
                'netid' => 'nhavey',
                'deadline' => '2016-01-30 22:04:09.000000',
                'created_at' => '2016-01-24 22:04:09.000000',
                'questionable_id' => 195,
                'questionable_type' => 'App\\Project',
            ),
            317 => 
            array (
                'id_old' => 1999329,
            'question_text' => 'Who will be developing the application for SafeRide? (Professional developers, graduate students, student developers, etc.)',
                'netid' => 'nhavey',
                'deadline' => '2016-01-30 22:05:20.000000',
                'created_at' => '2016-01-24 22:05:20.000000',
                'questionable_id' => 192,
                'questionable_type' => 'App\\Project',
            ),
            318 => 
            array (
                'id_old' => 1999330,
                'question_text' => 'Can you please provide a bit of elaboration regarding the budget line items for the application\'s development? We were confused in particular with the second year.',
                'netid' => 'nhavey',
                'deadline' => '2016-01-30 22:05:52.000000',
                'created_at' => '2016-01-24 22:05:52.000000',
                'questionable_id' => 192,
                'questionable_type' => 'App\\Project',
            ),
            319 => 
            array (
                'id_old' => 1999331,
            'question_text' => 'Could we see a copy of the application for funding? (Or just an elaboration of the criteria that is taken in consideration when these grants are disbursed.)',
                'netid' => 'nhavey',
                'deadline' => '2016-01-30 22:07:49.000000',
                'created_at' => '2016-01-24 22:07:49.000000',
                'questionable_id' => 200,
                'questionable_type' => 'App\\Project',
            ),
            320 => 
            array (
                'id_old' => 1999332,
                'question_text' => 'How is this grant opportunity being marketed to graduate and undergraduate students? Is there a way to improve this marketing and outreach?',
                'netid' => 'nhavey',
                'deadline' => '2016-01-30 22:08:11.000000',
                'created_at' => '2016-01-24 22:08:11.000000',
                'questionable_id' => 200,
                'questionable_type' => 'App\\Project',
            ),
            321 => 
            array (
                'id_old' => 1999333,
                'question_text' => 'One of our primary concerns with this program was the staying power/consistency of a graduate student within the position. Will there eventually be professional staff member in the position so that graduate assistants do not cycle through too fast? Will a training program be devised?',
                'netid' => 'nhavey',
                'deadline' => '2016-01-30 22:09:46.000000',
                'created_at' => '2016-01-24 22:09:46.000000',
                'questionable_id' => 205,
                'questionable_type' => 'App\\Project',
            ),
            322 => 
            array (
                'id_old' => 1999334,
                'question_text' => 'Is there a demonstrated field-wide interest and correlated benefit to electronic badging and/or certificates? If so, could you please provide examples?',
                'netid' => 'nhavey',
                'deadline' => '2016-01-30 22:11:46.000000',
                'created_at' => '2016-01-24 22:11:46.000000',
                'questionable_id' => 179,
                'questionable_type' => 'App\\Project',
            ),
            323 => 
            array (
                'id_old' => 1999335,
                'question_text' => 'What are the circumstances that make it more favorable to use stipends for student employees rather than credits?',
                'netid' => 'tzaman',
                'deadline' => '2016-02-06 23:56:25.000000',
                'created_at' => '2016-01-31 23:56:25.000000',
                'questionable_id' => 175,
                'questionable_type' => 'App\\Project',
            ),
            324 => 
            array (
                'id_old' => 1999336,
                'question_text' => 'What is the role of student ambassadors given how Career Services is already very well-known?',
                'netid' => 'tzaman',
                'deadline' => '2016-02-07 00:15:54.000000',
                'created_at' => '2016-02-01 00:15:54.000000',
                'questionable_id' => 193,
                'questionable_type' => 'App\\Project',
            ),
            325 => 
            array (
                'id_old' => 1999343,
            'question_text' => 'Q: Could we see a copy of the application for funding? (Or just an elaboration of the criteria that is taken in consideration when these grants are disbursed.)',
                'netid' => 'bonnie',
                'deadline' => '2016-02-09 18:19:37.000000',
                'created_at' => '2016-02-03 18:19:37.000000',
                'questionable_id' => 200,
                'questionable_type' => 'App\\Project',
            ),
            326 => 
            array (
                'id_old' => 1999338,
                'question_text' => 'What initiatives are taken by Career Services to address the needs of students outside of business/retail, such as students in the arts, social sciences, or humanities?',
                'netid' => 'tzaman',
                'deadline' => '2016-02-07 00:30:37.000000',
                'created_at' => '2016-02-01 00:30:37.000000',
                'questionable_id' => 193,
                'questionable_type' => 'App\\Project',
            ),
            327 => 
            array (
                'id_old' => 1999339,
                'question_text' => 'How much is a workshop presenter compensated?',
                'netid' => 'tzaman',
                'deadline' => '2016-02-07 00:38:13.000000',
                'created_at' => '2016-02-01 00:38:13.000000',
                'questionable_id' => 201,
                'questionable_type' => 'App\\Project',
            ),
            328 => 
            array (
                'id_old' => 1999340,
                'question_text' => 'What steps are being taken to secure outside funding for the longevity of this program?',
                'netid' => 'tzaman',
                'deadline' => '2016-02-07 00:39:12.000000',
                'created_at' => '2016-02-01 00:39:12.000000',
                'questionable_id' => 202,
                'questionable_type' => 'App\\Project',
            ),
            329 => 
            array (
                'id_old' => 1999341,
                'question_text' => 'What steps are being taken to secure outside funding for the longevity of this program?',
                'netid' => 'tzaman',
                'deadline' => '2016-02-07 00:50:12.000000',
                'created_at' => '2016-02-01 00:50:12.000000',
                'questionable_id' => 199,
                'questionable_type' => 'App\\Project',
            ),
            330 => 
            array (
                'id_old' => 1999342,
                'question_text' => 'How many students use Career Services, and what are their demographics?',
                'netid' => 'tzaman',
                'deadline' => '2016-02-07 00:52:25.000000',
                'created_at' => '2016-02-01 00:52:25.000000',
                'questionable_id' => 193,
                'questionable_type' => 'App\\Project',
            ),
            331 => 
            array (
                'id_old' => 1999344,
                'question_text' => 'How is this grant opportunity being marketed to graduate and undergraduate students? Is there a way to improve this marketing and outreach?',
                'netid' => 'bonnie',
                'deadline' => '2016-02-09 18:19:54.000000',
                'created_at' => '2016-02-03 18:19:54.000000',
                'questionable_id' => 200,
                'questionable_type' => 'App\\Project',
            ),
            332 => 
            array (
                'id_old' => 1999345,
                'question_text' => 'How much is a workshop presenter compensated?',
                'netid' => 'bonnie',
                'deadline' => '2016-02-10 15:05:16.000000',
                'created_at' => '2016-02-04 15:05:16.000000',
                'questionable_id' => 201,
                'questionable_type' => 'App\\Project',
            ),
            333 => 
            array (
                'id_old' => 1999346,
                'question_text' => 'How much is a workshop presenter compensated?',
                'netid' => 'bonnie',
                'deadline' => '2016-02-10 17:05:08.000000',
                'created_at' => '2016-02-04 17:05:08.000000',
                'questionable_id' => 201,
                'questionable_type' => 'App\\Project',
            ),
            334 => 
            array (
                'id_old' => 1999347,
                'question_text' => 'How much is a workshop presenter compensated?',
                'netid' => 'twhetzel',
                'deadline' => '2016-02-10 17:35:01.000000',
                'created_at' => '2016-02-04 17:35:01.000000',
                'questionable_id' => 201,
                'questionable_type' => 'App\\Project',
            ),
            335 => 
            array (
                'id_old' => 1999349,
                'question_text' => 'Could you please elaborate on your partnership with UITS and the libraries in regards to additional funding?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-14 06:55:24.000000',
                'created_at' => '2016-02-08 06:55:24.000000',
                'questionable_id' => 206,
                'questionable_type' => 'App\\Project',
            ),
            336 => 
            array (
                'id_old' => 1999350,
                'question_text' => 'Approximately how many students does the entirety of this program serve? Is there a wide variety of unique students or are most events composed of the same students?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-14 06:56:25.000000',
                'created_at' => '2016-02-08 06:56:25.000000',
                'questionable_id' => 206,
                'questionable_type' => 'App\\Project',
            ),
            337 => 
            array (
                'id_old' => 1999351,
                'question_text' => 'How are you, specifically, working to engage non-STEM students in your program?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-14 06:56:47.000000',
                'created_at' => '2016-02-08 06:56:47.000000',
                'questionable_id' => 206,
                'questionable_type' => 'App\\Project',
            ),
            338 => 
            array (
                'id_old' => 1999352,
                'question_text' => 'How are the colleges that do not participate in PASS preventing student probation? Are there any funding mechanisms within the participating colleges that could mitigate costs for the program?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-14 06:57:57.000000',
                'created_at' => '2016-02-08 06:57:57.000000',
                'questionable_id' => 196,
                'questionable_type' => 'App\\Project',
            ),
            339 => 
            array (
                'id_old' => 1999353,
            'question_text' => 'Can you provide a clearer picture of retention? (IE: delineating what differences are caused by graduation, etc.)',
                'netid' => 'nhavey',
                'deadline' => '2016-02-14 06:58:28.000000',
                'created_at' => '2016-02-08 06:58:28.000000',
                'questionable_id' => 196,
                'questionable_type' => 'App\\Project',
            ),
            340 => 
            array (
                'id_old' => 1999354,
                'question_text' => 'Have you considered an appointment based scheduling system to mitigate random rush?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-14 07:02:23.000000',
                'created_at' => '2016-02-08 07:02:23.000000',
                'questionable_id' => 191,
                'questionable_type' => 'App\\Project',
            ),
            341 => 
            array (
                'id_old' => 1999355,
                'question_text' => 'Is there a correlation between employees and reduced wait time? What is the relationship, if any, between the number of employees working and the reduction in wait time?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-14 07:03:05.000000',
                'created_at' => '2016-02-08 07:03:05.000000',
                'questionable_id' => 191,
                'questionable_type' => 'App\\Project',
            ),
            342 => 
            array (
                'id_old' => 1999356,
                'question_text' => 'It appears that the wait time and number of students requesting customer service at the Financial Aid office has decreased in the past few years. Are there any efforts or programs to thank for this or is this incidental? If there are things effecting this positively, how can they be improved to reduce inflow?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-14 07:05:13.000000',
                'created_at' => '2016-02-08 07:05:13.000000',
                'questionable_id' => 191,
                'questionable_type' => 'App\\Project',
            ),
            343 => 
            array (
                'id_old' => 1999357,
                'question_text' => 'How will these improvements/programs be marketed to students to increase engagement and use?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-14 07:05:37.000000',
                'created_at' => '2016-02-08 07:05:37.000000',
                'questionable_id' => 187,
                'questionable_type' => 'App\\Project',
            ),
            344 => 
            array (
                'id_old' => 1999358,
                'question_text' => 'What is the likelihood you will receive the other funding you listed in your budget as possible?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-21 18:57:59.000000',
                'created_at' => '2016-02-15 18:57:59.000000',
                'questionable_id' => 185,
                'questionable_type' => 'App\\Project',
            ),
            345 => 
            array (
                'id_old' => 1999359,
                'question_text' => 'How many student employees are required to maintain the website?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-21 18:58:14.000000',
                'created_at' => '2016-02-15 18:58:14.000000',
                'questionable_id' => 185,
                'questionable_type' => 'App\\Project',
            ),
            346 => 
            array (
                'id_old' => 1999360,
            'question_text' => 'Is $1,000 a reasonable budget amount for individual hall programming? (Are there budgets available of the purchasing for programs?)',
                'netid' => 'nhavey',
                'deadline' => '2016-02-21 18:59:04.000000',
                'created_at' => '2016-02-15 18:59:04.000000',
                'questionable_id' => 177,
                'questionable_type' => 'App\\Project',
            ),
            347 => 
            array (
                'id_old' => 1999361,
                'question_text' => 'How will these changes be marketed campus-wide? Will participation be mandatory or optional for faculty and staff?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-21 18:59:52.000000',
                'created_at' => '2016-02-15 18:59:52.000000',
                'questionable_id' => 188,
                'questionable_type' => 'App\\Project',
            ),
            348 => 
            array (
                'id_old' => 1999362,
            'question_text' => 'How will this program prevent overlap with existing programs within Student Affairs? (Cultural Centers, LGBTQA+ Resource, Women\'s Resource, OASIS, SafeZone, etc.) Have these programs been contacted and investigated as already fulfilling the goals of this proposal?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-21 19:00:45.000000',
                'created_at' => '2016-02-15 19:00:45.000000',
                'questionable_id' => 188,
                'questionable_type' => 'App\\Project',
            ),
            349 => 
            array (
                'id_old' => 1999363,
                'question_text' => 'Can we have an action plan of how this will be implemented?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-21 19:01:26.000000',
                'created_at' => '2016-02-15 19:01:26.000000',
                'questionable_id' => 188,
                'questionable_type' => 'App\\Project',
            ),
            350 => 
            array (
                'id_old' => 1999364,
                'question_text' => 'How much of your funding comes from ASUA?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-21 19:01:53.000000',
                'created_at' => '2016-02-15 19:01:53.000000',
                'questionable_id' => 182,
                'questionable_type' => 'App\\Project',
            ),
            351 => 
            array (
                'id_old' => 1999365,
                'question_text' => 'What type of communications equipment are you requesting?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-21 19:03:19.000000',
                'created_at' => '2016-02-15 19:03:19.000000',
                'questionable_id' => 182,
                'questionable_type' => 'App\\Project',
            ),
            352 => 
            array (
                'id_old' => 1999366,
                'question_text' => 'Have you discussed the feasibility of half time wages for your staff? Have you clarified that this is even legal?',
                'netid' => 'nhavey',
                'deadline' => '2016-02-21 19:04:47.000000',
                'created_at' => '2016-02-15 19:04:47.000000',
                'questionable_id' => 182,
                'questionable_type' => 'App\\Project',
            ),
            353 => 
            array (
                'id_old' => 1999367,
                'question_text' => 'How many of the Peer Mentors continue on after working for a year?',
                'netid' => 'jasonharris',
                'deadline' => '2017-01-26 23:47:50.000000',
                'created_at' => '2017-01-20 23:47:50.000000',
                'questionable_id' => 218,
                'questionable_type' => 'App\\Project',
            ),
            354 => 
            array (
                'id_old' => 1999368,
                'question_text' => 'Is the increase in requested funds solely for student employees or other areas?',
                'netid' => 'jasonharris',
                'deadline' => '2017-01-26 23:48:22.000000',
                'created_at' => '2017-01-20 23:48:22.000000',
                'questionable_id' => 218,
                'questionable_type' => 'App\\Project',
            ),
            355 => 
            array (
                'id_old' => 1999369,
                'question_text' => 'What is the average attendance for movie showings and how is this tracked?',
                'netid' => 'jasonharris',
                'deadline' => '2017-01-26 23:48:56.000000',
                'created_at' => '2017-01-20 23:48:56.000000',
                'questionable_id' => 244,
                'questionable_type' => 'App\\Project',
            ),
            356 => 
            array (
                'id_old' => 1999370,
                'question_text' => 'Is there student input on which movies are to be shown?',
                'netid' => 'jasonharris',
                'deadline' => '2017-01-26 23:49:34.000000',
                'created_at' => '2017-01-20 23:49:34.000000',
                'questionable_id' => 244,
                'questionable_type' => 'App\\Project',
            ),
            357 => 
            array (
                'id_old' => 1999371,
                'question_text' => 'How many classes are offered in Gallagher Theater per year?',
                'netid' => 'jasonharris',
                'deadline' => '2017-01-26 23:49:58.000000',
                'created_at' => '2017-01-20 23:49:58.000000',
                'questionable_id' => 229,
                'questionable_type' => 'App\\Project',
            ),
            358 => 
            array (
                'id_old' => 1999372,
                'question_text' => 'Why does Gallagher Theater not receive classroom funding?',
                'netid' => 'jasonharris',
                'deadline' => '2017-01-26 23:50:24.000000',
                'created_at' => '2017-01-20 23:50:24.000000',
                'questionable_id' => 229,
                'questionable_type' => 'App\\Project',
            ),
            359 => 
            array (
                'id_old' => 1999373,
                'question_text' => 'Does the games room receive any external funding?',
                'netid' => 'jasonharris',
                'deadline' => '2017-01-26 23:50:45.000000',
                'created_at' => '2017-01-20 23:50:45.000000',
                'questionable_id' => 228,
                'questionable_type' => 'App\\Project',
            ),
            360 => 
            array (
                'id_old' => 1999374,
                'question_text' => 'Has Residence Life attempted to request funding from ASUA Club Funding for student travel?',
                'netid' => 'jasonharris',
                'deadline' => '2017-01-26 23:52:36.000000',
                'created_at' => '2017-01-20 23:52:36.000000',
                'questionable_id' => 222,
                'questionable_type' => 'App\\Project',
            ),
            361 => 
            array (
                'id_old' => 1999375,
                'question_text' => 'What is the difference between ASUA and GPSC club funding? Is it because of different things that can be funded?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-03 04:16:40.000000',
                'created_at' => '2017-01-28 04:16:40.000000',
                'questionable_id' => 211,
                'questionable_type' => 'App\\Project',
            ),
            362 => 
            array (
                'id_old' => 1999376,
                'question_text' => 'Have there been efforts between GPSC and ASUA to collaborate on the idea of club funding?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-03 04:16:52.000000',
                'created_at' => '2017-01-28 04:16:52.000000',
                'questionable_id' => 211,
                'questionable_type' => 'App\\Project',
            ),
            363 => 
            array (
                'id_old' => 1999377,
                'question_text' => 'Are the study abroad, alternative spring break, and gap experiences mentioned in the proposal performed through partnerships with existing departments, or new, separate initiatives?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-03 04:19:53.000000',
                'created_at' => '2017-01-28 04:19:53.000000',
                'questionable_id' => 231,
                'questionable_type' => 'App\\Project',
            ),
            364 => 
            array (
                'id_old' => 1999378,
                'question_text' => 'How much is the nutrition calculator used? Are there any metrics you can provide to the Board as to how much students use it, or elaborate on the survey mentioned in the proposal and how it was conducted?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-03 04:21:28.000000',
                'created_at' => '2017-01-28 04:21:28.000000',
                'questionable_id' => 230,
                'questionable_type' => 'App\\Project',
            ),
            365 => 
            array (
                'id_old' => 1999379,
                'question_text' => 'It was mentioned that food grown at the gardens would go to Campus Pantry and be used in food at the Student Unions. Is there any information about the differences in percentage, or differences in the types of food, that would be going to both Campus Pantry and the Unions?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-03 04:24:36.000000',
                'created_at' => '2017-01-28 04:24:36.000000',
                'questionable_id' => 236,
                'questionable_type' => 'App\\Project',
            ),
            366 => 
            array (
                'id_old' => 1999380,
                'question_text' => 'Can you elaborate on the cost of the Galaxy Digital website including its function and its decreasing cost over the years?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-03 04:25:29.000000',
                'created_at' => '2017-01-28 04:25:29.000000',
                'questionable_id' => 212,
                'questionable_type' => 'App\\Project',
            ),
            367 => 
            array (
                'id_old' => 1999381,
                'question_text' => 'In answering our previous question, you stated: "Last semester Gallagher Theater saw 802 attendees. On average Gallagher sees 120 – 145 attendees a week." The math behind this did not seem to add up, as 120-145 attendees per week would add up to far more than 802 for the semester. Can you offer some clarification on this? How is attendance tracked for Gallagher Theater?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-03 04:29:00.000000',
                'created_at' => '2017-01-28 04:29:00.000000',
                'questionable_id' => 244,
                'questionable_type' => 'App\\Project',
            ),
            368 => 
            array (
                'id_old' => 1999382,
                'question_text' => 'Are the study abroad, alternative spring break, and gap experiences mentioned in the proposal performed through partnerships with existing departments, or new, separate initiatives?',
                'netid' => 'twhetzel',
                'deadline' => '2017-02-07 22:36:52.000000',
                'created_at' => '2017-02-01 22:36:52.000000',
                'questionable_id' => 231,
                'questionable_type' => 'App\\Project',
            ),
            369 => 
            array (
                'id_old' => 1999383,
                'question_text' => 'What is the attendance/reach of programs listed in the Additional Documents? Do you have any metrics you could provide for us as to the success of these events?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-11 20:22:31.000000',
                'created_at' => '2017-02-05 20:22:31.000000',
                'questionable_id' => 247,
                'questionable_type' => 'App\\Project',
            ),
            370 => 
            array (
                'id_old' => 1999384,
                'question_text' => 'How effective is the online workshop component compared to the peer mentoring component?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-11 20:23:24.000000',
                'created_at' => '2017-02-05 20:23:24.000000',
                'questionable_id' => 233,
                'questionable_type' => 'App\\Project',
            ),
            371 => 
            array (
                'id_old' => 1999385,
                'question_text' => 'What sorts of things do the peer mentors get trained in, and what is their retention like?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-11 20:24:12.000000',
                'created_at' => '2017-02-05 20:24:12.000000',
                'questionable_id' => 233,
                'questionable_type' => 'App\\Project',
            ),
            372 => 
            array (
                'id_old' => 1999386,
                'question_text' => 'How do retention numbers of PASS participants compare to those that do not participate in the program?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-11 20:24:46.000000',
                'created_at' => '2017-02-05 20:24:46.000000',
                'questionable_id' => 233,
                'questionable_type' => 'App\\Project',
            ),
            373 => 
            array (
                'id_old' => 1999387,
                'question_text' => 'The PASS Punctuated program would start mid-semester, so is it going to be in-person, online, or both? What is the reasoning behind this?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-11 20:25:46.000000',
                'created_at' => '2017-02-05 20:25:46.000000',
                'questionable_id' => 233,
                'questionable_type' => 'App\\Project',
            ),
            374 => 
            array (
                'id_old' => 1999388,
            'question_text' => 'Can we get an elaboration on the breakdown of the student wages area? Are the director(s) and assistant director(s) still getting ASUA stipends? How will the money be divided between them?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-17 07:04:26.000000',
                'created_at' => '2017-02-11 07:04:26.000000',
                'questionable_id' => 217,
                'questionable_type' => 'App\\Project',
            ),
            375 => 
            array (
                'id_old' => 1999389,
                'question_text' => 'Is there a plan to integrate the alumni network with the Wildcat Joblink website at all? Is the separation because it is run through the Alumni association instead?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-17 07:06:30.000000',
                'created_at' => '2017-02-11 07:06:30.000000',
                'questionable_id' => 248,
                'questionable_type' => 'App\\Project',
            ),
            376 => 
            array (
                'id_old' => 1999390,
                'question_text' => 'How many unique people use SafeRide, if the information is available? How do you track passengers?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-25 23:00:20.000000',
                'created_at' => '2017-02-19 23:00:20.000000',
                'questionable_id' => 245,
                'questionable_type' => 'App\\Project',
            ),
            377 => 
            array (
                'id_old' => 1999391,
                'question_text' => 'What is the breakdown of the App Maintenance area of your budget? Why is this so high?',
                'netid' => 'jasonharris',
                'deadline' => '2017-02-25 23:00:38.000000',
                'created_at' => '2017-02-19 23:00:38.000000',
                'questionable_id' => 245,
                'questionable_type' => 'App\\Project',
            ),
            378 => 
            array (
                'id_old' => 1999392,
                'question_text' => 'How do students gain access to the fridge, and to the program in general?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-01-26 00:38:47.000000',
                'created_at' => '2018-01-20 00:38:47.000000',
                'questionable_id' => 265,
                'questionable_type' => 'App\\Project',
            ),
            379 => 
            array (
                'id_old' => 1999393,
                'question_text' => 'Please break down the $10,000 that is to go to employees and staffing. IE tasks, hours put in, etc.',
                'netid' => 'sixstringsent',
                'deadline' => '2018-01-26 00:40:20.000000',
                'created_at' => '2018-01-20 00:40:20.000000',
                'questionable_id' => 265,
                'questionable_type' => 'App\\Project',
            ),
            380 => 
            array (
                'id_old' => 1999394,
            'question_text' => 'Please provide further details on how line 47 ($9000) will be used. Specific examples would be greatly appreciated.',
                'netid' => 'sixstringsent',
                'deadline' => '2018-01-26 00:41:32.000000',
                'created_at' => '2018-01-20 00:41:32.000000',
                'questionable_id' => 289,
                'questionable_type' => 'App\\Project',
            ),
            381 => 
            array (
                'id_old' => 1999395,
                'question_text' => 'Please show the product mix between the rooftop garden and the garden outside the Nugent Building. IE what will be grown at each location.',
                'netid' => 'sixstringsent',
                'deadline' => '2018-01-26 00:42:28.000000',
                'created_at' => '2018-01-20 00:42:28.000000',
                'questionable_id' => 266,
                'questionable_type' => 'App\\Project',
            ),
            382 => 
            array (
                'id_old' => 1999396,
                'question_text' => 'What portion of the products grown will go to students, and what portion will be sold back to the University. Please include how profits will be used in your response.',
                'netid' => 'sixstringsent',
                'deadline' => '2018-01-26 00:43:26.000000',
                'created_at' => '2018-01-20 00:43:26.000000',
                'questionable_id' => 266,
                'questionable_type' => 'App\\Project',
            ),
            383 => 
            array (
                'id_old' => 1999397,
                'question_text' => 'The board would like to see construction blueprints to see how this will impact the UA Mall area, including what specific types of construction will be completed.',
                'netid' => 'sixstringsent',
                'deadline' => '2018-01-26 00:44:06.000000',
                'created_at' => '2018-01-20 00:44:06.000000',
                'questionable_id' => 266,
                'questionable_type' => 'App\\Project',
            ),
            384 => 
            array (
                'id_old' => 1999398,
                'question_text' => 'How does this grant program reach all faculty/advisors? Please expand your outreach plans.',
                'netid' => 'sixstringsent',
                'deadline' => '2018-01-26 00:44:48.000000',
                'created_at' => '2018-01-20 00:44:48.000000',
                'questionable_id' => 281,
                'questionable_type' => 'App\\Project',
            ),
            385 => 
            array (
                'id_old' => 1999399,
                'question_text' => 'In asking for a second GA, what will be the delegation of responsibility and roles within the framework of the proposal?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-01-26 00:45:46.000000',
                'created_at' => '2018-01-20 00:45:46.000000',
                'questionable_id' => 269,
                'questionable_type' => 'App\\Project',
            ),
            386 => 
            array (
                'id_old' => 1999400,
                'question_text' => 'The board would like clarification of Line 46, a breakdown of how $75,000 will be used each year.',
                'netid' => 'sixstringsent',
                'deadline' => '2018-01-26 00:46:36.000000',
                'created_at' => '2018-01-20 00:46:36.000000',
                'questionable_id' => 269,
                'questionable_type' => 'App\\Project',
            ),
            387 => 
            array (
                'id_old' => 1999401,
            'question_text' => 'How does the grant program reach out to faculty, so that they know about the Student Faculty Interaction Grant Program?  Please provide detail of your plans for outreach to faculty.  (This question was submitted by advisor Teresa Whetzel because Caitlin did not receive a link to respond to the question submitted by Stephen Westby.)',
                'netid' => 'twhetzel',
                'deadline' => '2018-01-28 17:21:00.000000',
                'created_at' => '2018-01-22 17:21:00.000000',
                'questionable_id' => 281,
                'questionable_type' => 'App\\Project',
            ),
            388 => 
            array (
                'id_old' => 1999402,
                'question_text' => 'The board would like a little more information about Line 48 - the Art & Culture Initiative. How will this $3000 be used?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:35:56.000000',
                'created_at' => '2018-01-29 05:35:56.000000',
                'questionable_id' => 250,
                'questionable_type' => 'App\\Project',
            ),
            389 => 
            array (
                'id_old' => 1999403,
                'question_text' => 'Is there a demonstrable impact to the student community as a direct result of marketing from initial funding?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:37:14.000000',
                'created_at' => '2018-01-29 05:37:14.000000',
                'questionable_id' => 250,
                'questionable_type' => 'App\\Project',
            ),
            390 => 
            array (
                'id_old' => 1999404,
                'question_text' => 'What is your intended marketing strategy?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:37:42.000000',
                'created_at' => '2018-01-29 05:37:42.000000',
                'questionable_id' => 250,
                'questionable_type' => 'App\\Project',
            ),
            391 => 
            array (
                'id_old' => 1999405,
                'question_text' => 'Is internal funding from Student Unions possible?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:38:38.000000',
                'created_at' => '2018-01-29 05:38:38.000000',
                'questionable_id' => 180,
                'questionable_type' => 'App\\Project',
            ),
            392 => 
            array (
                'id_old' => 1999406,
                'question_text' => 'What are your estimated revenues from food sales?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:39:03.000000',
                'created_at' => '2018-01-29 05:39:03.000000',
                'questionable_id' => 180,
                'questionable_type' => 'App\\Project',
            ),
            393 => 
            array (
                'id_old' => 1999407,
                'question_text' => 'What other sources of funding exist for this project outside SSFAB?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:40:27.000000',
                'created_at' => '2018-01-29 05:40:27.000000',
                'questionable_id' => 180,
                'questionable_type' => 'App\\Project',
            ),
            394 => 
            array (
                'id_old' => 1999408,
                'question_text' => 'Why is only one student being paid, despite there being 14 internships?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:41:51.000000',
                'created_at' => '2018-01-29 05:41:51.000000',
                'questionable_id' => 180,
                'questionable_type' => 'App\\Project',
            ),
            395 => 
            array (
                'id_old' => 1999409,
                'question_text' => 'What is the maximum attendance per session?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:42:14.000000',
                'created_at' => '2018-01-29 05:42:14.000000',
                'questionable_id' => 238,
                'questionable_type' => 'App\\Project',
            ),
            396 => 
            array (
                'id_old' => 1999410,
                'question_text' => 'How do students receive the free class?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:42:33.000000',
                'created_at' => '2018-01-29 05:42:33.000000',
                'questionable_id' => 238,
                'questionable_type' => 'App\\Project',
            ),
            397 => 
            array (
                'id_old' => 1999411,
                'question_text' => 'Have you accounted for potential abuse of the app?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:43:03.000000',
                'created_at' => '2018-01-29 05:43:03.000000',
                'questionable_id' => 242,
                'questionable_type' => 'App\\Project',
            ),
            398 => 
            array (
                'id_old' => 1999412,
                'question_text' => '$1 seems steep for stickers, when we’re looking at 8000 of them. Is there a reason for this steep price?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:43:53.000000',
                'created_at' => '2018-01-29 05:43:53.000000',
                'questionable_id' => 242,
                'questionable_type' => 'App\\Project',
            ),
            399 => 
            array (
                'id_old' => 1999413,
                'question_text' => 'Are there any opportunities for additional funding, independent of SSFAB?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-04 05:44:23.000000',
                'created_at' => '2018-01-29 05:44:23.000000',
                'questionable_id' => 268,
                'questionable_type' => 'App\\Project',
            ),
            400 => 
            array (
                'id_old' => 1999414,
                'question_text' => 'Thanks for answering the first round of questions. The board would like to ask if it will take three hours a week to send out emails and texts about the food availability? Is a listserv being used?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-11 04:17:32.000000',
                'created_at' => '2018-02-05 04:17:32.000000',
                'questionable_id' => 265,
                'questionable_type' => 'App\\Project',
            ),
            401 => 
            array (
                'id_old' => 1999415,
                'question_text' => 'Thanks for answering our first round of questions! One more - what happens to unused vouchers and coupons? Is there a way to recoup funds in the event that they aren\'t used?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-11 04:18:34.000000',
                'created_at' => '2018-02-05 04:18:34.000000',
                'questionable_id' => 289,
                'questionable_type' => 'App\\Project',
            ),
            402 => 
            array (
                'id_old' => 1999416,
                'question_text' => 'Will vouchers be purchased on a case-by-case basis or all together at once?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-11 04:19:13.000000',
                'created_at' => '2018-02-05 04:19:13.000000',
                'questionable_id' => 289,
                'questionable_type' => 'App\\Project',
            ),
            403 => 
            array (
                'id_old' => 1999417,
                'question_text' => 'What is the strategy in play to make HydroCats self-sustaining in the long-term?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-11 04:19:43.000000',
                'created_at' => '2018-02-05 04:19:43.000000',
                'questionable_id' => 180,
                'questionable_type' => 'App\\Project',
            ),
            404 => 
            array (
                'id_old' => 1999418,
                'question_text' => 'Thank you for answering our initial questions! One more came up in our review last week: Are you able to show your attendance numbers from before and after your initial funding?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-11 04:20:36.000000',
                'created_at' => '2018-02-05 04:20:36.000000',
                'questionable_id' => 250,
                'questionable_type' => 'App\\Project',
            ),
            405 => 
            array (
                'id_old' => 1999419,
                'question_text' => 'What is the projected impact of additional employees for future years?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-18 05:47:36.000000',
                'created_at' => '2018-02-12 05:47:36.000000',
                'questionable_id' => 277,
                'questionable_type' => 'App\\Project',
            ),
            406 => 
            array (
                'id_old' => 1999420,
                'question_text' => 'Are you adding any new programs in FY2019, and what is the cost of them?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-18 05:48:06.000000',
                'created_at' => '2018-02-12 05:48:06.000000',
                'questionable_id' => 277,
                'questionable_type' => 'App\\Project',
            ),
            407 => 
            array (
                'id_old' => 1999421,
                'question_text' => 'How many new employees will you hire, if approved?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-18 05:48:37.000000',
                'created_at' => '2018-02-12 05:48:37.000000',
                'questionable_id' => 277,
                'questionable_type' => 'App\\Project',
            ),
            408 => 
            array (
                'id_old' => 1999422,
                'question_text' => 'Please provide a distribution of employees and which programs they are responsible for.',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-18 05:49:07.000000',
                'created_at' => '2018-02-12 05:49:07.000000',
                'questionable_id' => 277,
                'questionable_type' => 'App\\Project',
            ),
            409 => 
            array (
                'id_old' => 1999423,
                'question_text' => 'Please provide the duties and responsibilities of a year-round GA.',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-18 05:49:31.000000',
                'created_at' => '2018-02-12 05:49:31.000000',
                'questionable_id' => 260,
                'questionable_type' => 'App\\Project',
            ),
            410 => 
            array (
                'id_old' => 1999424,
                'question_text' => 'Did you consider a FTE instead of a year-round GA? What made you go with the latter?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-18 05:50:00.000000',
                'created_at' => '2018-02-12 05:50:00.000000',
                'questionable_id' => 260,
                'questionable_type' => 'App\\Project',
            ),
            411 => 
            array (
                'id_old' => 1999425,
                'question_text' => 'Please provide details of the CGA Retreat expenses.',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-18 05:50:19.000000',
                'created_at' => '2018-02-12 05:50:19.000000',
                'questionable_id' => 260,
                'questionable_type' => 'App\\Project',
            ),
            412 => 
            array (
                'id_old' => 1999426,
                'question_text' => 'What was the impact of not receiving full funding last year?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-18 05:50:50.000000',
                'created_at' => '2018-02-12 05:50:50.000000',
                'questionable_id' => 259,
                'questionable_type' => 'App\\Project',
            ),
            413 => 
            array (
                'id_old' => 1999427,
                'question_text' => 'Please help us contrast 2016-2017 and 2017-2018. How many projects were funded, and how many are you on track for this year?',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-18 05:51:38.000000',
                'created_at' => '2018-02-12 05:51:38.000000',
                'questionable_id' => 259,
                'questionable_type' => 'App\\Project',
            ),
            414 => 
            array (
                'id_old' => 1999428,
                'question_text' => 'Please detail the roles and responsibilities of the student ambassadors.',
                'netid' => 'sixstringsent',
                'deadline' => '2018-02-18 05:52:12.000000',
                'created_at' => '2018-02-12 05:52:12.000000',
                'questionable_id' => 282,
                'questionable_type' => 'App\\Project',
            ),
            415 => 
            array (
                'id_old' => 1999429,
            'question_text' => 'Thank you for responding to the previous questions!  The Board has two additional questions.  Question 1:  What initiatives have you done (or are planned) during the 2017-2018 academic year that relate to students with disabilities?',
                'netid' => 'hauff',
                'deadline' => '2018-02-23 00:08:24.000000',
                'created_at' => '2018-02-17 00:08:24.000000',
                'questionable_id' => 260,
                'questionable_type' => 'App\\Project',
            ),
            416 => 
            array (
                'id_old' => 1999430,
            'question_text' => 'Question 2:  Why is this a 12-month GA position instead of a 9-month GA position (which is more common)?  What specific responsibilities will the GA have during the summer months?',
                'netid' => 'hauff',
                'deadline' => '2018-02-23 00:09:16.000000',
                'created_at' => '2018-02-17 00:09:16.000000',
                'questionable_id' => 260,
                'questionable_type' => 'App\\Project',
            ),
            417 => 
            array (
                'id_old' => 1999431,
                'question_text' => 'This proposal was funded with $15K from ASUA during the 2017-2018 year.  Will ASUA be providing that support for the 2018-2019 year?',
                'netid' => 'hauff',
                'deadline' => '2018-02-23 00:10:08.000000',
                'created_at' => '2018-02-17 00:10:08.000000',
                'questionable_id' => 279,
                'questionable_type' => 'App\\Project',
            ),
            418 => 
            array (
                'id_old' => 1999432,
                'question_text' => 'Please provide a breakdown of the specific personnel and costs in the Personal Services budget.  Also, a GA position is represented in the proposal text, but is not represented in the proposal budget or the other sources of funding.  Was this supposed to be in the budget, or is it covered from another source?',
                'netid' => 'hauff',
                'deadline' => '2018-02-23 00:11:56.000000',
                'created_at' => '2018-02-17 00:11:56.000000',
                'questionable_id' => 237,
                'questionable_type' => 'App\\Project',
            ),
            419 => 
            array (
                'id_old' => 1999433,
                'question_text' => 'What is the role of the new care coordinator?  What will that coordinator\'s work load look like?  What is the expected impact of this position with the student population?',
                'netid' => 'hauff',
                'deadline' => '2018-02-23 00:14:07.000000',
                'created_at' => '2018-02-17 00:14:07.000000',
                'questionable_id' => 237,
                'questionable_type' => 'App\\Project',
            ),
            420 => 
            array (
                'id_old' => 1999434,
                'question_text' => 'What is the Graduate Assistant\'s specific role?  What will the GA\'s duties & responsibilities be?',
                'netid' => 'hauff',
                'deadline' => '2018-02-23 00:14:55.000000',
                'created_at' => '2018-02-17 00:14:55.000000',
                'questionable_id' => 280,
                'questionable_type' => 'App\\Project',
            ),
            421 => 
            array (
                'id_old' => 1999435,
                'question_text' => 'What are the coverage areas and boundaries for the night security officers?  Is it just near residence halls or other areas of campus?  Is there is a map or list of coverage areas?',
                'netid' => 'hauff',
                'deadline' => '2018-02-23 00:16:49.000000',
                'created_at' => '2018-02-17 00:16:49.000000',
                'questionable_id' => 270,
                'questionable_type' => 'App\\Project',
            ),
            422 => 
            array (
                'id_old' => 1999436,
                'question_text' => 'How is the current GA funded?  Why can that funding not continue for next year?',
                'netid' => 'hauff',
                'deadline' => '2018-02-23 00:17:28.000000',
                'created_at' => '2018-02-17 00:17:28.000000',
                'questionable_id' => 272,
                'questionable_type' => 'App\\Project',
            ),
            423 => 
            array (
                'id_old' => 1999437,
            'question_text' => 'This proposal was funded with $15K from ASUA during the 2017-2018 year. Will ASUA be providing that support for the 2018-2019 year? (this question was sent again by Teresa Whetzel- because Kelly did not receive an email.)',
                'netid' => 'twhetzel',
                'deadline' => '2018-02-28 23:37:26.000000',
                'created_at' => '2018-02-22 23:37:26.000000',
                'questionable_id' => 279,
                'questionable_type' => 'App\\Project',
            ),
        ));
        
        
    }
}