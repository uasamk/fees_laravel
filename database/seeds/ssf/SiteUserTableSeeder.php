<?php

use Illuminate\Database\Seeder;

class SsfSiteUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('site_user')->delete();
        
        \DB::table('site_user')->insert(array (
            0 => 
            array (
                'site_id' => 2,
                'user_id' => 10,
            ),
            1 => 
            array (
                'site_id' => 2,
                'user_id' => 18,
            ),
            2 => 
            array (
                'site_id' => 2,
                'user_id' => 8,
            ),
            3 => 
            array (
                'site_id' => 2,
                'user_id' => 21,
            ),
            4 => 
            array (
                'site_id' => 2,
                'user_id' => 13,
            ),
            5 => 
            array (
                'site_id' => 2,
                'user_id' => 5,
            ),
            6 => 
            array (
                'site_id' => 2,
                'user_id' => 17,
            ),
            7 => 
            array (
                'site_id' => 2,
                'user_id' => 4,
            ),
            8 => 
            array (
                'site_id' => 2,
                'user_id' => 19,
            ),
            9 => 
            array (
                'site_id' => 2,
                'user_id' => 9,
            ),
            10 => 
            array (
                'site_id' => 2,
                'user_id' => 3,
            ),
            11 => 
            array (
                'site_id' => 2,
                'user_id' => 20,
            ),
            12 => 
            array (
                'site_id' => 2,
                'user_id' => 11,
            ),
            13 => 
            array (
                'site_id' => 2,
                'user_id' => 7,
            ),
            14 => 
            array (
                'site_id' => 2,
                'user_id' => 15,
            ),
            15 => 
            array (
                'site_id' => 2,
                'user_id' => 2,
            ),
            16 => 
            array (
                'site_id' => 2,
                'user_id' => 14,
            ),
            17 => 
            array (
                'site_id' => 2,
                'user_id' => 12,
            ),
            18 => 
            array (
                'site_id' => 2,
                'user_id' => 6,
            ),
            19 => 
            array (
                'site_id' => 2,
                'user_id' => 16,
            ),
            20 => 
            array (
                'site_id' => 2,
                'user_id' => 1,
            ),
        ));
        
        
    }
}