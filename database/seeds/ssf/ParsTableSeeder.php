<?php

use Illuminate\Database\Seeder;

class SsfParsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pars')->delete();
        
        \DB::table('pars')->insert(array (
            0 => 
            array (
                'id_old' => 19991,
                'netid' => 'kmbeyer',
                'changes' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'advice' => 'Test of PAR system.

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
                'approved' => 0,
                'vp_comments' => 'Test of PAR system

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  xxxxxxxxxxxxxxxxxxxxxxxxxx',
                'vp_approved' => 0,
                'created_at' => '2011-11-28 18:12:55.000000',
                'timestamp_review' => '2012-11-20 21:33:06.000000',
                'netid_review' => 'twhetzel',
                'timestamp_vp' => '2012-11-20 21:34:20.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 62,
            ),
            1 => 
            array (
                'id_old' => 19999,
                'netid' => 'krista',
                'changes' => 'In August of 2012, the Program Director of the Women\'s Resource Center took an unpaid absence during her Maternity Leave. This resulted in available wage and ERE funds in the WRC 2658410 account, funded by SSF. 
The WRC requests that the ERE funds, in the amount of $4465.68, be returned to the SSF to offset the Graduate Assistant ERE increase that SSF has generously covered for the WRC.
However, the WRC requests that the wage funds, in the amount of $5510.53, be reallocated for use toward the 2nd annual NEW Leadership™ Arizona. 

Reasons:
1. UA is among a few select universities chosen by Rutgers to become a national development partner of the NEW Leadership™ summer institute program, whose focus is to promote women’s leadership in politics. 2012 was the inaugural year, and we had tremendous community support. However, we are seeking to expand this year-- from three days to four, from 16 students to 20, as well as add Dine College to our list of participating schools. Because NEW Leadership™ Arizona brings positive local and national attention to the UA, the WRC would benefit from having this salary savings to ensure quality expanded programming at NEW Leadership™ Arizona. 
2. These proposed changes are consistent with the original intent of the proposal, which was to help foster the work of the Women’s Resource Center in its outreach to students and its offering of courses and programming that help to promote a campus community of inclusion and gender equity. No programming highlights this more than NEW Leadership Arizona, which specifically promotes gender equity in politics among students at Arizona universities and colleges, while drawing positive national attention to the University of Arizona.  
3. This request is being put forth under the presumption that the SSFAB originally designated this salary money to ensure quality programming of the Women’s Resource Center. Although this money will now be reallocated, its aim and purpose will remain the same—enabling the WRC to provide quality programming that promotes gender equity issues among UA students, and beyond.
4. UA is part of a national development network of 22 campuses. NEW Leadership™ graduates go on to become elected officials, campaign managers, congressional staffers, issue advocates, and leaders in their communities and on campus.
5. The additional benefits gained from this reallocation of funds would allow us to continue to expand this program, moving us closer to the high quality programming that other universities offer.


TOTAL REQUEST for REALLOCATION from wages to general budget: $5510.53',
                'advice' => 'It is the boards opinion that this PAR be approved to move the salary money and it be used towards the cost of the Leadership conference. It is our opinion that in falls in line with the original intent of the WRC application',
                'approved' => 1,
                'vp_comments' => 'This PAR is approved by Melissa Vito on 12/18/12 as recommended by the Board.',
                'vp_approved' => 1,
                'created_at' => '2012-11-06 18:51:52.000000',
                'timestamp_review' => '2012-12-14 22:48:56.000000',
                'netid_review' => 'jevans91',
                'timestamp_vp' => '2012-12-19 16:21:10.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 47,
            ),
            2 => 
            array (
                'id_old' => 19992,
                'netid' => 'claudia',
                'changes' => 'Safe Ride recently made a change to their current approved proposal to purchase a replacement vehicle for $9,500.00, which appeared in an equipment object code. We would like to reduce personnel budget by $9,500.00 to offset this cost. 
This was an emergency purchase due to the fact that two Safe Ride vehicles were not repairable. At this time, we are not requesting additional funds from the Student Fee Committee and we feel we will not overspend on this account.',
                'advice' => 'Dr. Vito,
The SSFAB received this PAR from ASUA Safe Ride requesting a shift of $9,500 from the personnel budget to pay for the replacement costs of a broken car.  The Board was assured that this shift in money would not impact actual student wages and since the cars are required for the basic program function the Board felt that there was no reason to reject this PAR.

The Board voted 9-0 in favor of this PAR during the 4/20/12 open board meeting.',
                'approved' => 1,
                'vp_comments' => 'Approved by Melissa Vito on 4/26/12.',
                'vp_approved' => 1,
                'created_at' => '2012-02-17 19:20:13.000000',
                'timestamp_review' => '2012-04-25 19:36:37.000000',
                'netid_review' => 'altomare',
                'timestamp_vp' => '2012-04-27 17:43:45.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 57,
            ),
            3 => 
            array (
                'id_old' => 19993,
                'netid' => 'vanarsde',
                'changes' => 'Request
Carry over to 2012-2013 the unused balance of the 2011-2012 allocation for SF12.01, Campus Neighborhoods Night Security.

Summary
An unused balance of approximately $83,600 is expected in this account at the end of the current fiscal year. Approval is requested to carry over the unused balance from 2011-2012 to 2012-2013.

Background
•	In spring, 2011, the SSFAB recommended, and Vice President Vito approved, a total allocation of $156,000 to pilot an increase in late night UAPD presence around campus housing.  Analysis was subsequently conducted that showed a targeted need for additional officers Thursday, Friday, and Saturday nights between the hours of 10:00PM and 3:00AM, occurring primarily in the southwest portion of campus.  Two officers were assigned accordingly.  Additional officers were sought, but were not available.
•	Early fall semester data showed that the area of greatest need had shifted from the southwest area to the Highland corridor.  Officer placement on campus shifted accordingly.
•	Per the Mid-Year Report, the additional officers are related to fewer police call-outs and fewer residence hall conduct incidents.  Campus Safety has improved.  These improvements are documented in the Fall 2010-2011 Conduct Comparison Report (Residence Life Internal Document, Jan. 2012), attached.
•	Because only two officers were available for the added Late Night duty, significant savings have occurred this year.  Expenses to date have been about $52,400, and expenses for the entire year are expected to total about $72,400.  This will leave an unused balance of approximately $83,600.
•	Carrying over the unused balance to 2012-2013 would enable a program that has now been successfully piloted to continue for another year at no added cost.
•	The original request indicated that SSF funding was requested for only one year, and that funding beyond the first year would be covered by other sources.  It was anticipated that Residence Life would cover the long-term funding need.  However, campus and state-wide issues require Residence Life to keep any rate increase for 2012-2013 to an absolute minimum.  Consequently, Residence Life funding will not be available next year as anticipated, and therefore a carryover of funds is being sought.',
                'advice' => 'Dr. Vito,
The SSFAB received a second PAR from Campus Neighborhoods Night Security requesting to carry-over $83,600 that was unused during Fiscal Year (FY)2012 to be used for FY2013.  This was one of the most nuanced PAR discussions that the Board has ever carried.  The Board was divided between principle and practicality, the merits of the program and the importance of precedence.  

Those in favor of the PAR advocated the following in summary:
- Students will not have a voice in where these funds are allocated if the PAR is rejected on principle.
- There are special circumstances surrounding this PAR that has prohibited it from entering the normal application process.
- The program demonstrated through hard quantitative data that it has helped improve campus security.
- Precedence of the Board is only relevant until a new iteration of the Board is seated.

Those against the PAR advocated the following for rejection in summary:
- This PAR is using the alteration request system to backdoor in funding for the next fiscal year outside of the application process.
- Regardless of the specific circumstances this program is threatening to undermine the integrity and transparency of the allocation process.
- The program has likely resulted in more student arrests/tickets which may overstep the Board\'s duty of representing student needs.
- The precedent the board sets on this PAR decision will directly effect the decision of PAR SF11.25P2.

Board members in favor of approving or rejecting this PAR held some, none or all of the discussion points listed above.

The Board voted 3-6 against this PAR during the 4/20/12 open board meeting.',
                'approved' => 0,
                'vp_comments' => 'Melissa Vito supports the Board recommendation to disapprove this PAR as of 4-26-12.',
                'vp_approved' => 0,
                'created_at' => '2012-02-23 22:28:29.000000',
                'timestamp_review' => '2012-04-25 20:07:44.000000',
                'netid_review' => 'altomare',
                'timestamp_vp' => '2012-04-27 17:50:05.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 40,
            ),
            4 => 
            array (
                'id_old' => 19994,
                'netid' => 'rsaxby1',
                'changes' => 'March 27, 2012

RE: PAR for SSFAB Grant ID# SF11.25P2

Dear Student Services Fee Advisory Board,

The Graduate and Professional Student Council (GPSC) respectfully submits this Program Alteration Request (PAR) to the Student Services Fee Advisory Board (SSFAB) to allow the GPSC to roll-over and keep $766.00 of FY2012 SSF-supported Professional Opportunity Development (POD) funding dollars.  This money will be used to support an event hosted by the Society of Professional Journalists Student Chapter (SPJ) (UA School of Journalism) in FY2013 (Fall 2012).  The GPSC cannot pay the SPJ until expenses are incurred.  Due to this fact, the GPSC is submitting this PAR so that FY2012 funds can roll-over to the GPSC’s FY2013 budget.  If this PAR request is approved, SPJ will not receive the money until they incur approved, event-related expenses.  The $766.00 request is in addition to the $20,000 in POD funds that SSFAB pledged to GPSC for FY2013.

On January 25, 2012, the GPSC approved a POD fund request from the SPJ.  The total amount pledged to this group was $766.00 (see Table 1 for breakdown of expenses to be paid by GPSC) for an event to take place from March 20-22, 2012.  The SPJ will bring a prominent English-language blogger from Egypt to the UA campus to discuss the global role of activist bloggers and social media in the “Arab Spring,” as well as the outlook for Egypt’s political future.  Among other smaller events and lectures, the SPJ will host a large public lecture in the Gallagher Theater.

On March 12, 2012, the GPSC was notified that the SPJ will be unable to host the event in the Spring 2012 semester.  The SPJ is postponing the event until the Fall 2012 semester (dates TBD) because the speaker Mahmoud Salem, the blogger of sandmonkey.com, suffered severe nerve damage when police attacked protesters in Cairo.  The SPJ has requested that the GPSC defer its pledge of $766.00 until Fall 2012; the GPSC supports this request.

This request is consistent with the original proposal that GPSC submitted to SSFAB (ID# SF11.25P2) for FY2011 and FY2012.  The original grant from SSFAB was $15,000 per year, for two years.  The intended use of the funds has not changed; the only requested change is the timeframe that FY2012 funds will be used.  This rationale for this request is explained above, but the simple fact is, the GPSC-funded SPJ event cannot physically take place in FY2012 and must be postponed until FY2013.  The GPSC does not want to use the $20,000 we will receive from SSFAB to fund this event because we want to have the largest pot of money available to sponsor POD requests from student groups for the 2012-2013 academic year.  The POD grant program administered by the GPSC is very well-received by the graduate and professional student body.  It is extremely popular and the number of POD applications the GPSC receives each grant round has grown.  Please see the FY2013 SSFAB application for a more robust illustration of the success of the program.  Obviously, if SSFAB approves this PAR, the positive result will be two-fold.  First, the SPJ is assured funding to host an event that will interest many students on the UA campus.  Second, in FY2013, the GPSC will not have to dip into the $20,000 that SSFAB will give the GPSC for POD events—this will ensure we have the largest pot of money available to support additional student groups and events in the next academic year.

Please see Tables 2 and 3 for a look at the original FY2012 and proposed revision to the FY2013 budget, respectively.

The GPSC thanks the SSFAB for its consideration to approve this PAR and welcomes any questions.

Respectfully submitted,

Ryan Saxby, Program Coordinator
Graduate and Professional Student Council
Dean of Students Office',
                'advice' => 'Dr. Vito,
The SSFAB received this PAR from GPSC Professional Opportunity Development Grants requesting to carry-over $766 that was unused during Fiscal Year (FY)2012 to be used for FY2013.  The discussion of this PAR was undeniably tied to the discussion of SF12.01P1VP1 PAR 2.

The stakes were much lower for this PAR then for the Campus Security PAR mentioned above.  Both the dollar amount requested to be carried over was much smaller and this program was already approved for funding for FY2013.  The Board sympathized with the reasons for requesting carry-over funding (as with the Campus Security Program) and discussion was held along similar lines to the Campus Security Program.

However the fact this program already has funding for FY2013, made rejecting this PAR much easier to justify.

The Board voted 2-7 against this PAR during the 4/20/12 open board meeting.',
                'approved' => 0,
                'vp_comments' => 'Melissa Vito supports the Board recommendation to disapprove this PAR as of 4-26-12.',
                'vp_approved' => 0,
                'created_at' => '2012-03-27 17:54:38.000000',
                'timestamp_review' => '2012-04-25 20:14:05.000000',
                'netid_review' => 'altomare',
                'timestamp_vp' => '2012-04-27 17:50:54.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 27,
            ),
            5 => 
            array (
                'id_old' => 19995,
                'netid' => 'cls',
                'changes' => 'I would like to respectfully request an adjustment to the personnel related portion of the allocated funds for the PASS Probation Program.  My request is to fund a full-time coordinator position for the fiscal year with 4 graduate assistants and 23 peer advisors instead of 6 graduate assistants and 25 peer advisors. The proposed adjustment will not only stay true but enhance the original intent of the proposal.  

In planning the implementation of the program it has become clear that there is a need for a full-time professional staff member to lead and supervise the PASS student staff including graduate students and peer advisors.  Our goal is to offer a great opportunity for growth and development for all student staff as well as providing the best services to students participating in the program. The goal can be accomplished by hiring a qualified professional staff person.  The coordinator will assist in maintaining the essential partnerships created with the targeted colleges, provide the level of knowledge required in navigating university policies and procedures and also monitor the aspects of the job that are sensitive in nature. In addition, the coordinator will play an integral role in assessing the program in order to identify the key outcomes to demonstrate the effectiveness of the services in the short term to determine the value of the program in the long term. We understand the funding was granted for one year and there is not a guarantee of future funding however a professional staff position would allow for more effective implementation of the program put forth.  We plan to hire the coordinator as an Extended Temporary Employee.  Please see the proposed job descriptions:

Coordinator
•Develop, coordinate and implement PASS Probation Program  
•Select, train and supervise graduate assistants and peer advisors
•Conduct weekly staff meetings to discuss progress and development of program
•Provide reports on progress of students and number of students served
•Develop workshop series curriculum for PASS students
•Meet with students individually to design individual success plans and provide resources and referrals students
•Assist with the development of a comprehensive assessment plan including quantitative and qualitative analysis of student participation and improvement
•Serve as liaison to college partners for PASS

Graduate Assistants
•Assist in the supervision and training of peer advisors with direction from the coordinator/learning specialist 
•Facilitate probationary workshops 
•Assist in the development of the curriculum for an 8 week workshop series related to strategies for success as a college student
•Provide one-on-one service for 100 students outside of workshop 
•Assist in the development and coordination of individualized success plans 
•Administer follow-up to students receiving probationary programming 
•Attend weekly staff meetings
•Refer students to appropriate resources on campus
•Track contact with students in database
•Enter data and assist in data analysis for Academic Success & Achievement  

Peer Advisors
•Serve as peer mentors and on-campus resources for first year-students
•Facilitate personal, academic, and social transitions from high school in to college life
•Perform one-on-one meetings with 15 to 20 students on academic probation
•Co-facilitate 8 weekly workshops for students on academic probation
•Refer mentees to appropriate campus resources as needed
•Help mentees track their academic progress throughout the semester and refer to graduate assistant if necessary
•Track student contact information

Attached please find a revised budget for PASS.  Thank you for your consideration and once again we appreciate the opportunity to provide this service to students.',
                'advice' => 'Dr. Vito,
The SSFAB received this PAR from the Pathway to Academic Student Success (PASS) Probation Program requesting to use graduate and undergraduate staff funding to hire a full-time coordinator.  This received mixed results from the Board as some members were not wholly impressed with the original application and were pleased to see the applicants already reevaluating and making improvements.  Otherwise were concerned to see student funding being diverted away from directly supporting students.  The discussion was largely divided along these lines.

Ultimately the majority of the board decided that the overall program success was more important for student needs then the relative (yet still important) benefits of employment for the student employees.

The Board voted 8-1 in favor of this PAR at the 4/20/2012 open board meeting.',
                'approved' => 1,
                'vp_comments' => 'Approved by Melissa Vito on 4/26/12',
                'vp_approved' => 1,
                'created_at' => '2012-03-30 20:44:27.000000',
                'timestamp_review' => '2012-04-25 20:20:50.000000',
                'netid_review' => 'altomare',
                'timestamp_vp' => '2012-04-27 17:45:14.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 71,
            ),
            6 => 
            array (
                'id_old' => 19996,
                'netid' => 'claudia',
                'changes' => 'Safe Ride recently made a change to their current approved proposal to purchase a replacement vehicle for $7,500.00, which appeared in an equipment object code.  Due to unforeseen vehicle breakdown, Safe Ride had to purchase a car to continue the efficency of the program.  We would like to request $7,5000 to be moved from operations to capital to
cover expense. Safe Ride will not over-draw from the original budgeted amount and we will NOT reduce our student payroll or student hours for April and May because of this purchase.  Any questions, please let us know. Thank you.',
                'advice' => 'Dr. Vito,
The SSFAB received this PAR from ASUA Safe Ride requesting a shift of $7,500 from the operations budget to pay for the replacement costs of another broken car.  Similarly to Item 12.18F3, the Board was again assured that this shift in money would not impact actual student wages and since the cars are required for the basic program function the Board felt that there was no reason to reject this PAR.  One member objected to the PAR on the grounds that it was submitted very late in the Board processes and gave the Board little time to review the claim thoroughly.

The Board voted 7-1 in favor of this PAR during the 4/20/12 open board meeting.',
                'approved' => 1,
                'vp_comments' => 'Approved by Melissa Vito on 4/26/12',
                'vp_approved' => 1,
                'created_at' => '2012-04-18 23:03:22.000000',
                'timestamp_review' => '2012-04-25 19:40:40.000000',
                'netid_review' => 'altomare',
                'timestamp_vp' => '2012-04-27 17:44:34.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 57,
            ),
            7 => 
            array (
                'id_old' => 19997,
                'netid' => 'lorit',
            'changes' => 'On behalf of the Student Services Fee JEDI Committee, we would like to request a Program Alteration Request (PAR) for two areas in our proposal. The first alteration would be to reallocate some of the funding of the SSF grant to hire a graduate student for 0.25 assistantship that could lead the program. After much planning and discussion, we are proposing this option due to the extensive time to oversee this program and we feel that a GA position is necessary to develop and facilitate this program to its fullest potential. Currently, the areas included in the program proposal function with only one full time staff member. By dedicating a GA to this program, the alternation will be able strengthen the original intent of the program, creating more focused development of the students and events of the program. This will be the first year of the JEDI Program’s existence, but there are many components of the program that have been successful for the cultural centers, WRC, and LGBTQ Affairs in the past. The funding of this new position will be split between two areas within the granted budget. 

•	GA Duties & Responsibilities
o	Lead the cross departmental team of JEDI Interns.
	Social Justice/Intersectional Leadership Development
	Facilitating weekly team meetings with JEDI Interns
	Coordinating and facilitating Intern trainings
	Meet with SSF Directors once a month for updates
	Meet with Jedi Director to discuss progress and updates

o	Plan and coordinate at least one social justice speaker per semester
that aligns with the intersections of the areas involved in the JEDI
Program. 
	Lead and facilitate JEDI campus wide workshops in conjunction with
mission of JEDI Program.
	Manage and keep detailed reports of the JEDI Budget.
	Develop evaluations and assessment for the JEDI Program including all
activities.

•	.25 (10 hours per week) GA position 
o	6750.00 Wages + 2545.00 ERE = $9295.00. 

Initiative	                Proposed Budget PAR Alteration Grad. Assistant
Prof. Development & Training    $5,000	        $300	       $4,700
Credit Bearing Internship	$5,000	        $5,000	       $0
Paid Internship (6 x $2000)	$12,000	        $12,000	       $0
Campus Wide Events	        $18,000	        $13,405	       $4,595
TOTAL	                        $40,000	        $30,705	       $9,295

The second alteration we would like to request is the change of the Principle Investigator (PI) for this proposal. We request this change in lieu of the former PI’s change in position. Lori Tochihara has moved positions and is no longer working with the cultural center or the JEDI Program. We would like to request the PI position to be changed to Dan Xayaphanh, Asian Pacific American Student Affairs Program Director, who will be leading the grant. This alteration should not change the original intention of the program and would allow for a more efficient structure, since Lori Tochihara is no longer working in the area. 

Thank you ahead of time for your consideration and we look forward to hearing from you soon. If you have any questions or concerns, please feel free to contact Dan at danthaix@email.arizona.edu. 

A PDF of this proposal is attached for easier viewing.',
                'advice' => 'Dan Xayaphanh requested that this PAR be withdrawn, as different funding has been secured for the requested change.  The PAR is no longer necessary.  The JEDI project funding remains as originally approved by the Board.',
                'approved' => 0,
                'vp_comments' => 'PAR was withdrawn by Project Contact',
                'vp_approved' => 0,
                'created_at' => '2012-07-31 18:26:29.000000',
                'timestamp_review' => '2012-09-17 17:32:58.000000',
                'netid_review' => 'twhetzel',
                'timestamp_vp' => '2012-09-17 17:39:06.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 63,
            ),
            8 => 
            array (
                'id_old' => 19998,
                'netid' => 'aweddle14',
                'changes' => 'I am submitting this PAR form to request a small reallocation of my allotted funds to permit me and one of my committee members to attend the 2012 NACA West Regional Conference. The total amount requested to attend this conference, including conference fees, hotel, and airfare, is $1,500. While this is a substantial amount, I would like to stress that this accounts for only 1.5% of our annual budget, and I believe it will prove a very lucrative investment in terms of student benefit because of quality of the events we will be able to host here on campus. 

The NACA West Regional Conference takes place in mid-November and has been proven to be a vital asset to the Wildcat Events Board for many years. It is an event designed specifically to benefit campus activities boards and event planners by exposing them to various performances geared towards the college community, providing networking opportunities with our peers from other institutions across the country,  as well as teaching useful skills such as the benefits of proper negotiation and block-booking. 

Attending this conference will directly benefit the Speakers Committee by allowing us to increase the buying power of our campus programming dollars, and by doing so, benefit the campus community in the greatest way possible. 
So far, our plan for this year has been to save our biggest speaker of the year for a large performance in the Spring semester. Along with this large act, we also plan on having 2-3 medium-sized speaking engagements during this time. Though my entire committee has been working diligently scouting talent thus far, I cannot stress enough the benefit this would bring to my committee and the overall quality and quantity of the events we are able to put on. 

My vision for this year has been to bring the largest and most-attended events that the Wildcat Events Board has seen thus far. In staying in line with that goal, my committee and I have allocated the brunt of our budget to the Spring Semester, planning frugal, and yet effective events for the fall semester. To illustrate this I have attached to this PAR form a simple excel sheet outlining our events and budgeted events thus far for the fall semester. 

I strongly believe that attending the NACA West Conference will be extremely beneficial to my committee, the events we put on for the campus community, and by association the student body as a whole. Please take all of this into account as you consider my reallocation request, thank you.',
                'advice' => 'It is the Board\'s recomendation that this PAR be approved. We feel that the director and his committee member attending this conference would be beneficial in their search for speakers to bring to the UA campus. However, it should be noted that the board has expressed concern that such a small amount of money has been allocated to programming this semester. We look forward to seeing the progress report of this program.',
                'approved' => 1,
                'vp_comments' => 'This PAR is approved by Melissa Vito on 12/18/12 as recommended by the Board.',
                'vp_approved' => 1,
                'created_at' => '2012-10-12 14:58:46.000000',
                'timestamp_review' => '2012-10-16 00:22:09.000000',
                'netid_review' => 'jevans91',
                'timestamp_vp' => '2012-12-19 16:19:38.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 64,
            ),
            9 => 
            array (
                'id_old' => 199910,
                'netid' => 'kmbeyer',
                'changes' => 'Adding PAR that was previously submitted in paper format to online system for consistency of archives. Added on 11/19/2012 but backdated to reflect date of original PAR.',
                'advice' => 'On December 2, 2011 this PAR was reviewed at the open Board Meeting.  The Board voted to approve both request 1 & 2 provided in the PAR and recommended that Melissa Vito the Vice President for Student Affairs approve this PAR.


Note:  This Par was submitted on Sep 9, 2011 before the on-line PAR process was in place.  It is being recorded on the website at this time so that the on-line records reflect all of the FY2012 activity for this project.',
                'approved' => 1,
                'vp_comments' => 'Melissa Vito approves both Request 1 & 2 for this PAR as recommended by the Board.

Note: This Par was submitted on Sep 9, 2011 before the on-line PAR process was in place. It is being recorded on the website at this time so that the on-line records reflect all of the FY2012 activity for this project.',
                'vp_approved' => 1,
                'created_at' => '2011-09-09 19:00:00.000000',
                'timestamp_review' => '2012-11-20 15:22:13.000000',
                'netid_review' => 'twhetzel',
                'timestamp_vp' => '2012-11-20 15:43:05.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 60,
            ),
            10 => 
            array (
                'id_old' => 199911,
                'netid' => 'kmbeyer',
                'changes' => 'Adding PAR that was previously submitted in paper format to online system for consistency of archives. Added on 11/19/2012 but backdated to reflect date of original PAR.',
                'advice' => 'On Decemebr 2, 2011 this PAR was reviewed at the open Board Meeting.  The Board voted to approve both requests 1 & 2 and recommended that Melissa Vito, the Vice President for Student Affairs, approve this PAR.


Note: This Par was submitted on Nov 16, 2011 before the on-line PAR process was in place. It is being recorded on the website at this time so that the on-line records reflect all of the FY2012 activity for this project.',
                'approved' => 1,
                'vp_comments' => 'Melissa Vito approves both request 1 &2 for this PAR as recommended by the Board.

Note: This Par was submitted on Nov 16, 2011 before the on-line PAR process was in place. It is being recorded on the website at this time so that the on-line records reflect all of the FY2012 activity for this project.',
                'vp_approved' => 1,
                'created_at' => '2011-11-16 19:00:00.000000',
                'timestamp_review' => '2012-11-20 16:01:37.000000',
                'netid_review' => 'twhetzel',
                'timestamp_vp' => '2012-11-20 16:05:32.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 40,
            ),
            11 => 
            array (
                'id_old' => 199912,
                'netid' => 'kmbeyer',
                'changes' => 'Adding PAR that was previously submitted in paper format to online system for consistency of archives. Added on 11/19/2012 but backdated to reflect date of original PAR.',
                'advice' => 'On December 2, 2011 this PAR was reviewed at the open Board Meeting. The Board voted to approve  request 1 but did not approve request #2 as provided in the PAR and recommended that Melissa Vito the Vice President for Student Affairs approve request 1 and deny request 2 for this PAR.

Note: This Par was submitted on Oct 4, 2011 before the on-line PAR process was in place. It is being recorded on the website at this time so that the on-line records reflect all of the FY2012 activity for this project.',
                'approved' => 1,
                'vp_comments' => 'Melissa Vito approves request 1 and denies request 2 for this PAR as recommended by the Board.

Note: This Par was submitted on Oct 4, 2011 before the on-line PAR process was in place. It is being recorded on the website at this time so that the on-line records reflect all of the FY2012 activity for this project',
                'vp_approved' => 1,
                'created_at' => '2011-10-04 19:00:00.000000',
                'timestamp_review' => '2012-11-20 19:37:45.000000',
                'netid_review' => 'twhetzel',
                'timestamp_vp' => '2012-11-20 21:21:51.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 47,
            ),
            12 => 
            array (
                'id_old' => 199913,
                'netid' => 'rudyp',
            'changes' => 'We would like to transfer five-thousand dollars ($5000) from this project to our Travel Grants for Graduate & Professional Students because of the sharp increase in demand by students for that program.',
                'advice' => 'It is the Board\'s recommendation that this PAR be denied. It contains non sustenance in support of the offer to transfer funds and we recommend that GPSC resubmit it with more detail.',
                'approved' => 0,
                'vp_comments' => 'This PAR is disapproved by Melissa Vito on 12/18/12 as recommended by the Board.  Melissa agrees with the Board recommendation that the project reapply for the PAR, providing more detailed information for the Board to consider.',
                'vp_approved' => 0,
                'created_at' => '2012-11-28 03:30:49.000000',
                'timestamp_review' => '2012-12-14 22:50:33.000000',
                'netid_review' => 'jevans91',
                'timestamp_vp' => '2012-12-19 16:27:32.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 69,
            ),
            13 => 
            array (
                'id_old' => 199914,
                'netid' => 'sferrell',
                'changes' => 'Student Legal Services uses student workers to make appointments, call with reminders and greet clients at the front desk. We would like to devote $4500.00 to paying a part-time student worker. The money should come from the following items:

office supplies: $2,500.00
advertising: $2,000.00

Costs for office supplies and advertising fluctuate, depending upon the needs of the program. We have not spent all the money devoted to office supplies and advertising this year so those funds are available.',
                'advice' => 'The Board recommends approval of this PAR. Student Legal Services has been vital to students on campus. They provide a free essential service and it is high in demand. Allowing them to reallocate funds for a student worker to assist the lawyer falls within the original intentions of the application.',
                'approved' => 1,
                'vp_comments' => 'Dr Melissa Vito agreed with the Boards recommendation and approved this PAR via e-mail on 3/19/13.',
                'vp_approved' => 1,
                'created_at' => '2013-01-15 16:34:54.000000',
                'timestamp_review' => '2013-03-19 21:10:17.000000',
                'netid_review' => 'jevans91',
                'timestamp_vp' => '2013-03-19 23:04:46.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 39,
            ),
            14 => 
            array (
                'id_old' => 199915,
                'netid' => 'jkl2',
                'changes' => 'The GPSC requests that $5,000 be transferred from its POD Grant Project to its Travel Grants Project. 
The GPSC Travel Grants program is among the only programs of its kind at the U of A. It provides a dedicated source of funding for graduate and professional students who need to travel around the nation and abroad to further their academic studies. Students who seek funding from the Travel Grants program are required to submit a detailed application about their proposed trip which is evaluated by independent student judges and finally given a score based on a detailed rubric. 
During the Fall 2012 semester the Travel Grants program received a significant increase in applications due to an increase of the popularity of GPSC’s Travel Grants Program and to the decrease in departmental funding for student travel. Based on current trends we are expecting more than 650 applications totaling $315,000, whereas in the previous fiscal year the program only received 593 applications totaling $282,068; the trend since FY2009 also support this expected increase. Thus we expect demand to remain high and constant over the coming semester. 
The POD Grant Project, which was awarded $20,000 in SSF funding last year has awarded over $10,000. Because POD applications come from groups of students, the GPSC receive far fewer applications. The GPSC has also allocated a portion of its own surplus funds from previous years to support the POD program. Thus, if demand remains constant from the Fall, the GPSC will be able to fund future POD application without the portion we request to be transferred.  
The Travel Grants program is the most popular service that the GPSC provides to graduate and professional students at the University of Arizona. In fact, according to the 2012 SSF Survey, increasing the GPSC’s Travel Funding would support the top four initiatives for graduate students: 1) funding for graduate student academic travel, presentations, and professional development (86%), 2) access to scholarships and financial aid information (81%), 3) expanded career-related opportunities (74%), and 4) academic support services (including tutoring, supplemental instruction, and educational planning) that teach the skills needed to be a successful student (69%). In fact, 86% of graduate students indicated that they “strongly agree” or “agree” to support “funding for graduate student academic travel, presentations, and professional development,” making it the number one funding initiative for UA graduate and professional students for the fourth consecutive year.  Approving this transfer will allow the GPSC to better meet the needs of these students and fulfill its mission of supporting their professional and academic growth.',
                'advice' => 'The Board recommends approval of the transfer of funds from the GPSC POD funding to Travel Grants. Travel grants are essential to graduate and professional students. Although this PAR does not fit into the original intention of the proposal, the board feels that it is an appropriate use of the money.',
                'approved' => 1,
                'vp_comments' => 'Dr Melissa Vito agrees with the Board\'s recommendation and approved this PAR via e-mail on 3/19/13.',
                'vp_approved' => 1,
                'created_at' => '2013-01-22 18:06:39.000000',
                'timestamp_review' => '2013-03-19 21:12:36.000000',
                'netid_review' => 'jevans91',
                'timestamp_vp' => '2013-03-19 23:06:18.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 69,
            ),
            15 => 
            array (
                'id_old' => 199916,
                'netid' => 'ceagan',
                'changes' => 'The Dean of Students Office is requesting the reallocation of Student Services Fee funds due to one of our Student Assistance Coordinator’s departure from The University of Arizona.  In an effort to ensure a seamless transition in personnel and continued support of students, we need to recruit and hire a new Student Assistance Coordinator by the start of fiscal year 2014. 

Our timeline for conducting a search runs from late March 2013 through May 2013, with an anticipated start date of July 1, 2013, or sooner. The Dean of Students Office anticipates approximately two months of salary savings and employee related expenses for this short vacancy (anticipated savings $5846.00 in salary, $1824.00 in ERE).

This PAR is to request the reallocation of $3,000.00 of the salary savings to operations in support of our initiation of a national search. This funding would allow the Dean of Students Office to effectively advertise this career opportunity and pay for search expenditures as follows: 

Advertisement/Marketing:$500
Candidate Travel: $1800 (3 candidates, $600 per roundtrip flight)
Candidate accommodations:$525 (3 candidates at $175 per night)
Search Miscellaneous: $175

Thank you for considering this Program Alteration Request. 

*During the 12-13 Academic year, Student Assistance has served 832 students to date. The top three concerns students seek resources for are academics, family emergencies, and wellness checks.',
                'advice' => 'The SSF Board approved this PAR with a vote of 9 for to 1 against at the open Board Meeting on 4/19/13.',
                'approved' => 1,
                'vp_comments' => 'Approved by Melissa Vito 05-13-13',
                'vp_approved' => 1,
                'created_at' => '2013-03-20 22:29:58.000000',
                'timestamp_review' => '2013-05-13 15:42:20.000000',
                'netid_review' => 'twhetzel',
                'timestamp_vp' => '2013-05-13 22:25:05.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 66,
            ),
            16 => 
            array (
                'id_old' => 199917,
                'netid' => 'danthaix',
                'changes' => 'The current JEDI grant has funding listed for 5 Intergroup Dialogue facilitators to each receive $1,000 stipends for a semester’s worth of work with the program. Partner funding in the form of an additional $1,000 was also contributed by Residence Life, Social Justice. After training 6 facilitators to work with the program, 1 facilitator indicated at the beginning of the Spring 2013 semester that he would not be able to participate, which eliminated the 2 person facilitation team needed to implement the third IGD section. Because of this unanticipated loss of a facilitator, 4 Intergroup Dialogue facilitators were continued with the program to facilitate 2 IGD sections. We would like to move the $1,000 in funding for the 5th IGD facilitator over to the operations line of the JEDI grant where it will be used to support the remaining Spring events.',
            'advice' => 'The Board unanimously voted (10 for votes) to approve this PAR at the Open Board Meeting on 4/19/13.',
                'approved' => 1,
                'vp_comments' => 'Approved by Melissa Vito 05-13-13',
                'vp_approved' => 1,
                'created_at' => '2013-03-26 17:23:05.000000',
                'timestamp_review' => '2013-05-13 15:47:19.000000',
                'netid_review' => 'twhetzel',
                'timestamp_vp' => '2013-05-13 22:26:07.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 63,
            ),
            17 => 
            array (
                'id_old' => 199918,
                'netid' => 'moorem',
                'changes' => 'Greetings, 

We currently have $1,000 approved for Operations-Marketing under the Common Ground Alliance Project. The person we identified to do the marketing design work is a graduate student with the University of Arizona who is only able to be paid through student wages. Can we re-categorize the $1000.00 designated as marketing under Personnel with the Expense Type as Student Employees instead of how it\'s currently designated? With this funding the student employee will design a logo, and other branding/marketing materials for our online and social media presence, flyers, handouts, etc, which will greatly impact our ability to inform students about the many opportunities provided through the Common Ground Project.

Please let me know if there are any additional questions or concerns regarding this request. Attached is the final copy of our approved budget.

Thank you,

Maria Moore
Program Director 
African American Student Affairs
626-2660
moorem@email.arizona.edu',
                'advice' => 'The Board recommends approval of this PAR. Transferring funding from Operations-Marketing to Student Employees to hire a graduate student to perform the marketing work supports the original intentions of the application and supports student employment.',
                'approved' => 1,
                'vp_comments' => 'Dr Melissa Vito agrees with the Board\'s recommendation and approved this PAR via e-mail on 12/10/13.',
                'vp_approved' => 1,
                'created_at' => '2013-09-25 18:40:34.000000',
                'timestamp_review' => '2013-12-10 15:20:53.000000',
                'netid_review' => 'ledward1',
                'timestamp_vp' => '2013-12-10 17:37:08.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 99,
            ),
            18 => 
            array (
                'id_old' => 199919,
                'netid' => 'krista',
                'changes' => 'The Women\'s Resource Center hereby submits a program alteration request in the form of moving $800 from Operations to Professional Development/Travel.

Each year, the WRC sends its four FORCE student intern leaders to a professional development conference. This outstanding opportunity is the "reward" for all the extra hours and energy the leaders put into the FORCE programs and events, for which they receive no extra academic credit beyond what the other interns receive. Each year, this incentive creates a strong leadership team who returns from the conference with energy, enthusiasm, and new ideas for the work of the Women\'s Resource Center.

This year, the FORCE leaders have been accepted at the very competitive TEDWomen conference in San Francisco. This is a once in a lifetime opportunity, as women such as Sheryl Sandberg present at this annual conference. However, this particular conference is more expensive than conferences attended by the FORCE leaders in the past. Therefore, the WRC needs to reallocate $800 from Operations to Professional Development/Travel in order to cover these expenses appropriately.

The total annual budget amount remains the same. However, the transfer from one category to another is reflected in the attached revised budget.',
                'advice' => 'The Board recommends approval of this PAR. Allowing the Women’s Resource Center to reallocate funding from Operations to Professional Development/Travel to attend the TEDWomen Conference will benefit the student interns who are able to attend and bring recognition to the University of Arizona. The board feels that this is an appropriate use of the money.',
                'approved' => 1,
                'vp_comments' => 'Dr Melissa Vito agrees with the Board\'s recommendation and approved this PAR via e-mail on 12/10/13.',
                'vp_approved' => 1,
                'created_at' => '2013-10-14 16:52:24.000000',
                'timestamp_review' => '2013-12-10 15:21:59.000000',
                'netid_review' => 'ledward1',
                'timestamp_vp' => '2013-12-10 17:37:55.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 92,
            ),
            19 => 
            array (
                'id_old' => 199920,
                'netid' => 'jkl2',
                'changes' => 'The GPSC requests that $1000 that has been allocated to GPSC Childcare Marketing for this project be moved to LWC Funding in order to provide additional childcare subsidies to students in need. After much discussion with the office of Life and Work Connections we feel there is adequate marketing for this program and there would be a greater direct benefit to students if the $1000 originally approved for marketing could be reallocated into subsidy funding. We have found ways to promote this service through free outlets and there is quite a lot of awareness for the program at this time, as such the need for marketing dollars is less significant that originally planned. Thus far in FY14 almost half of the funding has been allocated to students, leaving just over $12,000 available for students next semester. With the reallocation of this $1000 more students and their children would benefit than through marketing efforts.  We are requesting this reallocation for both years that the project has been funded, FY14 and FY15. The GPSC feels that this money will be put to greater use by allocating it to go directly to students and we appreciate your consideration of our request.',
                'advice' => 'The Board recommends approval of this PAR. Allowing funding to be transferred from Marketing to Life and Work Connections fulfills the original intentions of the application and makes more funding available to directly impact students.',
                'approved' => 1,
                'vp_comments' => 'Dr Melissa Vito agrees with the Board\'s recommendation and approved this PAR via e-mail on 12/10/13.',
                'vp_approved' => 1,
                'created_at' => '2013-11-21 23:39:31.000000',
                'timestamp_review' => '2013-12-10 15:22:37.000000',
                'netid_review' => 'ledward1',
                'timestamp_vp' => '2013-12-10 17:38:44.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 104,
            ),
            20 => 
            array (
                'id_old' => 199921,
                'netid' => 'krista',
            'changes' => 'In an effort to formalize The Men\'s Project internship and bring it onto par with the FORCE internship, the Women\'s Resource Center would like to reallocate $1000 from its Operations budget to its Personnel budget for 2014-2015, in order to provide a stipend for a Student Director of The Men\'s Project in academic year 2014-2015. Although the WRC is seeking ASUA recognition for The Men\'s Project, through Programs & Services, this 2014-2015 funding is not guaranteed and would not be determined until later in the Spring 2014 semester (making a Spring semester search-and-hire difficult). The total WRC budget from SSF would remain unchanged by this reallocation.',
                'advice' => 'The Board recommends approval of PAR SF 14.22-P2. Transferring funding from Operations to Personnel to hire a Student Director to run the Men\'s Project supports the original intentions of the application and supports student employment.',
                'approved' => 1,
                'vp_comments' => 'Melissa Vito approved this PAR on 3/10/14, as recommended by the Board.',
                'vp_approved' => 1,
                'created_at' => '2014-02-03 18:37:27.000000',
                'timestamp_review' => '2014-03-06 15:34:34.000000',
                'netid_review' => 'ledward1',
                'timestamp_vp' => '2014-03-10 18:04:06.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 92,
            ),
            21 => 
            array (
                'id_old' => 199922,
                'netid' => 'krista',
                'changes' => 'Consistent with one of the original intents of our approved proposal to the SSF Board - to provide opportunities for employment and leadership development of Graduate Students through Assistantship opportunitites - the WRC seeks to reallocate approximately $1446.80 from its Operations budget to its Personnel budget. 
This request is due to the fact that the WRC requested a GA rate that was less than the greater Dean of Students Office GA rate. Since the WRC falls within the DOS central office, and our GAs are part of the DOS Emerging Leaders cohort, the WRC used the DOS GA offer letter template in Summer 2013. In an oversight on the part of the WRC Director, but consistent and equitable with the DOS GA cohort payrate, the two WRC Graduate Assistants were offered a slightly higher payrate than allocated in the WRC\'s SSF GA Wages + ERE allocation. 
The result is a Personnel budget deficit of $3,236.400 Wages and $1,925.658 ERE, for a total of $5,162.058 Combined Wages & ERE. However, the WRC is happy to use funds from its other funding sources to cover $3715.26 of this deficit! The remaining request to reallocate $1446.80 would help to complete this deficit. The reallocation would be toward Object Codes 1190, 2119, and 2120.
The WRC Director wishes to note that our Graduate Assistants have been a crucial part of the great success of NEW Leadership Arizona, The Men\'s Project, and other WRC initiatives!',
                'advice' => 'The Board recommends approving this PAR to move $1,446.80 from Operations to Personnel budget for graduate assistant wages and ERE. Transferring unused operations budget to use for student salaries is in line with the Board’s values of supporting student employment.',
                'approved' => 1,
                'vp_comments' => 'Melissa Vito approved this PAR on 4/28/14, as recommended by the Board.',
                'vp_approved' => 1,
                'created_at' => '2014-03-21 21:08:52.000000',
                'timestamp_review' => '2014-04-26 01:49:39.000000',
                'netid_review' => 'ledward1',
                'timestamp_vp' => '2014-04-28 21:02:48.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 92,
            ),
            22 => 
            array (
                'id_old' => 199923,
                'netid' => 'moorem',
                'changes' => 'We would like to transfer unspent funding from the salary allocation for the CGA Graduate Assistant position to increase professional development funding for the graduate student. The CGA Graduate Assistant position was approved for a .75 FTE position. Due to the impact and subsequent limitations of the Affordable Care Act, the CGA GA position had to be reduced to .66 FTE per university policy change. We calculated that this would mean $1,303 in unspent salary funding for the CGA GA by the end of FY14.

The reduction in pay has had a detrimental affect on our graduate staff. Moving $1,303 in anticipated unspent salary funds to professional development is a reasonable way to compensate the CGA Graduate Assistant for the loss in pay. 

Thank you for taking the time to consider this PAR request.',
                'advice' => 'The Board recommends approving this PAR to move $1,303 of surplus funds from the Personnel Budget to the Professional Development budget to compensate for the loss in pay to graduate assistants. Providing opportunities to graduate assistants to compensate for a reduction in their FTE is in line with the original intention of the application and supports student employees.',
                'approved' => 1,
                'vp_comments' => 'Melissa Vito approved this PAR on 04/28/14, as recommended by the Board.',
                'vp_approved' => 1,
                'created_at' => '2014-04-02 22:06:24.000000',
                'timestamp_review' => '2014-04-26 01:52:21.000000',
                'netid_review' => 'ledward1',
                'timestamp_vp' => '2014-04-28 21:03:48.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 99,
            ),
            23 => 
            array (
                'id_old' => 199924,
                'netid' => 'kvd',
                'changes' => 'We are requesting an alteration in our budget to transfer $14,000 of funding from the Operations line to the Personnel and ERE lines of our student employees, particularly our Graduate Assistants. This alteration will in no way change the intent of our proposal and remains consistent with the overall mission of Scholarship Universe.  This alteration will simply allow us to continue funding our highly skilled and hardworking student employees – in particular our development and coding team through the end of this fiscal year.

The computer programming level for Scholarship Universe has risen tremendously over the past couple of years as we have worked to integrate external scholarships with a diversity of internal UA scholarship processes from a dozen colleges/divisions (Phase II). Our graduate assistant programmers, who currently perform 90% of the ongoing development are doing everything from carrying out website maintenance, migrating data, and developing new features such as confidential recommendation letters, and our coming email notifications and thank you letter process. Not to mention they will be heavily depended upon for a planned site redesign that will make Scholarship Universe more user friendly, mobile friendly, and UA branded. Because of rapid growth and site complexity we have had to hire additional graduate assistant programmers. The combination of qualifications to acquire a programming position with Scholarship Universe and the skills gained on the job so far have launched UA student programmers from Scholarship Universe into jobs with Microsoft, Amazon, Zappos, and Intel. We are proud to say our UA student team members are securing some of the best jobs available upon graduation. However, as bright and talented as our programmers have been, it take at least an entire semester for a programmer to just get comfortable with everything Scholarship Universe is currently doing. We are often faced with one or more programmers graduating each fall and spring because their degree programs are two years (computer science) or less (Management Information Systems). We are also faced with programmers receiving graduate assistant offers elsewhere due to their high aptitude. Therefore, we must hire additional graduate assistant programmers at least a semester in advance of our graduating programmers. This strategy has helped us retain team members that we have already invested in, and it allows the proper transfer of critical knowledge to new team members so they can manage the heavy lifting when called upon. We are able to afford a shift in operation funds at this time because these positions are paramount to our service. Additionally, our recent and ongoing assessment efforts have revealed that our promotion efforts are strong – with as many as 75% of current students and over 90% of current freshmen having logged into Scholarship Universe.  At the same time, as we continue adding additional college/departmental scholarships to SU, we also benefit from the participating departments communication to students on our behalf. The end result is that we’ve needed less money in our operations budget to fuel growth and awareness of Scholarship Universe, but need more funds to meet the demands for our service. Not funding these essential student positions would not only create serious setbacks to our coming developments, but it also would create major interruptions to our current services. 

As our December Progress report indicated, Scholarship Universe continues to be a successful program in terms of our impact on the student population.  As Phase II continues to progress, we’ve increased the number of UA Scholarships available in the system from 329 being offered for the 2013-14 school year to 560 being offered for the 2014-15 school year thus far. We expect this number to increase throughout the rest of the calendar year. User engagement with the site remains high and continues to increase as well.

The work of our student employees is indispensable to the success of Scholarship Universe.  They makeup 75% of our team. As a result of this budget change, we’ll be able to maintain our current number of student employees, particularly those in our most critical positions. This, in turn, allows us to continue developing and refining Scholarship Universe ensuring that it remains an innovative and impactful service by UA students for UA students.',
                'advice' => 'The Board recommends approving this PAR to move $14,000 from Operations to Personnel and ERE for student employment. Transferring unused Operations budget to use for student salaries is in line with the Board’s values of supporting student employment and is in line with the original intention of the program.',
                'approved' => 1,
                'vp_comments' => 'Melissa Vito approved this PAR on 4/28/14, as recommended by the Board.',
                'vp_approved' => 1,
                'created_at' => '2014-04-07 15:44:47.000000',
                'timestamp_review' => '2014-04-26 01:54:41.000000',
                'netid_review' => 'ledward1',
                'timestamp_vp' => '2014-04-28 21:04:26.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 91,
            ),
            24 => 
            array (
                'id_old' => 199925,
                'netid' => 'rcn1',
                'changes' => 'Good afternoon: 

I am requesting to move the $1000 originally approved for student support to out of state travel.

The University of Arizona currently has four finalists for the class of 2014 Tillman Military Scholars (TMS). We may end up with zero new TMS but we are hopeful we will have new scholars. If we do have new scholars the Pat Tillman Foundation hosts an annual summit the last week in June for new TMS in Chicago. Our intent would be to help fund the travel for the new scholars to attend the summit. 

Thank you for your consideration. If you have any questions please do not hesitate to contact me. 

Thank you,

Cody',
                'advice' => 'The Board recommends approving this PAR to move $1,000 from student support to travel for future use by the four finalists for the 2014 Tillman Military Scholarship. It is an honor for the University of Arizona to have finalists for this award, and the Board believes that providing funding for travel expenses for these students is an appropriate use of funding.',
                'approved' => 1,
                'vp_comments' => 'Melissa Vito approved this PAR on 4/28/14, as recommended by the Board.',
                'vp_approved' => 1,
                'created_at' => '2014-04-16 17:38:54.000000',
                'timestamp_review' => '2014-04-26 02:00:20.000000',
                'netid_review' => 'ledward1',
                'timestamp_vp' => '2014-04-28 21:05:29.000000',
                'netid_vp' => 'twhetzel',
                'project_id' => 85,
            ),
            25 => 
            array (
                'id_old' => 199927,
                'netid' => 'gmiller1',
                'changes' => 'University EMS PAR request
Joseph Menke
Project SF15.22P2
Requesting to move $3000 from unforeseen expenses to travel.

University EMS would like to request a PAR to realign our current budget to allow for travel and consolidation of operational funds. Currently, UEMS has no funds remaining in the previously allocated travel and expenses budget due to additional, unforeseen, trips taken during the Fall 2014 semester. The funds in the original travel line item were originally intended for travel to the National Collegiate Emergency Medical Services Foundation (NCEMSF). As UEMS is still a fairly new and growing service at the UA, opportunities arose in the fall in which we needed to utilize those funds to help UEMS to continue to grow and shape the future of the service.
The previously allocated travel budget was utilized to advance UEMS\' business and operational models, which will allow for further growth and success as an emergency medical service for the U of A. UEMS and our base hospital (UAMC) representatives had the opportunity to travel to Los Angeles to meet with EMS officials at UCLA who have a student run program that has been in operation since 1978. From that trip we have begun to look at their model when looking at our 5 year plan which entails ultimately transportation of patient.  Through the UCLA Trip, we learned of another conference with industry professionals at the World EMS Expo in Nashville. The trip to Nashville proved beneficial in that we meet with several EMS representatives who went over their vehicular equipment and supplies model-leading us to believe that this would be the route to take in the near future. We also attended many workshops, one of which was the anaphylaxis emergencies. This was especially helpful as we have been working on with campus health and the base hospital to potentially pilot a program called FARE (food allergy and research program) which deals with "Emergency Response and Training for Food Allergies" on university campuses. 
During these events, we were able to gain invaluable knowledge about EMS operations and opportunities available to our agency. We have since put into place numerous operational and policy changes that have made our operations more efficient and effective. We feel the NCEMSF Conference is still beneficial, as we want to take the leadership of the group that will take over in FY16.
As a collegiate EMS agency, we are a member of the National Collegiate Emergency Medical Services Foundation (NCEMSF), which is the national organization that provides guidance to collegiate EMS agencies across the country. Each spring NCEMSF holds a conference where they provide lectures, symposiumns, technical skills competitions, and other information sharing events that provide an enormous benefit for every agency attending. Since the inception of UEMS 4 years ago, representatives of our agency have attended this annual conference and we believe that it is a critical and vital event for UEMS to send representatives to continue to network with other Student EMS groups from across the nation. Leaders of UEMS will gain valuable information pertaining to managing and leading a colligate EMS organization to up-and-coming policies and procedures on health care delivery. Two members of the executive leadership team (Chief & Asst. Chief), as well as the 2 members in general leadership (future executive leadership), will be attending this conference. It is important to have these veterans pass on how the conference works and introduce them to colleagues and business associates that we have worked with over the past 4 years. Doing this will ensure proper guidance for future growth and maturity of University Emergency Medical services and its members. 
After analyzing our current budget, we have determined that we would have enough funds remaining in our operational budget to sustain our operations, as well as fund a trip to the NCEMSF conference at the end of February. Being a vital information sharing and educational opportunity, we feel that requesting a PAR to realign our budget for travel is a prudent use of funds and will provide continued benefit for UEMS and the U of A community. Visit https://www.ncemsf.org for more information.',
            'advice' => 'The board recommends approving this par in full (to transfer $3,000 from Operations to Travel for the purpose of covering unexpected travel expenses in the past and ensuring safe and funded travel to future conferences and events for professional development). Due to the nature of this request, the board feels that - if UAEMS thinks that they can continue to exist with the lessened operations total - this movement of funds reflects the interests of both the board and the project. The project is requesting this money to ensure continued commitments to professional development for members of the program (UAEMS) and is funding continued student development.

Consensus was unanimous in the positive.',
                'approved' => 1,
                'vp_comments' => 'Approved by SVP Vito on March 4, 2015.',
                'vp_approved' => 1,
                'created_at' => '2015-02-23 23:34:04.000000',
                'timestamp_review' => '2015-03-04 21:18:38.000000',
                'netid_review' => 'nhavey',
                'timestamp_vp' => '2015-03-05 21:45:14.000000',
                'netid_vp' => 'bonnie',
                'project_id' => 136,
            ),
            26 => 
            array (
                'id_old' => 199928,
                'netid' => 'krista',
                'changes' => 'The Women’s Resource Center would like to request a change in fund designation from GA Salary/ERE to Operations for the amount $3280. 
One of the WRC graduate assistants found full-time employment and left her WRC position in late November 2014. We filled this GA position with a new graduate student, effective February 2, 2015. The salary and ERE savings from the interim period total $3280. 
The WRC currently has the opportunity to expand its space in the Center for Student Involvement and Leadership (CSIL), as well as move a Lactation Station into the WRC space. Both of these items will directly benefit students! We have maxed out the capacity of the WRC with attendance at or over 100 people for several events in recent years, so the added space would directly impact the programming ability of FORCE and The Men’s Project. Additionally, it is a priority of the WRC to support parent-students. Because the only lactation/private family room in the Student Union is in danger of being removed in order to create more space, the WRC has volunteered to have it moved into our space (as well as maintain it). A private space for moms/parents/children is crucial for those students who are managing school and family life, and the WRC is happy to move the current Lactation Station into its area to ensure that the space is preserved.
The WRC would like to use the $3280 savings to help pay for Facilities expenses through its Operations budget.
Attached is the new, revised budget.',
                'advice' => 'The board is entirely in favor of approving this PAR. Citing the necessity for lactation rooms on campus as well as a divested interest in preserving both the sanctity of the WRC at present and encouraging its increased proliferation on campus, the board feels that this PAR is a slam-dunk in terms of reasonable re-allocation of funding. 

Because the money requested is simply leftover from an interim period without a staff member, we feel that the revision is entirely acceptable.',
                'approved' => 1,
                'vp_comments' => 'Approved by SVP Vito on 4/22/15.',
                'vp_approved' => 1,
                'created_at' => '2015-03-16 19:15:00.000000',
                'timestamp_review' => '2015-04-21 23:12:43.000000',
                'netid_review' => 'nhavey',
                'timestamp_vp' => '2015-04-29 15:25:26.000000',
                'netid_vp' => 'bonnie',
                'project_id' => 92,
            ),
            27 => 
            array (
                'id_old' => 199929,
                'netid' => 'jawillia',
                'changes' => 'PAR – Innovate UA
Requested Changes (Please describe the changes you wish to make to your project in the space below. Limited to 1500 words.):
I am requesting 2 changes for the Innovate UA project.
1.	Transfer $12,000 from Operations, currently specified as Project Event Costs, to the Student Support line.  The nature of the planned expense is essentially unchanged. These funds will be used for two purposes.  

First, Innovate UA’s Student Incubator program provides aspiring student entrepreneurs from all across campus a structured program to test a venture concept development under the guidance of  and as selected by the Innovate UA Executive Director.  Approximately 12 student projects are eligible to apply to receive funds not to exceed $1,000 per project.  The projects will submit a detailed budget of expenditures and receive a stipend based on these expenditures.  
An example of a project requesting funding was generated out of a Start-Up Tucson weekend event.  The student (PhD candidate) wants to create “Edible Optics”, candy shaped liked devices that would be STEM education learning tools.  The requested funding is $400 which would be used for various supplies to create the candy (sugar, flavorings, etc.) and some equipment like saucepans, candy thermometer and oven mitts.

Second, it is expected that a variety of Innovate UA’s Project Event Costs will actually be delivered by and paid to students.    Examples of this include small stipends for student event photographers or a larger website development project to support delivery of the 1000 Pitches program.  The change to a stipend from operations simplifies the process for the student to receive the funds needed for the project.  

The combined total use of these funds is not expected to exceed the $12,000 previously allocated to Project Event Costs.

2.	Transfer $5,000 from Operations specified as 1000 Pitches Award to Student wages for $3,000 and Grad Wages for $2,000.  And transfer $1,400 from Operations specified as Other Operational Expenses to Grad & student ERE.  These changes are necessary because 5 of the awards were to students who are current student employees.  The university requires that the awards received by these students be paid through payroll and not as an Operation expense.  This change than requires that ERE is also paid, thus the need to move the additional $1,400 from Operations to ERE.

A revised budget for the project is attached.',
                'advice' => 'This PAR, which is requesting that funds be moved around to accommodate funding for awards as well as desired alternative executions of previously approved tasks within their original application, is approved by the board.

Because the PAR does not request any outlandish reappropriation of fee money and is being used to uphold the word of the applicants in granting awards for innovative students, the board believes that this proposal is in line with the goals of the division and of the board in general.',
                'approved' => 1,
                'vp_comments' => 'Approved by SVP Vito on 4/22/15.',
                'vp_approved' => 1,
                'created_at' => '2015-04-03 17:34:38.000000',
                'timestamp_review' => '2015-04-21 23:14:47.000000',
                'netid_review' => 'nhavey',
                'timestamp_vp' => '2015-04-29 15:26:10.000000',
                'netid_vp' => 'bonnie',
                'project_id' => 151,
            ),
            28 => 
            array (
                'id_old' => 199930,
                'netid' => 'kvd',
            'changes' => 'We are requesting an alteration in our budget to transfer $11,000 of funding from the Operations line to the Personnel and ERE lines of our student employees. This alteration will in no way change the intent of our proposal and remains consistent with the overall mission of Scholarship Universe (SU).  This alteration will simply allow us to continue funding our highly skilled and hardworking student employees through the end of this fiscal year.
Our student employees make up 75% of the Scholarship Universe team. The computer programming level for Scholarship Universe has risen tremendously over the past couple of years as we have worked to integrate external scholarship matching with a diversity of internal UA scholarship departments and processes from across campus (Phase II). Our student programmers have been integral in assisting our full-time developer with the major redesign (Appendix Fig. 1) currently underway, website maintenance, migrating data, developing new features and making improvements based on user feedback. The combination of qualifications to acquire a student programming position with Scholarship Universe and the skills gained on the job so far have already launched UA student programmers from Scholarship Universe into jobs with Microsoft, Amazon, IBM, Ebay, Zappos, and Intel. We are proud to say our UA student team members are securing some of the best jobs available upon graduation. This is also true for our student researchers who have secured positions with successful companies and government positions. Our student researchers play a critical role alongside our student developers. The research team’s primary role is to find and vet scholarship opportunities every year including adding the matching logic which matches users to scholarships. They are reviewing roughly 3,000 non-UA scholarships annually for applicability, changes on donor sites, and matching logic changes. They also assist in the reviewing and matching of the nearly 800-1000 UA scholarships in the system. Training for Scholarship Universe is extensive and retaining the continuity of knowledge and skills of our student team is vital to keep improving the program and maintain services. Therefore, we need to move operations funding to cover all of our student wages and ERE through the end of this fiscal year. They are greatly needed and funding these essential student positions would create serious setbacks to our coming improvements and ongoing service.
Scholarship Universe continues to be a successful program in terms of our impact on the student population and instrumental in helping departments get scholarships awarded to eligible students.  The results have been outstanding thus far with UA students receiving over $2.4 million dollars this academic year from non-UA scholarships that were available in SU, and over $2.3 million last year. By building a highly functional departmental scholarship management system, SU also has been able to attract sixteen colleges/divisions to utilize Scholarship Universe for their processes. So far this has translated to nearly four million dollars in guaranteed awards to Wildcats through a single streamlined source. Furthermore, the site continues to see dramatic student usage. According to Google Analytics (Appendix Fig.2), Pageviews and Sessions have increased significantly and the site’s Bounce Rate has dropped. The student site is on pace to surpass a million Pageviews in the first six months of this year.
The work of our student employees has been indispensable to the success of Scholarship Universe.  As a result of this budget change, we’ll be able to fund all our current student employees for the remainder of this fiscal year. This, in turn, allows us to continue developing and refining Scholarship Universe ensuring that it remains an innovative and impactful service by UA students for UA students.',
                'advice' => 'While the board\'s relationship with Scholarship Universe has been tumultuous, we feel that this PAR is in line with serving the recommendations we set out for the applicants in our approval of their original proposal. The PAR, which requests a movement to better cover ERE and student wages is completely legitimate and in the vein of what we as a board hoped that the applicants would do. That being said, we are in favor of approving this PAR.',
                'approved' => 1,
                'vp_comments' => 'Approved by SVP Vito on 4/22/15.',
                'vp_approved' => 1,
                'created_at' => '2015-04-17 18:40:57.000000',
                'timestamp_review' => '2015-04-21 23:19:37.000000',
                'netid_review' => 'nhavey',
                'timestamp_vp' => '2015-04-29 15:26:36.000000',
                'netid_vp' => 'bonnie',
                'project_id' => 91,
            ),
            29 => 
            array (
                'id_old' => 199931,
                'netid' => 'moorem',
                'changes' => 'We\'d like to move $1,000 from the Common Ground Courage Retreat line to the Student Stipends line for fiscal year 2014-2015. In August, we invited two graduate student staff to work the retreat for $500 each. These grad student expenses were budgeted into the retreat line of our grant, instead of student stipends. 

This adjustment allows us to stay within our student stipend and retreat budget lines, without the addition of any additional funding.',
                'advice' => 'The board unanimously voted to approve this PAR on the grounds that it is a minor re-allocation of funds and does not go against any of the rationale that caused this proposal to be approved in the first place. We are happy to support funding graduate student employment and involvement.',
                'approved' => 1,
                'vp_comments' => 'Approved by SVP Vito on 12/18/15.',
                'vp_approved' => 1,
                'created_at' => '2015-05-30 00:38:49.000000',
                'timestamp_review' => '2015-12-08 18:38:02.000000',
                'netid_review' => 'nhavey',
                'timestamp_vp' => '2015-12-18 22:08:17.000000',
                'netid_vp' => 'bonnie',
                'project_id' => 122,
            ),
            30 => 
            array (
                'id_old' => 199932,
                'netid' => 'jawillia',
                'changes' => 'Below is a request for 2 changes for the Innovate UA project.
Awards and Project Costs Update
Currently, $10,000 is allocated to Operations for Innovation Challenge Awards. We expect that additional ore related program operational costs may be incurred.  As a result we request the spending category be renamed “Awards and Program Costs”.  The new spending category description will provide for the needed flexibility to make limited adjustments as needed.   Additionally, outside support is being solicited which might allow for better and high use of these limited funds.  Further, awards may be issued through either payroll or direct payment to students depending on their employment status with the University.  We are requesting the flexibility to utilize these funds through either budget category for logistical purposes.  

Student Payroll to Student Support Update
The $12,500 currently listed in Wages & ERE is now requested to be moved to Student Support for logistical purposes in appropriately issuing director stipends.  As with the awards and Project Costs item above, the stipends may be issued through either payroll or direct payment to students depending on their employment status with the University.  We are requesting the flexibility to utilize these funds through either budget category for logistical purposes.  

A revised budget for the project is attached.',
                'advice' => 'As this PAR solely sought to reallocate funding due to a financial/accounting issue, the board unanimously voted to approve this at our open board. We strongly support this proposal in general due to its commitment to student engagement - the PAR simply sought to make it easier to support this student engagement by fixing a budget line.',
                'approved' => 1,
                'vp_comments' => 'Approved by SVP Vito on 12/18/15.',
                'vp_approved' => 1,
                'created_at' => '2015-09-15 20:21:40.000000',
                'timestamp_review' => '2015-12-08 18:39:31.000000',
                'netid_review' => 'nhavey',
                'timestamp_vp' => '2015-12-18 22:08:38.000000',
                'netid_vp' => 'bonnie',
                'project_id' => 153,
            ),
            31 => 
            array (
                'id_old' => 199933,
                'netid' => 'rcn1',
                'changes' => 'VETS requests the approval of a program alteration request in the form of moving funding from Operations & Programs to Student Support, Travel-Out, and Funding for a Faculty Fellow under Appointed & Faculty Regular and ERE. 

Whether the proposed changes are consistent with the original intent of the proposal, i.e. in terms of student priorities addressed and impact.
The proposed changes are consistent with the original intent of the proposal. Specifically, the proposed changes enhance current efforts identified by both undergraduate and graduate student initiatives and addressed by Veterans Education and Transition Services; “Expanded career-related opportunities” and “Academic support services (including tutoring, supplemental instruction, and educational planning) that teach the skills needed to be a successful student”. 
In this effort the proposed changes would move funding from Operations and Programs to Student Support. 

The rational for the request.
Moving funding from operations and programs to student support will allow student veterans to build out projects that will expand our career-related opportunities and academic support services, supplemental instruction, and educational planning. Specifically, this will provide support for student veterans to build out projects specific to enhancing relationships with Career Services and employers interested in engaging specifically with student veterans, building out our Bridge Program (supplemental instruction and educational planning that teach the skills needed to be a successful student) slated for summer 2016, and enhancing current tutoring services.  

VETS maintains a strong relationship with Career Services and both recognize an ever increasing interest from outside companies seeking student veterans as potential employees. The intent is for student veterans to help create a program that can be executed into the future benefiting current and future student veterans when seeking career-related opportunities.

VETS also recognizes some first year first time student veterans arrive at the university deficient in mathematics and English. The intent of the Bridge program is for participants to explore the methodology behind learning mathematics, provide fundamental English refresher, resiliency training, and ensure participating student veterans are prepared for the start of the fall semester. 

In collaboration with the THINK TANK student veteran tutors are trained and provide tutoring for other student veterans in the VETS Center. Further building out the tutoring program and training will enable a more effective all around tutoring program. 

The specific funding request would be to move $8250 from operations to student support to enable four student veterans to engage these projects this fall. 

VETS would also request to adjust funding to support Faculty Fellow Dr. Faten Ghosen

The funding request for Faculty Fellow Dr. Faten Ghosn totals $4,000. 

Finally, VETS would like to move $2000 from Operations to Travel-Out. VETS has recently been notified that three proposals to present at the Student Veterans of America National Conference will be approved. The conference is in Orlando Florida January 7th – 10th 2016. The request would support travel expenses including flight and partial hotel. The travel costs would help support three student veterans and one staff member. Current estimated flights from Tucson to Orlando are already a minimum of $371 per person or $1484 for four persons (not including fees and taxes). Conference hotel rooms are listed at $135 per night or $540 for one room not including fees and taxes for the nights of the 7th, 8th, and 9th of January. Of course efforts to find cheaper lodging are already underway. 

In total VETS request to move from Operations & Programs to the following:
$8250 to student support
$4000 to Appointed & Faculty Regular
$1300 to ERE for Appointed & Faculty Regular
$2000 to Travel Out

$15,550	Total

The overall budget remains the same. 

To offset moving funding from Operations & Programs will make adjustments to the operations budget. Additionally, VETS recently received a funding that will support ongoing Operations and Programs support. 

The current success of the program.
The enhancement of both the relationship with Career Services and employers interested in engaging student veterans and in building out the Bridge Program are fundamentally new and the success of these programs cannot be evaluated at this time. 
The tutoring program has allowed student veteran tutors to be trained by, and work collaboratively with, the THINK TANK. The program has been successful. However, we believe there is room for growth and continued enhancement of the tutoring program.

The additional benefits gained from the change. 
Benefits gained include more robust efforts to expand career related activities, expanded supplemental instruction and educational planning. This will allow current student veterans to take an active roll in the projects.',
                'advice' => 'This PAR marks all the boxes produced by our annual survey. We could not be more delighted to approve it as it is simply great changes to an already excellent proposal. The board unanimously voted to approve this PAR.',
                'approved' => 1,
                'vp_comments' => 'Approved by SVP Vito on 12/18/15.',
                'vp_approved' => 1,
                'created_at' => '2015-09-16 18:10:35.000000',
                'timestamp_review' => '2015-12-08 18:41:58.000000',
                'netid_review' => 'nhavey',
                'timestamp_vp' => '2015-12-18 22:09:04.000000',
                'netid_vp' => 'bonnie',
                'project_id' => 162,
            ),
            32 => 
            array (
                'id_old' => 199934,
                'netid' => 'tbrett',
                'changes' => 'The Common Ground Alliance project requests a change to the budget submitted for Intersectional Collaborations. We were allocated $6500 in the operations category to support the continued development of collaborative programming that teaches about and engages students\' multiple identities. The Queer People of Color and 2 Spirit Project is one of those Projects funded under this line. 

This project is designed to engage participants in empowerment building dialogue and activities. As part of the empowerment activities we request $600 be moved from operations to travel to support attendance at and presentation by the students involved in the Project at a conference in Phoenix.',
                'advice' => 'This PAR is excellent because it allows an already great program the opportunity to branch out and allow for student engagement and professional development. This is what the board referred to as a "slam dunk". We unanimously voted to approve it.',
                'approved' => 1,
                'vp_comments' => 'Approved by SVP Vito on 12/18/15.',
                'vp_approved' => 1,
                'created_at' => '2015-12-01 19:38:11.000000',
                'timestamp_review' => '2015-12-08 18:42:56.000000',
                'netid_review' => 'nhavey',
                'timestamp_vp' => '2015-12-18 22:09:30.000000',
                'netid_vp' => 'bonnie',
                'project_id' => 122,
            ),
            33 => 
            array (
                'id_old' => 199935,
                'netid' => 'krista',
                'changes' => 'The Women’s Resource Center would like to request a change in fund designation from Director Salary to Operations for the amount of $5,000 and to travel for the amount of $4,000 for a total of $9,000. 
On July 1, 2015, the Oasis Program against Sexual Assault and Relationship Violence was moved from the Campus Health Service to the Women\'s Resource Center. This outstanding program--which offers workshops, events, education, and internship opportunities to nearly 3000 UA students each year--was accompanied by a new grant from the Arizona Department of Health Services (ADHS). This grant covers 50% of salary and ERE for the Principle Investigator (PI) on the grant, namely the Oasis Coordinator. However, the grant only includes $795 for Operations of the program. 
The Oasis Coordinator left her position on July 31, 2015 to begin a PhD program at UA. The WRC posted the open position, but the Director of the WRC fulfilled the PI position in the interim, resulting in a salary and ERE savings to our SSF award of over $9,000 in salary for 2015-2016.
The relocation of the Oasis Program means the expanded opportunity to offer creative and engaging sexual assault education to our campus. It is a priority of the WRC to provide greater, more comprehensive sexual assault education on campus, and we aspire to reach every incoming Wildcat, every year. Creative and innovative programming is crucial to combat the statistics that our campus sexual assault survey from April 2014 revealed about our community. However, the addition of the Oasis Program and its services are now being funded as a part of the WRC, with only a $795 increase to our original Operations budget and no allocation for the professional development of the Coordinator or two Graduate Assistants of the Program. Therefore, the WRC would like to use $9,000 of salary savings from this year to create a $5,000 operations line in our budget to fund the important work of our Sexual Assault and Violence Prevention Programming, and $4,000 to professional development (travel) for the Coordinator and 2 GAs.
We will return the extra $263 from salary and $1731.53 from ERE to SSF as a gesture of thanks for all of the expenses that you incur and increases that you fund on our behalf over the years.',
                'advice' => 'Noting that the board could not in good face react poorly to a program that is actively saving us money, the board unanimously voted to approve this PAR. 

Citing that the changes in fund allocation would support multiple goals of the survey, the board is overjoyed to support this PAR.',
                'approved' => 1,
                'vp_comments' => 'Approved by SVP Vito on March 22, 2016.',
                'vp_approved' => 1,
                'created_at' => '2016-01-25 17:59:40.000000',
                'timestamp_review' => '2016-03-21 16:46:53.000000',
                'netid_review' => 'nhavey',
                'timestamp_vp' => '2016-03-22 17:42:20.000000',
                'netid_vp' => 'bonnie',
                'project_id' => 160,
            ),
            34 => 
            array (
                'id_old' => 199936,
                'netid' => 'celinar',
            'changes' => 'Our proposal, Campus Events and Engagement, requested $9,000 for one graduate student intern stipend ($5,000) and four undergraduate student stipends ($1,000 each) per year. We have since learned we are unable to use stipends to pay the interns because we will be directing their work, and should instead pay them as student employees. We propose to spend $8,600 on student employee wages for the graduate and undergraduate interns and $400 on ERE, for the same total of $9,000. We have revised our initial budget to reflect these changes.',
                'advice' => 'The board is delighted to approve this funding alteration to allow the employees to be paid. The vote was unanimously in favor.',
                'approved' => 1,
                'vp_comments' => 'Approved by SVP Vito on April 20, 2016',
                'vp_approved' => 1,
                'created_at' => '2016-04-12 19:23:25.000000',
                'timestamp_review' => '2016-04-19 04:42:17.000000',
                'netid_review' => 'nhavey',
                'timestamp_vp' => '2016-04-20 15:20:21.000000',
                'netid_vp' => 'bullockj',
                'project_id' => 175,
            ),
            35 => 
            array (
                'id_old' => 199939,
                'netid' => 'krista',
            'changes' => 'In an effort to balance work and family life, the WRC Director will be doing a flex year in the 2016-2017 school year, taking off the month of July (1st-31st). This is a slower time for the WRC and its programming. However, the WRC still does important work to educate groups on campus around sexual assault and violence prevention during this time. For that reason, the WRC seeks to use the Director salary savings of $4,846.07 to pay for one Graduate Assistant to staff the WRC and cover presentation requests. This would maintain the WRC as a summer resource for students and also continue our work of educational outreach for campus. The amount needed for GA salary and ERE for July, as well as August 1-14 (to help prep for the launch of new Fall programs) is $2312.20.',
                'advice' => 'The Board fully approves of this PAR, both in that it allows for the smooth running of the center over the summer and supports a student\'s GA position in order to do so. We have no questions for the WRC.',
                'approved' => 1,
                'vp_comments' => 'Per VP Melissa Vito this PAR was approved via email Tuesday, December 13, 2016 8:44 PM',
                'vp_approved' => 1,
                'created_at' => '2016-06-23 17:18:09.000000',
                'timestamp_review' => '2016-12-12 19:12:03.000000',
                'netid_review' => 'jasonharris',
                'timestamp_vp' => '2016-12-14 20:18:15.000000',
                'netid_vp' => 'bullockj',
                'project_id' => 160,
            ),
            36 => 
            array (
                'id_old' => 199938,
                'netid' => 'kvd',
                'changes' => 'This request is to transfer $5,000 from Operations to Student Wages to help the Scholarship Universe team support student positions through the remainder of this fiscal year. Scholarship Universe typically employees 10 student positions each year and will have some students staying on this summer and perhaps additional students will be hired to fill positions vacated by graduating students or students who will be working at summer internships like one of our G.A.\'s who has earned an internship with Facebook. Our students gain experiential learning at Scholarship Universe and they support many aspects of SU from customer service, scholarship matching, development, events, special projects etc. We will have enough remaining in operations to cover our expenses and the transfer to student wages ensures we can support our current student employees.',
                'advice' => 'The board is happy that estimated costs were lower than expected and approves of the remaining funds being rolled into paying employees for more work. The vote was unanimously in favor.',
                'approved' => 1,
                'vp_comments' => 'Approved by SVP Vito on April 20, 2016',
                'vp_approved' => 1,
                'created_at' => '2016-04-14 00:10:27.000000',
                'timestamp_review' => '2016-04-19 04:43:07.000000',
                'netid_review' => 'nhavey',
                'timestamp_vp' => '2016-04-20 15:21:32.000000',
                'netid_vp' => 'bullockj',
                'project_id' => 156,
            ),
            37 => 
            array (
                'id_old' => 199940,
                'netid' => 'tbrett',
                'changes' => 'The Common Ground Alliance program is requesting a PAR to redistribute funding to address specific needs for this year. All changes are consistent with the priorities for the project as originally approved by SSFAB. 

1. Move $2,000 from Student Stipends (line 56) to Student Wages (line 22). One of the students who will receive a stipend this academic year for working with the spoken word poetry group needs to be paid out of student wages. She receive financial aid and a stipend award would impact her financial aid award. We are requesting to pay her hourly instead. In addition, we would need to move $34 to cover ERE for those wages from Intersectional Collaborations (line 39) to Student wages ERE (line 33).

2. Our second request is a result of temporary staffing changes in the appointed staff supervising the Common Ground Alliance project (this staffing position is funded through the Dean of Students Office). This staff person has been temporarily reassigned to serve as Interim Director of the Guerrero Student Center and supervises Common Ground Alliance. We are requesting a PAR so that we can increase funding to the graduate student who was hired for this year in the project. This graduate student will serve in a .75 staff position for 10 months with increased responsibilities for the project. 

We are requesting $17,200 from line 27 (Grad Assistants Combined) be moved to line 23, (Appointed Regular Staff).  Because the graduate assistant will be in a .75 staff position she will receive qualified tuition reduction. To fund the rest of the salary we are requesting to move $4,800 from line 34 to line 23. 

To fund the ERE we are requesting $6,600 be moved from line 34 to line 29. And $1,400 from line 33 to line 29. 

This PAR is an excellent opportunity for the graduate student currently hired to get professional experience while working on her Ph.D. and all changes requested continue to support the graduate student.',
                'advice' => 'The Board fully supports this PAR, and has no questions for the Common Ground Alliance at this time. Given staffing changes, we are happy to support a student that is able to pick up extra responsibilities and allow the center to continue to run smoothly. Given the need allocate student wages in a different way, the Board is happy to approve a workaround so as not to impact the student\'s financial aid.',
                'approved' => 1,
                'vp_comments' => 'Per VP Melissa Vito this PAR was approved via email Tuesday, December 13, 2016 8:44 PM',
                'vp_approved' => 1,
                'created_at' => '2016-11-14 18:10:45.000000',
                'timestamp_review' => '2016-12-12 19:15:36.000000',
                'netid_review' => 'jasonharris',
                'timestamp_vp' => '2016-12-14 20:18:45.000000',
                'netid_vp' => 'bullockj',
                'project_id' => 122,
            ),
            38 => 
            array (
                'id_old' => 199941,
                'netid' => 'jawillia',
                'changes' => 'We are requesting to move $5000 from Student Support to Program Stipends. This change is a necessary only as a means of delivering payments properly.  Our program will deliver the same services and programs with the same budget, but these payments will now need to be processed as stipends.',
                'advice' => 'The Board approves of the change to Innovate UA\'s budget in this way, tentative on the fact that the budget change be from Total Operations to Total Student Support, as there may have been a typo when submitting the request. The submitted budget makes sense however, and is approved, but it should be made clear to any receiving students that their financial aid award may change, and Innovate UA should be able to handle the concern if it comes up.',
                'approved' => 1,
                'vp_comments' => 'Per VP Melissa Vito this PAR was approved via email Tuesday, December 13, 2016 8:44 PM',
                'vp_approved' => 1,
                'created_at' => '2016-12-01 18:51:49.000000',
                'timestamp_review' => '2016-12-12 19:20:55.000000',
                'netid_review' => 'jasonharris',
                'timestamp_vp' => '2016-12-14 20:19:07.000000',
                'netid_vp' => 'bullockj',
                'project_id' => 206,
            ),
            39 => 
            array (
                'id_old' => 199942,
                'netid' => 'celinar',
                'changes' => 'We hired a 1.0 FTE Program Coordinator at a salary of $35,000. Our Program Coordinator took some unpaid leave during fall semester, resulting in salary savings of $2,146.00 as of December 1, 2016. Additionally, our Program Coordinator has requested to reduce his hours to .85 FTE  from January 16, 2017 until June 4, 2017, which would result in salary savings of $2011.50.  We would like to use the salary savings to pay for additional hours for our undergraduate interns next semester.',
                'advice' => 'This PAR was unanimously approved by the Board after discussion of its merits. The salary savings being used to support undergraduate interns is something that the Board feels is a great use of funds, as we approve of any extra funding going to supporting students. Additionally, the extra hours offered will hopefully be able to account for the program coordinator role being at .85 FTE.',
                'approved' => 1,
                'vp_comments' => 'Per VP Melissa Vito this PAR was approved via email Thursday, March 9, 2017 at 10:56AM',
                'vp_approved' => 1,
                'created_at' => '2017-01-20 20:15:33.000000',
                'timestamp_review' => '2017-03-08 21:22:10.000000',
                'netid_review' => 'jasonharris',
                'timestamp_vp' => '2017-03-09 18:00:14.000000',
                'netid_vp' => 'bullockj',
                'project_id' => 175,
            ),
            40 => 
            array (
                'id_old' => 199943,
                'netid' => 'krista',
                'changes' => 'This program alteration request from the Women\'s Resource Center seeks to allocate $1000 of our Operations budget toward a different object code, namely capital equipment.

Six of the units within the Center for Student Involvement and Leadership (CSIL) of the Student Union share use of a copy machine. However, our copy machine was 12 years old, and barely functional. This made for many challenges with our units, as it slowed our ability to do daily business functions. Together, we purchased a new Xerox machine (as purchasing was cheaper than leasing). Each unit contributed $1000 toward this essential item.  

We will reallocate funds from our "Women in Leadership Series" line within Operations to create this new category, Capital Equipment, within our budget while maintaining our same annual funding total.',
                'advice' => 'The board greatly approves of the women\'s resource center, and though this PAR does not necessarily live up the goals and mission of the center/proposal by taking $1000 away from the \'Women in Leadership\' series, the board still believes that this PAR will benefit the center in the long-term.',
                'approved' => 1,
                'vp_comments' => 'VP Melissa Vito approved this PAR via email Tuesday, May 9, 2017 at 10:23PM',
                'vp_approved' => 1,
                'created_at' => '2017-03-14 21:04:23.000000',
                'timestamp_review' => '2017-05-08 20:10:05.000000',
                'netid_review' => 'jasonharris',
                'timestamp_vp' => '2017-05-10 14:36:32.000000',
                'netid_vp' => 'bullockj',
                'project_id' => 160,
            ),
            41 => 
            array (
                'id_old' => 199944,
                'netid' => 'tbrett',
                'changes' => 'The Common Ground Alliance program is requesting a PAR to redistribute funding to a specific need that arose this Spring 2017 semester. We believe this change is consistent with the priorities for the project as originally approved by SSFAB. 

We request to move $1,000 from Student Stipends (line 56) to Student Wages (line 22). One of the students who was to receive a stipend this academic year for facilitating dialogue needs to be paid out of student wages. She receives financial aid and was notified by OSFA this semester that a stipend award would impact her financial aid award. Essentially she would be overawarded if she accepted the stipend. We are requesting to pay her hourly instead. Thank you for considering this request.',
                'advice' => 'The board greatly approves of the Common Ground Alliance\'s efforts, so alteration requests that are submitted are generally no-brainers. We admire the work that happens across campus by the individuals involved, and are happy with any effort to make it easier for students.',
                'approved' => 1,
                'vp_comments' => 'VP Melissa Vito approved this PAR via email Tuesday, May 9, 2017 at 10:23PM',
                'vp_approved' => 1,
                'created_at' => '2017-04-10 22:51:50.000000',
                'timestamp_review' => '2017-05-08 20:12:13.000000',
                'netid_review' => 'jasonharris',
                'timestamp_vp' => '2017-05-10 14:36:59.000000',
                'netid_vp' => 'bullockj',
                'project_id' => 122,
            ),
            42 => 
            array (
                'id_old' => 199945,
                'netid' => 'jawillia',
                'changes' => 'PAR – Innovate UA
I am requesting 1 changes for the Innovate UA project.
1.	Transfer $6,000 from Student Support > Leadership Team Stipends … to … Operations > Programming.  These changes are necessary because we have operated with fewer stipend students but incurred more than expected programming costs for existing programs including Startup Weekend, Accelerator & Hackathon.
A revised budget for the project is attached.',
                'advice' => 'The board appreciates what Innovate UA brings to campus, and would love to make it easier for them to put on their large-scale events by moving around budget items. However, we worry simply because this $6,000 is being moved from Student Support to programming, and hope that students are still better supported in the future by the program.',
                'approved' => 1,
                'vp_comments' => 'VP Melissa Vito approved this PAR via email Tuesday, May 9, 2017 at 10:23PM',
                'vp_approved' => 1,
                'created_at' => '2017-04-13 02:59:51.000000',
                'timestamp_review' => '2017-05-08 20:15:40.000000',
                'netid_review' => 'jasonharris',
                'timestamp_vp' => '2017-05-10 14:37:27.000000',
                'netid_vp' => 'bullockj',
                'project_id' => 206,
            ),
            43 => 
            array (
                'id_old' => 199946,
                'netid' => 'tbrett',
            'changes' => 'The SPEAKOut initiative is requesting a PAR to move $500 from professional development (line 45) to Printing Materials and Supplies (line 38). The reason for the request is that all of the students have completed their professional development/travel for the year, however, we have the opportunity to host an on-campus workshop for them that would require supplies and food. The purpose of the workshop is still in line with the original request, as it is for professional development of the graduate and undergraduate students. Thank you for your consideration of this change.',
                'advice' => 'The board greatly approves of the SPEAKout program and all of the work that it does, and this PAR does still live up to the original intent of the request. We are happy to approve, in order to better foster professional development among students who are doing great work on campus.',
                'approved' => 1,
                'vp_comments' => 'VP Melissa Vito approved this PAR via email Tuesday, May 9, 2017 at 10:23PM',
                'vp_approved' => 1,
                'created_at' => '2017-04-13 23:45:35.000000',
                'timestamp_review' => '2017-05-08 20:18:51.000000',
                'netid_review' => 'jasonharris',
                'timestamp_vp' => '2017-05-10 14:37:53.000000',
                'netid_vp' => 'bullockj',
                'project_id' => 161,
            ),
            44 => 
            array (
                'id_old' => 199947,
                'netid' => 'kmclarken',
                'changes' => 'We would like to submit a PAR request to move the funds currently allocated to Student Employees to Student Support. We believe this was perhaps made in error in our originally submitted budget request after our funding request was approved. The funds allocated to Student Employees was meant to provide monetary support to the Director, Assistant Director, and Committee Chairs of the UA Campus Pantry. We are wanting to pay the positions in stipends, and not in hourly wages. The current budget would require us to pay hourly wages of the employees. In order to ensure that we do not go over budget in paying our employees, we would like to move the funds to Student Support as originally intended with these funds.',
                'advice' => 'On behalf of the Student Services Fee Advisory Board Chair, Adam Ciampaglio, the following advice is given. Campus Pantry had originally thought that they would control the work hours and tasks of their students, however this was not possible for them due to the nature of the work and the project itself. Instead Campus Pantry would like to compensate it’s students via a stipend. The PAR submitted by Campus Pantry was approved by the board at the open board meeting on 12/01/17. This PAR is in line with Campus Pantry’s overall mission and continues to benefit students.',
                'approved' => 1,
                'vp_comments' => 'On behalf of Melissa Vito, PAR SF-47 had been approved via email on 12/19/2017.',
                'vp_approved' => 1,
                'created_at' => '2017-11-28 18:41:29.000000',
                'timestamp_review' => '2017-12-19 17:09:00.000000',
                'netid_review' => 'aarmour',
                'timestamp_vp' => '2017-12-19 18:19:05.000000',
                'netid_vp' => 'aarmour',
                'project_id' => 217,
            ),
            45 => 
            array (
                'id_old' => 199948,
                'netid' => 'rcn1',
            'changes' => 'The University of Arizona (UA) has been recognized as a leader nationally when it comes to serving the military and veteran connected student population. At the UA, this totals over 4,000 students. In the fall of 2017 VETS submitted a proposal to present at the NASPA (Student Affairs Administrators in Higher Education) National Symposium on Military Connected Students in February of 2018. Not only was the presentation approved VETS was asked to take the 50-minute presentation and turn it into a two-hour Extended Learning Session entitled “Integrating Military Experience Into Academic Life”. VETS was subsequently asked to conduct a 3-hour Pre-Symposium Workshop entitled, “Serving Military Connected Students 101”. VETS original SSFAB budget included only one person attending. However, with the increased request from NASPA, VETS is requesting funding for three more individuals to attend, one being our SSFAB funded graduate assistant. 
Additionally, the University of Arizona is one of thirteen partner institutions with the Pat Tillman Foundation (PTF). Every summer the PTF holds their annual Summit in Chicago, ILL. In previous year individuals from partner institutions (such as the University of Arizona) have been allowed to room in the dorms at Roosevelt University. The cost was minimal. This past Summit (July 2017) partner institutions were notified that Roosevelt University dorms were overbooked and that representatives from partner institutions would need to book their own hotel in downtown Chicago. This was a significant and unexpected setback to VETS travel budget. 
The two situations above are unique, one a great opportunity and one an unexpected and significant increase in cost to our travel budget. To offset these increased travel charges and in order to maintain our original budgeted travel VETS is requesting to move $6,550 from our operations account to our travel account.',
                'advice' => 'On behalf of the Student Services Fee Advisory Board Chair, Adam Ciampaglio, the following advice is given. The Veterans Education and Transition Services Project submitted a PAR to shift operations budget to travel. VETS has been presented with two very unique opportunities to showcase the University of Arizona’s veterans along with the many programs and initiatives that the VETS center has implemented to assist and educate veterans. The PAR is in line with VETS mission and fundamental project goals. The PAR was approved by the board at the open board meeting on 12/01/17.',
                'approved' => 1,
                'vp_comments' => 'On behalf of Melissa Vito, PAR SF-48 had been approved via email on 12/19/2017.',
                'vp_approved' => 1,
                'created_at' => '2017-11-30 00:08:17.000000',
                'timestamp_review' => '2017-12-19 17:09:43.000000',
                'netid_review' => 'aarmour',
                'timestamp_vp' => '2017-12-19 18:20:12.000000',
                'netid_vp' => 'aarmour',
                'project_id' => 162,
            ),
            46 => 
            array (
                'id_old' => 199949,
                'netid' => 'celinar',
            'changes' => 'For the Campus Events and Engagement Initiative, the Disability Resource Center (DRC) would like to decrease the Program Coordinator’s hours from 40 to 30 and allocate 10 hours per week for the DRC\'s Graduate Assistant (not currently funded under the grant), which would increase her FTE from .5 to .75. In order to maintain the Program Coordinator at 1.0, the DRC will be funding 10 hours for this staff member to work on other DRC projects. For the hours funded by the grant, the Program Coordinator will focus on the accessibility of physical spaces used for events and the Graduate Assistant will focus on outreach with student organizations and the staff that support them. Because the Program Coordinator\'s salary is slightly higher than the Graduate Assistant\'s salary, we would like to use the additional salary savings for student wages (see attached spreadsheet). 

Currently, the initiative has achieved a number of successes, including the creation of outreach materials and the assessment of over 230 centrally scheduled classroom spaces for disability access, along with a web page to make this information available for students and event planners. The rationale for the change is that the Program Coordinator has more experience with the physical access requirements of the ADA and how to assess physical spaces, whereas the Graduate Assistant has more experience participating in student organizations and working with staff who support student organizations. This change is consistent with the original intent of the proposal in that we will be meeting the same objectives and continuing to provide opportunities to graduate and undergraduate student employees.',
                'advice' => 'On behalf of the Student Services Fee Advisory Board Chair, Adam Ciampaglio, the following advice is given. Campus Events and Engagements requested to reduce staff salaries and use those savings to pay for graduate student wages, and additional student wages. Please see the attached detail for more calcification. This PAR was approved in the Open Board meeting on 12/01/2017.',
                'approved' => 1,
                'vp_comments' => 'On behalf of Melissa Vito, PAR SF-49 had been approved via email on 01/18/18',
                'vp_approved' => 1,
                'created_at' => '2017-11-30 20:13:10.000000',
                'timestamp_review' => '2017-12-21 19:04:26.000000',
                'netid_review' => 'aarmour',
                'timestamp_vp' => '2018-01-19 20:14:17.000000',
                'netid_vp' => 'aarmour',
                'project_id' => 175,
            ),
            47 => 
            array (
                'id_old' => 199950,
                'netid' => 'jawillia',
                'changes' => 'In order to ensure continued programming after budget cuts this past cycle, the student directors unanimously voted to discontinue student director stipends and use the funding to better fund more projects for students.  For that reason, we are submitting this PAR to move the money to engage students in programs to help foster entrepreneurship and innovation.',
                'advice' => 'On behalf of the Student Services Fee Advisory Board Chair, Adam Ciampaglio, the following advice is given. Innovate UA submitted a PAR to shift money from stipends to operations. Due to the lack of funding in the current fiscal year Innovate UA’s directors unanimously decided to forgo stipends in order to fund more innovation and entrepreneurial inspiring events for the UA community. This PAR is in line with their project goals and directly benefits students. This PAR was approved by the board on 12/01/17 in the open board meeting.',
                'approved' => 1,
                'vp_comments' => 'On behalf of Melissa Vito, PAR SF-50 had been approved via email on 12/19/2017.',
                'vp_approved' => 1,
                'created_at' => '2017-11-30 21:37:17.000000',
                'timestamp_review' => '2017-12-19 17:10:26.000000',
                'netid_review' => 'aarmour',
                'timestamp_vp' => '2017-12-19 18:20:33.000000',
                'netid_vp' => 'aarmour',
                'project_id' => 247,
            ),
            48 => 
            array (
                'id_old' => 199951,
                'netid' => 'christinecarlson',
                'changes' => 'The Student Unions is proud of the Rooftop Garden Project and the opportunities the project created for student engagement. Students designed the garden spaces and have helped coordinate the pieces required to have the greenhouse garden constructed on the roof of the Student Union Memorial Center.  The greenhouse construction is currently underway, but the cost to build the garden on the roof has far exceeded the estimated infrastructure costs submitted in our project budget. As a result, we are submitting this PAR to reallocate the remaining budget for labor to operations in order to complete the greenhouse garden construction. We must finish greenhouse construction before we can hire students to run and maintain the garden, so this reallocation of funds will still accomplish the original intent of the project since the Student Unions is committed to continuing student engagement and using student employees to manage the garden once it is built. 
Our approved SSF budget for this project allocated $10,300 for labor and ERE. We have spent $1,945.59 on labor and ERE to date, so we would like to reallocate the remaining $8,354.41 of budgeted labor and ERE expense to operations to help cover more of the garden’s infrastructure costs. If approved as requested, the new project budget would look like this:

Student Employees + ERE	$1,945.59
Operations: Infrastructure	$7,500.00
Operations: Infrastructure	$8,354.41
ASC				  $200.00
Total Project Funding	$18,000.00',
                'advice' => 'On behalf of the Student Services Fee Advisory Board, I give the following advice:

The board expressed a hope that shifting funds away from student employees won\'t result in a loss of positions for student employees, and that the Rooftop & Pangea Garden project can source funds to maintain student employees. That said, the PAR is in line with the original project proposal and was approved unanimously in the Friday, April 13th open board meeting. We look forward to seeing the Rooftop and Pangea Garden completed in the future!',
                'approved' => 1,
                'vp_comments' => 'On behalf of Melissa Vito, PAR SF-51 had been approved via email on 04/25/2018.',
                'vp_approved' => 1,
                'created_at' => '2018-04-10 18:41:00.000000',
                'timestamp_review' => '2018-04-22 21:37:28.000000',
                'netid_review' => 'sixstringsent',
                'timestamp_vp' => '2018-04-26 15:23:44.000000',
                'netid_vp' => 'aarmour',
                'project_id' => 236,
            ),
            49 => 
            array (
                'id_old' => 199952,
                'netid' => 'kmclarken',
            'changes' => 'We would like to submit a PAR request to move the funds currently allocated to Motor Pool Usage- Weekly Donation and Food Pickup to Operations. We believe that the Motor Pool budget can be used for the purchase of a new floor scale. Our current scale is used to weigh large donation bins (weighing up to 300 lbs when filled) and for weighing bags of food that leave the pantry during distribution. Our current scale is malfunctioning and reaches a max weight of 440 lbs. Its size was intended to be used as a table scale as well. With the budget transfer from Motor Pool to Operations the purchase of a new floor scale will allow us to weigh multiple crates of product at a time as well as our rolling donation bins. The purchase of a new floor scale will provide a more accurate weight of donations and food brought into/ out of the Pantry. The cost of a floor scale would use the full amount of funding from the Motor pool budget with shipping included.',
                'advice' => 'On behalf of the Student Services Fee Advisory Board, I submit the following advice:

The board asked for some clarification and was immediately provided with details; thank you for the quick followup! The board wanted to ensure that there wouldn\'t be a lapse in food pickups as a result of shifting funds away from Motor Pool. Given the response to that concern, it is evident that Campus Pantry will still be able to accomplish their mission. The board voted unanimously to approve this PAR, as it is in keeping with the original framework of the project proposal. The board expressed their satisfaction of the existence and success of Campus Pantry, and are excited to see the program grow and serve students in need. The PAR was approved on Friday, April 13th at the Open Board Meeting.

Stephen Westby
SSFAB Chair',
                'approved' => 1,
                'vp_comments' => 'On behalf of Melissa Vito, PAR SF-52 has been approved via email on 04/25/2018.',
                'vp_approved' => 1,
                'created_at' => '2018-04-11 06:58:58.000000',
                'timestamp_review' => '2018-04-22 21:42:06.000000',
                'netid_review' => 'sixstringsent',
                'timestamp_vp' => '2018-04-26 15:24:37.000000',
                'netid_vp' => 'aarmour',
                'project_id' => 217,
            ),
        ));
        
        
    }
}