DROP TABLE IF EXISTS fees_old.site_user;
DROP TABLE IF EXISTS fees_old.users;

RENAME TABLE ssfee_board_mem TO users;

CREATE TABLE site_user (
  site_id int,
  user_id int
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE users
ADD COLUMN role_id int,
ADD COLUMN user_status_id int,
ADD COLUMN user_standing_id int,
ADD COLUMN user_title_id int,
ADD COLUMN user_type_id int,
ADD COLUMN phone varchar(255);

UPDATE users
SET role_id = CASE
  	WHEN accessLevel = "Standard" THEN 1
    WHEN accessLevel = "Chair" THEN 2
    WHEN accessLevel = "Administrative" THEN 3
    WHEN accessLevel = "PowerUser" THEN 4
ELSE 0 END,
user_standing_id = CASE
    WHEN standing = "Freshman" THEN 1
    WHEN standing = "Sophomore" THEN 2
    WHEN standing = "Junior" THEN 3
    WHEN standing = "Senior" THEN 4
    WHEN standing = "Graduate" THEN 5
    WHEN standing = "Professional" THEN 6
    WHEN standing = "N/A" THEN 7
ELSE 0 END,
user_title_id = CASE
    WHEN title = "Chair" THEN 1
    WHEN title = "Co-Chair" THEN 2
    WHEN title = "Marketing Director" THEN 3
    WHEN title = "Member" THEN 4
    WHEN title = "Secretary" THEN 5
    WHEN title = "Treasurer" THEN 6
    WHEN title = "Vice Chair" THEN 7
ELSE 8 END,
user_type_id = CASE
    WHEN memberType = "Administrative Advisor" THEN 1
    WHEN memberType = "Advisor Ex-Officio" THEN 2
    WHEN memberType = "Voting Member" THEN 3
    WHEN memberType = "NON-MEMBER" THEN 4
ELSE 0 END,
user_status_id = CASE
    WHEN status = "Active" THEN 1
    WHEN status = "Retired" THEN 2
    WHEN status = "N/A" THEN 3
ELSE 0 END,
phone = CASE WHEN phoneArea != '' THEN CONCAT(phoneArea, "-", phoneExchange, "-", phoneNumber) ELSE "" END,
directory = CASE directory WHEN "yes" THEN 1 ELSE 0 END,
endYear = CASE endYear WHEN "" THEN 0 ELSE endYear END;

ALTER TABLE users
DROP COLUMN id,
CHANGE netID netid varchar(255),
CHANGE firstName first_name varchar(255),
CHANGE lastName last_name varchar(255),
CHANGE studentID student_id varchar(255),
CHANGE clubOrg1 club_org_1 varchar(255),
CHANGE clubOrg2 club_org_2 varchar(255),
CHANGE clubOrg3 club_org_3 varchar(255),
CHANGE clubOrg4 club_org_4 varchar(255),
CHANGE otherTitle title_other varchar(255),
CHANGE startYear start_year varchar(255),
CHANGE endYear fiscal_year varchar(255),
DROP COLUMN phoneArea,
DROP COLUMN phoneExchange,
DROP COLUMN phoneNumber,
DROP COLUMN accessLevel,
DROP COLUMN standing,
DROP COLUMN title,
DROP COLUMN memberType,
DROP COLUMN status,
DROP COLUMN startMonth,
DROP COLUMN endMonth;

DELETE FROM fees_old.users
WHERE user_status_id = 2
OR netid in (
	select netid from fees.users
)
OR netid = '';