SET @project_set = 4;

UPDATE fees_old.answers
SET questionID = CONCAT(@project_set, 999, questionID);

ALTER TABLE fees_old.answers
DROP COLUMN id,
CHANGE COLUMN timeStamp created_at varchar(255),
DROP COLUMN proposalID,
DROP COLUMN parID,
ADD COLUMN question_id INT(11),
CHANGE fundAnswer answer_text TEXT;

UPDATE fees_old.answers
SET created_at = FROM_UNIXTIME(created_at);

UPDATE fees_old.answers answers
INNER JOIN fees.questions questions
ON answers.questionID = questions.id_old
SET answers.question_id = questions.id;

ALTER TABLE fees_old.answers
DROP COLUMN questionID;

DELETE FROM fees_old.answers
WHERE question_id IS NULL;