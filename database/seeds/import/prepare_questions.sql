SET @project_set = 4;

UPDATE fees_old.questions
SET proposalID = CONCAT(@project_set, 999, proposalID),
parID = CONCAT(@project_set, 999, parID),
id = CONCAT(@project_set, 999, id);

ALTER TABLE fees_old.questions
ADD COLUMN questionable_id INT,
ADD COLUMN questionable_type VARCHAR(191),
CHANGE id id_old INT,
CHANGE modifiedBy netid VARCHAR(191),
CHANGE fundQuestion question_text TEXT,
CHANGE deadline deadline varchar(255),
CHANGE COLUMN timeStamp created_at varchar(255);

UPDATE fees_old.questions
SET created_at = FROM_UNIXTIME(created_at);

UPDATE fees_old.questions
SET questionable_type = CASE WHEN parID IS NULL THEN "App\\Project" ELSE "App\\Par" END,
deadline = FROM_UNIXTIME(deadline);

UPDATE fees_old.questions questions
INNER JOIN fees.projects projects
ON questions.proposalID = projects.id_old
SET questions.questionable_id = projects.id;

UPDATE fees_old.questions questions
INNER JOIN fees.pars pars
ON questions.parID = pars.id_old
SET questions.questionable_id = pars.id;

DELETE FROM fees_old.questions
WHERE questionable_id IS NULL;

ALTER TABLE fees_old.questions
DROP COLUMN proposalID,
DROP COLUMN parID;