SET @project_set = 4;

DROP TABLE IF EXISTS fees_old.progress_reports;

ALTER TABLE fees_old.progress
CHANGE proposalID proposalID BIGINT;

UPDATE fees_old.progress
SET proposalID = CONCAT(@project_set, 999, proposalID);

ALTER TABLE fees_old.progress
ADD project_id INT,
CHANGE netID netid VARCHAR(191),
CHANGE question1 scope TEXT,
CHANGE question2 outcomes TEXT,
CHANGE question3 response TEXT,
DROP COLUMN id,
DROP COLUMN question4,
DROP COLUMN question5,
DROP COLUMN question6,
DROP COLUMN fileOutcomes1,
DROP COLUMN fileOutcomes1Name,
DROP COLUMN fileFinance1,
DROP COLUMN fileFinance1Name,
DROP COLUMN fileOutcomes2,
DROP COLUMN fileOutcomes2Name,
CHANGE COLUMN timeStamp created_at varchar(255);

UPDATE fees_old.progress
SET created_at = FROM_UNIXTIME(created_at);

UPDATE fees_old.progress progress
INNER JOIN fees.projects projects
ON progress.proposalID = projects.id_old
SET progress.project_id = projects.id;

ALTER TABLE fees_old.progress
DROP COLUMN proposalID;

DELETE FROM fees_old.progress
WHERE project_id IS NULL;

RENAME TABLE progress TO progress_reports;