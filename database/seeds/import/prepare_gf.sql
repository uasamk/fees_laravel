# gf only
ALTER TABLE status
ADD COLUMN allocation tinyint default 0;

ALTER TABLE proposals
ADD COLUMN department_other varchar(255),
ADD COLUMN emailFiscalSent tinyint default 0,
CHANGE firstNamePM firstNameFiscal varchar(255),
CHANGE lastNamePM lastNameFiscal varchar(255),
CHANGE emailPM emailFiscal varchar(255),
DROP COLUMN u_coleader,
DROP COLUMN g_coleader,
DROP COLUMN u_coordinator,
DROP COLUMN g_coordinator,
DROP COLUMN u_worker,
DROP COLUMN g_worker,
DROP COLUMN u_volunteer,
DROP COLUMN g_volunteer,
DROP COLUMN u_learner,
DROP COLUMN g_learner,
DROP COLUMN applicantStatus,
DROP COLUMN studentMajor,
DROP COLUMN studentYear,
DROP COLUMN approvalStatus,
DROP COLUMN pastFundingExplain,
DROP COLUMN fundingDate,
DROP COLUMN fileExtra2,
DROP COLUMN fileExtra2Name,
DROP COLUMN fileExtra3,
DROP COLUMN fileExtra3Name,
DROP COLUMN fileExtra4,
DROP COLUMN fileExtra4Name,
DROP COLUMN fileExtra5,
DROP COLUMN fileExtra5Name,
DROP COLUMN fileExtra6,
DROP COLUMN fileExtra6Name,
DROP COLUMN fileExtra7,
DROP COLUMN fileExtra7Name,
DROP COLUMN extraUploads;

UPDATE proposals
SET amount = REPLACE(amount, ",", ""),
amount = REPLACE(amount, "$", ""),
department_other = CONCAT(studentDepartment, employeeDepartment);

ALTER TABLE proposals
DROP COLUMN studentDepartment,
DROP COLUMN employeeDepartment;

ALTER Table progress
DROP COLUMN u_coleader,
DROP COLUMN g_coleader,
DROP COLUMN u_coordinator,
DROP COLUMN g_coordinator,
DROP COLUMN u_worker,
DROP COLUMN g_worker,
DROP COLUMN u_volunteer,
DROP COLUMN g_volunteer,
DROP COLUMN u_learner,
DROP COLUMN g_learner,
DROP COLUMN fileFinance2,
DROP COLUMN fileFinance2Name;

ALTER TABLE greenfund_board_mem
DROP COLUMN degree;