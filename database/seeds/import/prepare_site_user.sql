SET @site = 2;

INSERT INTO fees_old.site_user (site_id, user_id)
(select @site as site_id, id 
    from fees.users
    where id not in (
        select user_id
        from fees.site_user
    )
)