alter table fees_old.initiative_project
add column project_id int;

update fees_old.initiative_project initiative_project
inner join fees.projects projects
on initiative_project.id_old = projects.id_old
set initiative_project.project_id = projects.id;

alter table fees_old.initiative_project
drop column id_old;
