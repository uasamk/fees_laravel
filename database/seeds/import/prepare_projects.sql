SET @project_set = 4;

DROP TABLE IF EXISTS fees_old.initiative_project;
DROP TABLE IF EXISTS fees_old.projects;

CREATE TABLE initiative_project (
  id_old int,
  initiative_id int,
  is_grad tinyint
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE proposals
ADD COLUMN project_set_id int,
ADD COLUMN phone varchar(255),
ADD COLUMN netid_modified varchar(255),
ADD COLUMN amount_received int,
ADD COLUMN duration_received int,
ADD COLUMN admin tinyint,
ADD COLUMN public tinyint,
ADD COLUMN archived tinyint,
ADD COLUMN funding_status_id int,
ADD COLUMN yearly_variation tinyint,
ADD COLUMN department_id int;

UPDATE proposals
INNER JOIN status
ON proposals.id = status.proposalID
SET proposals.netid_modified = status.modifiedBy,
proposals.amount_received = CASE WHEN status.amountReceived IS NULL OR status.amountReceived = "" THEN 0 ELSE status.amountReceived END,
proposals.duration_received = CASE WHEN status.durationReceived IS NULL OR status.durationReceived = "" THEN 0 ELSE status.durationReceived END,
proposals.admin = status.allocation,
proposals.public = CASE status.public WHEN "Yes" THEN 1 ELSE 0 END,
proposals.archived = CASE status.overallStatus WHEN "Archived" THEN 1 ELSE 0 END,
proposals.funding_status_id = CASE
  WHEN status.fundingStatus = "Pending" THEN 1
  WHEN status.fundingStatus = "Funded" THEN 2
  WHEN status.fundingStatus = "Full" THEN 3
  WHEN status.fundingStatus = "Partial" THEN 4
  WHEN status.fundingStatus = "Rejected" THEN 5
  WHEN status.fundingStatus = "Request Withdrawn" THEN 6
  ELSE 0 END;

UPDATE proposals
SET project_set_id = @project_set,
id = CONCAT(@project_set, 999, id),
phone = CASE WHEN phoneArea != '' THEN CONCAT(phoneArea, "-", phoneExchange, "-", phoneNumber) ELSE "" END,
newPrj = CASE newPrj WHEN "yes" THEN 1 ELSE 0 END,
pastFunding = CASE pastFunding WHEN "yes" THEN 1 ELSE 0 END,
yearly_variation = CASE yearlyVariation WHEN "Yes" THEN 1 ELSE 0 END,
soleFunding = CASE soleFunding WHEN "yes" THEN 1 ELSE 0 END,
terms = CASE terms WHEN "agree" THEN 1 ELSE 0 END,
preDeadline = CASE preDeadline WHEN "Yes" THEN 1 ELSE 0 END,
amount = CASE amount WHEN "" THEN 0 ELSE amount END,
startYear = CASE startYear WHEN "" THEN 0 ELSE startYear END,
emailSent = CASE WHEN emailSent IS NULL THEN 0 ELSE emailSent END,
emailDirSent = CASE WHEN emailDirSent IS NULL THEN 0 ELSE emailDirSent END,
emailFiscalSent = CASE WHEN emailFiscalSent IS NULL THEN 0 ELSE emailFiscalSent END,
signoff = CASE WHEN signoff IS NULL THEN 0 ELSE signoff END;

INSERT INTO initiative_project (id_old, initiative_id, is_grad)
(select id, 1 as initiative_id, 0 as is_grad from proposals where initiative1 != "" and id is not null) union
(select id, 2 as initiative_id, 0 as is_grad from proposals where initiative2 != "" and id is not null) union
(select id, 3 as initiative_id, 0 as is_grad from proposals where initiative3 != "" and id is not null) union
(select id, 4 as initiative_id, 0 as is_grad from proposals where initiative4 != "" and id is not null) union
(select id, 5 as initiative_id, 0 as is_grad from proposals where initiative5 != "" and id is not null) union
(select id, 6 as initiative_id, 1 as is_grad from proposals where gradInitiative1 != "" and id is not null) union
(select id, 1 as initiative_id, 1 as is_grad from proposals where gradInitiative2 != "" and id is not null) union
(select id, 3 as initiative_id, 1 as is_grad from proposals where gradInitiative3 != "" and id is not null) union
(select id, 2 as initiative_id, 1 as is_grad from proposals where gradInitiative4 != "" and id is not null) union
(select id, 7 as initiative_id, 1 as is_grad from proposals where gradInitiative5 != "" and id is not null);

UPDATE proposals
SET department_id = CASE
  	WHEN department = "Academic Success and Achievement" THEN 1
	WHEN department = "Access and Inclusion" THEN 2
	WHEN department = "Admissions" THEN 3
	WHEN department = "Arizona Student Media" THEN 4
	WHEN department = "Arizona Student Unions" THEN 5
	WHEN department = "ASUA (Associated Students of the University of Arizona)" THEN 6
	WHEN department = "BookStores" THEN 7
	WHEN department = "Campus Health" THEN 8
	WHEN department = "Campus Recreation" THEN 9
	WHEN department = "Career Services" THEN 10
	WHEN department = "Dean of Students Office" THEN 11
	WHEN department = "Disability Resource Center" THEN 12
	WHEN department = "Financial Aid" THEN 13
	WHEN department = "Fund Development" THEN 14
	WHEN department = "GPSC (Graduate and Professional Student Council)" THEN 15
	WHEN department = "Leadership Programs" THEN 16
	WHEN department = "Military Science" THEN 17
	WHEN department = "New Student Services" THEN 18
	WHEN department = "Parents & Family Association" THEN 19
	WHEN department = "Registrar" THEN 20
	WHEN department = "Residence Life" THEN 21
	WHEN department = "SALT Center" THEN 22
	WHEN department = "SA Central Office" THEN 23
	WHEN department = "SA Marketing" THEN 24
	WHEN department = "SA Systems Group" THEN 25
	WHEN department = "Testing Office" THEN 26
	WHEN department = "Think Tank" THEN 27
	WHEN department = "Transfer Student Services" THEN 28
	WHEN department = "UA Facilitators" THEN 29
	WHEN department = "VETS (Veterans Education & Transition Services)" THEN 30
ELSE 0 END;

ALTER TABLE proposals
CHANGE id id_old int,
CHANGE boardID board_id varchar(255),
CHANGE netID netid varchar(255),
CHANGE firstName first_name varchar(255),
CHANGE lastName last_name varchar(255),
DROP COLUMN phoneArea,
DROP COLUMN phoneExchange,
DROP COLUMN phoneNumber,
DROP COLUMN department,
CHANGE firstNameDir director_first_name varchar(255),
CHANGE lastNameDir director_last_name varchar(255),
CHANGE emailDir director_email varchar(255),
CHANGE firstNameFiscal fiscal_first_name varchar(255),
CHANGE lastNameFiscal fiscal_last_name varchar(255),
CHANGE emailFiscal fiscal_email varchar(255),
CHANGE projectName project_name varchar(255),
DROP COLUMN projectType,
DROP COLUMN projectTypeOther,
DROP COLUMN startSem,
CHANGE startYear start_year int,
CHANGE amount amount int,
DROP COLUMN initiative1,
DROP COLUMN initiative2,
DROP COLUMN initiative3,
DROP COLUMN initiative4,
DROP COLUMN initiative5,
DROP COLUMN gradInitiative1,
DROP COLUMN gradInitiative2,
DROP COLUMN gradInitiative3,
DROP COLUMN gradInitiative4,
DROP COLUMN gradInitiative5,
CHANGE newPrj new_project tinyint,
CHANGE pastFunding past_funding tinyint,
CHANGE duration duration int,
DROP COLUMN yearlyVariation,
CHANGE soleFunding sole_funding tinyint,
DROP COLUMN future,
DROP COLUMN futureExp,
DROP COLUMN fileBudget,
DROP COLUMN fileBudgetName,
DROP COLUMN fileDescription,
DROP COLUMN fileDescriptionName,
DROP COLUMN fileExtra,
DROP COLUMN fileExtraName,
CHANGE terms agree_terms tinyint,
CHANGE lockApp locked tinyint,
CHANGE preDeadline pre_deadline tinyint,
CHANGE COLUMN finalSubmitTime final_submit_time varchar(255),
CHANGE COLUMN timeStamp created_at varchar(255),
DROP COLUMN yearEntered,
CHANGE emailSent email_sent tinyint,
CHANGE emailDirSent director_email_sent tinyint,
CHANGE emailFiscalSent fiscal_email_sent tinyint;

UPDATE proposals
SET final_submit_time = FROM_UNIXTIME(final_submit_time),
created_at = FROM_UNIXTIME(created_at);

RENAME TABLE proposals TO projects;