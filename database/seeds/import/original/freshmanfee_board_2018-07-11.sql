# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: mysql_host (MySQL 5.1.33-community-log)
# Database: freshmanfee_board
# Generation Time: 2018-07-11 21:28:11 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table freshmanfee_board_app
# ------------------------------------------------------------

DROP TABLE IF EXISTS `freshmanfee_board_app`;

CREATE TABLE `freshmanfee_board_app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `netID` varchar(25) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `studentID` varchar(9) NOT NULL,
  `phoneArea` int(3) NOT NULL,
  `phoneExchange` int(3) NOT NULL,
  `phoneNumber` int(4) unsigned zerofill NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(70) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` enum('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming') NOT NULL,
  `zip` int(5) unsigned zerofill NOT NULL,
  `zipPlus4` int(4) unsigned zerofill DEFAULT NULL,
  `standing` enum('Freshman','Sophomore','Junior','Senior','Undergraduate Student','Graduate Student','Professional') NOT NULL,
  `declaredMajor` varchar(50) NOT NULL,
  `declaredMinor` varchar(50) NOT NULL,
  `currentGPA` decimal(10,2) NOT NULL,
  `cumulativeGPA` decimal(10,2) NOT NULL,
  `gradMonth` enum('January','February','March','April','May','June','July','August','September','October','November','December') NOT NULL,
  `gradYear` int(4) NOT NULL,
  `creditsF09` decimal(10,2) NOT NULL,
  `creditsS10` decimal(10,2) NOT NULL,
  `job` enum('Yes','No') NOT NULL,
  `jobHours` decimal(10,2) DEFAULT NULL,
  `jobPositions` text,
  `clubOrg1` varchar(50) DEFAULT NULL,
  `clubOrg2` varchar(50) DEFAULT NULL,
  `clubOrg3` varchar(50) DEFAULT NULL,
  `clubOrg4` varchar(50) DEFAULT NULL,
  `boardInterest` text NOT NULL,
  `boardActivities` text NOT NULL,
  `boardOtherInfo` text NOT NULL,
  `resume` varchar(512) NOT NULL,
  `ref1fName` varchar(50) NOT NULL,
  `ref1lName` varchar(50) NOT NULL,
  `ref1address` varchar(70) NOT NULL,
  `ref1city` varchar(50) NOT NULL,
  `ref1state` enum('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming','OTHER') NOT NULL,
  `ref1zip` int(5) unsigned zerofill NOT NULL,
  `ref1zipPlus4` int(4) unsigned zerofill DEFAULT NULL,
  `ref1phoneArea` int(3) NOT NULL,
  `ref1phoneExchange` int(3) NOT NULL,
  `ref1phoneNumber` int(4) unsigned zerofill NOT NULL,
  `ref1relationship` varchar(50) NOT NULL,
  `ref2fName` varchar(50) NOT NULL,
  `ref2lName` varchar(50) NOT NULL,
  `ref2address` varchar(70) NOT NULL,
  `ref2city` varchar(50) NOT NULL,
  `ref2state` enum('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming','OTHER') NOT NULL,
  `ref2zip` int(5) unsigned zerofill NOT NULL,
  `ref2zipPlus4` int(4) unsigned zerofill DEFAULT NULL,
  `ref2phoneArea` int(3) NOT NULL,
  `ref2phoneExchange` int(3) NOT NULL,
  `ref2phoneNumber` int(4) unsigned zerofill NOT NULL,
  `ref2relationship` varchar(50) NOT NULL,
  `terms` varchar(10) DEFAULT NULL,
  `status` varchar(15) DEFAULT 'Pending Review',
  `rejectEmail` varchar(10) DEFAULT NULL,
  `reviewComments` text,
  `timeStamp` bigint(20) unsigned DEFAULT NULL,
  `yearEntered` year(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table freshmanfee_board_mem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `freshmanfee_board_mem`;

CREATE TABLE `freshmanfee_board_mem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `netID` varchar(25) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `studentID` varchar(9) NOT NULL,
  `phoneArea` varchar(3) DEFAULT NULL,
  `phoneExchange` varchar(3) DEFAULT NULL,
  `phoneNumber` varchar(4) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(70) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `zip` varchar(5) DEFAULT NULL,
  `standing` varchar(50) NOT NULL,
  `clubOrg1` varchar(50) NOT NULL,
  `clubOrg2` varchar(50) NOT NULL,
  `clubOrg3` varchar(50) NOT NULL,
  `clubOrg4` varchar(50) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `otherTitle` varchar(200) DEFAULT NULL,
  `memberType` varchar(50) DEFAULT NULL,
  `responsibilities` text,
  `startMonth` varchar(50) DEFAULT NULL,
  `startYear` varchar(4) DEFAULT NULL,
  `endMonth` varchar(50) DEFAULT NULL,
  `endYear` varchar(4) DEFAULT NULL,
  `status` varchar(15) NOT NULL,
  `accessLevel` varchar(50) DEFAULT NULL,
  `directory` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `freshmanfee_board_mem` WRITE;
/*!40000 ALTER TABLE `freshmanfee_board_mem` DISABLE KEYS */;

INSERT INTO `freshmanfee_board_mem` (`id`, `netID`, `firstName`, `lastName`, `studentID`, `phoneArea`, `phoneExchange`, `phoneNumber`, `email`, `address`, `city`, `state`, `zip`, `standing`, `clubOrg1`, `clubOrg2`, `clubOrg3`, `clubOrg4`, `title`, `otherTitle`, `memberType`, `responsibilities`, `startMonth`, `startYear`, `endMonth`, `endYear`, `status`, `accessLevel`, `directory`)
VALUES
	(18,'vanarsde','James','Van Arsdel','','520','621','6505','vanarsdel@life.arizona.edu','','','','','N/A','','','','','','Assistant Vice President for Residence Life','Administrative Advisor','Advise on policy, finance and resources','October','2009','','','Retired','Administrative','no'),
	(45,'jevans91','Justin','Evans','jevans91@','','','','jevans91@email.arizona.edu','','','Arizona','85721','Junior','','','','','Chair','','Voting Member','Deliberate and recommend action on funding and membership applications','June','2013','August','2014','Retired','Chair','no'),
	(44,'hauff','Joel','Hauff','','','','','hauff@email.arizona.edu','','','','','N/A','','','','','','Student Affairs Interim Director of Finance','Administrative Advisor','Advise on policy, finance and resources','September','2011','','','Active','Administrative','yes'),
	(43,'twhetzel','Teresa','Whetzel','','','','','twhetzel@email.arizona.edu','','','','','N/A','','','','','','Finance, SA Central Office','NON-MEMBER','Committee Room Management','June','2011','','','N/A','PowerUser','no'),
	(5,'altomare','Daniel','Altomare','S02357747','619','889','5443','altomare@email.arizona.edu','575 N. Highland Ave.','Tuscon','Arizona','85719','Sophomore','Residence Hall Association','Investments Club','Young Democrats','American Marketing Association','Member','','Voting Member','Deliberate and recommend action on funding and membership applications','September','2009','May','2010','Retired','Standard','no'),
	(41,'cpatel1','Chandni','Patel','','','','','','','','','','N/A','','','','','Member','','Voting Member','Deliberate and recommend action on funding','March','2011','','','Retired','Standard','no'),
	(42,'jenbland','Jenna','Bourland','','','','','','','','','','N/A','','','','','','Office Specialist, Senior','NON-MEMBER','Assist in preparing applications for the Board','June','2011','','','Retired','Administrative','no'),
	(39,'knielson','Kenna','Nielson','','','','','','','','','','N/A','','','','','Member','','Voting Member','Deliberate and recommend action on funding','March','2011','','','Retired','Standard','no'),
	(40,'lbilby','Logan','Bilby','','','','','','','','','','N/A','','','','','Member','','Voting Member','Deliberate and recommend action on funding','March','2011','','','Retired','Standard','no'),
	(30,'khumphre','Keith','Humphrey','','','','','','','','','','N/A','','','','','','Assistant Vice President for Student Affairs','NON-MEMBER','Advise on policy, finance and resources','March','2011','','','Retired','Administrative','no'),
	(100,'marceladelgado','Marcela','Delgado','','','','','marceladelgado@email.arizona.edu','','','','','N/A','','','','','Member','','NON-MEMBER','Provide Administrative and Financial support for the Advisory Board','May','2017','','','N/A','Administrative','no'),
	(32,'lbingham','Lindsay','Bingham','','','','','','','','','','N/A','','','','','Member','','Voting Member','Deliberate and recommend action on funding','March','2011','','','Retired','Administrative','yes'),
	(33,'jmblack','Julia','Black','','','','','','','','','','N/A','','','','','Member','','Voting Member','Deliberate and recommend action on funding','March','2011','','','Retired','Standard','no'),
	(34,'mcleodm','Martha','McLeod','','','','','','','','','','N/A','','','','','Member','','Voting Member','Deliberate and recommend action on funding','March','2011','','','Retired','Standard','no'),
	(35,'djanis1','Dylan','Janis','','','','','','','','','','N/A','','','','','Member','','Voting Member','Deliberate and recommend action on funding','March','2011','','','Retired','Standard','no'),
	(36,'jkowalcz','Jessica','Kowalczyk','','','','','','','','','','N/A','','','','','Member','','Voting Member','Deliberate and recommend action on funding','March','2011','','','Retired','Standard','no'),
	(19,'kmbeyer','Kevin','Beyer','','','','','','','','','','N/A','','','','','','Senior Web Developer','NON-MEMBER','Building and maintaining the website','September','2009','','','Retired','PowerUser','no'),
	(20,'sanorris','Seth','Norris','','','','','','','','','','N/A','','','','','','Web Manager','NON-MEMBER','Building and maintaining the website','September','2009','June','2013','Retired','PowerUser','no'),
	(38,'luis92','Luis','Mariscal','','','','','','','','','','N/A','','','','','Member','','Voting Member','Deliberate and recommend action on funding','March','2011','','','Retired','Standard','no'),
	(37,'dmariner','Deanna','Mariner','','','','','','','','','','N/A','','','','','Member','','Voting Member','Deliberate and recommend action on funding','March','2011','','','Retired','Standard','no'),
	(28,'kpope','Kriss','Pope','','520','621','0964','kpope@email.arizona.edu','','','','','N/A','','','','','','Student Affairs Director of Finance','Administrative Advisor','Advise on policy, finance and resources','November','2010','','','Retired','Administrative','no'),
	(46,'vhanna','Valerie','Hanna','','','','','vhanna@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2013','','','Retired','Standard','no'),
	(47,'tiffanyfeller','Tiffany','Feller','','','','','tiffanyfeller@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding and proposals.','January','2013','','','Retired','Standard','no'),
	(48,'mitchellwagner','Mitchell','Wagner','','','','','mitchellwagner@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding and proposals.','January','2013','','','Retired','Standard','no'),
	(49,'jcilek','Joseph','Cilek','','','','','jcilek@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2013','','','Retired','Standard','no'),
	(50,'ikrstic','Irena','Krstic','','','','','ikrstic@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2013','','','Retired','Standard','no'),
	(51,'dbracamonte','Danielle','Bracamonte','','','','','dbracamonte@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2013','','','Retired','Standard','no'),
	(52,'cjkarl','CJ','Karl','','','','','cjkarl@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2013','','','Retired','Standard','no'),
	(53,'nhavey','Nick','Havey','','','','','nhavey@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2013','','','Retired','Standard','no'),
	(54,'alaguirre11','Aaron','Aguirre','','','','','alaguirre11@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','November','2013','','','Retired','Standard','no'),
	(55,'danieldouglas','Daniel','Douglas','','','','','danieldouglas@email.arizona.edu','','','','','Freshman','','','','','Member','','NON-MEMBER','Freshman Fee non-voting member','March','2013','','','Retired','Standard','no'),
	(56,'','CJ','Karl','23144708','','','','cjkarl@email.arizona.edu','','','','','Freshman','','','','','','Advisor','Advisor Ex-Officio','Serves as a student advisor to voting members','January','2013','May','2013','Retired','Standard','no'),
	(59,'gmiller1','Gabby','Miller','','','','','gmiller1@email.arizona.edu','','','','','N/A','','','','','','Admin Support','NON-MEMBER','Admin Support','July','2013','','','Retired','Administrative','no'),
	(60,'ddavis94','Drew','Davis','','','','','ddavis94@email.arizona.edu','','','','','N/A','','','','','Member','','Voting Member','N/A','February','2014','May','2014','Retired','Standard','no'),
	(61,'smasonaz','Sarah','Mason','','','','','smasonaz@email.arizona.edu','','','','','N/A','','','','','Member','','Voting Member','N/A','February','2014','May','2014','Retired','Standard','yes'),
	(62,'alexandriaboice','Alexandria','Boice','','','','','alexandriaboice@email.arizona.edu','','','','','N/A','','','','','Member','','Voting Member','N/A','February','2014','May','2014','Retired','Standard','yes'),
	(63,'Fishk94','Kyle','Fisher','','','','','Fishk94@email.arizona.edu','','','','','N/A','','','','','Member','','Voting Member','N/A','February','2014','May','2014','Retired','Standard','yes'),
	(64,'knmaroney','Kelly','Maroney','','','','','knmaroney@email.arizona.edu','','','','','N/A','','','','','Member','','Voting Member','N/A','February','2014','May','2014','Retired','Standard','yes'),
	(65,'aliciabarrios','Alicia','Barrios','','','','','aliciabarrios@email.arizona.edu','','','','','N/A','','','','','Member','','Voting Member','N/A','February','2014','May','2014','Retired','Standard','yes'),
	(66,'aislinnroberts','Aislinn','Roberts','','','','','aislinnroberts@email.arizona.edu','','','','','N/A','','','','','Member','','Voting Member','N/A','February','2014','May','2014','Retired','Standard','yes'),
	(67,'nhavey','Nick','Havey','','','','','nhavey@email.arizona.edu','','','','','N/A','','','','','Member','','Voting Member','N/A','February','2014','May','2014','Retired','Standard','yes'),
	(68,'osoo','Olukemi','Oso','','','','','osoo@email.arizona.edu','','','','','N/A','','','','','Chair','','Voting Member','Board Chair','February','2014','May','2014','Retired','Chair','yes'),
	(69,'kubac','Christopher','Kuba','','','','','','','','','','N/A','','','','','Member','','Voting Member','N/A','April','2014','May','2014','Retired','Standard','yes'),
	(70,'awaegli','Andrew','Waegli','','','','','','','','','','N/A','','','','','','Web Developer','NON-MEMBER','Building and maintaining the website','M','2014','','','N/A','PowerUser','no'),
	(71,'bmitchell','Bryan','Mitchell','bmitchell','','','','bmitchell@email.arizona.edu','','','Arizona','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2015','June','2015','Retired','Standard','yes'),
	(72,'cryan14','Cole','Ryan','cryan14','','','','cryan14@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2015','June','2015','Retired','Standard','yes'),
	(73,'hollydurr','Holly','Durr','','','','','hollydurr@email.arizona.edu','','','','','Sophomore','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2015','June','2015','Retired','Standard','yes'),
	(74,'kokane','Kaitlyn','O\'Kane','','','','','kokane@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2015','June','2015','Retired','Standard','yes'),
	(75,'lmunoz13','Louie','Munoz','','','','','lmunoz13@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2015','June','2015','Retired','Standard','yes'),
	(76,'mrobles2896','Michael','Robles','','','','','mrobles2896@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2015','June','2015','Retired','Standard','yes'),
	(77,'tzaman','Tanzida','Zaman','','','','','tzaman@email.arizona.edu','','','','','Graduate','','','','','Chair','','Voting Member','Deliberate and recommend action on funding proposals.','January','2015','June','2016','Retired','Standard','yes'),
	(78,'nhavey','Nick','Havey','','','','','nhavey@email.arizona.edu','','','','','Junior','','','','','Chair','','Voting Member','Deliberate and recommend action on funding proposals.','January','2015','June','2016','Retired','Chair','yes'),
	(79,'adamciampaglio','Adam','Ciampaglio','','','','','adamciampaglio@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and recommend action on funding proposals.','January','2015','June','2015','Retired','Standard','yes'),
	(101,'yuwenwang','Yuwen','Wang','','','','','yuwenwang@email.arizona.edu','','','Arizona','','N/A','','','','','Member','','NON-MEMBER','Manage Freshman Fee website','July','2017','','','Active','PowerUser','no'),
	(81,'bullockj','Janice','Bullock','','','','','','','','','','N/A','','','','','Member','','NON-MEMBER','Provide administrative and financial support for the advisory board.','January','2016','','','N/A','PowerUser','no'),
	(82,'amberleecampman','Amber','Campman','','','','','amberleecampman@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','February','2016','June','2016','Retired','Standard','yes'),
	(83,'rcrocco','Ronnie','Crocco','','','','','rcrocco@email.arizona.edu','','','','','Senior','','','','','Chair','','Voting Member','Deliberate and make recommendations on funding proposals','February','2016','June','2017','Retired','Standard','yes'),
	(84,'jalonjackson','Jalon','Jackson','','','','','jalonjackson@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','February','2016','June','2016','Retired','Standard','yes'),
	(85,'jgutierrez2','Jessica','Gutierrez','','','','','jgutierrez2@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','February','2016','June','2016','Retired','Standard','yes'),
	(86,'ljohn117','Lorenzo','Johnson','','','','','ljohn117@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','January','2016','June','2016','Retired','Standard','yes'),
	(87,'mokothe','Monica','Kothe','','','','','mokothe@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','February','2016','June','2016','Retired','Standard','yes'),
	(88,'roman3','Saul','Roman','','','','','roman3@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','February','2016','June','2016','Retired','Standard','yes'),
	(89,'aarmour','Andrew','Armour','','','','','aarmour@catworks.arizona.edu','','','','','Senior','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','July','2016','June','2018','Active','PowerUser','yes'),
	(90,'marianna11mtz','Marianna','Martinez','','','','','marianna11mtz@email.arizona.edu','','','','','Sophomore','','','','','Member','','NON-MEMBER','Admin','July','2016','','','N/A','Administrative','no'),
	(92,'tledbetter','Trevor','Ledbetter','','','','','tledbetter@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','April','2017','June','2017','Active','Standard','yes'),
	(93,'hhogue','Hunter','Hogue','','','','','','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','April','2017','June','2017','Active','Standard','yes'),
	(94,'jjenkins5','Jacob','Jenkins','','','','','jjenkins5@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','April','2017','June','2017','Active','Standard','yes'),
	(95,'mdavenport','Michaela','Davenport','','','','','mdavenport@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','April','2017','June','2017','Active','Standard','yes'),
	(96,'nochoahurst','Nicolas','Ochoa-Hurst','','','','','nochoahurst@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','April','2017','June','2017','Active','Standard','yes'),
	(97,'krishnap','Krishna','Patel','','','','','Krishnap@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','April','2017','June','2017','Active','Standard','yes'),
	(98,'dlandrews','Deidre','Andrews','','','','','dlandrews@email.arizona.edu','','','','','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','April','2017','June','2017','Active','Standard','yes'),
	(99,'arthur','Art','Contreras','','','','','','','','','','N/A','','','','','Member','','NON-MEMBER','Provide Administrative and Financial support for the Advisory Board','May','2017','','','N/A','PowerUser','no'),
	(102,'aarmour','Andrew','Armour','','','','','aarmour@email.arizona.edu','','','Arizona','','N/A','','','','','','Staff','NON-MEMBER','Update website','July','2017','July','2018','N/A','PowerUser','no'),
	(103,'jordanstrang','Jordan','Strang','','','','','jordanstrang@email.arizona.edu','','Tucson','Arizona','85721','Senior','','','','','Chair','','Voting Member','represent all Freshman at the University of Arizona, maintain unbiased approach, understand and outreach to the departments of Student Affairs and be familiar with university-wide budgets.','January','2018','','','Active','Chair','yes'),
	(105,'bnobbe','Bridgette','Nobbe','23413617','920','257','9339','bnobbe@email.arizona.edu','850 E Wetmore Rd','Tucson','Arizona','85721','Graduate','Higher Education Student Organization','','','','Member','','Voting Member','Represent all Freshman at the University of Arizona, maintain unbiased approach, understand and outreach to the departments of Student Affairs and be familiar with university-wide budgets.','January','2018','','','Active','Standard','yes'),
	(106,'anthonyrusk','Anthony','Rusk','','','','','anthonyrusk@email.arizona.edu','','Tucson','Arizona','85741','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','March','2018','May','2018','Active','Standard','yes'),
	(107,'spencercampman','Spencer','Campman','','','','','spencercampman@email.arizona.edu','','Tucson','','85741','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','March','2018','May','2018','Active','Standard','yes'),
	(108,'vanessaordway','Vanessa','Ordway','','','','','vanessaordway@email.arizona.edu','','Tucson','Arizona','85741','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','March','2018','May','2018','Active','Standard','yes'),
	(109,'taliac','Talia','Capozzoli','','','','','taliac@email.arizona.edu','','Tucson','Arizona','85741','Junior','','','','','Secretary','','Voting Member','Deliberate and make recommendations on funding proposals','March','2018','May','2018','Active','Standard','yes'),
	(110,'dmireles','Deanna','Mireles','','','','','dmireles@email.arizona.edu','','Tucson','Arizona','85741','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','March','2018','','','Active','Standard','yes'),
	(111,'kmaynard1','Kaylee','Maynard','','','','','kmaynard1@email.arizona.edu','','Tucson','Arizona','85741','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','March','2018','','','Active','Standard','yes'),
	(112,'bdryden','Brody','Dryden','','','','','bdryden@email.arizona.edu','','Tucson','Arizona','85741','Freshman','','','','','Member','','Voting Member','Deliberate and make recommendations on funding proposals','March','2018','','','Active','Standard','yes');

/*!40000 ALTER TABLE `freshmanfee_board_mem` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
