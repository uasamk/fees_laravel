SET @project_set = 4;

DROP TABLE IF EXISTS fees_old.pars;

ALTER TABLE fees_old.par
CHANGE boardApproval boardApproval VARCHAR(191),
CHANGE vpApproval vpApproval VARCHAR(191);

UPDATE fees_old.par
SET proposalID = CONCAT(@project_set, 999, proposalID),
id = CONCAT(@project_set, 999, id),
boardApproval = CASE boardApproval WHEN "Yes" THEN 1 ELSE 0 END,
vpApproval = CASE vpApproval WHEN "Yes" THEN 1 ELSE 0 END;

ALTER TABLE fees_old.par
ADD COLUMN project_id INT,
CHANGE id id_old INT,
CHANGE netID netid VARCHAR(191),
CHANGE modifiedByReview netid_review VARCHAR(191),
CHANGE modifiedByVPReview netid_vp VARCHAR(191),
CHANGE vpComments vp_comments TEXT,
CHANGE boardApproval approved TINYINT(1),
CHANGE vpApproval vp_approved TINYINT(1),
DROP COLUMN comments,
DROP COLUMN fileParSupport,
DROP COLUMN fileParSupportName,
DROP COLUMN fileAdvice,
DROP COLUMN fileAdviceName,
CHANGE COLUMN timeStamp created_at varchar(255),
CHANGE COLUMN timeStampReview timestamp_review varchar(255),
CHANGE COLUMN timeStampVPReview timestamp_vp varchar(255);

UPDATE fees_old.par par
SET created_at = FROM_UNIXTIME(created_at),
timestamp_review = FROM_UNIXTIME(timestamp_review),
timestamp_vp = FROM_UNIXTIME(timestamp_vp);

UPDATE fees_old.par par
INNER JOIN fees.projects projects
ON par.proposalID = projects.id_old
SET par.project_id = projects.id;

ALTER TABLE fees_old.par
DROP COLUMN proposalID;

RENAME TABLE par TO pars;