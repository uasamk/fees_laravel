# ff only
ALTER TABLE status
ADD COLUMN allocation tinyint default 0;

ALTER TABLE proposals
ADD COLUMN yearlyVariation varchar(255),
DROP COLUMN pastFundingExplain;

UPDATE proposals
SET amount = REPLACE(amount, ",", ""),
amount = REPLACE(amount, "$", "");