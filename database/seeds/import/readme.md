# Introduction
These instructions will build the necessary migration files out of the old fees data to be imported into the new laravel site. This only needs to be done if the old database changed needs to be re-imported for some reason

## Note:
At the top of most sql files, there will be a variable that looks like this:
```
SET @project_set = 4;
```
Make sure this line points to the correct project set depending on what site you're importing

# Import the data from the old site
Create a database called `fees_old` alongside the new `fees` database in your local dev environment and import the board and funding sql files

# Run any site specific preparation queries
Different sites might have slightly different data structures that might need to be standardized before import. In this case there will be a prepare_[site].sql that needs to be run before anything else.

If this is not the first site in the series, you'll also want to remove this line from the top of your newly generated seeders:
```
\DB::table('table_name')->delete();
```

# Prepare projects table
Run prepare_projects.sql in your favorite sql client and then:
```
php artisan iseed projects --database=mysql2
php artisan db:seed --class=ProjectsTableSeeder
```

# Prepare the initiatives and pars table
Run prepare_initiatives.sql, prepare_pars.sql and then:
```
php artisan iseed initiative_project --database=mysql2
php artisan iseed pars --database=mysql2
```

Then Run:
```
php artisan db:seed --class=InitiativeProjectTableSeeder
php artisan db:seed --class=ParsTableSeeder
```

# Prepare the questions & answers tables
Run prepare_questions.sql and then:
```
php artisan iseed questions --database=mysql2
php artisan db:seed --class=QuestionsTableSeeder
```

Then run prepare_answers.sql, then:
```
php artisan iseed answers --database=mysql2
php artisan db:seed --class=AnswersTableSeeder
```

# Prepare the progress reports table
Run prepare_progress_reports.sql, then:
```
php artisan iseed progress_reports --database=mysql2
php artisan db:seed --class=ProgressReportsTableSeeder
```

# Prepare the users and user/sites tables
Make sure this line is set to the correct table at the top of prepare_users.sql:
```
RENAME TABLE [site]_board_mem TO users;
```

Run prepare_users.sql, then:
```
php artisan iseed users --database=mysql2
php artisan db:seed --class=UsersTableSeeder
```

Now, make sure this line `SET @site = 2` is set to the correct site in prepare_site_user.sql and run it, then:
```
php artisan iseed site_user --database=mysql2
php artisan db:seed --class=SiteUserTableSeeder
```
